"use strict";

/* BEGIN DATA TABLE */

function clearString(string) {
    string = string.toString();
    string = string.toLowerCase();
    string = string.trim();
    return string;
}

var DataRow = React.createClass({
    displayName: "DataRow",

    render: function render() {
        var align = this.props.text_align || 'center-th';
        var isHeader = this.props.isHeader || false;
        if (isHeader) return React.createElement(
            "th",
            { className: "{align}" },
            this.props.value
        );else return React.createElement(
            "td",
            { className: "{align}" },
            this.props.value
        );
    }
});

var Row = React.createClass({
    displayName: "Row",

    render: function render() {
        // get props
        var isHeader = this.props.isHeader || false;
        var addClass = this.props.addClass || '';
        var text_align = this.props.text_align || 'center-th';
        var addRole = this.props.addRole || '';
        var columns = this.props.columns || [];
        var valueFilters = this.props.valueFilters || [];

        // rendering
        var dataRows = columns.map(function (item, i) {
            return React.createElement(DataRow, { value: item, text_align: text_align, isHeader: isHeader });
        });

        // output
        return React.createElement(
            "tr",
            { role: addRole, className: addClass },
            dataRows
        );
    }
});

var InputFilter = React.createClass({
    displayName: "InputFilter",
    getInitialState: function getInitialState() {
        return {
            index: this.props.index
        };
    },

    render: function render() {
        // get props
        var column = this.props.column || {};
        var filters = this.props.filters || [];
        var indexRow = this.props.index;

        var isFiltered = false;
        filters.forEach(function (value, index) {
            if (value.index == indexRow) isFiltered = true;
        });
        if (isFiltered) return React.createElement("input", { type: "text", className: "input-filter form-control", onChange: this.changeListener });else return React.createElement("span", null);
    },
    changeListener: function changeListener(event) {
        var parent = this._reactInternalInstance._currentElement._owner._instance;
        parent.changeFilter(this.state.index, event.target.value);
    }
});

var FilterHeader = React.createClass({
    displayName: "FilterHeader",

    render: function render() {
        // get props
        var columns = this.props.columns[0] || [];
        var filters = this.props.filters || [];

        var filtersInputs = columns.map(function (item, i) {
            return React.createElement(
                "td",
                null,
                React.createElement(InputFilter, { column: item, index: i, filters: filters })
            );
        });
        return React.createElement(
            "tr",
            { role: "row", "class": "filter" },
            " ",
            filtersInputs,
            " "
        );
    },
    changeFilter: function changeFilter(index, filtro) {
        var parent = this._reactInternalInstance._currentElement._owner._instance;
        parent.changeFilter(index, filtro);
    }
});

var Header = React.createClass({
    displayName: "Header",

    render: function render() {
        // get props
        var rows = this.props.rows || [];
        var filters = this.props.filters || [];

        // rendering
        var titles = rows.map(function (item, i) {
            return React.createElement(Row, { columns: item, isHeader: "true", addClass: "heading cursor", addRole: "row" });
        });
        var colsFilters = "";

        // count num of filters
        if (filters.length > 0) {
            colsFilters = React.createElement(FilterHeader, { columns: rows, filters: filters });
        }

        // output
        return React.createElement(
            "thead",
            null,
            titles,
            colsFilters
        );
    },
    changeFilter: function changeFilter(index, filtro) {
        var parent = this._reactInternalInstance._currentElement._owner._instance;
        parent.changeFilter(index, filtro);
    }
});

var Body = React.createClass({
    displayName: "Body",

    render: function render() {
        // get props
        var rows = this.props.rows || [];
        var limit = this.props.limit || 0;
        var valueFilters = this.props.valueFilters || {};
        var page = this.props.page || 1;
        var filterData = [];

        // filter data
        var keys = Object.keys(valueFilters);
        if (keys.length > 0) {
            // keys of filters
            keys.forEach(function (key) {
                var value = valueFilters[key];

                filterData = [];
                rows.every(function (row, index) {
                    if (limit > 0 && filterData.length > limit) return false;
                    if (limit > 0 && index < limit * (page - 1)) return true;

                    if (value != "" && value != undefined && index >= limit * page) {
                        // sanitize
                        if (clearString(row[key]).includes(clearString(value))) {
                            filterData.push(row);
                        }
                    }
                    return true;
                });
                rows = filterData;
            });
        } else {
            if (limit > 0) {
                filterData = [];
                rows.every(function (item, i) {
                    if (limit > 0 && i < limit * (page - 1)) return true;
                    if (filterData.length >= limit) if (i >= limit * (page - 1)) return false;
                    filterData.push(item);
                    return true;
                });
            } else filterData = rows;
        }

        // rendering
        var bodyRows = filterData.map(function (item, i) {
            return React.createElement(Row, { columns: item, valueFilters: valueFilters });
        });
        // output
        return React.createElement(
            "tbody",
            null,
            bodyRows
        );
    }
});

var Footer = React.createClass({
    displayName: "Footer",
    getInitialState: function getInitialState() {
        return {
            page: this.props.page || 1,
            numPages: Math.ceil(this.props.rows.length / (this.props.limit > 0 ? this.props.limit : this.props.rows.length))
        };
    },

    render: function render() {
        var rows = this.props.rows || [];
        var limit = this.props.limit || 0;
        // var numPages = rows.length / (limit > 0) ? limit : rows.length
        // numPages = (numPages % 1 != 0) ? Math.ceil(numPages) : numPages

        var colspan = 0;
        if (rows.length > 0) colspan = rows[0].length;

        return React.createElement(
            "tfoot",
            null,
            React.createElement(
                "tr",
                null,
                React.createElement(
                    "td",
                    { colSpan: colspan },
                    React.createElement(
                        "div",
                        { className: "col-md-5 col-sm-12" },
                        "P\xE1gina ",
                        this.state.page,
                        " de ",
                        this.state.numPages
                    ),
                    React.createElement(
                        "div",
                        { className: "col-md-7 col-sm-12" },
                        React.createElement(
                            "button",
                            { className: "btn", onClick: this.prev },
                            " ",
                            "<",
                            " "
                        ),
                        React.createElement("input", { onChange: this.setPage, type: "text", value: this.state.page, className: "pagination-panel-input form-control input-sm input-inline input-mini", maxlength: "5" }),
                        React.createElement(
                            "button",
                            { className: "btn", onClick: this.next },
                            " ",
                            ">",
                            " "
                        )
                    )
                )
            )
        );
    },
    prev: function prev() {
        if (this.state.page - 1 > 0) {
            var parent = this._reactInternalInstance._currentElement._owner._instance;
            this.setState({ page: this.state.page - 1 });
            parent.setPage(this.state.page - 1);
        }
    },
    next: function next() {
        if (this.state.page + 1 <= this.state.numPages) {
            var parent = this._reactInternalInstance._currentElement._owner._instance;
            this.setState({ page: this.state.page + 1 });
            parent.setPage(this.state.page + 1);
        }
    },
    setPage: function setPage() {
        var page = event.target.value;
        if (parseInt(page)) {
            if (page >= 1 && page <= this.state.numPages) {
                var parent = this._reactInternalInstance._currentElement._owner._instance;
                this.setState({ page: page });
                parent.setPage(page);
            }
        }
    }
});

/*
    header.type 
     == text
     == number
     == date ( startDate, endDate )

    props => {
        header : [
            { name : '', filter : false, type : 'text' }
        ],
        pagination : false,

    }

*/
var DataTable = React.createClass({
    displayName: "DataTable",
    getInitialState: function getInitialState() {
        return {
            valueFilters: {},
            page: 1
        };
    },

    render: function render() {
        var options = this.props.options;
        var id = options.id || 'datatable';
        var head = "";
        var body = "";
        var foot = "";

        var filters = options.filters || [];

        if (options.header) {
            head = React.createElement(Header, { rows: options.header, filters: filters });
        }
        if (options.body) {
            body = React.createElement(Body, { rows: options.body, valueFilters: this.state.valueFilters, limit: options.limit, page: this.state.page });
        }
        if (options.pagination) {
            foot = React.createElement(Footer, { rows: options.body, limit: options.limit, page: this.state.page });
        }
        var table = React.createElement(
            "table",
            { className: "table table-striped table-bordered table-hover table-header-fixed dataTable no-footer", id: id },
            head,
            body,
            foot
        );
        return table;
    },
    changeFilter: function changeFilter(index, filtro) {
        var filters = this.state.valueFilters;
        if (filtro != "" && filtro != undefined) {
            filters[index] = filtro;
        } else {
            delete filters[index];
        }
        this.setState({ valueFilters: filters });
    },
    setPage: function setPage(_page) {
        this.setState({ page: _page });
    },
    setLimit: function setLimit(limit) {
        var newLimit = limit > 0 ? limit : 0;
        this.props.options.limit = newLimit;
    }
});