import React from 'react'
import './styles.css'

class AccordionItem extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            active: false
        }
        this.toggle = this.toggle.bind(this);
    }

    toggle() {
        this.setState({
            active: !this.state.active,
            className: "active"
        });
    }

    render() {
        const activeClass = this.state.active ? "active" : "inactive";
        const question = this.props.details;
        return (
            <div className={activeClass}>
                <span className="summary" onClick={this.toggle}>{question.summary}</span>
                <span className="folding-pannel answer">{question.answer}</span>
            </div>
        );
    }
}
  
class Accordion extends React.Component {

    constructor(props) {
        super(props)
        this.renderQuestion = this.renderQuestion.bind(this);
    }

    renderQuestion(key) {
        return <AccordionItem key={key} index={key} details={this.props.tabs[key]} />
    }

    render() {
        const { tabs } = this.props
        return(
            <div>
                <div className="accordion-container">
                    {Object.keys(tabs).map(this.renderQuestion)}
                </div>
            </div>
        )
    }
}

export default Accordion