import React from 'react';
import Sound from 'react-sound';
import {
    Button
} from '@material-ui/core'
import {
    PlayArrow,
    Stop
} from '@material-ui/icons'

class PlayerControls extends React.Component {
    state = {
        playStatus: Sound.status.STOPPED
    }

    constructor(props){
        super(props)
        this.play = this.play.bind(this)
        this.stop = this.stop.bind(this)
        this.renderControls = this.renderControls.bind(this)
    }

    render() {
        return <div>{this.renderControls()}</div>;
    }

    play(){
        this.setState({
            playStatus : Sound.status.PLAYING
        })
    }

    stop(){
        this.setState({
            playStatus : Sound.status.STOPPED  
        })
    }

    renderControls() {
        return (
            <div>
                <Sound
                    url={this.props.url}
                    playStatus={this.state.playStatus}
                    onFinishedPlaying={() => this.setState({ playStatus: Sound.status.STOPPED })}
                    debugMode={false}
                    consoleOnly={false}
                    useConsole={false}
                />
                <Button variant="contained" onClick={this.play}>
                    <PlayArrow />
                </Button>
                <Button variant="contained" onClick={this.stop}>
                    <Stop />
                </Button>
            </div>
        );
    }
}

class Notification extends React.Component {

    state = {
        playStatus: Sound.status.STOPPED
    }

    play(){
        this.setState({
            playStatus : Sound.status.PLAYING
        })
    }

    stop(){
        this.setState({
            playStatus : Sound.status.STOPPED  
        })
    }

    render(){
        const { play } = this.props
        if(play === true) this.play()
        return (
            <Sound
                url={'https://s3.amazonaws.com/json-publicos/notification.mp3'}
                playStatus={this.state.playStatus}
                onFinishedPlaying={() => this.setState({ playStatus: Sound.status.STOPPED })}
                debugMode={false}
                consoleOnly={false}
                useConsole={false}
            />
        )
    }
} 

export {Notification}
export default PlayerControls