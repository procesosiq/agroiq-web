import React from 'react';
import Sound from 'react-sound';

class Notification extends React.Component {

    state = {
        playStatus: Sound.status.STOPPED
    }

    play(){
        this.setState({
            playStatus : Sound.status.PLAYING
        })
    }

    stop(){
        this.setState({
            playStatus : Sound.status.STOPPED  
        })
    }

    render(){
        const { play } = this.props
        if(play === true) this.play()
        return (
            <Sound
                url={'https://s3.amazonaws.com/json-publicos/notification.mp3'}
                playStatus={this.state.playStatus}
                onFinishedPlaying={() => this.setState({ playStatus: Sound.status.STOPPED })}
                debugMode={false}
                consoleOnly={false}
                useConsole={false}
            />
        )
    }
} 

export default Notification