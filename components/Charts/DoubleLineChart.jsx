import React from 'react'
import {LineChart, Line, XAxis, YAxis, CartesianGrid, ResponsiveContainer, Tooltip, ReferenceLine} from 'recharts';

class LinesChart extends React.Component {
	render () {
        const { data, umbral } = this.props
        return (
            <div style={{height: 400}}>
                <ResponsiveContainer>
                    <LineChart data={data}>
                        <XAxis dataKey="name"/>
                        <YAxis yAxisId="left"/>
                        <YAxis yAxisId="right" orientation="right"/>
                        <CartesianGrid strokeDasharray="3 3"/>
                        <Tooltip />

                        { umbral &&
                            <ReferenceLine y={umbral} label="" stroke="red"/>
                        }
                        <Line name="KM" yAxisId="left" type="monotone" dataKey="value" stroke="#2ca02c"/>
                        <Line name="Galones" yAxisId="right" type="monotone" dataKey="value2" stroke="#d62728" />
                    </LineChart>
                </ResponsiveContainer>
            </div>
        );
    }
}

export default LinesChart