import { createStore } from 'redux'

const initialState = {
    chat : {},
    maximized : false
}

const reducer = (state = initialState, action) => {
    switch(action.type){
        case 'OPEN_CHAT': {
            return Object.assign(state, {
                    lastAction : action.type,
                    chat : {
                        id_socket : action.id_socket,
                        name : action.name
                    }
                }
            )
        }
        default: break;
    }

    return Object.assign(state, { lastAction : action.type })
}
const store = createStore(reducer)
export default store
