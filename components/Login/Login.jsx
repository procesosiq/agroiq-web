import React from 'react'
import "../../assets/pages/css/login-5.min.css"
import "../../assets/global/plugins/uniform/css/uniform.default.css"
import "../../assets/global/css/plugins.min.css"
import "jquery-backstretch"
import LogoAgroIQ from '../../logo.png';
import Img1 from '../../img/login/1.jpg'
import Img2 from '../../img/login/2.jpg'
import Img3 from '../../img/login/3.jpg'
import Img4 from '../../img/login/4.jpg'
import Auth from '../../utils/store/session'
const $ = window.$

class Login extends React.Component {
    state = { username : '', password : '' }
    constructor(props){
        super(props)
        this._functionality = this._functionality.bind(this)
        this._submit = this._submit.bind(this)
        this._onChange = this._onChange.bind(this)
    }

    componentDidMount(){
        this._functionality()

        $('.login-bg').backstretch([
                Img1, Img2, Img3, Img4
            ], {
                fade: 1000,
                duration: 8000
            }
        );

        $('.forget-form').hide();
    }

    _onChange(e){
        this.setState({
            [e.target.name] : e.target.value
        })
    }

    async _submit(e){
        e.preventDefault()
        Auth.dispatch({
            type : 'AUTHENTICATE',
            user : this.state.username,
            password : this.state.password
        })
    }

    _functionality(){
        $(".login-form").on('submit', this._submit)

        $('.login-form input').keypress(function(e) {
            if (e.which === 13) {
                $('.login-form').submit();
                return true
            }
        });

        $('.forget-form input').keypress(function(e) {
            if (e.which === 13) {
                $('.forget-form').submit();
                return true
            }
        });

        $('#forget-password').click(function(){
            $('.login-form').hide();
            $('.forget-form').show();
        });

        $('#back-btn').click(function(){
            $('.login-form').show();
            $('.forget-form').hide();
        });
    }

    render(){
        return (
            <div className="user-login-5">
                <div className="row bs-reset">
                    <div className="col-md-6 login-container bs-reset">
                        <img className="login-logo login-6" alt="Logo" src={LogoAgroIQ}  style={{width:'70%'}} />

                        <div className="login-content">
                            <h1>Inicio de Sesión</h1>
                            <p> Introduzca su usuario y contraseña para ingresar al portal.<br/>
                            Si usted no recuerda su correo y/o contraseña de clic en 'olvidó contraseña' y enviaremos a su correo la información
                            </p>
                            <form action="#" className="login-form" method="post">
                                <div className="alert alert-danger display-hide">
                                    <button className="close" data-close="alert"></button>
                                    <span>Ingrese su correo y contraseña. </span>
                                </div>
                                <div className="row">
                                    <div className="col-xs-6">
                                        <input className="form-control form-control-solid placeholder-no-fix form-group" type="text" placeholder="Correo" name="username" required onChange={this._onChange}/>
                                    </div>
                                    <div className="col-xs-6">
                                        <input className="form-control form-control-solid placeholder-no-fix form-group" type="password" placeholder="Contraseña" name="password" required onChange={this._onChange}/>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-sm-offset-4 col-sm-8 text-right">
                                        <div className="forgot-password">
                                            <a id="forget-password" className="forget-password">Olvidó su contraseña?</a>
                                        </div>
                                        <button className="btn blue" type="submit">Ingresar</button>
                                    </div>
                                </div>
                            </form>
                            <form className="forget-form" method="post">
                                <h3 className="font-green">Olvide mi contraseña ?</h3>
                                <p> Ingrese su e-mail para recuperar su contraseña. </p>
                                <div className="form-group">
                                    <input className="form-control placeholder-no-fix" type="text" placeholder="Email" name="email" />
                                </div>
                                <div className="form-actions">
                                    <button type="button" id="back-btn" className="btn grey btn-default">Regresar</button>
                                    <button type="submit" className="btn blue btn-success uppercase pull-right">Enviar</button>
                                </div>
                            </form>
                        </div>
                        <div className="login-footer">
                            <div className="row bs-reset">
                                <div className="col-xs-5 bs-reset">
                                    <ul className="login-social">
                                        <li>
                                            <a>
                                                <i className="icon-social-facebook"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a >
                                                <i className="icon-social-twitter"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a >
                                                <i className="icon-social-dribbble"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div className="col-xs-7 bs-reset">
                                    <div className="login-copyright text-right">
                                        <p>Copyright &copy; PROCESOS IQ 2016</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-6 bs-reset">
                        <div className="login-bg"> </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Login