import React from 'react'

class Stat extends React.Component {
    render(){
        const { color, title, title_small, value, subtitle, sub_value, col, icon, porcent, style, styles } = this.props

        return (
            <div className={`col-lg-${col} col-md-${col}`}>
                <div className="dashboard-stat2 bordered" style={{...style, ...styles.content}}>
                    <div className="display">
                        <div className="number">
                            <h3 className={`font-${color}`}>
                                <span data-counter="counterup" data-value={`${value}`} style={styles.title}>{ value }</span>
                                <small className={`font-${color}`}>{ React.isValidElement(title_small) ? title_small : title_small }</small>
                            </h3>
                            <small>{ title }</small>
                        </div>
                        <div className="icon">
                            <i className={icon}></i>
                        </div>
                    </div>
                    { sub_value &&
                        <div className="progress-info">
                            <div className="progress">
                                <span style={{width: `${sub_value}%`, ...styles.bar}} className={`progress-bar ${color}`}>
                                    <span className="sr-only">{sub_value} { subtitle }</span>
                                </span>
                            </div>
                            <div className="status">
                                <div className="status-title"> {subtitle} </div>
                                <div className="status-number"> {sub_value} {sub_value && porcent ? '%' : ''} </div>
                            </div>
                        </div>
                    }
                </div>
            </div>
        )
    }
}

Stat.defaultProps = {
    color : 'green-sharp',
    title : '',
    value : 0,
    subtitle : '',
    sub_value : '',
    col : 3,
    icon : '',
    porcent : false,
    fontSize : 30,
    styles : {}
}

export default Stat