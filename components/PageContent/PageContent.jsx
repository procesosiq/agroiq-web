import React from 'react'
import "./../../assets/layouts/layout2/css/themes/dark.css"
import SideMenu from '../SideMenu'
import Header from '../Header'
import Footer from '../Footer'

class PageContent extends React.Component {
    render(){
        const { Body, Logo } = this.props
        return (
            <div className="page-container-bg-solid page-header-fixed page-sidebar-fixed page-sidebar-closed-hide-logo">
                <Header LogoModule={Logo} />
                <div className="clearfix"> </div>
                <div id="page_content" className="page-container">
                    <SideMenu />
                    <div className="page-content-wrapper">
                        <div className="page-content">
                            { Body }
                        </div>
                    </div>
                </div>
                <Footer />
            </div>
        )
    }
}

export default PageContent