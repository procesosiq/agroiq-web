import React from 'react'
import { withStyles } from '@material-ui/core/styles';
import { Typography, Card, CardContent, CardMedia, CardHeader, Menu, MenuItem } from '@material-ui/core';
import request from 'utils/request'
import Auth from 'utils/store/session'
import DefaultAvatar from 'assets/default_avatar.jpg'
import { IconButton } from '@material-ui/core';
import { MoreVert as MoreVertIcon } from '@material-ui/icons';
import UploadPhotoModal from './UploadPhotoModal'

const classes = theme => ({
    card: {
        maxWidth: 345,
    },
    media: {
        height: 0,
        paddingTop: '100%', // 16:9
    },
    actions: {
        display: 'flex',
      },
    expand: {
        transform: 'rotate(0deg)',
        transition: theme.transitions.create('transform', {
          duration: theme.transitions.duration.shortest,
        }),
        marginLeft: 'auto',
        [theme.breakpoints.up('sm')]: {
          marginRight: -8,
        },
      },
      expandOpen: {
        transform: 'rotate(180deg)',
      },
})
const ITEM_HEIGHT = 48;

class Profile extends React.Component {

    state = {
        user_data : {
            image : DefaultAvatar
        },
        uploadphoto : false
    }

    componentDidMount(){
        this.changeImage = this.changeImage.bind(this)
        this.closeModal = this.closeModal.bind(this)
        this.getData()
    }

    async getData(){
        let params = Auth.getState().session_data
        let r = await request.post('phrapi/profile/index', params)
        this.setState({
            user_data : r.data
        })
    }

    handleClick = event => {
        this.setState({ anchorEl: event.currentTarget });
    };

    handleClose = () => {
        this.setState({ anchorEl: null });
    };

    changeImage = () => {
        this.handleClose()
        this.setState({
            uploadphoto : true
        })
    }

    closeModal(){
        this.setState({
            uploadphoto : false
        })
    }

    render(){
        const { classes } = this.props
        const { user_data, anchorEl, uploadphoto } = this.state

        return (
            <div className="row">
                <div className="col-md-3">
                    <UploadPhotoModal 
                        open={uploadphoto}
                        handleClose={this.closeModal}
                    />
                    <Card className={classes.card}>
                        <CardHeader
                            action={
                                <div>
                                    <IconButton
                                        aria-label="More"
                                        aria-owns={anchorEl ? 'long-menu' : null}
                                        aria-haspopup="true"
                                        onClick={this.handleClick}
                                    >
                                        <MoreVertIcon />
                                    </IconButton>
                                    <Menu
                                        anchorEl={anchorEl}
                                        open={Boolean(anchorEl)}
                                        onClose={this.handleClose}
                                        PaperProps={{
                                            style: {
                                                maxHeight: ITEM_HEIGHT * 4.5,
                                                width: 200,
                                            },
                                        }}
                                    >
                                        <MenuItem onClick={this.handleClose}>Dar de Baja</MenuItem>
                                        <MenuItem onClick={this.changeImage}>Cambiar Imagen</MenuItem>
                                    </Menu>
                                </div>
                            }
                        />
                        <CardMedia
                            className={classes.media}
                            image={user_data.image}
                            src={'img'}
                        />
                        <CardContent>
                            <Typography gutterBottom variant="headline" component="h3">
                                Información de Usuario
                            </Typography>
                            <Typography component="p">
                                Alias : { user_data.label }
                            </Typography>

                            <br/>
                            <Typography gutterBottom variant="headline" component="h3">
                                Información Laboral
                            </Typography>
                            <Typography component="p">
                                Puesto : { user_data.employment }
                            </Typography>

                            <br/>
                            <Typography gutterBottom variant="headline" component="h3">
                                Información Personal
                            </Typography>
                            <Typography component="p">
                                Nombre : { user_data.name_complete } { user_data.lastname_complete }
                            </Typography>
                            <Typography component="p">
                                Cumpleaños : { user_data.birthday }
                            </Typography>
                        </CardContent>
                    </Card>
                </div>
                <div className="col-md-9">

                </div>
            </div>
        )
    }
}

export default withStyles(classes)(Profile)