import React from 'react'
import {
    Dialog,
    withStyles,
    DialogActions,
    Button
} from '@material-ui/core'
import ReactCrop from 'react-image-crop'
import 'react-image-crop/dist/ReactCrop.css'
import withMobileDialog from '@material-ui/core/withMobileDialog';

const styles = {
    appBar: {
        position: 'relative',
    },
    root : {
        width : '70%',
        minHeight : 300
    },
    primary : {
        background : 'linear-gradient(60deg, #66bb6a, #43a047)'
    },
    flex: {
        flex: 1,
    },
    actions : {
        bottom : 0
    }
}

class UploadPhotoModal extends React.Component {
    state = {
        src: null,
        crop: {
            x: 10,
            y: 10,
            width: 80,
            height: 80,
            aspect: 1
        },
    }

    constructor(props){
        super(props)
        this.handleClose = this.handleClose.bind(this)
    }

    onSelectFile = e => {
        if (e.target.files && e.target.files.length > 0) {
            const reader = new FileReader()
            reader.addEventListener(
                'load',
                () =>
                    this.setState({
                        src: reader.result,
                    }),
                    false
            )
            reader.readAsDataURL(e.target.files[0])
        }
    }

    onImageLoaded = image => {
        console.log('onCropComplete', image)
    }

    onCropComplete = crop => {
        console.log('onCropComplete', crop)
    }

    onCropChange = crop => {
        this.setState({ crop })
    }

    handleClose(){
        this.props.handleClose()
    }

    render() {
        const { selectedValue, classes, images, closeModal, ...other } = this.props;
        return (
            <Dialog 
                fullWidth
                aria-labelledby="simple-dialog-title" 
                {...other}
                classes={{paper : classes.root}}
                onEscapeKeyDown={closeModal}
                onBackdropClick={closeModal}
            >
                <div className="col-md-12">
                    <div class="row">
                        <div className="col-md-12">
                            <div className="fileinput fileinput-new" data-provides="fileinput">
                                <span className="input-group-addon btn default btn-file">
                                    <span className="fileinput-new"> Seleccionar Imagen </span>
                                    <input type="file" name="..." onChange={this.onSelectFile}/>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div className="row" style={{minHeight : 200}}>
                        {this.state.src && (
                            <ReactCrop
                                src={this.state.src}
                                crop={this.state.crop}
                                onImageLoaded={this.onImageLoaded}
                                onComplete={this.onCropComplete}
                                onChange={this.onCropChange}
                                style={{margin: 'auto'}}
                            />
                        )}
                    </div>
                </div>
                <DialogActions className={classes.actions}>
                    <Button onClick={this.handleClose} color="primary">
                        Cancelar
                    </Button>
                    <Button onClick={this.handleClose} color="primary" autoFocus>
                        Guardar
                    </Button>
                </DialogActions>
            </Dialog>
      );
    }
}

UploadPhotoModal.defaultProps = {
    onClose : () => {  }
}

export default withStyles(styles)(withMobileDialog()(UploadPhotoModal));