import React from 'react'
import ItemMenu from './ItemMenu'
import GroupItemMenu from './GroupItemMenu'

class SideMenu extends React.Component {
    render(){
        return ( 
            <div className="page-sidebar-wrapper">
                <div id="primary-page-sidebar" className="page-sidebar navbar-collapse collapse">
                    <ul className="page-sidebar-menu page-header-fixed page-sidebar-menu-compact" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
                        <ItemMenu name="Tickets" href="/reporteDia" />

                        <GroupItemMenu title="Balanzas">
                            <ItemMenu name="Allweights" href="/allweights" />
                            <ItemMenu name="Quintana" href="/balanzaQuintana" />
                        </GroupItemMenu>

                        <GroupItemMenu title="Configuración">
                            <ItemMenu name="Tutoriales" href="/tutoriales" />
                            <ItemMenu name="Prontoforms" href="/prontoforms" />
                        </GroupItemMenu>
                    </ul>
                </div>
            </div>
        )
    }
}

export default SideMenu