<?php
    /*=============================================================
    =            Insertar aqui validacion  de usuarios            =
    =============================================================*/
    $modulo = preg_replace('~/~', '_', getValueFrom($_GET,'page', 'agroaudit'));
    $session = Session::getInstance();
    $paths = new stdClass;
    $paths->sigat = '/logos/sigat.png';
    $paths->agroaudit = '/logos/agroaudit.png';
    $paths->merma = '/logos/merma.png';
    $paths->calidad = '/logos/calidad.png';
    $paths->membresias = '/logos/Logo.png';
    $paths->configuracion = '/logos/Logo.png';
    $paths->produccion = '/logos/produccion.png';
    $paths->lancofruit = '/logos/lancofruit.png';

    $agroaudit = [
        "laboresAgricolasDia",
        "laboresAgricolasComparativo",
        "laboresAgricolasTendencia",
        "laboresAgricolas",
        "reportes",
        "geoposicion",
        "rpersonal"
    ];
    $produccion = [
        "produccionsemana",
        "recobro",
        "producciongerencia",
        "produccionReporte",
        "produccionResumenCorte",
        "produccionDemo",
        "produccionComparacion",
        "produccionReporteAcumulado",
        "produccion",
        "produccioncajas",
        "produccioncajasbase",
        "produccionenfunde",
        "produccionRacimos",
        "produccionRacimosFormularios",
        "produccionPrecalibracion",
        "nuevoEnfundeSemanal"
    ];
    $lancofruit = [
        'revisionLancofruit',
        'lancofruit',
        'perchas',
        'lancofruitTemperatura',
        'ventas',
        'rastreo',
        'revisionLancofruitOnlyExcel',
        'importarInformacion',
        'cercaLancofruit',
        'configLancofruit',
        'rastreoLancofruit',
        'lancofruitVentasDia',
        'lancofruitVentasComparativo',
        'lancofruitVentasTendencia',
        'perchasDia',
        'perchasBD',
        'perchasComparacion',
        'perchasTendencia'
    ];
    $calidad = [
        'calidad',
        'calidad2',
        'calidadComparacion',
        'calidadTendencia'
    ];
    $merma = [
        'merma',
        'mermadia'
    ];
    
    $paths->image = '';
    if(in_array($modulo, $agroaudit)) $paths->image = $paths->agroaudit;
    if(in_array($modulo, $merma)) $paths->image = $paths->merma;
    if(in_array($modulo, $calidad)) $paths->image = $paths->calidad;
    if(in_array($modulo, $produccion)){
        $paths->image = $paths->produccion;
        $style = "height: 54px;";
    }
    if(in_array($modulo, $lancofruit)){
        $paths->image = $paths->lancofruit;
        $style = "height: 54px; width : 37%;";
    }

    /* IMAGEN DE PERFIL */
    $profile = "../assets/layouts/layout4/img/avatar9.jpg";
    $profile = "";
    if($session->id_company == 2)
        $profile = "./logos/logo_pubenza.png";
    if($session->id_company == 11)
        $profile = "./logos/logo_sumifru.png";

    /* NOTIFICACIONES */
    /*$piq = DB::getInstance();
    $sql = "SELECT 
                noti.id,
                noti.mensaje,
                tipo.icon,
                tipo.label_color,
                DATE(created_at) fecha
            FROM notificaciones noti
            INNER JOIN notificaciones_tipo tipo ON noti.id_tipo_notificacion = tipo.id
            WHERE id_company = $session->id_company AND status = 'Activo'
            ORDER BY created_at DESC";
    $notificaciones = $piq->queryAll($sql);*/
    $notificaciones = [];


    /*=====  End of Insertar aqui validacion  de usuarios  ======*/
?>
        <!-- BEGIN HEADER -->
        <div class="page-header navbar navbar-fixed-top">
            <!-- BEGIN HEADER INNER -->
            <div class="page-header-inner ">
                <!-- BEGIN LOGO -->
                <div class="page-logo">
                    <a href="start.php">
                        <img src="./logo.png" style="width: 132px; margin-top: 20px;" alt="logo" class="logo-default" />
                    </a>
                    <div class="menu-toggler sidebar-toggler" id="menu-toggler">
                        <!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
                    </div>
                </div>
                <!-- END LOGO -->
                <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
                <div class="page-top">
                    <div class="agrouaudit pull-left">
                        <ul class="nav navbar-nav pull-left">
                            <li class="dropdown dropdown-user dropdown-dark">
                                <?php if($paths->image != ''): ?> <img src="<?=$paths->image?>" class="agrouaudit-image" style="<?php if($modulo == "produccion" || $modulo=="produccionsemana"): ?> width:50% !important;  <?php endif; ?> <?= $style ?>"  alt=""> <?php endif; ?>
                            </li>
                        </ul>
                    </div>
                    <div class="top-menu">
                        <ul class="nav navbar-nav pull-right">
                            <li class="dropdown dropdown-extended dropdown-notification" id="header_notification_bar">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="true">
                                    <i class="<?= count($notificaciones) > 0 ? 'ring' : '' ?> icon-bell"></i>
                                    <span class="badge badge-default"><?= count($notificaciones) > 0 ? count($notificaciones) : '' ?></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <div class="slimScrollDiv" style="position: relative; overflow: auto; width: auto; height: 250px;" id="notificaciones">
                                            <ul class="dropdown-menu-list scroller" style="height: 250px; overflow: auto; width: auto;" data-handle-color="#637283" data-initialized="1">
                                                <?php foreach($notificaciones as $noti): ?>
                                                <li>
                                                    <a href="javascript:;">
                                                        <span class="time"><?= $noti->fecha == date('Y-m-d') ? 'Hoy' : $noti->fecha ?></span>
                                                        <span class="details">
                                                            <span class="label label-sm label-icon label-<?= $noti->label_color ?>">
                                                                <i class="fa fa-<?= $noti->icon ?>"></i>
                                                            </span> <?= $noti->mensaje ?>
                                                        </span>
                                                    </a>
                                                </li>
                                                <?php endforeach; ?>
                                                <?php if(count($notificaciones) == 0): ?>
                                                <li>
                                                    <a href="javascript:;">
                                                        
                                                        <span class="details">
                                                            <span class="label label-sm label-icon label-info">
                                                                <i class="fa fa-bell-slash-o"></i>
                                                            </span> NO HAY NOTIFICACIONES
                                                        </span>
                                                    </a>
                                                </li>
                                                <?php endif ; ?>
                                            </ul>
                                            <div class="slimScrollBar" style="background: rgb(99, 114, 131); width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 121.359px;"></div><div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(234, 234, 234); opacity: 0.2; z-index: 90; right: 1px;">
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <li class="dropdown dropdown-user dropdown-dark">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <span class="username username-hide-on-mobile"> <?php print Session::getInstance()->nombre; ?> </span>
                                    <!-- DOC: Do not remove below empty space(&nbsp;) as its purposely used -->
                                    <img alt="" class="img-circle" <?php if($profile != ''): ?>src="<?= $profile ?>" <?php endif; ?> />
                                </a>
                                <ul class="dropdown-menu dropdown-menu-default">
                                    <!--<li>
                                        <a href="index.php?page=user">
                                            <i class="icon-user"></i> Mi Perfil </a>
                                    </li>-->
                                    <li>
                                        <a href="phrapi/access/logout">
                                            <i class="icon-key"></i> Log Out </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <!-- END TOP NAVIGATION MENU -->
                </div>
                <!-- END PAGE TOP -->
            </div>
            <!-- END HEADER INNER -->
        </div>
        <!-- END HEADER -->
