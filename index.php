<?php include_once 'phrapi/index.php' ?>
<?php $factory->Access->if_not_logged_redirect() ?>
<?php $carga_modulo = $factory->CargaModulos ?>
<?php $vistas = $carga_modulo->vistas() ?>
<?php
    if($carga_modulo->modulo == 'dashboard'){
        header("Location: http://{$_SERVER['SERVER_NAME']}/start.php");
    }
    $footerFixed = "";
    if(Session::getInstance()->agent_user == "bonita"){
        $footerFixed = "";
    }
?>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-129242483-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-129242483-1');
        </script>

        <link href="assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/clockface/css/clockface.css" rel="stylesheet" type="text/css" />

        <!-- BEGIN MULTIPLE SELECT -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/css/bootstrap-select.min.css">
        <!-- END MULTIPLE SELECT -->
        <meta charset="utf-8" />
        <title>Agroaudit | Dashboard</title>
        <base href="<?=$config['url']?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
        <link href="assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="http://cdn.procesos-iq.com//global/plugins/jquery-multi-select/css/multi-select.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap-toastr/toastr.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/cubeportfolio/css/cubeportfolio.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap-table-master/bootstrap-table.min.css" rel="stylesheet" type="text/css" />
        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/css/autocomplete.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="assets/layouts/layout2/css/layout.css" rel="stylesheet" type="text/css" />
        <link href="assets/layouts/layout2/css/themes/dark.css" rel="stylesheet" type="text/css" id="style_color" />
        <!-- <link href="assets/layouts/layout4/css/themes/sigat.css" rel="stylesheet" type="text/css" id="style_color" /> -->
        <link href="assets/layouts/layout2/css/custom.css" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <link href="componentes/css/charts.css?1" rel="stylesheet" type="text/css" />
        <link rel="shortcut icon" href="favicon.ico" /> </head>
        <script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
    <!-- END HEAD -->
    <style>
        ul.page-sidebar-menu > li > .active {
            background: #5b9bd1 !important;
        }
        .bg-brown {
            background-color : rgb(151, 71, 6) !important;
        }
        .bg-font-brown {
            color : white !important;
        }
        .bg-brown:hover {
            background-color : rgb(151, 71, 6) !important;
        }
        .bg-font-brown:hover {
            color : white !important;
        }
        .dashboard-stat2 {
            border-radius : 3px !important;
            box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
        }
        .modal-auto {
            width: auto !important;
        }
        .modal-lg {
            width: 90% !important;
        }
        .modal-70 {
            width: 70% !important;
        }
        .modal-dialog {
            border-radius : 10px !important;
        }
        div.react-grid-Grid > div > div > div > div > div > div > div {
            border : unset !important;
            height : 100%;
        }
        .font-weight-bold {
            font-weight:bold;
        }
        .swal-icon {
            border-radius: 50% !important;
        }
        .swal-icon--warning__body {
            border-radius: 2px !important;
        }
        .swal-modal {
            border-radius : 5px !important;
        }
        .swal-icon--success__ring, .swal-icon {
            border-radius : 50% !important;
        }
        .swal-button {
            border-radius: 5px !important;
        }
        .modal-backdrop {
            z-index : 9998 !important;
        }
        .blockUI.blockOverlay {
            z-index : 10001 !important;
        }
        .blockUI.blockMsg.blockPage {
            z-index : 10001 !important;
        }
        .react-grid-Row {
            contain : unset !important;
        }
    </style>
    <body class="page-container-bg-solid page-header-fixed <?=$footerFixed?> page-sidebar-fixed page-sidebar-closed-hide-logo">

        <!-- page-header-fixed page-sidebar-fixed page-footer-fixed -->
    	<?php include ("header.php"); ?>
		<div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div id="page_content" class="page-container" ng-app="procesosiq">
            <?php include ("menu.php"); ?>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div id="preloader">
                        <div data-loader="circle-side">
                            <img src="iqicon.png" width="50" height="50" alt="IQ"/>
                        </div>
                    </div><!-- /Preload -->
                    
                    <?=$vistas['vista']?>

                    <div id="webchat"></div>
                </div>
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
        <?php include("footer.php");?>
        <!--[if lt IE 9]>
<script src="assets/global/plugins/respond.min.js"></script>
<script src="assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        
        <script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <!-- <script src="assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script> -->
        <script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>
        <script src="assets/pages/scripts/components-bootstrap-select.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="assets/global/plugins/moment.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>
        <script src="assets/global/plugins/morris/morris.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/morris/raphael-min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-toastr/toastr.min.js" type="text/javascript"></script>
        <!--<script src="assets/global/scripts/datatable.js" type="text/javascript"></script>-->
        <script src="assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
        <script src="assets/pages/scripts/table-datatables-fixedheader.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js?1" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
        
        <script src="//maps.google.com/maps/api/js?key=AIzaSyAaLJvNuFG6elskiX3RV5rA5_kKzMX9ltw&sensor=true" type="text/javascript"></script>
        <script src="assets/global/plugins/gmaps/gmaps.min.js" type="text/javascript"></script>

        <script src="assets/global/plugins/bootstrap-table-master/bootstrap-table.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/select2/js/select2.min.js" type="text/javascript"></script>


        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="assets/pages/scripts/dashboard.min.js" type="text/javascript"></script>
        <!-- <script src="//cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/0.10.0/ui-bootstrap-tpls.min.js"></script> -->
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="http://cdn.procesos-iq.com//global/plugins/jquery-multi-select/js/jquery.multi-select.js" type="text/javascript"></script>
        <script src="http://cdn.procesos-iq.com//global/plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>
        <script src="assets/layouts/layout4/scripts/layout.js" type="text/javascript"></script>
        <script src="assets/layouts/layout4/scripts/demo.min.js" type="text/javascript"></script>
        <script src="assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
        <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.7.1/angular.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/floatthead/2.0.3/jquery.floatThead.min.js"></script>
        <script src="js/echarts.min.js"></script>
        <script src="js/charts.js?1"></script>
        <script src="js/marlon.js?4" type="text/javascript"></script>
        <script src="js/autocomplete-alt.js" type="text/javascript"></script>
        <script src="js/main.js?4" type="text/javascript"></script>
        <script src="js/directives.js?2" type="text/javascript"></script>
        <script src="http://cdn.rawgit.com/MrRio/jsPDF/master/dist/jspdf.min.js"></script>
        <script src="http://cdn.rawgit.com/niklasvh/html2canvas/0.5.0-alpha2/dist/html2canvas.min.js"></script>
        <script type="text/javascript" src="bower_components/excellentexport/excellentexport.min.js"></script>
        <!-- REACTJS -->
        <script src="assets/global/plugins/react.min.js"></script>
        <script src="assets/global/plugins/react-dom.min.js"></script>
        <script src="assets/global/plugins/prop-types/prop-types.min.js"></script>
        <script src="assets/global/plugins/classnames/classnames.min.js"></script>
        <script src="assets/global/scripts/react-components.js" type="text/javascript"></script>
        <script src="js/charts_react.js?4321542315" type="text/javascript"></script>
        <script src="componentes/tags.js?1300669523" type="text/javascript"></script>
        <!-- REACTJS -->
        <!-- END THEME LAYOUT SCRIPTS -->
        <!-- MULTIPLE SELECT -->
        <script src="assets/global/plugins/bootstrap-select.min.js"></script>
        <script src="assets/global/plugins/sweetalert.min.js"></script>
        
        <?=$vistas['vista_js']?>
        <script>
            <?=$vistas['vista_file_js'];?>
        </script>

    </body>

</html>