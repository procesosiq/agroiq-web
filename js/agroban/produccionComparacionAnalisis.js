/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./js_modules/agroban/produccionComparacionAnalisis.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./js_modules/agroban/produccionComparacionAnalisis.js":
/*!*************************************************************!*\
  !*** ./js_modules/agroban/produccionComparacionAnalisis.js ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nvar responseHectareas = {\n    \"data\": [{\n        \"name\": \"RACIMOS CORTADOS\",\n        \"laniado\": \"2052.96\",\n        \"marun\": \"1849.12\",\n        \"reiset\": \"2322.32\",\n        \"quintana\": \"2412.80\",\n        \"agroban\": 2159.3\n    }, {\n        \"name\": \"RACIMOS PROCESADOS\",\n        \"laniado\": \"2046.20\",\n        \"marun\": \"1824.68\",\n        \"reiset\": \"2273.96\",\n        \"quintana\": \"2390.96\",\n        \"agroban\": 2133.95\n    }, {\n        \"name\": \"RACIMOS RECUSADOS\",\n        \"laniado\": \"6.76\",\n        \"marun\": \"25.48\",\n        \"reiset\": \"48.36\",\n        \"quintana\": \"43.68\",\n        \"agroban\": 31.07\n    }, {\n        \"name\": \"CAJAS CONV\",\n        \"laniado\": \"3623.88\",\n        \"marun\": \"3254.68\",\n        \"reiset\": \"3756.48\",\n        \"quintana\": \"4456.92\",\n        \"agroban\": 3772.99\n    }, {\n        \"name\": \"ENFUNDE\",\n        \"laniado\": \"2297.36\",\n        \"marun\": null,\n        \"reiset\": \"2551.64\",\n        \"quintana\": null,\n        \"agroban\": 2424.5\n    }]\n};\n\nvar responseSemanal = {\n    \"semanas\": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28],\n    \"data\": [{\n        \"name\": \"PESO RACIMO PROM LB\",\n        \"laniado\": \"83.04\",\n        \"marun\": \"83.20\",\n        \"reiset\": \"79.69\",\n        \"quintana\": \"91.43\",\n        \"agroban\": 84.34\n    }, {\n        \"name\": \"EDAD PROM\",\n        \"laniado\": \"12.99\",\n        \"marun\": \"11.07\",\n        \"reiset\": \"11.80\",\n        \"quintana\": \"12.48\",\n        \"agroban\": 12.09\n    }, {\n        \"name\": \"% TALLO\",\n        \"laniado\": \"9.86\",\n        \"marun\": \"7.64\",\n        \"reiset\": \"9.37\",\n        \"quintana\": \"9.67\",\n        \"agroban\": 9.14\n    }, {\n        \"name\": \"% RECUSADOS\",\n        \"laniado\": \"0.31\",\n        \"marun\": \"1.21\",\n        \"reiset\": \"2.07\",\n        \"quintana\": \"0.92\",\n        \"agroban\": 1.13\n    }, {\n        \"name\": \"CALIBRE PROM\",\n        \"laniado\": \"44.88\",\n        \"marun\": \"44.93\",\n        \"reiset\": \"44.54\",\n        \"quintana\": \"44.79\",\n        \"agroban\": 44.79\n    }, {\n        \"name\": \"MANOS PROM\",\n        \"laniado\": \"8.37\",\n        \"marun\": \"5.62\",\n        \"reiset\": \"9.53\",\n        \"quintana\": \"9.62\",\n        \"agroban\": 8.29\n    }, {\n        \"name\": \"% MUESTREO\",\n        \"laniado\": \"9.90\",\n        \"marun\": \"32.50\",\n        \"reiset\": \"38.00\",\n        \"quintana\": \"100.00\",\n        \"agroban\": 45.1\n    }, {\n        \"name\": \"RATIO CORTADO\",\n        \"laniado\": \"1.76\",\n        \"marun\": \"1.94\",\n        \"reiset\": \"1.56\",\n        \"quintana\": \"1.84\",\n        \"agroban\": 1.78\n    }, {\n        \"name\": \"RATIO PROCESADO\",\n        \"laniado\": \"1.77\",\n        \"marun\": \"1.96\",\n        \"reiset\": \"1.59\",\n        \"quintana\": \"1.86\",\n        \"agroban\": 1.8\n    }, {\n        \"name\": \"% MERMA CORTADA\",\n        \"laniado\": \"15.62\",\n        \"marun\": \"19.46\",\n        \"reiset\": \"17.03\",\n        \"quintana\": \"19.32\",\n        \"agroban\": 17.86\n    }, {\n        \"name\": \"% MERMA PROCESADA\",\n        \"laniado\": 5.76,\n        \"marun\": 11.82,\n        \"reiset\": 7.66,\n        \"quintana\": 9.65,\n        \"agroban\": 8.72\n    }, {\n        \"name\": \"% MERMA NETA\",\n        \"laniado\": \"1.16\",\n        \"marun\": \"3.31\",\n        \"reiset\": \"2.76\",\n        \"quintana\": \"2.40\",\n        \"agroban\": 2.41\n    }, {\n        \"name\": \"RECOBRO\",\n        \"laniado\": \"97.01\",\n        \"marun\": \"\",\n        \"reiset\": \"95.35\",\n        \"quintana\": \"\",\n        \"agroban\": 96.18\n    }]\n};\n\nvar responseHectareasSem = {\n    \"data\": [{\n        \"name\": \"RACIMOS CORTADOS\",\n        \"laniado\": \"39.48\",\n        \"marun\": \"35.56\",\n        \"reiset\": \"44.66\",\n        \"quintana\": \"46.40\",\n        \"agroban\": 41.53\n    }, {\n        \"name\": \"RACIMOS PROCESADOS\",\n        \"laniado\": \"39.35\",\n        \"marun\": \"35.09\",\n        \"reiset\": \"43.73\",\n        \"quintana\": \"45.98\",\n        \"agroban\": 41.04\n    }, {\n        \"name\": \"RACIMOS RECUSADOS\",\n        \"laniado\": \"0.13\",\n        \"marun\": \"0.49\",\n        \"reiset\": \"0.93\",\n        \"quintana\": \"0.84\",\n        \"agroban\": 0.6\n    }, {\n        \"name\": \"CAJAS CONV\",\n        \"laniado\": \"69.69\",\n        \"marun\": \"62.59\",\n        \"reiset\": \"72.24\",\n        \"quintana\": \"85.71\",\n        \"agroban\": 72.56\n    }, {\n        \"name\": \"ENFUNDE\",\n        \"laniado\": \"44.18\",\n        \"marun\": null,\n        \"reiset\": \"49.07\",\n        \"quintana\": null,\n        \"agroban\": 46.63\n    }]\n};\n\napp.service('request', ['$http', function ($http) {\n\n    var service = {};\n\n    service.porHectareas = function (callback, params) {\n        if (params.por_hectareas == 'HA/ANIO') callback(responseHectareas);else if (params.por_hectareas == 'HA/SEM') callback(responseHectareasSem);else {\n            load.block('porHectareas');\n            $http.post('phrapi/comparacion/hectareas', params || {}).then(function (r) {\n                load.unblock('porHectareas');\n                callback(r.data);\n            });\n        }\n    };\n\n    service.semanal = function (callback, params) {\n        if (params.por_semana == '') callback(responseSemanal);else {\n            load.block('semanal');\n            $http.post('phrapi/comparacion/semanal', params || {}).then(function (r) {\n                load.unblock('semanal');\n                callback(r.data);\n            });\n        }\n    };\n\n    return service;\n}]);\n\nfunction getOptionsGraficaReact(id, options, umbral) {\n    var newOptions = {\n        series: options.series,\n        legend: options.legends,\n        umbral: umbral,\n        id: id,\n        type: 'bar',\n        actions: false,\n        zoom: false\n    };\n    return newOptions;\n}\n\nfunction initGrafica(id, options, umbral) {\n    setTimeout(function () {\n        var parent = $(\"#\" + id).parent();\n        $(\"#\" + id).remove();\n        parent.append(\"<div id=\\\"\" + id + \"\\\" style=\\\"height:400px\\\"></div>\");\n        var data = getOptionsGraficaReact(id, options, umbral);\n        ReactDOM.render(React.createElement(Historica, data), document.getElementById(id));\n    }, 250);\n}\n\nvar invertidos = ['RACIMOS RECUSADOS', 'EDAD PROM', '% RECUSADOS', '% MERMA CORTADA', '% MERMA PROCESADA'];\n\napp.controller('produccion', ['$scope', 'request', function ($scope, $request) {\n\n    $scope.getUmbral = function (umbral, value, name) {\n        var invertido = true;\n        if (invertidos.indexOf(name) > -1) invertido = false;\n        if (!invertido) {\n            if (value > 0 && umbral > 0) {\n                if (value <= umbral) return 'border-bottom-green';else return 'border-bottom-red';\n            }\n        } else {\n            if (value > 0 && umbral > 0) {\n                if (value >= umbral) return 'border-bottom-green';else return 'border-bottom-red';\n            }\n        }\n        return '';\n    };\n\n    $scope.getUmbralFont = function (umbral, value, name) {\n        var invertido = true;\n        if (invertidos.indexOf(name) > -1) invertido = false;\n        if (!invertido) {\n            if (value > 0 && umbral > 0) {\n                if (value <= umbral) return 'text-green';else return 'text-red';\n            }\n        } else {\n            if (value > 0 && umbral > 0) {\n                if (value >= umbral) return 'text-green';else return 'text-red';\n            }\n        }\n        return '';\n    };\n\n    $scope.filters = {\n        por_hectareas: 'HA/ANIO',\n        por_semana: '',\n        chart_por_hectareas: 'RACIMOS CORTADOS',\n        chart_semanal: 'PESO RACIMO PROM LB'\n    };\n    $scope.por_hectareas = [];\n    $scope.init = function () {\n        $scope.getPorHectareas();\n        $scope.getSemanal();\n    };\n\n    $scope.getSemanal = function () {\n        $request.semanal(printSemanal, $scope.filters);\n    };\n    $scope.getPorHectareas = function () {\n        $request.porHectareas(printPorHectareas, $scope.filters);\n    };\n    $scope.chartPorHectareas = function () {\n        var data = {};\n        $scope.por_hectareas.map(function (s) {\n            if (s.name == $scope.filters.chart_por_hectareas) data = s;\n        });\n        var series = {\n            QUINTANA: {\n                name: 'LOS RIOS NORTE',\n                data: [data.quintana],\n                connectNulls: true,\n                type: \"bar\",\n                itemStyle: {\n                    normal: {\n                        barBorderRadius: 0,\n                        barBorderWidth: 6\n                    }\n                },\n                label: {\n                    normal: {\n                        show: true,\n                        position: \"inside\"\n                    }\n                }\n            },\n            LANIADO: {\n                name: 'EL ORO',\n                data: [data.laniado],\n                connectNulls: true,\n                type: \"bar\",\n                itemStyle: {\n                    normal: {\n                        barBorderRadius: 0,\n                        barBorderWidth: 6\n                    }\n                },\n                label: {\n                    normal: {\n                        show: true,\n                        position: \"inside\"\n                    }\n                }\n            },\n            MARUN: {\n                name: 'GUAYAS',\n                data: [data.marun],\n                connectNulls: true,\n                type: \"bar\",\n                itemStyle: {\n                    normal: {\n                        barBorderRadius: 0,\n                        barBorderWidth: 6\n                    }\n                },\n                label: {\n                    normal: {\n                        show: true,\n                        position: \"inside\"\n                    }\n                }\n            },\n            REISET: {\n                name: 'LOS RIOS SUR',\n                data: [data.reiset],\n                connectNulls: true,\n                type: \"bar\",\n                itemStyle: {\n                    normal: {\n                        barBorderRadius: 0,\n                        barBorderWidth: 6\n                    }\n                },\n                label: {\n                    normal: {\n                        show: true,\n                        position: \"inside\"\n                    }\n                }\n            }\n        },\n            legends = [''];\n        initGrafica('chart-porHeactareas', { series: series, legends: legends }, data.agroban);\n    };\n    $scope.chartSemanal = function () {\n        var data = {};\n        $scope.semanal.map(function (s) {\n            if (s.name == $scope.filters.chart_semanal) data = s;\n        });\n        var series = {\n            QUINTANA: {\n                name: 'LOS RIOS NORTE',\n                data: [data.quintana],\n                connectNulls: true,\n                type: \"bar\",\n                itemStyle: {\n                    normal: {\n                        barBorderRadius: 0,\n                        barBorderWidth: 6\n                    }\n                },\n                label: {\n                    normal: {\n                        show: true,\n                        position: \"inside\"\n                    }\n                }\n            },\n            LANIADO: {\n                name: 'EL ORO',\n                data: [data.laniado],\n                connectNulls: true,\n                type: \"bar\",\n                itemStyle: {\n                    normal: {\n                        barBorderRadius: 0,\n                        barBorderWidth: 6\n                    }\n                },\n                label: {\n                    normal: {\n                        show: true,\n                        position: \"inside\"\n                    }\n                }\n            },\n            MARUN: {\n                name: 'GUAYAS',\n                data: [data.marun],\n                connectNulls: true,\n                type: \"bar\",\n                itemStyle: {\n                    normal: {\n                        barBorderRadius: 0,\n                        barBorderWidth: 6\n                    }\n                },\n                label: {\n                    normal: {\n                        show: true,\n                        position: \"inside\"\n                    }\n                }\n            },\n            REISET: {\n                name: 'LOS RIOS SUR',\n                data: [data.reiset],\n                connectNulls: true,\n                type: \"bar\",\n                itemStyle: {\n                    normal: {\n                        barBorderRadius: 0,\n                        barBorderWidth: 6\n                    }\n                },\n                label: {\n                    normal: {\n                        show: true,\n                        position: \"inside\"\n                    }\n                }\n            }\n        },\n            legends = [''];\n        initGrafica('chart-semanal', { series: series, legends: legends }, data.agroban);\n    };\n\n    printPorHectareas = function printPorHectareas(r) {\n        $scope.por_hectareas = r.data;\n        $scope.chartPorHectareas();\n    };\n\n    printSemanal = function printSemanal(r) {\n        $scope.semanal = r.data;\n        $scope.semanas = r.semanas;\n        $scope.chartSemanal();\n    };\n\n    $scope.init();\n}]);\n\n//# sourceURL=webpack:///./js_modules/agroban/produccionComparacionAnalisis.js?");

/***/ })

/******/ });