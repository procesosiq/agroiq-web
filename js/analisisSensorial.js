/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./js_modules/marun/analisisSensorial.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./js_modules/marun/analisisSensorial.js":
/*!***********************************************!*\
  !*** ./js_modules/marun/analisisSensorial.js ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\napp.service('request', ['$http', function ($http) {\n    this.getFilters = function (callback, params) {\n        load.block('filters');\n        $http.post('phrapi/marun/cacao/filters', params || {}).then(function (r) {\n            load.unblock('filters');\n            if (callback) callback(r.data);\n        });\n    };\n    this.getLast = function (callback, params) {\n        $http.post('phrapi/marun/cacao/last', params || {}).then(function (r) {\n            if (callback) callback(r.data);\n        });\n    };\n    this.getDataAnalisis = function (callback, params) {\n        $http.post('phrapi/marun/cacao/analisis', params || {}).then(function (r) {\n            if (callback) callback(r.data);\n        });\n    };\n}]);\n\napp.filter('sumOfValue', function () {\n    return function (data, key) {\n        if (angular.isUndefined(data) || angular.isUndefined(key)) return 0;\n        var sum = 0;\n        angular.forEach(data, function (value) {\n            if (value[key] != \"\" && value[key] != undefined && parseFloat(value[key])) {\n                sum = sum + parseFloat(value[key], 10);\n            }\n        });\n        return sum;\n    };\n});\n\napp.controller('controller', ['$scope', 'request', function ($scope, $request) {\n\n    $scope.filters = {\n        codigo: '',\n        finca: 'San José',\n        fecha: ''\n    };\n    getOptions = function getOptions(data) {\n        var option = {\n            series: [{\n                name: 'Sensores',\n                value: []\n            }],\n            legend: [],\n            id: 'grafica-radar'\n        };\n        data.map(function (item) {\n            option.legend.push({\n                name: item.name,\n                max: item.max || null\n            });\n            option.series[0].value.push(item.value);\n        });\n        return option;\n    };\n\n    renderGraficaRadar = function renderGraficaRadar(options) {\n        ReactDOM.render(React.createElement(Radar, options), document.getElementById('grafica-radar'));\n    };\n\n    $scope.reloadFilters = function () {\n        $request.getFilters(function (r) {\n            $scope.responsables = r.responsables;\n            if (r.responsables.indexOf($scope.filters.responsable) == -1) $scope.filters.responsable = '';\n            $scope.fechas = r.fechas;\n            if (r.fechas.indexOf($scope.filters.fecha) == -1) $scope.filters.fecha = '';\n            $scope.codigos = r.codigos;\n            if (r.codigos.indexOf($scope.filters.codigo) == -1) $scope.filters.codigo = '';\n            $scope.fincas = r.fincas;\n            if (r.fincas.indexOf($scope.filters.finca) == -1) $scope.filters.finca = '';\n\n            //$scope.getDataAnalisis()\n        }, $scope.filters);\n    };\n\n    $scope.getDataAnalisis = function () {\n        if ($scope.filters.codigo != '') {\n            $request.getDataAnalisis(function (r) {\n                $scope.data = r.data;\n                $scope.filters.responsable = r.row.responsable;\n                $scope.filters.fecha = r.row.fecha;\n                $scope.reloadFilters();\n                $scope.imagenes = r.imagenes || [];\n                $scope.observaciones = r.row.observaciones || '';\n                renderGraficaRadar(getOptions(r.data));\n            }, $scope.filters);\n        }\n    };\n\n    $scope.init = function () {\n        $request.getLast(function (r) {\n            $scope.filters.codigo = r.row.codigo;\n            $scope.filters.fecha = r.row.fecha;\n            $scope.filters.responsable = r.row.responsable;\n            $scope.data = r.data;\n            $scope.imagenes = r.row.imagenes || [];\n            $scope.observaciones = r.row.observaciones || '';\n            renderGraficaRadar(getOptions(r.data));\n            $scope.reloadFilters();\n        });\n    };\n    $scope.init();\n}]);\n\n//# sourceURL=webpack:///./js_modules/marun/analisisSensorial.js?");

/***/ })

/******/ });