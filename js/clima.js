/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./js_modules/quintana/clima.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./js_modules/quintana/clima.js":
/*!**************************************!*\
  !*** ./js_modules/quintana/clima.js ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\napp.service('request', function ($http) {\n    var service = {};\n\n    service.init = function (params) {\n        return $http.post('phrapi/quintana/clima/index', params);\n    };\n\n    service.graficaHoras = function (params) {\n        return $http.post('phrapi/quintana/clima/horas', params);\n    };\n\n    return service;\n});\n\napp.filter('orderObjectBy', function () {\n    return function (items, field, reverse) {\n        var filtered = [];\n        angular.forEach(items, function (item) {\n            filtered.push(item);\n        });\n        filtered.sort(function (a, b) {\n            if (parseFloat(a[field]) && parseFloat(b[field])) {\n                return parseFloat(a[field]) > parseFloat(b[field]) ? 1 : -1;\n            } else {\n                return a[field] > b[field] ? 1 : -1;\n            }\n        });\n        if (reverse) filtered.reverse();\n        return filtered;\n    };\n});\n\napp.filter('avg', function () {\n    return function (items) {\n        var sum = 0,\n            count = 0;\n        angular.forEach(items, function (value) {\n            if (parseFloat(value)) {\n                count++;\n                sum += parseFloat(value);\n            }\n        });\n\n        return (sum / count).toFixed(2);\n    };\n});\n\napp.filter('sum', function () {\n    return function (items) {\n        var sum = 0;\n        angular.forEach(items, function (value) {\n            if (parseFloat(value)) {\n                sum += parseFloat(value);\n            }\n        });\n\n        return sum.toFixed(2);\n    };\n});\n\napp.filter('min', function () {\n    return function (items) {\n        var min = null;\n        angular.forEach(items, function (value) {\n            if (parseFloat(value) && min == null || parseFloat(value) && parseFloat(value) < min) {\n                min = parseFloat(value);\n            }\n        });\n\n        return min;\n    };\n});\n\napp.filter('max', function () {\n    return function (items) {\n        var max = null;\n        angular.forEach(items, function (value) {\n            if (parseFloat(value) && max == null || parseFloat(value) && parseFloat(value) > max) {\n                max = parseFloat(value);\n            }\n        });\n\n        return max;\n    };\n});\n\napp.controller('controller', ['$scope', 'request', function ($scope, $request) {\n    $scope.filters = {\n        year: moment().format(\"YYYY\"),\n        luz: 400,\n        fecha_inicial: moment().subtract(7, 'day').format('YYYY-MM-DD'),\n        fecha_final: moment().format('YYYY-MM-DD')\n    };\n\n    $scope.StartEndDateDirectives = {\n        startDate: moment().subtract(7, 'day'),\n        endDate: moment()\n    };\n\n    $scope.changeRangeDate = function (data) {\n        if (data) {\n            $scope.filters.fecha_inicial = data.hasOwnProperty(\"first_date\") ? data.first_date : $scope.filters.fecha_inicial;\n            $scope.filters.fecha_final = data.hasOwnProperty(\"second_date\") ? data.second_date : $scope.filters.fecha_final;\n        }\n        $scope.chageGraficaHorasDias();\n    };\n\n    $scope.years = [];\n    $scope.gerentes = [];\n    var tables = [];\n\n    $scope.grafica_temp_min = [];\n    $scope.grafica_temp_max = [];\n    $scope.grafica_lluvia = [];\n    $scope.grafica_humedad = [];\n    $scope.grafica_rad_solar = [];\n    $scope.grafica_horas_luz = {};\n\n    initTable = function initTable(r, index) {\n        var props = {\n            header: [{\n                key: 'detalle',\n                name: 'DETALLE',\n                titleClass: 'text-center',\n                locked: true,\n                expandable: true,\n                resizable: true,\n                width: 170\n            }, {\n                key: 'total',\n                name: 'TOTAL',\n                locked: true,\n                sortable: true,\n                titleClass: 'text-center',\n                alignContent: 'right',\n                filterable: true,\n                filterRenderer: 'NumericFilter',\n                formatter: 'Number',\n                resizable: true\n            }, {\n                key: 'avg',\n                name: 'AVG',\n                locked: true,\n                sortable: true,\n                titleClass: 'text-center',\n                alignContent: 'right',\n                filterable: true,\n                filterRenderer: 'NumericFilter',\n                formatter: 'Number',\n                resizable: true\n            }, {\n                key: 'max',\n                name: 'MAX',\n                locked: true,\n                titleClass: 'text-center',\n                sortable: true,\n                alignContent: 'right',\n                filterable: true,\n                filterRenderer: 'NumericFilter',\n                resizable: true\n            }, {\n                key: 'min',\n                name: 'MIN',\n                locked: true,\n                sortable: true,\n                titleClass: 'text-center',\n                alignContent: 'right',\n                filterable: true,\n                filterRenderer: 'NumericFilter',\n                resizable: true\n            }],\n            data: r,\n            buttons: [{\n                title: 'Excel',\n                action: function action() {\n                    tables[index].exportToExcel();\n                },\n                className: ''\n            }]\n        };\n        $scope.semanas.map(function (sem) {\n            props.header.push({\n                key: 'sem_' + sem,\n                name: 'SEM ' + sem,\n                sortable: true,\n                alignContent: 'right',\n                titleClass: 'text-center',\n                filterable: true,\n                filterRenderer: 'NumericFilter',\n                resizable: true\n            });\n        });\n        tables[index] = ReactDOM.render(React.createElement(ReactDataGrid, props), document.getElementById(\"table_\" + index));\n    };\n\n    initTables = function initTables() {\n        Object.keys($scope.tablas).map(function (key, index) {\n            var data = [$scope.tablas[key].temp_min, $scope.tablas[key].temp_max, $scope.tablas[key].lluvia, $scope.tablas[key].rad_solar, $scope.tablas[key].horas_luz_100, $scope.tablas[key].horas_luz_150, $scope.tablas[key].horas_luz_200, $scope.tablas[key].horas_luz_400, $scope.tablas[key].humedad];\n            initTable(data, index);\n        });\n    };\n\n    $scope.success = function (r) {\n        var r = r.data;\n\n        if (r.hasOwnProperty(\"semanas\")) {\n            $scope.semanas = r.semanas;\n        }\n        if (r.hasOwnProperty(\"years\")) {\n            $scope.years = r.years;\n        }\n        if (r.hasOwnProperty(\"gerentes\")) {\n            $scope.gerentes = r.gerentes;\n        }\n        if (r.hasOwnProperty(\"tablas\")) {\n            $scope.tablas = r.tablas;\n        }\n        if (r.hasOwnProperty(\"grafica_temp_min\")) {\n            var data = {\n                series: r.grafica_temp_min.data,\n                legend: r.grafica_temp_min.legend,\n                umbral: null,\n                id: \"grafica_temp_min\",\n                type: 'line',\n                zoom: false,\n                legendBottom: true,\n                min: r.grafica_temp_min.min\n            };\n            ReactDOM.render(React.createElement(Historica, data), document.getElementById('grafica_temp_min'));\n        }\n        if (r.hasOwnProperty(\"grafica_temp_max\")) {\n            var data = {\n                series: r.grafica_temp_max.data,\n                legend: r.grafica_temp_max.legend,\n                umbral: null,\n                id: \"grafica_temp_max\",\n                type: 'line',\n                zoom: false,\n                legendBottom: true,\n                min: r.grafica_temp_max.min\n            };\n            ReactDOM.render(React.createElement(Historica, data), document.getElementById('grafica_temp_max'));\n        }\n        if (r.hasOwnProperty(\"grafica_lluvia\")) {\n            var data = {\n                series: r.grafica_lluvia.data,\n                legend: r.grafica_lluvia.legend,\n                umbral: null,\n                id: \"grafica_lluvia\",\n                type: 'line',\n                zoom: false,\n                legendBottom: true\n            };\n            ReactDOM.render(React.createElement(Historica, data), document.getElementById('grafica_lluvia'));\n        }\n        if (r.hasOwnProperty(\"grafica_humedad\")) {\n            var data = {\n                series: r.grafica_humedad.data,\n                legend: r.grafica_humedad.legend,\n                umbral: null,\n                id: \"grafica_humedad\",\n                type: 'line',\n                zoom: false,\n                legendBottom: true,\n                min: r.grafica_humedad.min\n            };\n            ReactDOM.render(React.createElement(Historica, data), document.getElementById('grafica_humedad'));\n        }\n        if (r.hasOwnProperty(\"grafica_rad_solar\")) {\n            var data = {\n                series: r.grafica_rad_solar.data,\n                legend: r.grafica_rad_solar.legend,\n                umbral: null,\n                id: \"grafica_rad_solar\",\n                type: 'line',\n                zoom: false,\n                legendBottom: true,\n                min: r.grafica_rad_solar.min\n            };\n            ReactDOM.render(React.createElement(Historica, data), document.getElementById('grafica_rad_solar'));\n        }\n        if (r.hasOwnProperty(\"grafica_horas_luz\")) {\n            $scope.grafica_horas_luz = r.grafica_horas_luz;\n            var data = {\n                series: r.grafica_horas_luz[$scope.filters.luz].data,\n                legend: r.grafica_horas_luz[$scope.filters.luz].legend,\n                umbral: null,\n                id: \"grafica_horas_luz\",\n                type: 'line',\n                zoom: false,\n                legendBottom: true,\n                min: r.grafica_horas_luz[$scope.filters.luz].min\n            };\n            ReactDOM.render(React.createElement(Historica, data), document.getElementById('grafica_horas_luz'));\n        }\n\n        setTimeout(function () {\n            initTables();\n        }, 200);\n    };\n\n    $scope.changeLuz = function () {\n        var data = {\n            series: $scope.grafica_horas_luz[$scope.filters.luz].data,\n            legend: $scope.grafica_horas_luz[$scope.filters.luz].legend,\n            umbral: null,\n            id: \"grafica_horas_luz\",\n            type: 'line',\n            zoom: false,\n            legendBottom: true,\n            min: $scope.grafica_horas_luz[$scope.filters.luz].min\n        };\n        ReactDOM.render(React.createElement(Historica, data), document.getElementById('grafica_horas_luz'));\n    };\n\n    $scope.chageGraficaHorasDias = function () {\n        var data = $scope.filters;\n        load.block('grafica_horas_dia');\n        $request.graficaHoras(data).then(function (r) {\n            load.unblock('grafica_horas_dia');\n            var r = r.data;\n\n            if (r.hasOwnProperty(\"grafica_horas_dia\")) {\n                var data = {\n                    series: r.grafica_horas_dia.data,\n                    legend: r.grafica_horas_dia.legend,\n                    umbral: null,\n                    id: \"grafica_horas_dia\",\n                    type: 'line',\n                    zoom: false,\n                    legendBottom: true,\n                    min: r.grafica_horas_dia.min\n                };\n                var parent = $(\"#grafica_horas_dia\").parent();\n                parent.empty();\n                parent.append('<div id=\"grafica_horas_dia\" class=\"charts\"></div>');\n\n                ReactDOM.render(React.createElement(Historica, data), document.getElementById('grafica_horas_dia'));\n            }\n        }).catch($scope.error);\n    };\n\n    $scope.setFilterGerente = function (data) {\n        if (data) {\n            $scope.filters.gerente = data.id;\n            $scope.filters.gerenteName = data.label;\n            $scope.init($scope.filters);\n        }\n    };\n    $scope.setFilterYears = function (year) {\n        if (year) {\n            $scope.filters.year = year;\n            $scope.init($scope.filters);\n        }\n    };\n\n    $scope.error = function (err) {\n        console.error(err);\n    };\n\n    $scope.init = function () {\n        var data = $scope.filters;\n        $request.init(data).then($scope.success).catch($scope.error);\n\n        $scope.chageGraficaHorasDias();\n    };\n\n    $scope.exportExcel = function (id_table, title) {\n        var tableToExcel = function () {\n            var uri = 'data:application/vnd.ms-excel;base64,',\n                template = '<html xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:excel\" xmlns=\"http://www.w3.org/TR/REC-html40\"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv=\"content-type\" content=\"text/plain; charset=UTF-8\"/></head><body><table>{table}</table></body></html>',\n                base64 = function base64(s) {\n                return window.btoa(unescape(encodeURIComponent(s)));\n            },\n                format = function format(s, c) {\n                return s.replace(/{(\\w+)}/g, function (m, p) {\n                    return c[p];\n                });\n            };\n            return function (table, name) {\n                if (!table.nodeType) table = document.getElementById(table);\n                var contentTable = table.innerHTML;\n                // remove filters\n                /*var cut = contentTable.search('<tr role=\"row\" class=\"filter\">')\n                var cut2 = contentTable.search('</thead>')\n                 var part1 = contentTable.substring(0, cut)\n                var part2 = contentTable.substring(cut2, contentTable.length)\n                contentTable = part1 + part2*/\n\n                var ctx = { worksheet: name || 'Worksheet', table: contentTable };\n                window.location.href = uri + base64(format(template, ctx));\n            };\n        }();\n        tableToExcel(id_table, title || \"\");\n    };\n\n    $scope.exportPrint = function (id_table) {\n        var image = '<div style=\"display:block; height:55;\"><img style=\"float: right;\" width=\"100\" height=\"50\" src=\"./../logos/Logo.png\" /></div><br>';\n        var table = document.getElementById(id_table);\n        var contentTable = table.outerHTML;\n        // remove filters\n        /*var cut = contentTable.search('<tr role=\"row\" class=\"filter\">')\n        var cut2 = contentTable.search('</thead>')\n        var part1 = contentTable.substring(0, cut)\n        var part2 = contentTable.substring(cut2, contentTable.length)\n        // add image\n        contentTable = image + part1 + part2*/\n\n        var newWin = window.open(\"\");\n        newWin.document.write(contentTable);\n        newWin.print();\n        newWin.close();\n    };\n}]);\n\n//# sourceURL=webpack:///./js_modules/quintana/clima.js?");

/***/ })

/******/ });