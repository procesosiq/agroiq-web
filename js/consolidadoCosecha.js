/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./js_modules/marun/consolidadoCosecha.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./js_modules/marun/consolidadoCosecha.js":
/*!************************************************!*\
  !*** ./js_modules/marun/consolidadoCosecha.js ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\napp.service('request', ['$http', function ($http) {\n    this.loadData = function (callback, params) {\n        load.block();\n        $http.post('phrapi/marun/consolidadoCosecha/data', params || {}).then(function (r) {\n            load.unblock();\n            if (callback) callback(r.data);\n        });\n    };\n\n    this.save = function (callback, params) {\n        $http.post('phrapi/marun/consolidadoCosecha/save', params || {}).then(function (r) {\n            if (callback) callback(r.data);\n        });\n    };\n}]);\n\napp.controller('controller', ['$scope', 'request', function ($scope, $request) {\n\n    $scope.changeRangeDate = function () {\n        var data = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};\n\n        if (data) {\n            $scope.filters.fecha_inicial = data.first_date;\n            $scope.filters.fecha_final = data.second_date;\n            $scope.init();\n        }\n    };\n\n    save = function save(row, campo, valor) {\n        if (id > 0) {\n            $request.save(function (r) {\n                if (r.status == 200) {\n                    alert(\"Se guardo correctamente\", \"\", \"success\");\n                } else {\n                    alert(\"Hubo un error favor de intentar mas tarde\");\n                }\n            }, {\n                fecha: $scope.filters.fecha,\n                sector: row.sector,\n                lote: row.lote,\n                codigo: row.codigo,\n\n                campo: campo,\n                valor: valor\n            });\n        }\n    };\n\n    renderTableData = function renderTableData(data) {\n        var props = {\n            header: [{\n                key: 'fecha',\n                name: 'FECHA',\n                titleClass: 'text-center',\n                sortable: true,\n                alignContent: 'center',\n                filterable: true,\n                resizable: true\n            }, {\n                key: 'cedula',\n                name: 'CEDULA',\n                titleClass: 'text-center',\n                sortable: true,\n                alignContent: 'center',\n                filterable: true,\n                resizable: true\n            }, {\n                key: 'cosechador',\n                name: 'COSECHADOR',\n                titleClass: 'text-center',\n                sortable: true,\n                alignContent: 'center',\n                filterable: true,\n                resizable: true,\n                width: 250,\n                customCell: function customCell(data) {\n                    var nombre = data.cosechador || '';\n                    return '\\n                            <div style=\"height: 100%;\" class=\"' + (data.cedula != 'TOTAL' && nombre == '' ? 'bg-red-thunderbird' : '') + '\">\\n                                ' + nombre + '\\n                            </div>\\n                        ';\n                }\n            }, {\n                key: 'sector',\n                name: 'SECTOR',\n                titleClass: 'text-center',\n                sortable: true,\n                alignContent: 'center',\n                filterable: true,\n                resizable: true,\n                customCell: function customCell(data) {\n                    var sector = data.cedula != 'N/A' ? data.sector : 'N/A';\n                    return '\\n                            <div style=\"height: 100%;\" class=\"text-center ' + (sector == '' ? 'bg-red-thunderbird bg-font-red-thunderbird' : '') + '\">\\n                                <span style=\"top: 25%; position: relative;\">' + sector + '</span>\\n                            </div>\\n                        ';\n                }\n            }, {\n                key: 'lote',\n                name: 'LOTE',\n                titleClass: 'text-center',\n                sortable: true,\n                alignContent: 'center',\n                filterable: true,\n                resizable: true,\n                customCell: function customCell(data) {\n                    var lote = data.cedula != 'N/A' ? data.lote : 'N/A';\n                    return '\\n                            <div style=\"height: 100%;\" class=\"text-center ' + (lote == '' ? 'bg-red-thunderbird bg-font-red-thunderbird' : '') + '\">\\n                                <span style=\"top: 25%; position: relative;\">' + lote + '</span>\\n                            </div>\\n                        ';\n                }\n            }, {\n                key: 'codigo',\n                name: 'CODIGO',\n                titleClass: 'text-center',\n                sortable: true,\n                alignContent: 'center',\n                filterable: true,\n                resizable: true\n            }, {\n                key: 'entregadas',\n                name: 'ENTREGADAS',\n                titleClass: 'text-center',\n                sortable: true,\n                alignContent: 'center',\n                filterable: true,\n                resizable: true\n            }, {\n                key: 'recibidas',\n                name: 'RECIBIDAS',\n                titleClass: 'text-center',\n                sortable: true,\n                alignContent: 'center',\n                filterable: true,\n                resizable: true,\n                customCell: function customCell(data) {\n                    var recibidas = data.recibidas || 0;\n                    var saldo = data.saldo || 0;\n                    return '\\n                            <div style=\"height: 100%;\" class=\"text-center ' + (!['TOTAL', 'N/A'].includes(data.cedula) && saldo < 0 ? 'bg-red-thunderbird bg-font-red-thunderbird' : '') + '\">\\n                                <span style=\"top: 25%; position: relative;\">' + recibidas + '</span>\\n                            </div>\\n                        ';\n                }\n            }, {\n                key: 'saldo',\n                name: 'SALDO',\n                titleClass: 'text-center',\n                sortable: true,\n                alignContent: 'center',\n                filterable: true,\n                resizable: true\n            }, {\n                key: 'peso',\n                name: 'PESO TOTAL',\n                titleClass: 'text-center',\n                sortable: true,\n                alignContent: 'center',\n                filterable: true,\n                resizable: true,\n                width: 100\n            }],\n            data: data,\n            buttons: [{\n                title: 'Excel',\n                action: function action() {\n                    table1.exportToExcel();\n                },\n                className: ''\n            }]\n        };\n        $(\"#table-data\").html(\"\");\n        table1 = ReactDOM.render(React.createElement(ReactDataGrid, props), document.getElementById('table-data'));\n    };\n\n    $scope.StartEndDateDirectives = {\n        startDate: moment(),\n        endDate: moment()\n    };\n    $scope.filters = {\n        fecha_inicial: moment().format('YYYY-MM-DD'),\n        fecha_final: moment().format('YYYY-MM-DD'),\n        unidad: 'LB'\n    };\n\n    $scope.init = function () {\n        $request.loadData(function (r) {\n            renderTableData(r.data);\n        }, $scope.filters);\n    };\n    $scope.init();\n}]);\n\n//# sourceURL=webpack:///./js_modules/marun/consolidadoCosecha.js?");

/***/ })

/******/ });