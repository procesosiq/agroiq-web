/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./js_modules/quintana/laboresAgricolas.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./js_modules/quintana/laboresAgricolas.js":
/*!*************************************************!*\
  !*** ./js_modules/quintana/laboresAgricolas.js ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\napp.service('request', ['$http', function ($http) {\n    var service = {};\n\n    service.tendencia = function (callback, params) {\n        load.block('grafica_tendecia');\n        $http.post('phrapi/quintana/laboresAgricolas/tendencia', params || {}).then(function (r) {\n            load.unblock('grafica_tendecia');\n            callback(r.data);\n        });\n    };\n\n    service.tablaHistorica = function (callback, params) {\n        load.block('tablaHistorica');\n        $http.post('phrapi/quintana/laboresAgricolas/tablaHistorica', params || {}).then(function (r) {\n            load.unblock('tablaHistorica');\n            callback(r.data);\n        });\n    };\n\n    service.graficaLabores = function (callback, params) {\n        $http.post('phrapi/quintana/laboresAgricolas/graficaLabores', params || {}).then(function (r) {\n            callback(r.data);\n        });\n    };\n\n    service.fotos = function (callback, params) {\n        $http.post('phrapi/quintana/laboresAgricolas/fotos', params || {}).then(function (r) {\n            callback(r.data);\n        });\n    };\n\n    return service;\n}]);\n\napp.filter('orderObjectBy', function () {\n    return function (items, field, reverse) {\n        var filtered = [];\n        angular.forEach(items, function (item) {\n            if (field == 'hora' || field == 'fecha') {\n                item.date = moment(item.fecha + ' ' + item.hora);\n            }\n            if (!isNaN(parseInt(item))) {\n                item = parseInt(item);\n            }\n            filtered.push(item);\n        });\n        filtered.sort(function (a, b) {\n            if (field == 'hora' || field == 'fecha') {\n                return moment(a.date).isAfter(b.date) ? 1 : -1;\n            } else if (parseFloat(a[field]) && parseFloat(b[field])) {\n                return parseFloat(a[field]) > parseFloat(b[field]) ? 1 : -1;\n            } else {\n                return a[field] > b[field] ? 1 : -1;\n            }\n        });\n        if (reverse) filtered.reverse();\n        return filtered;\n    };\n});\n\nfunction getOptionsGraficaReact(id, options) {\n    var newOptions = {\n        series: options.series,\n        legend: options.legends,\n        umbral: null,\n        id: id,\n        type: 'line',\n        min: 'dataMin',\n        max: null\n    };\n    return newOptions;\n}\n\nfunction initGrafica(id, options) {\n    setTimeout(function () {\n        var data = getOptionsGraficaReact(id, options);\n        ReactDOM.render(React.createElement(Historica, data), document.getElementById(id));\n    }, 250);\n}\n\nfunction initPastel(id, series) {\n    var legends = [];\n    var newSeries = [];\n    series.map(function (value) {\n        newSeries.push({\n            label: value.name,\n            value: parseFloat(value.value)\n        });\n        if (legends.indexOf(value.name) != -1) legends.push(value.name);\n    });\n    setTimeout(function () {\n        data = {\n            data: newSeries,\n            nameseries: \"Pastel\",\n            legend: legends,\n            titulo: \"\",\n            id: id\n        };\n        ReactDOM.render(React.createElement(Pastel, data), document.getElementById(id));\n    }, 250);\n}\n\napp.controller('controller', ['$scope', 'request', function ($scope, $request) {\n\n    $scope.filters = {};\n\n    $scope.checks = function (data, periodo) {\n        var porcentaje = parseFloat(data['PERIODO ' + periodo]);\n        if (!isNaN(porcentaje) && porcentaje >= 0) {\n            if (porcentaje > parseFloat($scope.umbrales.yellow_umbral_2)) return 'bg-green-haze bg-font-green-haze';else if (porcentaje < parseFloat($scope.umbrales.yellow_umbral_1)) return 'bg-red-thunderbird bg-font-red-thunderbird';else return 'bg-yellow-lemon bg-font-yellow-lemon';\n        }\n        return '';\n    };\n\n    renderTablaReact = function renderTablaReact(data, periodos) {\n        var props = {\n            header: [{\n                key: 'zona',\n                name: 'VARIABLE',\n                titleClass: 'text-center',\n                locked: true,\n                expandable: true,\n                resizable: true,\n                width: 300\n            }],\n            data: data,\n            buttons: [{\n                title: 'Excel',\n                action: function action() {\n                    table1.exportToExcel();\n                },\n                className: ''\n            }],\n            height: 450\n        };\n        periodos.map(function (p) {\n            var value = p.periodo;\n            props.header.push({\n                key: 'PERIODO ' + value,\n                name: '' + value,\n                sortable: true,\n                alignContent: 'right',\n                titleClass: 'text-center',\n                filterable: true,\n                filterRenderer: 'NumericFilter',\n                resizable: true,\n                customCell: function customCell(rowData) {\n                    var valNumber = parseFloat(rowData['PERIODO ' + value]);\n                    var checkUmbral = $scope.checks(rowData, value);\n                    if (rowData['tipo'] == 'CAUSA') checkUmbral = '';\n                    return '\\n                        <div class=\"text-center ' + checkUmbral + '\" style=\"height: 100%\">\\n                            ' + valNumber + ' %\\n                        </div>\\n                    ';\n                }\n            });\n        });\n        document.getElementById('tablaHistorica-react').innerHTML = \"\";\n        table1 = ReactDOM.render(React.createElement(ReactDataGrid, props), document.getElementById('tablaHistorica-react'));\n    };\n\n    responseGraficaTendencia = function responseGraficaTendencia(r) {\n        initGrafica('grafica_tendencia', r.week);\n    };\n    initGraficaTendencia = function initGraficaTendencia() {\n        $request.tendencia(responseGraficaTendencia, $scope.filters);\n    };\n\n    responseTablaHistorica = function responseTablaHistorica(r) {\n        $scope.umbrales = r.umbrals || {};\n        $scope.periodos = r.periodos || [];\n        $scope.tablaHistorica = r.data || [];\n\n        renderTablaReact(r.data, r.periodos);\n    };\n    initTablaHistorica = function initTablaHistorica() {\n        $request.tablaHistorica(responseTablaHistorica, $scope.filters);\n    };\n\n    initGraficaLabores = function initGraficaLabores() {\n        $request.graficaLabores(responseGraficaLabores, $scope.filters);\n    };\n    responseGraficaLabores = function responseGraficaLabores(r) {\n        initPastel('grafica_labores', r.main);\n        $scope.labores = r.labores;\n        $scope.filters.labor = Object.keys($scope.labores)[0];\n        $scope.changeLaborGrafica();\n    };\n\n    requestFotos = function requestFotos() {\n        $request.fotos(function (r) {\n            $scope.fotos = r.fotos;\n        }, $scope.filters);\n    };\n\n    $scope.changeLaborGrafica = function () {\n        initPastel('grafica_causas', $scope.labores[$scope.filters.labor]);\n    };\n\n    $scope.init = function () {\n        initGraficaTendencia();\n        initGraficaLabores();\n        requestFotos();\n        initTablaHistorica();\n    };\n\n    $scope.fnExcelReport = function (id_table, title) {\n        var data = new Blob([document.getElementById(id_table).outerHTML], {\n            type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf8'\n        });\n        saveAs(data, title + '.xls');\n    };\n\n    $scope.init();\n}]);\n\n//# sourceURL=webpack:///./js_modules/quintana/laboresAgricolas.js?");

/***/ })

/******/ });