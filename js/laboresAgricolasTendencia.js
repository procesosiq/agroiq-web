/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./js_modules/laboresAgricolasTendencia.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./js_modules/laboresAgricolasTendencia.js":
/*!*************************************************!*\
  !*** ./js_modules/laboresAgricolasTendencia.js ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\napp.service('request', ['$http', function ($http) {\n    var service = {};\n\n    service.last = function (callback, params) {\n        load.block();\n        $http.post('phrapi/laboresAgricolasTendencia/last', params || {}).then(function (r) {\n            load.unblock();\n            callback(r.data);\n        });\n    };\n\n    service.index = function (callback, params) {\n        load.block();\n        $http.post('phrapi/laboresAgricolasTendencia/index', params || {}).then(function (r) {\n            load.unblock();\n            callback(r.data);\n        });\n    };\n\n    service.variables = function (callback, params) {\n        load.block();\n        $http.post('phrapi/laboresAgricolasTendencia/variables', params || {}).then(function (r) {\n            load.unblock();\n            callback(r.data);\n        });\n    };\n\n    service.causasPeriodo = function (callback, params) {\n        load.block();\n        $http.post('phrapi/laboresAgricolasTendencia/causasPeriodo', params || {}).then(function (r) {\n            load.unblock();\n            callback(r.data);\n        });\n    };\n\n    service.calidadPeriodo = function (callback, params) {\n        load.block();\n        $http.post('phrapi/laboresAgricolasTendencia/calidadPeriodo', params || {}).then(function (r) {\n            load.unblock();\n            callback(r.data);\n        });\n    };\n\n    return service;\n}]);\n\napp.controller('controller', ['$scope', 'request', function ($scope, $request) {\n\n    $scope.filters = {\n        calidadPeriodo: {\n            mode: 'fincas'\n        },\n        causasPeriodo: {\n            finca: '',\n            labor: ''\n        }\n    };\n\n    $scope.last = function () {\n        $request.last(function (r) {\n            $scope.filters.anio = r.anio;\n            $request.variables(function (r) {\n                responseVariables(r);\n                $scope.index();\n            }, $scope.filters);\n        });\n    };\n\n    $scope.index = function () {\n        $request.index(function (r) {\n            responseCalidadPeriodo(r.calidad_periodo);\n            responseCausasPeriodo(r.causas_periodo);\n        }, $scope.filters);\n    };\n\n    $scope.calidadPeriodo = function () {\n        $request.calidadPeriodo(function (r) {\n            responseCalidadPeriodo(r);\n        }, $scope.filters);\n    };\n\n    $scope.changeFilterCausa = function () {\n        $request.variables(function (r) {\n            responseVariables(r);\n            $scope.causasPeriodo();\n        }, $scope.filters);\n    };\n\n    $scope.causasPeriodo = function () {\n        $request.causasPeriodo(function (r) {\n            responseCausasPeriodo(r);\n        }, $scope.filters);\n    };\n\n    var responseCalidadPeriodo = function responseCalidadPeriodo(r) {\n        $scope.calidad_periodo = r;\n        renderTableCalidadPeriodo(r.periodos, r.data);\n        renderChartCalidadPeriodo(r.periodos, r.data);\n    };\n\n    var responseVariables = function responseVariables(r) {\n        $scope.labores = angular.copy(r.labores);\n        $scope.fincas = angular.copy(r.fincas);\n\n        var e = r.labores.filter(function (l) {\n            return l.idLabor == $scope.filters.causasPeriodo.labor;\n        });\n        if (!e.length > 0) {\n            $scope.filters.causasPeriodo.labor = r.labores[0].idLabor;\n        }\n\n        e = r.fincas.filter(function (l) {\n            return l.idFinca == $scope.filters.causasPeriodo.finca;\n        });\n        if (!e.length > 0) {\n            $scope.filters.causasPeriodo.finca = r.fincas[0].idFinca;\n        }\n    };\n\n    var responseCausasPeriodo = function responseCausasPeriodo(r) {\n        renderTableCausasPeriodo(r.periodos, r.data);\n        renderChartCausasPeriodo(r.periodos, r.data);\n    };\n\n    var renderTableCalidadPeriodo = function renderTableCalidadPeriodo(periodos, data) {\n        var props = {\n            header: [{\n                key: 'finca',\n                name: 'FINCA',\n                titleClass: 'text-center',\n                alignContent: 'left',\n                locked: true,\n                expandable: true,\n                resizable: true\n            }, {\n                key: 'avg',\n                name: 'PROM',\n                locked: true,\n                sortable: true,\n                titleClass: 'text-center',\n                alignContent: 'right',\n                filterable: true,\n                filterRenderer: 'NumericFilter',\n                formatter: 'Number',\n                resizable: true\n            }, {\n                key: 'max',\n                name: 'MAX',\n                locked: true,\n                titleClass: 'text-center',\n                sortable: true,\n                alignContent: 'right',\n                filterable: true,\n                filterRenderer: 'NumericFilter',\n                resizable: true\n            }, {\n                key: 'min',\n                name: 'MIN',\n                locked: true,\n                sortable: true,\n                titleClass: 'text-center',\n                alignContent: 'right',\n                filterable: true,\n                filterRenderer: 'NumericFilter',\n                resizable: true\n            }],\n            data: data,\n            buttons: [{\n                title: 'Excel',\n                action: function action() {\n                    $scope.table1.exportToExcel();\n                },\n                className: ''\n            }],\n            height: 450\n        };\n        periodos.map(function (per) {\n            props.header.push({\n                key: 'periodo_' + per,\n                name: '' + per,\n                sortable: true,\n                alignContent: 'right',\n                titleClass: 'text-center',\n                filterable: true,\n                filterRenderer: 'NumericFilter',\n                resizable: true\n            });\n        });\n        document.getElementById('table-calidad-periodal').innerHTML = \"\";\n        $scope.table1 = ReactDOM.render(React.createElement(ReactDataGrid, props), document.getElementById('table-calidad-periodal'));\n    };\n\n    var renderChartCalidadPeriodo = function renderChartCalidadPeriodo(periodos, data) {\n        var series = [];\n        var legends = periodos;\n\n        for (var i in data) {\n            var finca = data[i];\n            var s = {\n                name: finca.finca,\n                connectNulls: true,\n                type: 'line',\n                data: []\n            };\n\n            for (var j in periodos) {\n                var per = periodos[j];\n                s.data.push(finca['periodo_' + per]);\n            }\n            series.push(s);\n        }\n\n        var parent = $(\"#chart-calidad-periodal\").parent();\n        parent.empty();\n        parent.append('<div id=\"chart-calidad-periodal\" class=\"chart\"></div>');\n\n        var props = {\n            series: series,\n            legend: legends,\n            id: \"chart-calidad-periodal\",\n            legendBottom: true,\n            zoom: false,\n            type: 'line',\n            min: 'dataMin'\n        };\n        ReactDOM.render(React.createElement(Historica, props), document.getElementById('chart-calidad-periodal'));\n    };\n\n    var renderTableCausasPeriodo = function renderTableCausasPeriodo(periodos, data) {\n        var props = {\n            header: [{\n                key: 'causa',\n                name: 'CAUSA',\n                titleClass: 'text-center',\n                alignContent: 'left',\n                locked: true,\n                resizable: true\n            }, {\n                key: 'avg',\n                name: 'PROM',\n                locked: true,\n                sortable: true,\n                titleClass: 'text-center',\n                alignContent: 'right',\n                filterable: true,\n                filterRenderer: 'NumericFilter',\n                formatter: 'Number',\n                resizable: true\n            }, {\n                key: 'max',\n                name: 'MAX',\n                locked: true,\n                titleClass: 'text-center',\n                sortable: true,\n                alignContent: 'right',\n                filterable: true,\n                filterRenderer: 'NumericFilter',\n                resizable: true\n            }, {\n                key: 'min',\n                name: 'MIN',\n                locked: true,\n                sortable: true,\n                titleClass: 'text-center',\n                alignContent: 'right',\n                filterable: true,\n                filterRenderer: 'NumericFilter',\n                resizable: true\n            }],\n            data: data,\n            buttons: [{\n                title: 'Excel',\n                action: function action() {\n                    $scope.table2.exportToExcel();\n                },\n                className: ''\n            }],\n            height: 450\n        };\n        periodos.map(function (per) {\n            props.header.push({\n                key: 'periodo_' + per,\n                name: '' + per,\n                sortable: true,\n                alignContent: 'right',\n                titleClass: 'text-center',\n                filterable: true,\n                filterRenderer: 'NumericFilter',\n                resizable: true\n            });\n        });\n        document.getElementById('table-causas-periodal').innerHTML = \"\";\n        $scope.table2 = ReactDOM.render(React.createElement(ReactDataGrid, props), document.getElementById('table-causas-periodal'));\n    };\n\n    var renderChartCausasPeriodo = function renderChartCausasPeriodo(periodos, data) {\n        var series = [];\n        var legends = periodos;\n\n        for (var i in data) {\n            var causa = data[i];\n            var s = {\n                name: causa.causa,\n                connectNulls: true,\n                type: 'line',\n                data: []\n            };\n\n            for (var j in periodos) {\n                var per = periodos[j];\n                s.data.push(causa['periodo_' + per]);\n            }\n            series.push(s);\n        }\n\n        var parent = $(\"#chart-causas-periodal\").parent();\n        parent.empty();\n        parent.append('<div id=\"chart-causas-periodal\" class=\"chart\"></div>');\n\n        var props = {\n            series: series,\n            legend: legends,\n            id: \"chart-causas-periodal\",\n            legendBottom: true,\n            zoom: false,\n            type: 'line',\n            min: 'dataMin'\n        };\n        ReactDOM.render(React.createElement(Historica, props), document.getElementById('chart-causas-periodal'));\n    };\n\n    $scope.last();\n}]);\n\n//# sourceURL=webpack:///./js_modules/laboresAgricolasTendencia.js?");

/***/ })

/******/ });