/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./js_modules/lancofruitVentasComparativo.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./js_modules/lancofruitVentasComparativo.js":
/*!***************************************************!*\
  !*** ./js_modules/lancofruitVentasComparativo.js ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nvar _window = window,\n    app = _window.app,\n    load = _window.load;\n\n\napp.filter('sumOfValue', function () {\n    return function (data, key) {\n        if (angular.isUndefined(data) || angular.isUndefined(key)) return 0;\n        var sum = 0;\n        angular.forEach(data, function (value) {\n            if (value[key] != \"\" && value[key] != undefined && parseFloat(value[key])) {\n                sum = sum + parseFloat(value[key], 10);\n            }\n        });\n        return sum;\n    };\n});\n\napp.filter('sumOfValueDouble', function () {\n    return function (data, key, key2) {\n        if (angular.isUndefined(data) || angular.isUndefined(key) || angular.isUndefined(key2)) return 0;\n        var sum = 0;\n        angular.forEach(data, function (value) {\n            if (value[key][key2] != \"\" && value[key][key2] != undefined && parseFloat(value[key][key2])) {\n                sum = sum + parseFloat(value[key][key2], 10);\n            }\n        });\n        return sum;\n    };\n});\n\napp.filter('getNotRepeat', function () {\n    return function (data, key) {\n        if (angular.isUndefined(data) || angular.isUndefined(key)) return [];\n\n        var _arr = [];\n        data.forEach(function (value) {\n            if (value[key]) {\n                if (!_arr.includes(value[key])) _arr.push(value[key]);\n            }\n        });\n        return _arr;\n    };\n});\n\napp.filter('avgOfValue', function () {\n    return function (data, key) {\n        if (angular.isUndefined(data) || angular.isUndefined(key)) return 0;\n        var sum = 0;\n        var count = 0;\n        angular.forEach(data, function (value) {\n            if (value[key] != \"\" && value[key] != undefined && value[key] > 0) {\n                sum = sum + parseFloat(value[key], 10);\n                count++;\n            }\n        });\n        sum = sum / count;\n        if (isNaN(sum)) return 0;\n        return sum;\n    };\n});\n\napp.filter('keys', function () {\n    return function (data) {\n        return Object.keys(data).length;\n    };\n});\n\napp.service('request', ['$http', function ($http) {\n    this.ventaComparativo = function (callback, params) {\n        var url = 'phrapi/lancofruit/reportes/ventaComparativo';\n        load.block('tabla_venta_dia');\n        $http.post(url, params || {}).then(function (r) {\n            load.unblock('tabla_venta_dia');\n            callback(r.data);\n        });\n    };\n    this.last = function (callback, params) {\n        var url = 'phrapi/lancofruit/reportes/last';\n        $http.post(url, params || {}).then(function (r) {\n            callback(r.data);\n        });\n    };\n}]);\n\nfunction initPastel(id, series) {\n    var legends = [];\n    series.map(function (key) {\n        if (legends.indexOf(series[key]) != -1) legends.push(series[key]);\n    });\n    setTimeout(function () {\n        var data = {\n            data: series,\n            nameseries: \"Pastel\",\n            legend: legends,\n            titulo: \"\",\n            id: id\n        };\n        var parent = $(\"#\" + id).parent();\n        $(\"#\" + id).remove();\n        parent.append('<div id=\"' + id + '\" class=\"chart\"></div>');\n        ReactDOM.render(React.createElement(Pastel, data), document.getElementById(id));\n    }, 250);\n}\n\nfunction printGrafica(id, r) {\n    var type = '';\n    if (r.legend.length > 1) {\n        type = 'line';\n    } else {\n        type = 'bar';\n    }\n\n    setTimeout(function () {\n        var data = {\n            series: r.data,\n            legend: r.legend,\n            umbral: '',\n            id: id,\n            type: type,\n            min: 'dataMin'\n        };\n        var parent = $(\"#\" + id).parent();\n        parent.empty();\n        parent.append('<div id=\"' + id + '\" class=\"chart\"></div>');\n        ReactDOM.render(React.createElement(Historica, data), document.getElementById(id));\n    }, 250);\n}\n\napp.controller('comparativo', ['$scope', 'request', function ($scope, request) {\n\n    $scope.var = 'tabla';\n    $scope.datesEnabled = [];\n    $scope.filters = {\n        fecha_inicial: moment().startOf('month').format('YYYY-MM-DD'),\n        fecha_final: moment().endOf('month').format('YYYY-MM-DD'),\n        id_ruta: 1,\n        type: \"VENTAS\"\n    };\n    $scope.mostrar = true;\n    $scope.filtros = {};\n\n    $scope.rangesDirectives = {\n        'Últimos 7 Días': [moment().subtract(6, 'days'), moment()],\n        'Últimos 30 Días': [moment().subtract(29, 'days'), moment()],\n        'Últimos 90 Días': [moment().subtract(89, 'days'), moment()],\n        'Último Año': [moment().subtract(1, 'year').startOf('year'), moment().subtract(1, 'year').endOf('year')],\n        'Este Año': [moment().startOf('year'), moment().endOf(\"year\")],\n        'Este mes': [moment().startOf('month'), moment().endOf('month')],\n        'Último mes': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]\n    };\n\n    $scope.StartEndDateDirectives = {\n        startDate: moment().startOf('month'),\n        endDate: moment().endOf('month')\n    };\n\n    $scope.changeRangeDate = function (data) {\n        if (data) {\n            $scope.filters.fecha_inicial = data.hasOwnProperty(\"first_date\") ? data.first_date : $scope.filters.fecha_inicial;\n            $scope.filters.fecha_final = data.hasOwnProperty(\"second_date\") ? data.second_date : $scope.filters.fecha_final;\n            $scope.changeCalendar();\n        }\n    };\n    $scope.changeCalendar = function () {\n        request.last(function (r) {\n            $scope.clientes = r.clientes;\n            $scope.comparativo();\n        }, $scope.filters);\n    };\n    $scope.init = function () {\n        request.last(function (r) {\n            $scope.clientes = r.clientes;\n            $scope.rutas = r.rutas;\n            $scope.filters.fecha_inicial = r.fecha;\n            $scope.filters.fecha_final = r.fecha;\n            setTimeout(function () {\n                $(\"#date-picker\").html(r.fecha + ' - ' + r.fecha);\n                $scope.changeRangeDate();\n                $scope.comparativo();\n            }, 250);\n        }, $scope.filters);\n    };\n    $scope.comparativo = function () {\n        request.ventaComparativo(function (r) {\n            $scope.datesEnabled = r.days;\n            $scope.data = r.data;\n            if (r.data.length > 0) {\n                $scope.data_bar = r.data_bar;\n                $scope.data_pie = r.data_pie.map(function (r) {\n                    return {\n                        label: r.ruta,\n                        value: r.ventas\n                    };\n                });\n\n                initPastel('pastel-ventadia', $scope.data_pie);\n                printGrafica('line-ventadia', $scope.data_bar);\n            } else {\n                initPastel('pastel-ventadia', [{ label: 'VENTAS POR DIA', value: 0 }]);\n                printGrafica('line-ventadia', $scope.data_bar);\n            }\n        }, $scope.filters);\n    };\n\n    $scope.renderPie = function () {\n        initPastel('pastel-ventadia', $scope.data_pie);\n    };\n\n    $scope.renderBar = function () {\n        printGrafica('line-ventadia', $scope.data_bar);\n    };\n\n    $scope.init();\n}]);\n\n//# sourceURL=webpack:///./js_modules/lancofruitVentasComparativo.js?");

/***/ })

/******/ });