/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./js_modules/login.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./js_modules/login.js":
/*!*****************************!*\
  !*** ./js_modules/login.js ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nvar Login = function () {\n\n    var handleLogin = function handleLogin() {\n\n        $('.login-form').validate({\n            errorElement: 'span', //default input error message container\n            errorClass: 'help-block', // default input error message class\n            focusInvalid: false, // do not focus the last invalid input\n            rules: {\n                username: {\n                    required: true\n                },\n                password: {\n                    required: true\n                },\n                remember: {\n                    required: false\n                }\n            },\n\n            messages: {\n                username: {\n                    required: \"Username is required.\"\n                },\n                password: {\n                    required: \"Password is required.\"\n                }\n            },\n\n            invalidHandler: function invalidHandler(event, validator) {\n                //display error alert on form submit   \n                $('.alert-danger', $('.login-form')).show();\n            },\n\n            highlight: function highlight(element) {\n                // hightlight error inputs\n                $(element).closest('.form-group').addClass('has-error'); // set error class to the control group\n            },\n\n            success: function success(label) {\n                label.closest('.form-group').removeClass('has-error');\n                label.remove();\n            },\n\n            errorPlacement: function errorPlacement(error, element) {\n                error.insertAfter(element.closest('.input-icon'));\n            },\n\n            submitHandler: function submitHandler(form) {\n                form.submit(); // form validation success, call ajax form submit\n            }\n        });\n\n        $('.login-form input').keypress(function (e) {\n            if (e.which == 13) {\n                if ($('.login-form').validate().form()) {\n                    $('.login-form').submit(); //form validation success, call ajax form submit\n                }\n                return false;\n            }\n        });\n\n        $('.forget-form input').keypress(function (e) {\n            if (e.which == 13) {\n                if ($('.forget-form').validate().form()) {\n                    $('.forget-form').submit();\n                }\n                return false;\n            }\n        });\n\n        $('#forget-password').click(function () {\n            $('.login-form').hide();\n            $('.forget-form').show();\n        });\n\n        $('#back-btn').click(function () {\n            $('.login-form').show();\n            $('.forget-form').hide();\n        });\n    };\n\n    return {\n        //main function to initiate the module\n        init: function init() {\n\n            handleLogin();\n\n            // init background slide images\n            $('.login-bg').backstretch([\"./img/4.jpg\", \"./img/1.jpg\", \"./img/2.jpg\", \"./img/3.jpg\"], {\n                fade: 1000,\n                duration: 8000\n            });\n\n            $('.forget-form').hide();\n        }\n\n    };\n}();\n\njQuery(document).ready(function () {\n    Login.init();\n});\n\n//# sourceURL=webpack:///./js_modules/login.js?");

/***/ })

/******/ });