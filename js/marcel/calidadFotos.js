/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./js_modules/marcel/calidadFotos.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./js_modules/marcel/calidadFotos.js":
/*!*******************************************!*\
  !*** ./js_modules/marcel/calidadFotos.js ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\napp.service('request', ['$http', function ($http) {\n\n    this.getFotos = function (callback, params) {\n        load.block('accordion3');\n\n        var data = params || {};\n        var url = 'phrapi/marcel/calidad/fotos';\n        $http.post(url, data).then(function (r) {\n            load.unblock('accordion3');\n            callback(r.data);\n        });\n    };\n\n    this.getLast = function (callback, params) {\n        var data = params || {};\n        var url = 'phrapi/marcel/calidad/last';\n        $http.post(url, data).then(function (r) {\n            callback(r.data);\n        });\n    };\n}]);\n\napp.filter('orderObjectBy', function () {\n    return function (items, field, reverse) {\n        var filtered = [];\n        angular.forEach(items, function (item) {\n            if (!parseFloat(item[field])) {\n                item[field] = parseFloat(item[field]);\n            }\n            filtered.push(item);\n        });\n        filtered.sort(function (a, b) {\n            if (field == 'hora' || field == 'fecha') {\n                return moment(a.date).isAfter(b.date) ? 1 : -1;\n            } else if (parseFloat(a[field]) && parseFloat(b[field])) {\n                return parseFloat(a[field]) > parseFloat(b[field]) ? 1 : -1;\n            } else {\n                return a[field] > b[field] ? 1 : -1;\n            }\n        });\n        if (reverse) filtered.reverse();\n        return filtered;\n    };\n});\n\napp.controller('produccion', ['$scope', 'request', function ($scope, $request) {\n\n    $scope.filters = {\n        desde: \"\",\n        hasta: \"\"\n    };\n\n    var printFotos = function printFotos(r) {\n        if (r.data) {\n            $scope.marcas = r.marcas;\n            $scope.fotos = r.data;\n        }\n    };\n\n    $scope.getFotos = function () {\n        $request.getFotos(printFotos, $scope.filters);\n    };\n\n    $scope.init = function () {\n        $request.getLast(function (r) {\n            $scope.semanas = r.semanas_disponibles;\n            $scope.filters.desde = r.semana;\n            $scope.filters.hasta = r.semana;\n            $scope.getFotos();\n        });\n    };\n\n    $scope.menorSemana = function (sem) {\n        return parseInt(sem) <= parseInt($scope.filters.hasta);\n    };\n    $scope.mayorSemana = function (sem) {\n        return parseInt(sem) >= parseInt($scope.filters.desde);\n    };\n\n    $scope.init();\n}]);\n\n//# sourceURL=webpack:///./js_modules/marcel/calidadFotos.js?");

/***/ })

/******/ });