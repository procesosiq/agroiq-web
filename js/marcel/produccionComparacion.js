/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./js_modules/marcel/produccionComparacion.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./js_modules/marcel/produccionComparacion.js":
/*!****************************************************!*\
  !*** ./js_modules/marcel/produccionComparacion.js ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\napp.service('request', ['$http', function ($http) {\n\n    var service = {};\n\n    service.graficaVariables = function (callback, params) {\n        load.block('graficas');\n        $http.post('phrapi/comparacion/graficaVariables', params || {}).then(function (r) {\n            load.unblock('graficas');\n            callback(r.data);\n        });\n    };\n\n    service.laniado = function (callback, params) {\n        load.block('laniado');\n        $http.post('phrapi/marcel/produccionsemana/graficaVariables', params || {}).then(function (r) {\n            load.unblock('laniado');\n            callback(r.data);\n        });\n    };\n\n    service.marun = function (callback, params) {\n        load.block('marun');\n        $http.post('phrapi/marun/produccionsemana/graficaVariables', params || {}).then(function (r) {\n            load.unblock('marun');\n            callback(r.data);\n        });\n    };\n\n    service.reiset = function (callback, params) {\n        load.block('reiset');\n        $http.post('phrapi/reiset/produccionsemana/graficaVariables', params || {}).then(function (r) {\n            load.unblock('reiset');\n            callback(r.data);\n        });\n    };\n\n    service.quintana = function (callback, params) {\n        load.block('quintana');\n        $http.post('phrapi/quintana/produccionsemana/graficaVariables', params || {}).then(function (r) {\n            load.unblock('quintana');\n            callback(r.data);\n        });\n    };\n\n    return service;\n}]);\n\nvar charts = {};\n\napp.controller('produccion', ['$scope', 'request', function ($scope, $request) {\n    $scope.variables = {\n        cajas: [\"CAJAS/HA\"],\n        racimos: [\"Calibre\", \"Edad\", \"Peso (lb)\", \"RAC PROC/HA\", \"RAC CORT/HA\", \"RATIO CORTADO\", \"RATIO PROCESADO\", \"MERMA CORTADA\", \"MERMA PROCESADA\"]\n    };\n\n    $scope.variables_correlacion = {\n        cajas: [\"Cajas\"],\n        clima: [\"Horas Luz (400)\", \"Humedad\", \"Lluvia\", \"Rad. Solar\", \"Temp Max.\", \"Temp Min.\"],\n        merma: [\"Merma Neta\", \"Tallo\"],\n        racimos: [\"Calibre\", \"Edad\", \"Manos\", \"Peso\", \"Racimos Procesados\", \"Racimos Cosechados\", \"Ratio Cortado\", \"Ratio Procesado\"],\n        sigat: [\"HT (0S)\", \"HT (11S)\"]\n    };\n\n    $scope.charts = {\n        variables: new echartsPlv()\n    };\n\n    $scope.filters = {\n        var1: 'Peso (lb)',\n        type1: 'line'\n    };\n\n    $scope.filtersCorrelacion = {\n        year: moment().year(),\n        var1: \"Peso\",\n        var2: \"Calibre\",\n        type1: \"line\",\n        type2: \"line\"\n    };\n\n    $scope.init = function () {\n        $scope.initGraficaVariables();\n        $scope.initCorrelacion();\n    };\n\n    $scope.initCorrelacion = function () {\n        $scope.initLanido();\n        $scope.initMarun();\n        $scope.initReiset();\n        $scope.initQuintana();\n    };\n\n    var data_variables = {};\n    var printGraficaVariables = function printGraficaVariables(data, id) {\n        var height = id != 'variables' ? 400 : 500;\n        var parent = $(\"#\" + id).parent();\n        $(\"#\" + id).remove();\n        parent.append('<div id=\"' + id + '\" style=\"height:' + height + 'px;\"></div>');\n        charts[id] = new echartsPlv().init(id, data);\n    };\n\n    $scope.initGraficaVariables = function () {\n        $request.graficaVariables(function (r) {\n            data_variables.correlacion = r.data;\n            printGraficaVariables(r.data, 'variables');\n        }, $scope.filters);\n    };\n\n    $scope.initLanido = function () {\n        $request.laniado(function (r) {\n            data_variables.laniado = r.data;\n            printGraficaVariables(r.data, 'laniado');\n        }, $scope.filtersCorrelacion);\n    };\n    $scope.initMarun = function () {\n        $request.marun(function (r) {\n            data_variables.marun = r.data;\n            printGraficaVariables(r.data, 'marun');\n        }, Object.assign({ idFinca: 1, finca: 1 }, $scope.filtersCorrelacion));\n    };\n    $scope.initReiset = function () {\n        $request.reiset(function (r) {\n            data_variables.reiset = r.data;\n            printGraficaVariables(r.data, 'reiset');\n        }, $scope.filtersCorrelacion);\n    };\n    $scope.initQuintana = function () {\n        $request.quintana(function (r) {\n            data_variables.quintana = r.data;\n            printGraficaVariables(r.data, 'quintana');\n        }, Object.assign({ idFinca: 2, finca: 2 }, $scope.filtersCorrelacion));\n    };\n\n    $scope.selected = function (val) {\n        return [$scope.filters.var1, $scope.filters.var2].indexOf(val) > -1;\n    };\n\n    $scope.toggleLineBar = function (num, type) {\n        if (num == 1) {\n            data_variables.laniado.series = $scope.toggle(data_variables.laniado, $scope.filtersCorrelacion.var1, type);\n            data_variables.marun.series = $scope.toggle(data_variables.marun, $scope.filtersCorrelacion.var1, type);\n            data_variables.reiset.series = $scope.toggle(data_variables.reiset, $scope.filtersCorrelacion.var1, type);\n            data_variables.quintana.series = $scope.toggle(data_variables.quintana, $scope.filtersCorrelacion.var1, type);\n        }\n\n        if (num == 2) {\n            data_variables.laniado.series = $scope.toggle(data_variables.laniado, $scope.filtersCorrelacion.var2, type);\n            data_variables.marun.series = $scope.toggle(data_variables.marun, $scope.filtersCorrelacion.var2, type);\n            data_variables.reiset.series = $scope.toggle(data_variables.reiset, $scope.filtersCorrelacion.var2, type);\n            data_variables.quintana.series = $scope.toggle(data_variables.quintana, $scope.filtersCorrelacion.var2, type);\n        }\n\n        printGraficaVariables(data_variables.laniado, 'laniado');\n        printGraficaVariables(data_variables.marun, 'marun');\n        printGraficaVariables(data_variables.reiset, 'reiset');\n        printGraficaVariables(data_variables.quintana, 'quintana');\n    };\n\n    $scope.toggle = function (data, variable, type) {\n        var series = angular.copy(data.series);\n        series.map(function (serie, index) {\n            if (serie.name.toUpperCase().includes(variable.toUpperCase())) {\n                series[index].type = type;\n                $scope.filters.type1 = type;\n            }\n        });\n\n        return series;\n    };\n\n    $scope.resizeChart = function () {\n        console.log(\"hola\");\n    };\n}]);\n\nfunction resizeChart(id) {\n    console.log(\"hola\");\n    charts[id].resize();\n}\n\n//# sourceURL=webpack:///./js_modules/marcel/produccionComparacion.js?");

/***/ })

/******/ });