/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./js_modules/mario/labores.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./js_modules/mario/labores.js":
/*!*************************************!*\
  !*** ./js_modules/mario/labores.js ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\napp.service('labores', ['$http', function ($http) {\n    this.data = function (callback, params) {\n        load.block('labores_agricolas');\n        $http.post('phrapi/labores/data', params).then(function (r) {\n            if (r.data) {\n                callback(r.data);\n                load.unblock('labores_agricolas');\n            }\n        }).catch(function (err) {\n            load.unblock('labores_agricolas');\n            lert(\"Ocurrio algun problema al descargar la información\");\n        });\n    };\n}]);\n\napp.filter('num', function () {\n    return function (input) {\n        return parseFloat(input) || 0;\n    };\n});\n\napp.filter('sumOfValue', function () {\n    return function (data, key) {\n        if (angular.isUndefined(data) || angular.isUndefined(key)) return 0;\n        var sum = 0;\n        angular.forEach(data, function (value) {\n            if (value[key]) if (parseFloat(value[key])) sum = sum + parseFloat(value[key]);\n        });\n        return sum;\n    };\n});\n\napp.controller('labores', ['$scope', 'labores', function ($scope, labores) {\n\n    $scope.data_labores = [];\n    $scope.filters = {};\n    $scope.params = {};\n    $scope.getSortCls = function (field) {\n        if (field == $scope.searchTable.orderBy) {\n            if ($scope.searchTable.reverse) return 'sorting_desc';else return 'sorting_asc';\n        }\n        return '';\n    };\n\n    $scope.init = function () {\n        labores.data(function (r) {\n            if (r) {\n                console.log(r.tipo_labores);\n                $scope.data_labores = r.tipo_labores;\n            }\n        });\n    };\n\n    $scope.isNumeric = function (val) {\n        return $.isNumeric(val);\n    };\n\n    $scope.semaforo = function (value) {\n        if ($scope.isNumeric(value)) {\n            return 'bg-green-jungle bg-font-green-jungle';\n        } else if (value == 'F') {\n            return 'bg-red-thunderbird bg-font-red-thunderbird';\n        }\n        return '';\n    };\n\n    $scope.exportExcel = function (id_table) {\n        var tableToExcel = function () {\n            var uri = 'data:application/vnd.ms-excel;base64,',\n                template = '<html xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:excel\" xmlns=\"http://www.w3.org/TR/REC-html40\"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv=\"content-type\" content=\"text/plain; charset=UTF-8\"/></head><body><table>{table}</table></body></html>',\n                base64 = function base64(s) {\n                return window.btoa(unescape(encodeURIComponent(s)));\n            },\n                format = function format(s, c) {\n                return s.replace(/{(\\w+)}/g, function (m, p) {\n                    return c[p];\n                });\n            };\n            return function (table, name) {\n                if (!table.nodeType) table = document.getElementById(table);\n                var contentTable = table.innerHTML;\n                // remove filters\n                var cut = contentTable.search('<tr role=\"row\" class=\"filter\">');\n                var cut2 = contentTable.search('</thead>');\n\n                var part1 = contentTable.substring(0, cut);\n                var part2 = contentTable.substring(cut2, contentTable.length);\n                contentTable = part1 + part2;\n\n                var ctx = { worksheet: name || 'Worksheet', table: contentTable };\n                window.location.href = uri + base64(format(template, ctx));\n            };\n        }();\n        tableToExcel(id_table, \"Información de Transtito\");\n    };\n\n    $scope.exportPrint = function (id_table) {\n        var image = '<div style=\"display:block; height:55;\"><img style=\"float: right;\" width=\"100\" height=\"50\" src=\"./../logos/Logo.png\" /></div><br>';\n        var table = document.getElementById(id_table);\n        var contentTable = table.outerHTML;\n        // remove filters\n        var cut = contentTable.search('<tr role=\"row\" class=\"filter\">');\n        var cut2 = contentTable.search('</thead>');\n        var part1 = contentTable.substring(0, cut);\n        var part2 = contentTable.substring(cut2, contentTable.length);\n        // add image\n        contentTable = image + part1 + part2;\n\n        newWin = window.open(\"\");\n        newWin.document.write(contentTable);\n        newWin.print();\n        newWin.close();\n    };\n\n    $scope.init();\n}]);\n\n//# sourceURL=webpack:///./js_modules/mario/labores.js?");

/***/ })

/******/ });