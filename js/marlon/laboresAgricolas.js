/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./js_modules/marlon/laboresAgricolas.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./js_modules/marlon/laboresAgricolas.js":
/*!***********************************************!*\
  !*** ./js_modules/marlon/laboresAgricolas.js ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\napp.service('request', ['$http', function ($http) {\n    var service = {};\n\n    service.last = function (callback, params) {\n        load.block('grafica_tendecia');\n        $http.post('phrapi/marlon/laboresAgricolas/last', params || {}).then(function (r) {\n            load.unblock('grafica_tendecia');\n            callback(r.data);\n        });\n    };\n\n    service.tendencia = function (callback, params) {\n        load.block('grafica_tendecia');\n        $http.post('phrapi/marlon/laboresAgricolas/tendencia', params || {}).then(function (r) {\n            load.unblock('grafica_tendecia');\n            callback(r.data);\n        });\n    };\n\n    service.tablaHistorica = function (callback, params) {\n        load.block('tablaHistorica');\n        $http.post('phrapi/marlon/laboresAgricolas/tablaHistorica', params || {}).then(function (r) {\n            load.unblock('tablaHistorica');\n            callback(r.data);\n        });\n    };\n\n    service.graficaLabores = function (callback, params) {\n        load.block('labores');\n        $http.post('phrapi/marlon/laboresAgricolas/graficaLabores', params || {}).then(function (r) {\n            load.unblock('labores');\n            callback(r.data);\n        });\n    };\n\n    service.fotos = function (callback, params) {\n        load.block('accordion1');\n        $http.post('phrapi/marlon/laboresAgricolas/fotos', params || {}).then(function (r) {\n            load.unblock('accordion1');\n            callback(r.data);\n        }).catch(function () {\n            return load.unblock('accordion1');\n        });\n    };\n\n    return service;\n}]);\n\napp.filter('orderObjectBy', function () {\n    return function (items, field, reverse) {\n        var filtered = [];\n        angular.forEach(items, function (item) {\n            if (field == 'hora' || field == 'fecha') {\n                item.date = moment(item.fecha + ' ' + item.hora);\n            }\n            if (!isNaN(parseInt(item))) {\n                item = parseInt(item);\n            }\n            filtered.push(item);\n        });\n        filtered.sort(function (a, b) {\n            if (field == 'hora' || field == 'fecha') {\n                return moment(a.date).isAfter(b.date) ? 1 : -1;\n            } else if (parseFloat(a[field]) && parseFloat(b[field])) {\n                return parseFloat(a[field]) > parseFloat(b[field]) ? 1 : -1;\n            } else {\n                return a[field] > b[field] ? 1 : -1;\n            }\n        });\n        if (reverse) filtered.reverse();\n        return filtered;\n    };\n});\n\nfunction getOptionsGraficaReact(id, options) {\n    var newOptions = {\n        series: options.series,\n        legend: options.legends,\n        umbral: null,\n        id: id,\n        type: 'line',\n        min: 'dataMin',\n        max: null\n    };\n    return newOptions;\n}\n\nfunction initGrafica(id, options) {\n    setTimeout(function () {\n        var data = getOptionsGraficaReact(id, options);\n        var parent = $(\"#\" + id).parent();\n        parent.empty();\n        parent.append('<div id=\"' + id + '\" class=\"charts-500\"></div>');\n        ReactDOM.render(React.createElement(Historica, data), document.getElementById(id));\n    }, 250);\n}\n\nfunction initPastel(id, series) {\n    var legends = [];\n    var newSeries = [];\n    series.map(function (value) {\n        newSeries.push({\n            label: value.name,\n            value: parseFloat(value.value)\n        });\n        if (legends.indexOf(value.name) != -1) legends.push(value.name);\n    });\n    setTimeout(function () {\n        var data = {\n            data: newSeries,\n            nameseries: \"Pastel\",\n            legend: legends,\n            titulo: \"\",\n            id: id\n        };\n        var parent = $(\"#\" + id).parent();\n        parent.empty();\n        parent.append('<div id=\"' + id + '\" class=\"charts-400\"></div>');\n        ReactDOM.render(React.createElement(Pastel, data), document.getElementById(id));\n    }, 250);\n}\n\napp.controller('controller', ['$scope', 'request', function ($scope, $request) {\n\n    $scope.filters = {\n        anio: '' + moment().year()\n    };\n    $scope.anios = [];\n\n    $scope.checks = function (data, semana) {\n        var porcentaje = parseFloat(data['SEM ' + semana]);\n        if (!isNaN(porcentaje) && porcentaje > 0) {\n            if (porcentaje > parseFloat($scope.umbrales.yellow_umbral_2)) return 'bg-green-jungle bg-font-green-jungle';else if (porcentaje < parseFloat($scope.umbrales.yellow_umbral_1)) return 'bg-red-thunderbird bg-font-red-thunderbird';else return 'bg-yellow-lemon bg-font-yellow-lemon';\n        }\n        return '';\n    };\n\n    var responseGraficaTendencia = function responseGraficaTendencia(r) {\n        initGrafica('grafica_tendencia', r.week);\n    };\n    var initGraficaTendencia = function initGraficaTendencia() {\n        $request.tendencia(responseGraficaTendencia, $scope.filters);\n    };\n\n    var responseTablaHistorica = function responseTablaHistorica(r) {\n        $scope.umbrales = r.umbrals || {};\n        $scope.semanas = r.semanas || [];\n        $scope.tablaHistorica = r.data || [];\n    };\n    var initTablaHistorica = function initTablaHistorica() {\n        $request.tablaHistorica(responseTablaHistorica, $scope.filters);\n    };\n\n    var initGraficaLabores = function initGraficaLabores() {\n        $request.graficaLabores(responseGraficaLabores, $scope.filters);\n    };\n    var responseGraficaLabores = function responseGraficaLabores(r) {\n        initPastel('grafica_labores', r.main);\n        $scope.labores = r.labores;\n        $scope.filters.labor = Object.keys($scope.labores)[0];\n        $scope.changeLaborGrafica();\n    };\n\n    var requestFotos = function requestFotos() {\n        $request.fotos(function (r) {\n            $scope.fotos = r.fotos;\n        }, $scope.filters);\n    };\n\n    $scope.changeLaborGrafica = function () {\n        initPastel('grafica_causas', $scope.labores[$scope.filters.labor]);\n    };\n\n    $scope.init = function () {\n        initGraficaTendencia();\n        initGraficaLabores();\n        requestFotos();\n        initTablaHistorica();\n    };\n\n    $scope.fnExcelReport = function (id_table, title) {\n        /*var tab_text=\"<table border='2px'><tr bgcolor='#87AFC6'>\";\n        var textRange; var j=0;\n        tab = document.getElementById(id_table); // id of table\n         for(j = 0 ; j < tab.rows.length ; j++) \n        {     \n            tab_text=tab_text+tab.rows[j].innerHTML+\"</tr>\";\n            //tab_text=tab_text+\"</tr>\";\n        }\n         tab_text=tab_text+\"</table>\";\n        window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));*/\n        var data = new Blob([document.getElementById(id_table).outerHTML], {\n            type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf8'\n        });\n        saveAs(data, title + '.xls');\n    };\n\n    $scope.last = function () {\n        return new Promise(function (resolve) {\n            $request.last(function (r) {\n                $scope.anios = r.anios;\n                resolve();\n            });\n        });\n    };\n\n    $scope.last().then(function () {\n        $scope.init();\n    });\n}]);\n\n//# sourceURL=webpack:///./js_modules/marlon/laboresAgricolas.js?");

/***/ })

/******/ });