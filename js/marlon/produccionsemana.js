/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./js_modules/marlon/produccionsemana.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./js_modules/marlon/produccionsemana.js":
/*!***********************************************!*\
  !*** ./js_modules/marlon/produccionsemana.js ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nfunction dlCanvas(nombre, mode) {\n    var self = this;\n    // $(\"#inicio\")[0]\n    html2canvas($(\"#contenedor\"), {\n        onrendered: function onrendered(canvas) {\n            var dt = canvas.toDataURL('image/png');\n            /* Change MIME type to trick the browser to downlaod the file instead of displaying it */\n            // dt = dt.replace(/^data:image\\/[^;]*/, 'data:application/octet-stream');\n\n            /* In addition to <a>'s \"download\" attribute, you can define HTTP-style headers */\n            // dt = dt.replace(/^data:application\\/octet-stream/, 'data:application/octet-stream;headers=Content-Disposition%3A%20attachment%3B%20filename=Canvas.png');\n            if (mode == \"pdf\") {\n                var doc = new jsPDF('p', 'mm');\n                doc.addImage(dt, 'PNG', 5, 5, 195, 290);\n                doc.save(nombre + '.pdf');\n            } else {\n                var a = document.createElement('a');\n                a.href = dt;\n                a.download = nombre + '.png';\n\n                a.click();\n            }\n        }\n    });\n};\n\napp.filter('orderObjectBy', function () {\n    return function (items, field, reverse) {\n        var filtered = [];\n        angular.forEach(items, function (item) {\n            filtered.push(item);\n        });\n        filtered.sort(function (a, b) {\n            if (parseFloat(a[field]) && parseFloat(b[field])) return parseFloat(a[field]) > parseFloat(b[field]) ? 1 : -1;else return a[field] > b[field] ? 1 : -1;\n        });\n        if (reverse) filtered.reverse();\n        return filtered;\n    };\n});\n\napp.filter('orderByCajas', function () {\n    return function (items, field) {\n        var filtered = [];\n        angular.forEach(items, function (item) {\n            filtered.push(item);\n        });\n        filtered.sort(function (a, b) {\n            if (a[field] == 'MI COMISARIATO') return 1;\n            if (b[field] == 'MI COMISARIATO') return -1;\n            return a[field] > b[field] ? 1 : -1;\n        });\n\n        return filtered;\n    };\n});\n\napp.service('request', ['$http', function ($http) {\n\n    var service = {};\n\n    service.last = function (callback, params) {\n        $http.post('phrapi/quintana/produccionsemana/last', params || {}).then(function (r) {\n            callback(r.data);\n        });\n    };\n\n    service.tags = function (callback, params) {\n        $http.post('phrapi/quintana/produccionsemana/tags', params || {}).then(function (r) {\n            callback(r.data);\n        });\n    };\n\n    service.graficaPromEdad = function (callback, params) {\n        load.block('edad_promedio');\n        $http.post('phrapi/quintana/produccionsemana/graficaEdadPromedio', params || {}).then(function (r) {\n            load.unblock('edad_promedio');\n            callback(r.data);\n        });\n    };\n\n    service.resumenAcumulado = function (callback, params) {\n        load.block('resumen_acumulado');\n        $http.post('phrapi/quintana/produccionsemana/resumenAcumulados', params || {}).then(function (r) {\n            load.unblock('resumen_acumulado');\n            callback(r.data);\n        });\n    };\n\n    service.reporteProduccion = function (callback, params) {\n        load.block('reporte_produccion');\n        $http.post('phrapi/quintana/produccionsemana/reporteProduccion', params || {}).then(function (r) {\n            load.unblock('reporte_produccion');\n            callback(r.data);\n        });\n    };\n\n    service.graficaVariables = function (callback, params) {\n        load.block('graficas');\n        $http.post('phrapi/quintana/produccionsemana/graficaVariables', params || {}).then(function (r) {\n            load.unblock('graficas');\n            callback(r.data);\n        });\n    };\n\n    return service;\n}]);\n\napp.controller('produccion', ['$scope', 'client', 'request', function ($scope, client, $request) {\n    cambiosLotes = function cambiosLotes() {\n        $scope.init();\n    };\n    $scope.type = \"cosechados\";\n    $scope.interval = {};\n    cambiarCosecha = function cambiarCosecha() {\n        clearInterval($scope.interval);\n        $scope.init();\n    };\n\n    $scope.variables = {\n        cajas: [\"Cajas\"],\n        clima: [\"Horas Luz (100)\", \"Horas Luz (150)\", \"Horas Luz (200)\", \"Horas Luz (400)\", \"Humedad\", \"Lluvia\", \"Rad. Solar\", \"Temp Max.\", \"Temp Min.\"],\n        merma: [\"Merma Neta\", \"Tallo\", \"Cosecha\", \"Enfunde\", \"ADM\", \"Natural\", \"Proceso\"],\n        racimos: [\"Calibre\", \"Edad\", \"Manos\", \"Peso\", \"Racimos Procesados\", \"Racimos Cosechados\", \"Ratio Cortado\", \"Ratio Procesado\"]\n    };\n\n    $scope.printPdf = function () {\n        dlCanvas(\"Reporte Racimos\", \"pdf\");\n    };\n\n    $scope.printNormal = function () {\n        dlCanvas(\"Reporte Racimos\", \"print\");\n    };\n\n    $scope.id_company = 0;\n    $scope.filters = {\n        idFinca: 2,\n        idLote: 0,\n        idLabor: 0,\n        fecha_inicial: moment().startOf('month').format('YYYY-MM-DD'),\n        fecha_final: moment().endOf('month').format('YYYY-MM-DD'),\n        cliente: \"\",\n        marca: \"\",\n        palanca: \"\",\n        finca: 2,\n        type: \"cosechados\",\n        year: moment().format('YYYY'),\n        var1: \"Peso\",\n        var2: \"Calibre\",\n        type1: \"line\",\n        type2: \"line\"\n    };\n\n    $scope.StartEndDateDirectives = {\n        startDate: moment().startOf('month'),\n        endDate: moment().endOf('month')\n    };\n\n    $scope.changeRangeDate = function (data) {\n        if (data) {\n            $scope.filters.fecha_inicial = data.hasOwnProperty(\"first_date\") ? data.first_date : $scope.filters.fecha_inicial;\n            $scope.filters.fecha_final = data.hasOwnProperty(\"second_date\") ? data.second_date : $scope.filters.fecha_final;\n        }\n        $scope.init();\n    };\n\n    $scope.tabla = {\n        produccion: [],\n        resumen: []\n    };\n\n    $scope.openDetalle = function (data) {\n        if (data.campo != \"MAX\" && data.campo != \"MIN\" && data.campo != \"AVG\") data.expanded = !data.expanded;\n    };\n\n    $scope.openDetalleLote = function (data) {\n        data.expanded = !data.expanded;\n    };\n\n    $scope.sorterFunc = function (semana) {\n        return parseInt(semana.campo);\n    };\n\n    $scope.colores = [];\n    $scope.totales = [];\n    $scope.palancas = [];\n    $scope.lotes = [];\n    $scope.visibleColumns = 7;\n    $scope.withTable = 100 / $scope.visibleColumns + \"%\";\n\n    $scope.tags = {\n        racimo: {\n            value: 0\n        },\n        ratio_cortado: {\n            value: 0\n        },\n        ratio_procesado: {\n            value: 0\n        },\n        ratooning: {\n            value: 0\n        },\n        merma_cortada: {\n            value: 0\n        },\n        merma_procesada: {\n            value: 0\n        },\n        edad: {\n            value: 0\n        },\n        recusados: {\n            value: 0\n        },\n        cajas: {\n            value: 0\n        },\n        otras_cajas: {\n            value: 0\n        },\n        calibracion: {\n            value: 0\n        },\n        merma_cosechada: {\n            value: 0\n        },\n        enfunde: {\n            value: 0\n        },\n        recobro: {\n            value: 0\n        },\n        merma_neta: {\n            value: 0\n        },\n        merma_cajas: {\n            value: 0\n        },\n        merma_dolares: {\n            value: 0\n        },\n        merma_kg: {\n            value: 0\n        }\n    };\n\n    $scope.charts = {\n        variables: new echartsPlv()\n    };\n\n    var printTags = function printTags(r) {\n        $scope.tags.cajas.value = r.data.peso_prom_cajas.value;\n        $scope.tags.calibracion.value = r.data.calibracion_prom.value;\n        $scope.tags.racimo.value = r.data.peso_prom_racimos.value;\n        $scope.tags.edad.value = r.data.edad_prom_racimos.value;\n        $scope.tags.ratio_cortado.value = r.data.ratio_cortado.value;\n        $scope.tags.ratio_procesado.value = r.data.ratio_procesado.value;\n        $scope.tags.merma_cosechada.value = r.data.porc_merma_cosechada.value;\n        $scope.tags.merma_procesada.value = r.data.porc_merma_procesado.value;\n        $scope.tags.enfunde.value = r.data.enfunde_ha.value;\n        $scope.tags.merma_neta.value = r.data.porc_merma_neta.value;\n        $scope.tags.merma_kg.value = r.data.merma_kg.value;\n        $scope.tags.merma_cajas.value = r.data.merma_cajas.value;\n        $scope.tags.merma_dolares.value = r.data.merma_dolares.value;\n\n        setTimeout(function () {\n            $(\".counter_tags\").counterUp({\n                delay: 10,\n                time: 1000\n            });\n\n            $scope.num_semanas = $scope.filters.year == moment().format('YYYY') ? moment().week() : 52;\n        }, 250);\n    };\n\n    var printAcumulado = function printAcumulado(r) {\n        $scope.resumen = r.resumen;\n    };\n\n    $scope.init = function () {\n        var data = $scope.filters;\n        $request.tags(printTags, data);\n        $request.resumenAcumulado(printAcumulado, data);\n        //client.post('phrapi/quintana/produccionsemana/index', $scope.printDetails, data, 'graficas')\n        $scope.initGraficaEdadProm();\n        $scope.initGraficaVariables();\n        $scope.initTableReporteProduccion();\n    };\n\n    $scope.changeFinca = function () {\n        $scope.init();\n    };\n\n    $scope.table = {\n        orderBy: 'semana',\n        reverse: false\n    };\n    $scope.setOrder = function (column) {\n        if ($scope.table.orderBy != column) {\n            $scope.table.orderBy = column;\n            $scope.table.reverse = false;\n        } else {\n            $scope.table.reverse = !$scope.table.reverse;\n        }\n    };\n\n    $scope.start = true;\n    $scope.cajas = 0;\n\n    function numberWithCommas(x) {\n        return x.toString().replace(/\\B(?=(\\d{3})+(?!\\d))/g, \",\");\n    }\n\n    var printGraficaEdad = function printGraficaEdad(r) {\n\n        var data = {\n            series: r.chart.data,\n            legend: r.chart.legend,\n            umbral: r.chart.umbral,\n            id: \"edad_promedio\",\n            legendBottom: true,\n            zoom: false,\n            type: 'line',\n            min: 'dataMin'\n        };\n        ReactDOM.render(React.createElement(Historica, data), document.getElementById('edad_promedio'));\n    };\n\n    $scope.initGraficaEdadProm = function () {\n        $request.graficaPromEdad(printGraficaEdad, $scope.filters);\n    };\n\n    var printTableReporteProduccion = function printTableReporteProduccion(r) {\n        $scope.reporteProduccion = r.data;\n    };\n\n    $scope.initTableReporteProduccion = function () {\n        $request.reporteProduccion(printTableReporteProduccion, $scope.filters);\n    };\n\n    var data_variables = {};\n    var printGraficaVariables = function printGraficaVariables(r) {\n        data_variables = r.data;\n        $scope.charts.variables.init('variables', r.data);\n    };\n\n    $scope.initGraficaVariables = function () {\n        $request.graficaVariables(printGraficaVariables, $scope.filters);\n    };\n\n    $scope.selected = function (val) {\n        return [$scope.filters.var1, $scope.filters.var2].indexOf(val) > -1;\n    };\n\n    $scope.toggleLineBar = function (num, type) {\n        if (num == 1 && data_variables.series[0].name.toUpperCase() == $scope.filters.var1.toUpperCase()) {\n            data_variables.series[0].type = type;\n            $scope.filters.type1 = type;\n        } else if (num == 1) {\n            data_variables.series[1].type = type;\n            $scope.filters.type1 = type;\n        }\n\n        if (num == 2 && data_variables.series[0].name.toUpperCase() == $scope.filters.var2.toUpperCase()) {\n            data_variables.series[0].type = type;\n            $scope.filters.type2 = type;\n        } else if (num == 2) {\n            data_variables.series[1].type = type;\n            $scope.filters.type2 = type;\n        }\n        printGraficaVariables({ data: data_variables });\n    };\n\n    $scope.last = function () {\n        $request.last(function (r) {\n            if (Object.keys(r.fincas).length > 0) if (Object.keys(r.fincas).indexOf($scope.filters.finca) == -1) {\n                $scope.filters.finca = Object.keys(r.fincas)[0];\n            }\n            $scope.filters.fecha_inicial = r.fecha;\n            $scope.filters.fecha_final = r.fecha;\n            $(\"#date-picker\").html(r.fecha + \" -  \" + r.fecha);\n            $scope.init();\n        });\n    };\n}]);\n\n//# sourceURL=webpack:///./js_modules/marlon/produccionsemana.js?");

/***/ })

/******/ });