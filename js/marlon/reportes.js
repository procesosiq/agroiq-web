/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./js_modules/marlon/reportes.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./js_modules/marlon/reportes.js":
/*!***************************************!*\
  !*** ./js_modules/marlon/reportes.js ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nvar _typeof2 = __webpack_require__(/*! babel-runtime/helpers/typeof */ \"./node_modules/babel-runtime/helpers/typeof.js\");\n\nvar _typeof3 = _interopRequireDefault(_typeof2);\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\n/*----------  UTILIDADES SOBRE ARRAYS  ----------*/\nArray.prototype.amax = function () {\n\treturn {\n\t\tlabel: this.indexOf(Math.max.apply(null, this)),\n\t\tvalue: new Number(Math.max.apply(null, this)).toFixed(2)\n\t};\n};\n\nArray.prototype.amin = function () {\n\treturn {\n\t\tlabel: this.indexOf(Math.min.apply(null, this)),\n\t\tvalue: new Number(Math.min.apply(null, this)).toFixed(2)\n\t};\n};\n\nArray.prototype.sum = function (prop) {\n\tvar property = prop || -1;\n\tvar total = 0;\n\tif (property == prop) {\n\t\tfor (var i = 0, _len = this.length; i < _len; i++) {\n\t\t\ttotal += this[i][prop];\n\t\t}\n\t} else {\n\t\tfor (var i = 0, _len = this.length; i < _len; i++) {\n\t\t\ttotal += this[i];\n\t\t}\n\t}\n\treturn total;\n};\n\nArray.prototype.avg = function (prop) {\n\tvar property = prop || -1;\n\tvar total = 0,\n\t    avg = 0;\n\tif (property == prop) {\n\t\tfor (var i = 0, _len = this.length; i < _len; i++) {\n\t\t\ttotal += this[i][prop];\n\t\t}\n\t} else {\n\t\tfor (var i = 0, _len = this.length; i < _len; i++) {\n\t\t\ttotal += this[i];\n\t\t}\n\t}\n\n\tavg = total / this.length;\n\n\treturn new Number(avg).toFixed(2);\n};\n\nArray.prototype.getUnique = function () {\n\tvar u = {},\n\t    a = [];\n\tfor (var i = 0, l = this.length; i < l; ++i) {\n\t\tif (u.hasOwnProperty(this[i])) {\n\t\t\tcontinue;\n\t\t}\n\t\ta.push(this[i]);\n\t\tu[this[i]] = 1;\n\t}\n\treturn a;\n};\n/*----------  UTILIDADES SOBRE ARRAYS  ----------*/\n\nvar myChartPrincial = {};\nvar data_graficas = {\n\tsemana: {},\n\tmes: {},\n\tauditoria: {}\n};\nvar actual_graficas = {};\n\napp.filter('orderObjectBy', function () {\n\treturn function (items, field, reverse) {\n\t\tvar filtered = [];\n\t\tangular.forEach(items, function (item) {\n\t\t\tif (item.hasOwnProperty(\"promedio\")) {\n\t\t\t\titem.promedio = parseFloat(item.promedio);\n\t\t\t}\n\t\t\tif (item.hasOwnProperty(\"porcentaje\")) {\n\t\t\t\tif (!isNaN(parseFloat(item.porcentaje))) item.porcentaje = parseFloat(item.porcentaje);\n\t\t\t}\n\t\t\tif (item.hasOwnProperty(\"porcentaje2\")) {\n\t\t\t\tif (!isNaN(parseFloat(item.porcentaje2))) item.porcentaje2 = parseFloat(item.porcentaje2);\n\t\t\t}\n\n\t\t\tfiltered.push(item);\n\t\t});\n\t\tfiltered.sort(function (a, b) {\n\t\t\treturn a[field] > b[field] ? 1 : -1;\n\t\t});\n\t\tif (reverse) filtered.reverse();\n\t\treturn filtered;\n\t};\n});\n\napp.filter('orderObjectBy2', function () {\n\treturn function (items, field, reverse) {\n\t\tvar filtered = [];\n\t\tangular.forEach(items, function (item) {\n\t\t\tif (item.hasOwnProperty(\"promedio\")) {\n\t\t\t\titem.promedio = parseFloat(item.promedio);\n\t\t\t}\n\t\t\tif (item.hasOwnProperty(\"porcentaje\")) {\n\t\t\t\tif (!isNaN(parseFloat(item.porcentaje))) item.porcentaje = parseFloat(item.porcentaje);\n\t\t\t}\n\t\t\tif (item.hasOwnProperty(\"porcentaje2\")) {\n\t\t\t\tif (!isNaN(parseFloat(item.porcentaje2))) item.porcentaje2 = parseFloat(item.porcentaje2);\n\t\t\t}\n\n\t\t\tfiltered.push(item);\n\t\t});\n\t\tfiltered.sort(function (a, b) {\n\t\t\tif (!reverse) {\n\t\t\t\tif (a[field] == null) {\n\t\t\t\t\ta[field] = '900.00000';\n\t\t\t\t}\n\t\t\t\tif (b[field] == null) {\n\t\t\t\t\tb[field] = '900.00000';\n\t\t\t\t}\n\t\t\t\tif (a[field] == '0.00000') {\n\t\t\t\t\ta[field] = '900.00000';\n\t\t\t\t}\n\t\t\t\tif (b[field] == '0.00000') {\n\t\t\t\t\tb[field] = '900.00000';\n\t\t\t\t}\n\t\t\t} else {\n\t\t\t\tif (a[field] == null) {\n\t\t\t\t\ta[field] = '0.00000';\n\t\t\t\t}\n\t\t\t\tif (b[field] == null) {\n\t\t\t\t\tb[field] = '0.00000';\n\t\t\t\t}\n\t\t\t\tif (a[field] == '900.00000') {\n\t\t\t\t\ta[field] = '0.00000';\n\t\t\t\t}\n\t\t\t\tif (b[field] == '900.00000') {\n\t\t\t\t\tb[field] = '0.00000';\n\t\t\t\t}\n\t\t\t}\n\t\t\t// alert(b[field]);\n\t\t\treturn a[field] - b[field];\n\t\t});\n\t\tif (reverse) filtered.reverse();\n\t\treturn filtered;\n\t};\n});\n\napp.filter('unique', function () {\n\treturn function (collection, keyname) {\n\t\tvar output = [],\n\t\t    keys = [];\n\n\t\tangular.forEach(collection, function (item) {\n\t\t\tvar key = item[keyname];\n\t\t\tif (keys.indexOf(key) === -1) {\n\t\t\t\tkeys.push(key);\n\t\t\t\toutput.push(item);\n\t\t\t}\n\t\t});\n\n\t\treturn output;\n\t};\n});\n\napp.filter('float', function () {\n\treturn function (input) {\n\t\treturn parseFloat(input);\n\t};\n});\n\napp.factory('Excel', function ($window) {\n\tvar uri = 'data:application/vnd.ms-excel;base64,',\n\t    template = '<html xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:excel\" xmlns=\"http://www.w3.org/TR/REC-html40\"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>',\n\t    base64 = function base64(s) {\n\t\treturn $window.btoa(unescape(encodeURIComponent(s)));\n\t},\n\t    format = function format(s, c) {\n\t\treturn s.replace(/{(\\w+)}/g, function (m, p) {\n\t\t\treturn c[p];\n\t\t});\n\t};\n\treturn {\n\t\ttableToExcel: function tableToExcel(tableId, worksheetName) {\n\t\t\tvar table = $(tableId),\n\t\t\t    ctx = { worksheet: worksheetName, table: table.html() },\n\t\t\t    href = uri + base64(format(template, ctx));\n\t\t\treturn href;\n\t\t}\n\t};\n});\n\nfunction getOptionsGraficaReact(id, options) {\n\tvar newOptions = {\n\t\tseries: options.series,\n\t\tlegend: options.legends,\n\t\tumbral: null,\n\t\tid: id,\n\t\ttype: 'line',\n\t\tmin: 'dataMin', //getMinSeries(options.series),\n\t\tmax: null //getMaxSeries(options.series)\n\t};\n\treturn newOptions;\n}\n\napp.controller('reportes', ['$scope', '$http', 'client', '$controller', 'Excel', '$filter', function ($scope, $http, client, $controller, Excel, $filter) {\n\tvar isPushStep4 = false;\n\t$scope.id_company = 0;\n\t$scope.umbrales = {};\n\t$scope.cache = {};\n\n\tvar umbral_range = {\n\t\tmin: 0,\n\t\tmax: 100\n\t};\n\n\t$scope.wizardStep = {\n\t\tparams: {\n\t\t\tidZona: 1,\n\t\t\tidFinca: 0,\n\t\t\tidLote: 0,\n\t\t\tidLabor: 0,\n\t\t\tidZonaLabor: 0,\n\t\t\tidFincaLabor: 0,\n\t\t\tidLoteLabor: 0,\n\t\t\tfecha_inicial: moment().subtract(1, 'month').startOf('month').format('YYYY-MM-DD'),\n\t\t\tfecha_final: moment().subtract(1, 'month').endOf('month').format('YYYY-MM-DD')\n\t\t},\n\t\tinicialPath: 'phrapi/marlon/reportes/index',\n\t\tstep: 0,\n\t\tpath: [],\n\t\ttemplatePath: [],\n\t\tnocache: function nocache(data) {\n\t\t\tvar self = this;\n\t\t\tclient.post(self.inicialPath, function (r, b) {\n\t\t\t\tif (r) {\n\t\t\t\t\tself.params.idZona = 1;\n\t\t\t\t\tself.params.idFinca = 1;\n\t\t\t\t\tself.step = 2;\n\t\t\t\t\tif (r.id_company == 5 || r.id_company == 8) {\n\t\t\t\t\t\tself.params.idZona = 0;\n\t\t\t\t\t\tself.params.idFinca = 0;\n\t\t\t\t\t\tself.step = 0;\n\t\t\t\t\t} else if (r.id_company == 4 || r.id_company == 7 || r.id_company == 3) {\n\t\t\t\t\t\tself.params.idZona = 1;\n\t\t\t\t\t\tself.params.idFinca = 0;\n\t\t\t\t\t\tself.step = 1;\n\t\t\t\t\t}\n\n\t\t\t\t\tself.path.push('phrapi/marlon/reportes/zonas');\n\t\t\t\t\tself.path.push('phrapi/marlon/reportes/fincas');\n\t\t\t\t\tself.path.push('phrapi/marlon/reportes/lotes');\n\t\t\t\t\tself.path.push('phrapi/marlon/reportes/labores');\n\t\t\t\t\tself.path.push('phrapi/marlon/reportes/causas');\n\t\t\t\t\t/*----------  LABORES  ----------*/\n\t\t\t\t\tself.path.push('phrapi/marlon/labores/zonas');\n\t\t\t\t\tself.path.push('phrapi/marlon/labores/fincas');\n\t\t\t\t\tself.path.push('phrapi/marlon/labores/lotes');\n\t\t\t\t\t/*----------  LABORES  ----------*/\n\t\t\t\t\tself.templatePath.push('/views/marlon/step5.html?' + Math.random());\n\t\t\t\t\tself.templatePath.push('/views/marlon/step4.html?' + Math.random());\n\t\t\t\t\tself.templatePath.push('/views/marlon/step3.html?' + Math.random());\n\t\t\t\t\tself.templatePath.push('/views/marlon/step2.html?' + Math.random());\n\t\t\t\t\tself.templatePath.push('/views/marlon/step1.html?' + Math.random());\n\t\t\t\t\t/*----------  LABORES  ----------*/\n\t\t\t\t\tself.templatePath.push('/views/marlon/labor_zona.html?' + Math.random());\n\t\t\t\t\tself.templatePath.push('/views/marlon/labor_finca.html?' + Math.random());\n\t\t\t\t\tself.templatePath.push('/views/marlon/labor_lote.html?' + Math.random());\n\t\t\t\t\t/*----------  LABORES  ----------*/\n\n\t\t\t\t\t$scope.id_company = r.id_company;\n\t\t\t\t\tvar data = self.params;\n\t\t\t\t}\n\t\t\t});\n\t\t}\n\n\t};\n\n\t$scope.tags = {\n\t\tlote: {\n\t\t\tvalor: 0,\n\t\t\tlabel: \"\"\n\t\t},\n\t\tlabor: {\n\t\t\tvalor: 0,\n\t\t\tlabel: \"\"\n\t\t},\n\t\toperador: {\n\t\t\tvalor: 0,\n\t\t\tlabel: \"\"\n\t\t},\n\t\tauditoria: {\n\t\t\tvalor: 0\n\t\t}\n\t};\n\n\t$scope.interval = 140000;\n\t$scope.modo = \"mes\";\n\n\t$scope.wizardData = {\n\t\tdata: [],\n\t\tdata_labores: [],\n\t\tdata_labores_lotes: [],\n\t\tlabores: [],\n\t\tmain: [],\n\t\tlote_name: [],\n\t\tlabor_name: []\n\t};\n\n\t$scope.status_tittle = \"\";\n\n\t$scope.tittles = {\n\t\tlote: \"Todos\",\n\t\tlabor: \"Todos\",\n\t\tgetTittle: function getTittle() {\n\t\t\tvar response = \"Auditoría de Labores Agrícolas\";\n\t\t\tif (this.lote == \"Todos\" && this.labor == \"Todos\") {\n\t\t\t\tresponse = \"Auditoría de Labores Agrícolas\";\n\t\t\t} else {\n\t\t\t\tresponse = \"Lote \" + this.lote + \" - Labor \" + this.labor;\n\t\t\t}\n\n\t\t\treturn response;\n\t\t}\n\t};\n\n\t$scope.exportToExcel = function (tableId) {\n\t\t// ex: '#my-table'\n\t\tvar exportHref = Excel.tableToExcel(tableId, 'sheet name');\n\t\t$timeout(function () {\n\t\t\tlocation.href = exportHref;\n\t\t}, 100); // trigger download\n\t};\n\n\t$scope.createPDF = function () {\n\t\thtml2canvas($(\"#reportes_all\"), {\n\t\t\tonrendered: function onrendered(canvas) {\n\t\t\t\t;\n\t\t\t\tvar img = canvas.toDataURL(\"image/png\"),\n\t\t\t\t    doc = new jsPDF(\"p\", \"mm\", \"a4\");\n\t\t\t\tdoc.addImage(img, 'PNG', 0, 0, 210, 300);\n\t\t\t\tdoc.save('ficha.pdf');\n\t\t\t\t// $(\".page-bar\").css('display', '');\n\t\t\t}\n\t\t});\n\t};\n\n\t$scope.demoFromHTML = function () {\n\t\t// $(\".page-bar\").css('display', 'none');\n\t\t$scope.createPDF();\n\t};\n\n\t$scope.exportToPdf = function (tableId) {\n\t\t$(\".actions\").css('display', 'none');\n\t\tvar pdf = new jsPDF(\"p\", \"mm\", \"a4\");\n\t\t// source can be HTML-formatted string, or a reference\n\t\t// to an actual DOM element from which the text will be scraped.\n\t\tsource = $(tableId)[0];\n\n\t\t// we support special element handlers. Register them with jQuery-style\n\t\t// ID selector for either ID or node name. (\"#iAmID\", \"div\", \"span\" etc.)\n\t\t// There is no support for any other type of selectors\n\t\t// (class, of compound) at this time.\n\t\tspecialElementHandlers = {\n\t\t\t// element with id of \"bypass\" - jQuery style selector\n\t\t\t'#bypassme': function bypassme(element, renderer) {\n\t\t\t\t// true = \"handled elsewhere, bypass text extraction\"\n\t\t\t\treturn true;\n\t\t\t}\n\t\t};\n\t\tmargins = {\n\t\t\ttop: 20,\n\t\t\tbottom: 20,\n\t\t\tleft: 5,\n\t\t\twidth: 80\n\t\t};\n\t\t// all coords and widths are in jsPDF instance's declared units\n\t\t// 'inches' in this case\n\t\tpdf.fromHTML(source, // HTML string or DOM elem ref.\n\t\tmargins.left, // x coord\n\t\tmargins.top, { // y coord\n\t\t\t'width': margins.width, // max width of content on PDF\n\t\t\t'elementHandlers': specialElementHandlers\n\t\t}, function (dispose) {\n\t\t\t// dispose: object with X, Y of the last line add to the PDF\n\t\t\t//          this allow the insertion of new lines after html\n\t\t\t$(\".actions\").css('display', '');\n\t\t\tpdf.save(tableId + '.pdf');\n\t\t}, margins);\n\t};\n\n\t$scope.init = function () {\n\t\t$interval($scope.loadExternal, $scope.interval);\n\t};\n\t$scope.$watch('search', function () {});\n\t$scope.changeRangeDate = function (data) {\n\t\tif (data) {\n\t\t\t$scope.wizardStep.params.fecha_inicial = data.hasOwnProperty(\"first_date\") ? data.first_date : $scope.wizardStep.params.fecha_inicial;\n\t\t\t$scope.wizardStep.params.fecha_final = data.hasOwnProperty(\"second_date\") ? data.second_date : $scope.wizardStep.params.fecha_final;\n\t\t\t$scope.loadExternal($scope.modo);\n\t\t}\n\t};\n\n\t$scope.changeStep = function (step, idZona, idFinca, idLote, idLabor, idZonaLabor, idFincaLabor, idLoteLabor) {\n\t\t$scope.wizardStep.step = step;\n\t\t$scope.wizardStep.params.idZona = idZona || 0;\n\t\t$scope.wizardStep.params.idFinca = idFinca || 0;\n\t\t$scope.wizardStep.params.idLote = idLote || 0;\n\t\t$scope.wizardStep.params.idLabor = idLabor || 0;\n\t\t$scope.wizardStep.params.idZonaLabor = idZonaLabor || 0;\n\t\t$scope.wizardStep.params.idFincaLabor = idFincaLabor || 0;\n\t\t$scope.wizardStep.params.idLoteLabor = idLoteLabor || 0;\n\t\t// $scope.tittles.lote = lote || 'Todos';\n\t\t// $scope.tittles.labor = labor || 'Todos';\n\t\t// $scope.wizardStep.templatePath[step] += \"?\"+Math.random();\n\t};\n\n\t$scope.loadExternal = function (modo) {\n\t\tif ($scope.wizardStep.step != 0) $scope.startGallery = true;\n\t\tif ($scope.wizardStep.path[$scope.wizardStep.step] != \"\") {\n\t\t\tvar data = $scope.wizardStep.params;\n\n\t\t\tclient.post($scope.wizardStep.path[$scope.wizardStep.step], $scope.getDetails, data);\n\t\t}\n\t};\n\n\t$scope.initGrafica = function (id, options) {\n\t\tsetTimeout(function () {\n\t\t\tvar data = getOptionsGraficaReact(id, options);\n\t\t\tReactDOM.render(React.createElement(Historica, data), document.getElementById(id));\n\t\t}, 250);\n\t};\n\n\t$scope.initBarras = function (id, options) {\n\t\tsetTimeout(function () {\n\t\t\tReactDOM.render(React.createElement(Barras, options), document.getElementById(id));\n\t\t}, 250);\n\t};\n\n\t$scope.initPastel = function (id, series) {\n\t\tvar legends = [];\n\t\tvar newSeries = [];\n\t\tseries.data.map(function (val, key) {\n\t\t\tnewSeries.push({\n\t\t\t\tlabel: val.label,\n\t\t\t\tvalue: parseFloat(val.value)\n\t\t\t});\n\t\t\tif (legends.indexOf(val.label) != -1) legends.push(val.label);\n\t\t});\n\t\tsetTimeout(function () {\n\t\t\tdata = {\n\t\t\t\tdata: newSeries,\n\t\t\t\tnameseries: \"Pastel\",\n\t\t\t\tlegend: legends,\n\t\t\t\ttitulo: \"\",\n\t\t\t\tid: id\n\t\t\t};\n\t\t\tReactDOM.render(React.createElement(Pastel, data), document.getElementById(id));\n\t\t}, 250);\n\t};\n\n\t$scope.clearCache = function (modo) {\n\t\t$scope.modo = modo || \"mes\";\n\t\t$scope.getDetails($scope.cache, function () {});\n\t};\n\n\t$scope.getDetails = function (r, b) {\n\t\tb();\n\t\tif (r) {\n\t\t\tif ($scope.wizardStep.step <= 4) {\n\t\t\t\tvar options = angular.copy(r.causas) || {};\n\t\t\t\tvar OptionsPrincipal = r.principal || {};\n\t\t\t\t$scope.id_company = r.id_company;\n\t\t\t\t$scope.wizardData.data = angular.copy(r.causas) || [];\n\n\t\t\t\t$scope.wizardData.main = r.main || [];\n\t\t\t\t$scope.wizardData.week = r.week || [];\n\t\t\t\t$scope.initGrafica(r.idStepName == 'labores' ? \"echarts_bar_causa_principal\" : \"echarts_bar\", r.main);\n\n\t\t\t\t$scope.wizardData.lote_name = r.lote_name || [];\n\t\t\t\t$scope.wizardData.data_labores = r.data_labores || [];\n\t\t\t\t$scope.wizardData.data_labores_lotes = r.data_labores_lotes || [];\n\t\t\t\t$scope.umbrales = r.umbrals || {};\n\t\t\t\toptions.id_company = parseInt(r.id_company);\n\n\t\t\t\tif (options) {\n\t\t\t\t\toptions.data_labores = {};\n\t\t\t\t\tvar promedio_tipo = Object.keys($scope.wizardData.data_labores);\n\t\t\t\t\tvar data = $scope.wizardData.data_labores.amap(function (data) {\n\t\t\t\t\t\tangular.extend(options.data_labores, data);\n\t\t\t\t\t});\n\t\t\t\t\toptions.main = $scope.wizardData.main;\n\t\t\t\t\toptions.lote_name = $scope.wizardData.lote_name;\n\t\t\t\t\toptions.labor_name = r.labor_name || \"\";\n\t\t\t\t}\n\t\t\t\tif (r.hasOwnProperty(\"causas\") && r.causas.hasOwnProperty(\"lote\") && r.causas.lote.hasOwnProperty(\"labores\")) {\n\t\t\t\t\tvar loteLabores = {};\n\t\t\t\t\tvar copyCausas = angular.copy($scope.wizardData.data.lote.labores);\n\t\t\t\t\tvar data = copyCausas.amap(function (data) {\n\t\t\t\t\t\tangular.extend(loteLabores, data);\n\t\t\t\t\t});\n\t\t\t\t\toptions.lote.labores = angular.copy(loteLabores);\n\t\t\t\t\t$scope.wizardData.data = r.causas || [];\n\t\t\t\t}\n\t\t\t\tif (r.labores) {\n\t\t\t\t\tvar labores = r.labores.filter(function (item, pos) {\n\t\t\t\t\t\treturn r.labores.indexOf(item) == pos;\n\t\t\t\t\t});\n\n\t\t\t\t\t$scope.wizardData.labores = labores;\n\t\t\t\t}\n\t\t\t\tif (r.hasOwnProperty(\"causas\") && r.causas.hasOwnProperty(\"zonas\")) {\n\t\t\t\t\toptions.zonas = {\n\t\t\t\t\t\tseries: [],\n\t\t\t\t\t\tlabel: []\n\t\t\t\t\t};\n\t\t\t\t\tvar zonas = $scope.wizardData.data.zonas;\n\t\t\t\t\tfor (var i in zonas) {\n\t\t\t\t\t\tif (zonas[i].hasOwnProperty(\"nombre\")) {\n\t\t\t\t\t\t\toptions.zonas.label.push(zonas[i].nombre);\n\t\t\t\t\t\t\toptions.zonas.series.push({\n\t\t\t\t\t\t\t\tname: zonas[i].nombre,\n\t\t\t\t\t\t\t\ttype: \"bar\",\n\t\t\t\t\t\t\t\tdata: [$filter('number')(zonas[i].promedio, 2)]\n\t\t\t\t\t\t\t});\n\t\t\t\t\t\t}\n\t\t\t\t\t}\n\t\t\t\t}\n\t\t\t\tif (r.hasOwnProperty(\"causas\") && r.causas.hasOwnProperty(\"fincas\")) {\n\t\t\t\t\toptions.fincas = {\n\t\t\t\t\t\tdata: [],\n\t\t\t\t\t\tid: \"echarts_bar_lotes\"\n\t\t\t\t\t};\n\t\t\t\t\tvar fincas = $scope.wizardData.data.fincas;\n\t\t\t\t\tfor (var i in fincas) {\n\t\t\t\t\t\tif (fincas[i].hasOwnProperty(\"nombre\")) {\n\t\t\t\t\t\t\toptions.fincas.data.push({\n\t\t\t\t\t\t\t\tlabel: fincas[i].nombre,\n\t\t\t\t\t\t\t\tvalue: $filter('number')(fincas[i].promedio, 2)\n\t\t\t\t\t\t\t});\n\t\t\t\t\t\t}\n\t\t\t\t\t}\n\t\t\t\t\t//React Barras\n\t\t\t\t\t$scope.initBarras(\"echarts_bar_lotes\", options.fincas);\n\t\t\t\t}\n\t\t\t\tif (r.hasOwnProperty(\"causas\") && r.causas.hasOwnProperty(\"lote\")) {\n\t\t\t\t\toptions.labores_lotes = {\n\t\t\t\t\t\tdata: [],\n\t\t\t\t\t\tid: \"echarts_bar_labores_lotes\"\n\t\t\t\t\t};\n\t\t\t\t\tvar lote = $scope.wizardData.data.lote;\n\t\t\t\t\tfor (var i in lote.labores) {\n\t\t\t\t\t\tfor (var j in lote.labores[i]) {\n\t\t\t\t\t\t\tif (lote.labores[i][j].hasOwnProperty(\"lote\")) {\n\t\t\t\t\t\t\t\toptions.labores_lotes.data.push({\n\t\t\t\t\t\t\t\t\tlabel: j,\n\t\t\t\t\t\t\t\t\tvalue: $filter('number')(lote.labores[i][j].promedio, 2)\n\t\t\t\t\t\t\t\t});\n\t\t\t\t\t\t\t}\n\t\t\t\t\t\t}\n\t\t\t\t\t}\n\t\t\t\t\t$scope.initBarras(\"echarts_bar_labores_lotes\", options.labores_lotes);\n\t\t\t\t}\n\t\t\t\tif (r.hasOwnProperty(\"causas\") && r.idStepName == 'labores') {\n\t\t\t\t\toptions.data_causas = {\n\t\t\t\t\t\tdata: [],\n\t\t\t\t\t\tid: \"echarts_bar_lotes_labor\"\n\t\t\t\t\t};\n\t\t\t\t\tr.causas.map(function (value, index) {\n\t\t\t\t\t\tif (value.porcentaje2 > 0) {\n\t\t\t\t\t\t\toptions.data_causas.data.push({\n\t\t\t\t\t\t\t\tlabel: value.causa,\n\t\t\t\t\t\t\t\tvalue: value.porcentaje2\n\t\t\t\t\t\t\t});\n\t\t\t\t\t\t}\n\t\t\t\t\t});\n\t\t\t\t\t$scope.initPastel(\"echarts_bar_lotes_labor\", options.data_causas);\n\t\t\t\t}\n\t\t\t\tif (r.hasOwnProperty(\"data_labores_zonas\")) {\n\t\t\t\t\t$scope.wizardData.data_labores_lotes = r.data_labores_zonas.zonas || [];\n\t\t\t\t}\n\t\t\t\tif (r.hasOwnProperty(\"data_labores_fincas\")) {\n\t\t\t\t\t$scope.wizardData.filterLabores = r.filterLabores || [];\n\t\t\t\t\t$scope.wizardData.data_labores_lotes = r.data_labores_fincas.fincas || [];\n\t\t\t\t}\n\t\t\t\tif (r.hasOwnProperty(\"data_labores_lotes\")) {\n\t\t\t\t\t$scope.wizardData.filterLabores = r.filterLabores || [];\n\t\t\t\t\t$scope.wizardData.data_labores_lotes = r.data_labores_lotes.lotes || [];\n\t\t\t\t\toptions.data_lotes = {\n\t\t\t\t\t\tdata: [],\n\t\t\t\t\t\tid: \"echarts_bar_lotes\"\n\t\t\t\t\t};\n\n\t\t\t\t\tObject.keys($scope.wizardData.data_labores_lotes).map(function (lote_key) {\n\t\t\t\t\t\tvar lote = $scope.wizardData.data_labores_lotes[lote_key];\n\t\t\t\t\t\tvar sum = 0;\n\t\t\t\t\t\tvar labores_keys = Object.keys(lote.labores);\n\t\t\t\t\t\tlabores_keys.map(function (labor_key) {\n\t\t\t\t\t\t\tvar labor = lote.labores[labor_key];\n\t\t\t\t\t\t\tsum += parseFloat(labor.value);\n\t\t\t\t\t\t});\n\t\t\t\t\t\toptions.data_lotes.data.push({\n\t\t\t\t\t\t\tlabel: lote.nombre,\n\t\t\t\t\t\t\tvalue: parseFloat(sum / labores_keys.length).toFixed(2)\n\t\t\t\t\t\t});\n\t\t\t\t\t});\n\t\t\t\t\t$scope.initBarras('echarts_bar_lotes', options.data_lotes);\n\t\t\t\t}\n\t\t\t\tif (r.hasOwnProperty(\"filterLabores\")) {\n\t\t\t\t\t$scope.wizardData.filterLabores = r.filterLabores || [];\n\t\t\t\t}\n\t\t\t\tif (r.hasOwnProperty(\"categories\")) {\n\t\t\t\t\toptions.categories = r.categories || [];\n\t\t\t\t}\n\t\t\t\tif (r.hasOwnProperty(\"zonas\") && r.zonas.hasOwnProperty(\"zonas\")) {\n\t\t\t\t\toptions.label_principal = r.zonas.zonas || [];\n\t\t\t\t\tif (r.hasOwnProperty(\"idStepName\")) {\n\t\t\t\t\t\tactual_graficas[r.idStepName] = r.zonas.zonas || [];\n\t\t\t\t\t}\n\t\t\t\t}\n\t\t\t\tif (r.hasOwnProperty(\"zonas\") && r.zonas.hasOwnProperty(\"fincas\")) {\n\t\t\t\t\tif (r.hasOwnProperty(\"idStepName\")) {\n\t\t\t\t\t\tactual_graficas[r.idStepName] = r.zonas.fincas || [];\n\t\t\t\t\t}\n\t\t\t\t\toptions.label_principal = r.zonas.fincas || [];\n\t\t\t\t}\n\t\t\t\tif (r.hasOwnProperty(\"type_labor\")) {\n\t\t\t\t\t$scope.wizardData.type_labor = r.type_labor;\n\t\t\t\t}\n\t\t\t\tif (r.hasOwnProperty(\"personal_type_labor\")) {\n\t\t\t\t\t$scope.wizardData.personal_type_labor = r.personal_type_labor;\n\t\t\t\t}\n\t\t\t\tif (r.hasOwnProperty(\"personal\")) {\n\t\t\t\t\toptions.personal = {\n\t\t\t\t\t\tdata: [],\n\t\t\t\t\t\tid: r.idStepName ? \"echarts_bar_operador\" : \"echarts_bar_supervisor\"\n\t\t\t\t\t};\n\t\t\t\t\tvar personal = r.personal;\n\t\t\t\t\tfor (var i in personal) {\n\t\t\t\t\t\tif (personal[i].hasOwnProperty(\"personal\")) {\n\t\t\t\t\t\t\toptions.personal.data.push({\n\t\t\t\t\t\t\t\tlabel: personal[i].personal,\n\t\t\t\t\t\t\t\tvalue: $filter('number')(personal[i].promedio, 2)\n\t\t\t\t\t\t\t});\n\t\t\t\t\t\t}\n\t\t\t\t\t}\n\t\t\t\t\t$scope.initBarras(options.personal.id, options.personal);\n\t\t\t\t}\n\t\t\t\tif (r.photos) {\n\t\t\t\t\t$scope.wizardData.photos = r.photos || [];\n\t\t\t\t\tif ($scope.startGallery) {\n\t\t\t\t\t\tsetTimeout(function () {\n\t\t\t\t\t\t\t$scope.startGallery = false;\n\t\t\t\t\t\t\t$('#js-grid-lightbox-gallery').cubeportfolio({\n\t\t\t\t\t\t\t\tfilters: '#js-filters-lightbox-gallery1, #js-filters-lightbox-gallery2',\n\t\t\t\t\t\t\t\tloadMore: '#js-loadMore-lightbox-gallery',\n\t\t\t\t\t\t\t\tloadMoreAction: 'click',\n\t\t\t\t\t\t\t\tlayoutMode: 'grid',\n\t\t\t\t\t\t\t\tmediaQueries: [{\n\t\t\t\t\t\t\t\t\twidth: 1500,\n\t\t\t\t\t\t\t\t\tcols: 5\n\t\t\t\t\t\t\t\t}, {\n\t\t\t\t\t\t\t\t\twidth: 1100,\n\t\t\t\t\t\t\t\t\tcols: 4\n\t\t\t\t\t\t\t\t}, {\n\t\t\t\t\t\t\t\t\twidth: 800,\n\t\t\t\t\t\t\t\t\tcols: 3\n\t\t\t\t\t\t\t\t}, {\n\t\t\t\t\t\t\t\t\twidth: 480,\n\t\t\t\t\t\t\t\t\tcols: 2\n\t\t\t\t\t\t\t\t}, {\n\t\t\t\t\t\t\t\t\twidth: 320,\n\t\t\t\t\t\t\t\t\tcols: 1\n\t\t\t\t\t\t\t\t}],\n\t\t\t\t\t\t\t\tdefaultFilter: '*',\n\t\t\t\t\t\t\t\tanimationType: 'rotateSides',\n\t\t\t\t\t\t\t\tgapHorizontal: 10,\n\t\t\t\t\t\t\t\tgapVertical: 10,\n\t\t\t\t\t\t\t\tgridAdjustment: 'responsive',\n\t\t\t\t\t\t\t\tcaption: 'zoom',\n\t\t\t\t\t\t\t\tdisplayType: 'sequentially',\n\t\t\t\t\t\t\t\tdisplayTypeSpeed: 100,\n\n\t\t\t\t\t\t\t\t// lightbox\n\t\t\t\t\t\t\t\tlightboxDelegate: '.cbp-lightbox',\n\t\t\t\t\t\t\t\tlightboxGallery: true,\n\t\t\t\t\t\t\t\tlightboxTitleSrc: 'data-title',\n\t\t\t\t\t\t\t\tlightboxCounter: '<div class=\"cbp-popup-lightbox-counter\">{{current}} of {{total}}</div>',\n\n\t\t\t\t\t\t\t\t// singlePageInline\n\t\t\t\t\t\t\t\tsinglePageInlineDelegate: '.cbp-singlePageInline',\n\t\t\t\t\t\t\t\tsinglePageInlinePosition: 'below',\n\t\t\t\t\t\t\t\tsinglePageInlineInFocus: true,\n\t\t\t\t\t\t\t\tsinglePageInlineCallback: function singlePageInlineCallback(url, element) {\n\t\t\t\t\t\t\t\t\t// to update singlePageInline content use the following method: this.updateSinglePageInline(yourContent)\n\t\t\t\t\t\t\t\t\tvar t = this;\n\n\t\t\t\t\t\t\t\t\t$.ajax({\n\t\t\t\t\t\t\t\t\t\turl: url,\n\t\t\t\t\t\t\t\t\t\ttype: 'GET',\n\t\t\t\t\t\t\t\t\t\tdataType: 'html',\n\t\t\t\t\t\t\t\t\t\ttimeout: 10000\n\t\t\t\t\t\t\t\t\t}).done(function (result) {\n\n\t\t\t\t\t\t\t\t\t\tt.updateSinglePageInline(result);\n\t\t\t\t\t\t\t\t\t}).fail(function () {\n\t\t\t\t\t\t\t\t\t\tt.updateSinglePageInline('AJAX Error! Please refresh the page!');\n\t\t\t\t\t\t\t\t\t});\n\t\t\t\t\t\t\t\t}\n\t\t\t\t\t\t\t});\n\t\t\t\t\t\t}, 900);\n\t\t\t\t\t}\n\t\t\t\t}\n\t\t\t\tif (!angular.equals({}, options.data_labores)) {\n\t\t\t\t\t/*----------  LABOR  ----------*/\n\t\t\t\t\tvar labor = [],\n\t\t\t\t\t    label = [],\n\t\t\t\t\t    value = [],\n\t\t\t\t\t    lowFinca = [],\n\t\t\t\t\t    data_grafica = { id: \"echarts_bar_labores\", data: [] };\n\t\t\t\t\tlabor = options.data_labores.amap(function (item) {\n\t\t\t\t\t\tlabel[new Number(item.promedio).toFixed(2)] = item.labor;\n\t\t\t\t\t\tlowFinca[new Number(item.promedio).toFixed(2)] = item.finca;\n\t\t\t\t\t\tvalue.push(item.promedio);\n\t\t\t\t\t\tdata_grafica.data.push({\n\t\t\t\t\t\t\tvalue: new Number(item.promedio).toFixed(2),\n\t\t\t\t\t\t\tlabel: item.labor\n\t\t\t\t\t\t});\n\t\t\t\t\t\treturn item.promedio;\n\t\t\t\t\t});\n\t\t\t\t\t//React Barras\n\t\t\t\t\t$scope.initBarras(\"echarts_bar_labores\", data_grafica);\n\n\t\t\t\t\tif ($scope.tags.labor.label == \"\") if (r.hasOwnProperty(\"tags\")) {\n\t\t\t\t\t\t//victor\n\t\t\t\t\t\t$scope.tags.labor.valor = r.tags.labor.VALUE;\n\t\t\t\t\t\t$scope.tags.labor.label = r.tags.labor.label;\n\t\t\t\t\t} else {\n\t\t\t\t\t\t//javi\n\t\t\t\t\t\t$scope.tags.labor.valor = value.amin().value;\n\t\t\t\t\t\t$scope.tags.labor.label = lowFinca[value.amin().value] + \".\" + label[value.amin().value];\n\t\t\t\t\t}\n\t\t\t\t\t/*----------  LABOR  ----------*/\n\n\t\t\t\t\t/*----------  LOTE  ----------*/\n\t\t\t\t\tif (r.hasOwnProperty(\"low_lote\")) {\n\t\t\t\t\t\tvar label = \"\";\n\t\t\t\t\t\tif ($scope.id_company == 5 || $scope.id_company == 8) {\n\t\t\t\t\t\t\tlabel = r.low_lote.zona + \". \" + r.low_lote.finca + \". \" + r.low_lote.lote;\n\t\t\t\t\t\t} else {\n\t\t\t\t\t\t\tlabel = r.low_lote.finca + \". \" + r.low_lote.lote;\n\t\t\t\t\t\t}\n\t\t\t\t\t\t$scope.tags.lote.label = label;\n\t\t\t\t\t\t$scope.tags.lote.valor = new Number(r.low_lote.prom).toFixed(2);\n\t\t\t\t\t} else {\n\t\t\t\t\t\tif (!r.hasOwnProperty(\"zonas\") && !r.zonas.hasOwnProperty(\"zonas\")) {\n\t\t\t\t\t\t\tvar lote = [],\n\t\t\t\t\t\t\t    lote_label = [],\n\t\t\t\t\t\t\t    lote_value = [];\n\t\t\t\t\t\t\tlote = r.causas.lote.amap(function (item) {\n\t\t\t\t\t\t\t\tlote_value.push($scope.getPromedio(item, 1));\n\t\t\t\t\t\t\t\treturn Object.keys(item);\n\t\t\t\t\t\t\t});\n\t\t\t\t\t\t\tlote_label = Object.keys(r.causas.lote);\n\t\t\t\t\t\t\t$scope.tags.lote.valor = lote_value.amin().value;\n\t\t\t\t\t\t\t$scope.tags.lote.label = lote_label[lote_value.amin().label];\n\t\t\t\t\t\t} else {}\n\t\t\t\t\t}\n\t\t\t\t\t/*----------  LOTE  ----------*/\n\n\t\t\t\t\t/*----------  PERSONAL  ----------*/\n\t\t\t\t\tif (r.hasOwnProperty(\"low_personal\")) {\n\t\t\t\t\t\tvar label = r.low_personal.finca + \". \" + r.low_personal.lote + \". \" + r.low_personal.personal;\n\t\t\t\t\t\t$scope.tags.operador.label = label;\n\t\t\t\t\t\t$scope.tags.operador.valor = new Number(r.low_personal.promedio).toFixed(2);\n\t\t\t\t\t}\n\n\t\t\t\t\t/*----------  PROMEDIO GENERAL  ----------*/\n\t\t\t\t\tif (r.hasOwnProperty(\"prom_general\")) {\n\t\t\t\t\t\t/*----------  AUDITORIA  ----------*/\n\t\t\t\t\t\t$scope.tags.auditoria.valor = new Number(r.prom_general.promedio).toFixed(2);\n\t\t\t\t\t}\n\n\t\t\t\t\tif (r.hasOwnProperty(\"operador\")) {\n\t\t\t\t\t\t/*----------  OPERADOR AGRICOLA  ----------*/\n\t\t\t\t\t\tvar labor_operador = [],\n\t\t\t\t\t\t    label_operador = [],\n\t\t\t\t\t\t    value_operador = [];\n\t\t\t\t\t\tlabor_operador = r.operador.amap(function (item) {\n\t\t\t\t\t\t\tlabel_operador.push(item.ResponsableLabor);\n\t\t\t\t\t\t\tvalue_operador.push(item.promedio / item.avg);\n\t\t\t\t\t\t\treturn item.promedio / item.avg;\n\t\t\t\t\t\t});\n\t\t\t\t\t\t$scope.tags.operador.valor = value_operador.amin().value;\n\t\t\t\t\t\t$scope.tags.operador.label = label_operador[value_operador.amin().label];\n\t\t\t\t\t\t/*----------  OPERADOR AGRICOLA  ----------*/\n\t\t\t\t\t}\n\n\t\t\t\t\tif (!r.hasOwnProperty(\"zonas\") && !r.zonas.hasOwnProperty(\"zonas\")) {\n\t\t\t\t\t\t/*----------  AUDITORIA  ----------*/\n\t\t\t\t\t\t$scope.tags.auditoria.valor = lote_value.avg();\n\t\t\t\t\t}\n\n\t\t\t\t\tsetTimeout(function () {\n\t\t\t\t\t\t$(\".counter_tags\").counterUp({\n\t\t\t\t\t\t\tdelay: 10,\n\t\t\t\t\t\t\ttime: 1000\n\t\t\t\t\t\t});\n\t\t\t\t\t}, 1000);\n\n\t\t\t\t\toptions.data_labores.labels = promedio_tipo;\n\t\t\t\t}\n\t\t\t}\n\n\t\t\tif ($scope.wizardStep.step > 4) {\n\t\t\t\tvar options = {};\n\t\t\t\tif (r.hasOwnProperty(\"causas\")) {\n\t\t\t\t\t$scope.wizardData.labores.causas = angular.copy(r.causas) || [];\n\t\t\t\t\toptions.options_porcentaje = angular.copy(r.causas) || [];\n\t\t\t\t}\n\t\t\t\tif (r.hasOwnProperty(\"zonas\")) {\n\t\t\t\t\t$scope.wizardData.labores.zonas = angular.copy(r.zonas) || [];\n\t\t\t\t\toptions.options_zonas = angular.copy(r.zonas) || [];\n\t\t\t\t}\n\t\t\t\tif (r.hasOwnProperty(\"fincas\")) {\n\t\t\t\t\t$scope.wizardData.labores.fincas = angular.copy(r.fincas) || [];\n\t\t\t\t\toptions.options_fincas = angular.copy(r.fincas) || [];\n\t\t\t\t}\n\t\t\t\tif (r.hasOwnProperty(\"lotes\")) {\n\t\t\t\t\t$scope.wizardData.labores.lotes = angular.copy(r.lotes) || [];\n\t\t\t\t\toptions.options_lotes = angular.copy(r.lotes) || [];\n\t\t\t\t}\n\t\t\t\tif (r.hasOwnProperty(\"labor_name\")) {\n\t\t\t\t\toptions.labor_name = angular.copy(r.labor_name) || \"LABOR\";\n\t\t\t\t}\n\t\t\t}\n\n\t\t\t$scope.cache = angular.copy(r) || {};\n\n\t\t\tif (r.hasOwnProperty(\"idStepName\")) {\n\t\t\t\t//mes\n\t\t\t\tvar tmp = {};\n\t\t\t\tif (r.hasOwnProperty(\"zonas_main\") && r.zonas_main.hasOwnProperty(\"months\")) {\n\t\t\t\t\ttmp = r.zonas_main.months;\n\t\t\t\t} else {\n\t\t\t\t\ttmp = r.zonas.months;\n\t\t\t\t}\n\t\t\t\tdata_graficas.mes[r.idStepName] = tmp;\n\n\t\t\t\t//auditoria\n\t\t\t\ttmp = {};\n\t\t\t\tif (r.hasOwnProperty(\"zonas_main\") && r.zonas_main.hasOwnProperty(\"months\")) {\n\t\t\t\t\ttmp = r.zonas_main.months;\n\t\t\t\t} else {\n\t\t\t\t\ttmp = r.zonas.months;\n\t\t\t\t}\n\t\t\t\tdata_graficas.auditoria[r.idStepName] = tmp;\n\n\t\t\t\t//semana\n\t\t\t\ttmp = {};\n\t\t\t\tif (r.hasOwnProperty(\"zonas_main\") && r.zonas_main.hasOwnProperty(\"weeks\")) {\n\t\t\t\t\ttmp = options.series = r.zonas_main.weeks;\n\t\t\t\t} else {\n\t\t\t\t\ttmp = options.series = r.zonas.weeks;\n\t\t\t\t}\n\t\t\t\tdata_graficas.semana[r.idStepName] = tmp;\n\t\t\t}\n\n\t\t\tif ($scope.modo == \"mes\") {\n\t\t\t\toptions.main = r.main;\n\t\t\t\toptions.categories = r.categories.months;\n\n\t\t\t\tif (r.hasOwnProperty(\"zonas\") && r.zonas.hasOwnProperty(\"labores\")) {\n\t\t\t\t\toptions.series_labores = r.zonas.labores.months;\n\t\t\t\t}\n\n\t\t\t\tif (r.hasOwnProperty(\"idStepName\")) {\n\t\t\t\t\tif (r.idStepName == \"lotes\") {\n\t\t\t\t\t\toptions.series = [];\n\t\t\t\t\t\toptions.series[r.nameFinca] = data_graficas.mes.fincas[r.nameFinca];\n\t\t\t\t\t} else if (r.idStepName == \"labores\") {\n\t\t\t\t\t\toptions.series = [];\n\t\t\t\t\t\toptions.series[r.nameFinca] = data_graficas.mes.fincas[r.nameFinca];\n\t\t\t\t\t\toptions.series[r.nameLote] = data_graficas.mes.lotes[r.nameLote];\n\t\t\t\t\t}\n\t\t\t\t}\n\n\t\t\t\tif (r.hasOwnProperty(\"zonas_main\") && r.zonas_main.hasOwnProperty(\"months\")) {\n\t\t\t\t\toptions.series = r.zonas_main.months;\n\t\t\t\t} else {\n\t\t\t\t\tif ($scope.isArray(options.series)) {\n\t\t\t\t\t\t$.each(r.zonas.months, function (key, value) {\n\t\t\t\t\t\t\toptions.series[key] = value;\n\t\t\t\t\t\t});\n\t\t\t\t\t} else {\n\t\t\t\t\t\toptions.series = r.zonas.months;\n\t\t\t\t\t}\n\t\t\t\t}\n\n\t\t\t\tif (r.hasOwnProperty(\"zonas_main\") && r.zonas_main.hasOwnProperty(\"mes\")) {\n\t\t\t\t\toptions.labor = r.zonas_main.mes;\n\t\t\t\t}\n\t\t\t}\n\t\t\tif ($scope.modo == \"auditoria\") {\n\t\t\t\toptions.main = r.auditoria;\n\t\t\t\toptions.categories = r.categories.months;\n\n\t\t\t\tif (r.hasOwnProperty(\"zonas\") && r.zonas.hasOwnProperty(\"labores\")) {\n\t\t\t\t\toptions.series_labores = r.zonas.labores.months;\n\t\t\t\t}\n\n\t\t\t\tif (r.hasOwnProperty(\"idStepName\")) {\n\t\t\t\t\tif (r.idStepName == \"lotes\") {\n\t\t\t\t\t\toptions.series = [];\n\t\t\t\t\t\toptions.series[r.nameFinca] = data_graficas.auditoria.fincas[r.nameFinca];\n\t\t\t\t\t} else if (r.idStepName == \"labores\") {\n\t\t\t\t\t\toptions.series = [];\n\t\t\t\t\t\toptions.series[r.nameFinca] = data_graficas.auditoria.fincas[r.nameFinca];\n\t\t\t\t\t\toptions.series[r.nameLote] = data_graficas.auditoria.lotes[r.nameLote];\n\t\t\t\t\t}\n\t\t\t\t}\n\n\t\t\t\tif (r.hasOwnProperty(\"zonas_main\") && r.zonas_main.hasOwnProperty(\"months\")) {\n\t\t\t\t\toptions.series = r.zonas_main.months;\n\t\t\t\t} else {\n\t\t\t\t\tif ($scope.isArray(options.series)) {\n\t\t\t\t\t\t$.each(r.zonas.months, function (key, value) {\n\t\t\t\t\t\t\toptions.series[key] = value;\n\t\t\t\t\t\t});\n\t\t\t\t\t} else {\n\t\t\t\t\t\toptions.series = r.zonas.months;\n\t\t\t\t\t}\n\t\t\t\t}\n\n\t\t\t\tif (r.hasOwnProperty(\"zonas_main\") && r.zonas_main.hasOwnProperty(\"mes\")) {\n\t\t\t\t\toptions.labor = r.zonas_main.mes;\n\t\t\t\t}\n\t\t\t}\n\t\t\tif ($scope.modo == \"semana\") {\n\t\t\t\toptions.main = r.week;\n\t\t\t\toptions.categories = r.categories.weeks;\n\n\t\t\t\tif (r.hasOwnProperty(\"zonas\") && r.zonas.hasOwnProperty(\"labores\")) {\n\t\t\t\t\toptions.series_labores = r.zonas.labores.weeks;\n\t\t\t\t}\n\n\t\t\t\tif (r.hasOwnProperty(\"idStepName\")) {\n\t\t\t\t\tif (r.idStepName == \"lotes\") {\n\t\t\t\t\t\toptions.series = [];\n\t\t\t\t\t\toptions.series[r.nameFinca] = data_graficas.semana.fincas[r.nameFinca];\n\t\t\t\t\t} else if (r.idStepName == \"labores\") {\n\t\t\t\t\t\toptions.series = [];\n\t\t\t\t\t\toptions.series[r.nameFinca] = data_graficas.semana.fincas[r.nameFinca];\n\t\t\t\t\t\toptions.series[r.nameLote] = data_graficas.semana.lotes[r.nameLote];\n\t\t\t\t\t}\n\t\t\t\t}\n\n\t\t\t\tif (r.hasOwnProperty(\"zonas_main\") && r.zonas_main.hasOwnProperty(\"weeks\")) {\n\t\t\t\t\toptions.series = r.zonas_main.weeks;\n\t\t\t\t} else {\n\t\t\t\t\tif ($scope.isArray(options.series)) {\n\t\t\t\t\t\t$.each(r.zonas.weeks, function (key, value) {\n\t\t\t\t\t\t\toptions.series[key] = value;\n\t\t\t\t\t\t});\n\t\t\t\t\t} else {\n\t\t\t\t\t\toptions.series = r.zonas.weeks;\n\t\t\t\t\t}\n\t\t\t\t}\n\n\t\t\t\tif (r.hasOwnProperty(\"zonas_main\") && r.zonas_main.hasOwnProperty(\"semana\")) {\n\t\t\t\t\toptions.labor = r.zonas_main.semana;\n\t\t\t\t}\n\t\t\t}\n\n\t\t\tif (r.hasOwnProperty(\"tittle\")) {\n\t\t\t\t$scope.status_tittle = r.tittle;\n\t\t\t}\n\n\t\t\tif ($scope.umbrales.hasOwnProperty(\"range_min\") && $scope.umbrales.hasOwnProperty(\"range_max\")) {\n\t\t\t\tumbral_range.min = $scope.umbrales.range_min;\n\t\t\t\tumbral_range.max = $scope.umbrales.range_max;\n\t\t\t}\n\n\t\t\t$scope.scrollAnimateTop();\n\t\t}\n\t};\n\n\t$scope.scrollAnimateTop = function () {\n\t\tvar body = angular.element(\"html, body\");\n\t\tbody.stop().animate({ scrollTop: 0 }, '500', 'swing', function () {});\n\t};\n\n\t$scope.getTotalsLabor = function (type) {\n\t\ttype = type || 1;\n\t\tvar data = $scope.wizardData.labores.causas;\n\t\tvar total_1 = 0;\n\t\tvar total_2 = 0;\n\t\tfor (var i = 0; i < data.length; i++) {\n\t\t\tif (!isNaN(parseFloat(data[i].porcentaje))) total_1 += parseFloat(data[i].porcentaje);\n\t\t\tif (!isNaN(parseFloat(data[i].porcentaje2))) total_2 += parseFloat(data[i].porcentaje2);\n\t\t}\n\t\treturn type == 1 ? total_1 : total_2;\n\t};\n\n\t$scope.getAvgTotalsStep2 = function (type_labor) {\n\t\tvar count = 0;\n\t\tvar promedio = 0;\n\t\tvar total = 0;\n\t\tfor (var key in type_labor) {\n\t\t\tcount++;\n\t\t\tpromedio += parseFloat(type_labor[key].promedio);\n\t\t}\n\t\ttotal = new Number(promedio / count).toFixed(2);\n\t\treturn total;\n\t};\n\n\t$scope.getAvgTotals = function (type_labor) {\n\t\tvar count = 0;\n\t\tvar promedio = 0;\n\t\tvar total = 0;\n\t\tfor (var key in type_labor) {\n\t\t\tcount++;\n\t\t\tpromedio += parseFloat(type_labor[key].promedio / type_labor[key].avg);\n\t\t}\n\t\ttotal = new Number(promedio / count).toFixed(2);\n\t\treturn total;\n\t};\n\n\t$scope.getTotals = function (type) {\n\t\ttype = type || 1;\n\t\tvar data = $scope.wizardData.data;\n\t\tvar total_1 = 0;\n\t\tvar total_2 = 0;\n\t\tfor (var i = 0; i < data.length; i++) {\n\t\t\tif (!isNaN(parseFloat(data[i].porcentaje))) total_1 += parseFloat(data[i].porcentaje);\n\t\t\tif (!isNaN(parseFloat(data[i].porcentaje2))) total_2 += parseFloat(data[i].porcentaje2);\n\t\t}\n\t\treturn type == 1 ? total_1 : total_2;\n\t};\n\n\t$scope.checks = function (porcentaje) {\n\t\tporcentaje = parseFloat(porcentaje);\n\t\tif (porcentaje >= parseFloat($scope.umbrales.green_umbral_1)) return 'fa fa-check font-green-jungle';else if (porcentaje >= parseFloat($scope.umbrales.yellow_umbral_1) && porcentaje <= parseFloat($scope.umbrales.yellow_umbral_2)) return 'fa fa-exclamation font-yellow-lemon';else return 'fa fa-close font-red-thunderbird';\n\t};\n\n\t$scope.checks2 = function (porcentaje) {\n\t\tporcentaje = parseFloat(porcentaje);\n\t\tif (porcentaje >= parseFloat($scope.umbrales.green_umbral_1) && porcentaje <= parseFloat($scope.umbrales.green_umbral_2)) return 'fa fa-check font-green-jungle';else if (porcentaje >= parseFloat($scope.umbrales.yellow_umbral_1) && porcentaje <= parseFloat($scope.umbrales.yellow_umbral_2)) return 'fa fa-exclamation font-yellow-lemon';else if (porcentaje > parseFloat($scope.umbrales.red_umbral_1) && porcentaje < parseFloat($scope.umbrales.red_umbral_2)) return 'fa fa-close font-red-thunderbird';else return '';\n\t};\n\n\t$scope.revision = function (porcentaje) {\n\t\t// alert(porcentaje);\n\t\tif (porcentaje >= parseFloat($scope.umbrales.green_umbral_1)) return 'green-jungle';else if (porcentaje >= parseFloat($scope.umbrales.yellow_umbral_1) && porcentaje <= parseFloat($scope.umbrales.yellow_umbral_2)) return 'yellow-lemon';else return 'red-thunderbird';\n\t};\n\n\t$scope.arrayToString = function (string) {\n\t\treturn string;\n\t};\n\n\t$scope.getPromedio = function (data, v) {\n\t\tvar count = 0;\n\t\tvar promedio = 0;\n\t\tvar total = 0;\n\t\tfor (var key in data.labores) {\n\t\t\tcount++;\n\t\t\tpromedio += parseFloat(data.labores[key].promedio);\n\t\t}\n\t\ttotal = parseFloat(parseFloat(promedio) / parseFloat(count));\n\t\treturn total;\n\t};\n\n\t$scope.getPromedioLote = function (data) {\n\t\tvar count = 0;\n\t\tvar promedio = 0;\n\t\tvar total = 0;\n\t\tfor (var key in data) {\n\t\t\tcount++;\n\t\t\tpromedio += parseFloat($scope.getPromedio(data[key]));\n\t\t}\n\t\ttotal = parseFloat(parseFloat(promedio) / parseFloat(count));\n\t\treturn total;\n\t};\n\n\t$scope.getPromedioLoteZona = function (data) {\n\t\tvar count = 0;\n\t\tvar promedio = 0;\n\t\tvar total = 0;\n\t\tfor (var key in data) {\n\t\t\tcount++;\n\t\t\tpromedio += parseFloat($scope.getPromedioLote(data[key]));\n\t\t}\n\t\ttotal = parseFloat(parseFloat(promedio) / parseFloat(count));\n\t\treturn total;\n\t};\n\n\t$scope.createProperty = function (data, key, value, index) {\n\t\tdata[key] = value;\n\t\treturn value;\n\t};\n\n\t$scope.isArray = function (value) {\n\t\tif (value) {\n\t\t\tif ((typeof value === \"undefined\" ? \"undefined\" : (0, _typeof3.default)(value)) === 'object') {\n\t\t\t\treturn Object.prototype.toString.call(value) == '[object Array]';\n\t\t\t}\n\t\t}\n\t\treturn false;\n\t};\n}]);\n\n//# sourceURL=webpack:///./js_modules/marlon/reportes.js?");

/***/ }),

/***/ "./node_modules/babel-runtime/core-js/symbol.js":
/*!******************************************************!*\
  !*** ./node_modules/babel-runtime/core-js/symbol.js ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("module.exports = { \"default\": __webpack_require__(/*! core-js/library/fn/symbol */ \"./node_modules/core-js/library/fn/symbol/index.js\"), __esModule: true };\n\n//# sourceURL=webpack:///./node_modules/babel-runtime/core-js/symbol.js?");

/***/ }),

/***/ "./node_modules/babel-runtime/core-js/symbol/iterator.js":
/*!***************************************************************!*\
  !*** ./node_modules/babel-runtime/core-js/symbol/iterator.js ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("module.exports = { \"default\": __webpack_require__(/*! core-js/library/fn/symbol/iterator */ \"./node_modules/core-js/library/fn/symbol/iterator.js\"), __esModule: true };\n\n//# sourceURL=webpack:///./node_modules/babel-runtime/core-js/symbol/iterator.js?");

/***/ }),

/***/ "./node_modules/babel-runtime/helpers/typeof.js":
/*!******************************************************!*\
  !*** ./node_modules/babel-runtime/helpers/typeof.js ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nexports.__esModule = true;\n\nvar _iterator = __webpack_require__(/*! ../core-js/symbol/iterator */ \"./node_modules/babel-runtime/core-js/symbol/iterator.js\");\n\nvar _iterator2 = _interopRequireDefault(_iterator);\n\nvar _symbol = __webpack_require__(/*! ../core-js/symbol */ \"./node_modules/babel-runtime/core-js/symbol.js\");\n\nvar _symbol2 = _interopRequireDefault(_symbol);\n\nvar _typeof = typeof _symbol2.default === \"function\" && typeof _iterator2.default === \"symbol\" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof _symbol2.default === \"function\" && obj.constructor === _symbol2.default && obj !== _symbol2.default.prototype ? \"symbol\" : typeof obj; };\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\nexports.default = typeof _symbol2.default === \"function\" && _typeof(_iterator2.default) === \"symbol\" ? function (obj) {\n  return typeof obj === \"undefined\" ? \"undefined\" : _typeof(obj);\n} : function (obj) {\n  return obj && typeof _symbol2.default === \"function\" && obj.constructor === _symbol2.default && obj !== _symbol2.default.prototype ? \"symbol\" : typeof obj === \"undefined\" ? \"undefined\" : _typeof(obj);\n};\n\n//# sourceURL=webpack:///./node_modules/babel-runtime/helpers/typeof.js?");

/***/ }),

/***/ "./node_modules/core-js/library/fn/symbol/index.js":
/*!*********************************************************!*\
  !*** ./node_modules/core-js/library/fn/symbol/index.js ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("__webpack_require__(/*! ../../modules/es6.symbol */ \"./node_modules/core-js/library/modules/es6.symbol.js\");\n__webpack_require__(/*! ../../modules/es6.object.to-string */ \"./node_modules/core-js/library/modules/es6.object.to-string.js\");\n__webpack_require__(/*! ../../modules/es7.symbol.async-iterator */ \"./node_modules/core-js/library/modules/es7.symbol.async-iterator.js\");\n__webpack_require__(/*! ../../modules/es7.symbol.observable */ \"./node_modules/core-js/library/modules/es7.symbol.observable.js\");\nmodule.exports = __webpack_require__(/*! ../../modules/_core */ \"./node_modules/core-js/library/modules/_core.js\").Symbol;\n\n\n//# sourceURL=webpack:///./node_modules/core-js/library/fn/symbol/index.js?");

/***/ }),

/***/ "./node_modules/core-js/library/fn/symbol/iterator.js":
/*!************************************************************!*\
  !*** ./node_modules/core-js/library/fn/symbol/iterator.js ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("__webpack_require__(/*! ../../modules/es6.string.iterator */ \"./node_modules/core-js/library/modules/es6.string.iterator.js\");\n__webpack_require__(/*! ../../modules/web.dom.iterable */ \"./node_modules/core-js/library/modules/web.dom.iterable.js\");\nmodule.exports = __webpack_require__(/*! ../../modules/_wks-ext */ \"./node_modules/core-js/library/modules/_wks-ext.js\").f('iterator');\n\n\n//# sourceURL=webpack:///./node_modules/core-js/library/fn/symbol/iterator.js?");

/***/ }),

/***/ "./node_modules/core-js/library/modules/_a-function.js":
/*!*************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_a-function.js ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = function (it) {\n  if (typeof it != 'function') throw TypeError(it + ' is not a function!');\n  return it;\n};\n\n\n//# sourceURL=webpack:///./node_modules/core-js/library/modules/_a-function.js?");

/***/ }),

/***/ "./node_modules/core-js/library/modules/_add-to-unscopables.js":
/*!*********************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_add-to-unscopables.js ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = function () { /* empty */ };\n\n\n//# sourceURL=webpack:///./node_modules/core-js/library/modules/_add-to-unscopables.js?");

/***/ }),

/***/ "./node_modules/core-js/library/modules/_an-object.js":
/*!************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_an-object.js ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var isObject = __webpack_require__(/*! ./_is-object */ \"./node_modules/core-js/library/modules/_is-object.js\");\nmodule.exports = function (it) {\n  if (!isObject(it)) throw TypeError(it + ' is not an object!');\n  return it;\n};\n\n\n//# sourceURL=webpack:///./node_modules/core-js/library/modules/_an-object.js?");

/***/ }),

/***/ "./node_modules/core-js/library/modules/_array-includes.js":
/*!*****************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_array-includes.js ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// false -> Array#indexOf\n// true  -> Array#includes\nvar toIObject = __webpack_require__(/*! ./_to-iobject */ \"./node_modules/core-js/library/modules/_to-iobject.js\");\nvar toLength = __webpack_require__(/*! ./_to-length */ \"./node_modules/core-js/library/modules/_to-length.js\");\nvar toAbsoluteIndex = __webpack_require__(/*! ./_to-absolute-index */ \"./node_modules/core-js/library/modules/_to-absolute-index.js\");\nmodule.exports = function (IS_INCLUDES) {\n  return function ($this, el, fromIndex) {\n    var O = toIObject($this);\n    var length = toLength(O.length);\n    var index = toAbsoluteIndex(fromIndex, length);\n    var value;\n    // Array#includes uses SameValueZero equality algorithm\n    // eslint-disable-next-line no-self-compare\n    if (IS_INCLUDES && el != el) while (length > index) {\n      value = O[index++];\n      // eslint-disable-next-line no-self-compare\n      if (value != value) return true;\n    // Array#indexOf ignores holes, Array#includes - not\n    } else for (;length > index; index++) if (IS_INCLUDES || index in O) {\n      if (O[index] === el) return IS_INCLUDES || index || 0;\n    } return !IS_INCLUDES && -1;\n  };\n};\n\n\n//# sourceURL=webpack:///./node_modules/core-js/library/modules/_array-includes.js?");

/***/ }),

/***/ "./node_modules/core-js/library/modules/_cof.js":
/*!******************************************************!*\
  !*** ./node_modules/core-js/library/modules/_cof.js ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("var toString = {}.toString;\n\nmodule.exports = function (it) {\n  return toString.call(it).slice(8, -1);\n};\n\n\n//# sourceURL=webpack:///./node_modules/core-js/library/modules/_cof.js?");

/***/ }),

/***/ "./node_modules/core-js/library/modules/_core.js":
/*!*******************************************************!*\
  !*** ./node_modules/core-js/library/modules/_core.js ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("var core = module.exports = { version: '2.5.7' };\nif (typeof __e == 'number') __e = core; // eslint-disable-line no-undef\n\n\n//# sourceURL=webpack:///./node_modules/core-js/library/modules/_core.js?");

/***/ }),

/***/ "./node_modules/core-js/library/modules/_ctx.js":
/*!******************************************************!*\
  !*** ./node_modules/core-js/library/modules/_ctx.js ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// optional / simple context binding\nvar aFunction = __webpack_require__(/*! ./_a-function */ \"./node_modules/core-js/library/modules/_a-function.js\");\nmodule.exports = function (fn, that, length) {\n  aFunction(fn);\n  if (that === undefined) return fn;\n  switch (length) {\n    case 1: return function (a) {\n      return fn.call(that, a);\n    };\n    case 2: return function (a, b) {\n      return fn.call(that, a, b);\n    };\n    case 3: return function (a, b, c) {\n      return fn.call(that, a, b, c);\n    };\n  }\n  return function (/* ...args */) {\n    return fn.apply(that, arguments);\n  };\n};\n\n\n//# sourceURL=webpack:///./node_modules/core-js/library/modules/_ctx.js?");

/***/ }),

/***/ "./node_modules/core-js/library/modules/_defined.js":
/*!**********************************************************!*\
  !*** ./node_modules/core-js/library/modules/_defined.js ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("// 7.2.1 RequireObjectCoercible(argument)\nmodule.exports = function (it) {\n  if (it == undefined) throw TypeError(\"Can't call method on  \" + it);\n  return it;\n};\n\n\n//# sourceURL=webpack:///./node_modules/core-js/library/modules/_defined.js?");

/***/ }),

/***/ "./node_modules/core-js/library/modules/_descriptors.js":
/*!**************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_descriptors.js ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Thank's IE8 for his funny defineProperty\nmodule.exports = !__webpack_require__(/*! ./_fails */ \"./node_modules/core-js/library/modules/_fails.js\")(function () {\n  return Object.defineProperty({}, 'a', { get: function () { return 7; } }).a != 7;\n});\n\n\n//# sourceURL=webpack:///./node_modules/core-js/library/modules/_descriptors.js?");

/***/ }),

/***/ "./node_modules/core-js/library/modules/_dom-create.js":
/*!*************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_dom-create.js ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var isObject = __webpack_require__(/*! ./_is-object */ \"./node_modules/core-js/library/modules/_is-object.js\");\nvar document = __webpack_require__(/*! ./_global */ \"./node_modules/core-js/library/modules/_global.js\").document;\n// typeof document.createElement is 'object' in old IE\nvar is = isObject(document) && isObject(document.createElement);\nmodule.exports = function (it) {\n  return is ? document.createElement(it) : {};\n};\n\n\n//# sourceURL=webpack:///./node_modules/core-js/library/modules/_dom-create.js?");

/***/ }),

/***/ "./node_modules/core-js/library/modules/_enum-bug-keys.js":
/*!****************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_enum-bug-keys.js ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("// IE 8- don't enum bug keys\nmodule.exports = (\n  'constructor,hasOwnProperty,isPrototypeOf,propertyIsEnumerable,toLocaleString,toString,valueOf'\n).split(',');\n\n\n//# sourceURL=webpack:///./node_modules/core-js/library/modules/_enum-bug-keys.js?");

/***/ }),

/***/ "./node_modules/core-js/library/modules/_enum-keys.js":
/*!************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_enum-keys.js ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// all enumerable object keys, includes symbols\nvar getKeys = __webpack_require__(/*! ./_object-keys */ \"./node_modules/core-js/library/modules/_object-keys.js\");\nvar gOPS = __webpack_require__(/*! ./_object-gops */ \"./node_modules/core-js/library/modules/_object-gops.js\");\nvar pIE = __webpack_require__(/*! ./_object-pie */ \"./node_modules/core-js/library/modules/_object-pie.js\");\nmodule.exports = function (it) {\n  var result = getKeys(it);\n  var getSymbols = gOPS.f;\n  if (getSymbols) {\n    var symbols = getSymbols(it);\n    var isEnum = pIE.f;\n    var i = 0;\n    var key;\n    while (symbols.length > i) if (isEnum.call(it, key = symbols[i++])) result.push(key);\n  } return result;\n};\n\n\n//# sourceURL=webpack:///./node_modules/core-js/library/modules/_enum-keys.js?");

/***/ }),

/***/ "./node_modules/core-js/library/modules/_export.js":
/*!*********************************************************!*\
  !*** ./node_modules/core-js/library/modules/_export.js ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var global = __webpack_require__(/*! ./_global */ \"./node_modules/core-js/library/modules/_global.js\");\nvar core = __webpack_require__(/*! ./_core */ \"./node_modules/core-js/library/modules/_core.js\");\nvar ctx = __webpack_require__(/*! ./_ctx */ \"./node_modules/core-js/library/modules/_ctx.js\");\nvar hide = __webpack_require__(/*! ./_hide */ \"./node_modules/core-js/library/modules/_hide.js\");\nvar has = __webpack_require__(/*! ./_has */ \"./node_modules/core-js/library/modules/_has.js\");\nvar PROTOTYPE = 'prototype';\n\nvar $export = function (type, name, source) {\n  var IS_FORCED = type & $export.F;\n  var IS_GLOBAL = type & $export.G;\n  var IS_STATIC = type & $export.S;\n  var IS_PROTO = type & $export.P;\n  var IS_BIND = type & $export.B;\n  var IS_WRAP = type & $export.W;\n  var exports = IS_GLOBAL ? core : core[name] || (core[name] = {});\n  var expProto = exports[PROTOTYPE];\n  var target = IS_GLOBAL ? global : IS_STATIC ? global[name] : (global[name] || {})[PROTOTYPE];\n  var key, own, out;\n  if (IS_GLOBAL) source = name;\n  for (key in source) {\n    // contains in native\n    own = !IS_FORCED && target && target[key] !== undefined;\n    if (own && has(exports, key)) continue;\n    // export native or passed\n    out = own ? target[key] : source[key];\n    // prevent global pollution for namespaces\n    exports[key] = IS_GLOBAL && typeof target[key] != 'function' ? source[key]\n    // bind timers to global for call from export context\n    : IS_BIND && own ? ctx(out, global)\n    // wrap global constructors for prevent change them in library\n    : IS_WRAP && target[key] == out ? (function (C) {\n      var F = function (a, b, c) {\n        if (this instanceof C) {\n          switch (arguments.length) {\n            case 0: return new C();\n            case 1: return new C(a);\n            case 2: return new C(a, b);\n          } return new C(a, b, c);\n        } return C.apply(this, arguments);\n      };\n      F[PROTOTYPE] = C[PROTOTYPE];\n      return F;\n    // make static versions for prototype methods\n    })(out) : IS_PROTO && typeof out == 'function' ? ctx(Function.call, out) : out;\n    // export proto methods to core.%CONSTRUCTOR%.methods.%NAME%\n    if (IS_PROTO) {\n      (exports.virtual || (exports.virtual = {}))[key] = out;\n      // export proto methods to core.%CONSTRUCTOR%.prototype.%NAME%\n      if (type & $export.R && expProto && !expProto[key]) hide(expProto, key, out);\n    }\n  }\n};\n// type bitmap\n$export.F = 1;   // forced\n$export.G = 2;   // global\n$export.S = 4;   // static\n$export.P = 8;   // proto\n$export.B = 16;  // bind\n$export.W = 32;  // wrap\n$export.U = 64;  // safe\n$export.R = 128; // real proto method for `library`\nmodule.exports = $export;\n\n\n//# sourceURL=webpack:///./node_modules/core-js/library/modules/_export.js?");

/***/ }),

/***/ "./node_modules/core-js/library/modules/_fails.js":
/*!********************************************************!*\
  !*** ./node_modules/core-js/library/modules/_fails.js ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = function (exec) {\n  try {\n    return !!exec();\n  } catch (e) {\n    return true;\n  }\n};\n\n\n//# sourceURL=webpack:///./node_modules/core-js/library/modules/_fails.js?");

/***/ }),

/***/ "./node_modules/core-js/library/modules/_global.js":
/*!*********************************************************!*\
  !*** ./node_modules/core-js/library/modules/_global.js ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("// https://github.com/zloirock/core-js/issues/86#issuecomment-115759028\nvar global = module.exports = typeof window != 'undefined' && window.Math == Math\n  ? window : typeof self != 'undefined' && self.Math == Math ? self\n  // eslint-disable-next-line no-new-func\n  : Function('return this')();\nif (typeof __g == 'number') __g = global; // eslint-disable-line no-undef\n\n\n//# sourceURL=webpack:///./node_modules/core-js/library/modules/_global.js?");

/***/ }),

/***/ "./node_modules/core-js/library/modules/_has.js":
/*!******************************************************!*\
  !*** ./node_modules/core-js/library/modules/_has.js ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("var hasOwnProperty = {}.hasOwnProperty;\nmodule.exports = function (it, key) {\n  return hasOwnProperty.call(it, key);\n};\n\n\n//# sourceURL=webpack:///./node_modules/core-js/library/modules/_has.js?");

/***/ }),

/***/ "./node_modules/core-js/library/modules/_hide.js":
/*!*******************************************************!*\
  !*** ./node_modules/core-js/library/modules/_hide.js ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var dP = __webpack_require__(/*! ./_object-dp */ \"./node_modules/core-js/library/modules/_object-dp.js\");\nvar createDesc = __webpack_require__(/*! ./_property-desc */ \"./node_modules/core-js/library/modules/_property-desc.js\");\nmodule.exports = __webpack_require__(/*! ./_descriptors */ \"./node_modules/core-js/library/modules/_descriptors.js\") ? function (object, key, value) {\n  return dP.f(object, key, createDesc(1, value));\n} : function (object, key, value) {\n  object[key] = value;\n  return object;\n};\n\n\n//# sourceURL=webpack:///./node_modules/core-js/library/modules/_hide.js?");

/***/ }),

/***/ "./node_modules/core-js/library/modules/_html.js":
/*!*******************************************************!*\
  !*** ./node_modules/core-js/library/modules/_html.js ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var document = __webpack_require__(/*! ./_global */ \"./node_modules/core-js/library/modules/_global.js\").document;\nmodule.exports = document && document.documentElement;\n\n\n//# sourceURL=webpack:///./node_modules/core-js/library/modules/_html.js?");

/***/ }),

/***/ "./node_modules/core-js/library/modules/_ie8-dom-define.js":
/*!*****************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_ie8-dom-define.js ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("module.exports = !__webpack_require__(/*! ./_descriptors */ \"./node_modules/core-js/library/modules/_descriptors.js\") && !__webpack_require__(/*! ./_fails */ \"./node_modules/core-js/library/modules/_fails.js\")(function () {\n  return Object.defineProperty(__webpack_require__(/*! ./_dom-create */ \"./node_modules/core-js/library/modules/_dom-create.js\")('div'), 'a', { get: function () { return 7; } }).a != 7;\n});\n\n\n//# sourceURL=webpack:///./node_modules/core-js/library/modules/_ie8-dom-define.js?");

/***/ }),

/***/ "./node_modules/core-js/library/modules/_iobject.js":
/*!**********************************************************!*\
  !*** ./node_modules/core-js/library/modules/_iobject.js ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// fallback for non-array-like ES3 and non-enumerable old V8 strings\nvar cof = __webpack_require__(/*! ./_cof */ \"./node_modules/core-js/library/modules/_cof.js\");\n// eslint-disable-next-line no-prototype-builtins\nmodule.exports = Object('z').propertyIsEnumerable(0) ? Object : function (it) {\n  return cof(it) == 'String' ? it.split('') : Object(it);\n};\n\n\n//# sourceURL=webpack:///./node_modules/core-js/library/modules/_iobject.js?");

/***/ }),

/***/ "./node_modules/core-js/library/modules/_is-array.js":
/*!***********************************************************!*\
  !*** ./node_modules/core-js/library/modules/_is-array.js ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// 7.2.2 IsArray(argument)\nvar cof = __webpack_require__(/*! ./_cof */ \"./node_modules/core-js/library/modules/_cof.js\");\nmodule.exports = Array.isArray || function isArray(arg) {\n  return cof(arg) == 'Array';\n};\n\n\n//# sourceURL=webpack:///./node_modules/core-js/library/modules/_is-array.js?");

/***/ }),

/***/ "./node_modules/core-js/library/modules/_is-object.js":
/*!************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_is-object.js ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = function (it) {\n  return typeof it === 'object' ? it !== null : typeof it === 'function';\n};\n\n\n//# sourceURL=webpack:///./node_modules/core-js/library/modules/_is-object.js?");

/***/ }),

/***/ "./node_modules/core-js/library/modules/_iter-create.js":
/*!**************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_iter-create.js ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\nvar create = __webpack_require__(/*! ./_object-create */ \"./node_modules/core-js/library/modules/_object-create.js\");\nvar descriptor = __webpack_require__(/*! ./_property-desc */ \"./node_modules/core-js/library/modules/_property-desc.js\");\nvar setToStringTag = __webpack_require__(/*! ./_set-to-string-tag */ \"./node_modules/core-js/library/modules/_set-to-string-tag.js\");\nvar IteratorPrototype = {};\n\n// 25.1.2.1.1 %IteratorPrototype%[@@iterator]()\n__webpack_require__(/*! ./_hide */ \"./node_modules/core-js/library/modules/_hide.js\")(IteratorPrototype, __webpack_require__(/*! ./_wks */ \"./node_modules/core-js/library/modules/_wks.js\")('iterator'), function () { return this; });\n\nmodule.exports = function (Constructor, NAME, next) {\n  Constructor.prototype = create(IteratorPrototype, { next: descriptor(1, next) });\n  setToStringTag(Constructor, NAME + ' Iterator');\n};\n\n\n//# sourceURL=webpack:///./node_modules/core-js/library/modules/_iter-create.js?");

/***/ }),

/***/ "./node_modules/core-js/library/modules/_iter-define.js":
/*!**************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_iter-define.js ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\nvar LIBRARY = __webpack_require__(/*! ./_library */ \"./node_modules/core-js/library/modules/_library.js\");\nvar $export = __webpack_require__(/*! ./_export */ \"./node_modules/core-js/library/modules/_export.js\");\nvar redefine = __webpack_require__(/*! ./_redefine */ \"./node_modules/core-js/library/modules/_redefine.js\");\nvar hide = __webpack_require__(/*! ./_hide */ \"./node_modules/core-js/library/modules/_hide.js\");\nvar Iterators = __webpack_require__(/*! ./_iterators */ \"./node_modules/core-js/library/modules/_iterators.js\");\nvar $iterCreate = __webpack_require__(/*! ./_iter-create */ \"./node_modules/core-js/library/modules/_iter-create.js\");\nvar setToStringTag = __webpack_require__(/*! ./_set-to-string-tag */ \"./node_modules/core-js/library/modules/_set-to-string-tag.js\");\nvar getPrototypeOf = __webpack_require__(/*! ./_object-gpo */ \"./node_modules/core-js/library/modules/_object-gpo.js\");\nvar ITERATOR = __webpack_require__(/*! ./_wks */ \"./node_modules/core-js/library/modules/_wks.js\")('iterator');\nvar BUGGY = !([].keys && 'next' in [].keys()); // Safari has buggy iterators w/o `next`\nvar FF_ITERATOR = '@@iterator';\nvar KEYS = 'keys';\nvar VALUES = 'values';\n\nvar returnThis = function () { return this; };\n\nmodule.exports = function (Base, NAME, Constructor, next, DEFAULT, IS_SET, FORCED) {\n  $iterCreate(Constructor, NAME, next);\n  var getMethod = function (kind) {\n    if (!BUGGY && kind in proto) return proto[kind];\n    switch (kind) {\n      case KEYS: return function keys() { return new Constructor(this, kind); };\n      case VALUES: return function values() { return new Constructor(this, kind); };\n    } return function entries() { return new Constructor(this, kind); };\n  };\n  var TAG = NAME + ' Iterator';\n  var DEF_VALUES = DEFAULT == VALUES;\n  var VALUES_BUG = false;\n  var proto = Base.prototype;\n  var $native = proto[ITERATOR] || proto[FF_ITERATOR] || DEFAULT && proto[DEFAULT];\n  var $default = $native || getMethod(DEFAULT);\n  var $entries = DEFAULT ? !DEF_VALUES ? $default : getMethod('entries') : undefined;\n  var $anyNative = NAME == 'Array' ? proto.entries || $native : $native;\n  var methods, key, IteratorPrototype;\n  // Fix native\n  if ($anyNative) {\n    IteratorPrototype = getPrototypeOf($anyNative.call(new Base()));\n    if (IteratorPrototype !== Object.prototype && IteratorPrototype.next) {\n      // Set @@toStringTag to native iterators\n      setToStringTag(IteratorPrototype, TAG, true);\n      // fix for some old engines\n      if (!LIBRARY && typeof IteratorPrototype[ITERATOR] != 'function') hide(IteratorPrototype, ITERATOR, returnThis);\n    }\n  }\n  // fix Array#{values, @@iterator}.name in V8 / FF\n  if (DEF_VALUES && $native && $native.name !== VALUES) {\n    VALUES_BUG = true;\n    $default = function values() { return $native.call(this); };\n  }\n  // Define iterator\n  if ((!LIBRARY || FORCED) && (BUGGY || VALUES_BUG || !proto[ITERATOR])) {\n    hide(proto, ITERATOR, $default);\n  }\n  // Plug for library\n  Iterators[NAME] = $default;\n  Iterators[TAG] = returnThis;\n  if (DEFAULT) {\n    methods = {\n      values: DEF_VALUES ? $default : getMethod(VALUES),\n      keys: IS_SET ? $default : getMethod(KEYS),\n      entries: $entries\n    };\n    if (FORCED) for (key in methods) {\n      if (!(key in proto)) redefine(proto, key, methods[key]);\n    } else $export($export.P + $export.F * (BUGGY || VALUES_BUG), NAME, methods);\n  }\n  return methods;\n};\n\n\n//# sourceURL=webpack:///./node_modules/core-js/library/modules/_iter-define.js?");

/***/ }),

/***/ "./node_modules/core-js/library/modules/_iter-step.js":
/*!************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_iter-step.js ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = function (done, value) {\n  return { value: value, done: !!done };\n};\n\n\n//# sourceURL=webpack:///./node_modules/core-js/library/modules/_iter-step.js?");

/***/ }),

/***/ "./node_modules/core-js/library/modules/_iterators.js":
/*!************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_iterators.js ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = {};\n\n\n//# sourceURL=webpack:///./node_modules/core-js/library/modules/_iterators.js?");

/***/ }),

/***/ "./node_modules/core-js/library/modules/_library.js":
/*!**********************************************************!*\
  !*** ./node_modules/core-js/library/modules/_library.js ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = true;\n\n\n//# sourceURL=webpack:///./node_modules/core-js/library/modules/_library.js?");

/***/ }),

/***/ "./node_modules/core-js/library/modules/_meta.js":
/*!*******************************************************!*\
  !*** ./node_modules/core-js/library/modules/_meta.js ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var META = __webpack_require__(/*! ./_uid */ \"./node_modules/core-js/library/modules/_uid.js\")('meta');\nvar isObject = __webpack_require__(/*! ./_is-object */ \"./node_modules/core-js/library/modules/_is-object.js\");\nvar has = __webpack_require__(/*! ./_has */ \"./node_modules/core-js/library/modules/_has.js\");\nvar setDesc = __webpack_require__(/*! ./_object-dp */ \"./node_modules/core-js/library/modules/_object-dp.js\").f;\nvar id = 0;\nvar isExtensible = Object.isExtensible || function () {\n  return true;\n};\nvar FREEZE = !__webpack_require__(/*! ./_fails */ \"./node_modules/core-js/library/modules/_fails.js\")(function () {\n  return isExtensible(Object.preventExtensions({}));\n});\nvar setMeta = function (it) {\n  setDesc(it, META, { value: {\n    i: 'O' + ++id, // object ID\n    w: {}          // weak collections IDs\n  } });\n};\nvar fastKey = function (it, create) {\n  // return primitive with prefix\n  if (!isObject(it)) return typeof it == 'symbol' ? it : (typeof it == 'string' ? 'S' : 'P') + it;\n  if (!has(it, META)) {\n    // can't set metadata to uncaught frozen object\n    if (!isExtensible(it)) return 'F';\n    // not necessary to add metadata\n    if (!create) return 'E';\n    // add missing metadata\n    setMeta(it);\n  // return object ID\n  } return it[META].i;\n};\nvar getWeak = function (it, create) {\n  if (!has(it, META)) {\n    // can't set metadata to uncaught frozen object\n    if (!isExtensible(it)) return true;\n    // not necessary to add metadata\n    if (!create) return false;\n    // add missing metadata\n    setMeta(it);\n  // return hash weak collections IDs\n  } return it[META].w;\n};\n// add metadata on freeze-family methods calling\nvar onFreeze = function (it) {\n  if (FREEZE && meta.NEED && isExtensible(it) && !has(it, META)) setMeta(it);\n  return it;\n};\nvar meta = module.exports = {\n  KEY: META,\n  NEED: false,\n  fastKey: fastKey,\n  getWeak: getWeak,\n  onFreeze: onFreeze\n};\n\n\n//# sourceURL=webpack:///./node_modules/core-js/library/modules/_meta.js?");

/***/ }),

/***/ "./node_modules/core-js/library/modules/_object-create.js":
/*!****************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_object-create.js ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// 19.1.2.2 / 15.2.3.5 Object.create(O [, Properties])\nvar anObject = __webpack_require__(/*! ./_an-object */ \"./node_modules/core-js/library/modules/_an-object.js\");\nvar dPs = __webpack_require__(/*! ./_object-dps */ \"./node_modules/core-js/library/modules/_object-dps.js\");\nvar enumBugKeys = __webpack_require__(/*! ./_enum-bug-keys */ \"./node_modules/core-js/library/modules/_enum-bug-keys.js\");\nvar IE_PROTO = __webpack_require__(/*! ./_shared-key */ \"./node_modules/core-js/library/modules/_shared-key.js\")('IE_PROTO');\nvar Empty = function () { /* empty */ };\nvar PROTOTYPE = 'prototype';\n\n// Create object with fake `null` prototype: use iframe Object with cleared prototype\nvar createDict = function () {\n  // Thrash, waste and sodomy: IE GC bug\n  var iframe = __webpack_require__(/*! ./_dom-create */ \"./node_modules/core-js/library/modules/_dom-create.js\")('iframe');\n  var i = enumBugKeys.length;\n  var lt = '<';\n  var gt = '>';\n  var iframeDocument;\n  iframe.style.display = 'none';\n  __webpack_require__(/*! ./_html */ \"./node_modules/core-js/library/modules/_html.js\").appendChild(iframe);\n  iframe.src = 'javascript:'; // eslint-disable-line no-script-url\n  // createDict = iframe.contentWindow.Object;\n  // html.removeChild(iframe);\n  iframeDocument = iframe.contentWindow.document;\n  iframeDocument.open();\n  iframeDocument.write(lt + 'script' + gt + 'document.F=Object' + lt + '/script' + gt);\n  iframeDocument.close();\n  createDict = iframeDocument.F;\n  while (i--) delete createDict[PROTOTYPE][enumBugKeys[i]];\n  return createDict();\n};\n\nmodule.exports = Object.create || function create(O, Properties) {\n  var result;\n  if (O !== null) {\n    Empty[PROTOTYPE] = anObject(O);\n    result = new Empty();\n    Empty[PROTOTYPE] = null;\n    // add \"__proto__\" for Object.getPrototypeOf polyfill\n    result[IE_PROTO] = O;\n  } else result = createDict();\n  return Properties === undefined ? result : dPs(result, Properties);\n};\n\n\n//# sourceURL=webpack:///./node_modules/core-js/library/modules/_object-create.js?");

/***/ }),

/***/ "./node_modules/core-js/library/modules/_object-dp.js":
/*!************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_object-dp.js ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var anObject = __webpack_require__(/*! ./_an-object */ \"./node_modules/core-js/library/modules/_an-object.js\");\nvar IE8_DOM_DEFINE = __webpack_require__(/*! ./_ie8-dom-define */ \"./node_modules/core-js/library/modules/_ie8-dom-define.js\");\nvar toPrimitive = __webpack_require__(/*! ./_to-primitive */ \"./node_modules/core-js/library/modules/_to-primitive.js\");\nvar dP = Object.defineProperty;\n\nexports.f = __webpack_require__(/*! ./_descriptors */ \"./node_modules/core-js/library/modules/_descriptors.js\") ? Object.defineProperty : function defineProperty(O, P, Attributes) {\n  anObject(O);\n  P = toPrimitive(P, true);\n  anObject(Attributes);\n  if (IE8_DOM_DEFINE) try {\n    return dP(O, P, Attributes);\n  } catch (e) { /* empty */ }\n  if ('get' in Attributes || 'set' in Attributes) throw TypeError('Accessors not supported!');\n  if ('value' in Attributes) O[P] = Attributes.value;\n  return O;\n};\n\n\n//# sourceURL=webpack:///./node_modules/core-js/library/modules/_object-dp.js?");

/***/ }),

/***/ "./node_modules/core-js/library/modules/_object-dps.js":
/*!*************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_object-dps.js ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var dP = __webpack_require__(/*! ./_object-dp */ \"./node_modules/core-js/library/modules/_object-dp.js\");\nvar anObject = __webpack_require__(/*! ./_an-object */ \"./node_modules/core-js/library/modules/_an-object.js\");\nvar getKeys = __webpack_require__(/*! ./_object-keys */ \"./node_modules/core-js/library/modules/_object-keys.js\");\n\nmodule.exports = __webpack_require__(/*! ./_descriptors */ \"./node_modules/core-js/library/modules/_descriptors.js\") ? Object.defineProperties : function defineProperties(O, Properties) {\n  anObject(O);\n  var keys = getKeys(Properties);\n  var length = keys.length;\n  var i = 0;\n  var P;\n  while (length > i) dP.f(O, P = keys[i++], Properties[P]);\n  return O;\n};\n\n\n//# sourceURL=webpack:///./node_modules/core-js/library/modules/_object-dps.js?");

/***/ }),

/***/ "./node_modules/core-js/library/modules/_object-gopd.js":
/*!**************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_object-gopd.js ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var pIE = __webpack_require__(/*! ./_object-pie */ \"./node_modules/core-js/library/modules/_object-pie.js\");\nvar createDesc = __webpack_require__(/*! ./_property-desc */ \"./node_modules/core-js/library/modules/_property-desc.js\");\nvar toIObject = __webpack_require__(/*! ./_to-iobject */ \"./node_modules/core-js/library/modules/_to-iobject.js\");\nvar toPrimitive = __webpack_require__(/*! ./_to-primitive */ \"./node_modules/core-js/library/modules/_to-primitive.js\");\nvar has = __webpack_require__(/*! ./_has */ \"./node_modules/core-js/library/modules/_has.js\");\nvar IE8_DOM_DEFINE = __webpack_require__(/*! ./_ie8-dom-define */ \"./node_modules/core-js/library/modules/_ie8-dom-define.js\");\nvar gOPD = Object.getOwnPropertyDescriptor;\n\nexports.f = __webpack_require__(/*! ./_descriptors */ \"./node_modules/core-js/library/modules/_descriptors.js\") ? gOPD : function getOwnPropertyDescriptor(O, P) {\n  O = toIObject(O);\n  P = toPrimitive(P, true);\n  if (IE8_DOM_DEFINE) try {\n    return gOPD(O, P);\n  } catch (e) { /* empty */ }\n  if (has(O, P)) return createDesc(!pIE.f.call(O, P), O[P]);\n};\n\n\n//# sourceURL=webpack:///./node_modules/core-js/library/modules/_object-gopd.js?");

/***/ }),

/***/ "./node_modules/core-js/library/modules/_object-gopn-ext.js":
/*!******************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_object-gopn-ext.js ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// fallback for IE11 buggy Object.getOwnPropertyNames with iframe and window\nvar toIObject = __webpack_require__(/*! ./_to-iobject */ \"./node_modules/core-js/library/modules/_to-iobject.js\");\nvar gOPN = __webpack_require__(/*! ./_object-gopn */ \"./node_modules/core-js/library/modules/_object-gopn.js\").f;\nvar toString = {}.toString;\n\nvar windowNames = typeof window == 'object' && window && Object.getOwnPropertyNames\n  ? Object.getOwnPropertyNames(window) : [];\n\nvar getWindowNames = function (it) {\n  try {\n    return gOPN(it);\n  } catch (e) {\n    return windowNames.slice();\n  }\n};\n\nmodule.exports.f = function getOwnPropertyNames(it) {\n  return windowNames && toString.call(it) == '[object Window]' ? getWindowNames(it) : gOPN(toIObject(it));\n};\n\n\n//# sourceURL=webpack:///./node_modules/core-js/library/modules/_object-gopn-ext.js?");

/***/ }),

/***/ "./node_modules/core-js/library/modules/_object-gopn.js":
/*!**************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_object-gopn.js ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// 19.1.2.7 / 15.2.3.4 Object.getOwnPropertyNames(O)\nvar $keys = __webpack_require__(/*! ./_object-keys-internal */ \"./node_modules/core-js/library/modules/_object-keys-internal.js\");\nvar hiddenKeys = __webpack_require__(/*! ./_enum-bug-keys */ \"./node_modules/core-js/library/modules/_enum-bug-keys.js\").concat('length', 'prototype');\n\nexports.f = Object.getOwnPropertyNames || function getOwnPropertyNames(O) {\n  return $keys(O, hiddenKeys);\n};\n\n\n//# sourceURL=webpack:///./node_modules/core-js/library/modules/_object-gopn.js?");

/***/ }),

/***/ "./node_modules/core-js/library/modules/_object-gops.js":
/*!**************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_object-gops.js ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("exports.f = Object.getOwnPropertySymbols;\n\n\n//# sourceURL=webpack:///./node_modules/core-js/library/modules/_object-gops.js?");

/***/ }),

/***/ "./node_modules/core-js/library/modules/_object-gpo.js":
/*!*************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_object-gpo.js ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// 19.1.2.9 / 15.2.3.2 Object.getPrototypeOf(O)\nvar has = __webpack_require__(/*! ./_has */ \"./node_modules/core-js/library/modules/_has.js\");\nvar toObject = __webpack_require__(/*! ./_to-object */ \"./node_modules/core-js/library/modules/_to-object.js\");\nvar IE_PROTO = __webpack_require__(/*! ./_shared-key */ \"./node_modules/core-js/library/modules/_shared-key.js\")('IE_PROTO');\nvar ObjectProto = Object.prototype;\n\nmodule.exports = Object.getPrototypeOf || function (O) {\n  O = toObject(O);\n  if (has(O, IE_PROTO)) return O[IE_PROTO];\n  if (typeof O.constructor == 'function' && O instanceof O.constructor) {\n    return O.constructor.prototype;\n  } return O instanceof Object ? ObjectProto : null;\n};\n\n\n//# sourceURL=webpack:///./node_modules/core-js/library/modules/_object-gpo.js?");

/***/ }),

/***/ "./node_modules/core-js/library/modules/_object-keys-internal.js":
/*!***********************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_object-keys-internal.js ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var has = __webpack_require__(/*! ./_has */ \"./node_modules/core-js/library/modules/_has.js\");\nvar toIObject = __webpack_require__(/*! ./_to-iobject */ \"./node_modules/core-js/library/modules/_to-iobject.js\");\nvar arrayIndexOf = __webpack_require__(/*! ./_array-includes */ \"./node_modules/core-js/library/modules/_array-includes.js\")(false);\nvar IE_PROTO = __webpack_require__(/*! ./_shared-key */ \"./node_modules/core-js/library/modules/_shared-key.js\")('IE_PROTO');\n\nmodule.exports = function (object, names) {\n  var O = toIObject(object);\n  var i = 0;\n  var result = [];\n  var key;\n  for (key in O) if (key != IE_PROTO) has(O, key) && result.push(key);\n  // Don't enum bug & hidden keys\n  while (names.length > i) if (has(O, key = names[i++])) {\n    ~arrayIndexOf(result, key) || result.push(key);\n  }\n  return result;\n};\n\n\n//# sourceURL=webpack:///./node_modules/core-js/library/modules/_object-keys-internal.js?");

/***/ }),

/***/ "./node_modules/core-js/library/modules/_object-keys.js":
/*!**************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_object-keys.js ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// 19.1.2.14 / 15.2.3.14 Object.keys(O)\nvar $keys = __webpack_require__(/*! ./_object-keys-internal */ \"./node_modules/core-js/library/modules/_object-keys-internal.js\");\nvar enumBugKeys = __webpack_require__(/*! ./_enum-bug-keys */ \"./node_modules/core-js/library/modules/_enum-bug-keys.js\");\n\nmodule.exports = Object.keys || function keys(O) {\n  return $keys(O, enumBugKeys);\n};\n\n\n//# sourceURL=webpack:///./node_modules/core-js/library/modules/_object-keys.js?");

/***/ }),

/***/ "./node_modules/core-js/library/modules/_object-pie.js":
/*!*************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_object-pie.js ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("exports.f = {}.propertyIsEnumerable;\n\n\n//# sourceURL=webpack:///./node_modules/core-js/library/modules/_object-pie.js?");

/***/ }),

/***/ "./node_modules/core-js/library/modules/_property-desc.js":
/*!****************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_property-desc.js ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = function (bitmap, value) {\n  return {\n    enumerable: !(bitmap & 1),\n    configurable: !(bitmap & 2),\n    writable: !(bitmap & 4),\n    value: value\n  };\n};\n\n\n//# sourceURL=webpack:///./node_modules/core-js/library/modules/_property-desc.js?");

/***/ }),

/***/ "./node_modules/core-js/library/modules/_redefine.js":
/*!***********************************************************!*\
  !*** ./node_modules/core-js/library/modules/_redefine.js ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("module.exports = __webpack_require__(/*! ./_hide */ \"./node_modules/core-js/library/modules/_hide.js\");\n\n\n//# sourceURL=webpack:///./node_modules/core-js/library/modules/_redefine.js?");

/***/ }),

/***/ "./node_modules/core-js/library/modules/_set-to-string-tag.js":
/*!********************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_set-to-string-tag.js ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var def = __webpack_require__(/*! ./_object-dp */ \"./node_modules/core-js/library/modules/_object-dp.js\").f;\nvar has = __webpack_require__(/*! ./_has */ \"./node_modules/core-js/library/modules/_has.js\");\nvar TAG = __webpack_require__(/*! ./_wks */ \"./node_modules/core-js/library/modules/_wks.js\")('toStringTag');\n\nmodule.exports = function (it, tag, stat) {\n  if (it && !has(it = stat ? it : it.prototype, TAG)) def(it, TAG, { configurable: true, value: tag });\n};\n\n\n//# sourceURL=webpack:///./node_modules/core-js/library/modules/_set-to-string-tag.js?");

/***/ }),

/***/ "./node_modules/core-js/library/modules/_shared-key.js":
/*!*************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_shared-key.js ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var shared = __webpack_require__(/*! ./_shared */ \"./node_modules/core-js/library/modules/_shared.js\")('keys');\nvar uid = __webpack_require__(/*! ./_uid */ \"./node_modules/core-js/library/modules/_uid.js\");\nmodule.exports = function (key) {\n  return shared[key] || (shared[key] = uid(key));\n};\n\n\n//# sourceURL=webpack:///./node_modules/core-js/library/modules/_shared-key.js?");

/***/ }),

/***/ "./node_modules/core-js/library/modules/_shared.js":
/*!*********************************************************!*\
  !*** ./node_modules/core-js/library/modules/_shared.js ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var core = __webpack_require__(/*! ./_core */ \"./node_modules/core-js/library/modules/_core.js\");\nvar global = __webpack_require__(/*! ./_global */ \"./node_modules/core-js/library/modules/_global.js\");\nvar SHARED = '__core-js_shared__';\nvar store = global[SHARED] || (global[SHARED] = {});\n\n(module.exports = function (key, value) {\n  return store[key] || (store[key] = value !== undefined ? value : {});\n})('versions', []).push({\n  version: core.version,\n  mode: __webpack_require__(/*! ./_library */ \"./node_modules/core-js/library/modules/_library.js\") ? 'pure' : 'global',\n  copyright: '© 2018 Denis Pushkarev (zloirock.ru)'\n});\n\n\n//# sourceURL=webpack:///./node_modules/core-js/library/modules/_shared.js?");

/***/ }),

/***/ "./node_modules/core-js/library/modules/_string-at.js":
/*!************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_string-at.js ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var toInteger = __webpack_require__(/*! ./_to-integer */ \"./node_modules/core-js/library/modules/_to-integer.js\");\nvar defined = __webpack_require__(/*! ./_defined */ \"./node_modules/core-js/library/modules/_defined.js\");\n// true  -> String#at\n// false -> String#codePointAt\nmodule.exports = function (TO_STRING) {\n  return function (that, pos) {\n    var s = String(defined(that));\n    var i = toInteger(pos);\n    var l = s.length;\n    var a, b;\n    if (i < 0 || i >= l) return TO_STRING ? '' : undefined;\n    a = s.charCodeAt(i);\n    return a < 0xd800 || a > 0xdbff || i + 1 === l || (b = s.charCodeAt(i + 1)) < 0xdc00 || b > 0xdfff\n      ? TO_STRING ? s.charAt(i) : a\n      : TO_STRING ? s.slice(i, i + 2) : (a - 0xd800 << 10) + (b - 0xdc00) + 0x10000;\n  };\n};\n\n\n//# sourceURL=webpack:///./node_modules/core-js/library/modules/_string-at.js?");

/***/ }),

/***/ "./node_modules/core-js/library/modules/_to-absolute-index.js":
/*!********************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_to-absolute-index.js ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var toInteger = __webpack_require__(/*! ./_to-integer */ \"./node_modules/core-js/library/modules/_to-integer.js\");\nvar max = Math.max;\nvar min = Math.min;\nmodule.exports = function (index, length) {\n  index = toInteger(index);\n  return index < 0 ? max(index + length, 0) : min(index, length);\n};\n\n\n//# sourceURL=webpack:///./node_modules/core-js/library/modules/_to-absolute-index.js?");

/***/ }),

/***/ "./node_modules/core-js/library/modules/_to-integer.js":
/*!*************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_to-integer.js ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("// 7.1.4 ToInteger\nvar ceil = Math.ceil;\nvar floor = Math.floor;\nmodule.exports = function (it) {\n  return isNaN(it = +it) ? 0 : (it > 0 ? floor : ceil)(it);\n};\n\n\n//# sourceURL=webpack:///./node_modules/core-js/library/modules/_to-integer.js?");

/***/ }),

/***/ "./node_modules/core-js/library/modules/_to-iobject.js":
/*!*************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_to-iobject.js ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// to indexed object, toObject with fallback for non-array-like ES3 strings\nvar IObject = __webpack_require__(/*! ./_iobject */ \"./node_modules/core-js/library/modules/_iobject.js\");\nvar defined = __webpack_require__(/*! ./_defined */ \"./node_modules/core-js/library/modules/_defined.js\");\nmodule.exports = function (it) {\n  return IObject(defined(it));\n};\n\n\n//# sourceURL=webpack:///./node_modules/core-js/library/modules/_to-iobject.js?");

/***/ }),

/***/ "./node_modules/core-js/library/modules/_to-length.js":
/*!************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_to-length.js ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// 7.1.15 ToLength\nvar toInteger = __webpack_require__(/*! ./_to-integer */ \"./node_modules/core-js/library/modules/_to-integer.js\");\nvar min = Math.min;\nmodule.exports = function (it) {\n  return it > 0 ? min(toInteger(it), 0x1fffffffffffff) : 0; // pow(2, 53) - 1 == 9007199254740991\n};\n\n\n//# sourceURL=webpack:///./node_modules/core-js/library/modules/_to-length.js?");

/***/ }),

/***/ "./node_modules/core-js/library/modules/_to-object.js":
/*!************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_to-object.js ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// 7.1.13 ToObject(argument)\nvar defined = __webpack_require__(/*! ./_defined */ \"./node_modules/core-js/library/modules/_defined.js\");\nmodule.exports = function (it) {\n  return Object(defined(it));\n};\n\n\n//# sourceURL=webpack:///./node_modules/core-js/library/modules/_to-object.js?");

/***/ }),

/***/ "./node_modules/core-js/library/modules/_to-primitive.js":
/*!***************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_to-primitive.js ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// 7.1.1 ToPrimitive(input [, PreferredType])\nvar isObject = __webpack_require__(/*! ./_is-object */ \"./node_modules/core-js/library/modules/_is-object.js\");\n// instead of the ES6 spec version, we didn't implement @@toPrimitive case\n// and the second argument - flag - preferred type is a string\nmodule.exports = function (it, S) {\n  if (!isObject(it)) return it;\n  var fn, val;\n  if (S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it))) return val;\n  if (typeof (fn = it.valueOf) == 'function' && !isObject(val = fn.call(it))) return val;\n  if (!S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it))) return val;\n  throw TypeError(\"Can't convert object to primitive value\");\n};\n\n\n//# sourceURL=webpack:///./node_modules/core-js/library/modules/_to-primitive.js?");

/***/ }),

/***/ "./node_modules/core-js/library/modules/_uid.js":
/*!******************************************************!*\
  !*** ./node_modules/core-js/library/modules/_uid.js ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("var id = 0;\nvar px = Math.random();\nmodule.exports = function (key) {\n  return 'Symbol('.concat(key === undefined ? '' : key, ')_', (++id + px).toString(36));\n};\n\n\n//# sourceURL=webpack:///./node_modules/core-js/library/modules/_uid.js?");

/***/ }),

/***/ "./node_modules/core-js/library/modules/_wks-define.js":
/*!*************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_wks-define.js ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var global = __webpack_require__(/*! ./_global */ \"./node_modules/core-js/library/modules/_global.js\");\nvar core = __webpack_require__(/*! ./_core */ \"./node_modules/core-js/library/modules/_core.js\");\nvar LIBRARY = __webpack_require__(/*! ./_library */ \"./node_modules/core-js/library/modules/_library.js\");\nvar wksExt = __webpack_require__(/*! ./_wks-ext */ \"./node_modules/core-js/library/modules/_wks-ext.js\");\nvar defineProperty = __webpack_require__(/*! ./_object-dp */ \"./node_modules/core-js/library/modules/_object-dp.js\").f;\nmodule.exports = function (name) {\n  var $Symbol = core.Symbol || (core.Symbol = LIBRARY ? {} : global.Symbol || {});\n  if (name.charAt(0) != '_' && !(name in $Symbol)) defineProperty($Symbol, name, { value: wksExt.f(name) });\n};\n\n\n//# sourceURL=webpack:///./node_modules/core-js/library/modules/_wks-define.js?");

/***/ }),

/***/ "./node_modules/core-js/library/modules/_wks-ext.js":
/*!**********************************************************!*\
  !*** ./node_modules/core-js/library/modules/_wks-ext.js ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("exports.f = __webpack_require__(/*! ./_wks */ \"./node_modules/core-js/library/modules/_wks.js\");\n\n\n//# sourceURL=webpack:///./node_modules/core-js/library/modules/_wks-ext.js?");

/***/ }),

/***/ "./node_modules/core-js/library/modules/_wks.js":
/*!******************************************************!*\
  !*** ./node_modules/core-js/library/modules/_wks.js ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var store = __webpack_require__(/*! ./_shared */ \"./node_modules/core-js/library/modules/_shared.js\")('wks');\nvar uid = __webpack_require__(/*! ./_uid */ \"./node_modules/core-js/library/modules/_uid.js\");\nvar Symbol = __webpack_require__(/*! ./_global */ \"./node_modules/core-js/library/modules/_global.js\").Symbol;\nvar USE_SYMBOL = typeof Symbol == 'function';\n\nvar $exports = module.exports = function (name) {\n  return store[name] || (store[name] =\n    USE_SYMBOL && Symbol[name] || (USE_SYMBOL ? Symbol : uid)('Symbol.' + name));\n};\n\n$exports.store = store;\n\n\n//# sourceURL=webpack:///./node_modules/core-js/library/modules/_wks.js?");

/***/ }),

/***/ "./node_modules/core-js/library/modules/es6.array.iterator.js":
/*!********************************************************************!*\
  !*** ./node_modules/core-js/library/modules/es6.array.iterator.js ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\nvar addToUnscopables = __webpack_require__(/*! ./_add-to-unscopables */ \"./node_modules/core-js/library/modules/_add-to-unscopables.js\");\nvar step = __webpack_require__(/*! ./_iter-step */ \"./node_modules/core-js/library/modules/_iter-step.js\");\nvar Iterators = __webpack_require__(/*! ./_iterators */ \"./node_modules/core-js/library/modules/_iterators.js\");\nvar toIObject = __webpack_require__(/*! ./_to-iobject */ \"./node_modules/core-js/library/modules/_to-iobject.js\");\n\n// 22.1.3.4 Array.prototype.entries()\n// 22.1.3.13 Array.prototype.keys()\n// 22.1.3.29 Array.prototype.values()\n// 22.1.3.30 Array.prototype[@@iterator]()\nmodule.exports = __webpack_require__(/*! ./_iter-define */ \"./node_modules/core-js/library/modules/_iter-define.js\")(Array, 'Array', function (iterated, kind) {\n  this._t = toIObject(iterated); // target\n  this._i = 0;                   // next index\n  this._k = kind;                // kind\n// 22.1.5.2.1 %ArrayIteratorPrototype%.next()\n}, function () {\n  var O = this._t;\n  var kind = this._k;\n  var index = this._i++;\n  if (!O || index >= O.length) {\n    this._t = undefined;\n    return step(1);\n  }\n  if (kind == 'keys') return step(0, index);\n  if (kind == 'values') return step(0, O[index]);\n  return step(0, [index, O[index]]);\n}, 'values');\n\n// argumentsList[@@iterator] is %ArrayProto_values% (9.4.4.6, 9.4.4.7)\nIterators.Arguments = Iterators.Array;\n\naddToUnscopables('keys');\naddToUnscopables('values');\naddToUnscopables('entries');\n\n\n//# sourceURL=webpack:///./node_modules/core-js/library/modules/es6.array.iterator.js?");

/***/ }),

/***/ "./node_modules/core-js/library/modules/es6.object.to-string.js":
/*!**********************************************************************!*\
  !*** ./node_modules/core-js/library/modules/es6.object.to-string.js ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("\n\n//# sourceURL=webpack:///./node_modules/core-js/library/modules/es6.object.to-string.js?");

/***/ }),

/***/ "./node_modules/core-js/library/modules/es6.string.iterator.js":
/*!*********************************************************************!*\
  !*** ./node_modules/core-js/library/modules/es6.string.iterator.js ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\nvar $at = __webpack_require__(/*! ./_string-at */ \"./node_modules/core-js/library/modules/_string-at.js\")(true);\n\n// 21.1.3.27 String.prototype[@@iterator]()\n__webpack_require__(/*! ./_iter-define */ \"./node_modules/core-js/library/modules/_iter-define.js\")(String, 'String', function (iterated) {\n  this._t = String(iterated); // target\n  this._i = 0;                // next index\n// 21.1.5.2.1 %StringIteratorPrototype%.next()\n}, function () {\n  var O = this._t;\n  var index = this._i;\n  var point;\n  if (index >= O.length) return { value: undefined, done: true };\n  point = $at(O, index);\n  this._i += point.length;\n  return { value: point, done: false };\n});\n\n\n//# sourceURL=webpack:///./node_modules/core-js/library/modules/es6.string.iterator.js?");

/***/ }),

/***/ "./node_modules/core-js/library/modules/es6.symbol.js":
/*!************************************************************!*\
  !*** ./node_modules/core-js/library/modules/es6.symbol.js ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n// ECMAScript 6 symbols shim\nvar global = __webpack_require__(/*! ./_global */ \"./node_modules/core-js/library/modules/_global.js\");\nvar has = __webpack_require__(/*! ./_has */ \"./node_modules/core-js/library/modules/_has.js\");\nvar DESCRIPTORS = __webpack_require__(/*! ./_descriptors */ \"./node_modules/core-js/library/modules/_descriptors.js\");\nvar $export = __webpack_require__(/*! ./_export */ \"./node_modules/core-js/library/modules/_export.js\");\nvar redefine = __webpack_require__(/*! ./_redefine */ \"./node_modules/core-js/library/modules/_redefine.js\");\nvar META = __webpack_require__(/*! ./_meta */ \"./node_modules/core-js/library/modules/_meta.js\").KEY;\nvar $fails = __webpack_require__(/*! ./_fails */ \"./node_modules/core-js/library/modules/_fails.js\");\nvar shared = __webpack_require__(/*! ./_shared */ \"./node_modules/core-js/library/modules/_shared.js\");\nvar setToStringTag = __webpack_require__(/*! ./_set-to-string-tag */ \"./node_modules/core-js/library/modules/_set-to-string-tag.js\");\nvar uid = __webpack_require__(/*! ./_uid */ \"./node_modules/core-js/library/modules/_uid.js\");\nvar wks = __webpack_require__(/*! ./_wks */ \"./node_modules/core-js/library/modules/_wks.js\");\nvar wksExt = __webpack_require__(/*! ./_wks-ext */ \"./node_modules/core-js/library/modules/_wks-ext.js\");\nvar wksDefine = __webpack_require__(/*! ./_wks-define */ \"./node_modules/core-js/library/modules/_wks-define.js\");\nvar enumKeys = __webpack_require__(/*! ./_enum-keys */ \"./node_modules/core-js/library/modules/_enum-keys.js\");\nvar isArray = __webpack_require__(/*! ./_is-array */ \"./node_modules/core-js/library/modules/_is-array.js\");\nvar anObject = __webpack_require__(/*! ./_an-object */ \"./node_modules/core-js/library/modules/_an-object.js\");\nvar isObject = __webpack_require__(/*! ./_is-object */ \"./node_modules/core-js/library/modules/_is-object.js\");\nvar toIObject = __webpack_require__(/*! ./_to-iobject */ \"./node_modules/core-js/library/modules/_to-iobject.js\");\nvar toPrimitive = __webpack_require__(/*! ./_to-primitive */ \"./node_modules/core-js/library/modules/_to-primitive.js\");\nvar createDesc = __webpack_require__(/*! ./_property-desc */ \"./node_modules/core-js/library/modules/_property-desc.js\");\nvar _create = __webpack_require__(/*! ./_object-create */ \"./node_modules/core-js/library/modules/_object-create.js\");\nvar gOPNExt = __webpack_require__(/*! ./_object-gopn-ext */ \"./node_modules/core-js/library/modules/_object-gopn-ext.js\");\nvar $GOPD = __webpack_require__(/*! ./_object-gopd */ \"./node_modules/core-js/library/modules/_object-gopd.js\");\nvar $DP = __webpack_require__(/*! ./_object-dp */ \"./node_modules/core-js/library/modules/_object-dp.js\");\nvar $keys = __webpack_require__(/*! ./_object-keys */ \"./node_modules/core-js/library/modules/_object-keys.js\");\nvar gOPD = $GOPD.f;\nvar dP = $DP.f;\nvar gOPN = gOPNExt.f;\nvar $Symbol = global.Symbol;\nvar $JSON = global.JSON;\nvar _stringify = $JSON && $JSON.stringify;\nvar PROTOTYPE = 'prototype';\nvar HIDDEN = wks('_hidden');\nvar TO_PRIMITIVE = wks('toPrimitive');\nvar isEnum = {}.propertyIsEnumerable;\nvar SymbolRegistry = shared('symbol-registry');\nvar AllSymbols = shared('symbols');\nvar OPSymbols = shared('op-symbols');\nvar ObjectProto = Object[PROTOTYPE];\nvar USE_NATIVE = typeof $Symbol == 'function';\nvar QObject = global.QObject;\n// Don't use setters in Qt Script, https://github.com/zloirock/core-js/issues/173\nvar setter = !QObject || !QObject[PROTOTYPE] || !QObject[PROTOTYPE].findChild;\n\n// fallback for old Android, https://code.google.com/p/v8/issues/detail?id=687\nvar setSymbolDesc = DESCRIPTORS && $fails(function () {\n  return _create(dP({}, 'a', {\n    get: function () { return dP(this, 'a', { value: 7 }).a; }\n  })).a != 7;\n}) ? function (it, key, D) {\n  var protoDesc = gOPD(ObjectProto, key);\n  if (protoDesc) delete ObjectProto[key];\n  dP(it, key, D);\n  if (protoDesc && it !== ObjectProto) dP(ObjectProto, key, protoDesc);\n} : dP;\n\nvar wrap = function (tag) {\n  var sym = AllSymbols[tag] = _create($Symbol[PROTOTYPE]);\n  sym._k = tag;\n  return sym;\n};\n\nvar isSymbol = USE_NATIVE && typeof $Symbol.iterator == 'symbol' ? function (it) {\n  return typeof it == 'symbol';\n} : function (it) {\n  return it instanceof $Symbol;\n};\n\nvar $defineProperty = function defineProperty(it, key, D) {\n  if (it === ObjectProto) $defineProperty(OPSymbols, key, D);\n  anObject(it);\n  key = toPrimitive(key, true);\n  anObject(D);\n  if (has(AllSymbols, key)) {\n    if (!D.enumerable) {\n      if (!has(it, HIDDEN)) dP(it, HIDDEN, createDesc(1, {}));\n      it[HIDDEN][key] = true;\n    } else {\n      if (has(it, HIDDEN) && it[HIDDEN][key]) it[HIDDEN][key] = false;\n      D = _create(D, { enumerable: createDesc(0, false) });\n    } return setSymbolDesc(it, key, D);\n  } return dP(it, key, D);\n};\nvar $defineProperties = function defineProperties(it, P) {\n  anObject(it);\n  var keys = enumKeys(P = toIObject(P));\n  var i = 0;\n  var l = keys.length;\n  var key;\n  while (l > i) $defineProperty(it, key = keys[i++], P[key]);\n  return it;\n};\nvar $create = function create(it, P) {\n  return P === undefined ? _create(it) : $defineProperties(_create(it), P);\n};\nvar $propertyIsEnumerable = function propertyIsEnumerable(key) {\n  var E = isEnum.call(this, key = toPrimitive(key, true));\n  if (this === ObjectProto && has(AllSymbols, key) && !has(OPSymbols, key)) return false;\n  return E || !has(this, key) || !has(AllSymbols, key) || has(this, HIDDEN) && this[HIDDEN][key] ? E : true;\n};\nvar $getOwnPropertyDescriptor = function getOwnPropertyDescriptor(it, key) {\n  it = toIObject(it);\n  key = toPrimitive(key, true);\n  if (it === ObjectProto && has(AllSymbols, key) && !has(OPSymbols, key)) return;\n  var D = gOPD(it, key);\n  if (D && has(AllSymbols, key) && !(has(it, HIDDEN) && it[HIDDEN][key])) D.enumerable = true;\n  return D;\n};\nvar $getOwnPropertyNames = function getOwnPropertyNames(it) {\n  var names = gOPN(toIObject(it));\n  var result = [];\n  var i = 0;\n  var key;\n  while (names.length > i) {\n    if (!has(AllSymbols, key = names[i++]) && key != HIDDEN && key != META) result.push(key);\n  } return result;\n};\nvar $getOwnPropertySymbols = function getOwnPropertySymbols(it) {\n  var IS_OP = it === ObjectProto;\n  var names = gOPN(IS_OP ? OPSymbols : toIObject(it));\n  var result = [];\n  var i = 0;\n  var key;\n  while (names.length > i) {\n    if (has(AllSymbols, key = names[i++]) && (IS_OP ? has(ObjectProto, key) : true)) result.push(AllSymbols[key]);\n  } return result;\n};\n\n// 19.4.1.1 Symbol([description])\nif (!USE_NATIVE) {\n  $Symbol = function Symbol() {\n    if (this instanceof $Symbol) throw TypeError('Symbol is not a constructor!');\n    var tag = uid(arguments.length > 0 ? arguments[0] : undefined);\n    var $set = function (value) {\n      if (this === ObjectProto) $set.call(OPSymbols, value);\n      if (has(this, HIDDEN) && has(this[HIDDEN], tag)) this[HIDDEN][tag] = false;\n      setSymbolDesc(this, tag, createDesc(1, value));\n    };\n    if (DESCRIPTORS && setter) setSymbolDesc(ObjectProto, tag, { configurable: true, set: $set });\n    return wrap(tag);\n  };\n  redefine($Symbol[PROTOTYPE], 'toString', function toString() {\n    return this._k;\n  });\n\n  $GOPD.f = $getOwnPropertyDescriptor;\n  $DP.f = $defineProperty;\n  __webpack_require__(/*! ./_object-gopn */ \"./node_modules/core-js/library/modules/_object-gopn.js\").f = gOPNExt.f = $getOwnPropertyNames;\n  __webpack_require__(/*! ./_object-pie */ \"./node_modules/core-js/library/modules/_object-pie.js\").f = $propertyIsEnumerable;\n  __webpack_require__(/*! ./_object-gops */ \"./node_modules/core-js/library/modules/_object-gops.js\").f = $getOwnPropertySymbols;\n\n  if (DESCRIPTORS && !__webpack_require__(/*! ./_library */ \"./node_modules/core-js/library/modules/_library.js\")) {\n    redefine(ObjectProto, 'propertyIsEnumerable', $propertyIsEnumerable, true);\n  }\n\n  wksExt.f = function (name) {\n    return wrap(wks(name));\n  };\n}\n\n$export($export.G + $export.W + $export.F * !USE_NATIVE, { Symbol: $Symbol });\n\nfor (var es6Symbols = (\n  // 19.4.2.2, 19.4.2.3, 19.4.2.4, 19.4.2.6, 19.4.2.8, 19.4.2.9, 19.4.2.10, 19.4.2.11, 19.4.2.12, 19.4.2.13, 19.4.2.14\n  'hasInstance,isConcatSpreadable,iterator,match,replace,search,species,split,toPrimitive,toStringTag,unscopables'\n).split(','), j = 0; es6Symbols.length > j;)wks(es6Symbols[j++]);\n\nfor (var wellKnownSymbols = $keys(wks.store), k = 0; wellKnownSymbols.length > k;) wksDefine(wellKnownSymbols[k++]);\n\n$export($export.S + $export.F * !USE_NATIVE, 'Symbol', {\n  // 19.4.2.1 Symbol.for(key)\n  'for': function (key) {\n    return has(SymbolRegistry, key += '')\n      ? SymbolRegistry[key]\n      : SymbolRegistry[key] = $Symbol(key);\n  },\n  // 19.4.2.5 Symbol.keyFor(sym)\n  keyFor: function keyFor(sym) {\n    if (!isSymbol(sym)) throw TypeError(sym + ' is not a symbol!');\n    for (var key in SymbolRegistry) if (SymbolRegistry[key] === sym) return key;\n  },\n  useSetter: function () { setter = true; },\n  useSimple: function () { setter = false; }\n});\n\n$export($export.S + $export.F * !USE_NATIVE, 'Object', {\n  // 19.1.2.2 Object.create(O [, Properties])\n  create: $create,\n  // 19.1.2.4 Object.defineProperty(O, P, Attributes)\n  defineProperty: $defineProperty,\n  // 19.1.2.3 Object.defineProperties(O, Properties)\n  defineProperties: $defineProperties,\n  // 19.1.2.6 Object.getOwnPropertyDescriptor(O, P)\n  getOwnPropertyDescriptor: $getOwnPropertyDescriptor,\n  // 19.1.2.7 Object.getOwnPropertyNames(O)\n  getOwnPropertyNames: $getOwnPropertyNames,\n  // 19.1.2.8 Object.getOwnPropertySymbols(O)\n  getOwnPropertySymbols: $getOwnPropertySymbols\n});\n\n// 24.3.2 JSON.stringify(value [, replacer [, space]])\n$JSON && $export($export.S + $export.F * (!USE_NATIVE || $fails(function () {\n  var S = $Symbol();\n  // MS Edge converts symbol values to JSON as {}\n  // WebKit converts symbol values to JSON as null\n  // V8 throws on boxed symbols\n  return _stringify([S]) != '[null]' || _stringify({ a: S }) != '{}' || _stringify(Object(S)) != '{}';\n})), 'JSON', {\n  stringify: function stringify(it) {\n    var args = [it];\n    var i = 1;\n    var replacer, $replacer;\n    while (arguments.length > i) args.push(arguments[i++]);\n    $replacer = replacer = args[1];\n    if (!isObject(replacer) && it === undefined || isSymbol(it)) return; // IE8 returns string on undefined\n    if (!isArray(replacer)) replacer = function (key, value) {\n      if (typeof $replacer == 'function') value = $replacer.call(this, key, value);\n      if (!isSymbol(value)) return value;\n    };\n    args[1] = replacer;\n    return _stringify.apply($JSON, args);\n  }\n});\n\n// 19.4.3.4 Symbol.prototype[@@toPrimitive](hint)\n$Symbol[PROTOTYPE][TO_PRIMITIVE] || __webpack_require__(/*! ./_hide */ \"./node_modules/core-js/library/modules/_hide.js\")($Symbol[PROTOTYPE], TO_PRIMITIVE, $Symbol[PROTOTYPE].valueOf);\n// 19.4.3.5 Symbol.prototype[@@toStringTag]\nsetToStringTag($Symbol, 'Symbol');\n// 20.2.1.9 Math[@@toStringTag]\nsetToStringTag(Math, 'Math', true);\n// 24.3.3 JSON[@@toStringTag]\nsetToStringTag(global.JSON, 'JSON', true);\n\n\n//# sourceURL=webpack:///./node_modules/core-js/library/modules/es6.symbol.js?");

/***/ }),

/***/ "./node_modules/core-js/library/modules/es7.symbol.async-iterator.js":
/*!***************************************************************************!*\
  !*** ./node_modules/core-js/library/modules/es7.symbol.async-iterator.js ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("__webpack_require__(/*! ./_wks-define */ \"./node_modules/core-js/library/modules/_wks-define.js\")('asyncIterator');\n\n\n//# sourceURL=webpack:///./node_modules/core-js/library/modules/es7.symbol.async-iterator.js?");

/***/ }),

/***/ "./node_modules/core-js/library/modules/es7.symbol.observable.js":
/*!***********************************************************************!*\
  !*** ./node_modules/core-js/library/modules/es7.symbol.observable.js ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("__webpack_require__(/*! ./_wks-define */ \"./node_modules/core-js/library/modules/_wks-define.js\")('observable');\n\n\n//# sourceURL=webpack:///./node_modules/core-js/library/modules/es7.symbol.observable.js?");

/***/ }),

/***/ "./node_modules/core-js/library/modules/web.dom.iterable.js":
/*!******************************************************************!*\
  !*** ./node_modules/core-js/library/modules/web.dom.iterable.js ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("__webpack_require__(/*! ./es6.array.iterator */ \"./node_modules/core-js/library/modules/es6.array.iterator.js\");\nvar global = __webpack_require__(/*! ./_global */ \"./node_modules/core-js/library/modules/_global.js\");\nvar hide = __webpack_require__(/*! ./_hide */ \"./node_modules/core-js/library/modules/_hide.js\");\nvar Iterators = __webpack_require__(/*! ./_iterators */ \"./node_modules/core-js/library/modules/_iterators.js\");\nvar TO_STRING_TAG = __webpack_require__(/*! ./_wks */ \"./node_modules/core-js/library/modules/_wks.js\")('toStringTag');\n\nvar DOMIterables = ('CSSRuleList,CSSStyleDeclaration,CSSValueList,ClientRectList,DOMRectList,DOMStringList,' +\n  'DOMTokenList,DataTransferItemList,FileList,HTMLAllCollection,HTMLCollection,HTMLFormElement,HTMLSelectElement,' +\n  'MediaList,MimeTypeArray,NamedNodeMap,NodeList,PaintRequestList,Plugin,PluginArray,SVGLengthList,SVGNumberList,' +\n  'SVGPathSegList,SVGPointList,SVGStringList,SVGTransformList,SourceBufferList,StyleSheetList,TextTrackCueList,' +\n  'TextTrackList,TouchList').split(',');\n\nfor (var i = 0; i < DOMIterables.length; i++) {\n  var NAME = DOMIterables[i];\n  var Collection = global[NAME];\n  var proto = Collection && Collection.prototype;\n  if (proto && !proto[TO_STRING_TAG]) hide(proto, TO_STRING_TAG, NAME);\n  Iterators[NAME] = Iterators.Array;\n}\n\n\n//# sourceURL=webpack:///./node_modules/core-js/library/modules/web.dom.iterable.js?");

/***/ })

/******/ });