/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./js_modules/marun/cacaoReporteCosechaSector.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./js_modules/marun/cacaoReporteCosechaSector.js":
/*!*******************************************************!*\
  !*** ./js_modules/marun/cacaoReporteCosechaSector.js ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\napp.service('request', ['$http', function ($http) {\n    this.loadData = function (callback, params) {\n        $http.post('phrapi/marun/reporteCosechaSector/data', params || {}).then(function (r) {\n            if (callback) callback(r.data);\n        });\n    };\n\n    this.chart = function (callback, params) {\n        $http.post('phrapi/marun/reporteCosechaSector/chart', params || {}).then(function (r) {\n            if (callback) callback(r.data);\n        });\n    };\n}]);\n\napp.controller('controller', ['$scope', 'request', function ($scope, $request) {\n\n    var save = function save(id, campo, valor) {\n        $request.save(function (r) {\n            if (r.status == 200) {\n                alert(\"Se guardo correctamente\", \"\", \"success\");\n            } else {\n                alert(\"Hubo un error favor de intentar mas tarde\");\n            }\n        }, {\n            id: id, campo: campo, valor: valor\n        });\n    };\n\n    var renderChart = function renderChart(data) {\n        var props = {\n            series: data.data,\n            legend: data.legend,\n            umbral: data.umbral,\n            id: \"chart-lotes\",\n            legendBottom: false,\n            zoom: true,\n            type: 'line',\n            min: 'dataMin'\n        };\n        ReactDOM.render(React.createElement(Historica, props), document.getElementById('chart-lotes'));\n    };\n\n    var renderTableData = function renderTableData(data, periodos) {\n        var props = {\n            header: [{\n                key: 'sector',\n                name: 'SECTOR',\n                locked: true,\n                titleClass: 'text-center'\n            }, {\n                key: 'sum',\n                name: 'SUM',\n                locked: true,\n                titleClass: 'text-center',\n                alignContent: 'right'\n            }, {\n                key: 'avg',\n                name: 'AVG',\n                locked: true,\n                titleClass: 'text-center',\n                alignContent: 'right'\n            }, {\n                key: 'min',\n                name: 'MIN',\n                locked: true,\n                titleClass: 'text-center',\n                alignContent: 'right'\n            }, {\n                key: 'max',\n                name: 'MAX',\n                locked: true,\n                titleClass: 'text-center',\n                alignContent: 'right'\n            }],\n            data: data,\n            buttons: [{\n                title: 'Excel',\n                action: function action() {\n                    $scope.table1.exportToExcel();\n                },\n                className: ''\n            }]\n        };\n        periodos.map(function (s, i) {\n            props.header.push({\n                key: 'periodo_' + s,\n                name: '' + s,\n                titleClass: 'text-center',\n                alignContent: 'right'\n            });\n        });\n        $(\"#table-data\").html(\"\");\n        $scope.table1 = ReactDOM.render(React.createElement(ReactDataGrid, props), document.getElementById('table-data'));\n    };\n\n    $scope.filters = {\n        unidad: 'QQ',\n        anio: moment().year()\n    };\n\n    $scope.init = function () {\n        $request.loadData(function (r) {\n            $scope.anios = r.anios;\n            renderTableData(r.data, r.periodos);\n        }, $scope.filters);\n        $request.chart(function (r) {\n            renderChart(r.data);\n        }, $scope.filters);\n    };\n    $scope.init();\n}]);\n\n//# sourceURL=webpack:///./js_modules/marun/cacaoReporteCosechaSector.js?");

/***/ })

/******/ });