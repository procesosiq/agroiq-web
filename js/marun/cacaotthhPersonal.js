/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./js_modules/marun/cacaotthhPersonal.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./js_modules/marun/cacaotthhPersonal.js":
/*!***********************************************!*\
  !*** ./js_modules/marun/cacaotthhPersonal.js ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nvar TableDatatablesEditable = function () {\n\n    var cargos = \"\";\n    function getCargos() {\n        ahttp.post(\"phrapi/cargo/tipo\", function (r, b) {\n            b();\n            if (r) {\n                cargos = r || [];\n            }\n        });\n    }\n\n    getCargos();\n\n    var handleTable = function handleTable() {\n\n        function restoreRow(oTable, nRow) {\n            var aData = oTable.fnGetData(nRow);\n            var jqTds = $('>td', nRow);\n\n            for (var i = 0, iLen = jqTds.length; i < iLen; i++) {\n                oTable.fnUpdate(aData[i], nRow, i, false);\n            }\n\n            oTable.fnDraw();\n        }\n\n        function changeStatusRow(oTable, nRow, status) {\n            var html = [];\n            var className = status == 1 ? 'bg-green-jungle bg-font-green-jungle' : 'red';\n            var label = status == 1 ? 'Activo' : 'Inactivo';\n            html.push('<button class=\"btn btn-sm ' + className + ' status\" id=\"status\">');\n            html.push(label);\n            html.push('</button>');\n            oTable.fnUpdate(html.join(\"\"), nRow, 3, false);\n            oTable.fnDraw();\n        }\n\n        var table = $('#sample_editable_1');\n\n        var oTable = table.dataTable({\n\n            // Uncomment below line(\"dom\" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout\n            // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js). \n            // So when dropdowns used the scrollable div should be removed. \n            //\"dom\": \"<'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>\",\n\n            \"lengthMenu\": [[5, 15, 20, -1], [5, 15, 20, \"All\"] // change per page values here\n            ],\n\n            // Or you can use remote translation file\n            //\"language\": {\n            //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'\n            //},\n\n            // set the initial value\n            \"pageLength\": 5,\n\n            \"language\": {\n                \"lengthMenu\": \" _MENU_ records\"\n            },\n            \"columnDefs\": [{ // set default column settings\n                'orderable': true,\n                'targets': [0]\n            }, {\n                \"searchable\": true,\n                \"targets\": [0]\n            }, {\n                \"visible\": false,\n                \"targets\": [-1]\n            }],\n            \"order\": [[0, \"asc\"]] // set first column as a default sort by asc\n        });\n\n        console.log(oTable);\n        var nEditing = null;\n        var nNew = false;\n\n        $('#sample_editable_1_new').click(function (e) {\n            e.preventDefault();\n            document.location.href = \"/cacaotthhFPersonal\";\n        });\n\n        table.on('click', '.status', function (e) {\n            e.preventDefault();\n            if (confirm(\"¿Esta seguro de cambiar el registro?\")) {\n                var nRow = $(this).parents('tr')[0];\n                var aData = oTable.fnGetData(nRow);\n                if (aData.length > 0) {\n                    var data = {\n                        id_personal: aData[0],\n                        status: aData[8]\n                    };\n                    ahttp.post(\"phrapi/changecacao/status\", function (r, b) {\n                        b();\n                        if (r) {\n                            alert(\"Se cambio con \\xE9xito\", '', 'success');\n                            changeStatusRow(oTable, nRow, r.success);\n                        }\n                    }, data);\n                }\n            }\n        });\n\n        table.on('click', '.delete', function (e) {\n            e.preventDefault();\n\n            if (confirm(\"Esta seguro de Eliminar el registro?\") == false) {\n                return;\n            }\n\n            var nRow = $(this).parents('tr')[0];\n            var aData = oTable.fnGetData(nRow);\n            oTable.fnDeleteRow(nRow);\n        });\n\n        table.on('click', '.cancel', function (e) {\n            e.preventDefault();\n            if (nNew) {\n                oTable.fnDeleteRow(nEditing);\n                nEditing = null;\n                nNew = false;\n            } else {\n                restoreRow(oTable, nEditing);\n                nEditing = null;\n            }\n        });\n\n        table.on('click', '.edit', function (e) {\n            e.preventDefault();\n        });\n    };\n\n    return {\n        init: function init() {\n            handleTable();\n        }\n\n    };\n}();\n\napp.controller('informe_tthh', ['$scope', 'client', function ($scope, client) {\n\n    $scope.agregar = function (nombre, color, precio, cantidad) {\n\n        $scope.drogas.push({\n            nombre: nombre,\n            color: color,\n            precio: precio,\n            cant: cantidad\n        });\n    };\n\n    $scope.leyendaGeneralTitle = 'Merma';\n\n    $scope.id_company = 0;\n    $scope.tags = {\n        merma: {\n            value: 0,\n            label: \"\"\n        },\n        enfunde: {\n            value: 0,\n            label: \"\"\n        },\n        campo: {\n            value: 0,\n            label: \"\"\n        },\n        cosecha: {\n            value: 0,\n            label: \"\"\n        },\n        animales: {\n            value: 0,\n            label: \"\"\n        },\n        hongos: {\n            value: 0,\n            label: \"\"\n        },\n        empacadora: {\n            value: 0,\n            label: \"\"\n        },\n        fisiologicos: {\n            value: 0,\n            label: \"\"\n        }\n    };\n\n    $scope.graficas = {\n        historico: {},\n        historico_avg: 0,\n        dia: {},\n        dia_title: \"\",\n        danos: {},\n        danos_detalle: {}\n    };\n\n    $scope.umbrales = {};\n    $scope.calidad = {\n        params: {\n            idFinca: 1,\n            idLote: 0,\n            idLabor: 0,\n            fecha_inicial: '2016-08-01',\n            fecha_final: '2016-09-30',\n            cliente: \"\",\n            marca: \"\",\n            palanca: \"\"\n        },\n        step: 0,\n        path: ['phrapi/personalcacao/index'],\n        templatePath: [],\n        nocache: function nocache() {\n            $scope.tags.merma.value = new Number(98).toFixed(2);\n            $scope.tags.enfunde.value = new Number(90).toFixed(2);\n            $scope.tags.campo.value = new Number(99).toFixed(2);\n            $scope.tags.cosecha.value = new Number(97).toFixed(2);\n\n            setTimeout(function () {\n                $(\".counter_tags\").counterUp({\n                    delay: 10,\n                    time: 1000\n                });\n            }, 1000);\n        }\n\n    };\n\n    $scope.tabla = {\n        detalle: []\n    };\n\n    var detalle = {\n        nombre: \"FRNA\",\n        cedula: \"23423423\",\n        cargo: \"ADMON\",\n        fecha_ingreso: \"2016-09-29\",\n        fecha_salida: \"2017-09-29\",\n        status: \"ACTIVO\",\n        ficha: \"1\"\n\n        // $scope.tabla.detalle.push(detalle)\n\n    };$scope.palancas = [];\n\n    $scope.loadExternal = function () {\n        //console.log($scope.calidad.path[$scope.calidad.step]);\n        if ($scope.calidad.path[$scope.calidad.step] != \"\") {\n            var data = $scope.calidad.params;\n            client.post($scope.calidad.path[$scope.calidad.step], $scope.startDetails, data);\n        }\n    };\n\n    $scope.changeStatus = function () {\n        alert(\"Hola\");\n    };\n\n    $scope.startDetails = function (r, b) {\n        b();\n        if (r) {\n            var options = {};\n            $scope.tags.merma.value = new Number(98).toFixed(2);\n            $scope.tags.enfunde.value = new Number(90).toFixed(2);\n            $scope.tags.campo.value = new Number(99).toFixed(2);\n            $scope.tags.cosecha.value = new Number(97).toFixed(2);\n            $scope.tabla.detalle = r.data || [];\n            setTimeout(function () {\n                // console.log($scope.tags);\n                $(\".counter_tags\").counterUp({\n                    delay: 10,\n                    time: 1000\n                });\n\n                TableDatatablesEditable.init();\n            }, 1000);\n        }\n    };\n\n    $scope.loadExternal();\n}]);\n\n//# sourceURL=webpack:///./js_modules/marun/cacaotthhPersonal.js?");

/***/ })

/******/ });