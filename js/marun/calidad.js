/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./js_modules/marun/calidad.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./js_modules/marun/calidad.js":
/*!*************************************!*\
  !*** ./js_modules/marun/calidad.js ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nfunction graficas() {\n    var self = this;\n    self = {\n        options: {},\n        id: \"\",\n        theme: \"infographic\",\n        required: function required() {\n            if (!this.id) throw Error(\"id es Requida\");\n            if (!this.options) throw Error(\"url es Requido\");\n        }\n    };\n\n    var printGrafica = function printGrafica() {\n        var grafica = new echartsPlv();\n        grafica.init(self.id, self.options);\n    };\n\n    graficas.prototype.create = function (id, options) {\n        self.options = options;\n        self.id = id;\n        self.required();\n        return printGrafica();\n    };\n}\n\nfunction createGraficas(options, id) {\n    var grafica = new graficas();\n    grafica.create(id, options);\n}\n\nfunction calidad(id, options, margen) {\n    var margen = margen;\n    var options_historico = options.historico;\n    var options_historico_legends = options.historico_legends;\n    var category = [];\n    var legend = [];\n    var series = [];\n    var legend = [];\n    var legend_data = [];\n    var series = [];\n    legend = Object.keys(options_historico_legends);\n    for (var i in options_historico) {\n        series.push(options_historico[i]);\n        legend_data.push(options_historico[i].name);\n    }\n\n    var option = {\n        title: {\n            text: 'Calidad Fincas',\n            x: 'center',\n            align: 'right'\n        },\n        grid: {\n            bottom: 80\n        },\n        toolbox: {\n            feature: {\n                dataZoom: {\n                    yAxisIndex: 'none'\n                },\n                magicType: {\n                    type: ['line', 'bar']\n                },\n                restore: {},\n                saveAsImage: {}\n            }\n        },\n        tooltip: {\n            trigger: 'axis',\n            axisPointer: {\n                animation: false\n            }\n        },\n        legend: {\n            data: legend_data,\n            x: 'left'\n        },\n        dataZoom: [{\n            show: true,\n            realtime: true\n            // start: 65,\n            // end: 85\n        }, {\n            type: 'inside',\n            realtime: true\n            // start: 65,\n            // end: 85\n        }],\n        xAxis: [{\n            type: 'category',\n            boundaryGap: true,\n            axisLine: { onZero: true },\n            data: legend\n        }],\n        yAxis: [{\n            type: 'value',\n            min: margen.min,\n            max: margen.max\n        }],\n        series: series\n    };\n\n    createGraficas(option, id);\n}\n\nfunction clusterCahas(id, options, margen) {\n    var margen = margen;\n    var options_historico = options.historico;\n    var options_historico_legends = options.historico_legends;\n    var category = [];\n    var legend = [];\n    var series = [];\n    var legend = [];\n    var legend_data = [];\n    var series = [];\n    legend = Object.keys(options_historico_legends);\n    for (var i in options_historico) {\n        series.push(options_historico[i]);\n        legend_data.push(options_historico[i].name);\n    }\n\n    var option = {\n        title: {\n            text: 'Categorias',\n            x: 'center',\n            align: 'right'\n        },\n        grid: {\n            bottom: 80\n        },\n        toolbox: {\n            feature: {\n                dataZoom: {\n                    yAxisIndex: 'none'\n                },\n                magicType: {\n                    type: ['line', 'bar']\n                },\n                restore: {},\n                saveAsImage: {}\n            }\n        },\n        tooltip: {\n            trigger: 'axis',\n            axisPointer: {\n                animation: false\n            }\n        },\n        legend: {\n            data: legend_data,\n            x: 'left'\n        },\n        dataZoom: [{\n            show: true,\n            realtime: true\n            // start: 65,\n            // end: 85\n        }, {\n            type: 'inside',\n            realtime: true\n            // start: 65,\n            // end: 85\n        }],\n        xAxis: [{\n            type: 'category',\n            boundaryGap: true,\n            axisLine: { onZero: true },\n            data: legend\n        }],\n        yAxis: [{\n            type: 'value',\n            min: margen.min,\n            max: margen.max\n        }],\n        series: series\n    };\n\n    createGraficas(option, id);\n}\n\napp.directive('tooltip', function () {\n    return {\n        restrict: 'A',\n        link: function link(scope, element, attrs) {\n            $(element).hover(function () {\n                // on mouseenter\n                $(element).tooltip('show');\n            }, function () {\n                // on mouseleave\n                $(element).tooltip('hide');\n            });\n        }\n    };\n});\n\napp.filter('orderObjectBy', function () {\n    return function (items, field, reverse) {\n        var filtered = [];\n        angular.forEach(items, function (item) {\n            //// console.log(item)\n            filtered.push(item);\n        });\n        filtered.sort(function (a, b) {\n            //alert(a[field]);\n            return a[field] > b[field] ? 1 : -1;\n        });\n        if (reverse) filtered.reverse();\n        return filtered;\n    };\n});\n\napp.controller('informe_calidad', ['$scope', '$http', '$interval', 'client', '$controller', '$timeout', '$window', function ($scope, $http, $interval, client, $controller, $timeout, $window) {\n    $scope.id_company = 0;\n    $scope.tags = {\n        calidad_maxima: {\n            value: 0,\n            label: \"\"\n        },\n        calidad_minima: {\n            value: 0,\n            label: \"\"\n        },\n        cluster: {\n            value: 0,\n            label: \"\"\n        },\n        desviacion_estandar: {\n            value: 0,\n            label: \"\"\n        },\n        peso: {\n            value: 0,\n            label: \"\"\n        },\n        calidad_dedos: {\n            value: 0,\n            label: \"\"\n        },\n        calidad_cluster: {\n            value: 0,\n            label: \"\"\n        },\n        dedos_promedio: {\n            value: 0,\n            label: \"\"\n        }\n    };\n\n    // 03/03/2017 - TAG: Selects\n    $scope.fincas = [];\n    $scope.fincaSelected = \"\";\n    $scope.exportadores = [];\n    $scope.exportadorSelected = \"\";\n    $scope.clientes = [];\n    $scope.clienteSelected = \"\";\n    $scope.marcas = [];\n    $scope.marcaSelected = \"\";\n    $scope.contenedores = [];\n    $scope.contenedorSelected = \"\";\n    // 03/03/2017 - TAG: Selects\n\n\n    $scope.umbrales = {};\n    $scope.defectos = [];\n    $scope.tittleDefectos = [];\n    $scope.clusters = [];\n    $scope.dedos = [];\n    $scope.categorias = [];\n    $scope.totales = [];\n    $scope.muestras = [];\n\n    $scope.calidad = {\n        params: {\n            idFinca: 0,\n            idExportador: 0,\n            idCliente: 0,\n            idMarca: 0,\n            contenedor: \"\",\n            fecha_inicial: moment().subtract(6, 'days').format('YYYY-MM-DD'),\n            fecha_final: moment().format('YYYY-MM-DD'),\n            cliente: \"\",\n            marca: \"\"\n        },\n        step: 0,\n        templatePath: [],\n        nocache: function nocache() {\n            this.templatePath.push('/views/marun/templetes/calidad/principal.html?' + Math.random());\n        }\n\n        // 22/02/2017 - TAG: Filtros\n    };$scope.paramsGrafica = function () {\n        var response = {\n            graficas: {},\n            margen: {\n                min: 0,\n                max: 10,\n                umbral: 4\n            }\n        };\n        return response;\n    };\n\n    $scope.request = {\n        tags: function tags() {\n            var data = $scope.calidad.params;\n            client.post('phrapi/marun/calidad/home', $scope.printTags, data, \"contentTags\");\n        },\n        principal: function principal() {\n            var data = $scope.calidad.params;\n            client.post('phrapi/marun/calidad/main', $scope.printPrincipal, data, \"contentMain\");\n        },\n        clusterCajas: function clusterCajas() {\n            var data = $scope.calidad.params;\n            client.post('phrapi/marun/calidad/cluster/caja', $scope.printClusterCajas, data, \"contentClusterCaja\");\n        },\n        tableCluster: function tableCluster() {\n            var data = $scope.calidad.params;\n            client.post('phrapi/marun/calidad/table/cluster', $scope.printTableCluster, data, \"clusterContent\");\n        },\n        tableDedos: function tableDedos() {\n            var data = $scope.calidad.params;\n            client.post('phrapi/marun/calidad/table/dedos', $scope.printTableDedos, data, \"dedosConten\");\n        },\n        defectos: function defectos() {\n            var data = $scope.calidad.params;\n            client.post('phrapi/marun/calidad/defectos', $scope.printDefectos, data, \"contentDefectos\");\n        },\n        defectosDetailsTable: function defectosDetailsTable() {\n            var data = $scope.calidad.params;\n            client.post('phrapi/marun/calidad/table/defectos', $scope.printTableSubFinca, data, \"contentTableSubFinca\");\n        },\n        all: function all() {\n            this.tags();\n            this.principal();\n            this.clusterCajas();\n            this.tableCluster();\n            this.tableDedos();\n            this.defectos();\n        }\n\n    };\n\n    $scope.printTags = function (r, b) {\n        b(\"contentTags\");\n        if (r) {\n            if (r.hasOwnProperty(\"tags\")) {\n                $scope.tags.calidad_maxima.value = Math.round(r.tags.calidad_maxima);\n                $scope.tags.calidad_minima.value = Math.round(r.tags.calidad_minima);\n                $scope.tags.cluster.value = new Number(r.tags.cluster).toFixed(2);\n                $scope.tags.desviacion_estandar.value = Math.round(r.tags.desviacion_estandar);\n                $scope.tags.peso.value = Math.round(r.tags.peso);\n                $scope.tags.calidad_dedos.value = Math.round(r.tags.calidad_dedos);\n                $scope.tags.calidad_cluster.value = Math.round(r.tags.calidad_cluster);\n                $scope.tags.dedos_promedio.value = new Number(r.tags.dedos_promedio).toFixed(2);\n                // 22/02/2017 - TAG: FILTROS\n                $scope.fincas = r.filters.fincas || [];\n                $scope.exportadores = r.filters.exportadores || [];\n                $scope.clientes = r.filters.clientes || [];\n                $scope.marcas = r.filters.marcas || [];\n                $scope.contenedores = r.filters.contenedores || [];\n                // 22/02/2017 - TAG: FILTROS\n                setTimeout(function () {\n                    $(\".counter_tags\").counterUp({\n                        delay: 10,\n                        time: 1000\n                    });\n                }, 1000);\n            }\n        }\n    };\n\n    // 22/02/2017 - TAG: Grafica Calidad Finca\n    $scope.printPrincipal = function (r, b) {\n        b(\"contentMain\");\n        var data = $scope.paramsGrafica();\n        if (r) {\n            data.graficas.historico = r.data || [];\n            data.graficas.historico_avg = r.avg || [];\n            data.graficas.historico_legends = r.legend || [];\n            data.margen.min = 80;\n            data.margen.max = 100;\n            calidad(\"principal\", data.graficas, data.margen);\n        }\n    };\n    // 22/02/2017 - TAG: Grafica Cluster por Caja\n    $scope.printClusterCajas = function (r, b) {\n        b(\"contentClusterCaja\");\n        var data = $scope.paramsGrafica();\n        if (r) {\n            data.graficas.historico = r.data || [];\n            data.graficas.historico_avg = r.avg || [];\n            data.graficas.historico_legends = r.legend || [];\n            data.margen.min = 0;\n            data.margen.max = 20;\n            clusterCahas(\"clusterCajas\", data.graficas, data.margen);\n        }\n    };\n    // 22/02/2017 - TAG: Tabla de Clusters\n    $scope.printTableCluster = function (r, b) {\n        b(\"clusterContent\");\n        if (r) {\n            $scope.clusters = r.data || [];\n        }\n    };\n    // 22/02/2017 - TAG: Tabla de Dedos\n    $scope.printTableDedos = function (r, b) {\n        b(\"dedosConten\");\n        if (r) {\n            $scope.dedos = r.data || [];\n        }\n    };\n    // 22/02/2017 - TAG: Grafica de Defectos\n    $scope.printDefectos = function (r, b) {\n        b(\"contentDefectos\");\n        if (r) {\n            $scope.defectos = r.data || {};\n            $scope.categoriasFilters = r.categoriasFilters || {};\n            $scope.categorias = angular.copy(r.categorias) || {};\n            createGraficas($scope.defectos, \"CategoriasDefectos\");\n            var position = Object.keys($scope.categorias)[0];\n            var category = $scope.categorias[position];\n            createGraficas(category, \"defectos\");\n        }\n    };\n    // 24/02/2017 - TAG: Table Sub Finca\n    $scope.printTableSubFinca = function (r, b) {\n        b(\"contentTableSubFinca\");\n        console.log(\"tabla\");\n        if (r) {\n            $scope.tittleDefectos = r.defectos || {};\n            $scope.totales = r.totales || {};\n            $scope.muestras = r.muestras || {};\n            $('.tooltips').tooltip();\n        }\n    };\n\n    // 07/03/2017 - TAG: Change Category\n    cambiosCategoria = function cambiosCategoria() {\n        var position = $(\"#categoria\").val();\n        var category = $scope.categorias[position];\n        createGraficas(category, \"defectos\");\n    };\n\n    // 03/03/2017 - TAG: Select Fincas\n    cambiosFincas = function cambiosFincas() {\n        $scope.calidad.params.palanca = \"\";\n        $scope.calidad.params.idExportador = \"\";\n        $scope.calidad.params.idCliente = \"\";\n        $scope.calidad.params.idMarca = \"\";\n        $scope.request.defectosDetailsTable();\n        $scope.loadExternal();\n    };\n\n    // 03/03/2017 - TAG: Select Expotador\n    cambiosExportador = function cambiosExportador() {\n        $scope.calidad.params.idCliente = \"\";\n        $scope.calidad.params.idMarca = \"\";\n        $scope.request.all();\n    };\n\n    // 03/03/2017 - TAG: Select Cliente\n    cambiosCliente = function cambiosCliente() {\n        $scope.calidad.params.idMarca = \"\";\n        $scope.request.all();\n    };\n\n    // 03/03/2017 - TAG: Select Marca\n    cambiosMarca = function cambiosMarca() {\n        $scope.request.all();\n        //$scope.request.defectosDetailsTable($(\"#idMarca\").val());\n    };\n\n    $scope.changeContenedor = function () {\n        $scope.loadExternal();\n    };\n\n    $scope.openDetalle = function (data) {\n        data.expand = !data.expand;\n    };\n\n    $scope.changeRangeDate = function (data) {\n        if (data) {\n            $scope.calidad.params.fecha_inicial = data.hasOwnProperty(\"first_date\") ? data.first_date : $scope.wizardStep.params.fecha_inicial;\n            $scope.calidad.params.fecha_final = data.hasOwnProperty(\"second_date\") ? data.second_date : $scope.wizardStep.params.fecha_final;\n            $scope.loadExternal();\n        }\n    };\n    $scope.loadExternal = function () {\n        $scope.request.all();\n    };\n\n    $scope.toNumber = function (value) {\n        return parseFloat(value);\n    };\n}]);\n\n//# sourceURL=webpack:///./js_modules/marun/calidad.js?");

/***/ })

/******/ });