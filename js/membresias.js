/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./js_modules/membresias.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./js_modules/membresias.js":
/*!**********************************!*\
  !*** ./js_modules/membresias.js ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nfunction loadGrid(config) {\n    grid.id = config.id;\n    grid.url = config.path;\n    grid.buttons = [{\n        extend: 'print',\n        className: 'btn dark btn-outline',\n        \"title\": \"Listado\",\n        \"text\": \"Imprimir\",\n        customize: function customize(win) {\n            $(win.document.body).css('font-size', '10pt').css('float', 'rigth').prepend('<img src=\"http://orodelti.procesos-iq.com/assets/logo.png\" />');\n\n            $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');\n        }\n    }];\n\n    grid.addButtons({ extend: 'excel', \"title\": \"Listado de Sectores\", \"text\": \"Excel\", className: 'btn yellow btn-outline ',\n        exportOptions: {\n            columns: ':visible'\n        }\n    });\n    grid.addButtons({ extend: 'csv', \"title\": \"Listado de Sectores\", \"text\": \"CSV\", className: 'btn purple btn-outline ',\n        exportOptions: {\n            columns: ':visible'\n        }\n    });\n    grid.init();\n}\n\napp.directive('pluginUniform', function () {\n    return {\n        restrict: 'A',\n        link: function link(scope, element, attrs) {\n            /* element is jQuery object*/\n            element.parents(\".control-label\").css(\"cursor\", \"pointer\");\n        }\n    };\n});\n\napp.controller('membresias', ['$scope', '$http', '$interval', 'client', '$controller', function ($scope, $http, $interval, client, $controller) {\n    $scope.memberships = {\n        params: {\n            idCompany: 0,\n            idUser: 0\n        },\n        button: [\"Nuevo Usuario\", \"Editar Usuario\"],\n        type: 0,\n        path: ['phrapi/membresias/index', 'phrapi/membresias/edit', 'phrapi/membresias/save'],\n        templatePath: ['/views/membresias.html', '/views/membresia.html']\n    };\n\n    $scope.newUsers = function () {\n        setTimeout(function () {\n            $scope.changeTab(1);\n        }, 500);\n    };\n\n    $scope.changeTab = function (tab) {\n        tab = tab || 0;\n        $scope.memberships.type = tab;\n        $scope.memberships.templatePath[$scope.memberships.type] += \"?\" + Math.random();\n        console.log($scope.memberships.templatePath[$scope.memberships.type]);\n    };\n\n    $scope.loadExternal = function () {\n        if ($scope.memberships.path[$scope.memberships.type] != \"\") {\n            $scope.init();\n            var data = $scope.memberships.params;\n            client.post($scope.memberships.path[$scope.memberships.type], $scope.gridDetails, data);\n        }\n        console.log(\"hola\");\n    };\n\n    $scope.tiitle = {\n        tiitle: \"Listado de Membresias\"\n    };\n\n    $scope.dataset = [];\n    $scope.ficha = {\n        id: $scope.memberships.idUser,\n        nombre_medico: '',\n        empresa: '',\n        empleado: '',\n        trueapto_conp: '',\n        trueapto_sin: '',\n        truearea: '',\n        cedula_identidad: '',\n        edad: '',\n        fecha_ingreso_comp: '',\n        fecha_nacimiento: '',\n        apto_sin: '',\n        apto_con: '',\n        apto_conp: '',\n        no_apto: '',\n        recomendaciones: '',\n        sexo: '',\n        puesto_trabajo: ''\n    };\n\n    $scope.init = function () {\n        $('.date-picker').datepicker({\n            rtl: App.isRTL(),\n            autoclose: true\n        });\n    };\n\n    $scope.gridDetails = function (r, b) {\n        console.log(r);\n        b();\n        if (r) {\n            if ($scope.memberships.type == 0) {\n                var data = {\n                    id: \"#usuarios\",\n                    path: $scope.memberships.path[$scope.memberships.type]\n                };\n                loadGrid(data);\n            }\n            if ($scope.memberships.type == 1) {\n                $scope.init();\n                $scope.dataset = r.data;\n            }\n        }\n    };\n\n    $scope.success = function (r, b) {\n        b();\n        if (r) {\n            alert(\"Ficha registrada / modificada con exito\", \"Ficha\", \"success\", function () {\n                // document.location.href = \"/demoList\";\n            });\n        }\n        console.log(r);\n    };\n\n    // $scope.add = function(){\n    //     var data = $scope.ficha;\n    //     if(data.nombre_medico == \"\"){\n    //         alert(\"Favor de ingresar nombre\");\n    //     }       \n    //     else if(data.empresa == \"\"){\n    //         alert(\"Favor de ingresar empresa\");\n    //     }\n    //     else if(data.fecha_since == \"\"){\n    //         alert(\"Favor de ingresar fecha de nacimiento\");\n    //     }\n    //     else if(data.edad == \"\"){\n    //         alert(\"Favor de ingresar edad\");\n    //     }\n    //     else{\n    //         data.details = [];\n    //         $.each($(\".details\"),function (index, element) {\n    //             var id = $(element).prop(\"id\");\n    //             var value = $(element).is(\":checked\") || false;\n    //             var info = {\n    //                 id : id,\n    //                 value : value\n    //             }\n    //             data.details.push(info);\n    //         });\n\n    //         data.radios = [];\n    //         $.each($(\".radios_group\"),function (index, element) {\n    //             var id = $(element).prop(\"id\");\n    //             var value = $(element).is(\":checked\") || false;\n    //             var info = {\n    //                 id : id,\n    //                 value : value\n    //             }\n    //             data[id] = value;\n    //             data.radios.push(info);\n    //         });\n\n    //         client.post('./controllers/index.php?accion=Demo.save' , $scope.success ,data);\n    //     }\n    // }\n\n    // $(\".btnadd\").on(\"click\" , function(){\n    //     $scope.add();\n    // });\n\n    // $(\".cancel\").on(\"click\" , function(){\n    //     document.location.href = \"demoList\";\n    // });\n}]);\n\n//# sourceURL=webpack:///./js_modules/membresias.js?");

/***/ })

/******/ });