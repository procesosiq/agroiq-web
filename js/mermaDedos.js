/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./js_modules/reiset/mermaDedos.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./js_modules/reiset/mermaDedos.js":
/*!*****************************************!*\
  !*** ./js_modules/reiset/mermaDedos.js ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\napp.controller('controller', ['$scope', '$http', function ($scope, $http) {\n\n    $scope.filters = {\n        fecha_inicial: moment().format('YYYY-MM-DD'),\n        fecha_final: moment().format('YYYY-MM-DD')\n    };\n\n    checkUmbral = function checkUmbral(value) {\n        if (parseFloat(value)) {\n            var umbral = $scope.umbral;\n\n            if (value > umbral) {\n                return 'bg-red-thunderbird bg-font-red-thunderbird';\n            } else if (value == umbral) {\n                return 'bg-yellow-gold bg-font-yellow-gold';\n            } else {\n                return 'bg-green-jungle bg-font-green-jungle';\n            }\n        }\n        return '';\n    };\n\n    initTableDedosProm = function initTableDedosProm(r) {\n        var props = {\n            header: [{\n                key: 'lote',\n                name: 'LOTE',\n                titleClass: 'text-center',\n                alignContent: 'center',\n                locked: true,\n                resizable: true,\n                sortable: true,\n                width: 170\n            }, {\n                key: 'avg',\n                name: 'AVG',\n                locked: true,\n                sortable: true,\n                titleClass: 'text-center',\n                alignContent: 'right',\n                formatter: 'Number',\n                resizable: true,\n                customCell: function customCell(rowData, isChildren) {\n                    var valueCell = rowData['avg'];\n                    return '\\n                            <div class=\"text-center ' + checkUmbral(valueCell) + '\" style=\"height: 100%\">\\n                                ' + valueCell + '\\n                            </div>\\n                        ';\n                }\n            }, {\n                key: 'max',\n                name: 'MAX',\n                locked: true,\n                titleClass: 'text-center',\n                sortable: true,\n                alignContent: 'right',\n                resizable: true,\n                customCell: function customCell(rowData, isChildren) {\n                    var valueCell = rowData['max'];\n                    return '\\n                            <div class=\"text-center ' + checkUmbral(valueCell) + '\" style=\"height: 100%\">\\n                                ' + valueCell + '\\n                            </div>\\n                        ';\n                }\n            }, {\n                key: 'min',\n                name: 'MIN',\n                locked: true,\n                sortable: true,\n                titleClass: 'text-center',\n                alignContent: 'right',\n                resizable: true,\n                customCell: function customCell(rowData, isChildren) {\n                    var valueCell = rowData['min'];\n                    return '\\n                            <div class=\"text-center ' + checkUmbral(valueCell) + '\" style=\"height: 100%\">\\n                                ' + valueCell + '\\n                            </div>\\n                        ';\n                }\n            }],\n            data: r.data,\n            buttons: [{\n                title: 'Excel',\n                action: function action() {\n                    table1.exportToExcel();\n                },\n                className: ''\n            }]\n        };\n        Object.keys(r.semanas).map(function (index) {\n            var value = r.semanas[index];\n            props.header.push({\n                key: 'sem_' + value,\n                name: '' + value,\n                sortable: true,\n                alignContent: 'right',\n                titleClass: 'text-center',\n                resizable: true,\n                customCell: function customCell(rowData, isChildren) {\n                    var valueCell = rowData['sem_' + value];\n                    return '\\n                        <div class=\"text-center ' + checkUmbral(valueCell) + '\" style=\"height: 100%\">\\n                            ' + valueCell + '\\n                        </div>\\n                    ';\n                }\n            });\n        });\n        $(\"#table-dedos-prom\").html(\"\");\n        table1 = ReactDOM.render(React.createElement(ReactDataGrid, props), document.getElementById('table-dedos-prom'));\n        table1.handleGridSort('lote', 'ASC');\n    };\n\n    $scope.loadTableDedosProm = function () {\n        $http.post('phrapi/reiset/merma/tableDedosPromedio', $scope.filters).then(function (r) {\n            if (r) {\n                $scope.umbral = r.data.umbral;\n                initTableDedosProm(r.data);\n            }\n        });\n    };\n\n    $scope.init = function () {\n        $scope.loadTableDedosProm();\n    };\n\n    $scope.init();\n}]);\n\n//# sourceURL=webpack:///./js_modules/reiset/mermaDedos.js?");

/***/ })

/******/ });