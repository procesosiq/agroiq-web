/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./js_modules/produccion.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./js_modules/produccion.js":
/*!**********************************!*\
  !*** ./js_modules/produccion.js ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\napp.controller('produccion', ['$scope', '$http', '$interval', 'client', '$controller', '$timeout', '$window', function ($scope, $http, $interval, client, $controller, $timeout, $window) {\n    $scope.id_company = 0;\n    $scope.produccion = {\n        params: {\n            idFinca: 1,\n            idLote: 0,\n            idLabor: 0,\n            fecha_inicial: moment().startOf('month').format('YYYY-MM-DD'),\n            fecha_final: moment().endOf('month').format('YYYY-MM-DD'),\n            cliente: \"\",\n            marca: \"\",\n            palanca: \"\",\n            finca: \"\"\n        },\n        step: 0,\n        path: ['phrapi/produccion/index'],\n        templatePath: [],\n        nocache: function nocache() {\n            $scope.init();\n        }\n    };\n\n    $scope.StartEndDateDirectives = {\n        startDate: moment().startOf('month'),\n        endDate: moment().endOf('month')\n    };\n\n    $scope.changeRangeDate = function (data) {\n        if (data) {\n            // TableDatatablesEditable.restart(\"sample_editable_1\");\n            $scope.produccion.params.fecha_inicial = data.hasOwnProperty(\"first_date\") ? data.first_date : $scope.wizardStep.params.fecha_inicial;\n            $scope.produccion.params.fecha_final = data.hasOwnProperty(\"second_date\") ? data.second_date : $scope.wizardStep.params.fecha_final;\n        }\n        $scope.init();\n    };\n\n    $scope.tabla = {\n        produccion: []\n    };\n\n    $scope.openDetallePalanca = function (data) {\n        data.expanded = !data.expanded;\n    };\n\n    $scope.openDetalleLote = function (data) {\n        data.expanded = !data.expanded;\n    };\n\n    $scope.colores = [];\n    $scope.totales = [];\n    $scope.palancas = [];\n\n    $scope.init = function () {\n        var data = $scope.produccion.params;\n        client.post($scope.produccion.path, $scope.printDetails, data);\n    };\n\n    $scope.start = true;\n\n    $scope.printDetails = function (r, b) {\n        b();\n        if (r) {\n            $scope.tabla.produccion = r.data;\n            $scope.id_company = r.id_company;\n            $scope.colores = r.colores;\n            $scope.colores = r.colores;\n            $scope.palancas = r.palancas;\n            $scope.totales = r.totales;\n            if ($scope.start) {\n                $scope.start = false;\n                setInterval(function () {\n                    $scope.init();\n                }, 60000 * 5);\n            }\n        }\n    };\n}]);\n\n//# sourceURL=webpack:///./js_modules/produccion.js?");

/***/ })

/******/ });