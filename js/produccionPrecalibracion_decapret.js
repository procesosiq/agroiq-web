/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./js_modules/marcel/produccionPrecalibracion_decapret.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./js_modules/marcel/produccionPrecalibracion_decapret.js":
/*!****************************************************************!*\
  !*** ./js_modules/marcel/produccionPrecalibracion_decapret.js ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\napp.filter('sumOfValue', function () {\n    return function (data, key) {\n        if (angular.isUndefined(data) || angular.isUndefined(key)) return 0;\n        var sum = 0;\n        angular.forEach(data, function (value) {\n            if (parseFloat(value[key])) sum = sum + parseFloat(value[key], 10);\n        });\n        return sum;\n    };\n});\n\napp.filter('orderObjectBy', function () {\n    return function (items, field, reverse) {\n        var filtered = [];\n        angular.forEach(items, function (item) {\n            if (!isNaN(parseInt(item))) {\n                item = parseInt(item);\n            }\n            filtered.push(item);\n        });\n        filtered.sort(function (a, b) {\n            if (field == 'hora' || field == 'fecha') {\n                return moment(a.date).isAfter(b.date) ? 1 : -1;\n            } else if (parseFloat(a[field]) && parseFloat(b[field])) {\n                return parseFloat(a[field]) > parseFloat(b[field]) ? 1 : -1;\n            } else {\n                return a[field] > b[field] ? 1 : -1;\n            }\n        });\n        if (reverse) filtered.reverse();\n        return filtered;\n    };\n});\n\napp.filter('orderBy', function () {\n    return function (items, reverse) {\n        var filtered = [];\n        angular.forEach(items, function (item) {\n            if (!isNaN(parseInt(item))) {\n                item = parseInt(item);\n            }\n            filtered.push(item);\n        });\n        filtered.sort(function (a, b) {\n            if (parseFloat(a) && parseFloat(b)) {\n                return parseFloat(a) > parseFloat(b) ? 1 : -1;\n            } else {\n                return a > b ? 1 : -1;\n            }\n        });\n        if (reverse) filtered.reverse();\n        return filtered;\n    };\n});\n\napp.service('request', ['$http', function ($http) {\n    this.last = function (callback, params) {\n        $http.post('phrapi/marcel/precalibracion/last', params || {}).then(function (r) {\n            callback(r.data);\n        });\n    };\n    this.precalibracion = function (callback, params) {\n        $http.post('phrapi/marcel/precalibracion/index', params || {}).then(function (r) {\n            callback(r.data);\n        });\n    };\n}]);\n\napp.controller('produccion', ['$scope', 'request', '$filter', function ($scope, $request, $filter) {\n\n    $scope.totales = {};\n    $scope.filters = {\n        year: moment().year(),\n        week: moment().week()\n    };\n\n    $scope.coloresClass = {\n        'VERDE': 'bg-green-haze bg-font-green-haze',\n        'AZUL': 'bg-blue bg-font-blue',\n        'BLANCO': 'bg-white bg-font-white',\n        'LILA': 'bg-purple-studio bg-font-purple-studio',\n        'ROJA': 'bg-red-thunderbird bg-font-red-thunderbird',\n        'CAFE': 'bg-brown bg-font-brown',\n        'AMARILLO': 'bg-yellow-lemon bg-font-yellow-lemon',\n        'NEGRO': 'bg-dark bg-font-dark'\n    };\n\n    $scope.init = function () {\n        load.block('precalibracion');\n        $request.precalibracion(function (r) {\n            load.unblock('precalibracion');\n            $scope.colorClass = r.color.class;\n            $scope.edades = r.edades;\n            $scope.precalibracion = r.data;\n            $scope.reloadTotales();\n        }, $scope.filters);\n    };\n\n    getUmbral = function getUmbral(value) {\n        if (value) {\n            if (value < 98) return 'bg-red-thunderbird bg-font-red-thunderbird';else if (value == 98) return 'bg-yellow-gold bg-font-yellow-gold';else return 'bg-green-haze bg-font-green-haze';\n        }\n    };\n    $scope.getUmbral = getUmbral;\n\n    $scope.reloadTotales = function () {\n        setTimeout(function () {\n            $scope.$apply(function () {\n                $scope.totales.racimos_enfunde = $filter('sumOfValue')($scope.precalibracion, 'racimos_enfunde');\n                $scope.totales.recobro = $scope.totales.cosechados > 0 && $scope.totales.racimos_enfunde > 0 ? $scope.totales.cosechados / $scope.totales.racimos_enfunde * 100 : 0;\n                $scope.totales.caidos = $filter('sumOfValue')($scope.precalibracion, 'caidos') || 0;\n                $scope.totales.porc_caidos = $scope.totales.caidos > 0 && $scope.totales.racimos_enfunde > 0 ? $scope.totales.caidos / $scope.totales.racimos_enfunde * 100 : 0;\n            });\n        }, 100);\n    };\n\n    $scope.last = function () {\n        $request.last(function (r) {\n            $scope.years = r.years;\n            $scope.semanas = r.weeks;\n            $scope.filters.week = r.last_week;\n            $scope.init();\n        }, $scope.filters);\n    };\n\n    $scope.last();\n}]);\n\n//# sourceURL=webpack:///./js_modules/marcel/produccionPrecalibracion_decapret.js?");

/***/ })

/******/ });