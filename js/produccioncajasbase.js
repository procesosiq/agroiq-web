/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./js_modules/produccioncajasbase.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./js_modules/produccioncajasbase.js":
/*!*******************************************!*\
  !*** ./js_modules/produccioncajasbase.js ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nvar _window = window,\n    app = _window.app,\n    load = _window.load;\n\n\napp.service('request', ['client', '$http', function (client, $http) {\n    this.registros = function (callback, params) {\n        var url = 'phrapi/marun/cajas/registros';\n        var data = params || {};\n        client.post(url, callback, data, 'tabla_base');\n    };\n\n    this.last = function (callback, params) {\n        var url = 'phrapi/marun/cajas/last';\n        var data = params || {};\n        client.post(url, callback, data);\n    };\n\n    this.getMarcas = function (callback, params) {\n        var url = 'phrapi/marun/cajas/marcas';\n        var data = params || {};\n        client.post(url, callback, data);\n    };\n\n    this.eliminar = function (callback, params) {\n        var url = 'phrapi/marun/cajas/eliminar';\n        var data = params || {};\n        client.post(url, callback, data);\n    };\n\n    this.guardarCaja = function (callback, params) {\n        var url = 'phrapi/marun/cajas/guardarCaja';\n        var data = params || {};\n        client.post(url, callback, data);\n    };\n}]);\n\napp.filter('sumOfValue', function () {\n    return function (data, key) {\n        if (angular.isUndefined(data) || angular.isUndefined(key)) return 0;\n        var sum = 0;\n        angular.forEach(data, function (value) {\n            if (value[key] != \"\" && value[key] != undefined) {\n                sum = sum + parseFloat(value[key], 10);\n                count++;\n            }\n        });\n        return sum;\n    };\n});\n\napp.filter('avgOfValue', function () {\n    return function (data, key) {\n        if (angular.isUndefined(data) || angular.isUndefined(key)) return 0;\n        var sum = 0;\n        var count = 0;\n        angular.forEach(data, function (value) {\n            if (value[key] != \"\" && value[key] != undefined) {\n                sum = sum + parseFloat(value[key], 10);\n                count++;\n            }\n        });\n        sum = sum / count;\n        if (isNaN(sum)) return 0;\n        return sum;\n    };\n});\n\napp.filter('orderObjectBy', function () {\n    return function (items, field, reverse) {\n        var filtered = [];\n        angular.forEach(items, function (item) {\n            if (field == 'hora' || field == 'fecha') {\n                item.date = moment(item.fecha + ' ' + item.hora);\n            }\n            if (!isNaN(parseInt(item))) {\n                item = parseInt(item);\n            }\n            filtered.push(item);\n        });\n        filtered.sort(function (a, b) {\n            if (field == 'hora' || field == 'fecha') {\n                return moment(a.date).isAfter(b.date) ? 1 : -1;\n            } else if (parseFloat(a[field]) && parseFloat(b[field])) {\n                return parseFloat(a[field]) > parseFloat(b[field]) ? 1 : -1;\n            } else {\n                return a[field] > b[field] ? 1 : -1;\n            }\n        });\n        if (reverse) filtered.reverse();\n        return filtered;\n    };\n});\n\nfunction in_array(value, array) {\n    if (!Array.isArray(array)) return false;\n    return array.indexOf(value) >= 0;\n}\n\nfunction getParam(name) {\n    var url_string = window.location.href;\n    var url = new URL(url_string);\n    return url.searchParams.get(name);\n}\n\nvar datesEnabled = [];\n\napp.controller('produccion', ['$scope', 'request', '$filter', function ($scope, $request, $filter) {\n\n    var datepickerHighlight = function datepickerHighlight() {\n        $('#datepicker').datepicker({\n            language: 'es',\n            beforeShowDay: function beforeShowDay(date) {\n                var fecha = moment(date).format('YYYY-MM-DD');\n                var has = datesEnabled.indexOf(fecha) > -1;\n                return has ? { classes: 'highlight', tooltip: 'Procesado', enabled: true } : { tooltip: 'Sin proceso', enabled: false };\n            }\n        });\n        $('#datepicker').datepicker().on('changeDate', function (e) {\n            $scope.table.fecha_inicial = moment(e.date).format('YYYY-MM-DD');\n            $scope.changeRangeDate();\n        });\n        $(\"#datepicker\").datepicker('setDate', $scope.table.fecha_inicial);\n        $('#datepicker').datepicker('update');\n    };\n\n    $scope.registros = [];\n    $scope.seleccionados = [];\n    $scope.editar = {\n        id_marca: '',\n        peso: 0\n    };\n    $scope.table = {\n        pagination: 10,\n        startFrom: 0,\n        actualPage: 1,\n        fecha_inicial: moment().format('YYYY-MM-DD'),\n        fecha_final: moment().format('YYYY-MM-DD'),\n        search: {},\n        year: moment().year(),\n        semana: moment().week(),\n        unidad: 'lb',\n        finca: 0,\n        var: 'CONV'\n    };\n\n    $scope.changeRangeDate = function () {\n        $scope.table.fecha_final = $scope.table.fecha_inicial;\n        $scope.getRegistros();\n    };\n\n    $scope.next = function (dataSource) {\n        if ($scope.searchTable.actual_page < $scope.searchTable.numPages) {\n            $scope.searchTable.actual_page++;\n            $scope.searchTable.startFrom = ($scope.searchTable.actual_page - 1) * parseInt($scope.searchTable.limit);\n        }\n    };\n\n    $scope.prev = function (dataSource) {\n        if ($scope.searchTable.actual_page > 1) {\n            $scope.searchTable.actual_page--;\n            $scope.searchTable.startFrom = ($scope.searchTable.actual_page - 1) * parseInt($scope.searchTable.limit);\n        }\n    };\n\n    $scope.setOrderTable = function (field) {\n        var reverse = false;\n        if (field == $scope.searchTable.orderBy) {\n            reverse = !$scope.searchTable.reverse;\n        } else {\n            $scope.searchTable.orderBy = field;\n        }\n        $scope.searchTable.reverse = reverse;\n\n        $scope.registros = $filter('orderObjectBy')($scope.registros, $scope.searchTable.orderBy, $scope.searchTable.reverse);\n    };\n\n    $scope.getRegistros = function () {\n        $scope.seleccionados = [];\n\n        var data = $scope.table;\n        load.block('marcas');\n        $request.registros(function (r, b) {\n            b('tabla_base');\n            load.unblock('marcas');\n            if (r) {\n                $scope.table.finca = r.id_finca;\n                $scope.fincas = r.fincas;\n                $scope.marcas = r.marcas;\n                $scope.peso_prom_cajas = r.pesos_prom_cajas;\n                $scope.registros = r.data;\n                $scope.table.unidad = r.unidad;\n                setTimeout(function () {\n                    $scope.searchTable.changePagination();\n                }, 500);\n            }\n        }, data);\n    };\n\n    $scope.eliminar = function () {\n        if ($scope.seleccionados.length > 0) {\n            if (confirm('Seguro deseas eliminar ' + $scope.seleccionados.length + ' elementos?')) {\n                $request.eliminar(function (r, b) {\n                    b();\n                    if (r) {\n                        $scope.getRegistros();\n                        alert('Se borraron ' + $scope.seleccionados.length + ' registros con \\xE9xito', \"\", \"success\");\n                    }\n                }, { ids: $scope.seleccionados });\n            }\n        } else {\n            alert(\"No has seleccionado ninguna fila\");\n        }\n    };\n\n    $scope.editar = function (row, index) {\n        var value = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;\n\n        row.delete = value != null ? value : !row.delete;\n        row._delete = angular.copy(row.delete);\n\n        if (!row.delete && $scope.seleccionados.includes(row.id)) {\n            $scope.seleccionados.splice($scope.seleccionados.indexOf(row.id), 1);\n        } else if (row.delete) {\n            $scope.seleccionados.push(row.id);\n        }\n    };\n\n    $scope.seleccionarTodo = function () {\n        var listado = $scope.registros;\n        listado = $filter('filter')(listado, $scope.table.search);\n        listado = $filter('startFrom')(listado, $scope.searchTable.startFrom);\n        listado = $filter('limitTo')(listado, $scope.searchTable.limit);\n\n        for (var i in $scope.registros) {\n            $scope.registros[i].delete = false;\n        }\n        for (var _i in listado) {\n            var row = listado[_i];\n            for (var j in $scope.registros) {\n                var row2 = $scope.registros[j];\n                if (row.id && row2.id && row.id == row2.id) {\n                    $scope.editar(row2, _i, true);\n                    break;\n                }\n            }\n        }\n    };\n\n    $scope.deseleccionarTodo = function () {\n        var listado = $scope.registros;\n        listado = $filter('filter')(listado, $scope.table.search);\n        listado = $filter('startFrom')(listado, $scope.searchTable.startFrom);\n        listado = $filter('limitTo')(listado, $scope.searchTable.limit);\n\n        for (var i in $scope.registros) {\n            $scope.registros[i].delete = false;\n        }\n        for (var _i2 in listado) {\n            var row = listado[_i2];\n            for (var j in $scope.registros) {\n                var row2 = $scope.registros[j];\n                if (row.id == row2.id) {\n                    console.log(row.id, row2.id);\n                    $scope.editar(row2, _i2, false);\n                    break;\n                }\n            }\n        }\n    };\n\n    $scope.guardar = function () {\n        var rows = $(\"tr.delete\").length;\n        if (rows == $scope.seleccionados.length) {\n            if (confirm('Seguro deseas editar ' + $scope.seleccionados.length + ' elementos?')) {\n                $request.guardarCaja(function (r, b) {\n                    b();\n                    if (r.status == 200) {\n                        $(\"#editar-modal\").modal('hide');\n                        toastr.success('Guardado');\n                        $scope.getRegistros();\n                    } else {\n                        alert(r.message);\n                    }\n                }, { ids: $scope.seleccionados, id_marca: $scope.editar.id_marca, peso: $scope.editar.peso });\n            }\n        } else {\n            alert('Algo inesperado paso, favor refresque (F5)');\n        }\n    };\n\n    $scope.last = function (newDate, fecha) {\n        return new Promise(function (resolve) {\n            $request.last(function (r, b) {\n                b();\n                if (r) {\n                    $scope.fincas = r.fincas;\n                    $scope.available_fincas = r.available_fincas;\n\n                    if (Object.keys(r.fincas).length > 0) if (Object.keys(r.fincas).indexOf($scope.table.finca) == -1) {\n                        $scope.table.finca = Object.keys(r.fincas)[0];\n                        $scope.anioCambio = true;\n                    }\n\n                    if (r.days) {\n                        datesEnabled = r.days;\n                    }\n\n                    if (newDate) {\n                        $scope.table.fecha_inicial = fecha || r.last.fecha;\n                        $scope.table.fecha_final = fecha || r.last.fecha;\n                        $(\"#date-picker\").html($scope.table.fecha_inicial + ' - ' + $scope.table.fecha_final);\n                        $scope.anioCambio = true;\n                    }\n                }\n\n                resolve();\n            }, { newDate: newDate, filters: $scope.table });\n        });\n    };\n\n    $scope.searchTable = {\n        orderBy: \"hora\",\n        reverse: true,\n        limit: 10,\n        actual_page: 1,\n        startFrom: 0,\n        optionsPagination: [10, 50, 100],\n        numPages: 0,\n        calculatePages: function calculatePages() {\n            $scope.searchTable.numPages = parseInt($filter('filter')($scope.registros, $scope.table.search).length / $scope.searchTable.limit) + ($filter('filter')($scope.registros, $scope.table.search).length % $scope.searchTable.limit == 0 ? 0 : 1);\n        },\n        changePagination: function changePagination() {\n            if ($scope.searchTable.limit == 0) $scope.searchTable.limit = $scope.registros.length;\n\n            setTimeout(function () {\n                $scope.$apply(function () {\n                    $scope.searchTable.numPages = parseInt($filter('filter')($scope.registros, $scope.table.search).length / $scope.searchTable.limit) + ($filter('filter')($scope.registros, $scope.table.search).length % $scope.searchTable.limit == 0 ? 0 : 1);\n                    $scope.searchTable.actual_page = 1;\n                    $scope.searchTable.startFrom = 0;\n                });\n            }, 250);\n        }\n    };\n\n    $scope.getClassUmbral = function (marca) {\n        if (marca.class != '') {\n            return marca.class;\n        } else {\n            return '';\n        }\n    };\n\n    $scope.exportExcel = function (id_table) {\n        var data = new Blob([document.getElementById(id_table).outerHTML], {\n            type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf8'\n        });\n        saveAs(data, 'Cajas.xls');\n    };\n\n    $scope.exportPrint = function (id_table) {\n\n        var image = '<div style=\"display:block; height:55;\"><img style=\"float: right;\" width=\"100\" height=\"50\" src=\"./../logos/Logo.png\" /></div><br>';\n        var table = document.getElementById(id_table);\n        var contentTable = table.outerHTML;\n\n        if (id_table == 'table_1') {\n            var cut = contentTable.search('<tr role=\"row\" class=\"filter\">');\n            var cut2 = contentTable.search('</thead>');\n            var part1 = contentTable.substring(0, cut);\n            var part2 = contentTable.substring(cut2, contentTable.length);\n            contentTable = part1 + part2;\n        }\n\n        newWin = window.open(\"\");\n        newWin.document.write(contentTable);\n        newWin.print();\n        newWin.close();\n    };\n\n    $scope.hideActions = function () {\n        $(\"#actions-listado\").addClass(\"hide\");\n    };\n    $scope.showActions = function () {\n        $(\"#actions-listado\").removeClass(\"hide\");\n    };\n    $scope.not_selected_marca = function (index) {\n        var array = [];\n        $scope.marcasSeleccionadas.map(function (value, i) {\n            if (i != index) array.push(value);\n        });\n        return in_array($scope.marcasSeleccionadas[index], array);\n    };\n\n    var fecha = getParam('fecha');\n    $scope.last(true, fecha).then(function () {\n        datepickerHighlight();\n    });\n}]);\n\n//# sourceURL=webpack:///./js_modules/produccioncajasbase.js?");

/***/ })

/******/ });