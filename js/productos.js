/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./js_modules/marcel/productos.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./js_modules/marcel/productos.js":
/*!****************************************!*\
  !*** ./js_modules/marcel/productos.js ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\napp.filter('startfrom', function () {\n    return function (input, start) {\n        if (input === undefined) {\n            return \"loading...\";\n        }\n        start = +start; //parse to int\n        return input.slice(start);\n    };\n});\n\napp.filter('num', function () {\n    return function (input) {\n        return parseInt(input, 10);\n    };\n});\n\napp.service('productos', function ($http) {\n    var service = {};\n    var options = {\n        url: \"./controllers/index.php\",\n        method: \"POST\"\n    };\n\n    service.listado = function () {\n        var option = options;\n        option.data = {\n            accion: \"Productos\"\n        };\n        return $http(option);\n    };\n\n    service.formuladoras = function () {\n        var option = options;\n        option.data = {\n            accion: \"Productos.getFormuladoras\"\n        };\n        return $http(option);\n    };\n\n    service.proveedores = function () {\n        var option = options;\n        option.data = {\n            accion: \"Productos.getProveedores\"\n        };\n        return $http(option);\n    };\n\n    service.tipoProductos = function () {\n        var option = options;\n        option.data = {\n            accion: \"Productos.getTipoProductos\"\n        };\n        return $http(option);\n    };\n\n    service.frac = function () {\n        var option = options;\n        option.data = {\n            accion: \"Productos.getFrac\"\n        };\n        return $http(option);\n    };\n\n    // 26/04/2017 - TAG: PRECIOS\n    service.precios = function (id_producto) {\n        var option = options;\n        option.data = {\n            accion: \"Productos.getPrecios\",\n            id_producto: id_producto\n        };\n        return $http(option);\n    };\n    service.addPrecios = function (precios) {\n        var option = options;\n        precios.accion = \"Productos.savePrecios\";\n        option.data = precios;\n        return $http(option);\n    };\n\n    service.acciones = function () {\n        var option = options;\n        option.data = {\n            accion: \"Productos.getAccion\"\n        };\n        return $http(option);\n    };\n\n    service.show = function (id_producto) {\n        var option = options;\n        option.data = {\n            accion: \"Productos.show\",\n            id_producto: id_producto\n        };\n        return $http(option);\n    };\n\n    service.create = function (producto) {\n        var option = options;\n        producto.accion = \"Productos.create\";\n        option.data = producto;\n        return $http(option);\n    };\n\n    service.update = function (producto) {\n        var option = options;\n        producto.accion = \"Productos.update\";\n        option.data = producto;\n        return $http(option);\n    };\n\n    return service;\n});\n\napp.service('configuracion', function ($http) {\n    var configuracion = {};\n    var options = {\n        url: \"./controllers/index.php\",\n        method: \"POST\"\n    };\n\n    configuracion.save = function (detalle) {\n        var option = options;\n        detalle.accion = \"Productos.saveConfiguracion\";\n        option.data = detalle;\n        return $http(option);\n    };\n\n    return configuracion;\n});\n\napp.controller('productos', ['$scope', '$http', '$interval', 'productos', '$controller', 'configuracion', function ($scope, $http, $interval, productos, $controller, configuracion) {\n\n    // 26/04/2017 - TAG: CONFIGURACION\n    $scope.configuracion = {\n        state: \"listado\",\n        modo: \"\",\n        nombre: \"\",\n        campo: \"\",\n        save: function save() {\n            if (this.campo != \"\") {\n                var nombre = this.nombre;\n                var modo = this.modo;\n                var campo = this.campo;\n                var data = {\n                    nombre: nombre,\n                    modo: modo,\n                    campo: campo\n                };\n                $('#configuracion').modal('hide');\n                configuracion.save(data).then(function (data) {\n                    if ($scope.configuracion.state != \"listado\") {\n                        if ($scope.configuracion.nombre == \"Formuladora\") $scope.getFormuladoras();\n                        if ($scope.configuracion.nombre == \"Proveedor\") $scope.getProveedores();\n                        if ($scope.configuracion.nombre == \"Tipo de Producto\") $scope.getTipoProductos();\n                    }\n                }).catch(function (error) {\n                    return console.log(error);\n                });\n            }\n        }\n    };\n\n    $scope.setTipo = function (tipo) {\n        $scope.configuracion.nombre = tipo;\n        if ($scope.configuracion.nombre == \"Formuladora\") $scope.configuracion.modo = \"formuladora\";\n        if ($scope.configuracion.nombre == \"Proveedor\") $scope.configuracion.modo = \"proveedor\";\n        if ($scope.configuracion.nombre == \"Tipo de Producto\") $scope.configuracion.modo = \"tipo_producto\";\n    };\n    // 26/04/2017 - TAG: CONFIGURACION\n    $scope.formuladoras = [];\n    $scope.proveedores = [];\n    $scope.tipoProductos = [];\n    $scope.ingredientes_actios = [];\n    $scope.fracs = [];\n    $scope.acciones = [];\n\n    // 26/04/2017 - TAG: PRECIO\n    $scope.precio = {\n        precio: \"\",\n        fecha: \"\"\n    };\n\n    $scope.precios = [];\n\n    // 25/04/2017 - TAG: MODEL FOR PRODUCTS\n    $scope.clear = function () {\n        $scope.producto = $scope.oldProducto;\n        $scope.precio.precio = \"\";\n        $scope.precio.fecha = \"\";\n    };\n\n    $scope.producto = {\n        id_producto: 0,\n        codigo: \"\",\n        nombreComercial: \"\",\n        formuladora: \"\",\n        proveedor: \"\",\n        tipo_producto: \"\",\n        ingrediente_activo: \"\",\n        frac: \"\",\n        action: \"\",\n        dosis: \"\",\n        dosis_2: \"\",\n        dosis_3: \"\"\n    };\n\n    $scope.oldProducto = angular.copy($scope.producto);\n\n    $scope.proccess = function (r) {\n        if (r.hasOwnProperty(\"data\")) {\n            return r.data;\n        }\n    };\n\n    $scope.getFormuladoras = function () {\n        productos.formuladoras().then(function (data) {\n            return $scope.formuladoras = $scope.proccess(data);\n        }).catch(function (error) {\n            return console.log(error);\n        });\n    };\n\n    $scope.getProveedores = function () {\n        productos.proveedores().then(function (data) {\n            return $scope.proveedores = $scope.proccess(data);\n        }).catch(function (error) {\n            return console.log(error);\n        });\n    };\n\n    $scope.getTipoProductos = function () {\n        productos.tipoProductos().then(function (data) {\n            return $scope.tipoProductos = $scope.proccess(data);\n        }).catch(function (error) {\n            return console.log(error);\n        });\n    };\n\n    $scope.frac = function () {\n        productos.frac().then(function (data) {\n            return $scope.fracs = $scope.proccess(data);\n        }).catch(function (error) {\n            return console.log(error);\n        });\n    };\n\n    $scope.getAcciones = function () {\n        productos.acciones().then(function (data) {\n            return $scope.acciones = $scope.proccess(data);\n        }).catch(function (error) {\n            return console.log(error);\n        });\n    };\n\n    $scope.getPrecios = function () {\n        productos.precios($scope.producto.id_producto).then(function (data) {\n            var response = $scope.proccess(data);\n            if (response.hasOwnProperty(\"data\")) {\n                $scope.precios = response.data;\n            }\n        }).catch(function (error) {\n            return console.log(error);\n        });\n    };\n\n    $scope.show = function () {\n        productos.show($scope.producto.id_producto).then(function (data) {\n            var response = $scope.proccess(data);\n            if (response.hasOwnProperty(\"success\") && response.success == 200) {\n                response.data.dosis = parseFloat(response.data.dosis);\n                response.data.dosis_2 = parseFloat(response.data.dosis_2);\n                response.data.dosis_3 = parseFloat(response.data.dosis_3);\n\n                $scope.producto = response.data;\n                $scope.getPrecios();\n            }\n        }).catch(function (error) {\n            return console.log(error);\n        });\n    };\n\n    $scope.init = function () {\n        $scope.getFormuladoras();\n        $scope.getProveedores();\n        $scope.getTipoProductos();\n        $scope.frac();\n        $scope.getAcciones();\n        $scope.show();\n        if ($('.date-picker').length > 0) {\n            $('.date-picker').datepicker({\n                rtl: App.isRTL(),\n                autoclose: true\n            });\n        }\n\n        $scope.configuracion.state = \"producto\";\n    };\n\n    // 26/04/2017 - TAG: PRECIO\n    $scope.add = function () {\n        var precio = parseFloat($scope.precio.precio);\n        var fecha = $scope.precio.fecha;\n        if (isNaN(precio)) {\n            alert(\"Favor de ingresar un precio valido\");\n        } else if (precio <= 0) {\n            alert(\"El precio debe ser mayor a 0\");\n        } else {\n            var data = {\n                precio: precio,\n                fecha: fecha,\n                id_producto: $scope.producto.id_producto\n            };\n            $scope.precio.precio = \"\";\n            $scope.precio.fecha = \"\";\n            productos.addPrecios(data).then(function (data) {\n                var response = $scope.proccess(data);\n                if (!response.hasOwnProperty(\"error\")) {\n                    $scope.precios = response.data;\n                }\n            }).catch(function (error) {\n                return console.log(error);\n            });\n        }\n    };\n\n    $scope.cancel = function () {\n        var tab = \"listado\";\n        $scope.producto.id_producto = 0;\n        $('.nav-tabs a[href=\"#' + tab + '\"]').tab('show');\n        $scope.clear();\n        $scope.index();\n    };\n\n    $scope.save = function () {\n        if (!angular.isObject($scope.producto)) {\n            return false;\n        } else if ($scope.producto.nombreComercial.length <= 0 || $scope.producto.nombreComercial == \"\") {\n            alert(\"Favor de ingresar Nombre de Producto\");\n        } else if ($scope.producto.proveedor.length <= 0 || $scope.producto.proveedor == \"\") {\n            alert(\"Favor de ingresar Nombre del Proveedor\");\n        } else if ($scope.producto.tipo_producto.length <= 0 || $scope.producto.tipo_producto == \"\") {\n            alert(\"Favor de ingresar Tipo de Producto\");\n        } else if (isNaN(parseFloat($scope.producto.dosis)) || parseFloat($scope.producto.dosis) <= 0) {\n            alert(\"Favor de ingresar Dosis\");\n            // } else if ($scope.producto.frac.length <= 0 || $scope.producto.frac == \"\") {\n            //     alert(\"Favor de ingresar FRAC\");\n            // } else if ($scope.producto.action.length <= 0 || $scope.producto.action == \"\") {\n            //     alert(\"Favor de ingresar Acción\");\n        } else {\n            if ($scope.producto.id_producto <= 0) {\n                productos.create($scope.producto).then(function (data) {\n                    return $scope.success(data);\n                }).catch(function (error) {\n                    return console.log(error);\n                });\n            } else {\n                productos.update($scope.producto).then(function (data) {\n                    return $scope.success(data);\n                }).catch(function (error) {\n                    return console.log(error);\n                });\n            }\n        }\n    };\n\n    $scope.success = function (r) {\n        var response = $scope.proccess(r);\n        $scope.producto.id_producto = response.data;\n        $scope.producto.codigo = response.data;\n        alert(\"Registro registrado/modificado con el ID \" + $scope.producto.id_producto, \"Productos\", \"success\");\n    };\n\n    $scope.getProduct = function (id_producto) {\n        var tab = \"createproductos\";\n        $scope.clear();\n        $scope.producto.id_producto = id_producto || 0;\n        $('.nav-tabs a[href=\"#' + tab + '\"]').tab('show');\n        $scope.init();\n    };\n\n    // 25/04/2017 - TAG: SECCION FOR TABLE \n    $scope.table = [];\n    $scope.index = function () {\n        productos.listado().then(function (data) {\n            return $scope.table = $scope.proccess(data);\n        }).catch(function (error) {\n            return console.log(error);\n        });\n        $scope.configuracion.state = \"listado\";\n    };\n\n    //parametros de busqueda\n    $scope.search = {\n        nombre: \"\",\n        orderBy: \"codigo\",\n        reverse: false,\n        limit: 10,\n        actual_page: 1 // las paginas comienzan apartir del 1\n    };\n\n    //ordenamiento por columnas\n    $scope.changeSort = function (column) {\n        if ($scope.search.orderBy != column) {\n            var previous = $(\"th.selected\")[0];\n            $(previous).removeClass(\"selected\");\n            $(previous).removeClass(\"sorting_asc\");\n            $(previous).removeClass(\"sorting_desc\");\n        }\n        $scope.search.reverse = $scope.search.orderBy != column ? false : !$scope.search.reverse;\n        $scope.search.orderBy = column;\n        var actual_select = $(\"#\" + column + \"_column\");\n        if (!actual_select.hasClass(\"selected\")) {\n            actual_select.addClass(\"selected\");\n        }\n        if ($scope.search.reverse) {\n            actual_select.addClass(\"sorting_desc\");\n            actual_select.removeClass(\"sorting_asc\");\n        } else {\n            actual_select.addClass(\"sorting_asc\");\n            actual_select.removeClass(\"sorting_desc\");\n        }\n    };\n\n    //ir a la siguiente pagina\n    $scope.next = function (dataSource) {\n        if ($scope.search.actual_page < parseInt(dataSource.length / parseInt($scope.search.limit)) + (dataSource.length % parseInt($scope.search.limit) == 0 ? 0 : 1)) $scope.search.actual_page++;\n    };\n\n    //ir a la pagina anterior\n    $scope.prev = function (dataSource) {\n        if ($scope.search.actual_page > 1) $scope.search.actual_page--;\n    };\n}]);\n\n//# sourceURL=webpack:///./js_modules/marcel/productos.js?");

/***/ })

/******/ });