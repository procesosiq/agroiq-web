/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./js_modules/quintana/laboresAgricolasComparativo.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./js_modules/quintana/laboresAgricolasComparativo.js":
/*!************************************************************!*\
  !*** ./js_modules/quintana/laboresAgricolasComparativo.js ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nfunction arrayMin(arr, key) {\n    var min = null;\n    arr.forEach(function (val) {\n        if (val[key]) {\n            if (min == null || val[key] < min) min = val[key];\n        }\n    });\n    return min;\n}\n\napp.service('request', ['$http', function ($http) {\n    var service = {};\n\n    service.last = function (callback, params) {\n        load.block();\n        $http.post('phrapi/laboresAgricolasComparativo/last', params || {}).then(function (r) {\n            load.unblock();\n            callback(r.data);\n        });\n    };\n\n    service.variables = function (callback, params) {\n        load.block();\n        $http.post('phrapi/laboresAgricolasComparativo/variables', params || {}).then(function (r) {\n            load.unblock();\n            callback(r.data);\n        });\n    };\n\n    service.index = function (callback, params) {\n        load.block();\n        $http.post('phrapi/laboresAgricolasComparativo/index', params || {}).then(function (r) {\n            load.unblock();\n            callback(r.data);\n        });\n    };\n\n    service.tablaPorCausa = function (callback, params) {\n        load.block('tablaPorCausa');\n        load.block('calidadCausa');\n        $http.post('phrapi/laboresAgricolasComparativo/tablaPorCausa', params || {}).then(function (r) {\n            load.unblock('tablaPorCausa');\n            load.unblock('calidadCausa');\n            callback(r.data);\n        });\n    };\n\n    return service;\n}]);\n\napp.filter('avgOfValue', function () {\n    return function (data, key) {\n        if (angular.isUndefined(data) || angular.isUndefined(key)) return 0;\n        var sum = 0;\n        var count = 0;\n        angular.forEach(data, function (value) {\n            if (value[key] != \"\" && value[key] != undefined) {\n                sum = sum + parseFloat(value[key], 10);\n                count++;\n            }\n        });\n        sum = sum / count;\n        if (isNaN(sum)) return 0;\n        return sum;\n    };\n});\n\napp.filter('avg', function () {\n    return function (input) {\n        var count = 0,\n            sum = 0;\n        input.forEach(function (val) {\n            if (parseFloat(val)) {\n                count++;\n                sum += val;\n            }\n        });\n        return sum / count;\n    };\n});\n\napp.controller('controller', ['$scope', 'request', function ($scope, $request) {\n\n    $scope.filters = {\n        fecha_inicial: '',\n        fecha_final: '',\n        id_finca: 0,\n        tablaPorCausa: { labor: 0 },\n        calidadLabor: {},\n        calidadCausa: {}\n    };\n\n    responseTablaPorCausa = function responseTablaPorCausa(data) {\n        $scope.filters.calidadCausa.causa = '';\n        $scope.tabla_por_causa = data;\n    };\n\n    $scope.tablaPorCausa = function () {\n        $scope.labor = $scope.tabla_por_labor.labores.filter(function (l) {\n            return l.idLabor == $scope.filters.tablaPorCausa.labor;\n        })[0].nombre;\n        $request.tablaPorCausa(function (r) {\n            responseTablaPorCausa(r);\n            setTimeout(function () {\n                $scope.getGraficaPorCausa(r.fincas, r.data);\n                $scope.getGraficaPorLabor($scope.tabla_por_labor.fincas, $scope.tabla_por_labor.tabla_por_labor);\n            }, 250);\n        }, $scope.filters);\n    };\n\n    $scope.variables = function () {\n        $request.variables(function (r) {\n            $scope.fincas = r.fincas;\n            $scope.labores = r.labores;\n\n            var e = r.labores.filter(function (c) {\n                return c.idLabor == $scope.filters.tablaPorCausa.labor;\n            });\n            if (!e.length > 0) {\n                $scope.filters.tablaPorCausa.labor = r.labores[0].idLabor;\n            }\n\n            $scope.index();\n        }, $scope.filters);\n    };\n\n    $scope.index = function () {\n        $request.index(function (r) {\n            $scope.tabla_por_labor = r.tabla_por_labor;\n            responseTablaPorCausa(r.tabla_por_causa);\n\n            setTimeout(function () {\n                $scope.getGraficaPorLabor(r.tabla_por_labor.fincas, r.tabla_por_labor.tabla_por_labor);\n                $scope.getGraficaPorCausa(r.tabla_por_causa.fincas, r.tabla_por_causa.data);\n            }, 250);\n        }, $scope.filters);\n    };\n\n    $scope.last = function () {\n        $request.last(function (r) {\n            $scope.filters.fecha_inicial = r.fecha;\n            $scope.filters.fecha_final = r.fecha;\n            $(\"#datepicker\").html(r.fecha + ' - ' + r.fecha);\n\n            $scope.variables();\n        });\n    };\n\n    $scope.getGraficaPorLabor = function (fincas, datatable) {\n        var series = [{\n            name: 'Cantidad',\n            type: 'bar',\n            data: []\n        }];\n        var legends = [];\n\n        var _loop = function _loop(i) {\n            var finca = fincas[i];\n            legends.push(finca.nombre);\n\n            var valor = 'promedio';\n            if ($scope.filters.tablaPorCausa.labor) {\n                valor = 'labor_' + $scope.filters.tablaPorCausa.labor;\n            }\n            series[0].data.push({\n                value: datatable.filter(function (r) {\n                    return r.idFinca == finca.idFinca;\n                })[0][valor]\n            });\n        };\n\n        for (var i in fincas) {\n            _loop(i);\n        }\n\n        ///----------------\n        var props = {\n            id: 'chart-calidad-by-labor',\n            series: series,\n            legend: legends,\n            showLegends: false,\n            orientation: 'horizontal',\n            min: Math.round((arrayMin(series[0].data, 'value') - 5) * 100) / 100\n        };\n        $(\"#chart-calidad-by-labor\").empty();\n        ReactDOM.render(React.createElement(BarrasStacked, props), document.getElementById('chart-calidad-by-labor'));\n    };\n\n    $scope.getGraficaPorCausa = function (fincas, datatable) {\n        var series = [{\n            name: 'Cantidad',\n            type: 'bar',\n            data: []\n        }];\n        var legends = [];\n\n        var _loop2 = function _loop2(i) {\n            var finca = fincas[i];\n            legends.push(finca.nombre);\n\n            var valor = 'promedio';\n            if ($scope.filters.calidadCausa.causa) {\n                valor = 'causa_' + $scope.filters.calidadCausa.causa;\n            }\n            series[0].data.push({\n                value: datatable.filter(function (r) {\n                    return r.idFinca == finca.idFinca;\n                })[0][valor]\n            });\n        };\n\n        for (var i in fincas) {\n            _loop2(i);\n        }\n\n        ///----------------\n        var min = Math.round((arrayMin(series[0].data, 'value') - 0.8) * 100) / 100;\n        var props = {\n            id: 'chart-calidad-by-causa',\n            series: series,\n            legend: legends,\n            showLegends: false,\n            orientation: 'horizontal',\n            min: min >= 0 ? min : 0\n        };\n        $(\"#chart-calidad-by-causa\").empty();\n        ReactDOM.render(React.createElement(BarrasStacked, props), document.getElementById('chart-calidad-by-causa'));\n    };\n\n    $scope.last();\n}]);\n\n//# sourceURL=webpack:///./js_modules/quintana/laboresAgricolasComparativo.js?");

/***/ })

/******/ });