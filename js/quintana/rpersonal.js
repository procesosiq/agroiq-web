/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./js_modules/quintana/rpersonal.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./js_modules/quintana/rpersonal.js":
/*!******************************************!*\
  !*** ./js_modules/quintana/rpersonal.js ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\napp.controller('rpersonal', ['$scope', 'client', function ($scope, client) {\n\n    $scope.filters = {\n        finca: 1\n    };\n    $scope.umbrales = {};\n    $scope.checks = function (data, periodo) {\n        var porcentaje = parseFloat(data['PERIODO ' + periodo]);\n        if (!isNaN(porcentaje) && porcentaje >= 0) {\n            if (porcentaje > parseFloat($scope.umbrales.yellow_umbral_2)) return 'bg-green-haze bg-font-green-haze';else if (porcentaje < parseFloat($scope.umbrales.yellow_umbral_1)) return 'bg-red-thunderbird bg-font-red-thunderbird';else return 'bg-yellow-lemon bg-font-yellow-lemon';\n        }\n        return '';\n    };\n\n    $scope.init = function () {\n        client.post('phrapi/quintana/laboresAgricolas/personal', $scope.printDetails, $scope.filters);\n    };\n\n    $scope.printDetails = function (r, b) {\n        b();\n        $scope.umbrales = r.umbrals || {};\n        renderTablaReact(r.data, r.periodos);\n    };\n\n    var renderTablaReact = function renderTablaReact(data, periodos) {\n        var props = {\n            header: [{\n                key: 'zona',\n                name: 'VARIABLE',\n                titleClass: 'text-center',\n                locked: true,\n                expandable: true,\n                resizable: true,\n                width: 400\n            }],\n            data: data,\n            buttons: [{\n                title: 'Excel',\n                action: function action() {\n                    $scope.table1.exportToExcel();\n                },\n                className: ''\n            }],\n            height: 450\n        };\n        periodos.map(function (p) {\n            var value = p.periodo;\n            props.header.push({\n                key: 'PERIODO ' + value,\n                name: '' + value,\n                sortable: true,\n                alignContent: 'right',\n                titleClass: 'text-center',\n                filterable: true,\n                filterRenderer: 'NumericFilter',\n                resizable: true,\n                customCell: function customCell(rowData) {\n                    var valNumber = parseFloat(rowData['PERIODO ' + value]);\n                    var checkUmbral = $scope.checks(rowData, value);\n                    if (rowData['tipo'] == 'CAUSA') checkUmbral = '';\n                    return '\\n                        <div class=\"text-center ' + checkUmbral + '\" style=\"height: 100%\">\\n                            ' + valNumber + ' %\\n                        </div>\\n                    ';\n                }\n            });\n        });\n        document.getElementById('table-react').innerHTML = \"\";\n        $scope.table1 = ReactDOM.render(React.createElement(ReactDataGrid, props), document.getElementById('table-react'));\n    };\n\n    $scope.init();\n}]);\n\n//# sourceURL=webpack:///./js_modules/quintana/rpersonal.js?");

/***/ })

/******/ });