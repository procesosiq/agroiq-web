/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./js_modules/rastreoLancofruit2.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./js_modules/rastreoLancofruit2.js":
/*!******************************************!*\
  !*** ./js_modules/rastreoLancofruit2.js ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nfunction initGrafica(id) {\n    var options = {\n        tooltip: {\n            trigger: 'axis',\n            axisPointer: {\n                type: 'shadow'\n            }\n        },\n        toolbox: {\n            show: true,\n            feature: {\n                mark: { show: true },\n                magicType: { show: true, type: ['line', 'bar'] },\n                restore: { show: true },\n                saveAsImage: { show: true }\n            }\n        },\n        legend: {\n            data: ['TM', 'TE'],\n            y: 'bottom'\n        },\n        grid: {\n            left: '3%',\n            right: '4%',\n            bottom: '10%',\n            containLabel: true\n        },\n        xAxis: [{\n            type: 'category',\n            data: ['RUTA 1', 'RUTA 2']\n        }],\n        yAxis: [{\n            type: 'value'\n        }],\n        series: [{\n            name: 'TM',\n            type: 'bar',\n            data: [0, 0]\n        }, {\n            name: 'TE',\n            type: 'bar',\n            data: [0, 0]\n        }]\n    };\n    var myChart = new echartsPlv();\n    myChart.init(id, options);\n}\n\nfunction initGrafica2(id) {\n    var options = {\n        toolbox: {\n            show: true,\n            feature: {\n                mark: { show: true },\n                magicType: { show: true, type: ['line', 'bar'] },\n                restore: { show: true },\n                saveAsImage: { show: true }\n            }\n        },\n        legend: {\n            show: true,\n            orient: 'vertical',\n            x: 'left'\n        },\n        grid: {\n            left: '3%',\n            right: '4%',\n            bottom: '10%',\n            containLabel: true\n        },\n        series: {\n            type: 'pie',\n            data: [{\n                name: 'TM',\n                type: 'pie',\n                value: 0,\n                label: {\n                    normal: {\n                        show: true,\n                        position: 'inside',\n                        formatter: '{b} \\n {c} ({d}%)'\n                    }\n                }\n            }, {\n                name: 'TE',\n                value: 0,\n                label: {\n                    normal: {\n                        show: true,\n                        position: 'inside',\n                        formatter: '{b} \\n {c} ({d}%)'\n                    }\n                }\n            }]\n        }\n    };\n    var myChart = new echartsPlv();\n    myChart.init(id, options);\n}\n\napp.service('request', ['$http', function ($http) {\n\n    this.ventaDia = function (callback, params) {\n        var url = 'phrapi/lancofruit/rastreo/ventadia';\n        $http.post(url, params || {}).then(function (r) {\n            callback(r.data);\n        });\n    };\n    this.ventaSemana = function (callback, params) {\n        var url = 'phrapi/lancofruit/rastreo/ventasemana';\n        $http.post(url, params || {}).then(function (r) {\n            callback(r.data);\n        });\n    };\n    this.ventaMes = function (callback, params) {\n        var url = 'phrapi/lancofruit/rastreo/ventames';\n        $http.post(url, params || {}).then(function (r) {\n            callback(r.data);\n        });\n    };\n    this.ventaAnio = function (callback, params) {\n        var url = 'phrapi/lancofruit/rastreo/ventaanio';\n        $http.post(url, params || {}).then(function (r) {\n            callback(r.data);\n        });\n    };\n    this.dia = function (callback, params) {\n        var url = 'phrapi/lancofruit/rastreo/dia';\n        $http.post(url, params || {}).then(function (r) {\n            callback(r.data);\n        });\n    };\n    this.semana = function (callback, params) {\n        var url = 'phrapi/lancofruit/rastreo/semana';\n        $http.post(url, params || {}).then(function (r) {\n            callback(r.data);\n        });\n    };\n    this.mes = function (callback, params) {\n        var url = 'phrapi/lancofruit/rastreo/mes';\n        $http.post(url, params || {}).then(function (r) {\n            callback(r.data);\n        });\n    };\n    this.anio = function (callback, params) {\n        var url = 'phrapi/lancofruit/rastreo/anio';\n        $http.post(url, params || {}).then(function (r) {\n            callback(r.data);\n        });\n    };\n    this.last = function (callback, params) {\n        var url = 'phrapi/lancofruit/rastreo/last';\n        $http.post(url, params || {}).then(function (r) {\n            callback(r.data);\n        });\n    };\n    this.loadPoints = function (callback, params) {\n        var url = 'phrapi/lancofruit/rastreo/points';\n\n        load.block('map_box');\n        $http.post(url, params || {}).then(function (r) {\n            load.unblock('map_box');\n            callback(r.data);\n        });\n    };\n    this.loadMorePoints = function (callback, params) {\n        var url = 'phrapi/lancofruit/rastreo/points';\n        $http.post(url, params || {}).then(function (r) {\n            callback(r.data);\n        });\n    };\n}]);\n\napp.controller('rastreo', ['$scope', 'request', function ($scope, request) {\n\n    // 27/04/2017 - TAG: GOOGLE MAPS\n    var lineSymbol = {\n        path: google.maps.SymbolPath.FORWARD_OPEN_ARROW\n    };\n    $scope.now = moment().format('YYYY-MM-DD');\n\n    var cargo_pestana_uno = false;\n    var cargo_pestana_dos = false;\n    var cargo_pestana_tres = false;\n    var cargo_pestana_cuatro = false;\n    var routes = [],\n        map = {};\n    $scope.data_ventas_dia = [];\n    $scope.data_ventas_semana = [];\n    $scope.data_ventas_mes = [];\n    $scope.data_ventas_anio = [];\n    $scope.data_semanas = [];\n    $scope.table = [];\n    $scope.filters = {\n        sector: \"\",\n        ruta: \"\",\n        search: \"\",\n        tipo: \"\",\n        status: \"\",\n        year: moment().year(),\n        semana: moment().week(),\n        fecha: moment().format('YYYY-MM-DD')\n    };\n\n    setCameraPosition = function setCameraPosition(lat, lng) {\n        var fromProjection = new OpenLayers.Projection(\"EPSG:4326\"); // Transform from WGS 1984\n        var toProjection = new OpenLayers.Projection(\"EPSG:900913\"); // to Spherical Mercator Projection\n        var position = new OpenLayers.LonLat(lng, lat).transform(fromProjection, toProjection);\n        var zoom = 16;\n        map.setCenter(position, zoom);\n    };\n\n    $scope.createMap = function () {\n        var options = {\n            controls: [new OpenLayers.Control.Navigation(), new OpenLayers.Control.PanZoomBar()]\n        };\n        map = new OpenLayers.Map(\"gmap_routes\", options);\n        map.addLayer(new OpenLayers.Layer.OSM());\n        setCameraPosition(-2.188861, -79.898896);\n    };\n\n    $scope.last = function () {\n        request.last(function (r) {\n            $scope.anios = r.years;\n            $scope.filters.year = r.last_year;\n            $scope.fechas = r.fechas;\n\n            $scope.notToday = r.fechas.indexOf($scope.now) == -1;\n        });\n    };\n\n    $scope.route = [];\n    $scope.routeObj = [];\n\n    $scope.trazarRuta = function () {\n        $scope.removeAllSteps();\n\n        var waypoints = angular.copy($scope.route);\n        $scope.startRouting(waypoints);\n    };\n\n    /*$scope.startRouting = (start, end, waypoints) => {\n        console.log(`${start.lat},${start.lng} -> ${end.lat},${end.lng}`)\n         map.travelRoute({\n            origin: [start.lat, start.lng],\n            destination: [end.lat, end.lng],\n            travelMode: 'driving',\n            step: function(e) {\n                $('#gmap_routes_instructions').append('<li>'+e.instructions+'</li>');\n                $('#gmap_routes_instructions li:eq(' + e.step_number + ')').delay(450 * e.step_number).fadeIn(200, function() {\n                    routes.push(map.drawPolyline({\n                        path: e.path,\n                        strokeColor: '#131540',\n                        strokeOpacity: 0.6,\n                        strokeWeight: 6\n                    }));\n                });\n            },\n            end : function(){\n                waypoints.splice(0, 1)\n                if(waypoints.length > 1){\n                    $scope.startRouting(waypoints[0], waypoints[1], waypoints)\n                }\n            }\n        });\n    }*/\n\n    $scope.startRouting = function (waypoints) {\n        var vectorLayer = new OpenLayers.Layer.Vector(\"Overlay\", {\n            renderers: ['SVGExtended', 'VMLExtended', 'CanvasExtended'],\n            styleMap: new OpenLayers.StyleMap({\n                'default': OpenLayers.Util.extend({\n                    orientation: true\n                }, OpenLayers.Feature.Vector.style['default']),\n                'temporary': OpenLayers.Util.extend({\n                    orientation: true\n                }, OpenLayers.Feature.Vector.style['temporary'])\n            }),\n            style: {\n                strokeColor: '#ff0000',\n                orientation: 'any value'\n            }\n        });\n\n        var feature = new OpenLayers.Feature.Vector(new OpenLayers.Geometry.LineString(waypoints));\n\n        vectorLayer.addFeatures(feature);\n        map.addLayer(vectorLayer);\n        routes.push(vectorLayer);\n    };\n\n    delay = function delay() {\n        return new Promise(function (resolve, reject) {\n            setTimeout(function () {\n                if ($scope.filters.fecha == moment().format('YYYY-MM-DD')) $scope.loadMorePoints(true);\n            }, 5000);\n        });\n    };\n\n    $scope.changeDate = function () {\n        if ($scope.filters.fecha == moment().format('YYYY-MM-DD')) $(\"#live\").removeClass('hide');else $(\"#live\").addClass('hide');\n\n        $scope.removeAllSteps();\n        $scope.route = [];\n        $scope.routeObj = [];\n        $scope.loadMorePoints(false);\n    };\n\n    responsePoints = function responsePoints(r) {\n        if (r.data.length > 0) {\n            setCameraPosition(r.data[r.data.length - 1].lat, r.data[r.data.length - 1].lng);\n\n            var waypoints = [];\n            var epsg4326 = new OpenLayers.Projection(\"EPSG:4326\"); //WGS 1984 projection\n            var projectTo = map.getProjectionObject(); //The map projection (Spherical Mercator)\n            r.data.map(function (c) {\n                waypoints.push(new OpenLayers.Geometry.Point(c.lng, c.lat).transform(epsg4326, projectTo));\n            });\n            $scope.startRouting(waypoints);\n            $scope.route = $scope.route.concat(waypoints);\n            $scope.routeObj = $scope.routeObj.concat(r.data);\n        }\n        delay();\n    };\n\n    $scope.loadMorePoints = function (morePoints) {\n        if (morePoints) {\n            request.loadMorePoints(responsePoints, {\n                hora: $scope.routeObj.length > 0 ? $scope.routeObj[$scope.routeObj.length - 1].time : '',\n                fecha: $scope.filters.fecha\n            });\n        } else {\n            request.loadPoints(responsePoints, {\n                hora: $scope.routeObj.length > 0 ? $scope.routeObj[$scope.routeObj.length - 1].time : '',\n                fecha: $scope.filters.fecha\n            });\n        }\n    };\n\n    $scope.removeAllSteps = function () {\n        var ids = [];\n        routes.map(function (vectorLayer) {\n            vectorLayer.features.map(function (feature) {\n                ids.push(feature.id);\n            });\n            ids.map(function (id) {\n                vectorLayer.removeFeatures(vectorLayer.getFeatureById(id));\n            });\n        });\n    };\n\n    /*$scope.semaforo = value => {\n        if(value > 0)\n        if($scope.isNumeric(value)){\n            return 'bg-green-jungle bg-font-green-jungle'\n        }else if(value == 'F'){\n            return 'bg-red-thunderbird bg-font-red-thunderbird'\n        }\n        return ''\n    }*/\n\n    $scope.request = {\n        all_pestana_uno: function all_pestana_uno() {\n            if (!cargo_pestana_uno) {\n                cargo_pestana_uno = true;\n                this.ventaDia();\n                setTimeout(function () {\n                    initGrafica('chart_1');\n                    initGrafica2('chart_2');\n                }, 1000);\n            }\n        },\n        all_pestana_dos: function all_pestana_dos() {\n            if (!cargo_pestana_dos) {\n                cargo_pestana_dos = true;\n                this.semana();\n                this.ventaSemana();\n                setTimeout(function () {\n                    initGrafica('chart_3');\n                    initGrafica2('chart_4');\n                }, 1000);\n            }\n        },\n\n        all_pestana_tres: function all_pestana_tres() {\n            if (!cargo_pestana_tres) {\n                cargo_pestana_tres = true;\n                this.ventaMes();\n                setTimeout(function () {\n                    initGrafica('chart_5');\n                    initGrafica2('chart_6');\n                }, 1000);\n            }\n        },\n\n        all_pestana_cuatro: function all_pestana_cuatro() {\n            if (!cargo_pestana_cuatro) {\n                cargo_pestana_cuatro = true;\n                this.ventaAnio();\n                setTimeout(function () {\n                    initGrafica('chart_7');\n                    initGrafica2('chart_8');\n                }, 1000);\n            }\n        },\n        init: function init() {\n            $scope.createMap();\n            $scope.last();\n            $scope.loadMorePoints(false);\n        },\n        ventaDia: function ventaDia() {\n            request.ventaDia(function (r) {\n                if (r.data_venta_dia) {\n                    $scope.data_ventas_dia = r.data_venta_dia;\n                }\n            }, $scope.filters);\n        },\n        ventaSemana: function ventaSemana() {\n            request.ventaSemana(function (r) {\n                if (r.data_venta_semana) {\n                    $scope.data_ventas_semana = r.data_venta_semana;\n                }\n            }, $scope.filters);\n        },\n        ventaMes: function ventaMes() {\n            request.ventaMes(function (r) {\n                if (r.data_venta_mes) {\n                    $scope.data_ventas_mes = r.data_venta_mes;\n                }\n            }, $scope.filters);\n        },\n        ventaAnio: function ventaAnio() {\n            request.ventaAnio(function (r) {\n                if (r.data_venta_anio) {\n                    $scope.data_ventas_anio = r.data_venta_anio;\n                }\n            }, $scope.filters);\n        },\n        semana: function semana() {\n            request.semana(function (r) {\n                if (r.data_semana) {\n                    $scope.data_semanas = r.data_semana;\n                }\n            }, $scope.filters);\n        }\n    };\n\n    $scope.filtraSemana = function () {\n        $scope.filters.semana = \"#semana\".val();\n        $scope.request.all_pestana_dos();\n    };\n\n    $scope.request.init();\n}]);\n\n//# sourceURL=webpack:///./js_modules/rastreoLancofruit2.js?");

/***/ })

/******/ });