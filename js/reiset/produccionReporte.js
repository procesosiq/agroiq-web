/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./js_modules/reiset/produccionReporte.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./js_modules/reiset/produccionReporte.js":
/*!************************************************!*\
  !*** ./js_modules/reiset/produccionReporte.js ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\napp.service('request', ['$http', function ($http) {\n\n    this.data = function (callback, params) {\n        var url = 'phrapi/reiset/produccionreporte/data';\n        load.block();\n        $http.post(url, params || {}).then(function (r) {\n            load.unblock();\n            callback(r.data);\n        });\n    };\n\n    this.data2 = function (callback, params) {\n        var url = 'phrapi/reiset/produccionreporte/reporte2';\n        $http.post(url, params || {}).then(function (r) {\n            callback(r.data);\n        });\n    };\n\n    this.getWeeks = function (callback, params) {\n        var url = 'phrapi/reiset/produccionreporte/weeks';\n        $http.post(url, params || {}).then(function (r) {\n            callback(r.data);\n        });\n    };\n\n    this.last = function (callback, params) {\n        var url = 'phrapi/reiset/produccionreporte/last';\n        $http.post(url, params || {}).then(function (r) {\n            callback(r.data);\n        });\n    };\n}]);\n\napp.filter('avgOfValue', function () {\n    return function (data, key) {\n        if (angular.isUndefined(data) || angular.isUndefined(key)) return 0;\n        var sum = 0;\n        var count = 0;\n        angular.forEach(data, function (value) {\n            if (value[key] != \"\" && value[key] != undefined) {\n                sum = sum + parseFloat(value[key], 10);\n                count++;\n            }\n        });\n        sum = sum / count;\n        if (isNaN(sum)) return 0;\n        return sum;\n    };\n});\n\napp.controller('controller', ['$scope', 'request', function ($scope, $request) {\n\n    $scope.filters = {};\n    $scope.changeRangeDate = function (data) {\n        if (data) {\n            $scope.year = '';\n            $scope.semana = '';\n            $scope.filters.fecha_inicial = data.hasOwnProperty(\"first_date\") ? data.first_date : $scope.filters.fecha_inicial;\n            $scope.filters.fecha_final = data.hasOwnProperty(\"second_date\") ? data.second_date : $scope.filters.fecha_final;\n            getData();\n        }\n    };\n\n    $scope.changeYear = function () {\n        if ($scope.filters.year != '') {\n            $request.getWeeks(function (r) {\n                $scope.semanas = r.semanas;\n                if (r.semanas.indexOf($scope.filters.semana) == -1) {\n                    $scope.filters.semana = '';\n                }\n            }, {\n                year: $scope.filters.year\n            });\n        }\n    };\n    $scope.changeWeek = function () {\n        if ($scope.filters.semana != '' && $scope.filters.year != '') {\n            $(\"#date-picker\").html('N/A - N/A');\n            $scope.filters.fecha_inicial = '';\n            $scope.filters.fecha_final = '';\n            getData();\n        }\n    };\n    $scope.changeSector = function () {\n        return getData();\n    };\n\n    getData = function getData() {\n        $request.data(function (r) {\n            $scope.data = r.data;\n            $scope.totales = r.totales;\n            $scope.edades = r.edades;\n            $scope.sectores = r.sectores;\n        }, $scope.filters);\n\n        $request.data2(function (r) {\n            renderReporte2(r.data);\n        }, $scope.filters);\n    };\n\n    renderReporte2 = function renderReporte2(data) {\n        var props = {\n            header: [{\n                key: 'lote',\n                name: 'LOTE',\n                locked: true,\n                sortable: true,\n                titleClass: 'text-center',\n                alignContent: 'center',\n                filterable: true,\n                resizable: true\n            }, {\n                key: 'hectareas',\n                name: 'HA',\n                titleClass: 'text-center',\n                sortable: true,\n                alignContent: 'center',\n                resizable: true /*,{\n                                  key : 'enfunde',\n                                  name : 'TOT. ENF.',\n                                  sortable : true,\n                                  titleClass : 'text-center',\n                                  alignContent : 'center',\n                                  filterable : true,\n                                  filterRenderer: 'NumericFilter',\n                                  resizable : true,\n                                },{\n                                   key : 'enfunde_ha',\n                                   name : 'TOT. ENFU. / HA',\n                                   sortable : true,\n                                   titleClass : 'text-center',\n                                   alignContent : 'center',\n                                   filterable : true,\n                                   filterRenderer: 'NumericFilter',\n                                   resizable : true,\n                                   width : 150\n                                }*/ }, {\n                key: 'peso_prom_racimo',\n                name: 'PESO PROM.',\n                sortable: true,\n                titleClass: 'text-center',\n                alignContent: 'center',\n                filterable: true,\n                filterRenderer: 'NumericFilter',\n                resizable: true,\n                width: 100\n            }, {\n                key: 'manos',\n                name: 'NO. MANOS',\n                sortable: true,\n                titleClass: 'text-center',\n                alignContent: 'center',\n                filterable: true,\n                filterRenderer: 'NumericFilter',\n                resizable: true,\n                width: 100\n            }, {\n                key: 'calibre',\n                name: 'CALIB.',\n                sortable: true,\n                titleClass: 'text-center',\n                alignContent: 'center',\n                filterable: true,\n                filterRenderer: 'NumericFilter',\n                resizable: true /*,{\n                                  key : 'dedos',\n                                  name : 'L. DEDOS',\n                                  sortable : true,\n                                  titleClass : 'text-center',\n                                  alignContent : 'center',\n                                  filterable : true,\n                                  filterRenderer: 'NumericFilter',\n                                  resizable : true,\n                                }*/ }, {\n                key: 'merma_total',\n                name: 'MERMA TOT.',\n                sortable: true,\n                titleClass: 'text-center',\n                alignContent: 'center',\n                filterable: true,\n                filterRenderer: 'NumericFilter',\n                resizable: true,\n                width: 100\n            }, {\n                key: 'racimo_ha',\n                name: 'RAC. /HA',\n                sortable: true,\n                titleClass: 'text-center',\n                alignContent: 'center',\n                filterable: true,\n                filterRenderer: 'NumericFilter',\n                resizable: true\n            }, {\n                key: 'porc_racimos_recu',\n                name: '% RAC. RECH.',\n                sortable: true,\n                titleClass: 'text-center',\n                alignContent: 'center',\n                filterable: true,\n                filterRenderer: 'NumericFilter',\n                resizable: true,\n                width: 100\n            }, {\n                key: 'racimos_recu_ha',\n                name: 'RAC. RECH. /HA',\n                sortable: true,\n                titleClass: 'text-center',\n                alignContent: 'center',\n                filterable: true,\n                filterRenderer: 'NumericFilter',\n                resizable: true,\n                width: 120\n            }, {\n                key: 'racimos_proc_ha',\n                name: 'RAC. PROC. /HA',\n                sortable: true,\n                titleClass: 'text-center',\n                alignContent: 'center',\n                filterable: true,\n                filterRenderer: 'NumericFilter',\n                resizable: true,\n                width: 120\n            }, {\n                key: 'cajas_ha',\n                name: 'CAJAS /HA',\n                sortable: true,\n                titleClass: 'text-center',\n                alignContent: 'center',\n                filterable: true,\n                filterRenderer: 'NumericFilter',\n                resizable: true\n            }, {\n                key: 'ratio_cortado',\n                name: 'RATIO CORTADO',\n                sortable: true,\n                titleClass: 'text-center',\n                alignContent: 'center',\n                filterable: true,\n                filterRenderer: 'NumericFilter',\n                resizable: true,\n                width: 100\n            }, {\n                key: 'cajas_ha_proyeccion',\n                name: 'CAJAS /HA PROYEC',\n                sortable: true,\n                titleClass: 'text-center',\n                alignContent: 'center',\n                filterable: true,\n                filterRenderer: 'NumericFilter',\n                resizable: true,\n                width: 100\n            }],\n            data: data,\n            buttons: [{\n                title: 'Excel',\n                action: function action() {\n                    $scope.table1.exportToExcel();\n                },\n                className: ''\n            }]\n        };\n        $(\"#table-reporte\").html(\"\");\n        $scope.table1 = ReactDOM.render(React.createElement(ReactDataGrid, props), document.getElementById('table-reporte'));\n    };\n\n    $request.last(function (r) {\n        $scope.filters = {\n            fecha_inicial: r.fecha_inicial,\n            fecha_final: r.fecha_final,\n            year: '',\n            semana: '',\n            sector: ''\n        };\n        $scope.years = r.years;\n        $scope.semanas = r.weeks;\n        //$(\"#date-picker\").html(`${r.fecha_inicial} - ${r.fecha_final}`)\n        $scope.filters.year = r.last_year;\n        $scope.filters.semana = r.last_week;\n        getData();\n    });\n}]);\n\n//# sourceURL=webpack:///./js_modules/reiset/produccionReporte.js?");

/***/ })

/******/ });