/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./js_modules/rpersonal.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./js_modules/rpersonal.js":
/*!*********************************!*\
  !*** ./js_modules/rpersonal.js ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\napp.controller('rpersonal', ['$scope', '$http', '$interval', 'client', '$controller', '$timeout', '$window', function ($scope, $http, $interval, client, $controller, $timeout, $window) {\n\n    /*----------  FILTERS  ----------*/\n    $scope.search = \"\";\n    $scope.weeks = [];\n\n    $scope.viewModel = function () {\n        console.log($scope.search);\n    };\n\n    $scope.generateWeeks = function (is_filter) {\n        is_filter = is_filter || 1;\n        var semanas = [];\n        for (var i = 1; 53 > i; i++) {\n            semanas.push(i);\n        }\n        if (is_filter == 1) {\n            semanas.push(\"TOTAL\");\n        }\n        return semanas;\n    };\n\n    $scope.weeks = $scope.generateWeeks(-1);\n    // $scope.generateYears = function(){\n    //     var years = [];\n    //     var years = [\"ENE\" , \"FEB\" , \"MAR\" , \"ABR\" , \"MAY\" , \"JUN\", \"JUL\", \"AGO\", \"SEP\", \"OCT\", \"NOV\", \"DIC\" , \"TOTAL\"];\n    //     return years;\n    // }\n\n    // $scope.generateDays = function(){\n    //     var days = [\"LUN\" , \"MAR\" , \"MIE\" , \"JUE\" , \"VIE\" , \"SAB\" , \"TOTAL\"];\n    //     return days;\n    // }\n\n    $scope.dataFilters = [];\n    $scope.filters = {};\n\n    $scope.withTable = \"\";\n    $scope.colspan = \"\";\n\n    var changeFilter = function changeFilter() {\n        var type = $(\"#filter\").val();\n        $scope.init();\n    };\n\n    var changeFilterWeek = function changeFilterWeek() {\n        $scope.init();\n        $scope.withTable = \"12.5%\";\n    };\n\n    $scope.getType = function (type) {\n        $scope.rpersonal.params.type = type;\n        $scope.init();\n    };\n    /*----------  FILTERS  ----------*/\n    $scope.rpersonal = {\n        params: {\n            idFinca: 1,\n            idLote: 0,\n            idLabor: 0,\n            fecha_inicial: moment().startOf('month').format('YYYY-MM-DD'),\n            fecha_final: moment().endOf('month').format('YYYY-MM-DD'),\n            finca: \"\",\n            filter: \"\",\n            type: \"T\",\n            week: \"\"\n        },\n        step: 0,\n        path: ['phrapi/agricolas/index'],\n        templatePath: [],\n        nocache: function nocache() {\n            $scope.init();\n        }\n    };\n\n    $scope.tabla = {\n        rpersonal: []\n    };\n\n    $scope.openDetalle = function (data) {\n        data.expanded = !data.expanded;\n    };\n\n    $scope.totales = [];\n    $scope.umbral = [];\n    $scope.loteros_aereo = [];\n    $scope.cosechadores = [];\n\n    $scope.init = function () {\n        var data = $scope.rpersonal.params;\n        client.post($scope.rpersonal.path, $scope.printDetails, data);\n    };\n\n    $scope.start = true;\n    $scope.printDetails = function (r, b) {\n        b();\n        if (r) {\n            $scope.loteros_terrestre = [];\n            $scope.loteros_aereo = [];\n            $scope.totales = [];\n            $scope.umbral = [];\n            $scope.dataFilters = [];\n            $scope.filters = [];\n            $scope.colspan = 4;\n            if (r.hasOwnProperty(\"data\")) {\n                if (r.hasOwnProperty(\"fincas\")) {\n                    $scope.filters = r.fincas;\n                }\n                if (r.data.hasOwnProperty(\"semana\")) {\n                    $scope.dataFilters = r.data.semana;\n                    $scope.colspan = parseInt(r.data.colspan);\n                    $scope.withTable = parseInt(r.data.withTable) + \"%\";\n                }\n                if (r.data.hasOwnProperty(\"terrestre\")) {\n                    $scope.loteros_terrestre = r.data.terrestre;\n                }\n                if (r.data.hasOwnProperty(\"aereo\")) {\n                    $scope.loteros_aereo = r.data.aereo;\n                }\n                if (r.data.hasOwnProperty(\"totales\")) {\n                    $scope.totales = r.data.totales;\n                    $scope.umbral = r.data.umbral.campo;\n                    console.log($scope.umbral);\n                }\n                if ($scope.start) {\n                    $scope.start = false;\n                    setInterval(function () {\n                        $scope.init();\n                    }, 60000 * 5);\n                }\n            }\n        }\n    };\n}]);\n\n//# sourceURL=webpack:///./js_modules/rpersonal.js?");

/***/ })

/******/ });