/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./js_modules/rufcorp/calidad2.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./js_modules/rufcorp/calidad2.js":
/*!****************************************!*\
  !*** ./js_modules/rufcorp/calidad2.js ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\napp.filter('sumOfValue', function () {\n    return function (data, key) {\n        if (angular.isUndefined(data) || angular.isUndefined(key)) return 0;\n        var sum = 0;\n        angular.forEach(data, function (value) {\n            if (value[key] != \"\" && value[key] != undefined && parseFloat(value[key])) {\n                sum = sum + parseFloat(value[key], 10);\n            }\n        });\n        return sum;\n    };\n});\n\napp.filter('sumOfValueDouble', function () {\n    return function (data, key, key2) {\n        if (angular.isUndefined(data) || angular.isUndefined(key) || angular.isUndefined(key2)) return 0;\n        var sum = 0;\n        angular.forEach(data, function (value) {\n            if (value[key][key2] != \"\" && value[key][key2] != undefined && parseFloat(value[key][key2])) {\n                sum = sum + parseFloat(value[key][key2], 10);\n            }\n        });\n        return sum;\n    };\n});\n\napp.filter('getNotRepeat', function () {\n    return function (data, key) {\n        if (angular.isUndefined(data) || angular.isUndefined(key)) return [];\n\n        var _arr = [];\n        data.forEach(function (value) {\n            if (value[key]) {\n                if (!_arr.includes(value[key])) _arr.push(value[key]);\n            }\n        });\n        return _arr;\n    };\n});\n\napp.filter('avgOfValue', function () {\n    return function (data, key) {\n        if (angular.isUndefined(data) || angular.isUndefined(key)) return 0;\n        var sum = 0;\n        var count = 0;\n        angular.forEach(data, function (value) {\n            if (value[key] != \"\" && value[key] != undefined && value[key] > 0) {\n                sum = sum + parseFloat(value[key], 10);\n                count++;\n            }\n        });\n        sum = sum / count;\n        if (isNaN(sum)) return 0;\n        return sum;\n    };\n});\n\napp.filter('keys', function () {\n    return function (data) {\n        return Object.keys(data).length;\n    };\n});\n\napp.service('request', ['$http', function ($http) {\n    var service = {};\n\n    service.index = function (callback, params) {\n        load.block('defectos');\n        load.block('empaque');\n        load.block('cluster');\n        load.block('fotos');\n        $http.post('phrapi/sumifru/calidad2/index', params).then(function (r) {\n            load.unblock('defectos');\n            load.unblock('empaque');\n            load.unblock('cluster');\n            load.unblock('fotos');\n            callback(r.data);\n        });\n    };\n\n    service.variables = function (callback, params) {\n        load.block();\n        $http.post('phrapi/sumifru/calidad2/variables', params).then(function (r) {\n            load.unblock();\n            callback(r.data);\n        });\n    };\n\n    service.last = function (callback, params) {\n        load.block();\n        $http.post('phrapi/sumifru/calidad2/last', params).then(function (r) {\n            load.unblock();\n            callback(r.data);\n        });\n    };\n\n    return service;\n}]);\n\nfunction initPastel(id, series) {\n    var legends = [];\n    series.map(function (key) {\n        if (legends.indexOf(series[key]) != -1) legends.push(series[key]);\n    });\n    setTimeout(function () {\n        var data = {\n            data: series,\n            nameseries: \"Pastel\",\n            legend: legends,\n            titulo: \"\",\n            id: id\n        };\n        var parent = $(\"#\" + id).parent();\n        parent.empty();\n        parent.append('<div id=\"' + id + '\" class=\"chart\"></div>');\n        ReactDOM.render(React.createElement(Pastel, data), document.getElementById(id));\n    }, 250);\n}\n\napp.controller('controller', ['$scope', 'request', function ($scope, $request) {\n\n    // UMBRAL\n\n    $scope.loadCalidadUmbral = function () {\n        var c = localStorage.getItem('banano_calidad_cluster_umbral');\n        if (c) {\n            $scope.umbral_cluster = JSON.parse(c);\n        } else {\n            $scope.umbral_cluster = {\n                max: 92,\n                min: 90\n            };\n        }\n\n        var d = localStorage.getItem('banano_calidad_dedos_umbral');\n        if (d) {\n            $scope.umbral_dedos = JSON.parse(d);\n        } else {\n            $scope.umbral_dedos = {\n                max: 92,\n                min: 90\n            };\n        }\n\n        var e = localStorage.getItem('banano_calidad_empaque_umbral');\n        if (e) {\n            $scope.umbral_empaque = JSON.parse(e);\n        } else {\n            $scope.umbral_empaque = {\n                max: 92,\n                min: 90\n            };\n        }\n    };\n    $scope.saveCalidadUmbral = function () {\n        toastr.success('Umbral guardado');\n        localStorage.setItem('banano_calidad_cluster_umbral', JSON.stringify($scope.umbral_cluster));\n        localStorage.setItem('banano_calidad_dedos_umbral', JSON.stringify($scope.umbral_dedos));\n        localStorage.setItem('banano_calidad_empaque_umbral', JSON.stringify($scope.umbral_empaque));\n        $scope.reRenderMarkers();\n        $scope.closeMenu();\n    };\n\n    $scope.openMenu = function () {\n        $(\".toggler-close, .theme-options\").show();\n    };\n    $scope.closeMenu = function () {\n        $(\".toggler-close, .theme-options\").hide();\n    };\n\n    $scope.umbral_cluster = {};\n    var getUmbralClusterMin = function getUmbralClusterMin() {\n        return $scope.umbral_cluster.min;\n    };\n\n    var getUmbralClusterHigh = function getUmbralClusterHigh() {\n        return $scope.umbral_cluster.max;\n    };\n    $scope.getClusterUmbral = function (value) {\n        if (value <= getUmbralClusterMin()) {\n            return 'font-red-thunderbird';\n        } else if (value >= getUmbralClusterHigh()) {\n            return 'font-green-haze';\n        } else {\n            return 'font-yellow-gold';\n        }\n    };\n\n    $scope.umbral_dedos = {};\n    var getUmbralDedosMin = function getUmbralDedosMin() {\n        return $scope.umbral_dedos.min;\n    };\n\n    var getUmbralDedosHigh = function getUmbralDedosHigh() {\n        return $scope.umbral_dedos.max;\n    };\n    $scope.getDedosUmbral = function (value) {\n        if (value <= getUmbralDedosMin()) {\n            return 'font-red-thunderbird';\n        } else if (value >= getUmbralDedosHigh()) {\n            return 'font-green-haze';\n        } else {\n            return 'font-yellow-gold';\n        }\n    };\n\n    $scope.umbral_empaque = {};\n    var getUmbralEmpaqueMin = function getUmbralEmpaqueMin() {\n        return $scope.umbral_empaque.min;\n    };\n\n    var getUmbralEmpaqueHigh = function getUmbralEmpaqueHigh() {\n        return $scope.umbral_empaque.max;\n    };\n    $scope.getEmpaqueUmbral = function (value) {\n        if (value <= getUmbralEmpaqueMin()) {\n            return 'font-red-thunderbird';\n        } else if (value >= getUmbralEmpaqueHigh()) {\n            return 'font-green-haze';\n        } else {\n            return 'font-yellow-gold';\n        }\n    };\n\n    // \n\n    // BEGIN CONFIG\n    $scope.config = {\n        calidad_empaque: true,\n        peso_prom_cluster: true\n    };\n\n    $scope.tag_md = $scope.config.calidad_empaque ? 4 : 3;\n    // END CONFIG\n\n    $scope.datesEnabled = [];\n    $scope.total_empaque = { cantidad: 0 };\n    $scope.total_defectos = { cantidad_cluster_caja: 0 };\n    $scope.fincas = [];\n    $scope.tags = {};\n    $scope.filters = {\n        fecha_inicial: moment().format('YYYY-MM-DD'),\n        fecha_final: moment().format('YYYY-MM-DD'),\n        id_finca: 0,\n        marca: '',\n        fotos: {}\n    };\n\n    $scope.changeRangeDate = function (data) {\n        if (data) {\n            $scope.filters.fecha_inicial = data.hasOwnProperty(\"first_date\") ? data.first_date : $scope.filters.fecha_inicial;\n            $scope.filters.fecha_final = data.hasOwnProperty(\"second_date\") ? data.second_date : $scope.filters.fecha_final;\n            $scope.variables();\n        }\n    };\n\n    $scope.init = function () {\n        $request.index(function (r) {\n            $scope.tags = r.tags;\n            $scope.defectos = r.defectos;\n            $scope.empaque = r.empaque;\n            $scope.cluster = r.cluster;\n            $scope.fotos = r.fotos;\n\n            initPastel('empaque-pie', r.empaque.data);\n            initPastel('cluster-pie', r.cluster.data.map(function (c) {\n                return { label: c.tipo + ' Dedos', value: c.cantidad };\n            }));\n        }, $scope.filters);\n    };\n\n    $scope.reRenderEmpaquePie = function () {\n        setTimeout(function () {\n            initPastel('empaque-pie', $scope.empaque.data);\n        }, 100);\n    };\n\n    $scope.reRenderClusterPie = function () {\n        setTimeout(function () {\n            initPastel('cluster-pie', $scope.cluster.data.map(function (c) {\n                return { label: c.tipo + ' Dedos', value: c.cantidad };\n            }));\n        }, 100);\n    };\n\n    $scope.variables = function () {\n        $request.variables(function (r) {\n            $scope.marcas = r.marcas;\n            // seleccionar marca\n            if (r.marcas) {\n                if ($scope.filters.marca) {\n                    if (!r.marcas.includes($scope.filters.marca)) {\n                        $scope.filters.marca = r.marcas[0];\n\n                        // no continuar y volver a pedir informacion de fincas\n                        $scope.variables();\n                        return;\n                    }\n                }\n            } else {\n                // todas\n                $scope.filters.marca = '';\n            }\n\n            $scope.fincas = r.fincas;\n            // seleccionar una finca default\n            if (r.fincas) {\n                if ($scope.filters.id_finca) {\n                    var finca = r.fincas.filter(function (f) {\n                        return f.id === $scope.filters.id_finca;\n                    });\n                    if (!finca) {\n                        $scope.filters.id_finca = r.fincas[0].id;\n                    }\n                }\n            } else {\n                // todas\n                $scope.filters.id_finca = 0;\n            }\n\n            $scope.init();\n        }, $scope.filters);\n    };\n\n    $scope.last = function () {\n        $request.last(function (r) {\n            $scope.datesEnabled = r.days;\n            if (r.fecha) {\n                $scope.filters.fecha_inicial = r.fecha;\n                $scope.filters.fecha_final = r.fecha;\n                setTimeout(function () {\n                    $(\"#date-picker\").html(r.fecha + ' - ' + r.fecha);\n                }, 100);\n            }\n            $scope.filters.id_finca = r.id_finca;\n            $scope.variables();\n        });\n    };\n\n    $scope.deleteFilter = function (campo) {\n        delete $scope.filters.fotos[campo];\n    };\n\n    $scope.exportExcel = function (id_table, title) {\n        var data = new Blob([document.getElementById(id_table).outerHTML], {\n            type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf8'\n        });\n        saveAs(data, title + '.xls');\n    };\n\n    $scope.last();\n    $scope.loadCalidadUmbral();\n}]);\n\n//# sourceURL=webpack:///./js_modules/rufcorp/calidad2.js?");

/***/ })

/******/ });