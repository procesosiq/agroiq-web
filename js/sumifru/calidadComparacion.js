/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./js_modules/sumifru/calidadComparacion.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./js_modules/sumifru/calidadComparacion.js":
/*!**************************************************!*\
  !*** ./js_modules/sumifru/calidadComparacion.js ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\napp.filter('sumOfValue', function () {\n    return function (data, key) {\n        if (angular.isUndefined(data) || angular.isUndefined(key)) return 0;\n        var sum = 0;\n        angular.forEach(data, function (value) {\n            if (value[key] != \"\" && value[key] != undefined && parseFloat(value[key])) {\n                sum = sum + parseFloat(value[key], 10);\n            }\n        });\n        return sum;\n    };\n});\n\napp.filter('orderObjectBy', function () {\n    return function (items, field, reverse) {\n        var filtered = [];\n        angular.forEach(items, function (item) {\n            if (field == 'hora' || field == 'fecha') {\n                item.date = moment(item.fecha + ' ' + item.hora);\n            }\n            if (!isNaN(parseInt(item))) {\n                item = parseInt(item);\n            }\n            filtered.push(item);\n        });\n        filtered.sort(function (a, b) {\n            if (field == 'hora' || field == 'fecha') {\n                return moment(a.date).isAfter(b.date) ? 1 : -1;\n            } else if (parseFloat(a[field]) && parseFloat(b[field])) {\n                return parseFloat(a[field]) > parseFloat(b[field]) ? 1 : -1;\n            } else {\n                return a[field] > b[field] ? 1 : -1;\n            }\n        });\n        if (reverse) filtered.reverse();\n        return filtered;\n    };\n});\n\napp.filter('sumOfValueDouble', function () {\n    return function (data, key, key2) {\n        if (angular.isUndefined(data) || angular.isUndefined(key) || angular.isUndefined(key2)) return 0;\n        var sum = 0;\n        angular.forEach(data, function (value) {\n            if (value[key][key2] != \"\" && value[key][key2] != undefined && parseFloat(value[key][key2])) {\n                sum = sum + parseFloat(value[key][key2], 10);\n            }\n        });\n        return sum;\n    };\n});\n\napp.filter('avgOfValue', function () {\n    return function (data, key) {\n        if (angular.isUndefined(data) || angular.isUndefined(key)) return 0;\n        var sum = 0;\n        var count = 0;\n        angular.forEach(data, function (value) {\n            if (value[key] != \"\" && value[key] != undefined && value[key] > 0) {\n                sum = sum + parseFloat(value[key], 10);\n                count++;\n            }\n        });\n        sum = sum / count;\n        if (isNaN(sum)) return 0;\n        return sum;\n    };\n});\n\napp.service('request', ['$http', function ($http) {\n    var service = {};\n\n    service.index = function (callback, params) {\n        load.block('principal');\n        $http.post('phrapi/sumifru/calidadComparacion/principal', params).then(function (r) {\n            load.unblock('principal');\n            callback(r.data);\n        });\n    };\n\n    service.variables = function (callback, params) {\n        load.block();\n        $http.post('phrapi/sumifru/calidadComparacion/variables', params).then(function (r) {\n            load.unblock();\n            callback(r.data);\n        });\n    };\n\n    service.last = function (callback, params) {\n        load.block();\n        $http.post('phrapi/sumifru/calidadComparacion/last', params).then(function (r) {\n            load.unblock();\n            callback(r.data);\n        });\n    };\n\n    service.graficaZona = function (callback, params) {\n        load.block('graficas-zona');\n        $http.post('phrapi/sumifru/calidadComparacion/graficaZona', params).then(function (r) {\n            load.unblock('graficas-zona');\n            callback(r.data);\n        });\n    };\n\n    return service;\n}]);\n\napp.controller('controller', ['$scope', 'request', function ($scope, $request) {\n\n    // UMBRAl\n\n    $scope.loadCalidadUmbral = function () {\n        var c = localStorage.getItem('banano_calidad_cluster_umbral');\n        if (c) {\n            $scope.umbral_cluster = JSON.parse(c);\n        } else {\n            $scope.umbral_cluster = {\n                max: 92,\n                min: 90\n            };\n        }\n\n        var d = localStorage.getItem('banano_calidad_dedos_umbral');\n        if (d) {\n            $scope.umbral_dedos = JSON.parse(d);\n        } else {\n            $scope.umbral_dedos = {\n                max: 92,\n                min: 90\n            };\n        }\n\n        var e = localStorage.getItem('banano_calidad_empaque_umbral');\n        if (e) {\n            $scope.umbral_empaque = JSON.parse(e);\n        } else {\n            $scope.umbral_empaque = {\n                max: 92,\n                min: 90\n            };\n        }\n    };\n\n    $scope.umbral_cluster = {};\n    var getUmbralClusterMin = function getUmbralClusterMin() {\n        return $scope.umbral_cluster.min;\n    };\n\n    var getUmbralClusterHigh = function getUmbralClusterHigh() {\n        return $scope.umbral_cluster.max;\n    };\n    $scope.getClusterUmbral = function (value) {\n        if (value <= getUmbralClusterMin()) {\n            return 'font-red-thunderbird';\n        } else if (value >= getUmbralClusterHigh()) {\n            return 'font-green-haze';\n        } else {\n            return 'font-yellow-gold';\n        }\n    };\n\n    $scope.umbral_dedos = {};\n    var getUmbralDedosMin = function getUmbralDedosMin() {\n        return $scope.umbral_dedos.min;\n    };\n\n    var getUmbralDedosHigh = function getUmbralDedosHigh() {\n        return $scope.umbral_dedos.max;\n    };\n    $scope.getDedosUmbral = function (value) {\n        if (value <= getUmbralDedosMin()) {\n            return 'font-red-thunderbird';\n        } else if (value >= getUmbralDedosHigh()) {\n            return 'font-green-haze';\n        } else {\n            return 'font-yellow-gold';\n        }\n    };\n\n    $scope.umbral_empaque = {};\n    var getUmbralEmpaqueMin = function getUmbralEmpaqueMin() {\n        return $scope.umbral_empaque.min;\n    };\n\n    var getUmbralEmpaqueHigh = function getUmbralEmpaqueHigh() {\n        return $scope.umbral_empaque.max;\n    };\n    $scope.getEmpaqueUmbral = function (value) {\n        if (parseFloat(value) <= getUmbralEmpaqueMin()) {\n            return 'font-red-thunderbird';\n        } else if (parseFloat(value) >= getUmbralEmpaqueHigh()) {\n            return 'font-green-haze';\n        } else {\n            return 'font-yellow-gold';\n        }\n    };\n\n    //\n\n    // BEGIN CONFIG\n    $scope.config = {\n        calidad_empaque: true,\n        peso_prom_cluster: true\n    };\n    $scope.nombre_cliente = 'Sumifru';\n    $scope.tag_md = $scope.config.calidad_empaque ? 4 : 3;\n    // END CONFIG\n\n    $scope.orderBy = {\n        general: { key: 'zona', direction: false },\n        defectos: { key: 'zona', direction: false },\n        empaque: { key: 'zona', direction: false },\n        cluster: { key: 'zona', direction: false }\n    };\n    $scope.toggleOrder = function (table, column) {\n        if ($scope.orderBy[table].key == column) {\n            $scope.orderBy[table].direction = !$scope.orderBy[table].direction;\n        } else {\n            $scope.orderBy[table] = {\n                key: column,\n                direcion: false\n            };\n        }\n    };\n\n    $scope.datesEnabled = [];\n    $scope.totales_defectos = {};\n    $scope.totales_empaque = {};\n    $scope.totales_cluster = {};\n    $scope.mode = 'general';\n    $scope.mode_graficas = 'defectos';\n    $scope.moda_graficas_desc = '(# Daños)';\n    $scope.modeCategoriaGraficaZona = '';\n    $scope.filters = {\n        fecha_inicial: moment().format('YYYY-MM-DD'),\n        fecha_final: moment().format('YYYY-MM-DD'),\n        id_finca: 0,\n        marca: ''\n    };\n\n    $scope.changeRangeDate = function (data) {\n        if (data) {\n            $scope.filters.fecha_inicial = data.hasOwnProperty(\"first_date\") ? data.first_date : $scope.filters.fecha_inicial;\n            $scope.filters.fecha_final = data.hasOwnProperty(\"second_date\") ? data.second_date : $scope.filters.fecha_final;\n            $scope.variables();\n        }\n    };\n\n    $scope.init = function () {\n        $request.index(function (r) {\n            $scope.data = r;\n        }, $scope.filters);\n        $request.graficaZona(function (r) {\n            $scope.dataGraficaZona = { cluster: r.cluster, defectos: r.defectos, empaque: r.empaque };\n            $scope.dataGraficaFinca = { cluster: r.cluster_finca, defectos: r.defectos_finca, empaque: r.empaque_finca };\n            $scope.renderGraficaZona($scope.modeCategoriaGraficaZona);\n\n            $scope.modeZonaGraficaFinca = $scope.dataGraficaZona.defectos.data[$scope.modeCategoriaGraficaZona].legends[0];\n            $scope.renderGraficaFinca($scope.modeCategoriaGraficaZona);\n        }, $scope.filters);\n    };\n\n    // GRAFICAS\n    $scope.renderGraficaZona = function (categoria) {\n        var props = {};\n        if ($scope.mode_graficas == 'defectos') {\n            if (!$scope.dataGraficaZona[$scope.mode_graficas].data[categoria]) {\n                categoria = Object.keys($scope.dataGraficaZona[$scope.mode_graficas].data)[0];\n            }\n\n            $scope.modeCategoriaGraficaZona = categoria;\n            props = {\n                id: 'grafica-zona-chart',\n                series: $scope.dataGraficaZona[$scope.mode_graficas].data[categoria].series,\n                legend: $scope.dataGraficaZona[$scope.mode_graficas].data[categoria].legends\n            };\n        } else {\n            props = {\n                id: 'grafica-zona-chart',\n                series: $scope.dataGraficaZona[$scope.mode_graficas].data.series,\n                legend: $scope.dataGraficaZona[$scope.mode_graficas].data.legends\n            };\n        }\n\n        $(\"#grafica-zona-chart\").empty();\n        ReactDOM.render(React.createElement(BarrasStacked, props), document.getElementById('grafica-zona-chart'));\n    };\n\n    $scope.renderGraficaFinca = function (categoria) {\n        setTimeout(function () {\n            var props = {};\n            var zona = $scope.modeZonaGraficaFinca;\n            if ($scope.mode_graficas == 'defectos') {\n                if (!$scope.dataGraficaFinca[$scope.mode_graficas].data[zona][categoria]) {\n                    categoria = Object.keys($scope.dataGraficaFinca[$scope.mode_graficas].data[zona])[0];\n                }\n\n                //$scope.modeCategoriaGraficaZona = categoria\n                props = {\n                    id: 'grafica-finca-chart',\n                    orientation: 'horizontal',\n                    series: $scope.dataGraficaFinca[$scope.mode_graficas].data[zona][categoria].series,\n                    legend: $scope.dataGraficaFinca[$scope.mode_graficas].data[zona][categoria].legends\n                };\n            } else {\n                props = {\n                    id: 'grafica-finca-chart',\n                    orientation: 'horizontal',\n                    series: $scope.dataGraficaFinca[$scope.mode_graficas].data[zona].series,\n                    legend: $scope.dataGraficaFinca[$scope.mode_graficas].data[zona].legends\n                };\n            }\n\n            $(\"#grafica-finca-chart\").empty();\n            ReactDOM.render(React.createElement(BarrasStacked, props), document.getElementById('grafica-finca-chart'));\n        }, 100);\n    };\n    // END GRAFICAS\n\n    $scope.variables = function () {\n        $request.variables(function (r) {\n            $scope.marcas = r.marcas;\n            // seleccionar marca\n            if (r.marcas) {\n                if ($scope.filters.marca) {\n                    if (!r.marcas.includes($scope.filters.marca)) {\n                        $scope.filters.marca = r.marcas[0];\n                    }\n                }\n            } else {\n                // todas\n                $scope.filters.marca = '';\n            }\n            $scope.init();\n        }, $scope.filters);\n    };\n\n    $scope.last = function () {\n        $request.last(function (r) {\n            $scope.datesEnabled = r.days;\n            if (r.fecha) {\n                $scope.filters.fecha_inicial = r.fecha;\n                $scope.filters.fecha_final = r.fecha;\n                setTimeout(function () {\n                    $(\"#date-picker\").html(r.fecha + ' - ' + r.fecha);\n                }, 100);\n            }\n            $scope.filters.id_finca = r.id_finca;\n            $scope.variables();\n        });\n    };\n\n    $scope.getUmbralDefectos = function (val) {\n        if (parseFloat(val)) {\n            return parseFloat(val) > 10 ? 'font-red-thunderbird' : '';\n        }\n        return '';\n    };\n\n    $scope.getUmbralEmpaque = function (val) {\n        if (parseFloat(val)) {\n            return parseFloat(val) > 10 ? 'font-red-thunderbird' : '';\n        }\n        return '';\n    };\n\n    $scope.exportExcel = function (id_table, title) {\n        var data = new Blob([document.getElementById(id_table).outerHTML], {\n            type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf8'\n        });\n        saveAs(data, title + '.xls');\n    };\n\n    $scope.last();\n    $scope.loadCalidadUmbral();\n}]);\n\n//# sourceURL=webpack:///./js_modules/sumifru/calidadComparacion.js?");

/***/ })

/******/ });