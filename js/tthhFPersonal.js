/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./js_modules/tthhFPersonal.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./js_modules/tthhFPersonal.js":
/*!*************************************!*\
  !*** ./js_modules/tthhFPersonal.js ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\napp.controller('personal_tthh', ['$scope', '$http', '$interval', 'client', '$controller', '$timeout', '$window', function ($scope, $http, $interval, client, $controller, $timeout, $window) {\n\n\t$scope.idPersonal = \"<?=$_GET['idPersonal']?>\";\n\n\t$scope.labores = [];\n\t$scope.fincas = [];\n\t$scope.showFinca = 0;\n\n\t$scope.Personal = {\n\t\tidPersonal: $scope.idPersonal,\n\t\tidFinca: 1,\n\t\tingreso: \"\",\n\t\tcontrato: \"\",\n\t\tid_contrato: 0,\n\t\tsalida: \"\",\n\t\tnombre: \"\",\n\t\tcodigo: \"\",\n\t\tcedula: \"\",\n\t\tsexo: \"Masculino\",\n\t\tcargo: \"OPERADOR AGRICOLA\",\n\t\tlabor: \"\",\n\t\tdiscapacitado: \"No\",\n\t\tafiliado: \"Si\",\n\t\tcivil: \"Soltero\",\n\t\tfamiliares: \"\",\n\t\tbeneficios: \"No\",\n\t\thoras: \"8\",\n\t\tsueldos: []\n\t};\n\n\t$scope.BkPersonal = {};\n\n\t$scope.nocache = function () {\n\t\tif ($('.date-picker').length > 0) {\n\t\t\t$('.date-picker').datepicker({\n\t\t\t\trtl: App.isRTL(),\n\t\t\t\tautoclose: true\n\t\t\t});\n\t\t}\n\n\t\t$scope.getLabores();\n\n\t\t$scope.BkPersonal = angular.copy($scope.Personal);\n\t};\n\n\t$scope.saveDatos = function () {\n\t\tif ($scope.Personal.nombre != \"\") {\n\t\t\tvar data = $scope.Personal;\n\t\t\tclient.post(\"phrapi/save/personal\", $scope.printPersonalSave, data);\n\t\t}\n\t};\n\n\t$scope.saveSal = function () {\n\t\tvar data = {\n\t\t\tidPersonal: parseInt($scope.idPersonal),\n\t\t\tsueldo: $(\"#sueldo\").val(),\n\t\t\tfecha: $(\"#fecha_sueldo\").val()\n\t\t};\n\t\tif (data.sueldo > 0 && data.fecha != \"\" && data.fecha != '0000-00-00') {\n\t\t\tclient.post(\"phrapi/guardar/sueldo\", function (r, b) {\n\t\t\t\tb();\n\t\t\t\tif (r) {\n\t\t\t\t\t$scope.Personal.sueldos = r;\n\t\t\t\t\t$(\"#sueldo\").val('');\n\t\t\t\t\t$(\"#fecha_sueldo\").val('');\n\t\t\t\t}\n\t\t\t}, data);\n\t\t}\n\t};\n\n\t$scope.getLabores = function () {\n\t\tclient.post(\"phrapi/cargo/tipo\", function (r, b) {\n\t\t\tb();\n\t\t\tif (r) {\n\t\t\t\t$scope.labores = r || [];\n\t\t\t\t$scope.init();\n\t\t\t}\n\t\t}, {});\n\t};\n\n\t$scope.init = function () {\n\t\tvar data = {\n\t\t\tidPersonal: parseInt($scope.idPersonal)\n\t\t};\n\t\tclient.post(\"phrapi/details/personal\", $scope.printPersonal, data);\n\t};\n\n\t$scope.printPersonalSave = function (r, b) {\n\t\tb();\n\t\talert(\"Registro Actualiazdo / Registrado\", \"Personal\", \"success\", function () {\n\t\t\tif (r) {\n\t\t\t\tif (r.hasOwnProperty(\"data\") && r.data.hasOwnProperty(\"nombre\")) {\n\t\t\t\t\t$scope.Personal = r.data;\n\t\t\t\t\t$scope.Personal.sueldos = r.sueldos;\n\t\t\t\t}\n\t\t\t}\n\t\t});\n\t};\n\n\t$scope.printPersonal = function (r, b) {\n\t\tb();\n\t\tif (r) {\n\t\t\tif (r.hasOwnProperty(\"data\") && r.data.hasOwnProperty(\"nombre\")) {\n\t\t\t\t$scope.Personal = r.data;\n\t\t\t\t$scope.Personal.sueldos = r.sueldos;\n\t\t\t\t$scope.fincas = r.fincas;\n\t\t\t\t$scope.showFinca = r.showFinca;\n\t\t\t}\n\t\t}\n\t};\n}]);\n\n//# sourceURL=webpack:///./js_modules/tthhFPersonal.js?");

/***/ })

/******/ });