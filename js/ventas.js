/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./js_modules/ventas.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./js_modules/ventas.js":
/*!******************************!*\
  !*** ./js_modules/ventas.js ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\napp.service('ventas', ['$http', function ($http) {\n\n    this.index = function (callback, params) {\n        var url = 'phrapi/lancofruit/ventas/index';\n        $http.post(url, params || {}).then(function (r) {\n            callback(r);\n        });\n    };\n\n    this.dia = function (callback, params) {\n        var url = 'phrapi/lancofruit/ventas/dia';\n        $http.post(url, params || {}).then(function (r) {\n            callback(r);\n        });\n    };\n\n    this.semana = function (callback, params) {\n        var url = 'phrapi/lancofruit/ventas/semana';\n        $http.post(url, params || {}).then(function (r) {\n            callback(r);\n        });\n    };\n\n    this.mes = function (callback, params) {\n        var url = 'phrapi/lancofruit/ventas/mes';\n        $http.post(url, params || {}).then(function (r) {\n            callback(r);\n        });\n    };\n\n    this.anio = function (callback, params) {\n        var url = 'phrapi/lancofruit/ventas/anio';\n        $http.post(url, params || {}).then(function (r) {\n            callback(r);\n        });\n    };\n\n    this.last = function (callback, params) {\n        var url = 'phrapi/lancofruit/ventas/last';\n        $http.post(url, params || {}).then(function (r) {\n            callback(r.data);\n        });\n    };\n}]);\n\napp.filter('num', function () {\n    return function (input) {\n        return parseInt(input, 10);\n    };\n});\n\napp.filter('startFrom', function () {\n    return function (input, start) {\n        start = +start; //parse to int\n        return input.slice(start);\n    };\n});\n\napp.filter('sumOfValue', function () {\n    return function (data, key) {\n        if (angular.isUndefined(data) || angular.isUndefined(key)) return 0;\n        var sum = 0;\n        var count = 0;\n        angular.forEach(data, function (value) {\n            if (value[key] != \"\" && value[key] != undefined && parseFloat(value[key])) {\n                sum = sum + parseFloat(value[key], 10);\n                count++;\n            }\n        });\n        if ([\"peso\", \"manos\", \"calibre\", \"dedos\"].indexOf(key) > -1) {\n            sum = sum / count;\n        }\n        return sum;\n    };\n});\n\napp.filter('sum', function () {\n    return function (data, key) {\n        var sum = 0;\n        angular.forEach(data, function (value) {\n            if (parseFloat(value)) sum += parseFloat(value);\n        });\n        return sum;\n    };\n});\n\napp.filter('avgOfValue', function () {\n    return function (data, key) {\n        if (angular.isUndefined(data) || angular.isUndefined(key)) return 0;\n        var sum = 0;\n        var count = 0;\n        angular.forEach(data, function (value) {\n            if (value[key] != \"\" && value[key] != undefined && parseFloat(value[key])) {\n                sum = sum + parseFloat(value[key], 10);\n                count++;\n            }\n        });\n        sum = sum / count;\n        return sum;\n    };\n});\n\napp.filter('orderObjectBy', function () {\n    return function (items, field, reverse) {\n        var filtered = [];\n        angular.forEach(items, function (item) {\n            if (!isNaN(parseInt(item))) {\n                item = parseInt(item);\n            }\n            filtered.push(item);\n        });\n        filtered.sort(function (a, b) {\n            if (parseFloat(a[field]) && parseFloat(b[field])) {\n                return parseFloat(a[field]) > parseFloat(b[field]) ? 1 : -1;\n            } else {\n                return a[field] > b[field] ? 1 : -1;\n            }\n        });\n        if (reverse) filtered.reverse();\n        return filtered;\n    };\n});\n\napp.controller('myController', ['$scope', '$http', '$interval', 'client', '$controller', '$timeout', '$window', 'ventas', function ($scope, $http, $interval, client, $controller, $timeout, $window, ventas) {\n\n    var graficas = {};\n    $scope.data = {\n        dia: [],\n        mes: [],\n        semana: [],\n        anio: []\n    };\n    $scope.filters = {\n        semana: \"\",\n        year: moment().year()\n        //moment().week()\n\n    };$scope.changeYear = function () {\n        var dia_open = $(\"#collapse_3_1\").hasClass('in');\n        var semana_open = $(\"#collapse_3_2\").hasClass('in');\n        var mes_open = $(\"#collapse_3_3\").hasClass('in');\n        var anio_open = $(\"#collapse_3_4\").hasClass('in');\n\n        if (dia_open) {\n            $scope.getDataDia();\n        } else if (semana_open) {\n            $scope.getDataSemana();\n        } else if (mes_open) {\n            $scope.getDataMes();\n        } else if (anio_open) {\n            $scope.getDataAnio();\n        }\n    };\n\n    $scope.getDataAnio = function () {\n        setTimeout(function () {\n            var anio_open = $(\"#collapse_3_4\").hasClass('in');\n            if (anio_open) {\n                ventas.anio(function (r) {\n                    if (r.data) {\n                        $scope.data.anio = angular.copy(r.data.data);\n                        $scope.rutas_anio = angular.copy(r.data.rutas);\n                        $scope.totales_anio = angular.copy(r.data.totales);\n\n                        graficas.line_anio = new echartsPlv();\n                        graficas.line_anio.init(\"lineal_anio\", r.data.line_chart);\n\n                        graficas.pie_anio = new echartsPlv();\n                        graficas.pie_anio.init(\"pastel_anio\", r.data.pie_chart);\n                        $scope.$apply();\n                    }\n                }, $scope.filters);\n            }\n        }, 1000);\n    };\n\n    $scope.getDataMes = function () {\n        setTimeout(function () {\n            var mes_open = $(\"#collapse_3_3\").hasClass('in');\n            if (mes_open) {\n                ventas.mes(function (r) {\n                    if (r.data) {\n                        $scope.data.mes = angular.copy(r.data.data);\n                        $scope.rutas_mes = angular.copy(r.data.rutas);\n                        $scope.totales_mes = angular.copy(r.data.totales);\n\n                        graficas.line_mes = new echartsPlv();\n                        graficas.line_mes.init(\"lineal_mes\", r.data.line_chart);\n\n                        graficas.pie_mes = new echartsPlv();\n                        graficas.pie_mes.init(\"pastel_mes\", r.data.pie_chart);\n                        $scope.$apply();\n                    }\n                }, $scope.filters);\n            }\n        }, 1000);\n    };\n\n    $scope.getDataSemana = function () {\n        setTimeout(function () {\n            var semana_open = $(\"#collapse_3_2\").hasClass('in');\n            if (semana_open) {\n                ventas.semana(function (r) {\n                    if (r.data) {\n                        $scope.data.semana = angular.copy(r.data.data);\n                        $scope.rutas_semana = angular.copy(r.data.rutas);\n                        $scope.totales_semana = angular.copy(r.data.totales);\n\n                        graficas.line_semana = new echartsPlv();\n                        graficas.line_semana.init(\"lineal_semana\", r.data.line_chart);\n\n                        graficas.pie_semana = new echartsPlv();\n                        graficas.pie_semana.init(\"pastel_semana\", r.data.pie_chart);\n                    }\n                }, $scope.filters);\n            }\n        }, 1000);\n    };\n\n    $scope.getDataDia = function () {\n        setTimeout(function () {\n            var dia_open = $(\"#collapse_3_1\").hasClass('in');\n            if (dia_open) {\n                ventas.dia(function (r) {\n                    if (r.data) {\n                        $scope.data.dia = angular.copy(r.data.data);\n                        $scope.rutas_dia = angular.copy(r.data.rutas);\n                        $scope.totales_dia = angular.copy(r.data.totales);\n\n                        graficas.line_dia = new echartsPlv();\n                        graficas.line_dia.init(\"lineal_dia\", r.data.line_chart);\n\n                        graficas.pie_dia = new echartsPlv();\n                        graficas.pie_dia.init(\"pastel_dia\", r.data.pie_chart);\n                        $scope.$apply();\n                    }\n                }, $scope.filters);\n            }\n        }, 1000);\n    };\n\n    $scope.init = function () {\n        ventas.last(function (r) {\n            $scope.anios = r.years;\n            $scope.filters.year = r.last_year;\n\n            $scope.semanas = r.semanas;\n            $scope.filters.semana = r.last_week;\n\n            /*ventas.index(r => {\n                if(r.data){\n                    $scope.semanas = r.data.semanas\n                    if(r.data.ultimaSemana){\n                        $scope.filters.semana = r.data.ultimaSemana\n                    }\n                }\n            },{\n                year : r.last_year\n            })*/\n        });\n    };\n\n    $scope.init();\n}]);\n\n//# sourceURL=webpack:///./js_modules/ventas.js?");

/***/ })

/******/ });