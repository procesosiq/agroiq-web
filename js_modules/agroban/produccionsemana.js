app.filter('orderObjectBy', function() {
    return function(items, field, reverse) {
        var filtered = [];
        angular.forEach(items, function(item) {
            filtered.push(item);
        });
        filtered.sort(function (a, b) {
            if(parseFloat(a[field]) && parseFloat(b[field]))
                return (parseFloat(a[field]) > parseFloat(b[field]) ? 1 : -1);
            else
                return (a[field] > b[field] ? 1 : -1);
        });
        if(reverse) filtered.reverse();
        return filtered;
    };
});

app.service('request', [ '$http', ($http) => {

    var service = {}

    service.tags = (callback, params) => {
        $http.post('phrapi/marun/produccionsemana/tags', params || {}).then(r => {
            callback(r.data)
        })
    }

    service.graficaPromEdad = (callback, params) => {
        /*$http.post('phrapi/marun/produccionsemana/graficaEdadPromedio', params || {}).then(r => {
            callback(r.data)
        })*/
    }

    service.resumenAcumulado = (callback, params) => {
        /*$http.post('phrapi/marun/produccionsemana/resumenAcumulados', params || {}).then(r => {
            callback(r.data)
        })*/
    }

    service.reporteProduccion = (callback, params) => {
        load.block('reporte-produccion')
        $http.post('phrapi/marun/produccionsemana/reporteProduccion', params || {}).then(r => {
            load.unblock('reporte-produccion')
            callback(r.data)
        })
    }

    service.graficaVariables = (callback, params) => {
        load.block('graficas')
        $http.post('phrapi/marun/produccionsemana/graficaVariables', params || {}).then(r => {
            load.unblock('graficas')
            callback(r.data)
        })
    }

    service.getLastWeekOfYear = (callback, params) => {
        $http.post('phrapi/marun/produccionsemana/lastWeek', params || {}).then(r => {
            callback(r.data)
        })
    }

    return service
}])

function number_format(amount, decimals) {
    
    amount += ''; // por si pasan un numero en vez de un string
    amount = parseFloat(amount.replace(/[^0-9\.]/g, '')); // elimino cualquier cosa que no sea numero o punto

    decimals = decimals || 0; // por si la variable no fue fue pasada

    // si no es un numero o es igual a cero retorno el mismo cero
    if (isNaN(amount) || amount === 0) 
        return parseFloat(0).toFixed(decimals);

    // si es mayor o menor que cero retorno el valor formateado como numero
    amount = '' + amount.toFixed(decimals);

    var amount_parts = amount.split('.'),
        regexp = /(\d+)(\d{3})/;

    while (regexp.test(amount_parts[0]))
        amount_parts[0] = amount_parts[0].replace(regexp, '$1' + ',' + '$2');

    return amount_parts.join('.');
}

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function checkUmbralMerma(value, umbral){
    if(value > 0 && umbral){
        if(value >= umbral) return 'font-green-haze'
        else return 'font-red-thunderbird'
    }
    return ''
}

let normales = ['RACIMOS CORTADOS/Ha', 'RACIMOS PROCESADOS/Ha', 'RACIMOS RECUSADOS/Ha', 'CAJAS CONV/Ha', 'ENFUNDE/Ha', 'PESO RACIMO PROM KG', 'EDAD PROMEDIO', 'CALIB 2DA PROM', 'CALIB ULT PROM', 'MANOS PROM', 'DEDOS PROM', 'RATIO CORTADO', 'RATIO PROCESADO', '% RECOBRO']
let invertidos = ['% TALLO', '% RECUSADOS', '% MERMA CORTADA', '% MERMA PROCESADA', '% MERMA NETA MUESTREO']

function checkUmbral(value, umbral, invertido = false){
    if(invertido){
        if(value > 0 && umbral > 0){
            if(value <= umbral) return 'font-green-haze'
            else return 'font-red-thunderbird'
        }
    }else{
        if(value > 0 && umbral > 0){
            if(value >= umbral) return 'font-green-haze'
            else return 'font-red-thunderbird'
        }
    }
    return ''
}

app.controller('produccion', ['$scope','client', 'request', function($scope, client, $request){
    
    $scope.interval = {};
    $scope.variables = {
        cajas : [
            "Cajas",
            "Cajas/Ha"
        ],
        clima : [
            "Horas Luz (100)",
            "Horas Luz (150)",
            "Horas Luz (200)",
            "Horas Luz (400)",
            "Humedad",
            "Lluvia",
            "Rad. Solar",
            "Temp Max.",
            "Temp Min.",
        ],
        merma : [
            "Merma Neta",
            "Empaque",
            "Deshoje",
            "Cosecha",
            "Lotero Aereo",
            "Administracion",
            "Fisiologicos",
            "Apuntalador"
        ],
        racimos : [
            "Calibre Segunda",
            "Calibre Ultima",
            "Edad",
            "Manos",
            "Peso",
            "Racimos Procesados",
            "Racimos Cosechados",
            "Ratio Cortado",
            "Ratio Procesado",
            "Racimos Cosechados/Ha",
            "Racimos Procesados/Ha",
            "Racimos Recusados/Ha",
            "% Tallo",
            "% Recusados",
            "% Merma Cortada",
            "% Merma Procesada",
            "% Recobro",
            "Ratooning"
        ],
        enfunde : [
            "Enfunde/Ha"
        ],
        sigat : [
            "HVLE (3M)",
            "Q<5 (3M)",
            "H3 (3M)",
            "H4 (3M)",
            "H5 (3M)",
            "HT (0S)",
            "Q<5 (0S)",
            "HVLE (0S)",
            "HT (11S)",
            "Q<5 (11S)",
            "FOLIAR",
        ]
    };

    $scope.anioCambio = false
    $scope.id_company = 0;
    $scope.filters = {
        idFinca : 1,
        idLote : 0,
        fecha_inicial : moment().startOf('month').format('YYYY-MM-DD'),
        fecha_final : moment().endOf('month').format('YYYY-MM-DD'),
        type : "cosechados",
        year : moment().year(),
        var1 : "Peso",
        var2 : "Calibre Segunda",
        type1 : "line",
        type2 : "line"
    }

    $scope.getLastWeek = (year, callback) => {
        $request.getLastWeekOfYear(function(data){
            callback(data)
        }, { year: $scope.filters.year, idFinca : $scope.filters.idFinca })
    }

    $scope.changeYear = () => {
        let data = $scope.getLastWeek($scope.filters.year, function(data) {
            //$scope.filters.fecha_inicial = data.firts_date
            //$scope.filters.fecha_final = data.second_date
            $("#date-picker").html(`${data.firts_date} - ${data.second_date}`)

            $scope.changeRangeDate({
                first_date : data.firts_date,
                second_date : data.second_date
            })
        })
    }

    $scope.changeFinca = () => {
        var data = $scope.filters;
        $request.tags(printTags, data)
        $scope.initGraficaEdadProm()
        $scope.initGraficaVariables()
        //$request.resumenAcumulado(printAcumulado, data)
        $scope.initTableReporteProduccion()
    }

    $scope.StartEndDateDirectives = {
        startDate : moment().startOf('month'),
        endDate :moment().endOf('month'),
    }

    $scope.changeRangeDate = function(data){
        if(data){
            $scope.anioCambio = moment(data.first_date).year() != moment($scope.filters.fecha_inicial).year()

            $scope.filters.fecha_inicial = data.hasOwnProperty("first_date") ? data.first_date : $scope.filters.fecha_inicial;
            $scope.filters.fecha_final = data.hasOwnProperty("second_date") ? data.second_date : $scope.filters.fecha_final;
        }else{
            $scope.anioCambio = false
        }

        var data = $scope.filters;
        $request.tags(printTags, data)
        if($scope.anioCambio){
            $scope.filters.year = moment($scope.filters.fecha_inicial).year()
            $scope.initGraficaEdadProm()
            $scope.initGraficaVariables()
            $request.resumenAcumulado(printAcumulado, data)
            $scope.initTableReporteProduccion()
        }
    }

    $scope.colores = [];
    $scope.totales = [];
    $scope.palancas = [];
    $scope.lotes = [];

    $scope.tags = {
        racimo : {
            value : 0
        },
        ratio_cortado : {
            value : 0
        },
        ratio_procesado : {
            value : 0
        },
        ratooning : {
            value : 0
        },
        merma_cortada : {
            value : 0
        },
        merma_procesada : {
            value : 0
        },
        edad : {
            value : 0
        },
        recusados : {
            value : 0
        },
        cajas : {
            value :0
        },
        otras_cajas : {
            value : 0
        },
        calibracion : {
            value : 0
        },
        merma_cosechada : {
            value : 0
        },
        enfunde : {
            value : 0
        },
        recobro : {
            value : 0
        },
        merma_neta : {
            value : 0
        },
        merma_cajas : {
            value : 0
        },
        merma_dolares : {
            value : 0
        },
        merma_kg : {
            value : 0
        },
        rac_cos_ha_sem : { value : 0 },
        rac_cos_ha_anio : { value : 0 },
        cajas_ha_sem : { value : 0 },
        cajas_ha_anio : { value : 0 },
    }


    $scope.charts = {
        variables : new echartsPlv(),
    }    

    var printTags = (r) => {
        $scope.tags.cajas.value = r.peso_prom_cajas;
        $scope.tags.calibracion.value = r.calibracion_prom;
        $scope.tags.racimo.value = r.peso_prom_racimos;
        $scope.tags.edad.value = r.edad_prom_racimos;
        $scope.tags.ratio_cortado.value = r.ratio_cortado;
        $scope.tags.ratio_procesado.value = r.ratio_procesado;
        $scope.tags.merma_cosechada.value = r.porc_merma_cosechada;
        $scope.tags.merma_procesada.value = r.porc_merma_procesado;
        $scope.tags.enfunde.value = r.enfunde_ha;
        $scope.tags.merma_neta.value = r.porc_merma_neta;
        $scope.tags.merma_kg.value = r.merma_kg;
        $scope.tags.merma_cajas.value = r.merma_cajas;
        $scope.tags.merma_dolares.value = r.merma_dolares;
        $scope.tags.rac_cos_ha_sem.value = r.rac_cos_ha_sem;
        $scope.tags.rac_cos_ha_anio.value = r.rac_cos_ha_anio;
        $scope.tags.cajas_ha_sem.value = r.cajas_conv_ha_sem;
        $scope.tags.cajas_ha_anio.value = r.cajas_conv_ha_anio;
        $scope.tags.recobro.value = r.recobro
        $scope.tags.ratooning.value = r.ratooning

        setTimeout(function(){
            $(".counter_tags").counterUp({
                delay: 10,
                time: 1000
            });

            $scope.num_semanas = ($scope.filters.year == moment().format('YYYY')) ? moment().week() : 52;
        }, 250)
    }

    var printAcumulado = (r) => {
        $scope.anios = r.years
        $scope.resumen = r.resumen
    }

    $scope.init = function(){
        var data = $scope.filters;
        $request.tags(printTags, data)
        $request.resumenAcumulado(printAcumulado, data)
        $scope.initGraficaEdadProm()
        $scope.initGraficaVariables()
        $scope.initTableReporteProduccion()
    }

    $scope.start = true;
    $scope.cajas = 0;

    var printTablaProduccion = (r) => {
        let id = 'reporte-produccion'
        var props = {
            header : [{
                   key : 'campo',
                   name : 'VARIABLE',
                   titleClass : 'text-center',
                   locked : true,
                   expandable : true,
                   resizable : true,
                   width: 170
                },{
                   key : 'avg',
                   name : 'AVG',
                   locked : true,
                   sortable : true,
                   titleClass : 'text-center',
                   alignContent : 'right',
                   filterable : true,
                   filterRenderer: 'NumericFilter',
                   formatter : 'Number',
                   resizable : true,
                   customCell : function(rowData, isChildren){
                        let valNumber = parseFloat(rowData['avg'])
                        let valueCell = valNumber > 0 ? valNumber : ''
                        var umbralClass = umbralClass = checkUmbral(valNumber, rowData['avg'], invertidos.indexOf(rowData['campo']) > -1)
                        return `
                            <div class="text-center ${umbralClass}" style="height: 100%">
                                ${valueCell}
                            </div>
                        `;
                    }
                },{
                   key : 'max',
                   name : 'MAX',
                   locked : true,
                   titleClass : 'text-center',
                   sortable : true,
                   alignContent : 'right',
                   filterable : true,
                   filterRenderer: 'NumericFilter',
                   resizable : true,
                   customCell : function(rowData, isChildren){
                        let valNumber = parseFloat(rowData['max'])
                        let valueCell = valNumber > 0 ? valNumber : ''
                        var umbralClass = ''
                        var umbralClass = umbralClass = checkUmbral(valNumber, rowData['avg'], invertidos.indexOf(rowData['campo']) > -1)

                        return `
                            <div class="text-center ${umbralClass}" style="height: 100%">
                                ${valueCell}
                            </div>
                        `;
                    }
                },{
                   key : 'min',
                   name : 'MIN',
                   locked : true,
                   sortable : true,
                   titleClass : 'text-center',
                   alignContent : 'right',
                   filterable : true,
                   filterRenderer: 'NumericFilter',
                   resizable : true,
                   customCell : function(rowData, isChildren){
                        let valNumber = parseFloat(rowData['min'])
                        var valueCell = valNumber > 0 ? valNumber : ''
                        var umbralClass = ''
                        var umbralClass = umbralClass = checkUmbral(valNumber, rowData['avg'], invertidos.indexOf(rowData['campo']) > -1)

                        return `
                            <div class="text-center ${umbralClass}" style="height: 100%">
                                ${valueCell}
                            </div>
                        `;
                    }
                }
            ],
            data : r.data,
            height : r.data.length*35+80,
            buttons : [
                {
                    title : 'Excel',
                    action : () => {
                        $scope.table1.exportToExcel()
                    },
                    className : ''
                }
            ]
        }
        Object.keys(r.semanas).map((key) => {
            let value = r.semanas[key]
            props.header.push({
                key : `sem_${value}`,
                name : `${value}`,
                sortable : true,
                alignContent : 'right',
                titleClass : 'text-center',
                filterable : true,
                filterRenderer: 'NumericFilter',
                resizable : true,
                customCell : function(rowData, isChildren){
                    let valNumber = parseFloat(rowData['sem_'+value])
                    let valueCell = valNumber > 0 ? valNumber : ''
                    var umbralClass = ''
                    var umbralClass = umbralClass = checkUmbral(valNumber, rowData['avg'], invertidos.indexOf(rowData['campo']) > -1)

                    return `
                        <div class="text-center ${umbralClass}" style="height: 100%">
                            ${valueCell}
                        </div>
                    `;
                }
            })
        })
        document.getElementById(id).innerHTML = ""
        $scope.table1 = ReactDOM.render(React.createElement(ReactDataGrid, props), document.getElementById(id))
    }

    var printGraficaEdad = (r) => {
        if(r.chart.legend.length == 0){
            var data = {
                series: [0],
                legend: ['NO HAY DATOS'],
                umbral: r.chart.umbral,
                id: "edad_promedio",
                legendBottom : true,
                zoom : false,
                type : 'line',
                min : 'dataMin',
                showLegends : false
            }
            ReactDOM.render(React.createElement(Historica, data), document.getElementById('edad_promedio'));
        }else{
            var data = {
                series: r.chart.data,
                legend: r.chart.legend,
                umbral: r.chart.umbral,
                id: "edad_promedio",
                legendBottom : true,
                zoom : false,
                type : 'line',
                min : 'dataMin',
                showLegends : false
            }
            ReactDOM.render(React.createElement(Historica, data), document.getElementById('edad_promedio'));
        }
    }

    $scope.initGraficaEdadProm = () => {
        $request.graficaPromEdad(printGraficaEdad, $scope.filters)
    }
    $scope.initGraficaEdadProm()

    $scope.initTableReporteProduccion = () => {
        $request.reporteProduccion(printTablaProduccion, $scope.filters)
    }

    var data_variables = {}
    var printGraficaVariables = (r) => {
        data_variables = r.data
        let parent = $("#variables").parent()
        parent.empty()
        parent.append('<div id="variables" style="height:500px;"></div>')
        $scope.charts.variables.init('variables', r.data)
    }

    $scope.initGraficaVariables = () => {
        $request.graficaVariables(printGraficaVariables, $scope.filters)
    }

    $scope.selected = (val) => {
        return [$scope.filters.var1, $scope.filters.var2].indexOf(val) > -1;
    }

    $scope.toggleLineBar = (num, type) => {
        if(num == 1)
        data_variables.series.map((data, index) => {
            if(data.name.toUpperCase().includes($scope.filters.var1.toUpperCase())){
                data_variables.series[index].type = type
                $scope.filters.type1 = type
            }
        })

        if(num == 2)
        data_variables.series.map((data, index) => {
            if(data.name.toUpperCase().includes($scope.filters.var2.toUpperCase())){
                data_variables.series[index].type = type
                $scope.filters.type2 = type
            }
        })
        printGraficaVariables({data : data_variables})
    }

    $scope.table = {
        orderBy : 'semana',
        reverse : false
    }
    $scope.setOrder = (column) => {
        if($scope.table.orderBy != column){
            $scope.table.orderBy = column
            $scope.table.reverse = false
        } else {
            $scope.table.reverse = !$scope.table.reverse
        }
    }

}]);

