app.filter('sumOfValue', function () {
    return function (data, key) {
        if (angular.isUndefined(data) || angular.isUndefined(key))
            return 0;        
        var sum = 0;        
        angular.forEach(data,function(value){
            if(parseFloat(value[key]))
                sum = sum + parseFloat(value[key], 10);
        });        
        return sum;
    }
})

app.filter('orderObjectBy', function() {
	return function(items, field, reverse) {
    	var filtered = [];
    	angular.forEach(items, function(item) {
    		if(!isNaN(parseInt(item))){
    			item = parseInt(item);
    		}
      		filtered.push(item);
    	});
    	filtered.sort(function (a, b) {
            if(field == 'hora' || field == 'fecha'){
                return moment(a.date).isAfter(b.date) ? 1 : -1;
            }else if(parseFloat(a[field]) && parseFloat(b[field])){
				return (parseFloat(a[field]) > parseFloat(b[field]) ? 1 : -1);
			}else{
				return (a[field] > b[field] ? 1 : -1);
			}
    	});
    	if(reverse) filtered.reverse();
    	return filtered;
  	};
});

app.filter('orderBy', function() {
	return function(items, reverse) {
    	var filtered = [];
    	angular.forEach(items, function(item) {
    		if(!isNaN(parseInt(item))){
    			item = parseInt(item);
    		}
      		filtered.push(item);
    	});
    	filtered.sort(function (a, b) {
            if(parseFloat(a) && parseFloat(b)){
				return (parseFloat(a) > parseFloat(b) ? 1 : -1);
			}else{
				return (a > b ? 1 : -1);
			}
    	});
    	if(reverse) filtered.reverse();
    	return filtered;
  	};
});

app.service('request', ['$http', function($http){
    this.getData = function(callback, params){
        let url = 'phrapi/sumifru/recobro/data'
        let data = params || {}

        load.block('data-table')
        $http.post(url, data).then((r) => {
            callback(r.data)
            load.unblock('data-table')
        })
        .catch(() => {
            load.unblock('data-table')
        });
    }

    this.index = (callback, params) => {
        $http.post('phrapi/sumifru/resumenenfunde/index', params || {}).then((r) => {
            callback(r.data)
        })
    }

    this.last = (callback, params) => {
        $http.post('phrapi/sumifru/resumenenfunde/last', params ||  {}).then((r) => {
            callback(r.data)
        })
    }
}])

app.controller('produccion', ['$scope', 'request', '$filter', function($scope, $request, $filter){

    $scope.totales = {}
    $scope.filters = {
        year : '',
        week : '',
        id_finca : ''
    }

    $scope.init = () => {
        load.block('saldo_fundas')
        $request.index((r) => {
            load.unblock('saldo_fundas')
            $scope.colorClass = r.color.class
            $scope.edades = r.edades
            $scope.data = r.data
            $scope.reloadTotales()
        }, $scope.filters)
    }

    getUmbral = (value) => {
        if(value){
            if(value < 98)
                return 'bg-red-thunderbird bg-font-red-thunderbird'
            else if (value == 98)
                return 'bg-yellow-gold bg-font-yellow-gold'
            else
                return 'bg-green-haze bg-font-green-haze'
        }
    }
    $scope.getUmbral = getUmbral

    $scope.getDataTable = ( ) => {
        $request.getData((r) => {
            $scope.lotes = r.lotes

            r.data.map((row) => {
                Object.keys(row).map((col) => {
                    if(col != 'sem_enf')
                    if(col.includes('sem_')){
                        if(row[col] == 0) row[col] = ''
                    }
                })
            })

            initTable(r.data, r.semanas_edad)
        }, $scope.filters)
    }

    initTable = (data, semanas) => {
        var props = {
            header : [{
                   key : 'sem_enf',
                   name : 'SEM/ENF',
                   titleClass : 'text-center',
                   alignContent : 'center',
                   locked : true,
                   expandable : true,
                   resizable : true,
                   width: 100,
                   customCell : (rowData) => {
                        let valueCell = rowData[`sem_enf`]
                        return `
                            <div class="text-center ${rowData['class']}" style="height: 100%; padding: 5px;">
                                ${valueCell}
                            </div>
                        `
                   }
                },{
                   key : 'enfunde',
                   name : 'RAC ENF',
                   locked : true,
                   sortable : true,
                   titleClass : 'text-center',
                   alignContent : 'center',
                   filterable : true,
                   filterRenderer: 'NumericFilter',
                   formatter : 'Number',
                   resizable : true,
                }
            ],
            data : data,
            buttons : [
                {
                    title : 'Excel',
                    action : () => {
                        $scope.table1.exportToExcel()
                    },
                    className : ''
                }
            ]
        }
        semanas.map((sem) => {
            props.header.push({
                key : `sem_${sem}`,
                name : `${sem}`,
                sortable : true,
                titleClass : 'text-center',
                alignContent : 'center',
                //filterable : true,
                filterRenderer: 'NumericFilter',
                formatter : 'Number',
                resizable : true,
                width: 50
                /*customCell : function(rowData){
                    let valueCell = parseInt(rowData[`sem_${sem}`])
                    let textCell = (valueCell > 0) ? valueCell : ''
                    return `
                        <div class="text-center" style="height: 100%;">
                            ${textCell}
                        </div>
                    `;
                }*/
             })
        })
        props.header.push({
            key : `total`,
            name : `TOTAL COSE`,
            sortable : true,
            titleClass : 'text-center',
            alignContent : 'center',
            filterable : true,
            filterRenderer: 'NumericFilter',
            formatter : 'Number',
            resizable : true,
        })
        props.header.push({
            key : `saldo`,
            name : `SALDO`,
            sortable : true,
            titleClass : 'text-center',
            alignContent : 'center',
            filterable : true,
            filterRenderer: 'NumericFilter',
            formatter : 'Number',
            resizable : true,
        })
        props.header.push({
            key : `rec`,
            name : `% RECOBRO`,
            sortable : true,
            titleClass : 'text-center',
            alignContent : 'center',
            filterable : true,
            filterRenderer: 'NumericFilter',
            formatter : 'Number',
            resizable : true,
            customCell : function(rowData){
                let valueCell = parseFloat(rowData[`rec`])
                let textCell = (valueCell > 0) ? valueCell : ''
                let umbral = getUmbral(valueCell)
                return `
                    <div class="text-center ${umbral}" style="height: 100%; padding: 5px;">
                        ${textCell}
                    </div>
                `;
            }
        })
        props.header.push({
            key : `cintas_caidas`,
            name : `CINTAS CAIDAS`,
            sortable : true,
            titleClass : 'text-center',
            alignContent : 'center',
            filterable : true,
            filterRenderer: 'NumericFilter',
            formatter : 'Number',
            resizable : true,
        })
        props.header.push({
            key : `porc_cintas_caidas`,
            name : `% CAIDAS`,
            sortable : true,
            titleClass : 'text-center',
            alignContent : 'center',
            filterable : true,
            filterRenderer: 'NumericFilter',
            formatter : 'Number',
            resizable : true,
        })
        props.header.push({
            key : `porc_total`,
            name : `% TOTAL`,
            sortable : true,
            titleClass : 'text-center',
            alignContent : 'center',
            filterable : true,
            filterRenderer: 'NumericFilter',
            formatter : 'Number',
            resizable : true,
        })
        props.header.push({
            key : `no_recuperable`,
            name : `NO RECUP`,
            sortable : true,
            titleClass : 'text-center',
            alignContent : 'center',
            filterable : true,
            filterRenderer: 'NumericFilter',
            formatter : 'Number',
            resizable : true,
        })
        $("#data-table").html("")
        $scope.table1 = ReactDOM.render(React.createElement(ReactDataGrid, props), document.getElementById('data-table'))
    }

    $scope.reloadTotales = () => {
        setTimeout(() => {
            $scope.$apply(() => {
                $scope.totales.racimos_enfunde = $filter('sumOfValue')($scope.data, 'racimos_enfunde')
                $scope.totales.recobro = $scope.totales.cosechados > 0 && $scope.totales.racimos_enfunde > 0 ? ($scope.totales.cosechados / $scope.totales.racimos_enfunde * 100) : 0
                $scope.totales.cintas_caidas = $filter('sumOfValue')($scope.data, 'cintas_caidas') || 0
                $scope.totales.p_cintas_caidas = $scope.totales.cintas_caidas > 0 && $scope.totales.racimos_enfunde > 0 ? ($scope.totales.cintas_caidas / $scope.totales.racimos_enfunde * 100) : 0
            })
        }, 100)
    }

    $scope.last = () => {
        $request.last((r) => {
            $scope.fincas = r.fincas
            $scope.years = r.years
            $scope.semanas = r.weeks
            if(r.last_year && r.last_week){
                $scope.filters.year = r.last_year
                $scope.filters.week = r.last_week
            }
            $scope.init()
        }, $scope.filters)
    }

    $scope.last()
    
    $scope.getDataTable()
}]);

