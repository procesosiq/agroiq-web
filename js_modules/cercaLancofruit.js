app.service('server', ['$http', function($http){
    this.index = (params) => {
        let url = 'phrapi/lancofruit/polygon/index'
        return $http.post(url , params || {})
    }
    this.save = (params) => {
        let url = 'phrapi/lancofruit/polygon/save'
        return $http.post(url , params || {})
    }
}]);

app.controller('lancofruit', ['$scope' , 'server' , function($scope , server){

    $scope.filters = {
        search : ""
    }
    $scope.nombre = ""

    const options = {
        zoom: 15
        , center: new google.maps.LatLng(-2.188861, -79.898896)
        , mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    
    const map = new google.maps.Map(document.getElementById('map-polygon'), options);
    
    var polygonArray = []
    var myPolygon = null
    var ArrayDebugMode = []
    
    $scope.polygonArray = []


    const clearBuffering = () => {
        myPolygon.setMap(null)
        myPolygon = null
        polygonArray = []
        $scope.polygonArray = polygonArray
    }
    
    const draw = () => {
        if(polygonArray.length > 0){
            $scope.polygonArray = angular.copy(polygonArray)
            if(myPolygon == null){
                myPolygon = new google.maps.Polygon({
                    paths: polygonArray,
                    draggable: true, // turn off if it gets annoying
                    editable: true,
                    strokeColor: '#FF0000',
                    strokeOpacity: 0.8,
                    strokeWeight: 2,
                    fillColor: '#FF0000',
                    fillOpacity: 0.35
                })
                myPolygon.setMap(map)
            }else{
                myPolygon.setPaths(polygonArray)
            }
        }else{
            clearBuffering()
        }
    }
    
    const getPoint = (event) => {
        let latitude = event.latLng.lat();
        let longitude = event.latLng.lng();
        let point = event.latLng;
        polygonArray.push(point)
        draw()
    }
    
    const deletePoint = (event) => {
        event.preventDefault()
        polygonArray.pop()
        draw()
    }
    
    const newPolygon = () => {
        let newPoly = true
        if(myPolygon != null){
            let result = confirm("Desea crear un nuevo polygono? , se perdera el progreso actual")
            if(!result) {
                newPoly = false
            }else{
                clearBuffering()
                $scope.nombre = ""
                $scope.id = 0
            }
        }

        if(newPoly){
            myPolygon = null
            polygonArray = []
            map.addListener('click' , getPoint)
        }
    }
    
    const getPolygonCoords = () => {
        let length = myPolygon.getPath().getLength()
        let points = []
        let coords = ``
        for (let i = 0; i < length; i++) {
            coords = `${myPolygon.getPath().getAt(i).lat()},${myPolygon.getPath().getAt(i).lng()}`
            points.push(coords)
        }

        return points
    }
    
    
    const savePolygon = () => {
        if(polygonArray.length > 2 && $scope.nombre != ""){
            google.maps.event.clearListeners(map , 'click')
            let points = getPolygonCoords()
            let options = {
                id : $scope.id ,
                nombre : $scope.nombre,
                paths : points
            }
            server.save(options).then( r => {
                let data = r.data
                if(data.code == 200){
                    clearBuffering()
                    alert("Guardada con exito" , "Cercas" , "success")
                    $scope.init()
                }
            })
            // showPolygon()
        }else{
            alert("Para generar una cerca debe contener al menos 3 puntos" , "Cercas")
        }
    }
    
    const clearMap = () => {
        clearBuffering()
    }
    
    const showPolygon = (paths) => {
        if(paths.length < 2){
            return false;
        }
        polygonArray = paths.map(e => new google.maps.LatLng(e[0],e[1]) )
        draw()
    }

    $scope.polygons = []
    $scope.id = 0
    $scope.showPoly = (polygon) => {
        if(myPolygon != null){
            myPolygon.setMap(null)
            myPolygon = null
        }
        polygonArray = []
        let coords = polygon.polygon.split("|").map((e) => e.split(","))
        $scope.id = polygon.id
        $scope.nombre = polygon.name
        showPolygon(coords)
    }
    $scope.init = () => {
        server.index().then( r => {
            let data = r.data
            if(data.code == 200){
                $scope.nombre = ""
                $scope.id = 0
                $scope.polygons = data.response
            }
        })
    }
    
    document.getElementById('last-point').addEventListener('click' , deletePoint)
    document.getElementById('new-draw').addEventListener('click' , newPolygon)
    document.getElementById('save-draw').addEventListener('click' , savePolygon)

}]);