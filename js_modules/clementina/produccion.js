app.service('produccion', ['client' , '$http', function(client, $http){
    this.lastDay = function(callback, params){
        let data = params || {}
        let url = 'phrapi/sumifru/produccion/last'
        client.post(url, callback, data)
    }

    this.registros = function(callback, params){
        let data = params || {}
        let url = 'phrapi/sumifru/produccion/historico'
        client.post(url, callback, data, 'registros')
    }

    this.racimosLote = function(callback, params){
        let data = params || {}
        let url = 'phrapi/sumifru/produccion/lote'
        client.post(url, callback, data, 'table_por_lote')
    }

    this.racimosPalanca = function(callback, params){
        let data = params || {}
        let url = 'phrapi/sumifru/produccion/palanca'
        client.post(url, callback, data, 'table_por_palanca')
    }

    this.racimosEdad = function(callback, params){
        let data = params || {}
        let url = 'phrapi/sumifru/produccion/edad'
        client.post(url, callback, data, 'table_por_edad')
    }

    this.promediosPorLote = function(callback, params){
        let data = params || {}
        let url = 'phrapi/sumifru/produccion/promedios'
        client.post(url, callback, data, 'promedios_lotes')
    }

    this.racimosFormularios = function(callback, params){
        let data = params || {}
        let url = 'phrapi/sumifru/produccion/formularios'
        client.post(url, callback, data, 'table_racimos_formularios')
    }

    this.eliminar = function(callback, params){
        let data = params || {}
        let url = 'phrapi/sumifru/produccion/eliminar'
        client.post(url, callback, data)
    }

    this.editar = function(callback, params){
        let data = params || {}
        let url = 'phrapi/sumifru/produccion/editar'
        client.post(url, callback, data, 'edit_registros')
    }

    this.cuadreRacimos = function(callback, params){
        let data = params || {}
        let url = 'phrapi/sumifru/produccion/cuadre'
        client.post(url, callback, data)
    }

    this.cuadrar = function(callback, params){
        let data = params || {}
        let url = 'phrapi/sumifru/produccion/cuadrar'
        client.post(url, callback, data)
    }

    this.analisisRecusados = function(callback, params){
        let data = params || {}
        let url = 'phrapi/sumifru/produccion/recusados'
        client.post(url, callback, data)
    }

    this.guardarExcel = function(callback, params){
        $http({
            method: 'POST',
            url: '/phrapi/sumifru/produccion/importar',
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            data: {
                upload: params.file
            },
            transformRequest: function (data, headersGetter) {
                var formData = new FormData();
                angular.forEach(data, function (value, key) {
                    formData.append(key, value);
                });

                var headers = headersGetter();
                delete headers['Content-Type'];

                return formData;
            }
        })
        .success(function (data) {
            callback(data)
        })
        .error(function (data, status) {
            callback(data)
        });
    }

    this.racimosPorViaje = function(callback, params){
        let data = params || {}
        let url = 'phrapi/sumifru/produccion/viajes'
        client.post(url, callback, data, 'collapse_3_8')
    }

    this.guardarViaje = function(callback, params){
        let data = params || {}
        let url = 'phrapi/sumifru/produccion/guardarViaje'
        client.post(url, callback, data)
    }

    this.procesarViaje = function(callback, params){
        let data = params || {}
        let url = 'phrapi/sumifru/produccion/procesarViaje'
        client.post(url, callback, data)
    }

    this.desprocesarViaje = function(callback, params){
        let data = params || {}
        let url = 'phrapi/sumifru/produccion/desprocesarViaje'
        client.post(url, callback, data)
    }

    this.crearViajeBalanza = function(callback, params){
        let data = params || {}
        let url = 'phrapi/sumifru/produccion/crearViaje'
        client.post(url, callback, data)
    }

    this.unirBalanzaFormulario = function(callback, params){
        let data = params || {}
        let url = 'phrapi/sumifru/produccion/unir'
        client.post(url, callback, data)
    }

    this.cambiarLote = function(callback, params){
        let data = params || {}
        let url = 'phrapi/sumifru/produccion/cambiarLote'
        client.post(url, callback, data)
    }

    this.cambiarCuadrilla = function(callback, params){
        let data = params || {}
        let url = 'phrapi/sumifru/produccion/cambiarCuadrilla'
        client.post(url, callback, data)
    }

    this.recusados = function(callback, params){
        load.block('racimos_recusados')
        $http.post('phrapi/sumifru/produccion/diarecusados', params || {})
        .then((r) => {
            load.unblock('racimos_recusados')
            callback(r.data)
        })
    }

    this.muestreo = function(callback, params){
        $http.post('phrapi/sumifru/produccion/muestreo', params || {})
        .then((r) => {
            callback(r.data)
        })
    }

    this.pesoCalibre = function(callback, params){
        $http.post('phrapi/sumifru/racimos/pesoCalibre', params || {})
        .then((r) => {
            callback(r.data)
        })
    }

    this.pesoMano = function(callback, params){
        $http.post('phrapi/sumifru/racimos/pesoMano', params || {})
        .then((r) => {
            callback(r.data)
        })
    }

    this.pesoRacimo = function(callback, params){
        $http.post('phrapi/sumifru/racimos/pesoRacimo', params || {})
        .then((r) => {
            callback(r.data)
        })
    }

    this.cantidadRacimoCalibreManos = function(callback, params){
        $http.post('phrapi/sumifru/racimos/cantidadRacimoCalibreManos', params || {})
        .then((r) => {
            callback(r.data)
        })
    }
}])

app.filter('num', function() {
    return function(input) {
    return parseInt(input, 10);
    };
});

app.filter('startFrom', function() {
    return function(input, start) {
        start = +start; //parse to int
        return input.slice(start);
    }
});

app.filter('sumOfValue', function () {
    return function (data, key) {        
        if (angular.isUndefined(data) || angular.isUndefined(key))
            return 0;        
        var sum = 0;
        angular.forEach(data,function(value){
            if(value[key] != "" && value[key] != undefined && parseFloat(value[key])){
                sum = sum + parseFloat(value[key], 10);
            }
        });
        return sum;
    }
})

app.filter('countOfValue', function () {
    return function (data, key) {
        if (angular.isUndefined(data) || angular.isUndefined(key))
            return 0;        
        var count = 0;
        angular.forEach(data,function(value){
            if(value[key] != undefined && value[key] != null && value[key] > 0){
                count++
            }
        });
        return count;
    }
})

app.filter('avgOfValue', function () {
    return function (data, key) {        
        if (angular.isUndefined(data) || angular.isUndefined(key))
            return 0;        
        var sum = 0;
        var count = 0;
        angular.forEach(data,function(value){
            if(value[key] != "" && value[key] != undefined && parseFloat(value[key])){
                sum = sum + parseFloat(value[key], 10);
                count++;
            }
        });
        sum = sum / count;
        return sum;
    }
})

app.filter('orderObjectBy', function() {
	return function(items, field, reverse) {
    	var filtered = [];
    	angular.forEach(items, function(item) {
            if(field == 'hora' || field == 'fecha'){
                item.date = moment(item.fecha  + ' ' + item.hora)
            }
    		if(!isNaN(parseInt(item))){
    			item = parseInt(item);
    		}
      		filtered.push(item);
    	});
    	filtered.sort(function (a, b) {
            if(field == 'hora' || field == 'fecha'){
                return moment(a.date).isAfter(b.date) ? 1 : -1;
            }else if(parseFloat(a[field]) && parseFloat(b[field])){
				return (parseFloat(a[field]) > parseFloat(b[field]) ? 1 : -1);
			}else{
				return (a[field] > b[field] ? 1 : -1);
			}
    	});
    	if(reverse) filtered.reverse();
    	return filtered;
  	};
});

app.directive('file', function () {
    return {
        scope: {
            file: '='
        },
        link: function (scope, el, attrs) {
            el.bind('change', function (event) {
                var file = event.target.files[0];
                scope.file = file ? file : undefined;
                scope.$apply();
            });
        }
    };
});

app.directive('enter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.enter);
                });

                event.preventDefault();
            }
        });
    };
});

app.directive('escape', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if(event.which === 27) {
                scope.$apply(function (){
                    scope.$eval(attrs.escape);
                });

                event.preventDefault();
            }
        });
    };
});

function number_format(amount, decimals) {
    
    amount += ''; // por si pasan un numero en vez de un string
    amount = parseFloat(amount.replace(/[^0-9\.]/g, '')); // elimino cualquier cosa que no sea numero o punto

    decimals = decimals || 0; // por si la variable no fue fue pasada

    // si no es un numero o es igual a cero retorno el mismo cero
    if (isNaN(amount) || amount === 0) 
        return parseFloat(0).toFixed(decimals);

    // si es mayor o menor que cero retorno el valor formateado como numero
    amount = '' + amount.toFixed(decimals);

    var amount_parts = amount.split('.'),
        regexp = /(\d+)(\d{3})/;

    while (regexp.test(amount_parts[0]))
        amount_parts[0] = amount_parts[0].replace(regexp, '$1' + ',' + '$2');

    return amount_parts.join('.');
}

function checkUmbral (value, umbral) {
    if(value && umbral){
        if(value > umbral) return 'bg-red-thunderbird bg-font-red-thunderbird'
        else return 'bg-green-haze bg-font-green-haze'
    }
    return ''
}
let datesEnabled = []
app.controller('produccion', ['$scope', 'produccion', function($scope, produccion){

    datepickerHighlight = () => {
        $('#datepicker').datepicker({
            beforeShowDay: function (date) {
                let fecha = moment(date).format('YYYY-MM-DD')
                let has = datesEnabled.indexOf(fecha) > -1
                return has ? { classes: 'highlight', tooltip: 'Procesado', enabled : true } : { tooltip : 'Sin proceso', enabled : false }
            }
        });
        $('#datepicker').datepicker()
        .on('changeDate', function(e) {
            $scope.changeRangeDate({ first_date: moment(e.date).format('YYYY-MM-DD'), second_date : moment(e.date).format('YYYY-MM-DD') })
        });
        $("#datepicker").datepicker('setDate', $scope.produccion.params.fecha_inicial)
        $('#datepicker').datepicker('update')
    }

    $scope.id_company = 0;
    $scope.subTittle = ""
    $scope.registros = [];
    $scope.count = 0;
    $scope.anioCambio = false
    $scope.viajeSelected = {
        racimos : {}
    }
    $scope.grupo_mano = 1
    $scope.grupo_racimo = 3
    $scope.grupo_calibre = 0.2
    $scope.graficas_add = {
        manos : [],
        calibre_segunda : [],
        calibre_ultima : [],
        var : 'manos'
    }
    $scope.coloresClass = {
        'VERDE' : 'bg-green-haze bg-font-green-haze',
        'AZUL' : 'bg-blue bg-font-blue',
        'BLANCO' : 'bg-white bg-font-white',
        'LILA' : 'bg-purple-studio bg-font-purple-studio',
        'ROJA' : 'bg-red-thunderbird bg-font-red-thunderbird',
        'CAFE' : 'bg-brown bg-font-brown',
        'AMARILLO' : 'bg-yellow-lemon bg-font-yellow-lemon',
        'NEGRO' : 'bg-dark bg-font-dark',
        'CELESTE' : 'bg-blue-sharp bg-font-blue-sharp',
        '' : 'bg-white bg-font-white',
        'S/C' : 'bg-white bg-font-white'
    }
    $scope.getNumber = function(num) {
        $scope.numbers = []
        for(var i = 0; i < num; i++){
            $scope.numbers.push(i+1)
        }
    }
    getArray = function(num){
        var array = []
        for(var i = 0; i < num; i++){
            array.push(i+1)
        }
        return array
    }

    $scope.getNumber(20)

    $scope.produccion = {
        params : {
            finca : 1,
            idFinca : 1,
            idLote : 0,
            idLabor : 0,
            fecha_inicial : moment().startOf('month').format('YYYY-MM-DD'),
            fecha_final : moment().endOf('month').format('YYYY-MM-DD'),
            cliente: "",
            marca: "",
            palanca : "",
            initial : 0,
        },
        nocache : function(){
            $scope.init();
            $scope.getRegistros()

            if($scope.anioCambio){
                $scope.getMuestreo()
                $scope.getAnalisisRecusados()
            }
        }   
    }

    $scope.getMuestreo = () => {
        produccion.muestreo((r) => {
            $scope.muestreo = r.data
            $scope.openMuestreo()
        }, $scope.produccion.params)
    }

    $scope.openMuestreo = () => {
        setTimeout(() => {
            if($("[href=#collapse_3_6]").attr('aria-expanded') == 'true'){
                new echartsPlv().init('grafica-muestreo', $scope.muestreo)
            }
        }, 250)
    }

    $scope.startDate = ''
    $scope.getLastDay = function(){
        produccion.lastDay(function(r, b){
            b()
            if(r){
                $scope.fincas = r.fincas
                
                $scope.fecha = r.last.fecha
                $scope.produccion.params.fecha_inicial = r.last.fecha
                $scope.produccion.params.fecha_final = r.last.fecha
                $scope.StartEndDateDirectives = {
                    startDate : moment(r.last.fecha),
                    endDate : moment(r.last.fecha)
                }
                $scope.startDate = r.last.fecha
                datesEnabled = r.days
                datepickerHighlight()
                //$scope.produccion.nocache()
                $scope.getMuestreo()
                $scope.getAnalisisRecusados()
            }
        })
    }

    $scope.lastDay = {
        startDate : moment().startOf('month'),
        endDate : moment().endOf('month'),
    }

    $scope.changeRangeDate = function(data){
        if(data){
            $scope.anioCambio = moment(data.first_date).year() != moment($scope.produccion.params.fecha_inicial).year()

            $scope.produccion.params.fecha_inicial = data.hasOwnProperty("first_date") ? data.first_date : $scope.wizardStep.params.fecha_inicial;
            $scope.produccion.params.fecha_final = data.hasOwnProperty("second_date") ? data.second_date : $scope.wizardStep.params.fecha_final;
            $scope.subTittle = data.date_select || "Ultimo Registro"
        }
        
        $scope.produccion.nocache()
    }

    $scope.tabla = {
        produccion : []
    }

    $scope.openDetallePalanca = function(data){
        data.expanded = !data.expanded;
    }

    $scope.openDetalleLote = function(data){
        data.expanded = !data.expanded;
    }

    $scope.eliminar = () => {
        var elements = $(".delete")
        if(elements.length > 0){
            if(confirm(`Seguro deseas eliminar ${elements.length} elementos?`)){
                var ids = []
                var index = []
                for(var x = 0; x < elements.length; x++){
                    ids.push({
                        id : $(elements[x]).attr('data-id')
                    })
                }
                produccion.eliminar((r, b) => { 
                    b()
                    if(r){
                        $scope.getRegistros()
                        alert(`Se borraron ${elements.length} registros con éxito`, "", "success")
                    }
                }, { ids : ids })
            }
        }else{
            alert("No has seleccionado ninguna fila")
        }
    }

    $scope.colores = [];
    $scope.totales = [];
    $scope.palancas = [];

    $scope.init = function(){
        var data = $scope.produccion.params;
        produccion.racimosLote(function(r, b){
            b('table_por_lote')
            if(r){
                $scope.totales = r.totales;
                $scope.id_company = r.id_company;
                $scope.tabla.produccion = r.data;
                $scope.tags = r.tags;

                if(r.fincas){
                    $scope.fincas = r.fincas
                    if(!r.fincas.hasOwnProperty(data.finca) && data.finca != ''){
                        $scope.produccion.params.finca = Object.keys(r.fincas)[0]
                        data.finca = $scope.produccion.params.finca
                    }
                }

                produccion.racimosPalanca(function(r, b){
                    b('table_por_palanca')
                    if(r){
                        $scope.palancas = r.palancas;
                    }
                }, data)
                produccion.racimosEdad(function(r, b){
                    b('table_por_edad')
                    if(r){
                        $scope.tabla.edades = r.data;
                    }
                }, data)
                produccion.promediosPorLote(function(r, b){
                    b('promedios_lotes')
                    if(r){
                        $scope.promedios = r.data
                        $scope.edades = r.edades
                    }
                }, data)
                produccion.racimosFormularios(function(r, b){
                    b('table_racimos_formularios')
                    if(r){
                        $scope.prom_edad_formularios = r.prom_edad
                        $scope.racimos_formularios = r.racimos_formularios
                        $scope.racimos_formularios_edad = r.table_edades
                        $scope.racimos_formularios_palanca = r.table_palanca
                    }
                }, data)
                produccion.cuadreRacimos(function(r, b){
                    b()
                    if(r){
                        $scope.cuadre_proc = r.data
                        $scope.cuadre_recu = r.data_recu
                        $scope.dia_cuadrado = r.cuadrado > 0
                        $scope.faltante_recu = r.faltante_recusados
                    }
                }, data)
                produccion.recusados(function(r){
                    if(r.data.length > 0){
                        $scope.recusados = r.data
                    }else{
                        $scope.recusados = []
                    }
                }, data)

                $scope.openGraficasAdicionales()
            }
        }, data)

        $scope.getViajes(data)
    }

    $scope.openGraficasAdicionales = () => {
        setTimeout(() => {
            if($("[href=#collapse_3_10]").attr('aria-expanded') == 'true'){
                produccion.pesoMano((r) => {
                    $scope.pesoManoTendencia = r.data
                    $scope.pesoMano = r.dia
                    
                    new echartsPlv().init('grafica-peso-mano-tendencia', $scope.pesoManoTendencia)
                    new echartsPlv().init('grafica-peso-mano', $scope.pesoMano)
                }, Object.assign(
                    { grupo : $scope.grupo_mano },
                    $scope.produccion.params
                ))
    
                produccion.pesoCalibre((r) => {
                    $scope.pesoCalibreTendencia = r.data
                    $scope.pesoCalibre = r.dia

                    new echartsPlv().init('grafica-peso-calibre-tendencia', $scope.pesoCalibreTendencia)
                    new echartsPlv().init('grafica-peso-calibre', $scope.pesoCalibre)
                }, Object.assign(
                    { grupo : $scope.grupo_calibre },
                    $scope.produccion.params
                ))

                produccion.pesoRacimo((r) => {
                    $scope.pesoRacimo = r.dia
                    $scope.pesoManosPeso = r.vs_manos

                    new echartsPlv().init('grafica-peso-racimo', $scope.pesoRacimo)
                    new echartsPlv().init('grafica-manos-peso', $scope.pesoManosPeso)
                }, Object.assign(
                    { grupo : $scope.grupo_racimo },
                    $scope.produccion.params
                ))

                produccion.cantidadRacimoCalibreManos((r) => {
                    $scope.graficas_add.manos = r.manos
                    $scope.graficas_add.calibre_segunda = r.calibre_segunda
                    $scope.graficas_add.calibre_ultima = r.calibre_ultima
                    $scope.renderCantidad()
                }, Object.assign(
                    $scope.produccion.params
                ))
            }
        }, 250)
    }

    $scope.renderCantidad = () => {
        new echartsPlv().init('grafica-cantidad-manos-calibre', $scope.graficas_add[$scope.graficas_add.var])
    }

    $scope.getViajes = (data) => {
        if(!data) data = angular.copy($scope.produccion.params)
        produccion.racimosPorViaje(function(r, b){
            b('collapse_3_8')
            if(r){
                $scope.racimosViajes = r.data
                $scope.racimosViajes.map((row, i) => {
                    row.index = i
                })
            }
        }, data)
    }

    $scope.getRegistros = function(){
        $scope.cargandoHistorico = true
        let data = $scope.produccion.params;
        produccion.registros(function(r, b){
            b('registros')
            $scope.cargandoHistorico = false

            if(r){
                $scope.registros = r.data
                $scope.searchTable.changePagination()
            }
        }, data)
    }

    $scope.edit = function(row){
        if(!$scope.editing){
            $scope.editing = true;
            $scope.editRow = row;
            $scope.editRow.editing = true;
        }
    }

    $scope.guardar = function(){
        var valid = true
        if(!$scope.editing) valid = false;
        if($scope.editRow.peso <= 0) valid = false;
        if($scope.editRow.cuadrilla == "") valid = false;
        if($scope.editRow.lote == "") valid = false;
        if($scope.editRow.edad == "") valid = false;
        
        if(valid){
            $scope.editRow.editing = false;
            $scope.editing = false;
            produccion.editar((r, b) => {
                b('edit_registros')
                if(r){
                    $scope.editRow.class = r.class
                }else{
                    alert("Ocurrio un error al guardar")
                }
            }, $scope.editRow)
        }else{
            alert("Favor de completar los campos")
        }
    }

    $scope.cuadrar = function(){
        if(!$scope.dia_cuadrado){
            produccion.cuadrar((r, b) => {
                b()
                if(r){
                    if(r.status == 200){
                        alert("Refrescando", "", "success", () => location.reload() )
                        $scope.dia_cuadrado = true
                    }
                    else alert("Hubo un problema al procesar");
                }
            }, $scope.produccion.params)
        }else{
            alert("El dia ya ha sido procesado")
        }
    }

    $scope.start = true;

    $scope.enableEdit = (row, campo) => {
        if(!row.grupo_racimo > 0 || !row.form){
            if(campo == 'lote'){
                row.editingLote = true
            }
            else if(campo == 'cuadrilla'){
                row.editingCuadrilla = true
            }
        }
    }


    $scope.enter = (row, campo) => {
        if(campo == 'lote'){
            if(row.newLote == ''){
                alert("El lote no puede ser vacio")
            }else{
                produccion.cambiarLote((r, b) => {
                    b()
                    if(r.status == 200){
                        row.lote = row.newLote
                        $scope.check = {}
                        $scope.escape(row, campo)
                    }else{
                        alert('Hubo un problema favor intentelo mas tarde')
                    }
                }, { grupo_racimo : row.grupo_racimo, 
                    lote : row.newLote, 
                    id_finca : $scope.produccion.params.finca,
                    id_formulario : row.id_formulario
                })
            }
        }else if(campo == 'cuadrilla'){
            if(row.newCuadrilla == ''){
                alert("La cuadrilla no puede estar vacia")
            }else{
                produccion.cambiarCuadrilla((r, b) => {
                    b()
                    if(r.status == 200){
                        row.cuadrilla = row.newCuadrilla
                        $scope.check = {}
                        $scope.escape(row, campo)
                    }else{
                        alert('Hubo un problema favor intentelo mas tarde')
                    }
                }, { grupo_racimo : row.grupo_racimo, 
                    cuadrilla : row.newCuadrilla, 
                    id_finca : $scope.produccion.params.finca,
                    id_formulario : row.id_formulario 
                })
            }
        }
    }

    $scope.escape = (row, campo) => {
        if(campo == 'lote'){
            row.editingLote = false
        }
        else if(campo == 'cuadrilla'){
            row.editingCuadrilla = false
        }
    }

    getIndexRacimo = (cinta, status, datasource) => {
        var index = -1
        datasource.map((racimo, i) => {
            if(cinta == racimo.cinta && status == racimo.tipo) index = i
        })
        return index
    }

    $scope.ordenarRacimos = (viaje) => {
        viaje.formulario_ordenado = {}
        var racimos = {}
        var formulario_arr = []
        var formulario_obj = []

        if(viaje.formulario){
            viaje.formulario.map((racimo) => {
                formulario_arr.push(racimo.cinta)
                formulario_obj.push(racimo)
            })
        }
        if(viaje.detalle){
            viaje.detalle.map((racimo) => {
                racimos[racimo.num_racimo] = racimo
            })
        }

        var numbers = (viaje.blz > 20 || viaje.form > 20)
            ? getArray((viaje.blz > viaje.form ? viaje.blz : viaje.form))
            : getArray(20)

        numbers.map((i) => {
            if(racimos[i]){
                let color_blz = racimos[i].cinta
                let tipo_blz = racimos[i].tipo
                let index_form = getIndexRacimo(color_blz, tipo_blz, formulario_obj)
                if(index_form > -1){
                    viaje.formulario_ordenado[i] = formulario_obj[index_form]
                    formulario_arr.splice(index_form, 1)
                    formulario_obj.splice(index_form, 1)
                }
            }
        })
        numbers.map((i) => {
            if(formulario_obj.length > 0)
            if(!viaje.formulario_ordenado[i]){
                viaje.formulario_ordenado[i] = formulario_obj[0]
                viaje.formulario_ordenado[i].num_racimo = i
                formulario_obj.splice(0, 1)
            }
        })

        viaje.balanza = racimos

        numbers.map((i) => {
            if(!viaje.formulario_ordenado[i]){
                viaje.formulario_ordenado[i] = {
                    num_racimo : i
                }
            }
            if(!viaje.balanza[i]){
                viaje.balanza[i] = {
                    num_racimo : i
                }
            }
        })
        if(viaje.blz == 12) console.log(viaje)
    }

    $scope.editarViaje = (viaje, index) => {
        $("#viajes").modal("show")

        $scope.viajeSelected = angular.copy(viaje)
        $scope.viajeSelected.index = index
        $scope.viajeSelected.formulario = angular.copy(viaje.formulario_ordenado)

        if($scope.viajeSelected.blz > 20 || $scope.viajeSelected.form > 20){
            if($scope.viajeSelected.blz > $scope.viajeSelected.form){
                $scope.getNumber($scope.viajeSelected.blz)
            }
            else{
                $scope.getNumber($scope.viajeSelected.form)
            }
        }else{
            $scope.getNumber(20)
        }
    }

    $scope.crearViajeBalanza = (viaje, index) => {
        if(confirm("¿Estas seguro de crear un viaje a la balanza basado en este formulario?")){
            if(viaje.id_formulario > 0){
                produccion.crearViajeBalanza((r, b) => {
                    b()
                    //$scope.getViajes(angular.copy($scope.produccion.params))
                    $scope.getViajes()
                    alert("Se creo correctamente", '', 'success')
                }, {
                    id_formulario : viaje.id_formulario,
                    lote : viaje.lote
                })
            }else{
                alert("Los viajes solo se pueden crear apartir de un formulario")
            }
        }
    }

    $scope.check = {}
    $scope.select = {
        BLZ : true,
        FORM : true
    }
    $scope.selectCheck = (index, row) => {
        if($scope.check.hasOwnProperty(`${index}`)){
            if(!$scope.check[index]){
                delete $scope.check[index]
                let type = row.blz > 0 ? 'BLZ' : 'FORM'
                $scope.select[type] = true
                row.selected = false
            }
            else {
                let type = row.blz > 0 ? 'BLZ' : 'FORM'
                $scope.select[type] = false
                row.selected = true
            }
        }
        
        var keys = Object.keys($scope.check)
        if(keys.length == 2){
            if(confirm('¿Seguro que quieres unir estos dos viajes?')){
                var balanza, formulario
                if($scope.racimosViajes[keys[0]].blz > 0){
                    balanza = angular.copy($scope.racimosViajes[keys[0]])
                    formulario  = angular.copy($scope.racimosViajes[keys[1]])
                }else{
                    formulario = angular.copy($scope.racimosViajes[keys[0]])
                    balanza  = angular.copy($scope.racimosViajes[keys[1]])
                }

                if(balanza.lote == formulario.lote && balanza.cuadrilla == formulario.cuadrilla){
                    produccion.unirBalanzaFormulario((r, b) => {
                        b()
                        if(r.status == 200){
                            alert("Se unieron correctamente", "", "success")
                            $scope.select = { BLZ : true, FORM : true }
                            $scope.check = {}
                            $scope.getViajes()
                        }else{
                            alert("Ocurrio algun error, favor de intentar mas tarde")
                        }
                    }, 
                    { 
                        grupo_racimo : balanza.grupo_racimo, 
                        id_formulario : formulario.id_formulario, 
                        id_finca : $scope.produccion.params.finca ,
                        lote : balanza.lote
                    })
                }else{
                    alert("Para poder unir los viajes deben ser del mismo lote y cuadrilla")
                }
            }
        }else if(keys.length > 2){
            alert("Solo debes elegir 2 viajes", "", "warning")
        }
    }

    $scope.encontrarDiferencias = (balanza, formulario) => {
        if(balanza && formulario){
            if(!balanza.id){
                return true;
            }else{
                if(balanza.cinta != formulario.cinta || balanza.tipo != formulario.tipo){
                    return true;
                }
            }
        }
        return false
    }

    $scope.encontrarDiferenciasTodos = (viaje) => {
        var diferencia = false
        if(viaje){
            var numbers = (viaje.blz > 20 || viaje.form > 20)
                ? getArray((viaje.blz > viaje.form ? viaje.blz : viaje.form))
                : getArray(20)

            numbers.map((i) => {
                if(viaje.detalle[i-1] === undefined && viaje.formulario_ordenado[i] === undefined)
                if(viaje.detalle[i-1] === undefined || viaje.formulario_ordenado[i] === undefined)
                    diferencia = true
                if(viaje.detalle)
                if($scope.encontrarDiferencias(viaje.detalle[i-1], viaje.formulario_ordenado[i]))
                    diferencia = true
            })
        }
        return diferencia
    }

    $scope.guardarRacimosViajes = () => {
        $scope.viajeSelected.finca = angular.copy($scope.produccion.params.finca);
        $scope.viajeSelected.fecha = angular.copy($scope.produccion.params.fecha_inicial);

        produccion.guardarViaje((r, b) => {
            b()
            if(r){
                if(r.status == 200){
                    alert("Modificado con éxito", "", "success")

                    $("#viajes").modal("hide")
                    $scope.getViajes(angular.copy($scope.produccion.params))
                }else{
                    alert(r.message)
                }
            }else{
                alert("Ha ocurrido un error, Favor de intentar mas tarde")
            }
        }, $scope.viajeSelected)
    }

    $scope.procesarViaje = () => {
        produccion.procesarViaje((r, b) => {
            b()
            if(r.status == 200){
                alert('Se finalizo correctamente', '', 'success')
                $("#viajes").modal("hide")
                $scope.getViajes(angular.copy($scope.produccion.params))
            }else{
                alert('Hubo algun error, favor de intentar mas tarde')
            }
        }, {
            grupo_racimo : $scope.viajeSelected.grupo_racimo,
            finca : $scope.produccion.params.finca,
            lote : $scope.viajeSelected.lote
        })
    }

    $scope.desprocesarViaje = () => {
        produccion.desprocesarViaje((r, b) => {
            b()
            if(r.status == 200){
                alert('Se hailito correctamente', '', 'success')
                $("#viajes").modal("hide")
                $scope.getViajes(angular.copy($scope.produccion.params))
            }else{
                alert('Hubo algun error, favor de intentar mas tarde')
            }
        }, {
            grupo_racimo : $scope.viajeSelected.grupo_racimo,
            finca : $scope.produccion.params.finca,
            lote : $scope.viajeSelected.lote
        })
    }

    $scope.printDetails = function(r,b){
        b();
        if(r){
            $scope.subTittle = r.date_select || $scope.subTittle
            $scope.produccion.params.initial = 1;
            //$scope.tabla.produccion = r.data;
            $scope.tabla.edades = r.data_edad;
            $scope.id_company = r.id_company;
            $scope.colores = r.colores;
            $scope.colores = r.colores;
            //$scope.palancas = r.palancas;
            $scope.totales = r.totales;
            if($scope.start){
                $scope.start = false;
                /*setInterval(function(){
                    $scope.init();
                } , (60000 * 5));*/
            }
        }
    }

    $scope.next = function(dataSource){
        if($scope.searchTable.actual_page < parseInt(dataSource.length / parseInt($scope.searchTable.limit)) + (dataSource.length % parseInt($scope.searchTable.limit) == 0 ? 0 : 1)) 
            $scope.searchTable.actual_page++;
        
        $scope.searchTable.startFrom = ($scope.searchTable.actual_page - 1) * (parseInt($scope.searchTable.limit))
    }

    $scope.prev = function(dataSource){
        if($scope.searchTable.actual_page > 1)
            $scope.searchTable.actual_page--;
        
        $scope.searchTable.startFrom = ($scope.searchTable.actual_page - 1) * (parseInt($scope.searchTable.limit))
    }

    $scope.setOrderTable = function(field){
        var reverse = false
        if(field == $scope.searchTable.orderBy){
            reverse = !$scope.searchTable.reverse
        }else{
            $scope.searchTable.orderBy = field
        }
        $scope.searchTable.reverse = reverse
    }

    $scope.searchTable = {
        orderBy : "id",
        reverse : false,
        limit : 10,
        actual_page : 1,
        startFrom : 0,
        optionsPagination : [
            10, 50, 100
        ],
        changePagination : function(){
            if($scope.searchTable.limit == 0)
                $scope.searchTable.limit = $scope.registros.length

            $scope.searchTable.numPages = parseInt($scope.registros.length / $scope.searchTable.limit)  + ($scope.registros.length % $scope.searchTable.limit == 0 ? 0 : 1)
            $scope.searchTable.actual_page = 1
            $scope.searchTable.startFrom = 0
        }
    }

    $scope.guardarExcel = () => {
        produccion.guardarExcel((r) => {
            alert("Se importo la información correctamente", "", "success", () => {
                location.reload()
            })
        }, { file : $scope.file })
    }

    $scope.importar = () => {
        $("#importar").modal("show")
    }

    $scope.exportExcel = function(id_table, title){
        var tableToExcel = (function() {
            var uri = 'data:application/vnd.ms-excel;base64,'
                , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body><table>{table}</table></body></html>'
                , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
                , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
            return function(table, name) {
                if (!table.nodeType) table = document.getElementById(table)
                var contentTable = table.innerHTML
                var ctx = {worksheet: name || 'Worksheet', table: contentTable}
                window.location.href = uri + base64(format(template, ctx))
            }
        })()
        tableToExcel(id_table, title)
    }

    $scope.exportPrint = function(id_table){
        $("#"+id_table).css("display", "")
        let image = '<div style="display:block; height:55;"><img style="float: right;" width="100" height="50" src="./../logos/Logo.png" /></div><br>';
        let table = document.getElementById(id_table);
        var contentTable = table.outerHTML;
        newWin = window.open("");
        newWin.document.write(contentTable);
        newWin.print();
        newWin.close();
        $("#"+id_table).css("display", "none")
    }

    $scope.exportarFormatoEspecial = () => {
        dayOfWeek = (day) => { return ["DOMINGO", "LUNES", "MARTES", "MIERCOLES", "JUEVES", "VIERNES", "SABADO"][day]  };
        embedInRow = (cols) => { return `<tr>${cols}</tr>` };
        generateRow = () => { var cols = []; for(var i = 0; i < 10; i ++) { cols.push("<td></td>") } return cols; };
        getCintaValida = (cinta) => { return { VERDE : 'VERDE', ROJA : 'ROJA', AMARILLO : 'AMARI', CAFE  : 'CAFE', AZUL : 'AZUL', BLANCO : 'BLANC', NEGRO : 'NEGRA', LILA : 'LILA' }[cinta]; }
        var table = []
        
        row = generateRow();
        row[0] = `<td>Archivo no.: ${moment($scope.produccion.params.fecha_inicial).format('DDD')}</td>`;
        table.push("<tr>" + row.join("") + "</tr>")

        row = generateRow();
        row[0] = `<td>Nombre: SEMANA ${moment($scope.produccion.params.fecha_inicial).format('WW')} ${dayOfWeek(moment($scope.produccion.params.fecha_inicial).format('d'))} ${moment($scope.produccion.params.fecha_inicial).format('DD')}</td>`;
        table.push("<tr>" + row.join("") + "</tr>")

        row = generateRow();
        row[0] = `<td>Fecha: ${moment($scope.produccion.params.fecha_inicial).format('DD-MM-YYYY')}</td>`;
        table.push("<tr>" + row.join("") + "</tr>")

        table.push([
            "<tr>",
            "<td>F01IDV(4)isID</td>",
            "<td>DW2Peso()</td>",
            "<td>C03LOTE()</td>",
            "<td>C13CINTA()</td>",
            "<td>F12MANOS(##)notID</td>",
            "<td>F22CALIBRE(##.##)notID</td>",
            "<td>F32L DEDOS(##.##)notID</td>",
            "<td>F43TIPO()notID</td>",
            "<td>F53NIVEL()notID</td>",
            "<td>F63CAUSA()notID</td>",
            "</tr>"
        ].join(""))

        table.push([
            "<tr>",
            "<td>IDV</td>",
            "<td>Peso</td>",
            "<td>LOTE</td>",
            "<td>CINTA</td>",
            "<td>MANOS</td>",
            "<td>CALIBRE</td>",
            "<td>L DEDOS</td>",
            "<td>TIPO</td>",
            "<td>NIVEL</td>",
            "<td>CAUSA</td>",
            "</tr>"
        ].join(""))

        $scope.registros.map((value, index) => {
            if(parseInt(value.id) > 0){
                table.push([
                    "<tr>",
                    `<td>${value.id}</td>`,
                    `<td>${value.peso}</td>`,
                    `<td>${value.lote}</td>`,
                    `<td>${getCintaValida(value.cinta)}</td>`,
                    `<td>${(value.manos > 0) ? value.manos : ''}</td>`,
                    `<td>${(value.calibre > 0) ? value.calibre : ''}</td>`,
                    `<td>${(value.dedos > 0) ? value.dedos : ''}</td>`,
                    `<td></td>`,
                    `<td>${(value.tipo == 'RECUSADO') ? 'RECHA' : ''}</td>`,
                    `<td>${(value.tipo == 'RECUSADO') ? value.causa : ''}</td>`,
                    "</tr>"
                ].join(""))
            }
        })

        // Añadie cuadre
        if($scope.dia_cuadrado) {
            $scope.cuadre_proc.map((value, index) => {
                var diferencia = value.form - value.blz 
                for(var i = 0; i < diferencia; i++){
                    table.push([
                        "<tr>",
                        `<td></td>`,
                        `<td>${value.peso_prom}</td>`,
                        `<td>${value.lote}</td>`,
                        `<td>${getCintaValida(value.cinta)}</td>`,
                        `<td></td>`,
                        `<td></td>`,
                        `<td></td>`,
                        `<td></td>`,
                        `<td></td>`,
                        `<td></td>`,
                        "</tr>"
                    ].join(""))
                }
            })

            $scope.faltante_recu.map((value, index) => {
                var diferencia = value.cantidad - value.cantidad_blz 
                for(var i = 0; i < diferencia; i++){
                    table.push([
                        "<tr>",
                        `<td></td>`,
                        `<td>${value.peso_prom}</td>`,
                        `<td>${value.lote}</td>`,
                        `<td>${getCintaValida(value.color_cinta)}</td>`,
                        `<td></td>`,
                        `<td></td>`,
                        `<td></td>`,
                        `<td></td>`,
                        `<td>RECHA</td>`,
                        `<td>${value.causa}</td>`,
                        "</tr>"
                    ].join(""))
                }
            })
        }
        
        var tableToExcel = (function() {
            var uri = 'data:application/vnd.ms-excel;base64,'
                , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body><table>{table}</table></body></html>'
                , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
                , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
            return function() {
                var contentTable = table.join("")
                var ctx = {worksheet: name || 'Worksheet', table: contentTable}
                window.location.href = uri + base64(format(template, ctx))
            }
        })()
        tableToExcel()
    }

    $scope.width = function(){
        return $('#fila1').width() + 8 +1
    }

    $('#input-file').bind('change', function() {
        $scope.isSet = true
    });

    $scope.getAnalisisRecusados = () => {
        produccion.analisisRecusados((r, b) => {
            b()
            r.data.map((value) => {
                if(value.dano == 'TOTAL') $scope.umbralRecusados = value.prom
            })
            printTableRecusados(r.data, r.semanas)
        }, $scope.produccion.params)
    }

    var printTableRecusados = (data, semanas) => {
        let id = 'table-defectos'
        var props = {
            header : [{
                   key : 'dano',
                   name : 'DEFECTO',
                   titleClass : 'text-center',
                   locked : true,
                   expandable : true,
                   resizable : true,
                   width: 170
                },{
                    key : 'sum',
                    name : 'SUM',
                    locked : true,
                    sortable : true,
                    titleClass : 'text-center',
                    alignContent : 'right',
                    filterable : true,
                    filterRenderer: 'NumericFilter',
                    formatter : 'Number',
                    resizable : true,
                    customCell : function(rowData, isChildren){
                        var valNumber = parseFloat(rowData['sum'])
                        if($scope.produccion.params.var_recusado == 'porc') valNumber = 0
                        let valueCell = valNumber > 0 ? number_format(valNumber, ($scope.produccion.params.var_recusado == 'cant') ? 0 : 2) : ''
                        return `
                             <div class="text-center ${checkUmbral(valNumber, $scope.umbralRecusados)}" style="height: 100%">
                                 ${valueCell}
                             </div>
                        `;
                     }
                 },{
                   key : 'prom',
                   name : 'AVG',
                   locked : true,
                   sortable : true,
                   titleClass : 'text-center',
                   alignContent : 'right',
                   filterable : true,
                   filterRenderer: 'NumericFilter',
                   formatter : 'Number',
                   resizable : true,
                   customCell : function(rowData, isChildren){
                        let valNumber = parseFloat(rowData['prom'])
                        let valueCell = valNumber > 0 ? number_format(valNumber, ($scope.produccion.params.var_recusado == 'cant') ? 0 : 2) : ''
                        return `
                            <div class="text-center ${checkUmbral(valNumber, $scope.umbralRecusados)}" style="height: 100%">
                                ${valueCell}
                            </div>
                        `;
                    }
                },{
                   key : 'max',
                   name : 'MAX',
                   locked : true,
                   titleClass : 'text-center',
                   sortable : true,
                   alignContent : 'right',
                   filterable : true,
                   filterRenderer: 'NumericFilter',
                   resizable : true,
                   customCell : function(rowData, isChildren){
                        let valNumber = parseFloat(rowData['max'])
                        let valueCell = valNumber > 0 ? number_format(valNumber, ($scope.produccion.params.var_recusado == 'cant') ? 0 : 2) : ''
                        return `
                            <div class="text-center ${checkUmbral(valNumber, $scope.umbralRecusados)}" style="height: 100%">
                                ${valueCell}
                            </div>
                        `;
                    }
                },{
                   key : 'min',
                   name : 'MIN',
                   locked : true,
                   sortable : true,
                   titleClass : 'text-center',
                   alignContent : 'right',
                   filterable : true,
                   filterRenderer: 'NumericFilter',
                   resizable : true,
                   customCell : function(rowData, isChildren){
                        let valNumber = parseFloat(rowData['min'])
                        let valueCell = valNumber > 0 ? number_format(valNumber, ($scope.produccion.params.var_recusado == 'cant') ? 0 : 2) : ''
                        return `
                            <div class="text-center ${checkUmbral(valNumber, $scope.umbralRecusados)}" style="height: 100%">
                                ${valueCell}
                            </div>
                        `;
                    }
                }
            ],
            data : data,
            buttons : [
                {
                    title : 'Excel',
                    action : () => {
                        $scope.table1.exportToExcel()
                    },
                    className : ''
                }
            ]
        }
        semanas.map((value) => {
            props.header.push({
                key : `sem_${value}`,
                name : `${value}`,
                sortable : true,
                alignContent : 'right',
                titleClass : 'text-center',
                filterable : true,
                filterRenderer: 'NumericFilter',
                resizable : true,
                customCell : function(rowData, isChildren){
                    let valNumber = parseFloat(rowData['sem_'+value])
                    let valueCell = valNumber > 0 ? number_format(valNumber, ($scope.produccion.params.var_recusado == 'cant') ? 0 : 2) : ''
                    return `
                        <div class="text-center ${checkUmbral(valNumber, $scope.umbralRecusados)}" style="height: 100%">
                            ${valueCell}
                        </div>
                    `;
                }
            })
        })
        document.getElementById(id).innerHTML = ""
        $scope.table1 = ReactDOM.render(React.createElement(ReactDataGrid, props), document.getElementById(id))
    }

}]);

