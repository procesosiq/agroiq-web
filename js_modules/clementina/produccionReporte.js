app.service('request', ['$http', function($http){
    
    this.data = function(callback, params){
        let url = 'phrapi/sumifru/produccionreporte/data'
        load.block()
        $http.post(url, params || {}).then(r => {
            load.unblock()
            callback(r.data)
        })
    }

    this.getWeeks = function(callback, params){
        let url = 'phrapi/sumifru/produccionreporte/weeks'
        $http.post(url, params || {}).then(r => {
            callback(r.data)
        })
    }

    this.fincas = function(callback, params){
        let url = 'phrapi/sumifru/produccionreporte/fincas'
        $http.post(url, params || {}).then(r => {
            callback(r.data)
        })
    }

    this.last = function(callback, params){
        load.block()
        let url = 'phrapi/sumifru/produccionreporte/last'
        $http.post(url, params || {}).then(r => {
            load.unblock()
            callback(r.data)
        })
    }
}])

app.filter('avgOfValue', function () {
    return function (data, key) {        
        if (angular.isUndefined(data) || angular.isUndefined(key))
            return 0;        
        var sum = 0;
        var count = 0;
        angular.forEach(data,function(value){
            if(value[key] != "" && value[key] != undefined){
                sum = sum + parseFloat(value[key], 10);
                count++;
            }
        });
        sum = sum / count;
        if(isNaN(sum))
            return 0;
        return sum;
    }
})

app.controller('controller', ['$scope','request', function($scope, $request){

    $scope.filters = {}
    $scope.changeRangeDate = (data) => {
        if(data){
            $scope.year = ''
            $scope.semana = ''
            $scope.filters.fecha_inicial = data.hasOwnProperty("first_date") ? data.first_date : $scope.filters.fecha_inicial;
            $scope.filters.fecha_final = data.hasOwnProperty("second_date") ? data.second_date : $scope.filters.fecha_final;
            getData()
        }
    }

    $scope.changeYear = () => {
        if($scope.filters.year != ''){
            $request.getWeeks((r) => {
                $scope.semanas = r.semanas
                if(r.semanas.indexOf($scope.filters.semana) == -1){
                    $scope.filters.semana = ''
                }
            }, {
                year: $scope.filters.year
            })
        }
    }
    $scope.changeWeek = () => {
        if($scope.filters.semana != '' && $scope.filters.year != ''){
            $scope.filters.fecha_inicial = ''
            $scope.filters.fecha_final = ''
            getData()
        }
    }

    getData = () => {
        $request.fincas((f) => {
            if(f.fincas.length > 0){
                if(Object.keys(f.fincas).indexOf($scope.filters.id_finca) == -1){
                    $scope.filters.id_finca = Object.keys(f.fincas)[0]
                }
            }
            $scope.fincas = f.fincas

            $request.data((r) => {
                $scope.cintaSC = r.cintaSC || false
                $scope.cintas = r.cintas
                $scope.data = r.data
                $scope.totales = r.totales
                $scope.edades = r.edades
            }, $scope.filters)
        }, $scope.filters)
    }

    $scope.fnExcelReport = function(id_table, title){
        var data = new Blob([document.getElementById(id_table).outerHTML], {
            type : 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf8'
        })
        saveAs(data, title+'.xls')
    }

    $request.last((r) => {
        $scope.filters = {
            fecha_inicial : r.fecha_inicial,
            fecha_final : r.fecha_final,
            year : '',
            semana : '',
            sector : ''
        }
        $scope.years = r.years
        $scope.semanas = r.weeks
        $scope.filters.year = r.last_year
        $scope.filters.semana = r.last_week
        getData()
    })
    
}]);