const { app } = window

app.service('request', ['$http', function($http){
    this.list = (callback, params) => {
        $http.post('phrapi/lancofruit/categoriasConfig/index', params || {})
        .then((r) => {
            callback(r.data)
        })
    }

    this.saveCategoria = (callback, params) => {
        $http.post('phrapi/lancofruit/categoriasConfig/save', params || {})
        .then((r) => {
            callback(r.data)
        })
    }

    this.borrarCategoria = (callback, params) => {
        $http.post('phrapi/lancofruit/categoriasConfig/borrar', params || {})
        .then((r) => {
            callback(r.data)
        })
    }

    this.toggleEstatusCategoria = (callback, params) => {
        $http.post('phrapi/lancofruit/categoriasConfig/toggleStatus', params || {})
        .then((r) => {
            callback(r.data)
        })
    }

    this.saveSubcategoria = (callback, params) => {
        $http.post('phrapi/lancofruit/subcategoriasConfig/save', params || {})
        .then((r) => {
            callback(r.data)
        })
    }

    this.borrarSubcategoria = (callback, params) => {
        $http.post('phrapi/lancofruit/subcategoriasConfig/borrar', params || {})
        .then((r) => {
            callback(r.data)
        })
    }

    this.toggleEstatusSubcategoria = (callback, params) => {
        $http.post('phrapi/lancofruit/subcategoriasConfig/toggleStatus', params || {})
        .then((r) => {
            callback(r.data)
        })
    }
}])

app.controller('lancofruit', ['$scope', 'request', function($scope, $request){

    /* VARIABLES  */

    var modalCategoria = $("#modal_categoria"),
        modalSubcategoria = $("#modal_subcategoria")
    $scope.filters = {}
    $scope.colorStatus = {
        'Activo' : 'bg-green-haze bg-font-green-haze',
        'Inactivo' : 'bg-red bg-font-red'
    }
    $scope.categorias = []
    $scope.subcategorias = []
    $scope.subcategorias_disponibles = []

    $scope.newCategoria = {
        id : null,
        nombre : '',
        subcategorias : [],
        save : () => {
            return new Promise((resolve, reject) => {
                let subcategorias = $('#select_multiple').val() || []

                $request.saveCategoria((r) => {
                    resolve(r)
                }, {
                    id : $scope.newCategoria.id,
                    nombre : $scope.newCategoria.nombre,
                    subcategorias : subcategorias
                })
            })
        }
    }

    $scope.newSubcategoria = {
        id : null,
        nombre : '',
        id_categoria : [],
        save : () => {
            return new Promise((resolve, reject) => {
                $request.saveSubcategoria((r) => {
                    resolve(r)
                }, {
                    id : $scope.newSubcategoria.id,
                    nombre : $scope.newSubcategoria.nombre,
                    id_categoria : $scope.newSubcategoria.id_categoria
                })
            })
        }
    }

    $scope.init = ( ) => {
        initMultiSelectCategorias()

        $request.list((r) => {
            $scope.categorias = r.categorias
            $scope.subcategorias = r.subcategorias
        }, $scope.filters)
    }

    /* CATEGORIAS */

    $scope.nuevaCategoria = () => {
        modalCategoria.modal('show')
        $scope.newCategoria.id = null
        $scope.newCategoria.nombre = ""
        $scope.newCategoria.subcategorias = []
        $scope.subcategorias_disponibles = $scope.subcategorias.filter((s) => s.id_categoria === null || s.id_categoria === 0)

        setTimeout(() => $('#select_multiple').multiSelect('refresh'), 250)
    }

    $scope.modificarCategoria = (categoria) => {
        modalCategoria.modal('show')
        $scope.newCategoria.id = angular.copy(categoria.id)
        $scope.newCategoria.nombre = angular.copy(categoria.nombre)
        $scope.newCategoria.subcategorias = $scope.subcategorias.filter((s) => s.id_categoria == categoria.id)
        $scope.subcategorias_disponibles = $scope.subcategorias.filter((s) => s.id_categoria === null || s.id_categoria === 0)

        setTimeout(() => $('#select_multiple').multiSelect('refresh'), 250)
    }

    $scope.toggleEstatusCategoria = (categoria) => {
        $request.toggleEstatusCategoria((r) => {
            if(r.status === 200){
                //alert("Se guardo correctamente", "Configuración", "success")
                categoria.status = categoria.status === 'Activo' ? 'Inactivo' : 'Activo'
            }else{
                alert("Ocurrio algo inesperado")
            }
        }, {
            id : categoria.id
        })
    }

    $scope.borrarCategoria = (categoria, index) => {
        $request.borrarCategoria((r) => {
            if(r.status === 200){
                alert("Se guardo correctamente", "Configuración", "success")
                $scope.categorias.splice(index, 1)
            }else{
                alert("Ocurrio algo inesperado")
            }
        }, {
            id : categoria.id
        })
    }

    $scope.guardarCategoria = () => {
        $scope.newCategoria.save()
        .then((r) => {
            if(r.status === 200){
                if($scope.newCategoria.id === null){
                    $scope.categorias.push({
                        id : r.id,
                        nombre : r.nombre,
                        status : 'Activo'
                    })
                }else{
                    modificarCategoria($scope.newCategoria.id, r.nombre, r.subcategorias)
                }

                $scope.$apply()
                cerrarModalCategoria()
                alert("Se guardo correctamente", "Configuración", "success")
            }
            else if(r.message){
                alert(r.message)
            }
            else{
                alert("Ocurrio algo inesperado")
            }
        })
    }

    const modificarCategoria = (id, nombre, subcategorias) => {
        $scope.categorias.map((c) => {
            if(c.id === $scope.newCategoria.id) {
                c.nombre = nombre
            }
        })

        $scope.subcategorias.map((s) => {
            if(subcategorias.indexOf(s.id) > -1){
                s.id_categoria = id
                s.categoria = nombre
            }
            else if(s.id_categoria === id){
                s.id_categoria = null
                s.categoria = ''
            }
        })
    }

    const cerrarModalCategoria = () => {
        modalCategoria.modal('hide')
        $scope.newCategoria.id = null
        $scope.newCategoria.nombre = ""
        $scope.newCategoria.subcategorias = []
    }

    const initMultiSelectCategorias = () => {
        $('#select_multiple').multiSelect({
            selectableHeader: "<div class='custom-header'> Subcategorías Disponibles </div>",
            selectionHeader: "<div class='custom-header'> Subcategorías Asignadas </div>",
        });
    }

    /* SUBCATEGORIAS */

    $scope.nuevaSubcategoria = () => {
        modalSubcategoria.modal('show')
        $scope.newSubcategoria.id = null
        $scope.newSubcategoria.nombre = ""
        $scope.newSubcategoria.id_categoria = null
    }

    $scope.modificarSubcategoria = (categoria) => {
        modalSubcategoria.modal('show')
        $scope.newSubcategoria.id = angular.copy(categoria.id)
        $scope.newSubcategoria.nombre = angular.copy(categoria.nombre)
        $scope.newSubcategoria.id_categoria = angular.copy(categoria.id_categoria)
    }

    $scope.guardarSubcategoria = () => {
        $scope.newSubcategoria.save()
        .then((r) => {
            if(r.status === 200){
                if($scope.newSubcategoria.id === null){
                    $scope.subcategorias.push({
                        id : r.id,
                        nombre : r.nombre,
                        id_categoria : r.id_categoria,
                        categoria : r.categoria,
                        status : 'Activo'
                    })
                }else{
                    modificarSubcategoria($scope.newSubcategoria.id, r.nombre, r.id_categoria, r.categoria)
                }

                $scope.$apply()
                cerrarModalSubcategoria()
                alert("Se guardo correctamente", "Configuración", "success")
            }
            else if(r.message){
                alert(r.message)
            }
            else{
                alert("Ocurrio algo inesperado")
            }
        })
    }

    $scope.toggleEstatusSubcategoria = (categoria) => {
        $request.toggleEstatusSubcategoria((r) => {
            if(r.status === 200){
                //alert("Se guardo correctamente", "Configuración", "success")
                categoria.status = categoria.status === 'Activo' ? 'Inactivo' : 'Activo'
            }else{
                alert("Ocurrio algo inesperado")
            }
        }, {
            id : categoria.id
        })
    }

    $scope.borrarSubcategoria = (categoria, index) => {
        $request.borrarSubcategoria((r) => {
            if(r.status === 200){
                alert("Se guardo correctamente", "Configuración", "success")
                $scope.subcategorias.splice(index, 1)
            }else{
                alert("Ocurrio algo inesperado")
            }
        }, {
            id : categoria.id
        })
    }

    const modificarSubcategoria = (id, nombre, id_categoria, categoria) => {
        $scope.subcategorias.map((s) => {
            if(s.id === id){
                s.nombre = nombre
                s.id_categoria = id_categoria
                s.categoria = categoria
            }
        })
    }

    const cerrarModalSubcategoria = () => {
        modalSubcategoria.modal('hide')
        $scope.newSubcategoria.id = null
        $scope.newSubcategoria.nombre = ""
        $scope.newSubcategoria.id_categoria = null
    }

    /* INIT */
    $scope.init()
    
}])