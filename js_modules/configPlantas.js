app.service('request', ['$http', ($http) => {
	var service = {}

	service.last = (params) => {
		return new Promise((resolve, reject) => {
			$http.post('phrapi/config/plantas/last', params || {})
			.then((r) => {
				resolve(r.data)
			})
			.catch((r) => {
				reject()
			})
		})
	}

	service.index = (params) => {
		return new Promise((resolve, reject) => {
			$http.post('phrapi/config/plantas/index', params || {})
			.then((r) => {
				resolve(r.data)
			})
			.catch((r) => {
				reject()
			})
		})
	}

	service.save = (params) => {
		return new Promise((resolve, reject) => {
			$http.post('phrapi/config/plantas/saveha', params || {})
			.then((r) => {
				resolve(r.data)
			})
			.catch((r) => {
				reject()
			})
		})
	}

	return service
}])

app.controller('controller', ['$scope', 'request', ($scope, $request) => {

	const save = (id, campo, valor) => {
        if(id > 0){
            $request.save({
				id_lote : id, 
				semana : campo.replace('sem_', ''), 
				hectareas : valor,
				anio : $scope.filters.anio,
				id_finca : $scope.filters.id_finca
            }).then((r) => {
                if(r.status == 200){
					alert("Se guardo correctamente", "", "success")
					$scope.init()
                }else{
                    alert("Hubo un error favor de intentar mas tarde")
                }
            })
        }
    }

	$scope.filters = {
		anio : moment().year(),
		id_finca : 0
	}

	$scope.last = () => {
		load.block()
		$request.last()
		.then((r) => {
			load.unblock()
			$scope.fincas = r.fincas
			$scope.anios = r.anios
			$scope.filters.anio = r.anio
			$scope.filters.id_finca = r.fincas[0].id

			$scope.init()
		})
	}

	$scope.init = () => {
		load.block()
		$request.index($scope.filters).then((r) => {
			load.unblock()
			renderTable(r.data, r.semanas)
		})
	}

	const renderTable = (data, semanas) => {
		let props = {
            header : [
				{
                    key : 'lote',
                    name : 'LOTE',
                    titleClass : 'text-center',
                    sortable : true,
                    alignContent : 'center',
                    filterable : true,
					resizable : true,
				},
            ],
            data : data,
            buttons : [
                {
                    title : 'Excel',
                    action : () => {
                        $scope.table1.exportToExcel()
                    },
                    className : ''
                }
            ]
		}
		
		for(let i in semanas){
			if(typeof semanas[i] !== 'function'){
				let sem = semanas[i]
				props.header.push({
					key : `sem_${sem}`,
					name : sem,
					titleClass : 'text-center',
					sortable : true,
					alignContent : 'center',
					filterable : true,
					resizable : true,
					editable : true,
					events: {
						onKeyDown: function(ev, column) {
							if (ev.key === 'Enter') {
								let index = parseInt(column.rowIdx)
								let key = column.column.key
								let row = $scope.table1.rowGetter(index)
								save(row.id_lote, key, row[key].toString().toUpperCase())
							}
						},
					},
					customCell : (data) => {
						return `
							<div style="height: 100%;" class="${data[`class_sem_${sem}`]}">
								${data[`sem_${sem}`]}
							</div>
						`
					}
				})
			}
		}
        $("#table-react").html("")
        $scope.table1 = ReactDOM.render(React.createElement(ReactDataGrid, props), document.getElementById('table-react'))
	}

	$scope.last()
}])