const { app } = window

app.directive('ngCalendarapp', [function () {

    function Library(callback){
        // var head = document.getElementsByTagName('head')[0];

        // var link = document.createElement( "link" );
        // link.rel = "stylesheet";
        // link.type = "text/css";
        // link.media = "all";
        // link.href = "assets/css/daterangepicker.css";
        // head.appendChild(link);

        // var script = document.createElement( "script" );
        // script.type = "text/javascript";
        // script.src = "assets/js/moment.js";
        // script.onload = function() {
        //     initMoment();
        //     var script = "";
        //     script = document.createElement( "script" );
        //     script.type = "text/javascript";
        //     script.onload = function() {
        //         initDateRangePicker();
        //         callback();
        //     }
        //     script.src = "assets/js/daterangepicker.js";
        //     head.appendChild(script);
        // }
        // head.appendChild(script);
        callback();
    }; 

    var html = [];
    html.push('<div id="dashboard-report-range" class="pull-right tooltips btn btn-fit-height green" data-placement="top" data-original-title="Change dashboard date range">');
    html.push('<i class="icon-calendar"></i>&nbsp;');
    html.push('<span id="date-picker" class="thin uppercase hidden-xs">July 11, 2016 - July 21, 2016</span>&nbsp;');
    html.push('<i class="fa fa-angle-down"></i>');
    html.push('</div>');

    // html.push('<div class="input-group demo">');
    // html.push('<input type="text" name="first_date" value="" class="form-control" id="first_date" data-validate="required">');
    // html.push('<span class="input-group-addon "> <i class="fa fa-calendar"></i> </span>');
    // html.push('</div>');
    return {
        scope : {
            search : "="
        },
        restrict: 'E',
        template: html.join(" "),
        controller: ['$scope', '$http', function($scope, $http) {
            $scope.search = {
                first_date :  moment().subtract(1, 'month').startOf('month'),
                second_date : moment().subtract(1, 'month').endOf('month') ,
                range : "Último mes",
                type_mov : "sql",
                date_select : ""
            }

            $scope.customDate = (date) => {
                if($scope.$parent.dateRangeDateCustomDate){
                    return $scope.$parent.dateRangeDateCustomDate(date)
                }
                return null
            }

            $scope.updateDate = function(){
                $scope.date_select = 'Fecha del ' + $scope.search.first_date.format('YYYY-MM-DD') + ' al ' + $scope.search.second_date.format('YYYY-MM-DD') + ' (rango definido: ' + $scope.search.range + ')';
            }

            $scope.resetRange = function(){
                setTimeout(function () {
                    $scope.$apply(function () {
                        $scope.search.first_date = moment().subtract(1, 'month').startOf('month').format('YYYY-MM-DD');
                        $scope.search.second_date = moment().subtract(1, 'month').endOf('month').format('YYYY-MM-DD');
                        $scope.search.range = "Último mes";
                        $scope.search.date_select = 'Fecha Seleccionadas : ' + moment().subtract(1, 'month').startOf('month').format('YYYY-MM-DD') + ' al ' + moment().subtract(1, 'month').endOf('month').format('YYYY-MM-DD') + ' (rango definido: ' + $scope.search.range + ')';
                        $('#dashboard-report-range span').html(moment().subtract(1, 'month').startOf('month').format('YYYY-MM-DD') + ' - ' + moment().subtract(1, 'month').endOf('month').format('YYYY-MM-DD'));
                    });
                }, 100);
            }

            $scope.getOptionRange = function(){
                var options = {
                    'Hoy': [moment(), moment()],
                    'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Últimos 7 Días': [moment().subtract(6, 'days'), moment()],
                    'Últimos 30 Días': [moment().subtract(29, 'days'), moment()],
                    'Últimos 90 Días': [moment().subtract(89, 'days'), moment()],
                    'Último Año' : [moment().subtract(1, 'year').startOf('year') , moment().subtract(1, 'year').endOf('year')],
                    'Este Año' : [moment().startOf('year') , moment().endOf("year")],
                    'Este mes': [moment().startOf('month'), moment().endOf('month')],
                    'Último mes': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
                if($scope.$parent.hasOwnProperty("rangesDirectives")){
                    options = $scope.$parent.rangesDirectives;
                }

                return options;
            }

            $scope.getSingleDatePicker = function(){
                var singleDatePicker = false;
                if($scope.$parent.hasOwnProperty("singleDatePickerDirective")){
                    singleDatePicker = true;
                }                

                return singleDatePicker;
            }

            $scope.getdateLimit = function(){
                var limit = {
                    days : 366
                };

                if($scope.$parent.hasOwnProperty("limitDirectives")){
                    limit = $scope.$parent.limitDirectives;
                }

                return limit;
            }

            $scope.getStartEndDate = function(){
                var startEndDate = {
                    startDate : moment().subtract(1, 'month').startOf('month'),
                    endDate :moment().subtract(1, 'month').endOf('month'),
                }

                if($scope.$parent.hasOwnProperty("StartEndDateDirectives")){
                    startEndDate = $scope.$parent.StartEndDateDirectives;
                }

                return startEndDate;
            }

            $scope.startRange = function(){
                setTimeout(function () {
                    $scope.$apply(function () {
                        $scope.search.first_date = $scope.getStartEndDate().startDate.format('YYYY-MM-DD');
                        $scope.search.second_date = $scope.getStartEndDate().endDate.format('YYYY-MM-DD');
                        // $scope.search.range = "Último mes";
                        // $scope.search.date_select = 'Fecha Seleccionadas : ' + moment().subtract(1, 'month').startOf('month').format('YYYY-MM-DD') + ' al ' + moment().subtract(1, 'month').endOf('month').format('YYYY-MM-DD') + ' (rango definido: ' + $scope.search.range + ')';
                        if($scope.getSingleDatePicker())
                            $('#dashboard-report-range span').html($scope.search.first_date)
                        else
                            $('#dashboard-report-range span').html($scope.search.first_date + ' - ' + $scope.search.second_date);
                    });
                }, 100);
            }

            Library(function(){
                console.log('rerender')
                $scope.reRender = () => {
                    var options = {
                        autoApply : true,
                        singleDatePicker : $scope.getSingleDatePicker(),
                        opens: "left",
                        linkedCalendars: false,
                        startDate: $scope.getStartEndDate().startDate,
                        endDate : $scope.getStartEndDate().endDate,
                        languague : 'es',
                        locale : {
                            postformat: 'MM/DD/YYYY',
                            separator: ' - ',
                            applyLabel: 'Aplicar',
                            cancelLabel: 'Cancelar',
                            fromLabel: 'From',
                            toLabel: 'To',
                            customRangeLabel: 'Definir fechas',
                            daysOfWeek: ['Dom', 'Lun', 'Mar', 'Mier', 'Jue', 'Vie','Sab'],
                            monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Juni', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                            firstDay: 1
                        },
                        ranges : $scope.getOptionRange(),
                        dateLimit : $scope.getdateLimit(),
                        applyClass: "btn-info",
                        isInvalidDate: function (date) {
                            if($scope.$parent.datesEnabled){
                                let fecha = moment(date).format('YYYY-MM-DD')
                                let has = $scope.$parent.datesEnabled.indexOf(fecha) > -1
                                return !has
                            }else{
                                return false
                            }
                        },
                        isCustomDate : $scope.customDate
                    };

                    // $(iElement[0].childNodes[0].childNodes[1]).daterangepicker(options, function(start, end, label) {
                    $('#dashboard-report-range').daterangepicker(options, function(start, end, label) {
                        $('#dashboard-report-range span').html(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
                        setTimeout(function () {
                            $scope.$apply(function () {
                                $scope.search.first_date = start.format('YYYY-MM-DD');
                                $scope.search.second_date = end.format('YYYY-MM-DD');
                                $scope.search.range = label;
                                $scope.search.date_select = 'Fecha Seleccionadas : ' + start.format('YYYY-MM-DD') + ' al ' + end.format('YYYY-MM-DD') + ' (rango definido: ' + label + ')';
                                $scope.$parent.changeRangeDate($scope.search);
                            });
                        }, 100);
                    });
                    //$('#dashboard-report-range').datepicker('update')
                }

                if($scope.$parent.hasOwnProperty("startDate")){
                    $scope.$parent.$watch('startDate', function(value){
                        $scope.startRange()
                    });
                }

                if($scope.$parent.hasOwnProperty("datesEnabled")){
                    $scope.$parent.$watch('datesEnabled', function(val, val2){
                        console.log(val, val2)
                        $scope.reRender()
                    })
                }

                $scope.reRender()
                $scope.startRange();
            });
            
            // $('.demo span').click(function() {
            //     $(this).parent().find('input').click();
            // }).css({
            //     "cursor" : "pointer"
            // });
        }],
        link: function (scope, iElement, iAttrs) {

        }
    };
}])