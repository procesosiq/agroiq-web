app.controller('control', ['$scope','$http','$interval','client','$controller','$timeout' ,'$window', function($scope,$http,$interval,client, $controller , $timeout,$window){

	$scope.original = {
		id : "<?= $_GET['id'] ?>",
		nombre : ""
	}
	$scope.data = {
		id : "<?= $_GET['id'] ?>",
		nombre : ""
	}
	$scope.exportadores = []
	$scope.clientes = []
		
	$scope.init = function(){
		client.post("phrapi/configuracion/get/categoria", function(r, b){
			b()
			if(r){
				console.log(r)
				if(r.hasOwnProperty("categoria")){
					$scope.original.nombre = r.categoria.nombre
					$scope.data.nombre = r.categoria.nombre
				}
			}
		}, { id : $scope.original.id })
	}

	$scope.saveDatos = function(){
		if($scope.data.nombre != ""){
			client.post("phrapi/configuracion/save/categoria", function(r, b){
				b()
				if(r){
					alert("Formulario Guardado", "Categoria", "success")
					window.location = "configCategorias";
				}
			}, $scope.data)
		}else{
			alert("Formulario incompleto")
		}
	}

	$scope.init();
}]);