app.controller('control', ['$scope','$http','$interval','client','$controller','$timeout' ,'$window', function($scope,$http,$interval,client, $controller , $timeout,$window){

	$scope.original = {
		id : "<?= $_GET['id'] ?>",
		exportador : "",
		cliente : ""
	}
	$scope.data = {
		id : "<?= $_GET['id'] ?>",
		exportador : "",
		cliente : ""
	}
		
	$scope.init = function(){
		client.post("phrapi/configuracion/get/exportador", function(r, b){
			b()
			if(r){
				if(r.hasOwnProperty("clientes")){
					$scope.clientes = r.clientes
				}
				if(r.hasOwnProperty("exportador")){
					$scope.original.exportador = r.exportador.nombre
					$scope.original.cliente = r.exportador.id_cliente

					$scope.data.exportador = r.exportador.nombre
					$scope.data.cliente = parseInt(r.exportador.id_cliente)					
				}
			}
		}, { id : $scope.original.id })
	}

	$scope.saveDatos = function(){
		if($scope.data.exportador != "" && $scope.data.cliente != ""){
			client.post("phrapi/configuracion/save/exportador", function(r, b){
				b()
				if(r){
					alert("Formulario Guardado", "Exportadores", "success")
					window.location = "configExportaodres";
				}
			}, $scope.data)
		}else{
			alert("Formulario incompleto")
		}
	}

	$scope.init();
}]);