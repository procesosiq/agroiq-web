app.controller('control', ['$scope','$http','$interval','client','$controller','$timeout' ,'$window', function($scope,$http,$interval,client, $controller , $timeout,$window){

	$scope.original = {
		id : "<?= $_GET['id'] ?>",
		cliente : "",
		exportador : "",
		marca : "",
		tipo_caja : ""
	}
	$scope.data = {
		id : "<?= $_GET['id'] ?>",
		cliente : "",
		exportador : "",
		maraca : "",
		tipo_caja : ""
	}
	$scope.exportadores = []
	$scope.clientes = []
		
	$scope.init = function(){
		client.post("phrapi/configuracion/get/marca", function(r, b){
			b()
			if(r){
				console.log(r)
				if(r.hasOwnProperty("exportadores")){
					$scope.exportadores = r.exportadores
				}
				if(r.hasOwnProperty("clientes")){
					$scope.clientes = r.clientes
				}
				if(r.hasOwnProperty("marca")){
					$scope.original.exportador = r.marca.id_exportador
					$scope.original.tipo_caja = r.marca.tipo_caja
					$scope.original.marca = r.marca.nombre

					$scope.data.exportador = r.marca.id_exportador
					$scope.data.tipo_caja = r.marca.tipo_caja
					$scope.data.marca = r.marca.nombre
				}
				if(r.hasOwnProperty("id_cliente")){
					$scope.data.cliente = r.id_cliente
				}
			}
		}, { id : $scope.original.id })

		$("#changeCliente").change(function(){
			$scope.changeCliente()
		})
	}

	$scope.changeCliente = function(){
		client.post("phrapi/configuracion/get/marca", function(r,b){
			b()
			console.log(r)
			if(r){
				if(r.hasOwnProperty("exportadores")){
					$scope.exportadores = r.exportadores
				}
				if(r.hasOwnProperty("clientes")){
					$scope.clientes = r.clientes
				}
			}
		}, $scope.data)
	}

	$scope.saveDatos = function(){
		if($scope.data.cliente != "" && $scope.data.exportador != "" && $scope.data.marca != "" && $scope.data.tipo_caja != ""){
			client.post("phrapi/configuracion/save/marca", function(r,b){
				b()
				if(r){
					alert("Formulario Guardado", "Marcas", "success")
					window.location = "configMarcas";
				}
			}, $scope.data)
		}else{
			alert("Formulario incompleto")
		}
	}

	$scope.init();
}]);