$(document).unbind('keydown').bind('keydown', function (event) {
    var doPrevent = false;
    if (event.keyCode === 8) {
        var d = event.srcElement || event.target;
        if ((d.tagName.toUpperCase() === 'INPUT' && 
	             (
	                 d.type.toUpperCase() === 'TEXT' ||
	                 d.type.toUpperCase() === 'PASSWORD' || 
	                 d.type.toUpperCase() === 'FILE' || 
	                 d.type.toUpperCase() === 'SEARCH' || 
	                 d.type.toUpperCase() === 'EMAIL' || 
	                 d.type.toUpperCase() === 'NUMBER' || 
	                 d.type.toUpperCase() === 'DATE' 
	             )
             ) 
        	|| d.tagName.toUpperCase() === 'TEXTAREA' || d.contentEditable == "true") {
            doPrevent = d.readOnly || d.disabled;
        }
        else {
            doPrevent = true;
        }
    }

    if (doPrevent) {
        event.preventDefault();
    }
});


app.directive('stringToNumber', function() {
  return {
    require: 'ngModel',
    link: function(scope, element, attrs, ngModel) {
      ngModel.$parsers.push(function(value) {
        return '' + value;
      });
      ngModel.$formatters.push(function(value) {
        return parseFloat(value, 10);
      });
    }
  };
});

app.controller('general', ['$scope','$http','$interval','client','$controller', function($scope,$http,$interval,client, $controller){

	$scope.init = function(){
		client.post("phrapi/General/index" , $scope.printGeneral, {});
	};

	$scope.general = {

	}

	$scope.save = function(){
		console.log($scope.general);
	}

	$scope.printGeneral = function(r , b){
		b();
		if(r){
			console.log(r)
		}
	}
}]);





