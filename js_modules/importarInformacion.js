const { app } = window

app.service('importar',[ '$http' , function($http){

}])

var xmls = []

app.directive("fileread", [function () {
    return {
        scope: {
            fileread: "="
        },
        link: function (scope, element, attributes) {
            element.bind("change", function (changeEvent) {
                
                scope.$apply(function () {
                    scope.fileread = changeEvent.target.files;
                    
                    xmls = []
                    Object.keys(changeEvent.target.files).map((key) => {
                        xmls.push(changeEvent.target.files[key].name)
                    })
                });
            });
        }
    }
}]);

app.directive('file', function () {
    return {
        scope: {
            file: '='
        },
        link: function (scope, el, attrs) {
            el.bind('change', function (event) {
                var file = event.target.files[0];
                scope.file = file ? file : undefined;
                scope.$apply();
            });
        }
    };
});
app.controller('importarInformacion', ['$scope', 'importar', '$http', function ($scope, importar, $http) {

    $scope.xmls = []
    $("#xmls").bind('change', () => {
        setTimeout(() => {
            console.log('change')
            console.log(xmls)
            $scope.$apply(() => {
                $scope.xmls = xmls || []
            })
        }, 250)
    })

    $scope.subirXmls = () => {
        document.getElementById('subir_fotos').classList.remove('hide')
        var data = {}
        var subidas = 0
        Object.keys($scope.fileread).map((key) => {
            let file = $scope.fileread[key]

            function callback(dataUrl){
                $http({
                    method: 'POST',
                    url: '/phrapi/lancofruit/upXmls',
                    data: {
                        upload : dataUrl
                    }
                })
                .success(function(data){
                    document.getElementById("no_"+file.name).classList.add('hide')
                    document.getElementById("si_"+file.name).classList.remove('hide')
                    subidas++
                    if(subidas == Object.keys($scope.fileread).length){
                        setTimeout(() => {
                            alert("Se guardo correctamente", "Formulario", "success", () => { 
                                $scope.$apply(() => {
                                    document.getElementById('xmls').value = "" 
                                    document.getElementById('subir_fotos').classList.add('hide')
                                })
                            })
                        }, 1000)
                    }
                })
                .error(function(data, status){
        
                })
            }

            imgToDataURL(file, callback);
        })
    }

    function imgToDataURL(file, callback) {
        var reader = new FileReader();
        reader.readAsDataURL(file)

        reader.onload = function(){
            callback(reader.result)
        }
    }

}]);