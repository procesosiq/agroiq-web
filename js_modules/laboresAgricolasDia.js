function arrayMin(arr, key) {
    let min = null
    arr.forEach((val) => {
        if(val[key]){
            if(min == null || val[key] < min) min = val[key]
        }
    })
    return min
}

app.service('request', ['$http', ($http) => {
    var service = {}

    service.last = (callback, params) => {
        load.block()
        $http.post('phrapi/laboresAgricolasDia/last', params || {}).then((r) => {
            load.unblock()
            callback(r.data)
        })
    }

    service.index = (callback, params) => {
        load.block()
        $http.post('phrapi/laboresAgricolasDia/index', params || {}).then((r) => {
            load.unblock()
            callback(r.data)
        })
    }

    service.variables = (callback, params) => {
        load.block()
        $http.post('phrapi/laboresAgricolasDia/variables', params || {}).then((r) => {
            load.unblock()
            callback(r.data)
        })
    }

    service.pieCausas = (callback, params) => {
        load.block('pie-causas-container')
        $http.post('phrapi/laboresAgricolasDia/pieCausas', params || {}).then((r) => {
            load.unblock('pie-causas-container')
            callback(r.data)
        })
    }

    service.calidadLabor = (callback, params) => {
        load.block('calidadLabor')
        $http.post('phrapi/laboresAgricolasDia/calidadLabor', params || {}).then((r) => {
            load.unblock('calidadLabor')
            callback(r.data)
        })
    }

    service.calidadLote = (callback, params) => {
        load.block('calidadLote')
        $http.post('phrapi/laboresAgricolasDia/calidadLote', params || {}).then((r) => {
            load.unblock('calidadLote')
            callback(r.data)
        })
    }

    service.markers = (callback, params) => {
        load.block('map-container')
        $http.post('phrapi/laboresAgricolasDia/markers', params || {}).then((r) => {
            load.unblock('map-container')
            callback(r.data)
        })
    }

    return service
}])

app.filter('avg', () => {
    return (input) => {
        let count = 0, sum = 0
        input.forEach((val) => {
            if(parseFloat(val)){
                count++
                sum+=val
            }
        })
        return sum/count
    }
})

app.filter('getNotRepeat', function () {
    return function (data, key) {        
        if (angular.isUndefined(data) || angular.isUndefined(key))
            return [];
        
        let _arr = []
        data.forEach((value) => {
            if(value[key]){
                if(!_arr.includes(value[key])) _arr.push(value[key])
            }
        })
        return _arr;
    }
})

function initPastel(id, series){
    var legends = []
    var newSeries = []
    Object.keys(series).map(key => {
        newSeries.push({
            label : series[key].label,
            value : parseFloat(series[key].value)
        })
        if(legends.indexOf(series[key]) != -1) legends.push(series[key]);
    })
    setTimeout(() => {
        let data = {
            data : newSeries,
            nameseries : "Pastel",
            legend : legends,
            titulo : "",
            id : id
        }
        let parent = $("#"+id).parent()
        parent.empty()
        parent.append(`<div id="${id}" class="chart"></div>`)
        ReactDOM.render(React.createElement(Pastel, data), document.getElementById(id));
    }, 250)
}

app.controller('controller', ['$scope', 'request', function($scope, $request){

    $scope.loadCalidadUmbral = () => {
        let c = localStorage.getItem('calidad_umbral')
        if(c){
            $scope.umbral = JSON.parse(c)
        }else{
            $scope.umbral = {
                max : 92,
                min : 90
            }
        }
    }
    $scope.saveCalidadUmbral = () => {
        toastr.success('Umbral guardado')
        localStorage.setItem('calidad_umbral', JSON.stringify($scope.umbral))
        $scope.reRenderMarkers()
    }

    $scope.openMenu = () => {
        $(".toggler-close, .theme-options").show()
    }
    $scope.closeMenu = () => {
        $(".toggler-close, .theme-options").hide()
    }


    // BEGIN UMBRAL
    $scope.umbral = {}
    const getUmbralMin = () => {
        return $scope.umbral.min
    }

    const getUmbralHigh = () => {
        return $scope.umbral.max
    }

    // BEGIN MAP
    let map = null
    let bounds = new google.maps.LatLngBounds();

    const initMap = () => {
        map = new GMaps({
            div: '#map',
            lat: -1.725187,
            lng: -78.854211,
            zoom: 7,
            streetViewControl: false,
        });

        map.setMapTypeId('satellite')
    }

    const getMarkerHTML = (marker) => {
        if(typeof marker === 'function') return null

        return `
            <div>
                <p>
                    <b>Lote : ${marker.lote}</b><br>
                    <b>Labor : ${marker.labor}</b><br>
                    <ol>
                        ${(marker.plantas || []).map((l) => { return `<li>${l}%</li>` }).join('')}
                    </ol>
                    <b>Prom : ${marker.promedio}%</b>
                </p>
            </div>
        `
    }

    const getColorMarkerUmbral = (value) => {
        if(value < getUmbralMin()){
            return 'red'
        }
        else if(value > getUmbralHigh()){
            return 'green'
        }else {
            return 'orange'
        }
    }

    const inRangeMarkers = (value) => {
        let e = true
        if($scope.filters.markers.bellow){
            if($scope.filters.markers.bellow <= value) e = false
        }
        if($scope.filters.markers.above){
            if($scope.filters.markers.above >= value) e = false
        }
        return e
    }

    const addMarker = function(data , label){
        if(data){
            for(let i in data){
                let marker = data[i]
                if(inRangeMarkers(marker.promedio)){
                    map.addMarker({
                        lat: marker.lat,
                        lng: marker.lng,
                        title: marker.labor,
                        icon : `http://maps.google.com/mapfiles/ms/icons/${getColorMarkerUmbral(marker.promedio)}-dot.png`,
                        infoWindow: {
                            content: getMarkerHTML(marker)
                        }
                    });
                    bounds.extend(new google.maps.LatLng(marker.lat, marker.lng));
                    map.fitBounds(bounds);
                }
            }
        }
    }

    const removeMarkers = () => {
        map.removeMarkers()
    }

    $scope.reRenderMarkers = () => {
        removeMarkers()
        addMarker($scope.markers.data)
    }

    $scope.refrestablecerMapa = () => {
        $scope.filters.markers = {}
        $scope.changeMap()
    }

    $scope.changeMap = () => {
        removeMarkers()
        $request.markers((r) => {
            responseMarkers(r)
        }, $scope.filters)
    }

    initMap()

    // END MAP

    $scope.$watch("filters.time_mode", function(newvalue){
        if(newvalue == 'Periodal'){
            $scope.filters.fecha_inicial = ''
            $scope.filters.fecha_final = ''
            $scope.semana = ''
            clearDatePicker()
        }

        if(newvalue == 'Semanal'){
            $scope.filters.fecha_inicial = ''
            $scope.filters.fecha_final = ''
            $scope.periodo = ''
        }

        if(newvalue == 'Diario'){
            $scope.filters.semana = ''
            $scope.filters.periodo = ''
        }
    })

    let datesEnabled = []
    const datepickerHighlight = () => {
        $('#datepicker').datepicker({
            beforeShowDay: function (date) {
                let fecha = moment(date).format('YYYY-MM-DD')
                let has = datesEnabled.indexOf(fecha) > -1
                return has ? { classes: 'highlight', tooltip: 'Procesado', enabled : true } : { tooltip : 'Sin proceso', enabled : false }
            }
        });
        $("#datepicker").datepicker('setDate', $scope.filters.fecha_inicial)
        $("#datepicker").on('changeDate', function(e){
            if(e.date){
                removeMarkers()
                $scope.filters.fecha_inicial = moment(e.date).format('YYYY-MM-DD')
                $scope.filters.fecha_final = moment(e.date).format('YYYY-MM-DD')
                $scope.variables()
            }
        })
        $('#datepicker').datepicker('update')
    }

    const clearDatePicker = ( ) => {
        $("#datepicker").datepicker('setDate', null)
        $('#datepicker').datepicker('update')
    }

    $scope.changePeriodo = () => {
        if($scope.filters.periodo > 0){
            $scope.variables()
        }
    }

    $scope.changeSemana = () => {
        if($scope.filters.semana > 0){
            $scope.variables()
        }
    }

    $scope.filters = {
        time_mode : 'Diario',
        fecha_inicial : '',
        fecha_final : '',
        periodo : '',
        semana : '',
        id_finca : 0,
        pieCausa : {},
        calidadLabor:{},
        calidadLote:{},
        markers : {},
        fotos : {}
    }
    $scope.pieCausa = {
        labores : [],
        lotes : []
    }

    $scope.getUmbralFont = (val) => {
        if(val){
            if(val >= getUmbralHigh()) return 'font-green-jungle'
            else if(val < getUmbralHigh() && val >= getUmbralMin()) return 'font-yellow-gold'
            else return 'font-red-thunderbird'
        }

        return ''
    }

    $scope.deleteFilter = (prop) => {
        delete $scope.filters.fotos[prop]
    }

    $scope.last = () => {
        $request.last((r) => {
            $scope.periodos = r.periodos
            $scope.semanas = r.semanas
            $scope.filters.fecha_inicial = r.fecha
            $scope.filters.fecha_final = r.fecha
            datesEnabled = r.days
            datepickerHighlight()
            $scope.variables()
        })
    }

    $scope.variables = () => {
        $request.variables((r) => {
            $scope.fincas = r.fincas

            $scope.index()
        }, $scope.filters)
    }

    $scope.index = () => {
        $request.index((r) => {
            $scope.lotes = r.tablaResumen.lotes
            $scope.tabla_resumen = r.tablaResumen.tabla_labores_defectos
            $scope.fotos = r.fotos
            responsePieCausas(r.pieCausas)
            responseCalidadPorLabor(r.calidadLabor)
            responseCalidadPorLote(r.calidadLote)
            responseMarkers(r.markers)
        }, $scope.filters)
    }

    $scope.clearPieCausa = () => {
        $scope.filters.pieCausa = {}
        $scope.refreshPieCausas()
    }

    $scope.refreshPieCausas = () => {
        $request.pieCausas((r) => {
            responsePieCausas(r)
        }, $scope.filters)
    }

    $scope.refreshCalidadLabor = () => {
        $request.calidadLabor((r) => {
            responseCalidadPorLabor(r)
        }, $scope.filters)
    }

    $scope.refreshCalidadLote = () => {
        $request.calidadLote((r) => {
            responseCalidadPorLote(r)
        }, $scope.filters)
    }

    const responsePieCausas = ({ data, labores, lotes }) => {
        if(labores){
            const _labores = angular.copy(labores)
            let e = _labores.filter((l) => l.idLabor == $scope.filters.pieCausa.labor)
            if(!e.length > 0){
                $scope.filters.pieCausa.labor = labores[0].idLabor
                $scope.refreshPieCausas()
                return ;
            }
        }

        $scope.pieCausa.labores = labores
        $scope.pieCausa.lotes = lotes
        renderPieCausas(data)
    }

    const renderPieCausas = (data) => {
        initPastel('pie-causas', data)
    }

    const responseCalidadPorLabor = (data) => {
        $scope.calidadLabor = data
        if(data.selected_lote){
            $scope.filters.calidadLabor.lote = data.selected_lote
        }
        renderCalidadPorLabor(data.series, data.legends)
    }

    const renderCalidadPorLabor = (series, legends) => {
        let props = {
            id : 'chart-calidad-by-labor',
            series : series,
            legend : legends,
            showLegends : false,
            orientation : 'horizontal',
            min : arrayMin(series[0].data, 'value')-5,
            grid : { left : '18%' }
        }
        $("#chart-calidad-by-labor").empty()
        ReactDOM.render(
            React.createElement(BarrasStacked, props),
            document.getElementById('chart-calidad-by-labor')
        )
    }


    const responseCalidadPorLote = (data) => {
        $scope.calidadLote = data
        if(data.selected_labor){
            $scope.filters.calidadLote.labor = data.selected_labor
        }
        renderCalidadPorLote(data.series, data.legends)
    }

    const renderCalidadPorLote = (series, legends) => {
        let props = {
            id : 'chart-calidad-by-lote',
            series : series,
            legend : legends,
            showLegends : false,
            orientation : 'horizontal',
            min : arrayMin(series[0].data, 'value')-5
        }
        $("#chart-calidad-by-lote").empty()
        ReactDOM.render(
            React.createElement(BarrasStacked, props),
            document.getElementById('chart-calidad-by-lote')
        )
    }

    const responseMarkers = (data) => {
        $scope.markers = data
        addMarker(data.data)
    }

    $scope.excel = (id) => {
        var tableToExcel = (function() {
            var uri = 'data:application/vnd.ms-excel;base64,'
                , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body><table>{table}</table></body></html>'
                , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
                , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
            return function() {
                var contentTable = $("#"+id).html()
                var ctx = {worksheet: name || 'Worksheet', table: contentTable}
                window.location.href = uri + base64(format(template, ctx))
            }
        })()
        tableToExcel()
    }

    $scope.last()
    $scope.loadCalidadUmbral()

}]);

