

app.controller('lancofruit', ['$scope', '$http', 'client', function ($scope, $http, client) {
    $scope.datos = {
        id: "<?=$_GET['id']?>",
        codigo: "<?=$_GET['id']?>",
        referencia: "",
        ruc: "",
        nombre_propietario: "",
        tipo_local: "",
        nombre_local: "",
        status: "",
        telefono: "",
        correo: "",
        ruta: "",
        sector: "",
        direccion: "",
        tendero : ""
    };

    /***
     * Send a broadcast to the directive in order to change itself
     * if an id parameter is given only this ancucomplete is changed
     * @param id
     */
    $scope.changeInput = function (id , pos) {
        if (id) {
            $scope.$broadcast('angucomplete-alt:changeInput', id, $scope.tipos_locales[pos]);
        }
    }

    $scope.changeRutaSelected = function(){
        $("#rutas").val($scope.datos.id_ruta)
        let index = $("#rutas :selected").attr("index")
        $scope.modal.selected_ruta = $scope.rutas[index]
        setTimeout(function(){
            $("#vendedores").val($scope.datos.id_vendedor)
        }, 250)
    }

    $scope.olddatos = angular.copy($scope.datos);
    $scope.rutas = [];
    $scope.tipos_locales = [
        {
            local: "Gimnasio",
        },
        {
            local: "Minorista",
        },
        {
            local: "Restaurante",
        }
    ]

    $scope.init = function(){
        var data = {
            id : $scope.datos.id
        }
        client.post("phrapi/lancofruit/edit" , $scope.print ,data)
        $scope.modal.init();
    }

    $scope.print = function(r , b){
        b()
        

        if(r.hasOwnProperty("data")){
            $scope.datos = r.data

            $("#rutas").val(r.data.id_ruta)
            if(r.id_vendedor > 0){
                $("#vendedores").val(r.data.id_vendedor)
            }

            /*var pos = 0
            if (r.tipo_local == "Gimnasio")
                pos = 0
            if (r.tipo_local == "Minorista")
                pos = 1
            if (r.tipo_local == "Restaurante")
                pos = 2

            $scope.changeInput("tipo_local" , pos)*/
        }
    }

    $scope.save = function(){
        var data = $scope.datos
        client.post("phrapi/lancofruit/save", $scope.success, data)
    }

    $scope.success = function(r , b){
        b();
        if(r.hasOwnProperty("data")){
            alert("Registro registrado/modificado con el ID " + $scope.datos.id, "Lancofruit", "success" , function(){
                document.location.href = "/revisionLancofruit";
            });
        }
    }

    $scope.cancel = function(){
        document.location.href = "/revisionLancofruit";
    }

    $scope.modal = {
        vendedores : [],
        init : function(){
            client.post("phrapi/lacofruit/filters", $scope.modal.print, {})
        },
        print : function(r , b){
            b();
            if(r){
                $scope.categorias = r.categorias
                $scope.subcategorias = r.subcategorias
                $scope.modal.vendedor.vendedor = "";
                if(r.hasOwnProperty("rutas")){
                    $scope.rutas = r.rutas

                    setTimeout(function(){
                        $scope.$apply(function(){
                            $scope.changeRutaSelected()
                        })
                    }, 250)
                }
            }
        },
        success : function(r , b){
            b();
            if(r){
                if(r.status == 200){
                    $("#ruta-modal").modal("hide");
                    $scope.init();
                    alert("Se asigno correctamente", "Asignar Vendedor", "success")
                }
                else if(r.status == 400){
                    alert(r.message, "Asignar Vendedor")
                }
            }
        },
        vendedor : {
            vendedor : "",
            save : function(){
                if($("#vendedores").text().trim() != ""){
                    var data = {
                        id_ruta : $scope.datos.id_ruta,
                        id_vendedor : $scope.datos.id_vendedor,
                        id : "<?= $_GET['id'] ?>"
                    } 
                    $('#vendedor-modal').modal('hide');
                    client.post("phrapi/lancofruit/vendedor", $scope.modal.success, data)
                }else{
                    alert("El vendedor no puede estar vacio")
                }
            }
        }
    }

}]);