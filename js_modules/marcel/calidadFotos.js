
app.service('request', ['$http', function($http){

    this.getFotos = function(callback, params){
        load.block('accordion3')

        let data = params || {}
        let url = 'phrapi/marcel/calidad/fotos'
        $http.post(url, data).then((r) => {
            load.unblock('accordion3')
            callback(r.data)
        })
    }

    this.getLast = function(callback, params){
        let data = params || {}
        let url = 'phrapi/marcel/calidad/last'
        $http.post(url, data).then((r) => {
            callback(r.data)
        })
    }
   
}])

app.filter('orderObjectBy', function() {
	return function(items, field, reverse) {
    	var filtered = [];
    	angular.forEach(items, function(item) {
    		if(!parseFloat(item[field])){
    			item[field] = parseFloat(item[field]);
    		}
      		filtered.push(item);
    	});
    	filtered.sort(function (a, b) {
            if(field == 'hora' || field == 'fecha'){
                return moment(a.date).isAfter(b.date) ? 1 : -1;
            }else if(parseFloat(a[field]) && parseFloat(b[field])){
				return (parseFloat(a[field]) > parseFloat(b[field]) ? 1 : -1);
			}else{
				return (a[field] > b[field] ? 1 : -1);
			}
    	});
    	if(reverse) filtered.reverse();
    	return filtered;
  	};
});

app.controller('produccion', ['$scope', 'request', function($scope, $request){

    $scope.filters = {
        desde : "",
        hasta : ""
    }

    var printFotos = (r) => {
        if(r.data){
            $scope.marcas = r.marcas
            $scope.fotos = r.data
        }
    }

    $scope.getFotos = () => {
        $request.getFotos(printFotos, $scope.filters)
    }

    $scope.init = () => {
        $request.getLast((r) => {
            $scope.semanas = r.semanas_disponibles
            $scope.filters.desde = r.semana
            $scope.filters.hasta = r.semana
            $scope.getFotos()
        })
    }

    $scope.menorSemana = (sem) => {
        return parseInt(sem) <= parseInt($scope.filters.hasta)
    }
    $scope.mayorSemana = (sem) => {
        return parseInt(sem) >= parseInt($scope.filters.desde)
    }

    $scope.init()

}]);

