app.service('request', [ '$http', ($http) => {

    var service = {}

    service.porHectareas = (callback, params) => {
        load.block('porHectareas')
        $http.post('phrapi/comparacion/hectareas', params || {}).then(r => {
            load.unblock('porHectareas')
            callback(r.data)
        })
    }

    service.semanal = (callback, params) => {
        load.block('semanal')
        $http.post('phrapi/comparacion/semanal', params || {}).then(r => {
            load.unblock('semanal')
            callback(r.data)
        })
    }

    return service
}])

function getOptionsGraficaReact(id, options, umbral){
    var newOptions = {
        series: options.series,
        legend: options.legends,
        umbral: umbral,
        id: id,
        type : 'bar',
        actions : false,
        zoom : false
    }
    return newOptions
}

function initGrafica(id, options, umbral){
    setTimeout(() => {
        let parent = $("#"+id).parent()
        $("#"+id).remove()
        parent.append(`<div id="${id}" style="height:400px"></div>`)
        var data = getOptionsGraficaReact(id, options, umbral)
        ReactDOM.render(React.createElement(Historica, data), document.getElementById(id));
    }, 250)
}


const invertidos = ['RACIMOS RECUSADOS', 'EDAD PROM', '% RECUSADOS', '% MERMA CORTADA', '% MERMA PROCESADA']

app.controller('produccion', ['$scope', 'request', function($scope, $request){

    $scope.getUmbral = (umbral, value, name) => {
        let invertido = true
        if(invertidos.indexOf(name) > -1) invertido = false
        if(!invertido){
            if(value > 0 && umbral > 0){
                if(value <= umbral) return 'border-bottom-green'
                else return 'border-bottom-red'
            }
        }else{
            if(value > 0 && umbral > 0){
                if(value >= umbral) return 'border-bottom-green'
                else return 'border-bottom-red'
            }
        }
        return ''
    }

    $scope.getUmbralFont = (umbral, value, name) => {
        let invertido = true
        if(invertidos.indexOf(name) > -1) invertido = false
        if(!invertido){
            if(value > 0 && umbral > 0){
                if(value <= umbral) return 'text-green'
                else return 'text-red'
            }
        }else{
            if(value > 0 && umbral > 0){
                if(value >= umbral) return 'text-green'
                else return 'text-red'
            }
        }
        return ''
    }

    $scope.filters = {
        por_hectareas : 'HA/ANIO',
        por_semana : '',
        chart_por_hectareas : 'RACIMOS CORTADOS',
        chart_semanal : 'PESO RACIMO PROM LB'
    }
    $scope.por_hectareas= []
    $scope.init = () => {
        $scope.getPorHectareas()
        $scope.getSemanal()
    }

    $scope.getSemanal = () => {
        console.log($scope.filters)
        $request.semanal(printSemanal, $scope.filters)
    }
    $scope.getPorHectareas = () => {
        $request.porHectareas(printPorHectareas, $scope.filters)
    }
    $scope.chartPorHectareas = () => {
        let data = {}
        $scope.por_hectareas.map((s) => {
            if(s.name == $scope.filters.chart_por_hectareas) data = s
        })
        let series = {
                QUINTANA : {
                    name : 'QUINTANA',
                    data : [data.quintana],
                    connectNulls : true,
                    type : "bar",
                    itemStyle : {
                        normal : {
                            barBorderRadius : 0,
                            barBorderWidth : 6
                        }
                    },
                    label : {
                        normal : {
                            show : true,
                            position : "inside"
                        }
                    }
                },
                LANIADO : {
                    name : 'LANIADO',
                    data : [data.laniado],
                    connectNulls : true,
                    type : "bar",
                    itemStyle : {
                        normal : {
                            barBorderRadius : 0,
                            barBorderWidth : 6
                        }
                    },
                    label : {
                        normal : {
                            show : true,
                            position : "inside"
                        }
                    }
                },
                MARUN : {
                    name : 'MARUN',
                    data : [data.marun],
                    connectNulls : true,
                    type : "bar",
                    itemStyle : {
                        normal : {
                            barBorderRadius : 0,
                            barBorderWidth : 6
                        }
                    },
                    label : {
                        normal : {
                            show : true,
                            position : "inside"
                        }
                    }
                },
                REISET : {
                    name : 'REISET',
                    data : [data.reiset],
                    connectNulls : true,
                    type : "bar",
                    itemStyle : {
                        normal : {
                            barBorderRadius : 0,
                            barBorderWidth : 6
                        }
                    },
                    label : {
                        normal : {
                            show : true,
                            position : "inside"
                        }
                    }
                }
            },
            legends = ['']
        initGrafica('chart-porHeactareas', { series, legends }, data.agroban)
    }
    $scope.chartSemanal = () => {
        let data = {}
        $scope.semanal.map((s) => {
            if(s.name == $scope.filters.chart_semanal) data = s
        })
        let series = {
                QUINTANA : {
                    name : 'QUINTANA',
                    data : [data.quintana],
                    connectNulls : true,
                    type : "bar",
                    itemStyle : {
                        normal : {
                            barBorderRadius : 0,
                            barBorderWidth : 6
                        }
                    },
                    label : {
                        normal : {
                            show : true,
                            position : "inside"
                        }
                    }
                },
                LANIADO : {
                    name : 'LANIADO',
                    data : [data.laniado],
                    connectNulls : true,
                    type : "bar",
                    itemStyle : {
                        normal : {
                            barBorderRadius : 0,
                            barBorderWidth : 6
                        }
                    },
                    label : {
                        normal : {
                            show : true,
                            position : "inside"
                        }
                    }
                },
                MARUN : {
                    name : 'MARUN',
                    data : [data.marun],
                    connectNulls : true,
                    type : "bar",
                    itemStyle : {
                        normal : {
                            barBorderRadius : 0,
                            barBorderWidth : 6
                        }
                    },
                    label : {
                        normal : {
                            show : true,
                            position : "inside"
                        }
                    }
                },
                REISET : {
                    name : 'REISET',
                    data : [data.reiset],
                    connectNulls : true,
                    type : "bar",
                    itemStyle : {
                        normal : {
                            barBorderRadius : 0,
                            barBorderWidth : 6
                        }
                    },
                    label : {
                        normal : {
                            show : true,
                            position : "inside"
                        }
                    }
                }
            },
            legends = ['']
        initGrafica('chart-semanal', { series, legends }, data.agroban)
    }

    printPorHectareas = (r) => {
        $scope.por_hectareas = r.data
        $scope.chartPorHectareas()
    } 

    printSemanal = (r) => {
        $scope.semanal = r.data
        $scope.semanas = r.semanas
        $scope.chartSemanal()
    }

    $scope.init()
    
}]);
