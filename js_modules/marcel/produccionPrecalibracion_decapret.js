app.filter('sumOfValue', function () {
    return function (data, key) {
        if (angular.isUndefined(data) || angular.isUndefined(key))
            return 0;        
        var sum = 0;        
        angular.forEach(data,function(value){
            if(parseFloat(value[key]))
                sum = sum + parseFloat(value[key], 10);
        });        
        return sum;
    }
})

app.filter('orderObjectBy', function() {
	return function(items, field, reverse) {
    	var filtered = [];
    	angular.forEach(items, function(item) {
    		if(!isNaN(parseInt(item))){
    			item = parseInt(item);
    		}
      		filtered.push(item);
    	});
    	filtered.sort(function (a, b) {
            if(field == 'hora' || field == 'fecha'){
                return moment(a.date).isAfter(b.date) ? 1 : -1;
            }else if(parseFloat(a[field]) && parseFloat(b[field])){
				return (parseFloat(a[field]) > parseFloat(b[field]) ? 1 : -1);
			}else{
				return (a[field] > b[field] ? 1 : -1);
			}
    	});
    	if(reverse) filtered.reverse();
    	return filtered;
  	};
});

app.filter('orderBy', function() {
	return function(items, reverse) {
    	var filtered = [];
    	angular.forEach(items, function(item) {
    		if(!isNaN(parseInt(item))){
    			item = parseInt(item);
    		}
      		filtered.push(item);
    	});
    	filtered.sort(function (a, b) {
            if(parseFloat(a) && parseFloat(b)){
				return (parseFloat(a) > parseFloat(b) ? 1 : -1);
			}else{
				return (a > b ? 1 : -1);
			}
    	});
    	if(reverse) filtered.reverse();
    	return filtered;
  	};
});

app.service('request', ['$http', function($http){
    this.last = (callback, params) => {
        $http.post('phrapi/marcel/precalibracion/last', params ||  {}).then((r) => {
            callback(r.data)
        })
    }
    this.precalibracion = (callback, params) => {
        $http.post('phrapi/marcel/precalibracion/index', params ||  {}).then((r) => {
            callback(r.data)
        })
    }
}])

app.controller('produccion', ['$scope', 'request', '$filter', function($scope, $request, $filter){

    $scope.totales = {}
    $scope.filters = {
        year : moment().year(),
        week : moment().week()
    }

    $scope.coloresClass = {
        'VERDE' : 'bg-green-haze bg-font-green-haze',
        'AZUL' : 'bg-blue bg-font-blue',
        'BLANCO' : 'bg-white bg-font-white',
        'LILA' : 'bg-purple-studio bg-font-purple-studio',
        'ROJA' : 'bg-red-thunderbird bg-font-red-thunderbird',
        'CAFE' : 'bg-brown bg-font-brown',
        'AMARILLO' : 'bg-yellow-lemon bg-font-yellow-lemon',
        'NEGRO' : 'bg-dark bg-font-dark'
    }

    $scope.init = () => {
        load.block('precalibracion')
        $request.precalibracion((r) => {
            load.unblock('precalibracion')
            $scope.colorClass = r.color.class
            $scope.edades = r.edades
            $scope.precalibracion = r.data
            $scope.reloadTotales()
        }, $scope.filters)
    }

    getUmbral = (value) => {
        if(value){
            if(value < 98)
                return 'bg-red-thunderbird bg-font-red-thunderbird'
            else if (value == 98)
                return 'bg-yellow-gold bg-font-yellow-gold'
            else
                return 'bg-green-haze bg-font-green-haze'
        }
    }
    $scope.getUmbral = getUmbral

    $scope.reloadTotales = () => {
        setTimeout(() => {
            $scope.$apply(() => {
                $scope.totales.racimos_enfunde = $filter('sumOfValue')($scope.precalibracion, 'racimos_enfunde')
                $scope.totales.recobro = $scope.totales.cosechados > 0 && $scope.totales.racimos_enfunde > 0 ? ($scope.totales.cosechados / $scope.totales.racimos_enfunde * 100) : 0
                $scope.totales.caidos = $filter('sumOfValue')($scope.precalibracion, 'caidos') || 0
                $scope.totales.porc_caidos = $scope.totales.caidos > 0 && $scope.totales.racimos_enfunde > 0 ? ($scope.totales.caidos / $scope.totales.racimos_enfunde * 100) : 0
            })
        }, 100)
    }

    $scope.last = () => {
        $request.last((r) => {
            $scope.years = r.years
            $scope.semanas = r.weeks
            $scope.filters.week = r.last_week
            $scope.init()
        }, $scope.filters)
    }

    $scope.last()
}]);

