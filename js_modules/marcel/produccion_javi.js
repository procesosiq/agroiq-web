/*var socket = io.connect('http://procesosiq.com:5050', { 'forceNew': true });
socket.on('data', function(data) {  
  console.log(data);
})*/

app.service('produccion', ['client', function(client){
    this.lastDay = function(callback, params){
        let data = params || {}
        let url = 'phrapi/marcel/produccion/last'
        client.post(url, callback, data)
    }

    this.registros = function(callback, params){
        let data = params || {}
        let url = 'phrapi/marcel/produccion/historico'
        client.post(url, callback, data, 'registros')
    }

    this.racimosLote = function(callback, params){
        let data = params || {}
        let url = 'phrapi/marcel/produccion/lote'
        client.post(url, callback, data, 'table_por_lote')
    }

    this.racimosPalanca = function(callback, params){
        let data = params || {}
        let url = 'phrapi/marcel/produccion/palanca'
        client.post(url, callback, data, 'table_por_palanca')
    }

    this.racimosEdad = function(callback, params){
        let data = params || {}
        let url = 'phrapi/marcel/produccion/edad'
        client.post(url, callback, data, 'table_por_edad')
    }

    this.promediosPorLote = function(callback, params){
        let data = params || {}
        let url = 'phrapi/marcel/produccion/promedios'
        client.post(url, callback, data, 'promedios_lotes')
    }

    this.racimosFormularios = function(callback, params){
        let data = params || {}
        let url = 'phrapi/marcel/produccion/formularios'
        client.post(url, callback, data, 'table_racimos_formularios')
    }

    this.eliminar = function(callback, params){
        let data = params || {}
        let url = 'phrapi/marcel/produccion/eliminar'
        client.post(url, callback, data)
    }

    this.editar = function(callback, params){
        let data = params || {}
        let url = 'phrapi/marcel/produccion/editar'
        client.post(url, callback, data, 'edit_registros')
    }

    this.cuadreRacimos = function(callback, params){
        let data = params || {}
        let url = 'phrapi/marcel/produccion/cuadre'
        client.post(url, callback, data)
    }

    this.cuadrar = function(callback, params){
        let data = params || {}
        let url = 'phrapi/marcel/produccion/cuadrar'
        client.post(url, callback, data)
    }
}])

app.filter('num', function() {
    return function(input) {
    return parseInt(input, 10);
    };
});

app.filter('startFrom', function() {
    return function(input, start) {
        start = +start; //parse to int
        return input.slice(start);
    }
});

app.filter('sumOfValue', function () {
    return function (data, key) {        
        if (angular.isUndefined(data) || angular.isUndefined(key))
            return 0;        
        var sum = 0;
        angular.forEach(data,function(value){
            if(value[key] != "" && value[key] != undefined && parseFloat(value[key])){
                sum = sum + parseFloat(value[key], 10);
            }
        });
        return sum;
    }
})

app.filter('avgOfValue', function () {
    return function (data, key) {        
        if (angular.isUndefined(data) || angular.isUndefined(key))
            return 0;        
        var sum = 0;
        var count = 0;
        angular.forEach(data,function(value){
            if(value[key] != "" && value[key] != undefined && parseFloat(value[key])){
                sum = sum + parseFloat(value[key], 10);
                count++;
            }
        });
        sum = sum / count;
        return sum;
    }
})

app.filter('orderObjectBy', function() {
	return function(items, field, reverse) {
    	var filtered = [];
    	angular.forEach(items, function(item) {
            if(field == 'hora' || field == 'fecha'){
                item.date = moment(item.fecha  + ' ' + item.hora)
            }
    		if(!isNaN(parseInt(item))){
    			item = parseInt(item);
    		}
      		filtered.push(item);
    	});
    	filtered.sort(function (a, b) {
            if(field == 'hora' || field == 'fecha'){
                return moment(a.date).isAfter(b.date) ? 1 : -1;
            }else if(parseFloat(a[field]) && parseFloat(b[field])){
				return (parseFloat(a[field]) > parseFloat(b[field]) ? 1 : -1);
			}else{
				return (a[field] > b[field] ? 1 : -1);
			}
    	});
    	if(reverse) filtered.reverse();
    	return filtered;
  	};
});

app.controller('produccion', ['$scope', 'produccion', function($scope, produccion){
    $scope.id_company = 0;
    $scope.subTittle = ""
    $scope.registros = [];
    $scope.count = 0;

    $scope.produccion = {
        params : {
            finca : 1,
            idFinca : 1,
            idLote : 0,
            idLabor : 0,
            fecha_inicial : moment().startOf('month').format('YYYY-MM-DD'),
            fecha_final : moment().endOf('month').format('YYYY-MM-DD'),
            cliente: "",
            marca: "",
            palanca : "",
            initial : 0,
        },
        nocache : function(){
            $scope.init();
            $scope.getRegistros()
        }
    }

    $scope.startDate = ''
    $scope.getLastDay = function(){
        produccion.lastDay(function(r, b){
            b()
            if(r){
                $scope.fincas = r.fincas

                $scope.produccion.params.fecha_inicial = r.last.fecha
                $scope.produccion.params.fecha_final = r.last.fecha
                $scope.StartEndDateDirectives = {
                    startDate : moment(r.last.fecha),
                    endDate : moment(r.last.fecha)
                }
                $scope.startDate = r.last.fecha
                $scope.produccion.nocache()
            }
        })
    }

    $scope.lastDay = {
        startDate : moment().startOf('month'),
        endDate : moment().endOf('month'),
    }

    $scope.changeRangeDate = function(data){
        if(data){
            // TableDatatablesEditable.restart("sample_editable_1");
            $scope.produccion.params.fecha_inicial = data.hasOwnProperty("first_date") ? data.first_date : $scope.wizardStep.params.fecha_inicial;
            $scope.produccion.params.fecha_final = data.hasOwnProperty("second_date") ? data.second_date : $scope.wizardStep.params.fecha_final;
            $scope.subTittle = data.date_select || "Ultimo Registro"
        }
        $scope.produccion.nocache()
    }

    $scope.tabla = {
        produccion : []
    }

    $scope.openDetallePalanca = function(data){
        data.expanded = !data.expanded;
    }

    $scope.openDetalleLote = function(data){
        data.expanded = !data.expanded;
    }

    $scope.eliminar = () => {
        var elements = $(".delete")
        if(elements.length > 0){
            if(confirm(`Seguro deseas eliminar ${elements.length} elementos?`)){
                var ids = []
                var index = []
                for(var x = 0; x < elements.length; x++){
                    ids.push({
                        id : $(elements[x]).attr('data-id')
                    })
                }
                produccion.eliminar((r, b) => { 
                    b()
                    if(r){
                        $scope.getRegistros()
                        alert(`Se borraron ${elements.length} registros con éxito`, "", "success")
                    }
                }, { ids : ids })
            }
        }else{
            alert("No has seleccionado ninguna fila")
        }
    }

    $scope.colores = [];
    $scope.totales = [];
    $scope.palancas = [];

    $scope.init = function(){
        var data = $scope.produccion.params;
        produccion.racimosLote(function(r, b){
            b('table_por_lote')
            if(r){
                $scope.totales = r.totales;
                
                $scope.id_company = r.id_company;
                $scope.tabla.produccion = r.data;
                $scope.tags = r.tags;
            }
        }, data)
        produccion.racimosPalanca(function(r, b){
            b('table_por_palanca')
            if(r){
                $scope.palancas = r.palancas;
            }
        }, data)
        produccion.racimosEdad(function(r, b){
            b('table_por_edad')
            if(r){
                $scope.tabla.edades = r.data;
            }
        }, data)
        produccion.promediosPorLote(function(r, b){
            b('promedios_lotes')
            if(r){
                $scope.promedios = r.data
            }
        }, data)
        produccion.racimosFormularios(function(r, b){
            b('table_racimos_formularios')
            if(r){
                $scope.prom_edad_formularios = r.prom_edad
                $scope.racimos_formularios = r.racimos_formularios
                $scope.racimos_formularios_edad = r.table_edades
                $scope.racimos_formularios_palanca = r.table_palanca
            }
        }, data)
        produccion.cuadreRacimos(function(r, b){
            b()
            if(r){
                $scope.cuadre_proc = r.data
                $scope.cuadre_recu = r.data_recu
                $scope.dia_cuadrado = r.cuadrado > 0
                $scope.faltante_recu = r.faltante_recusados
            }
        }, data)
    }

    $scope.getRegistros = function(){
        $scope.cargandoHistorico = true
        let data = $scope.produccion.params;
        produccion.registros(function(r, b){
            b('registros')
            $scope.cargandoHistorico = false

            if(r){
                $scope.registros = r.data
                $scope.searchTable.changePagination()
            }
        }, data)
    }

    $scope.edit = function(row){
        if(!$scope.editing){
            $scope.editing = true;
            $scope.editRow = row;
            $scope.editRow.editing = true;
        }
    }

    $scope.guardar = function(){
        var valid = true
        if(!$scope.editing) valid = false;
        if($scope.editRow.peso <= 0) valid = false;
        if($scope.editRow.cuadrilla == "") valid = false;
        if($scope.editRow.lote == "") valid = false;
        if($scope.editRow.edad == "") valid = false;
        
        if(valid){
            $scope.editRow.editing = false;
            $scope.editing = false;
            produccion.editar((r, b) => {
                b('edit_registros')
                if(r){
                    $scope.editRow.class = r.class
                }else{
                    alert("Ocurrio un error al guardar")
                }
            }, $scope.editRow)
        }else{
            alert("Favor de completar los campos")
        }
    }

    $scope.cuadrar = function(){
        if(!$scope.dia_cuadrado){
            produccion.cuadrar((r, b) => {
                b()
                if(r){
                    if(r.status == 200){
                        alert("Refrescando", "", "success", () => location.reload() )
                        $scope.dia_cuadrado = true
                    }
                    else alert("Hubo un problema al procesar");
                }
            }, $scope.produccion.params)
        }else{
            alert("El dia ya ha sido procesado")
        }
    }

    $scope.start = true;

    $scope.printDetails = function(r,b){
        b();
        if(r){
            $scope.subTittle = r.date_select || $scope.subTittle
            $scope.produccion.params.initial = 1;
            //$scope.tabla.produccion = r.data;
            $scope.tabla.edades = r.data_edad;
            $scope.id_company = r.id_company;
            $scope.colores = r.colores;
            $scope.colores = r.colores;
            //$scope.palancas = r.palancas;
            $scope.totales = r.totales;
            if($scope.start){
                $scope.start = false;
                /*setInterval(function(){
                    $scope.init();
                } , (60000 * 5));*/
            }
        }
    }

    $scope.next = function(dataSource){
        if($scope.searchTable.actual_page < parseInt(dataSource.length / parseInt($scope.searchTable.limit)) + (dataSource.length % parseInt($scope.searchTable.limit) == 0 ? 0 : 1)) 
            $scope.searchTable.actual_page++;
        
        $scope.searchTable.startFrom = ($scope.searchTable.actual_page - 1) * (parseInt($scope.searchTable.limit))
    }

    $scope.prev = function(dataSource){
        if($scope.searchTable.actual_page > 1)
            $scope.searchTable.actual_page--;
        
        $scope.searchTable.startFrom = ($scope.searchTable.actual_page - 1) * (parseInt($scope.searchTable.limit))
    }

    $scope.setOrderTable = function(field){
        var reverse = false
        if(field == $scope.searchTable.orderBy){
            reverse = !$scope.searchTable.reverse
        }else{
            $scope.searchTable.orderBy = field
        }
        $scope.searchTable.reverse = reverse
    }

    $scope.searchTable = {
        orderBy : "id",
        reverse : false,
        limit : 10,
        actual_page : 1,
        startFrom : 0,
        optionsPagination : [
            10, 50, 100
        ],
        changePagination : function(){
            if($scope.searchTable.limit == 0)
                $scope.searchTable.limit = $scope.registros.length

            $scope.searchTable.numPages = parseInt($scope.registros.length / $scope.searchTable.limit)  + ($scope.registros.length % $scope.searchTable.limit == 0 ? 0 : 1)
            $scope.searchTable.actual_page = 1
            $scope.searchTable.startFrom = 0
        }
    }

    $scope.exportExcel = function(id_table, title){
        var tableToExcel = (function() {
            var uri = 'data:application/vnd.ms-excel;base64,'
                , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body><table>{table}</table></body></html>'
                , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
                , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
            return function(table, name) {
                if (!table.nodeType) table = document.getElementById(table)
                var contentTable = table.innerHTML
                var ctx = {worksheet: name || 'Worksheet', table: contentTable}
                window.location.href = uri + base64(format(template, ctx))
            }
        })()
        tableToExcel(id_table, "Información de Transtito")
    }

    $scope.exportPrint = function(id_table){
        $("#"+id_table).css("display", "")
        let image = '<div style="display:block; height:55;"><img style="float: right;" width="100" height="50" src="./../logos/Logo.png" /></div><br>';
        let table = document.getElementById(id_table);
        var contentTable = table.outerHTML;
        newWin = window.open("");
        newWin.document.write(contentTable);
        newWin.print();
        newWin.close();
        $("#"+id_table).css("display", "none")
    }

    $scope.exportarFormatoEspecial = () => {
        dayOfWeek = (day) => { return ["DOMINGO", "LUNES", "MARTES", "MIERCOLES", "JUEVES", "VIERNES", "SABADO"][day]  };
        embedInRow = (cols) => { return `<tr>${cols}</tr>` };
        generateRow = () => { var cols = []; for(var i = 0; i < 10; i ++) { cols.push("<td></td>") } return cols; };
        getCintaValida = (cinta) => { return { VERDE : 'VERDE', ROJA : 'ROJA', AMARILLO : 'AMARI', CAFE  : 'CAFE', AZUL : 'AZUL', BLANCO : 'BLANC', NEGRO : 'NEGRA', LILA : 'LILA' }[cinta]; }
        var table = []
        
        row = generateRow();
        row[0] = `<td>Archivo no.: ${moment($scope.produccion.params.fecha_inicial).format('DDD')}</td>`;
        table.push("<tr>" + row.join("") + "</tr>")

        row = generateRow();
        row[0] = `<td>Nombre: SEMANA ${moment($scope.produccion.params.fecha_inicial).format('WW')} ${dayOfWeek(moment($scope.produccion.params.fecha_inicial).format('d'))} ${moment($scope.produccion.params.fecha_inicial).format('DD')}</td>`;
        table.push("<tr>" + row.join("") + "</tr>")

        row = generateRow();
        row[0] = `<td>Fecha: ${moment($scope.produccion.params.fecha_inicial).format('DD-MM-YYYY')}</td>`;
        table.push("<tr>" + row.join("") + "</tr>")

        table.push([
            "<tr>",
            "<td>F01IDV(4)isID</td>",
            "<td>DW2Peso()</td>",
            "<td>C03LOTE()</td>",
            "<td>C13CINTA()</td>",
            "<td>F12MANOS(##)notID</td>",
            "<td>F22CALIBRE(##.##)notID</td>",
            "<td>F32L DEDOS(##.##)notID</td>",
            "<td>F43TIPO()notID</td>",
            "<td>F53NIVEL()notID</td>",
            "<td>F63CAUSA()notID</td>",
            "</tr>"
        ].join(""))

        table.push([
            "<tr>",
            "<td>IDV</td>",
            "<td>Peso</td>",
            "<td>LOTE</td>",
            "<td>CINTA</td>",
            "<td>MANOS</td>",
            "<td>CALIBRE</td>",
            "<td>L DEDOS</td>",
            "<td>TIPO</td>",
            "<td>NIVEL</td>",
            "<td>CAUSA</td>",
            "</tr>"
        ].join(""))

        $scope.registros.map((value, index) => {
            table.push([
                "<tr>",
                `<td>${value.id}</td>`,
                `<td>${value.peso}</td>`,
                `<td>${value.lote}</td>`,
                `<td>${getCintaValida(value.cinta)}</td>`,
                `<td>${(value.manos > 0) ? value.manos : ''}</td>`,
                `<td>${(value.calibre > 0) ? value.calibre : ''}</td>`,
                `<td>${(value.dedos > 0) ? value.dedos : ''}</td>`,
                `<td></td>`,
                `<td>${(value.tipo == 'RECUSADO') ? 'RECHA' : ''}</td>`,
                `<td>${(value.tipo == 'RECUSADO') ? value.causa : ''}</td>`,
                "</tr>"
            ].join(""))
        })

        // Añadie cuadre
        $scope.cuadre_proc.map((value, index) => {
            var diferencia = value.form - value.blz 
            for(var i = 0; i < diferencia; i++){
                table.push([
                    "<tr>",
                    `<td></td>`,
                    `<td>${value.peso_prom}</td>`,
                    `<td>${value.lote}</td>`,
                    `<td>${getCintaValida(value.cinta)}</td>`,
                    `<td></td>`,
                    `<td></td>`,
                    `<td></td>`,
                    `<td></td>`,
                    `<td></td>`,
                    `<td></td>`,
                    "</tr>"
                ].join(""))
            }
        })

        $scope.faltante_recu.map((value, index) => {
            var diferencia = value.cantidad - value.cantidad_blz 
            for(var i = 0; i < diferencia; i++){
                table.push([
                    "<tr>",
                    `<td></td>`,
                    `<td>${value.peso_prom}</td>`,
                    `<td>${value.lote}</td>`,
                    `<td>${getCintaValida(value.color_cinta)}</td>`,
                    `<td></td>`,
                    `<td></td>`,
                    `<td></td>`,
                    `<td></td>`,
                    `<td>RECHA</td>`,
                    `<td>${value.causa}</td>`,
                    "</tr>"
                ].join(""))
            }
        })

        var tableToExcel = (function() {
            var uri = 'data:application/vnd.ms-excel;base64,'
                , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body><table>{table}</table></body></html>'
                , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
                , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
            return function() {
                var contentTable = table.join("")
                var ctx = {worksheet: name || 'Worksheet', table: contentTable}
                window.location.href = uri + base64(format(template, ctx))
            }
        })()
        tableToExcel()
    }

    $scope.width = function(){
        return $('#fila1').width() + 8 +1
    }

}]);

