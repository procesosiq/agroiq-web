app.service('labores',[ '$http' , function($http){
this.data = (callback, params) => {
    load.block('labores_agricolas')
    $http.post('phrapi/labores/data', params).then(r => {
        if(r.data){
            callback(r.data)
            load.unblock('labores_agricolas') 
        }
    }).catch(err => {
        load.unblock('labores_agricolas')
        lert("Ocurrio algun problema al descargar la información")
    })
}

}])

app.filter('num', function(){
return function(input){
    return parseFloat(input) || 0
}
})

app.filter('sumOfValue', function () {
return function (data, key) {        
    if (angular.isUndefined(data) || angular.isUndefined(key))
        return 0;        
    var sum = 0;
    angular.forEach(data, function(value){
        if(value[key]) if(parseFloat(value[key]))
            sum = sum + parseFloat(value[key]);
    });
    return sum;
}
})

app.controller('labores', ['$scope', 'labores', function ($scope, labores) {

$scope.data_labores = [];
$scope.filters = {}
$scope.params = {}
$scope.getSortCls = field => {
    if(field == $scope.searchTable.orderBy){
        if($scope.searchTable.reverse)
            return 'sorting_desc'
        else
            return 'sorting_asc'
    }
    return  ''
}

$scope.init = () => {
    labores.data(r => {
         if(r){
            console.log(r.tipo_labores); 
            $scope.data_labores = r.tipo_labores;
         }
    })
}

$scope.isNumeric = val => {
    return $.isNumeric(val)
}

$scope.semaforo = value => {
    if($scope.isNumeric(value)){
        return 'bg-green-jungle bg-font-green-jungle'
    }else if(value == 'F'){
        return 'bg-red-thunderbird bg-font-red-thunderbird'
    }
    return ''
}

$scope.exportExcel = function(id_table){
    var tableToExcel = (function() {
        var uri = 'data:application/vnd.ms-excel;base64,'
            , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body><table>{table}</table></body></html>'
            , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
            , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
        return function(table, name) {
            if (!table.nodeType) table = document.getElementById(table)
            var contentTable = table.innerHTML
            // remove filters
            var cut = contentTable.search('<tr role="row" class="filter">')
            var cut2 = contentTable.search('</thead>')

            var part1 = contentTable.substring(0, cut)
            var part2 = contentTable.substring(cut2, contentTable.length)
            contentTable = part1 + part2
            
            var ctx = {worksheet: name || 'Worksheet', table: contentTable}
            window.location.href = uri + base64(format(template, ctx))
        }
    })()
    tableToExcel(id_table, "Información de Transtito")
}

$scope.exportPrint = function(id_table){
    let image = '<div style="display:block; height:55;"><img style="float: right;" width="100" height="50" src="./../logos/Logo.png" /></div><br>';
    let table = document.getElementById(id_table);
    var contentTable = table.outerHTML;
    // remove filters
    var cut = contentTable.search('<tr role="row" class="filter">')
    var cut2 = contentTable.search('</thead>')
    var part1 = contentTable.substring(0, cut)
    var part2 = contentTable.substring(cut2, contentTable.length)
    // add image
    contentTable = image + part1 + part2

    newWin = window.open("");
    newWin.document.write(contentTable);
    newWin.print();
    newWin.close();
}

$scope.init()
}]);