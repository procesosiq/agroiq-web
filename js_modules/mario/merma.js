/*----------  UTILIDADES SOBRE ARRAYS  ----------*/
app.filter('orderObjectBy', function() {
  return function(items, field, reverse) {
      //alert(field);
    var filtered = [];
    angular.forEach(items, function(item) {
      filtered.push(item);
    });
    filtered.sort(function (a, b) {
        //alert(a[field]);
      return (a[field] > b[field] ? 1 : -1);
    });
    if(reverse) filtered.reverse();
    return filtered;
  };
});

app.filter('unique', function() {
   return function(collection, keyname) {
      var output = [], 
          keys = [];

      angular.forEach(collection, function(item) {
          var key = item[keyname];
          if(keys.indexOf(key) === -1) {
              keys.push(key);
              output.push(item);
          }
      });

      return output;
   };
});

/*----------  OBJETO PARA GRAFICAS ECHARTS  ----------*/
var globalEcharts;
var appEcharts = {
    require : require,
    options_historico : [],
    options_historico_legends : [],
    options_tendencia : [],
    options_tendencia_legends : [],
    options_dia : [],
    dia_title : "",
    historico_avg : 0,
    margen : {
        min : 0,
        min_tendencia : 0,
        max : 5,
        max_tendencia : 2,
        umbral : 2,
        umbral_tendencia : .5,
    },
    options_danos : [],
    options_danos_detalle : [],
    type : "ENFUNDE",
    init : function(callback){
        callback = callback || this.loadModules;
        callback(echarts)
    },
    // Este metodo existe por que aun no completo la function de parseo para que funcione directo
    danos : function(){
        var series = this.options_danos;
        var options = {
            title : {
                text: series.title.text,
                subtext: series.title.subtext,
                x:'center'
            },
            tooltip : {
                trigger: 'item',
                formatter: "{a} <br/>{b} : {c} ({d}%)"
            },
            legend: {
                show : true,
                // orient : 'vertical',
                x : 'center',
                y : 'bottom',
                data: series.legend.data
            },
            toolbox: {
                show : true,
                feature : {
                    mark : {show: false},
                    dataView : {show: false, readOnly: false},
                    magicType : {
                        show: true, 
                        type: ['pie', 'funnel'],
                        option: {
                            funnel: {
                                x: '25%',
                                width: '50%',
                                funnelAlign: 'left',
                                max: 1548
                            }
                        }
                    },
                    restore : {show: true},
                    saveAsImage : {show: true, name : series.toolbox.feature.saveAsImage.name}
                }
            },
            calculable : true,
            series : [
                {
                    name:series.series.name,
                    type:'pie',
                    radius : '40%',
                    center: ['50%', '40%'],
                    data:series.series.data
                }
            ]
        };
        return options;
    },
    // Este metodo existe por que aun no completo la function de parseo para que funcione directo
    // Adicionalmente es aqui donde se cambia entre tipo de detalle por medio de la flag 'type'
    danos_detalle : function(){
        var options = {};
        ////console.log(this.type);
        if(this.options_danos_detalle.hasOwnProperty(this.type)){
            var series = this.options_danos_detalle[this.type];
            options = {
                title : {
                    text: series.title.text,
                    subtext: series.title.subtext,
                    x:'center'
                },
                tooltip : {
                    trigger: 'item',
                    formatter: "{a} <br/>{b} : {c} ({d}%)"
                },
                legend: {
                    show : true,
                    // orient : 'vertical',
                    x : 'center',
                    y : 'bottom',
                    data: series.legend.data
                },
                toolbox: {
                    show : true,
                    feature : {
                        mark : {show: false},
                        dataView : {show: false, readOnly: false},
                        magicType : {
                            show: true, 
                            type: ['pie', 'funnel'],
                            option: {
                                funnel: {
                                    x: '25%',
                                    width: '50%',
                                    funnelAlign: 'left',
                                    max: 1548
                                }
                            }
                        },
                        restore : {show: true},
                        saveAsImage : {show: true, name : series.toolbox.feature.saveAsImage.name}
                    }
                },
                calculable : true,
                series : [
                    {
                        name:series.series.name,
                        type:'pie',
                        radius : '40%',
                        center: ['50%', '40%'],
                        data:series.series.data
                    }
                ]
            };
        }
        return options;
    },
    historico : function(){
        //// console.log(this.options_historico);
        var category = [];
        var legend = []; 
        var series = [];
        var legend = [];
        var legend_data = [];
        var series = [];
        legend = Object.keys(this.options_historico_legends);
        //console.log(legend)
        for(var i in this.options_historico){
            //console.log(this.options_historico[i]);
            series.push(this.options_historico[i]);
            legend_data.push(this.options_historico[i].name);
        }

        //console.log(series);

        var option = {   
            tooltip : {
                trigger: 'axis'
            },
            legend: {
                show : true,
                // orient : 'vertical',
                x : 'center',
                y : 'bottom',
                data: legend_data
            },
            toolbox: {
                    show : true,
                    feature : {        
                        // dataView : {show: true, readOnly: false},
                        magicType : {show: true, type: ['line', 'bar']},
                        restore : {show: true},
                        saveAsImage : {show: true}
                    }
                },
                calculable : true,
                xAxis : [
                    {
                        // xAxisName:'hola',
                        type : 'category',
                        boundaryGap : false,
                        data : legend           
                    },
                
                ],
                yAxis : [
                    {
                        type : 'value',
                        axisLabel : {
                            formatter: '{value}'
                        },
                        min : this.margen.min,
                        max : this.margen.max
                    }
                ],
                series : series
            };
        return option;
    },
    dia : function(){
        var legend_tmp = [];
        var legend = [];
        var series = []
        var options_dia = this.options_dia;

        legend_tmp = Object.keys(options_dia.label);

        legend_tmp.sort(function(a,b){
            return new Date(a) - new Date(b);
        });

        legend = legend_tmp.map(function(value,index,arr){
            var date = new Date(value);
            var horas = date.getHours();
            var minutos = date.getMinutes();
            if(parseInt(minutos) == 0){
                minutos = "00";
            }
            if(parseInt(minutos) < 10){
                minutos = "0" + minutos;
            }
            series.push(options_dia.series[value]);
            return horas + ":" + minutos ;
        });
        //// console.log(legend);
        //// console.log(options_dia);
        //// console.log(series);

        // var avg = new Number(this.historico_avg).toFixed(2);

        var option = {
            title : {
                text: this.dia_title,
                x:'center'
            },
            tooltip: {
                show: true
            },
            xAxis: [
                {
                    type: 'category',
                    data : legend
                }
            ],
            yAxis: [
                {
                    type: 'value',
                    min : this.margen.min,
                    max : this.margen.max
                }
            ],
            series: [
                {
                    name : "Merma",
                    type : "bar",
                    itemStyle: {
                        normal: {
                        color: '#007537',
                        barBorderColor: '#007537',
                        barBorderWidth: 6,
                        barBorderRadius:0,
                        label : {
                                show: true, position: 'insideTop'
                            }
                        }
                    },
                    markLine : {
                        data : [
                            [
                                {name: 'Umbral', value: this.margen.umbral, xAxis: -1, yAxis: this.margen.umbral},
                                {xAxis: 52, yAxis: this.margen.umbral}
                            ],
                        ]
                    },
                    data : series
                }
            ]
        };

        //console.log(option)
        return option;
    },
    tendencia : function(){
        //// console.log(this.options_historico);
        var category = [];
        var legend = []; 
        var series = [];
        var legend = [];
        var legend_data = [];
        var series = [];
        legend = Object.keys(this.options_tendencia_legends);
        //console.log(legend)
        for(var i in this.options_tendencia){
            //console.log(this.options_tendencia[i]);
            series.push(this.options_tendencia[i]);
            legend_data.push(this.options_tendencia[i].name);
        }

        //console.log(series);

        var option = {   
            tooltip : {
                trigger: 'axis'
            },
            legend: {
                show : true,
                // orient : 'vertical',
                x : 'center',
                y : 'bottom',
                data: legend_data
            },
            toolbox: {
                    show : true,
                    feature : {        
                        // dataView : {show: true, readOnly: false},
                        magicType : {show: true, type: ['line']},
                        restore : {show: true},
                        saveAsImage : {show: true}
                    }
                },
                calculable : true,
                xAxis : [
                    {
                        // xAxisName:'hola',
                        type : 'category',
                        boundaryGap : false,
                        data : legend           
                    },
                
                ],
                yAxis : [
                    {
                        type : 'value',
                        axisLabel : {
                            formatter: '{value}'
                        },
                        min : this.margen.min_tendencia,
                        max : this.margen.max_tendencia
                    }
                ],
                series : series
            };
        return option;
    },
    loadModules : function(echarts){
        globalEcharts = echarts;
        var historico = [], options_historico = appEcharts.historico();
        var tendencia = [], options_tendencia = appEcharts.tendencia();
        var dia = [], options_dia = appEcharts.dia();
        var danos = [] , options_danos = appEcharts.danos();
        var danos_detalle = [] , options_danos_detalle = appEcharts.danos_detalle();
        
        // Creamos la grafica
        historico = echarts.init(document.getElementById('historico') , 'infographic');
        // Asignamos las series , titulos , etc
        historico.setOption(options_historico);

        // Creamos la grafica
        tendencia = echarts.init(document.getElementById('tendencia') , 'infographic');
        // Asignamos las series , titulos , etc
        tendencia.setOption(options_tendencia);

        // Creamos la grafica
        dia = echarts.init(document.getElementById('dia') , 'infographic');
        // Asignamos las series , titulos , etc
        dia.setOption(options_dia);

        // Creamos la grafica
        danos = echarts.init(document.getElementById('echarts_generales') , 'infographic');
        // Asignamos las series , titulos , etc
        danos.setOption(options_danos);

        // Creamos la grafica
        danos_detalle = echarts.init(document.getElementById('echarts_generales_detalles') , 'infographic');
        // Validamos que exista data para el detalle
        if(options_danos_detalle.hasOwnProperty("title")){
            // Asignamos las series , titulos , etc
            danos_detalle.setOption(options_danos_detalle);
        }

        // Asignamos el tema
        danos.setTheme('infographic');
        danos_detalle.setTheme('infographic');
        // Asignamos el 'responsive'
        window.onresize = function(){
            historico.resize();
            tendencia.resize();
            dia.resize();
            danos_detalle.resize();
            danos.resize();
        }
    }
}
/*----------  OBJETO PARA GRAFICAS ECHARTS  ----------*/

function loadScript(options , margen){
    appEcharts.options_historico = options.historico;
    appEcharts.options_historico_legends = options.historico_legends;
    appEcharts.options_tendencia = options.tendencia;
    appEcharts.options_tendencia_legends = options.tendencia_legends;
    appEcharts.historico_avg = options.historico_avg;
    appEcharts.options_dia = options.dia;
    appEcharts.dia_title = options.dia_title;
    appEcharts.options_danos = options.danos;
    appEcharts.options_danos_detalle = options.danos_detalle;
    appEcharts.margen = margen;
    appEcharts.init();
}

app.filter('orderObjectBy', function() {
  return function(items, field, reverse) {
    var filtered = [];
    angular.forEach(items, function(item) {
        //// console.log(item)
        if(item.hasOwnProperty("lote")){
            if(isNaN(parseFloat(item.lote))){
                item.lote = parseFloat(item.lote);
            }else{
                item.lote = item.lote;
            }
        }
        if(item.hasOwnProperty("total_peso_merma")){
            item.total_peso_merma = parseFloat(item.total_peso_merma);
        }
        if(item.hasOwnProperty("total_defectos")){
            if(!isNaN(parseFloat(item.total_defectos)))
                item.total_defectos = parseFloat(item.total_defectos);
        }
        if(item.hasOwnProperty("merma")){
            if(!isNaN(parseFloat(item.merma)))
                item.merma = parseFloat(item.merma);
        }
        if(item.hasOwnProperty("danhos_peso")){
            if(!isNaN(parseFloat(item.danhos_peso)))
                item.danhos_peso = parseFloat(item.danhos_peso);
        }
        if(item.hasOwnProperty("filter")){
            if(!isNaN(parseFloat(item.filter)))
                item.filter = parseFloat(item.filter);
        }
        if(item.hasOwnProperty("cantidad")){
            if(!isNaN(parseFloat(item.cantidad)))
                item.cantidad = parseFloat(item.cantidad);
        }

      filtered.push(item);
    });
    filtered.sort(function (a, b) {
        //alert(a[field]);
      return (a[field] > b[field] ? 1 : -1);
    });
    if(reverse) filtered.reverse();
    return filtered;
  };
});

app.controller('informe_calidad', ['$scope', '$http', 'client', function($scope, $http, client){
    
    $scope.leyendaGeneralTitle = 'Merma';

    $scope.labelLbKg = "Kilos";
    $scope.statusLbKg = true;

    $scope.labelPeso = "";

    $scope.id_company = 0;
    $scope.tags = {
        merma : {
            value : 0,
            label : ""
        },
        merma_procesada : {
            value : 0,
            label : ""
        },
        merma_cortada : {
            value : 0,
            label : ""
        },
        tallo : {
            value : 0,
            label : ""
        },
        merma_cortada : {
            value : 0,
            label : ""
        },
        enfunde : {
            value : 0,
            label : ""
        },
        empaque : {
            value : 0,
            label : ""
        },
        deshoje : {
            value : 0,
            label : ""
        },
        lotero : {
            value : 0,
            label : ""
        },
        aereo : {
            value : 0,
            label : ""
        },
        terrestre : {
            value : 0,
            label : ""
        },
        administracion : {
            value : 0,
            label : ""
        },
        adm : {
            value : 0,
            label : ""
        },
        natural : {
            value : 0,
            label : ""
        },
        proceso : {
            value : 0,
            label : ""
        },
        campo : {
            value : 0,
            label : ""
        },
        cosecha : {
            value : 0,
            label : ""
        },
        animales : {
            value : 0,
            label : ""
        },
        hongos : {
            value : 0,
            label : ""
        },
        empacadora : {
            value : 0,
            label : ""
        },
        fisiologicos : {
            value : 0,
            label : ""
        },
        virus : {
            value : 0,
            label : ""
        },
        animal : {
            value : 0,
            label : ""
        },
        bacteria : {
            value : 0,
            label : ""
        },
        insectos : {
            value : 0,
            label : ""
        },
        viejos : {
            value : 0,
            label : ""
        },
        cajas : {
            value : 0,
            label : ""
        },
        usd : {
            value : 0,
            label : ""
        },
    };

    $scope.factor = 1;

    $scope.graficas = {
        historico : {},
        tendencia : {},
        historico_avg : 0,
        dia : {},
        dia_title : "",
        danos : {},
        danos_detalle : {},
        historico_legends : {},
        tendencia_legends : {},
    }

     $scope.StartEndDateDirectives = {
        startDate : moment().startOf('month'),
        endDate :moment().endOf('month'),
    }

    $scope.umbrales = {};
    $scope.calidad = {
        params : {
            idFinca : "",
            idFincaDia : "",
            idMerma : "MATERIA PRIMA",
            idLote : 0,
            idLabor : 0,
            fecha_inicial : moment().startOf('month').format('YYYY-MM-DD'),
            fecha_final : moment().endOf('month').format('YYYY-MM-DD'),
            cliente: "",
            marca: "",
            palanca : "",
            statusLbKg : true,
            year : "YEAR(CURRENT_DATE)"
        },
        step : 0,
        path : ['phrapi/mario/merma/index' , 'phrapi/merma/labores' , 'phrapi/merma/causas'],
        templatePath : [
            '/views/mario/templetes/merma/step1.html?' + Math.random()
        ]
    }

    $scope.historico_tabla = [];
    $scope.semanas_historico = [];

    $scope.historicoSemanal = function(mode){
        var data = {
            mode : mode,
            palanca : $scope.calidad.params.palanca
        }
        client.post('phrapi/marcel/merma/semanal' , $scope.printDetailsHistorica ,data , "contentHistorico");
    }
    
    $scope.openDetalleHistorico = function(data){
        data.expanded = !data.expanded;
    }

    $scope.bgTagsFlags = function(umbral, value){
        var className = "bg-yellow-gold bg-font-yellow-gold";
        if(!isNaN(parseFloat(value))){
            value = parseFloat(value)
            umbral = parseFloat(umbral || 0)

            if(value == umbral){
                return className
            }else if(value < umbral ){
                className = "bg-green-haze bg-font-green-haze"
            }else if(value > umbral){
                className = "bg-red-thunderbird bg-font-red-thunderbird"
            }
        }
        return className;
    }

    const convertModeSelect = function(){
        var mode = $("#mode").val();
        $scope.historicoSemanal(mode);
    }

    $scope.printDetailsHistorica = function(r,b){
        b("contentHistorico");
        if(r){
            if(r.hasOwnProperty("historico_tabla")){
                $scope.historico_tabla = r.historico_tabla;
                $scope.semanas_historico = r.semanas_historico;
            }
        }
    }

    // 24/06/2017 - TAG: HISTORICO SEMANAL

    $scope.fincas = [];
    $scope.mermas = {
        NETA : "Merma Neta",
        "MATERIA PRIMA" : "Merma Prima",
    };

    $scope.palancas = [];

    $scope.loadExternal = function(){
        if($scope.calidad.path[$scope.calidad.step] != ""){
            var data = $scope.calidad.params;
            client.post($scope.calidad.path[$scope.calidad.step] , $scope.startDetails , data);
            $scope.historicoSemanal("Total Peso");
        }
    }

    const changeYears = function(){
        $scope.calidad.params.year = $('#historico_years').val();
        $scope.loadExternal();
    }

    const cambiosMerma = function(){
        $scope.calidad.params.palanca = "";
        $scope.calidad.params.idFincaDia = "";
        $scope.loadExternal();
    }

    const cambiosPalanca = function(){
        $scope.loadExternal();
    }

    const cambiosFincas = function(){
        $scope.calidad.params.palanca = "";
        $scope.calidad.params.idFincaDia = "";
        $scope.loadExternal();
    }

	const cambiarDanosDetalle = function(){
		appEcharts.type = $('#detalles_danhos').val();
        appEcharts.init();
	}

    const fincasDia = function(){
        $scope.loadExternal();
    }

    const changeYearTendencia = function(){
        $scope.calidad.params.yearTendencia = $("#yearTendencia").val()
        $scope.loadExternal();   
    }

    $scope.changeRangeDate = function(data){
        if(data){
            $scope.calidad.params.fecha_inicial = data.hasOwnProperty("first_date") ? data.first_date : $scope.wizardStep.params.fecha_inicial;
            $scope.calidad.params.fecha_final = data.hasOwnProperty("second_date") ? data.second_date : $scope.wizardStep.params.fecha_final;
            $scope.loadExternal();
        }
    }

    $scope.tabla_danos_merma = [];
    $scope.tabla_lote_merma = [];
    $scope.tabla_danos_merma_danhos_merma = [];

    $scope.convertKg = function(){
        $scope.statusLbKg = !$scope.statusLbKg;
        if($scope.statusLbKg === true){
            $scope.labelLbKg = "Kilos";
            // $scope.labelPeso = "(KG)";
        }
        else if($scope.statusLbKg === false){
            $scope.labelLbKg = "Libras";
            // $scope.labelPeso = "(Lb)";
        }

        $scope.calidad.params.statusLbKg = $scope.statusLbKg;
        $scope.loadExternal();
    }

    $scope.classColumns = "col-md-6 col-sm-6";

    $scope.disableColumns = function(column , e){
        if(column){
            if($("." + column).hasClass('column_hide')){
                $("." + column).removeClass( column + ' column_hide').addClass( column );
                $(e.target).parent().removeClass('active')
            }else{
                $("." + column).addClass( column + ' column_hide');
                $(e.target).parent().addClass('active')
            }
        }
    }

    $scope.openDetalle= function(data , position){
        data.expanded = !data.expanded;
        if(position == 1){
            var generateId = ".detalle_"+data.bloque+"_"+data.type.replace(' ','_');
            if($(generateId).length > 0){
                if(data.expanded){
                    $(generateId).css('display', '');
                }else{
                    $(generateId).css('display', 'none');
                }
            }else{
                var row = $scope.generateRow(data);
                $("#2nivel_" +data.bloque +"_"+data.type.replace(' ','_')).after(row);
            }
        }
    }

    $scope.generateRow = function(data){
        var row = "";
        var generateId = "detalle_"+data.bloque+"_"+data.type.replace(' ','_');
        if(data.hasOwnProperty("details")){
            var table = data.details;
            for(var i in table){
                row += "<tr class='"+generateId+"' ng-show='category.expanded' style='cursor: pointer; text-align: right;'>";
                row += "<td style='text-align: left !important;'>"+table[i].campo+"</td>";
                row += "<td>"+parseFloat(table[i].cantidad).toFixed(2)+"</td>";
                row += "<td>"+parseFloat(table[i].danhos_peso).toFixed(2)+"</td>";
                row += "<td></td>";
                row +="</tr>";
            }
        }

        return row;
    }

    var $table_transform = $("#lote_table");

    $scope.startDetails = function(r , b){
        b();
        if(r){
        	$scope.factor = 1;
            $scope.classColumns = "col-md-6 col-sm-6";
			var options = {};
            // company 
            $scope.id_company = r.id_company
            // data grafica
            $scope.palancas = r.palanca || [];
            $scope.fincas = r.fincas || [];
            $scope.graficas.historico = r.historico || [];
            $scope.graficas.historico_avg = r.historico_avg || [];
            $scope.graficas.historico_legends = r.historico_legends || [];
            $scope.graficas.dia = r.dia || [];
            $scope.graficas.dia_title = r.dia_title || "";
            $scope.graficas.danos = r.danos || [];
            // La propiedad danos_detalle es un objeto con todos los detalles de daños 
            $scope.graficas.danos_detalle = r.danos_detalle || [];
            // Tags 
            $scope.umbrales = r.umbrals || {};
            $scope.tags.merma.value = new Number(r.tags.merma).toFixed(2);
            var margen = {
                min : 0 ,
                min_tendencia : 0 ,
                max : 5,
                max_tendencia : 100,
                umbral : 2,
                umbral_tendencia : 50,
            }
            if($scope.id_company == 2 || $scope.id_company == 8){
                $scope.tags.enfunde.value = new Number(r.tags.enfunde).toFixed(2);
                $scope.tags.campo.value = new Number(r.tags.campo).toFixed(2);
                $scope.tags.cosecha.value = new Number(r.tags.cosecha).toFixed(2);
                $scope.tags.animales.value = new Number(r.tags.animales).toFixed(2);
                $scope.tags.hongos.value = new Number(r.tags.hongos).toFixed(2);
                $scope.tags.empacadora.value = new Number(r.tags.empacadora).toFixed(2);
                $scope.tags.fisiologicos.value = new Number(r.tags.fisiologicos).toFixed(2);
                $scope.tags.cajas.value = new Number(r.tags.cajas).toFixed(2);
                $scope.tags.usd.value = new Number(r.tags.usd).toFixed(2);
                if($scope.statusLbKg === true){
                    $scope.labelPeso = "(KG)";
                }
                else if($scope.statusLbKg === false){
                    $scope.labelPeso = "(Lb)";
                }

                if($scope.id_company == 8){
                    margen.max = 5;
                    margen.umbral = 2;
                }
            }
            if($scope.id_company == 6){
                $scope.classColumns = "col-md-12 col-sm-12";
                $scope.tags.enfunde.value = new Number(r.tags.enfunde).toFixed(2);
                $scope.tags.adm.value = new Number(r.tags.adm).toFixed(2);
                $scope.tags.cosecha.value = new Number(r.tags.cosecha).toFixed(2);
                $scope.tags.natural.value = new Number(r.tags.natural).toFixed(2);
                $scope.tags.proceso.value = new Number(r.tags.proceso).toFixed(2);
                $scope.tags.merma_procesada.value = new Number(r.tags.merma_procesada).toFixed(2);
                $scope.tags.cajas.value = new Number(r.tags.cajas).toFixed(2);
                $scope.tags.usd.value = new Number(r.tags.usd).toFixed(2);
                margen.max = 25;
                margen.umbral = 10;
            }
            if($scope.id_company == 7){
                $scope.tags.merma_procesada.value = new Number(r.tags.merma_procesada).toFixed(2);
                $scope.tags.merma_cortada.value = new Number(r.tags.merma_cortada).toFixed(2);
                $scope.tags.tallo.value = new Number(r.tags.tallo).toFixed(2);
                $scope.tags.empaque.value = new Number(r.tags.empaque).toFixed(2);
                $scope.tags.administracion.value = new Number(r.tags.administracion).toFixed(2);
                $scope.tags.cosecha.value = new Number(r.tags.cosecha).toFixed(2);
                $scope.tags.lotero.value = new Number(r.tags.lotero).toFixed(2);
                $scope.tags.deshoje.value = new Number(r.tags.deshoje).toFixed(2);
                $scope.tags.fisiologicos.value = new Number(r.tags.fisiologicos).toFixed(2);
                $scope.tags.cajas.value = new Number(r.tags.cajas).toFixed(2);
                $scope.tags.usd.value = new Number(r.tags.usd).toFixed(2);
                if(r.hasOwnProperty("factor")){
                	$scope.factor = new Number(r.factor).toFixed(2);
                }
                $scope.labelPeso = "(KG)";
                margen.max = 25;
                margen.umbral = 10;
            }

            if($scope.id_company == 3){
                $scope.tags.merma_procesada.value = new Number(r.tags.merma_procesada).toFixed(2);
                $scope.tags.merma_cortada.value = new Number(r.tags.merma_cortada).toFixed(2);
                $scope.tags.tallo.value = new Number(r.tags.tallo).toFixed(2);
                $scope.tags.empaque.value = new Number(r.tags.empaque).toFixed(2);
                $scope.tags.administracion.value = new Number(r.tags.administracion).toFixed(2);
                $scope.tags.cosecha.value = new Number(r.tags.cosecha).toFixed(2);
                $scope.tags.lotero.value = new Number(r.tags.lotero).toFixed(2);
                $scope.tags.deshoje.value = new Number(r.tags.deshoje).toFixed(2);
                $scope.tags.fisiologicos.value = new Number(r.tags.fisiologicos).toFixed(2);
                $scope.tags.cajas.value = new Number(r.tags.cajas).toFixed(2);
                $scope.tags.usd.value = new Number(r.tags.usd).toFixed(2);
                if($scope.statusLbKg === true){
                    $scope.labelPeso = "(KG)";
                }
                else if($scope.statusLbKg === false){
                    $scope.labelPeso = "(Lb)";
                }
                margen.max = 25;
                margen.umbral = 10;
            }

            if($scope.id_company == 4 || $scope.id_company == 9){
                $scope.tags.merma_procesada.value = new Number(r.tags.merma_procesada).toFixed(2);
                $scope.tags.merma_cortada.value = new Number(r.tags.merma_cortada).toFixed(2);
                $scope.tags.tallo.value = new Number(r.tags.tallo).toFixed(2);
                $scope.tags.empacadora.value = new Number(r.tags.empacadora).toFixed(2);
                $scope.tags.animal.value = new Number(r.tags.animal).toFixed(2);
                $scope.tags.cosecha.value = new Number(r.tags.cosecha).toFixed(2);
                $scope.tags.terrestre.value = new Number(r.tags.terrestre).toFixed(2);
                $scope.tags.insectos.value = new Number(r.tags.insectos).toFixed(2);
                // $scope.tags.bacteria.value = new Number(r.tags.bacteria).toFixed(2);
                $scope.tags.aereo.value = new Number(r.tags.aereo).toFixed(2);
                $scope.tags.hongos.value = new Number(r.tags.hongos).toFixed(2);
                $scope.tags.fisiologicos.value = new Number(r.tags.fisiologicos).toFixed(2);
                $scope.tags.cajas.value = new Number(r.tags.cajas).toFixed(2);
                $scope.tags.usd.value = new Number(r.tags.usd).toFixed(2);
                $scope.labelPeso = "(KG)";
                margen.max = 20;
                margen.umbral = 5;
            }

            $scope.graficas.tendencia = r.tendencia || [];
            $scope.graficas.tendencia_legends = r.tendencia_legends || [];

            // tablas
			// console.log(r.tabla_lote_merma);
            $scope.tabla_lote_merma = r.tabla_lote_merma;
            $scope.tabla_danos_merma = r.tabla_danos_merma;
            $scope.tabla_danos_merma_danhos_merma = r.tabla_danos_merma_danhos_merma;
            // header
            if(r.data_header){
                $scope.param_peso    = r.data_header.peso;
                $scope.param_cluster = r.data_header.cluster;
                $scope.param_logo = r.data_header.logo;
            }

            setTimeout(function(){
                loadScript($scope.graficas , margen)
                generateSelect(r.tabla_danos_merma_danhos_merma);
                $(".counter_tags").counterUp({
                    delay: 10,
                    time: 1000
                });
                 $table_transform.bootstrapTable();
            } , 1000);
        }
    }

    const generateSelect = function(data){
        var html = [];
        var keys = Object.keys(data);
        for(var i in keys){
            html.push("<option value='"+keys[i]+"'>"+keys[i]+"</option>");
        }

        $('#detalles_danhos').empty();
        $('#detalles_danhos').append(html.join(' '));

        setTimeout(function(){
            cambiarDanosDetalle();
        } , 1000);
    }

    //$scope.companiesDefect = [2];
    $scope.companiesDefect = [];
    $scope.companiesDefectProm = [2, 7];

    $scope.inArray = function(needed , arr){
        return $.inArray(needed , arr) > -1 ? true : false; 
    }

    $scope.exportExcel = function(table_id, title){
        var tableToExcel = (function() {
            var uri = 'data:application/vnd.ms-excel;base64,'
                , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body><table>{table}</table></body></html>'
                , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
                , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
            return function(table, name) {
                let id_table = table
                if (!table.nodeType) table = document.getElementById(table)
                 // delete input
                var contentTable = table.innerHTML
                /*if(id_table == 'table_bonificacion'){
                    var cont = 0
                    while(contentTable.search('<input') > 0){
                        var input_start = contentTable.search('<input')
                        var input_end = contentTable.search('end=""></td>')

                        var input_comp = contentTable.substring(input_start, input_end + 7)
                        var value =  $(input_comp).val()
                        contentTable = contentTable.replace(input_comp, value)
                    }
                }*/
                var ctx = {worksheet: name || 'Worksheet', table: contentTable}
                window.location.href = uri + base64(format(template, ctx))
            }
        })()
        tableToExcel(table_id, title);
    }

    var load = function(){
        $http.post('phrapi/mario/merma/last').then(r => {
            $scope.calidad.params.idFinca = r.data.finca
            $scope.calidad.params.fecha_inicial = r.data.fecha
            $scope.calidad.params.fecha_final = r.data.fecha

            document.getElementById('date-picker').innerHTML = `${r.data.fecha} - ${r.data.fecha}`

            $scope.loadExternal();
        })

        //$scope.loadExternal()
    }
    load()

}]);