import echarts from 'echarts'


// /*==============================================
// 	=            Modificacion del Alert            =
// 	==============================================*/
window.old_alert = window.alert; 
window.delay = 1000;
window.alert = function(msj , categoria , typeMsj , callback){
	var titulo = typeof categoria == "undefined" ? "Notificacion" : categoria;
	var method = typeof typeMsj == "undefined" ? "error" : typeMsj;
	// var position = method == "error" ? "toast-top-full-width" : "toast-top-right";
	var functionCallback = typeof callback == "undefined" ? null : callback;
	var delay = window.delay;
	toastr.options = {
		"closeButton": true,
		"positionClass": "toast-top-full-width",
		"onclick": functionCallback,
		"showDuration": "1000",
		"hideDuration": delay,
		"timeOut": "5000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
	}

	toastr[method](msj, titulo);

	if(callback){
		setTimeout(callback, delay);
	}
}		
	
// 	/*=====  End of Modificacion del Alert  ======*/

var app = angular.module('procesosiq', []);

String.prototype.replaceAll = function(search, replacement) {
    let target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};

app.filter('replaceAll', () => {
	return (input, search, replacement) => {
		return input.replaceAll(search, replacement)
	}
})

app.filter('startFrom', function() {
    return function(input, start) {
        start = +start; //parse to int
        return (input || []).slice(start);
    }
});

//////// REVISAR SI PROCESOS IQ ESTA ONLINE
app.run(function($window, $rootScope) {
    $rootScope.online = navigator.onLine;
    $window.addEventListener("offline", function () {
		$rootScope.$apply(function() {
			alert("Se perdio la conexion a Internet" , "Procesos IQ");
		});
    }, false);
    $window.addEventListener("online", function () {
        $rootScope.$apply(function() {
          	// alert("Se perdio la conexion a Internet" , "PROCESOS IQ")
        });
    }, false);
});
//////// REVISAR SI PROCESOS IQ ESTA ONLINE

var load = {
	block : function(tableList){
		var config = (tableList && tableList != "") ? { target: '#' + tableList, animate: true } : { animate: true };
		App.blockUI(config);
	},
	unblock: function(tableList) { 
		var config = (tableList && tableList != "") ? '#' + tableList : '';
		App.unblockUI(config) 
	}
};

/////////// PLUGINS JQUERY
$.fn.serializeObject = function() {
	var o = {};
	var a = this.serializeArray();
	$.each(a, function() {
		if (o[this.name] !== undefined) {
			if (!o[this.name].push) {
				o[this.name] = [o[this.name]];
			}
			o[this.name].push(this.value || '');
		} else {
			o[this.name] = this.value || '';
		}
	});
	return o;
};

function ahttp (){
	var _this = this;
	_this = {
	url : "",
	method : "",
	header : {},
	data : {},
	callback : "" ,
	required : function(){
		if(!this.url) throw Error("url es Requida");
		if(!this.method) throw Error("Metodo es Requida");
		if(!this.dataType) this.dataType = 'json';
	},
	block : function(){
		load.block(ahttp.target);
	},
	unblock: function() { load.unblock(ahttp.target);  }
	}

	function call(url , method , callback,  data, header , dataType){
		_this.url = url;
		_this.method = method;
		_this.header = header;
		_this.data = data;
		_this.callback = callback;
		_this.required();
		//_this.block();
		var request = $.ajax({
			url: _this.url,
			method: _this.method ,
			data: _this.data,
			dataType: _this.dataType
		});

		request.success(function( r ) {
			// console.log(r);
			callback(r , _this.unblock);
		});

		request.fail(function( jqXHR, textStatus ) {
			load.unblock(ahttp.target);
			// console.log(jqXHR);
		});
	}

	ahttp.target  = "";

	ahttp.prototype.get = function(url , callback, data , header , dataType){
		return call(url , "get" , callback, data , header , dataType)
	}
	ahttp.prototype.post = function(url , callback, data , header , dataType){
		return call(url , "post" , callback, data , header , dataType)
	}
	ahttp.prototype.put = function(url , callback, data , header , dataType){
		return call(url , "put" , callback, data , header , dataType)
	}
	ahttp.prototype.delete = function(url , callback, data , header ,dataType){
		return call(url , "delete" , callback, data , header , dataType)
	}
};


////////// FACTORY PARA LAS PETICIONES HTTP
////////// FACTORY PARA LAS PETICIONES HTTP
app.service('client', function ($http) {
	var _this = this;
	_this.debug = false;
	_this.params = {
		url : "",
		method : "",
		header : {},
		data : {},
		callback : "" ,
		target : "",
		required : function(){
			if(!this.url) throw Error("url is Required");
			if(!this.method) throw Error("Metodo is Required");
			if(!typeof this.callback == 'function') throw Error("Callback is Required");
		},
	block : function(target){
		load.block(target);
	},
	unblock: function(target) { load.unblock(target); }
	}

	function call(url , method , callback,  data, target,  header){
		_this.params.url = url;
		_this.params.method = method;
		_this.params.header = header;
		_this.params.data = data;
		_this.params.target = target;
		_this.params.callback = callback;
		_this.params.required();
		_this.params.block(target);

		var dataConfig = {};
		dataConfig.url = _this.params.url;
		dataConfig.method = _this.params.method;
		dataConfig.header = _this.params.header;
		if(dataConfig.method == "get" ){
			dataConfig.params = _this.params.data;
		}else{
			dataConfig.data = _this.params.data;
		}
		$http({
			url : dataConfig.url  , 
			method : dataConfig.method ,
			async: true,
			headers : dataConfig.header,
			data : dataConfig.data,
			params : dataConfig.params
		}).then(function(r){
			callback(r.data , _this.params.unblock , _this.params.target);
		}, function(result){
			CallbackError( _this.params.unblock , result);
		})
	};

	function CallbackError(block , error){
		block(_this.params.target);
		if(_this.debug){
			if(error.status == 500 ){
				throw new Error(error.statusText);
			}
		}
	}

	_this.get = function(url , callback, data , target ,header){
		if(angular.isObject(data)){

		}
		return call(url , "get" , callback, data , target ,header)
	}

	_this.post = function(url , callback, data , target ,header){
		return call(url , "post" , callback, data , target ,header)
	}

	_this.put = function(url , callback, data , target ,header){
		return call(url , "put" , callback, data , target ,header)
	}

	_this.delete = function(url , callback, data , target ,header){
		return call(url , "delete" , callback, data , target ,header)
	}

	return _this;

})
////////// FACTORY PARA LAS PETICIONES HTTP
////////// FACTORY PARA LAS PETICIONES HTTP


if($('.date-picker').length > 0){
    $('.date-picker').datepicker({
        rtl: App.isRTL(),
        autoclose: true
    });
}

function echartsPlv(){
	var self = this;
	self = {
		echarts : echarts,
		callback : function(){},
		theme : "",
		id : "",
		options : "",
		required : function(){
			if(!(typeof echarts === 'object')) throw Error("Echarts Library is Required");
			if(!this.id) throw Error("ID is Required");
			if(!this.options) throw Error("Options is Required");
		},
		chart : {}
	} 

	function call(options , id, callback , theme){
		self.id = id;
		self.theme = theme || "infographic";
		self.options = options;
		self.callback = callback || function(){};
		self.required();
		var charts = self.echarts.init(document.getElementById(self.id) , self.theme);
		charts.setOption(self.options);
		self.chart = charts
		// console.log(charts);

		window.addEventListener("orientationchange", () => {
			setTimeout(() => {
				charts.resize()
			}, 200)
		})
		$(window).resize(function(){

			if(self.options.title.text == "Paises"){
				console.log("rezise")
				if( window.innerWidth < 700 &&  self.options.grid.bottom != "30%"){
					self.options.grid.bottom = "30%";
					console.log("risize 30%")
					self.echarts.reload(self.options, self.theme);
				}
				if (window.innerHeight >= 700 && window.innerWidth < 1200 && self.options.grid.bottom != "20%") {
					console.log("risize 20%")
					self.options.grid.bottom = "20%";
					self.echarts.reload(self.options, self.theme);
				} 
				if (window.innerHeight >= 1200 && self.options.grid.bottom != "10%"){
					console.log("risize 10%")
					self.options.grid.bottom = "10%";
					self.echarts.reload(self.options, self.theme);
				}
			}
			charts.resize();
		})
		return charts;
	}
	
	echartsPlv.prototype.init = function(id , options , callback , theme){
		if(options.title)
		if(options.title.text == "Paises"){
			options.grid.bottom = "10%";			
		}
		return call(options , id , callback , theme);
	}
	echartsPlv.prototype.reload = function(option , theme){
		option || self.options;
		theme = theme || self.theme;
		console.log("reload")
		console.log(self.options.grid.bottom)
		return call(self.id , self.options , self.callback , self.theme);
	}
}

window.load = load
window.app = app
window.echartsPlv = echartsPlv
window.ahttp = new ahttp()