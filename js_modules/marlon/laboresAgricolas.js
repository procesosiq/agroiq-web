app.service('request', ['$http', ($http) => {
    var service = {}

    service.last = (callback, params) => {
        load.block('grafica_tendecia')
        $http.post('phrapi/marlon/laboresAgricolas/last', params ||  {}).then((r) => {
            load.unblock('grafica_tendecia')
            callback(r.data)
        })
    }

    service.tendencia = (callback, params) => {
        load.block('grafica_tendecia')
        $http.post('phrapi/marlon/laboresAgricolas/tendencia', params ||  {}).then((r) => {
            load.unblock('grafica_tendecia')
            callback(r.data)
        })
    }

    service.tablaHistorica = (callback, params) => {
        load.block('tablaHistorica')
        $http.post('phrapi/marlon/laboresAgricolas/tablaHistorica', params ||  {}).then((r) => {
            load.unblock('tablaHistorica')
            callback(r.data)
        })
    }

    service.graficaLabores = (callback, params) => {
        load.block('labores')
        $http.post('phrapi/marlon/laboresAgricolas/graficaLabores', params ||  {}).then((r) => {
            load.unblock('labores')
            callback(r.data)
        })
    }

    service.fotos = (callback, params) => {
        load.block('accordion1')
        $http.post('phrapi/marlon/laboresAgricolas/fotos', params ||  {})
        .then((r) => {
            load.unblock('accordion1')
            callback(r.data)
        })
        .catch(() => load.unblock('accordion1'))
    }

    return service
}])

app.filter('orderObjectBy', function() {
	return function(items, field, reverse) {
    	var filtered = [];
    	angular.forEach(items, function(item) {
            if(field == 'hora' || field == 'fecha'){
                item.date = moment(item.fecha  + ' ' + item.hora)
            }
    		if(!isNaN(parseInt(item))){
    			item = parseInt(item);
    		}
      		filtered.push(item);
    	});
    	filtered.sort(function (a, b) {
            if(field == 'hora' || field == 'fecha'){
                return moment(a.date).isAfter(b.date) ? 1 : -1;
            }else if(parseFloat(a[field]) && parseFloat(b[field])){
				return (parseFloat(a[field]) > parseFloat(b[field]) ? 1 : -1);
			}else{
				return (a[field] > b[field] ? 1 : -1);
			}
    	});
    	if(reverse) filtered.reverse();
    	return filtered;
  	};
});

function getOptionsGraficaReact(id, options){
    let newOptions = {
        series: options.series,
        legend: options.legends,
        umbral: null,
        id: id,
        type : 'line',
        min : 'dataMin',
        max : null,
    }
    return newOptions
}

function initGrafica(id, options){
    setTimeout(() => {
        let data = getOptionsGraficaReact(id, options)
        let parent = $("#"+id).parent()
        parent.empty()
        parent.append(`<div id="${id}" class="charts-500"></div>`)
        ReactDOM.render(React.createElement(Historica, data), document.getElementById(id));
    }, 250)
}

function initPastel(id, series){
    let legends = []
    let newSeries = []
    series.map(value => {
        newSeries.push({
            label : value.name,
            value : parseFloat(value.value)
        })
        if(legends.indexOf(value.name) != -1) legends.push(value.name);
    })
    setTimeout(() => {
        let data = {
            data : newSeries,
            nameseries : "Pastel",
            legend : legends,
            titulo : "",
            id : id
        }
        let parent = $("#"+id).parent()
        parent.empty()
        parent.append(`<div id="${id}" class="charts-400"></div>`)
        ReactDOM.render(React.createElement(Pastel, data), document.getElementById(id));
    }, 250)
}

app.controller('controller', ['$scope', 'request', function($scope, $request){

    $scope.filters = {
        anio : `${moment().year()}`
    }
    $scope.anios = []
    
    $scope.checks = function(data, semana){
        var porcentaje = parseFloat(data['SEM '+semana])
        if(!isNaN(porcentaje) && porcentaje > 0){
            if(porcentaje > parseFloat($scope.umbrales.yellow_umbral_2))
                return 'bg-green-jungle bg-font-green-jungle';
            else if (porcentaje < parseFloat($scope.umbrales.yellow_umbral_1))
                return 'bg-red-thunderbird bg-font-red-thunderbird';
            else
                return 'bg-yellow-lemon bg-font-yellow-lemon';
        }
        return ''
	}

    const responseGraficaTendencia = (r) => {
        initGrafica('grafica_tendencia', r.week)
    }
    const initGraficaTendencia = () => {
        $request.tendencia(responseGraficaTendencia, $scope.filters)
    }

    const responseTablaHistorica = (r) => {
        $scope.umbrales = r.umbrals || {}
        $scope.semanas = r.semanas || []
        $scope.tablaHistorica = r.data || []
    }
    const initTablaHistorica = () => {
        $request.tablaHistorica(responseTablaHistorica, $scope.filters)
    }

    const initGraficaLabores = () => {
        $request.graficaLabores(responseGraficaLabores, $scope.filters)
    }
    const responseGraficaLabores = (r) => {
        initPastel('grafica_labores', r.main)
        $scope.labores = r.labores
        $scope.filters.labor = Object.keys($scope.labores)[0]
        $scope.changeLaborGrafica()
    }

    const requestFotos = () => {
        $request.fotos((r) => {
            $scope.fotos = r.fotos
        }, $scope.filters)
    }

    $scope.changeLaborGrafica = () => {
        initPastel('grafica_causas', $scope.labores[$scope.filters.labor])
    }

    $scope.init = () => {
        initGraficaTendencia()
        initGraficaLabores()
        requestFotos()
        initTablaHistorica()
    }

    $scope.fnExcelReport = function(id_table, title)
    {
        /*var tab_text="<table border='2px'><tr bgcolor='#87AFC6'>";
        var textRange; var j=0;
        tab = document.getElementById(id_table); // id of table

        for(j = 0 ; j < tab.rows.length ; j++) 
        {     
            tab_text=tab_text+tab.rows[j].innerHTML+"</tr>";
            //tab_text=tab_text+"</tr>";
        }

        tab_text=tab_text+"</table>";
        window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));*/
        var data = new Blob([document.getElementById(id_table).outerHTML], {
            type : 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf8'
        })
        saveAs(data, title+'.xls')
    }

    $scope.last = () => {
        return new Promise((resolve) => {
            $request.last((r) => {
                $scope.anios = r.anios
                resolve()
            })
        })
    }

    $scope.last().then(() => {
        $scope.init()
    })
}]);

