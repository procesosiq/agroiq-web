app.service('request', ['$http', ($http) => {
    var service = {}

    service.last = (callback, params) => {
        load.block()
        $http.post('phrapi/mario/laboresAgricolasDia/last', params || {}).then((r) => {
            load.unblock()
            callback(r.data)
        })
    }

    service.index = (callback, params) => {
        load.block()
        $http.post('phrapi/mario/laboresAgricolasDia/index', params || {}).then((r) => {
            load.unblock()
            callback(r.data)
        })
    }

    service.pieCausas = (callback, params) => {
        load.block('pie-causas-container')
        $http.post('phrapi/mario/laboresAgricolasDia/pieCausas', params || {}).then((r) => {
            load.unblock('pie-causas-container')
            callback(r.data)
        })
    }

    return service
}])

function initPastel(id, series){
    var legends = []
    var newSeries = []
    Object.keys(series).map(key => {
        newSeries.push({
            label : series[key].label,
            value : parseFloat(series[key].value)
        })
        if(legends.indexOf(series[key]) != -1) legends.push(series[key]);
    })
    setTimeout(() => {
        data = {
            data : newSeries,
            nameseries : "Pastel",
            legend : legends,
            titulo : "",
            id : id
        }
        let parent = $("#"+id).parent()
        parent.empty()
        parent.append(`<div id="${id}" class="chart"></div>`)
        ReactDOM.render(React.createElement(Pastel, data), document.getElementById(id));
    }, 250)
}

app.controller('controller', ['$scope', 'request', function($scope, $request){

    $scope.filters = {
        fecha : '',
        pieCausa : {
        }
    }
    $scope.pieCausa = {
        labores : [],
        lotes : []
    }

    $scope.last = () => {
        $request.last((r) => {
            $scope.filters.fecha = r.fecha

            $scope.index()
        })
    }

    $scope.index = () => {
        $request.index((r) => {
            $scope.muestras = new Array(r.tablaResumen.muestras)
            $scope.tabla_resumen = r.tablaResumen.tabla_labores_defectos

            responsePieCausas(r.pieCausas)
        }, $scope.filters)
    }

    $scope.clearPieCausa = () => {
        $scope.filters.pieCausa = {}
        $scope.refreshPieCausas()
    }

    $scope.refreshPieCausas = () => {
        $request.pieCausas((r) => {
            responsePieCausas(r)
        }, $scope.filters)
    }

    responsePieCausas = ({ data, labores, lotes }) => {
        $scope.pieCausa.labores = labores
        $scope.pieCausa.lotes = lotes
        renderPieCausas(data)
    }

    renderPieCausas = (data) => {
        initPastel('pie-causas', data)
    }

    $scope.last()

}]);

