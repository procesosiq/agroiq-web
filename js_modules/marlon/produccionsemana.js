function dlCanvas(nombre , mode ) {
    var self = this;
    // $("#inicio")[0]
    html2canvas($("#contenedor"), {
      onrendered: function(canvas) {
        var dt = canvas.toDataURL('image/png');
        /* Change MIME type to trick the browser to downlaod the file instead of displaying it */
        // dt = dt.replace(/^data:image\/[^;]*/, 'data:application/octet-stream');

        /* In addition to <a>'s "download" attribute, you can define HTTP-style headers */
        // dt = dt.replace(/^data:application\/octet-stream/, 'data:application/octet-stream;headers=Content-Disposition%3A%20attachment%3B%20filename=Canvas.png');
        if(mode == "pdf"){
            var doc = new jsPDF('p', 'mm');
            doc.addImage(dt, 'PNG',5, 5, 195, 290);
            doc.save(nombre+'.pdf');
        }else{
            var a  = document.createElement('a');
            a.href = dt;
            a.download = nombre + '.png';

            a.click()
        }
      }
    });
};

app.filter('orderObjectBy', function() {
    return function(items, field, reverse) {
        var filtered = [];
        angular.forEach(items, function(item) {
            filtered.push(item);
        });
        filtered.sort(function (a, b) {
            if(parseFloat(a[field]) && parseFloat(b[field]))
                return (parseFloat(a[field]) > parseFloat(b[field]) ? 1 : -1);
            else
                return (a[field] > b[field] ? 1 : -1);
        });
        if(reverse) filtered.reverse();
        return filtered;
    };
});

app.filter('orderByCajas', function() {
    return function(items, field) {
        var filtered = [];
        angular.forEach(items, function(item) {
            filtered.push(item);
        });
        filtered.sort(function (a, b) {
            if(a[field] == 'MI COMISARIATO') return 1;
            if(b[field] == 'MI COMISARIATO') return -1;
            return (a[field] > b[field] ? 1 : -1);
        });
        
        return filtered;
    };
});

app.service('request', [ '$http', ($http) => {

    var service = {}

    service.last = (callback, params) => {
        $http.post('phrapi/quintana/produccionsemana/last', params || {}).then(r => {
            callback(r.data)
        })
    }

    service.tags = (callback, params) => {
        $http.post('phrapi/quintana/produccionsemana/tags', params || {}).then(r => {
            callback(r.data)
        })
    }

    service.graficaPromEdad = (callback, params) => {
        load.block('edad_promedio')
        $http.post('phrapi/quintana/produccionsemana/graficaEdadPromedio', params || {}).then(r => {
            load.unblock('edad_promedio')
            callback(r.data)
        })
    }

    service.resumenAcumulado = (callback, params) => {
        load.block('resumen_acumulado')
        $http.post('phrapi/quintana/produccionsemana/resumenAcumulados', params || {}).then(r => {
            load.unblock('resumen_acumulado')
            callback(r.data)
        })
    }

    service.reporteProduccion = (callback, params) => {
        load.block('reporte_produccion')
        $http.post('phrapi/quintana/produccionsemana/reporteProduccion', params || {}).then(r => {
            load.unblock('reporte_produccion')
            callback(r.data)
        })
    }

    service.graficaVariables = (callback, params) => {
        load.block('graficas')
        $http.post('phrapi/quintana/produccionsemana/graficaVariables', params || {}).then(r => {
            load.unblock('graficas')
            callback(r.data)
        })
    }

    return service
}])

app.controller('produccion', ['$scope','client', 'request', function($scope, client, $request){
    cambiosLotes = function(){
        $scope.init();
    }
    $scope.type = "cosechados";
    $scope.interval = {};
    cambiarCosecha = function(){
        clearInterval($scope.interval);
        $scope.init();
    }

    $scope.variables = {
        cajas : [
            "Cajas"
        ],
        clima : [
            "Horas Luz (100)",
            "Horas Luz (150)",
            "Horas Luz (200)",
            "Horas Luz (400)",
            "Humedad",
            "Lluvia",
            "Rad. Solar",
            "Temp Max.",
            "Temp Min.",
        ],
        merma : [
            "Merma Neta",
            "Tallo",
            "Cosecha",
            "Enfunde",
            "ADM",
            "Natural",
            "Proceso"
        ],
        racimos : [
            "Calibre",
            "Edad",
            "Manos",
            "Peso",
            "Racimos Procesados",
            "Racimos Cosechados",
            "Ratio Cortado",
            "Ratio Procesado"
        ],
    };

    $scope.printPdf = function(){
        dlCanvas("Reporte Racimos" , "pdf");
    }

    $scope.printNormal = function(){
        dlCanvas("Reporte Racimos" , "print");
    }

    $scope.id_company = 0;
    $scope.filters = {
        idFinca : 2,
        idLote : 0,
        idLabor : 0,
        fecha_inicial : moment().startOf('month').format('YYYY-MM-DD'),
        fecha_final : moment().endOf('month').format('YYYY-MM-DD'),
        cliente: "",
        marca: "",
        palanca : "",
        finca : 2,
        type : "cosechados",
        year : moment().format('YYYY'),
        var1 : "Peso",
        var2 : "Calibre",
        type1 : "line",
        type2 : "line"
    }

    $scope.StartEndDateDirectives = {
        startDate : moment().startOf('month'),
        endDate :moment().endOf('month'),
    }

    $scope.changeRangeDate = function(data){
        if(data){
            $scope.filters.fecha_inicial = data.hasOwnProperty("first_date") ? data.first_date : $scope.filters.fecha_inicial;
            $scope.filters.fecha_final = data.hasOwnProperty("second_date") ? data.second_date : $scope.filters.fecha_final;
        }
        $scope.init()
    }

    $scope.tabla = {
        produccion : [],
        resumen : []
    }

    $scope.openDetalle = function(data){
        if(data.campo != "MAX" && data.campo != "MIN" && data.campo != "AVG")
            data.expanded = !data.expanded;
    }

    $scope.openDetalleLote = function(data){
        data.expanded = !data.expanded;
    }

    $scope.sorterFunc = function(semana){
        return parseInt(semana.campo);
    };

    $scope.colores = [];
    $scope.totales = [];
    $scope.palancas = [];
    $scope.lotes = [];
    $scope.visibleColumns = 7;
    $scope.withTable = (100 / $scope.visibleColumns) + "%";

    $scope.tags = {
        racimo : {
            value : 0
        },
        ratio_cortado : {
            value : 0
        },
        ratio_procesado : {
            value : 0
        },
        ratooning : {
            value : 0
        },
        merma_cortada : {
            value : 0
        },
        merma_procesada : {
            value : 0
        },
        edad : {
            value : 0
        },
        recusados : {
            value : 0
        },
        cajas : {
            value :0
        },
        otras_cajas : {
            value : 0
        },
        calibracion : {
            value : 0
        },
        merma_cosechada : {
            value : 0
        },
        enfunde : {
            value : 0
        },
        recobro : {
            value : 0
        },
        merma_neta : {
            value : 0
        },
        merma_cajas : {
            value : 0
        },
        merma_dolares : {
            value : 0
        },
        merma_kg : {
            value : 0
        }
    }

    $scope.charts = {
        variables : new echartsPlv(),
    }    

    var printTags = (r) => {
        $scope.tags.cajas.value = r.data.peso_prom_cajas.value;
        $scope.tags.calibracion.value = r.data.calibracion_prom.value;
        $scope.tags.racimo.value = r.data.peso_prom_racimos.value;
        $scope.tags.edad.value = r.data.edad_prom_racimos.value;
        $scope.tags.ratio_cortado.value = r.data.ratio_cortado.value;
        $scope.tags.ratio_procesado.value = r.data.ratio_procesado.value;
        $scope.tags.merma_cosechada.value = r.data.porc_merma_cosechada.value;
        $scope.tags.merma_procesada.value = r.data.porc_merma_procesado.value;
        $scope.tags.enfunde.value = r.data.enfunde_ha.value;
        $scope.tags.merma_neta.value = r.data.porc_merma_neta.value;
        $scope.tags.merma_kg.value = r.data.merma_kg.value;
        $scope.tags.merma_cajas.value = r.data.merma_cajas.value;
        $scope.tags.merma_dolares.value = r.data.merma_dolares.value;

        setTimeout(function(){
            $(".counter_tags").counterUp({
                delay: 10,
                time: 1000
            });

            $scope.num_semanas = ($scope.filters.year == moment().format('YYYY')) ? moment().week() : 52;
        }, 250)
    }

    var printAcumulado = (r) => {
        $scope.resumen = r.resumen
    }

    $scope.init = function(){
        var data = $scope.filters;
        $request.tags(printTags, data)
        $request.resumenAcumulado(printAcumulado, data)
        //client.post('phrapi/quintana/produccionsemana/index', $scope.printDetails, data, 'graficas')
        $scope.initGraficaEdadProm()
        $scope.initGraficaVariables()
        $scope.initTableReporteProduccion()
    }

    $scope.changeFinca = () => {
        $scope.init()
    }

    $scope.table = {
        orderBy : 'semana',
        reverse : false
    }
    $scope.setOrder = (column) => {
        if($scope.table.orderBy != column){
            $scope.table.orderBy = column
            $scope.table.reverse = false
        } else {
            $scope.table.reverse = !$scope.table.reverse
        }
    }

    $scope.start = true;
    $scope.cajas = 0;

    function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    var printGraficaEdad = (r) => {
        
        var data = {
            series: r.chart.data,
            legend: r.chart.legend,
            umbral: r.chart.umbral,
            id: "edad_promedio",
            legendBottom : true,
            zoom : false,
            type : 'line',
            min : 'dataMin'
        }
        ReactDOM.render(React.createElement(Historica, data), document.getElementById('edad_promedio'));
    }

    $scope.initGraficaEdadProm = () => {
        $request.graficaPromEdad(printGraficaEdad, $scope.filters)
    }

    var printTableReporteProduccion = (r) => {
        $scope.reporteProduccion = r.data
    }

    $scope.initTableReporteProduccion = () => {
        $request.reporteProduccion(printTableReporteProduccion, $scope.filters)
    }

    var data_variables = {}
    var printGraficaVariables = (r) => {
        data_variables = r.data
        $scope.charts.variables.init('variables', r.data)
    }

    $scope.initGraficaVariables = () => {
        $request.graficaVariables(printGraficaVariables, $scope.filters)
    }

    $scope.selected = (val) => {
        return [$scope.filters.var1, $scope.filters.var2].indexOf(val) > -1;
    }

    $scope.toggleLineBar = (num, type) => {
        if(num == 1 && data_variables.series[0].name.toUpperCase() == $scope.filters.var1.toUpperCase()) {
            data_variables.series[0].type = type
            $scope.filters.type1 = type
        }else if(num == 1){
            data_variables.series[1].type = type
            $scope.filters.type1 = type
        }

        if(num == 2 && data_variables.series[0].name.toUpperCase() == $scope.filters.var2.toUpperCase()) {
            data_variables.series[0].type = type
            $scope.filters.type2 = type
        }else if(num == 2){
            data_variables.series[1].type = type
            $scope.filters.type2 = type
        }
        printGraficaVariables({data : data_variables})
    }

    $scope.last = () => {
        $request.last((r) => {
            if(Object.keys(r.fincas).length > 0)
            if(Object.keys(r.fincas).indexOf($scope.filters.finca) == -1){
                $scope.filters.finca = Object.keys(r.fincas)[0]
            }
            $scope.filters.fecha_inicial = r.fecha
            $scope.filters.fecha_final = r.fecha
            $("#date-picker").html(`${r.fecha} -  ${r.fecha}`)
            $scope.init()
        })
    }

}]);

