app.service('request', ['$http', function($http){
    this.getFilters = (callback, params) => {
        load.block('filters')
        $http.post('phrapi/marun/cacao/filters', params || {}).then(r => {
            load.unblock('filters')
            if(callback) callback(r.data)
        })
    }
    this.getLast = (callback, params) => {
        $http.post('phrapi/marun/cacao/last', params || {}).then(r => {
            if(callback) callback(r.data)
        })
    }
    this.getDataAnalisis = (callback, params) => {
        $http.post('phrapi/marun/cacao/analisis', params || {}).then(r => {
            if(callback) callback(r.data)
        })
    }
}])

app.filter('sumOfValue', function () {
    return function (data, key) {        
        if (angular.isUndefined(data) || angular.isUndefined(key))
            return 0;        
        var sum = 0;
        angular.forEach(data,function(value){
            if(value[key] != "" && value[key] != undefined && parseFloat(value[key])){
                sum = sum + parseFloat(value[key], 10);
            }
        });
        return sum;
    }
})

app.controller('controller', ['$scope', 'request', function($scope, $request){

    $scope.filters = {
        codigo : '',
        finca : 'San José',
        fecha  : ''
    }
    const getOptions = (data) => {
        var option = {
            series : [{
                name  : 'Sensores',
                value : []
            }],
            legend : [],
            id : 'grafica-radar'
        }
        data.map((item) => {
            option.legend.push({
                name : item.name,
                max : item.max || null
            })
            option.series[0].value.push(item.value)
        })
        return option
    }

    const renderGraficaRadar = (options) => {
        ReactDOM.render(React.createElement(Radar, options), document.getElementById('grafica-radar'))
    }

    $scope.reloadFilters = () => {
        $request.getFilters((r) => {
            $scope.responsables = r.responsables
            if(r.responsables.indexOf($scope.filters.responsable) == -1) $scope.filters.responsable = ''
            $scope.fechas = r.fechas
            if(r.fechas.indexOf($scope.filters.fecha) == -1) $scope.filters.fecha = ''
            $scope.codigos = r.codigos
            if(r.codigos.indexOf($scope.filters.codigo) == -1) $scope.filters.codigo = ''
            $scope.fincas = r.fincas
            if(r.fincas.indexOf($scope.filters.finca) == -1) $scope.filters.finca = ''

            //$scope.getDataAnalisis()
        }, $scope.filters)
    }

    $scope.getDataAnalisis = () => {
        if($scope.filters.codigo != ''){
            $request.getDataAnalisis((r) => {
                $scope.data = r.data
                $scope.filters.responsable = r.row.responsable
                $scope.filters.fecha = r.row.fecha
                $scope.reloadFilters()
                $scope.imagenes = r.imagenes || []
                $scope.observaciones = r.row.observaciones || ''
                renderGraficaRadar(getOptions(r.data))
            }, $scope.filters)
        }
    }

    $scope.init = () => {
        $request.getLast((r) => {
            $scope.filters.codigo = r.row.codigo
            $scope.filters.fecha = r.row.fecha
            $scope.filters.responsable = r.row.responsable
            $scope.data = r.data
            $scope.imagenes = r.row.imagenes || []
            $scope.observaciones = r.row.observaciones || ''
            renderGraficaRadar(getOptions(r.data))
            $scope.reloadFilters()
        })
    }
    $scope.init()
}]);

