app.service('request', ['$http', function($http){
    this.loadData = (callback, params) => {        
        load.block()
        $http.post('phrapi/marun/balanzaMallas/data', params || {}).then(r => {
            load.unblock()
            if(callback) callback(r.data)
        })
    }

    this.save = (callback, params) => {
        $http.post('phrapi/marun/balanzaMallas/save', params || {}).then(r => {
            if(callback) callback(r.data)
        })
    }
}])

app.controller('controller', ['$scope', 'request', function($scope, $request){

    $scope.StartEndDateDirectives = {
        startDate : moment(),
        endDate :moment(),
    }
    $scope.filters = {
        unidad : 'LB',
        fecha_inicial : moment().format('YYYY-MM-DD'),
        fecha_final : moment().format('YYYY-MM-DD'),
    }

    $scope.changeRangeDate = (data = {}) => {
        if(data){
            $scope.filters.fecha_inicial = data.first_date
            $scope.filters.fecha_final = data.second_date
            $scope.init()
        }
    }

    const save = (id, campo, valor) => {
        if(id > 0){
            $request.save((r) => {
                if(r.status == 200){
                    alert("Se guardo correctamente", "", "success")
                }else{
                    alert("Hubo un error favor de intentar mas tarde")
                }
            }, {
                id : id, campo : campo, valor : valor
            })
        }
    }

    const renderTableData = (data) => {
        var props = {
            header : [ {
                    key : 'fecha',
                    name : 'FECHA',
                    sortable : true,
                    titleClass : 'text-center',
                    alignContent : 'center',
                    filterable : true,
                    resizable : true,
                    width : 100,
                },{
                    key : 'finca',
                    name : 'FINCA',
                    titleClass : 'text-center',
                    sortable : true,
                    alignContent : 'center',
                    filterable : true,
                    resizable : true,
                },{
                    key : 'codigo',
                    name : 'CODIGO',
                    titleClass : 'text-center',
                    sortable : true,
                    alignContent : 'center',
                    filterable : true,
                    resizable : true,
                    editable : true,
                    events: {
                        onKeyDown: function(ev, column) {
                            if (ev.key === 'Enter') {
                                let index = parseInt(column.rowIdx) - 1
                                let key = column.column.key
                                let row = $scope.table1.rowGetter(index)
                                save(row.id, key, row[key].toString().toUpperCase())
                            }
                        },
                    }
                },{
                    key : 'mallas',
                    name : 'MALLAS',
                    titleClass : 'text-center',
                    sortable : true,
                    alignContent : 'center',
                    filterable : true,
                    resizable : true,
                    width : 150,
                },{
                    key : 'total_peso',
                    name : 'PESO TOTAL',
                    titleClass : 'text-center',
                    sortable : true,
                    alignContent : 'center',
                    filterable : true,
                    resizable : true,
                    width : 100,
                }
            ],
            data : data,
            buttons : [
                {
                    title : 'Excel',
                    action : () => {
                        $scope.table1.exportToExcel()
                    },
                    className : ''
                }
            ]
        }
        $("#table-data").html("")
        $scope.table1 = ReactDOM.render(React.createElement(ReactDataGrid, props), document.getElementById('table-data'))
    }

    $scope.init = () => {
        $request.loadData((r) => {
            renderTableData(r.data)
        }, $scope.filters)
    }
    $scope.init()
}]);

