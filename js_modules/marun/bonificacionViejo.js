app.filter('order', function(){
    return function(items){
        var filtered = [];
        angular.forEach(items, function(item) {
            filtered.push(item)
        })
        filtered.sort(function (a, b) {
            if(parseFloat(a) && parseFloat(b)){
                return (parseFloat(a) > parseFloat(b) ? 1 : -1);    
            }
            return (a > b ? 1 : -1);
        });
        //if(true) filtered.reverse();
        return filtered;
    }
})

app.filter('orderObjectBy', function() {
  return function(items, field, reverse) {
    var filtered = [];
    angular.forEach(items, function(item) {
        // console.log(item)
        if(item.hasOwnProperty("lote")){
            if(!isNaN(parseFloat(item.lote)))
                item.lote = parseFloat(item.lote);
        }
        if(item.hasOwnProperty("total")){
            item.total = parseFloat(item.total);
        }
        if(item.hasOwnProperty("exedente")){
            if(!isNaN(parseFloat(item.exedente)))
                item.exedente = parseFloat(item.exedente);
        }
        if(item.hasOwnProperty("usd")){
            if(!isNaN(parseFloat(item.usd)))
                item.usd = parseFloat(item.usd);
        }

      filtered.push(item);
    });
    filtered.sort(function (a, b) {
        if(parseFloat(a[field]) && parseFloat(b[field])){
            return (parseFloat(a[field]) > parseFloat(b[field]) ? 1 : -1);    
        }
        return (a[field] > b[field] ? 1 : -1);
    });
    if(reverse) filtered.reverse();
    return filtered;
  };
});

app.controller('bonificacion', ['$scope','$http','$interval','client','$controller','$timeout' ,'$window', function($scope,$http,$interval,client, $controller , $timeout,$window){
    $scope.configuracion = {
        title : "LOTERO AEREO",
        mode : 2,
        get : function(){
            var self = $scope.configuracion
            var callback = self.set;
            var data = {
                tipo : self.title,
                mode : self.mode
            }
           client.post("phrapi/marun/bonificacion/configuracion" , callback , data) 
        },
        set : function(r , b){
            b();
            if(r.hasOwnProperty("data")){
                $scope.configuracion.data = r.data
            }
        },
        save : function(){
            var self = $scope.configuracion
            var callback = self.print;
            var data = {
                tipo : self.title,
                mode : self.mode,
                umbral : self.data.umbral,
                bono : self.data.bono,
                descuento : self.data.descuento,
            }

            client.post("phrapi/marun/bonificacion/save/config" , callback , data) 
        },
        print : function(r , b){
            b();
            if(r.hasOwnProperty("success") && r.success.success == 200){
                alert(r.success.msj ,"Bonificacion","success" , function(){
                    $scope.init();
                    // $scope.historica.init();
                    $scope.configuracion.get()
                })
            }
        },
        data : {
            umbral : 0,
            bono : 0,
            descuento : 0,
        }
    }
    
    $scope.id_company = "<?= $this->session->id_company ?>";
    $scope.reverseSort = false;
    $scope.orderByField = "lote";
    $scope.bonificacion = {
        params : {
            idFinca : 1,
            mode : 2,
            idLabor : 0,
            semana : moment().week(),
            type : "LOTERO AEREO",
            lote : ""
        },
        step : 0,
        path : ['phrapi/marun/bonificacion/index'],
        templatePath : [],
        nocache : function(){
            $scope.init();
            $scope.historica.init();
            $scope.configuracion.get()
        }
    }

    $scope.StartEndDateDirectives = {
        startDate : moment().startOf('month'),
        endDate :moment().endOf('month'),
    }

    changeweek = function(){
        $scope.init();
    }

    $scope.title = {
        column_one : "Lote",
        column_two : "Enfundador",
    }

    cambiosFincas = function(){
        $scope.bonificacion.params.idFinca = $("#idFinca").val();
        $scope.historica.idFinca = $("#idFinca").val();
        $scope.historica.init();
        $scope.init();
    }

    changeHistorica = function(){
        $scope.historica.type = $("#type").val();
        $scope.configuracion.title = $("#type").val();
        $scope.historica.idFinca = $("#idFinca").val();
        $scope.bonificacion.params.type = $("#type").val();
        $scope.bonificacion.params.idFinca = $("#idFinca").val();
        if($("#type").val() == "ENFUNDE" || $("#type").val() == "LOTERO AEREO"){
            $scope.title.column_one = "Lote";
            $scope.title.column_two = "Enfundador";
        }
        else if($("#type").val() == "COSECHA"){
            $scope.title.column_one = "Código";
            $scope.title.column_two = "Palanca";
        }
        $scope.init();
        $scope.historica.init();
        $scope.configuracion.get()
    }

    $scope.tabla = {
        bonificacion : []
    }

    $scope.openDetalle = function(data){
        data.expanded = !data.expanded;
    }

    $scope.dias = [];
    $scope.lotes = [];
    $scope.hectareas = {};
    $scope.totales = [];
    $scope.campos = [];
    $scope.semanas = [];
    $scope.fincas = [];
    $scope.colspan = 8;
    $scope.showFincas = 0;
    $scope.historico = new echartsPlv();
    $scope.historico_tabla = [];
    $scope.historica = {
        type : "LOTERO AEREO",
        idFinca : 1,
        mode : 2,
        lote : "",
        palanca : "",
        init : function(){
            var data = {
                type : this.type,
                idFinca : this.idFinca,
                mode : this.mode,
                lote : this.lote,
                palanca : this.palanca
            }
            client.post('phrapi/marun/bonificacion/historico' , $scope.printDetailsHistorica ,data , "contentHistorico");
        }
    };
    $scope.init = function(){
        var data = $scope.bonificacion.params;
        client.post($scope.bonificacion.path , $scope.printDetails ,data , "contentTable")
    }

    $scope.saveUmbrals = function(){
        var umbrals = $("#table_bonificacion .umbral")
        var hectareas = $("#table_bonificacion .hectareas")
        var data = {};
        var data_h = {};
        $.each(umbrals, function(index, value){
            data[$(value).attr("data-lote")] = ($(value).val())
        })
        $.each(hectareas, function(index, value){
            data_h[$(value).attr("data-lote")] = $(value).val()
        })
        client.post("phrapi/marun/bonificacion/save/umbrals", function(r, b){
            b()
            if(r){
                $("#saveUmbrals").addClass("hide");
                $scope.init();
                $scope.historica.init();
            }
        }, { umbrals : data, semana : $scope.bonificacion.params.semana, hectareas : data_h } )
    }


    $scope.start = true;
    $scope.umbral = 10;
    $scope.labelMode = "% Merma";
    $scope.statusMode = true;

    convertModeSelect = function(){
        var mode = $("#mode").val();
        $scope.historica.mode = mode;
        $scope.historica.lote = $("#lote").val()
        $scope.historica.palanca = $("#palanca").val()
        $scope.configuracion.mode = mode;
        $scope.init();
        $scope.historica.init();
        $scope.configuracion.get()
    }

    $scope.convertMode = function(){
        $scope.statusMode = !$scope.statusMode;
        $scope.labelMode = ($scope.labelMode == "Peso") ? "% Merma" : "Peso";
        $scope.historica.mode = $scope.statusMode;
        $scope.historica.lote = $("#lote").val()
        $scope.historica.palanca = $("#palanca").val()
        $scope.bonificacion.params.mode = $scope.statusMode
        $scope.init();
        $scope.historica.init();
    }

    $scope.tagsFlags = function(umbral, value){        
        var className = "fa fa-arrow-up font-red-thunderbird";
        if(!isNaN(parseFloat(value))){
            if(value <= (umbral || 0)){
                className = "fa fa-arrow-down font-green-jungle"
            }
        }else{
            className = "";
        }
        return className;
    }

    $scope.bgTagsFlags = function(umbral, value){
        var className = "bg-yellow-gold bg-font-yellow-gold";
        if(!isNaN(parseFloat(value))){
            value = parseFloat(value)
            umbral = parseFloat(umbral || 0)

            if(value == umbral){
                return className
            }else if(value < umbral ){
                className = "bg-green-haze bg-font-green-haze"
            }else if(value > umbral){
                className = "bg-red-thunderbird bg-font-red-thunderbird"
            }
        }
        return className;
    }

    $scope.printDetailsHistorica = function(r,b){
        b("contentHistorico");
        if(r){
            if(r.hasOwnProperty("historico")){
                $scope.historico.init("historico" , r.historico);
                $scope.historico_tabla = r.historico_tabla;
                $scope.semanas_historico = r.semanas_historico;
            }
        }
    }

    $scope.printDetails = function(r,b){
        b("contentTable");
        if(r){
            if(r.hasOwnProperty("titulos")){
                $scope.dias = r.titulos;
            }
            if(r.hasOwnProperty("personal")){
                $scope.personal = r.personal;
            }
            if(r.hasOwnProperty("dias")){
                $scope.campos = r.dias;
            }
            if(r.hasOwnProperty("data")){
                $scope.lotes = r.data;
            }
            if(r.hasOwnProperty("totales")){
                $scope.totales = r.totales;
            }
            if(r.hasOwnProperty("colspan")){
                $scope.colspan = r.colspan;
            }
            if(r.hasOwnProperty("semanas")){
                $scope.semanas = r.semanas;
            }
            if(r.hasOwnProperty("fincas")){
                $scope.fincas = r.fincas;
                $scope.showFincas = r.showFincas;
            }
            if($scope.start){
                $scope.start = false;
                setInterval(function(){
                    $scope.init();
                } , (60000 * 5));
            }
        }
    }

    $scope.exportExcel = function(table_id, title){
        var tableToExcel = (function() {
            var uri = 'data:application/vnd.ms-excel;base64,'
                , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body><table>{table}</table></body></html>'
                , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
                , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
            return function(table, name) {
                let id_table = table
                if (!table.nodeType) table = document.getElementById(table)
                 // delete input
                var contentTable = table.innerHTML
                if(id_table == 'table_bonificacion'){
                    var cont = 0
                    while(contentTable.search('<input') > 0){
                        var input_start = contentTable.search('<input')
                        var input_end = contentTable.search('end=""></td>')

                        var input_comp = contentTable.substring(input_start, input_end + 7)
                        var value =  $(input_comp).val()
                        contentTable = contentTable.replace(input_comp, value)
                    }
                }
                var ctx = {worksheet: name || 'Worksheet', table: contentTable}
                window.location.href = uri + base64(format(template, ctx))
            }
        })()
        tableToExcel(table_id, title);
    }

    $scope.exportPdf = function(table_id){
        //openWindowWithPostRequest(table_id);
        var table = tableToJson($('#'+table_id).get(0))
        var doc = new jsPDF('p','pt', 'a1', true);
        doc.cellInitialize();
        $.each(table, function (i, row){
            var numCell = 0
            $.each(row, function (j, cell){
                if(!cell.includes("<table")){
                    doc.cell(10, 150, ((numCell == 3) ? 400 : 100), 40, cell, i);  // 2nd parameter=top margin,1st=left margin 3rd=row cell width 4th=Row height
                }
                numCell++;
            })
        })


        doc.save('sample-file.pdf');
    }

    function tableToJson(table) {
        var data = [];

        // first row needs to be headers
        var headers = [];
        for (var i=0; i<table.rows[0].cells.length; i++) {
            headers[i] = table.rows[0].cells[i].innerHTML.toLowerCase().replace(/ /gi,'');
        }


        // go through cells
        for (var i=0; i<table.rows.length; i++) {

            var tableRow = table.rows[i];
            var rowData = {};
            for (var j=0; j<tableRow.cells.length; j++) {
                var dataCell = tableRow.cells[j].innerHTML;
                if(dataCell.includes("input")){
                    dataCell = $(dataCell).val()
                }
                if(dataCell.includes("span")){
                    dataCell = dataCell.replace('<span class="fa fa-arrow-down font-green-jungle"></span>', '');
                    dataCell = dataCell.replace('<span class="fa fa-arrow-up font-red-thunderbird"></span>', '');
                }
                rowData[ headers[j] ] = dataCell.trim();
            }
            data.push(rowData);
        }       

        return data;
    }

    function openWindowWithPostRequest(table_id) {
        var winName='PDF';
        var winURL='phrapi/export/pdf';
        var windowoption='resizable=yes,height=200,width=400,location=0,menubar=0,scrollbars=1';
        var params = { 'table' : $("#"+table_id).html() };
        var form = document.createElement("form");
        form.setAttribute("method", "post");
        form.setAttribute("action", winURL);
        form.setAttribute("target",winName); 
        for (var i in params) {
            if (params.hasOwnProperty(i)) {
                var input = document.createElement('input');
                input.type = 'hidden';
                input.name = i;
                input.value = params[i];
                form.appendChild(input);
            }
        }
        document.body.appendChild(form);
        window.open('', winName,windowoption);
        form.target = winName;
        form.submit();
        document.body.removeChild(form);
    }

}]);

$("#table_bonificacion").on('change', '.umbral', function(){
    $("#saveUmbrals").removeClass("hide");
})

$("#table_bonificacion").on('change', '.hectareas', function(){
    $("#saveUmbrals").removeClass("hide");
})