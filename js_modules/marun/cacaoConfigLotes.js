app.service('request', ['$http', function($http){
    this.index = (callback, params) => {        
        $http.post('phrapi/marun/cacaoConfig/lotes', params || {}).then(r => {
            if(callback) callback(r.data)
        })
    }
}])

const getSeries = (series) => {
    var arraySeries = []
    series.map((s, i) => {
        arraySeries.push({
            type: 'tree',        
            data: [s],
            top: '1%',
            left: '7%',
            bottom: '1%',
            right: '20%',

            symbolSize: 7,

            label: {
                normal: {
                    position: 'left',
                    verticalAlign: 'middle',
                    align: 'right',
                    fontSize: 9
                }
            },

            leaves: {
                label: {
                    normal: {
                        position: 'right',
                        verticalAlign: 'middle',
                        align: 'left'
                    }
                }
            },

            expandAndCollapse: false,
            animationDuration: 550,
            animationDurationUpdate: 750
        })
    })
    return arraySeries
}

app.controller('controller', ['$scope', 'request', function($scope, $request){

    let grafica = null
    $scope.lotes = []

    $scope.init = () => {
        $request.index((r) => {
            $scope.lotes = r.data
            renderGrafica(r.grafica)
        })
    }

    const renderGrafica = (data) => {
        let option = {
            series : getSeries(data)
        }
        if(grafica){
            grafica.clear()
        }
        grafica = new echartsPlv().init("grafica-arbol", option)
    }

    $scope.init()

}]);

