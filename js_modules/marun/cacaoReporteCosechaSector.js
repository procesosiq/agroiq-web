app.service('request', ['$http', function($http){
    this.loadData = (callback, params) => {        
        $http.post('phrapi/marun/reporteCosechaSector/data', params || {}).then(r => {
            if(callback) callback(r.data)
        })
    }

    this.chart = (callback, params) => {        
        $http.post('phrapi/marun/reporteCosechaSector/chart', params || {}).then(r => {
            if(callback) callback(r.data)
        })
    }
}])

app.controller('controller', ['$scope', 'request', function($scope, $request){

    const save = (id, campo, valor) => {
        $request.save((r) => {
            if(r.status == 200){
                alert("Se guardo correctamente", "", "success")
            }else{
                alert("Hubo un error favor de intentar mas tarde")
            }
        }, {
            id : id, campo : campo, valor : valor
        })
    }

    const renderChart = (data) => {
        let props = {
            series : data.data,
            legend : data.legend,
            umbral : data.umbral,
            id: "chart-lotes",
            legendBottom : false,
            zoom : true,
            type : 'line',
            min : 'dataMin'
        }
        ReactDOM.render(React.createElement(Historica, props), document.getElementById('chart-lotes'));
    }

    const renderTableData = (data, periodos) => {
        let props = {
            header : [
                {
                    key : 'sector',
                    name : 'SECTOR',
                    locked : true,
                    titleClass : 'text-center',
                },
                {
                    key : 'sum',
                    name : 'SUM',
                    locked : true,
                    titleClass : 'text-center',
                    alignContent : 'right'
                },
                {
                    key : 'avg',
                    name : 'AVG',
                    locked : true,
                    titleClass : 'text-center',
                    alignContent : 'right',
                },
                {
                    key : 'min',
                    name : 'MIN',
                    locked : true,
                    titleClass : 'text-center',
                    alignContent : 'right',
                },
                {
                    key : 'max',
                    name : 'MAX',
                    locked : true,
                    titleClass : 'text-center',
                    alignContent : 'right',
                },
            ],
            data : data,
            buttons : [
                {
                    title : 'Excel',
                    action : () => {
                        $scope.table1.exportToExcel()
                    },
                    className : ''
                }
            ]
        }
        periodos.map((s, i) => {
            props.header.push({
                key : `periodo_${s}`,
                name : `${s}`,
                titleClass : 'text-center',
                alignContent : 'right'
            })
        })
        $("#table-data").html("")
        $scope.table1 = ReactDOM.render(React.createElement(ReactDataGrid, props), document.getElementById('table-data'))
    }

    $scope.filters = {
        unidad : 'QQ',
        anio : moment().year()
    }

    $scope.init = () => {
        $request.loadData((r) => {
            $scope.anios = r.anios
            renderTableData(r.data, r.periodos)
        }, $scope.filters)
        $request.chart((r) => {
            renderChart(r.data)
        }, $scope.filters)
    }
    $scope.init()
}]);

