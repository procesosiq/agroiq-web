function graficas(){
    var self = this;
    self = {
        options : {},
        id : "",
        theme : "infographic",
        required : function(){
            if(!this.id) throw Error("id es Requida");
            if(!this.options) throw Error("url es Requido");
        }
    }

    var printGrafica = function(){    
        var grafica = new echartsPlv();  
        grafica.init(self.id , self.options);
    }

    graficas.prototype.create = function(id , options ){
        self.options = options;
        self.id = id;
        self.required();
        return printGrafica();
    }
}

function createGraficas(options , id){
    var grafica = new graficas();
    grafica.create(id , options);
}

function calidad(id , options , margen){
    var margen = margen;
    var options_historico = options.historico;
    var options_historico_legends = options.historico_legends;
    var category = [];
    var legend = []; 
    var series = [];
    var legend = [];
    var legend_data = [];
    var series = [];
    legend = Object.keys(options_historico_legends);
    for(var i in options_historico){
        series.push(options_historico[i]);
        legend_data.push(options_historico[i].name);
    }

    var option = {   
            title : {
                text: 'Calidad Fincas',
                x: 'center',
                align: 'right'
            },
            grid: {
                bottom: 80
            },
            toolbox: {
                feature: {
                    dataZoom: {
                        yAxisIndex: 'none'
                    },
                    magicType: {
                        type: ['line','bar']
                    },
                    restore: {},
                    saveAsImage: {}
                }
            },
            tooltip : {
                trigger: 'axis',
                axisPointer: {
                    animation: false
                }
            },
            legend: {
                data: legend_data,
                x: 'left'
            },
            dataZoom: [
                {
                    show: true,
                    realtime: true,
                    // start: 65,
                    // end: 85
                },
                {
                    type: 'inside',
                    realtime: true,
                    // start: 65,
                    // end: 85
                }
            ],
            xAxis : [
                {
                    type : 'category',
                    boundaryGap : true,
                    axisLine: {onZero: true},
                    data : legend
                }
            ],
            yAxis: [
                {
                    type: 'value',
                    min : margen.min,
                    max : margen.max
                }
            ],
            series: series
        };
    

    createGraficas(option ,id);
}

function clusterCahas(id , options , margen){
    var margen = margen;
    var options_historico = options.historico;
    var options_historico_legends = options.historico_legends;
    var category = [];
    var legend = []; 
    var series = [];
    var legend = [];
    var legend_data = [];
    var series = [];
    legend = Object.keys(options_historico_legends);
    for(var i in options_historico){
        series.push(options_historico[i]);
        legend_data.push(options_historico[i].name);
    }

    var option = {   
            title : {
                text: 'Categorias',
                x: 'center',
                align: 'right'
            },
            grid: {
                bottom: 80
            },
            toolbox: {
                feature: {
                    dataZoom: {
                        yAxisIndex: 'none'
                    },
                    magicType: {
                        type: ['line','bar']
                    },
                    restore: {},
                    saveAsImage: {}
                }
            },
            tooltip : {
                trigger: 'axis',
                axisPointer: {
                    animation: false
                }
            },
            legend: {
                data: legend_data,
                x: 'left'
            },
            dataZoom: [
                {
                    show: true,
                    realtime: true,
                    // start: 65,
                    // end: 85
                },
                {
                    type: 'inside',
                    realtime: true,
                    // start: 65,
                    // end: 85
                }
            ],
            xAxis : [
                {
                    type : 'category',
                    boundaryGap : true,
                    axisLine: {onZero: true},
                    data : legend
                }
            ],
            yAxis: [
                {
                    type: 'value',
                    min : margen.min,
                    max : margen.max
                }
            ],
            series: series
        };
    

    createGraficas(option ,id);
}

app.directive('tooltip', function(){
    return {
        restrict: 'A',
        link: function(scope, element, attrs){
            $(element).hover(function(){
                // on mouseenter
                $(element).tooltip('show');
            }, function(){
                // on mouseleave
                $(element).tooltip('hide');
            });
        }
    };
});

app.filter('orderObjectBy', function() {
  return function(items, field, reverse) {
    var filtered = [];
    angular.forEach(items, function(item) {
        //// console.log(item)
      filtered.push(item);
    });
    filtered.sort(function (a, b) {
        //alert(a[field]);
      return (a[field] > b[field] ? 1 : -1);
    });
    if(reverse) filtered.reverse();
    return filtered;
  };
});

app.controller('informe_calidad', ['$scope','$http','$interval','client','$controller','$timeout' ,'$window', function($scope,$http,$interval,client, $controller , $timeout,$window){
	$scope.id_company = 0;
    $scope.tags = {
        calidad_maxima : {
            value : 0,
            label : ""
        },
        calidad_minima : {
            value : 0,
            label : ""
        },
        cluster : {
            value : 0,
            label : ""
        },
        desviacion_estandar : {
            value : 0,
            label : ""
        },
        peso : {
            value : 0,
            label : ""
        },
        calidad_dedos : {
            value : 0,
            label : ""
        },
        calidad_cluster : {
            value : 0,
            label : ""
        },
        dedos_promedio : {
            value : 0,
            label : ""
        }
    };

    // 03/03/2017 - TAG: Selects
    $scope.fincas = [];
    $scope.fincaSelected = "";
    $scope.exportadores = [];
    $scope.exportadorSelected = "";
    $scope.clientes = [];
    $scope.clienteSelected = "";
    $scope.marcas = [];
    $scope.marcaSelected = "";
    $scope.contenedores = [];
    $scope.contenedorSelected = "";
    // 03/03/2017 - TAG: Selects
    
    
    $scope.umbrales = {};
    $scope.defectos = [];
    $scope.tittleDefectos = [];
    $scope.clusters = [];
    $scope.dedos = [];
    $scope.categorias = [];
    $scope.totales = [];
    $scope.muestras = [];

    $scope.calidad = {
        params : {
            idFinca : 0,
            idExportador : 0,
            idCliente : 0,
            idMarca : 0,
            contenedor : "",
            fecha_inicial : moment().subtract(6, 'days').format('YYYY-MM-DD'),
            fecha_final :  moment().format('YYYY-MM-DD'),
            cliente: "",
            marca: ""
        },
        step : 0,
        templatePath : [],
        nocache : function(){
            this.templatePath.push('/views/marun/templetes/calidad/principal.html?' +Math.random());
        }
    }

    // 22/02/2017 - TAG: Filtros
    $scope.paramsGrafica = function(){    
        var response = {
            graficas : {},
            margen : {
                min : 0 ,
                max : 10,
                umbral : 4,
            }
        }
        return response;
    }

    $scope.request = {
    	tags : function(){
    		var data = $scope.calidad.params;
            client.post('phrapi/marun/calidad/home' , $scope.printTags , data , "contentTags");
    	},
    	principal : function(){
    		var data = $scope.calidad.params;
            client.post('phrapi/marun/calidad/main' , $scope.printPrincipal , data , "contentMain");	
    	},
        clusterCajas : function(){
            var data = $scope.calidad.params;
            client.post('phrapi/marun/calidad/cluster/caja' , $scope.printClusterCajas , data , "contentClusterCaja");	
        },
        tableCluster : function(){
            var data = $scope.calidad.params;
            client.post('phrapi/marun/calidad/table/cluster' , $scope.printTableCluster, data , "clusterContent");	
        },
        tableDedos : function(){
            var data = $scope.calidad.params;
            client.post('phrapi/marun/calidad/table/dedos' , $scope.printTableDedos , data , "dedosConten");	
        },
        defectos : function(){
            var data = $scope.calidad.params;
            client.post('phrapi/marun/calidad/defectos' , $scope.printDefectos , data , "contentDefectos");	
        },
        defectosDetailsTable : function(){
            var data = $scope.calidad.params;
            client.post('phrapi/marun/calidad/table/defectos' , $scope.printTableSubFinca , data , "contentTableSubFinca");	
        },
    	all : function(){
    		this.tags();
    		this.principal();
            this.clusterCajas();
            this.tableCluster();
            this.tableDedos();
            this.defectos();
    	}

    }

    $scope.printTags = function(r,b){
    	b("contentTags");
    	if(r){
    		if(r.hasOwnProperty("tags")){
	            $scope.tags.calidad_maxima.value = Math.round(r.tags.calidad_maxima);
	            $scope.tags.calidad_minima.value = Math.round(r.tags.calidad_minima);
                $scope.tags.cluster.value = new Number(r.tags.cluster).toFixed(2);
	            $scope.tags.desviacion_estandar.value = Math.round(r.tags.desviacion_estandar);
	            $scope.tags.peso.value = Math.round(r.tags.peso);
				$scope.tags.calidad_dedos.value = Math.round(r.tags.calidad_dedos);
				$scope.tags.calidad_cluster.value = Math.round(r.tags.calidad_cluster);
				$scope.tags.dedos_promedio.value = new Number(r.tags.dedos_promedio).toFixed(2);
                // 22/02/2017 - TAG: FILTROS
                $scope.fincas = r.filters.fincas || [];
                $scope.exportadores = r.filters.exportadores || [];
                $scope.clientes = r.filters.clientes || [];
                $scope.marcas = r.filters.marcas || [];
                $scope.contenedores = r.filters.contenedores || [];
                // 22/02/2017 - TAG: FILTROS
                setTimeout(function(){
                    $(".counter_tags").counterUp({
                        delay: 10,
                        time: 1000
                    });
                } , 1000);
    		}
    	}
    }

    // 22/02/2017 - TAG: Grafica Calidad Finca
    $scope.printPrincipal = function(r , b){
        b("contentMain")
        var data = $scope.paramsGrafica();
    	if(r){
            data.graficas.historico = r.data || [];
            data.graficas.historico_avg = r.avg || [];
            data.graficas.historico_legends = r.legend || [];
            data.margen.min = 80;
            data.margen.max = 100;
			calidad("principal" ,data.graficas , data.margen );
    	}
    }
   // 22/02/2017 - TAG: Grafica Cluster por Caja
    $scope.printClusterCajas = function(r , b){
        b("contentClusterCaja")
        var data = $scope.paramsGrafica();
    	if(r){
            data.graficas.historico = r.data || [];
            data.graficas.historico_avg = r.avg || [];
            data.graficas.historico_legends = r.legend || [];
            data.margen.min = 0;
            data.margen.max = 20;
			clusterCahas("clusterCajas" ,data.graficas , data.margen );
    	}
    }
    // 22/02/2017 - TAG: Tabla de Clusters
    $scope.printTableCluster = function(r , b){
        b("clusterContent")
    	if(r){
           $scope.clusters = r.data || [];
    	} 
    }
    // 22/02/2017 - TAG: Tabla de Dedos
    $scope.printTableDedos = function(r , b){
        b("dedosConten")
    	if(r){
           $scope.dedos = r.data || [];
    	}
    }
    // 22/02/2017 - TAG: Grafica de Defectos
    $scope.printDefectos =  function(r , b){
        b("contentDefectos")
        if(r){
            $scope.defectos = r.data || {};
            $scope.categoriasFilters = r.categoriasFilters || {};
            $scope.categorias = angular.copy(r.categorias) || {};
            createGraficas($scope.defectos, "CategoriasDefectos")
            var position = Object.keys($scope.categorias)[0];
            var category = $scope.categorias[position];
            createGraficas(category , "defectos")
    	}
    }
    // 24/02/2017 - TAG: Table Sub Finca
    $scope.printTableSubFinca = function(r , b){
        b("contentTableSubFinca")
        console.log("tabla")
    	if(r){
            $scope.tittleDefectos = r.defectos || {};
            $scope.totales = r.totales || {};
            $scope.muestras = r.muestras || {};
            $('.tooltips').tooltip();
    	}
    }

    // 07/03/2017 - TAG: Change Category
    cambiosCategoria = function(){
        var position = $("#categoria").val();
        var category = $scope.categorias[position];
        createGraficas(category, "defectos")
    }

    // 03/03/2017 - TAG: Select Fincas
    cambiosFincas = function(){
        $scope.calidad.params.palanca = "";
        $scope.calidad.params.idExportador = "";
        $scope.calidad.params.idCliente = "";
        $scope.calidad.params.idMarca = "";
        $scope.request.defectosDetailsTable();
        $scope.loadExternal();
    }    

    // 03/03/2017 - TAG: Select Expotador
    cambiosExportador = function(){
        $scope.calidad.params.idCliente = "";
        $scope.calidad.params.idMarca = "";
        $scope.request.all();
    }
    
    // 03/03/2017 - TAG: Select Cliente
    cambiosCliente = function () {
        $scope.calidad.params.idMarca = "";
        $scope.request.all();
    }
   
   // 03/03/2017 - TAG: Select Marca
    cambiosMarca = function () {
        $scope.request.all();
        //$scope.request.defectosDetailsTable($("#idMarca").val());
    }

    $scope.changeContenedor = function(){
        $scope.loadExternal();
    }

    $scope.openDetalle = function(data){
        data.expand = !data.expand;
    }

	$scope.changeRangeDate = function(data){
		if(data){
			$scope.calidad.params.fecha_inicial = data.hasOwnProperty("first_date") ? data.first_date : $scope.wizardStep.params.fecha_inicial;
			$scope.calidad.params.fecha_final = data.hasOwnProperty("second_date") ? data.second_date : $scope.wizardStep.params.fecha_final;
			$scope.loadExternal();
		}
	}
    $scope.loadExternal = function(){
		$scope.request.all();
    }

    $scope.toNumber = function(value){
        return parseFloat(value);
    }
}]);