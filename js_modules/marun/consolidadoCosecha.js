app.service('request', ['$http', function($http){
    this.loadData = (callback, params) => {        
        load.block()
        $http.post('phrapi/marun/consolidadoCosecha/data', params || {}).then(r => {
            load.unblock()
            if(callback) callback(r.data)
        })
    }

    this.save = (callback, params) => {
        $http.post('phrapi/marun/consolidadoCosecha/save', params || {}).then(r => {
            if(callback) callback(r.data)
        })
    }
}])

app.controller('controller', ['$scope', 'request', function($scope, $request){

    $scope.changeRangeDate = (data = {}) => {
        if(data){
            $scope.filters.fecha_inicial = data.first_date
            $scope.filters.fecha_final = data.second_date
            $scope.init()
        }
    }

    const save = (row, campo, valor) => {
        if(id > 0){
            $request.save((r) => {
                if(r.status == 200){
                    alert("Se guardo correctamente", "", "success")
                }else{
                    alert("Hubo un error favor de intentar mas tarde")
                }
            }, {
                fecha : $scope.filters.fecha,
                sector : row.sector,
                lote : row.lote,
                codigo : row.codigo,

                campo : campo, 
                valor : valor
            })
        }
    }

    const renderTableData = (data) => {
        var props = {
            header : [
                {
                    key : 'fecha',
                    name : 'FECHA',
                    titleClass : 'text-center',
                    sortable : true,
                    alignContent : 'center',
                    filterable : true,
                    resizable : true
                },
                {
                    key : 'cedula',
                    name : 'CEDULA',
                    titleClass : 'text-center',
                    sortable : true,
                    alignContent : 'center',
                    filterable : true,
                    resizable : true
                },
                {
                    key : 'cosechador',
                    name : 'COSECHADOR',
                    titleClass : 'text-center',
                    sortable : true,
                    alignContent : 'center',
                    filterable : true,
                    resizable : true,
                    width : 250,
                    customCell : (data) => {
                        let nombre = data.cosechador || ''
                        return `
                            <div style="height: 100%;" class="${data.cedula != 'TOTAL' && nombre == '' ? 'bg-red-thunderbird' : ''}">
                                ${nombre}
                            </div>
                        `
                    }
                },{
                    key : 'sector',
                    name : 'SECTOR',
                    titleClass : 'text-center',
                    sortable : true,
                    alignContent : 'center',
                    filterable : true,
                    resizable : true,
                    customCell : (data) => {
                        let sector = data.cedula != 'N/A' ? data.sector : 'N/A'
                        return `
                            <div style="height: 100%;" class="text-center ${sector == '' ? 'bg-red-thunderbird bg-font-red-thunderbird' : ''}">
                                <span style="top: 25%; position: relative;">${sector}</span>
                            </div>
                        `
                    }
                },{
                    key : 'lote',
                    name : 'LOTE',
                    titleClass : 'text-center',
                    sortable : true,
                    alignContent : 'center',
                    filterable : true,
                    resizable : true,
                    customCell : (data) => {
                        let lote = data.cedula != 'N/A' ? data.lote : 'N/A'
                        return `
                            <div style="height: 100%;" class="text-center ${lote == '' ? 'bg-red-thunderbird bg-font-red-thunderbird' : ''}">
                                <span style="top: 25%; position: relative;">${lote}</span>
                            </div>
                        `
                    }
                },{
                    key : 'codigo',
                    name : 'CODIGO',
                    titleClass : 'text-center',
                    sortable : true,
                    alignContent : 'center',
                    filterable : true,
                    resizable : true,
                },{
                    key : 'entregadas',
                    name : 'ENTREGADAS',
                    titleClass : 'text-center',
                    sortable : true,
                    alignContent : 'center',
                    filterable : true,
                    resizable : true,
                },{
                    key : 'recibidas',
                    name : 'RECIBIDAS',
                    titleClass : 'text-center',
                    sortable : true,
                    alignContent : 'center',
                    filterable : true,
                    resizable : true,
                    customCell : (data) => {
                        let recibidas = data.recibidas || 0
                        let saldo = data.saldo || 0
                        return `
                            <div style="height: 100%;" class="text-center ${!['TOTAL', 'N/A'].includes(data.cedula) && saldo < 0 ? 'bg-red-thunderbird bg-font-red-thunderbird' : ''}">
                                <span style="top: 25%; position: relative;">${recibidas}</span>
                            </div>
                        `
                    }
                },{
                    key : 'saldo',
                    name : 'SALDO',
                    titleClass : 'text-center',
                    sortable : true,
                    alignContent : 'center',
                    filterable : true,
                    resizable : true,
                },{
                    key : 'peso',
                    name : 'PESO TOTAL',
                    titleClass : 'text-center',
                    sortable : true,
                    alignContent : 'center',
                    filterable : true,
                    resizable : true,
                    width : 100,
                }
            ],
            data : data,
            buttons : [
                {
                    title : 'Excel',
                    action : () => {
                        $scope.table1.exportToExcel()
                    },
                    className : ''
                }
            ]
        }
        $("#table-data").html("")
        $scope.table1 = ReactDOM.render(React.createElement(ReactDataGrid, props), document.getElementById('table-data'))
    }

    $scope.StartEndDateDirectives = {
        startDate : moment(),
        endDate :moment(),
    }
    $scope.filters = {
        fecha_inicial : moment().format('YYYY-MM-DD'),
        fecha_final : moment().format('YYYY-MM-DD'),
        unidad: 'LB'
    }

    $scope.init = () => {
        $request.loadData((r) => {
            renderTableData(r.data)
        }, $scope.filters)
    }
    $scope.init()
}]);

