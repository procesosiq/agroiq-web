app.service('request', ['$http', function($http){
    
    this.data = function(callback, params){
        let url = 'phrapi/marun/produccionreporte/data'
        load.block()
        $http.post(url, params || {}).then(r => {
            load.unblock()
            callback(r.data)
        })
    }

    this.muestreo = function(callback, params){
        let url = 'phrapi/marun/produccionreporte/muestreo'
        //load.block()
        /*$http.post(url, params || {}).then(r => {
            //load.unblock()
            callback(r.data)
        })*/
    }

    this.getWeeks = function(callback, params){
        let url = 'phrapi/marun/produccionreporte/weeks'
        $http.post(url, params || {}).then(r => {
            callback(r.data)
        })
    }

    this.last = function(callback, params){
        let url = 'phrapi/marun/produccionreporte/last'
        $http.post(url, params || {}).then(r => {
            callback(r.data)
        })
    }
}])

app.filter('avgOfValue', function () {
    return function (data, key) {        
        if (angular.isUndefined(data) || angular.isUndefined(key))
            return 0;        
        var sum = 0;
        var count = 0;
        angular.forEach(data,function(value){
            if(value[key] != "" && value[key] != undefined){
                sum = sum + parseFloat(value[key], 10);
                count++;
            }
        });
        sum = sum / count;
        if(isNaN(sum))
            return 0;
        return sum;
    }
})

app.filter('sumOfValue', function () {
    return function (data, key) {        
        if (angular.isUndefined(data) || angular.isUndefined(key))
            return 0;        
        var sum = 0;
        angular.forEach(data,function(value){
            if(value[key] != "" && value[key] != undefined){
                sum = sum + parseFloat(value[key], 10);
            }
        });
        if(isNaN(sum))
            return 0;
        return sum;
    }
})

app.controller('controller', ['$scope','request', function($scope, $request){

    $scope.filters = {}
    $scope.mostrar_calculos_muestreo = 0
    $scope.mostrar_calculos = 0
    $scope.changeRangeDate = (data) => {
        if(data){
            $scope.year = ''
            $scope.semana = ''
            $scope.filters.fecha_inicial = data.hasOwnProperty("first_date") ? data.first_date : $scope.filters.fecha_inicial;
            $scope.filters.fecha_final = data.hasOwnProperty("second_date") ? data.second_date : $scope.filters.fecha_final;
            getData()
        }
    }

    $scope.changeYear = () => {
        if($scope.filters.year != ''){
            $request.getWeeks((r) => {
                $scope.semanas = r.semanas
                if(r.semanas.indexOf($scope.filters.semana) == -1){
                    $scope.filters.semana = ''
                }
            }, {
                year: $scope.filters.year
            })
        }
    }
    $scope.changeWeek = () => {
        if($scope.filters.semana != '' && $scope.filters.year != ''){
            $scope.filters.fecha_inicial = ''
            $scope.filters.fecha_final = ''
            getData()
        }
    }

    getData = () => {
        $request.data((r) => {
            $scope.data = r.data
            $scope.totales = r.totales
            $scope.edades = r.edades
            $scope.cintas = r.cintas

            $scope.totales_general = {}
            for(let i in r.totales_general){
                let row = r.totales_general[i]
                $scope.totales_general[row.variable] = row.valor
            }
        }, $scope.filters)

        $request.muestreo((r) => {
            $scope.data_muestreo = r.data
        }, $scope.filters)
    }

    $scope.fnExcelReport = function(id_table, title){
        var data = new Blob([document.getElementById(id_table).outerHTML], {
            type : 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf8'
        })
        saveAs(data, title+'.xls')
    }

    $scope.getData = getData

    load.block()
    $request.last((r) => {
        load.unblock()
        $scope.filters = {
            year : r.last_year,
            semana : r.last_week
        }
        $scope.fincas = r.fincas
        $scope.years = r.years
        $scope.semanas = r.weeks

        $scope.filters.id_finca = Object.keys(r.fincas)[0]

        //$("#date-picker").html(`${r.fecha_inicial} - ${r.fecha_final}`)
        getData()
    })
    
}]);