 function loadGrid(config){
 	grid.id = config.id;
	grid.url = config.path;
	grid.buttons = [
        {
            extend: 'print', 
            className: 'btn dark btn-outline', 
            "title" : "Listado", 
            "text" : "Imprimir",
            customize: function ( win ) {
                $(win.document.body)
                    .css( 'font-size', '10pt' )
                    .css( 'float', 'rigth' )
                    .prepend(
                        '<img src="http://orodelti.procesos-iq.com/assets/logo.png" />'
                    );

                $(win.document.body).find( 'table' )
                    .addClass( 'compact' )
                    .css( 'font-size', 'inherit' );
            }
        }];

	grid.addButtons({ extend: 'excel', "title" : "Listado de Sectores", "text" : "Excel" ,className: 'btn yellow btn-outline ',
                    exportOptions: {
                        columns: ':visible'
                    } 
                });
	grid.addButtons({ extend: 'csv', "title" : "Listado de Sectores" ,"text" : "CSV", className: 'btn purple btn-outline ',
	                    exportOptions: {
	                        columns: ':visible'
	                    } 
	                });
	grid.init();
 }


app.directive('pluginUniform', function() {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
           /* element is jQuery object*/
            element.parents(".control-label").css("cursor" , "pointer");
        }
    };
});

app.controller('membresias', ['$scope','$http','$interval','client','$controller', function($scope,$http,$interval,client, $controller){
	$scope.memberships = {
		params : {
			idCompany : 0,
			idUser : 0,
		},
		button : ["Nuevo Usuario" , "Editar Usuario"],
		type : 0,
		path : ['phrapi/membresias/index' , 'phrapi/membresias/edit' , 'phrapi/membresias/save'],
		templatePath : ['/views/membresias.html' , '/views/membresia.html']
	}

	$scope.newUsers = function(){
		setTimeout(function() { $scope.changeTab(1); } , 500);
	}

	$scope.changeTab = function(tab){
		tab = tab || 0;
		$scope.memberships.type = tab;
		$scope.memberships.templatePath[$scope.memberships.type] += "?"+Math.random();
		console.log($scope.memberships.templatePath[$scope.memberships.type]);
	}

	$scope.loadExternal = function(){
		if($scope.memberships.path[$scope.memberships.type] != ""){
			$scope.init();
			var data = $scope.memberships.params;
			client.post($scope.memberships.path[$scope.memberships.type] , $scope.gridDetails , data);
		}
		console.log("hola");
	}

	$scope.tiitle = {
		tiitle : "Listado de Membresias"
	}

	$scope.dataset = [];
    $scope.ficha = {
        id : $scope.memberships.idUser,
        nombre_medico :'',
        empresa :'',
        empleado :'',
        trueapto_conp :'',
        trueapto_sin :'',
        truearea :'',
        cedula_identidad :'',
        edad :'',
        fecha_ingreso_comp :'',
        fecha_nacimiento :'',
        apto_sin : '',
        apto_con : '',
        apto_conp : '',
        no_apto : '',
        recomendaciones :'',
        sexo :'',
        puesto_trabajo :'',
    };


    $scope.init = function(){
        $('.date-picker').datepicker({
            rtl: App.isRTL(),
            autoclose: true
        });
    }

    $scope.gridDetails = function(r , b){
        console.log(r);
		b();
        if(r){
        	if($scope.memberships.type == 0){
        		var data = {
        			id : "#usuarios",
        			path : $scope.memberships.path[$scope.memberships.type]
        		}
        		loadGrid(data);
        	}
        	if($scope.memberships.type == 1){
	        	$scope.init();
	            $scope.dataset = r.data;
        	}
        }
    }

    $scope.success = function(r , b){
        b();
        if(r){
            alert("Ficha registrada / modificada con exito" ,"Ficha", "success" , function(){
                // document.location.href = "/demoList";
            })
        }
        console.log(r);
    }

    // $scope.add = function(){
    //     var data = $scope.ficha;
    //     if(data.nombre_medico == ""){
    //         alert("Favor de ingresar nombre");
    //     }       
    //     else if(data.empresa == ""){
    //         alert("Favor de ingresar empresa");
    //     }
    //     else if(data.fecha_since == ""){
    //         alert("Favor de ingresar fecha de nacimiento");
    //     }
    //     else if(data.edad == ""){
    //         alert("Favor de ingresar edad");
    //     }
    //     else{
    //         data.details = [];
    //         $.each($(".details"),function (index, element) {
    //             var id = $(element).prop("id");
    //             var value = $(element).is(":checked") || false;
    //             var info = {
    //                 id : id,
    //                 value : value
    //             }
    //             data.details.push(info);
    //         });

    //         data.radios = [];
    //         $.each($(".radios_group"),function (index, element) {
    //             var id = $(element).prop("id");
    //             var value = $(element).is(":checked") || false;
    //             var info = {
    //                 id : id,
    //                 value : value
    //             }
    //             data[id] = value;
    //             data.radios.push(info);
    //         });

    //         client.post('./controllers/index.php?accion=Demo.save' , $scope.success ,data);
    //     }
    // }

    // $(".btnadd").on("click" , function(){
    //     $scope.add();
    // });

    // $(".cancel").on("click" , function(){
    //     document.location.href = "demoList";
    // });

}]);