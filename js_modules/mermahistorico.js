
app.filter('orderObjectBy', function() {
  return function(items, field, reverse) {
    var filtered = [];
    angular.forEach(items, function(item) {
        // console.log(item)
        if(item.hasOwnProperty("lote")){
            if(!isNaN(parseFloat(item.lote)))
                item.lote = parseFloat(item.lote);
        }
        if(item.hasOwnProperty("total")){
            item.total = parseFloat(item.total);
        }
        if(item.hasOwnProperty("exedente")){
            if(!isNaN(parseFloat(item.exedente)))
                item.exedente = parseFloat(item.exedente);
        }
        if(item.hasOwnProperty("usd")){
            if(!isNaN(parseFloat(item.usd)))
                item.usd = parseFloat(item.usd);
        }

      filtered.push(item);
    });
    filtered.sort(function (a, b) {
        //alert(a[field]);
      return (a[field] > b[field] ? 1 : -1);
    });
    if(reverse) filtered.reverse();
    return filtered;
  };
});

app.controller('historico', ['$scope','$http','$interval','client','$controller','$timeout' ,'$window', function($scope,$http,$interval,client, $controller , $timeout,$window){
    $scope.id_company = 0;
    $scope.reverseSort = false;
    $scope.orderByField = "lote";
    $scope.historico = {
        params : {
            idFinca : 1,
            idLote : 0,
            idLabor : 0,
            semana : "",
            type : "ENFUNDE"
        },
        step : 0,
        path : ['phrapi/historico/index'],
        templatePath : [],
        nocache : function(){
            $scope.historica.init();
            $scope.init();
        }
    }

    $scope.StartEndDateDirectives = {
        startDate : moment().startOf('month'),
        endDate :moment().endOf('month'),
    }

    changeweek = function(){
        $scope.init();
    }

    $scope.title = {
        column_one : "Lote",
        column_two : "Enfundador",
    }

    changeHistorica = function(){
        $scope.historica.type = $("#type").val();
        $scope.historico.params.type = $("#type").val();
        if($("#type").val() == "ENFUNDE"){
            $scope.title.column_one = "Lote";
            $scope.title.column_two = "Enfundador";
        }
        else if($("#type").val() == "COSECHA"){
            $scope.title.column_one = "Código";
            $scope.title.column_two = "Palanca";
        }
        $scope.historica.init();
        $scope.init();
    }

    $scope.tabla = {
        historico : []
    }

    $scope.openDetalle = function(data){
        data.expanded = !data.expanded;
    }

    $scope.data = [];
    $scope.totales = [];
    $scope.semanas = [];
    $scope.historico = new echartsPlv();
    $scope.historica = {
        type : "ENFUNDE",
        init : function(){
            var data = {
                type : this.type
            }
            client.post('phrapi/historico/historico' , $scope.printDetailsHistorica ,data);
        }
    };
    $scope.init = function(){
        var data = $scope.historico.params;
        client.post($scope.historico.path , $scope.printDetails ,data)
    }

    $scope.start = true;
    $scope.umbral = 10;
    $scope.tagsFlags = function(value){
        var className = "fa fa-arrow-up font-red-thunderbird";
        if(!isNaN(parseFloat(value))){
            if(value <= $scope.umbral){
                className = "fa fa-arrow-down font-green-jungle"
            }
        }else{
            className = "";
        }

        return className;
    }

    $scope.printDetailsHistorica = function(r,b){
        b();
        if(r){
            if(r.hasOwnProperty("historico")){
                $scope.historico.init("historico" , r.historico);
            }
        }
    }

    $scope.printDetails = function(r,b){
        b();
        if(r){
            if(r.hasOwnProperty("totales")){
                $scope.totales = r.totales;
            }
            if(r.hasOwnProperty("data")){
                $scope.data = r.data;
            }
            if($scope.start){
                $scope.start = false;
                setInterval(function(){
                    $scope.init();
                } , (60000 * 5));
            }
        }
    }

}]);

