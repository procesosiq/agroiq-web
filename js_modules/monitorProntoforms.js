app.service('request', ['$http', ($http) => {
	var service = {}

	service.last = (params) => {
		return new Promise((resolve, reject) => {
			$http.post('phrapi/monitor/last', params || {})
			.then((r) => {
				resolve(r.data)
			})
			.catch((r) => {
				reject()
			})
		})
	}

	service.index = (params) => {
		return new Promise((resolve, reject) => {
			$http.post('phrapi/monitor/index', params || {})
			.then((r) => {
				resolve(r.data)
			})
			.catch((r) => {
				reject()
			})
		})
	}

	service.comentario = (params) => {
		return new Promise((resolve, reject) => {
			$http.post('phrapi/monitor/comentario', params || {})
			.then((r) => {
				resolve(r.data)
			})
			.catch((r) => {
				reject()
			})
		})
	}

	return service
}])

app.directive('enter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.enter);
                });

                event.preventDefault();
            }
        });
    };
});

app.controller('controller', ['$scope', 'request', ($scope, $request) => {

	$scope.data = {
		by_form : [],
		by_user : [],
		data : []
	}
	$scope.datesEnabled = []
	$scope.filters = {
		fecha : moment().format('YYYY-MM-DD')
	}
	$scope.singleDatePickerDirective = true
	$scope.startDate = moment().format('YYYY-MM-DD')
	$scope.StartEndDateDirectives = {
		startDate : moment(),
		endDate : moment()
	}

	$scope.changeRangeDate = (f) => {
		$scope.filters.fecha = f.first_date
		$scope.init()
	}

	$scope.last = () => {
		$request.last($scope.filters)
		.then((r) => {
			$scope.datesEnabled = r.days

			$scope.startDate = r.date
			$scope.StartEndDateDirectives.startDate = moment(r.date)
			$scope.filters = {
				fecha : r.date
			}
			$scope.init()
		})
	}

	$scope.init = () => {
		$request.index($scope.filters).then((r) => {
			$scope.formularios = r.forms
			$scope.data = r.data_by_user.data

			$scope.$apply()
		})
	}

	$scope.comentario = (row) => {
		let params = angular.copy(row)
		params.fecha = angular.copy($scope.filters.fecha)

		$request.comentario(params)
		.then((r) => {
			if(r.status === 200){
				toastr.success('Guardado')
			}else{
				alert('Ocurrio algo inesperado, contacte a soporte')
			}
		})
	}

	setInterval(() => $scope.init(), 30 * 1000)
	$scope.last()
	
}])