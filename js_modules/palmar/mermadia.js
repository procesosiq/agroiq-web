/*----------  UTILIDADES SOBRE ARRAYS  ----------*/
app.filter('unique', function() {
   return function(collection, keyname) {
      var output = [], 
          keys = [];

      angular.forEach(collection, function(item) {
          var key = item[keyname];
          if(keys.indexOf(key) === -1) {
              keys.push(key);
              output.push(item);
          }
      });

      return output;
   };
});

function loadScript(options){

}


app.filter('orderObjectBy', function() {
  return function(items, field, reverse) {
    var filtered = [];
    angular.forEach(items, function(item) {
        // console.log(item)
        if(item.hasOwnProperty("lote")){
            if(!isNaN(parseFloat(item.lote)))
                item.lote = parseFloat(item.lote);
        }
        if(item.hasOwnProperty("total_peso_merma")){
            item.total_peso_merma = parseFloat(item.total_peso_merma);
        }
        if(item.hasOwnProperty("total_defectos")){
            if(!isNaN(parseFloat(item.total_defectos)))
                item.total_defectos = parseFloat(item.total_defectos);
        }
        if(item.hasOwnProperty("merma")){
            if(!isNaN(parseFloat(item.merma)))
                item.merma = parseFloat(item.merma);
        }
        if(item.hasOwnProperty("danhos_peso")){
            if(!isNaN(parseFloat(item.danhos_peso)))
                item.danhos_peso = parseFloat(item.danhos_peso);
        }
        if(item.hasOwnProperty("filter")){
            if(!isNaN(parseFloat(item.filter)))
                item.filter = parseFloat(item.filter);
        }
        if(item.hasOwnProperty("cantidad")){
            if(!isNaN(parseFloat(item.cantidad)))
                item.cantidad = parseFloat(item.cantidad);
        }

      filtered.push(item);
    });
    filtered.sort(function (a, b) {
        //alert(a[field]);
      return (a[field] > b[field] ? 1 : -1);
    });
    if(reverse) filtered.reverse();
    return filtered;
  };
});


app.controller('informe_merma_dia', ['$scope','$http','$interval','client','$controller','$timeout' ,'$window', function($scope,$http,$interval,client, $controller , $timeout,$window){
    
    $scope.leyendaGeneralTitle = 'Merma';

    $scope.id_company = 0;
    
    $scope.labelLbKg = "Libras";
    $scope.statusLbKg = false;
    $scope.labelPeso = "";
    $scope.umbral_merma = Number(localStorage.getItem('umbral_merma') || 0)
    $scope.umbral_lb = Number(localStorage.getItem('umbral_lb') || 0)
    $scope.umbrales = {};

    $scope.rangesDirectives = {
        'Hoy': [moment(), moment()],
        'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Hace 2 Días': [moment().subtract(2, 'days'), moment().subtract(2, 'days')],
    }

    $scope.limitDirectives = {
        days : 0
    }

    $scope.StartEndDateDirectives = {
        startDate : moment(),
        endDate :moment(),
    }
    $scope.datesEnabled = []
    $scope.singleDatePickerDirective = true;

    $scope.table = {
        merma_widthCell : "10%",
        peso_widthCell : "10%",
    }

    $scope.calidad = {
        params : {
            idFinca : "",
            idFincaDia : "",
            idMerma : "MATERIA PRIMA",
            idLote : 0,
            idLabor : 0,
            fecha_inicial : moment().format('YYYY-MM-DD'),
            fecha_final : moment().format('YYYY-MM-DD'),
            cliente: "",
            marca: "",
            palanca : "",
            statusLbKg : true,
            categoria : "COSECHA"
        },
        step : 0,
        path : ['phrapi/palmar/diamermaaaaa/index'],
        templatePath : ['/views/palmar/templetes/mermadia/step1.html?' +Math.random()],
    }

    $scope.palancas = [];

    $scope.mermas = {
        NETA : "Merma Neta",
        "MATERIA PRIMA" : "Merma Prima",
    };

    $scope.cambiosMerma = function(){
        $scope.calidad.params.palanca = "";
        $scope.calidad.params.idFincaDia = "";
        $scope.loadExternal();
    }

    $scope.loadExternal = function(){
        if($scope.calidad.path[$scope.calidad.step] != ""){
            var data = $scope.calidad.params;
            client.post($scope.calidad.path[$scope.calidad.step] , $scope.startDetails , data);
        }
    }

    $scope.last = () => {
        client.post('phrapi/palmar/merma/last', (r, b) => {
            b()
            $scope.datesEnabled = r.days
            if(r.fecha){
                $scope.calidad.params.fecha_inicial = r.fecha
                $scope.calidad.params.fecha_final = r.fecha
                $("#date-picker").html(`${r.fecha}`)
            }
            $scope.loadExternal()
        })
    }

    $scope.cambiosCategoria = function(){
        $scope.loadExternal()
    }

    $scope.changeRangeDate = function(data){
        if(data){
            $scope.calidad.params.fecha_inicial = data.hasOwnProperty("first_date") ? data.first_date : $scope.wizardStep.params.fecha_inicial;
            $scope.calidad.params.fecha_final = data.hasOwnProperty("second_date") ? data.second_date : $scope.wizardStep.params.fecha_final;
            $scope.loadExternal();
        }
    }

    $scope.convertKg = function(){
        $scope.statusLbKg = !$scope.statusLbKg;
        if($scope.statusLbKg === true)
            $scope.labelLbKg = "Kilos";
        else if($scope.statusLbKg === false)
            $scope.labelLbKg = "Libras";

        $scope.calidad.params.statusLbKg = $scope.statusLbKg;
        $scope.loadExternal();
    }

    $scope.getKey = function(data , key){
        return Object.keys(data)[key];
    }

    $scope.saveUmbral = (key, value) => {
        localStorage.setItem(key, value)
    }

    $scope.getUmbralMerma = function(value){
        if(value > 0){
            if(value > $scope.umbral_merma)
                return 'fa fa-arrow-up font-red-thunderbird'
            else (value < $scope.umbral_merma)
                return 'fa fa-arrow-down font-green-haze'
        }
        return  'fa fa-arrows-h font-yellow-gold'
    }

    $scope.getUmbralPeso = function(value){
        if(value > 0){
            if(value > $scope.umbral_lb)
                return 'fa fa-arrow-up font-red-thunderbird'
            else (value < $scope.umbral_lb)
                return 'fa fa-arrow-down font-green-haze'
        }
        return  'fa fa-arrows-h font-yellow-gold'
    }

    $scope.totalDefectos = {};


    $scope.fincas = [];

    $scope.cambiosFincas = function(){
        $scope.calidad.params.palanca = "";
        $scope.calidad.params.idFincaDia = "";
        $scope.loadExternal();
    }

    $scope.startDetails = function(r , b){
        b();
        if(r){
            $scope.numViajes = r.viajes
            $scope.merma = r.merma
            $scope.defectos = r.defectos
            $scope.pesos = r.pesos
        }
    }

}]);