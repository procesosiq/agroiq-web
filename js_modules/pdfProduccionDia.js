import Promise from 'promise-polyfill';

app.service('request', ['client', '$http', function(client, $http){

    this.pesoMano = function(params){
        return new Promise((resolve) => {
            $http.post('phrapi/marun/racimos/pesoMano', params || {})
            .then((r) => {
                resolve(r.data)
            })
        })
    }

    this.pesoRacimo = function(params){
        return new Promise((resolve) => {
            $http.post('phrapi/marun/racimos/pesoRacimo', params || {})
            .then((r) => {
                resolve(r.data)
            })
        })
    }

    this.graficasBarras = function(callback, params){
        let url = 'phrapi/marun/cajas/graficasBarras'
        let data = params || {}
        client.post(url, callback, data, 'barras')
    }

    this.saveImg = function(params){
        return new Promise((resolve) => {
            let url = 'phrapi/marun/racimos/saveImg'
            let data = params || {}
            $http.post(url, data).then((r) => {
                resolve(r.data)
            }).catch((e) => {
                console.error('Error - No se pudo grabar la imagen /saveImg', e)
            });
        })
    }
}])

function getOptionsGraficaReact(id, options, title){
    let newOptions = {
        series: options.series,
        legend: options.legends,
        umbral: null,
        id: id,
        titulo : title || null,
        actions : false,
        showLegends : false,
        legendOrient : 'vertical'
    }
    return newOptions
}

function initGrafica(id, options, title){
    let parent = $("#"+id).parent()
    parent.empty()
    parent.append(`<div id="${id}" class="chart"></div>`)

    let data = getOptionsGraficaReact(id, options, title)
    let chart = React.createElement(Historica, data)
    ReactDOM.render(chart, document.getElementById(id));
}

function initPastel(id, series){
    let legends = []
    let newSeries = []
    Object.keys(series).map(key => {
        let label = series[key].label
        newSeries.push({
            label : series[key].label,
            value : parseFloat(series[key].value),
            color : (label.includes('>')) ? '#E43A45' : (label.includes('<')) ? '#C49F47' : '#26C281'
        })
        if(legends.indexOf(series[key]) != -1) legends.push(series[key]);
    })
    let data = {
        data : newSeries,
        nameseries : "Pastel",
        legend : legends,
        titulo : "",
        id : id
    }

    let parent = $("#"+id).parent()
    parent.empty()
    parent.append(`<div id="${id}" class="chart"></div>`)
    ReactDOM.render(React.createElement(Pastel, data), document.getElementById(id));
}

function wait(miliseconds = 1000){
    return new Promise((resolve) => {
        setTimeout(() => {
            resolve()
        }, miliseconds)
    })
}

app.controller('produccion', ['$scope', 'request', function($scope, $request){

    const getUrlParam = (key) => {
        let url_string = window.location.href
        let url = new URL(url_string);
        let c = url.searchParams.get(key);
        return c
    }

    $scope.produccion = {
        params : {
            fecha_inicial : getUrlParam('fecha_inicial'),
            finca : getUrlParam('finca'),
            grupo : 10
        }
    }

    $scope.openGraficasAdicionales = () => {
        return new Promise(async (resolve) => {
            await $scope.pesoMano()
            await $scope.pesoRacimo()
            resolve()
        })
    }

    $scope.pesoMano = async () => {
        let r = await $request.pesoMano(Object.assign(
            { grupo : $scope.grupo_mano },
            $scope.produccion.params
        ))
        $scope.pesoManoTendencia = r.data
        $scope.pesoMano = r.dia
        new echartsPlv().init('grafica-peso-mano', $scope.pesoMano)

        await wait()
        await $scope.saveImg($('#grafica-peso-mano canvas')[0], 'grafica-peso-mano')
    }

    $scope.pesoRacimo = async () => {
        let r = await $request.pesoRacimo(Object.assign(
            { grupo : $scope.grupo_racimo },
            $scope.produccion.params
        ))
        $scope.pesoRacimo = r.dia
        $scope.pesoManosPeso = r.vs_manos

        new echartsPlv().init('grafica-peso-racimo', $scope.pesoRacimo)
        new echartsPlv().init('grafica-manos-peso', $scope.pesoManosPeso)

        await wait()
        await $scope.saveImg($('#grafica-peso-racimo canvas')[0], 'grafica-peso-racimo')
        await $scope.saveImg($('#grafica-manos-peso canvas')[0], 'grafica-manos-peso')
    }

    $scope.loadPasteles = () => {
        return new Promise((resolve) => {
            $request.graficasBarras(function(r, b){
                b('barras')
                if(r){
                    $scope.marcasBarras = r.marcas

                    setTimeout(async () => {
                        $scope.graficasBarras = r.graficas
                        $scope.rangos = r.rangos
                        let keys = Object.keys(r.graficas),
                            index = -1
                        for(let key of keys){
                            index++
                            let min = null, max = null
                            if(r.graficas[key].series.Cantidad.umbral){
                                min = parseFloat(r.graficas[key].series.Cantidad.umbral.min)
                                max = parseFloat(r.graficas[key].series.Cantidad.umbral.max)
                            }
                            
                            r.graficas[key].series.Cantidad.itemStyle.normal.color = function(e){
                                // range umbral 
                                if(min && max){
                                    var min_peso = 0, max_peso = 0
                                    if(parseFloat(e.name)){
                                        min_peso = max_peso = parseFloat(e.name)
                                    }else{
                                        var min_max = e.name.split(',')
                                        min_peso = parseFloat(min_max[0].replace('[', '').trim())
                                        max_peso = parseFloat(min_max[1].replace(']', '').trim())
                                    }

                                    if(min_peso >= min && max_peso <= max){
                                        return '#26C281'
                                    }else if(max_peso <= min){
                                        return '#C49F47'
                                    }else{
                                        return '#E43A45'
                                    }
                                }

                                return '#E43A45'
                            }
                            initGrafica(`barras_${index}`, r.graficas[key], key)
                            initPastel(`pastel_${index}`, r.pasteles[key])
                            
                            await wait()
                            await $scope.saveImg($(`#barras_${index} canvas`)[0], `barras_${index}`)
                            await $scope.saveImg($(`#pastel_${index} canvas`)[0], `pastel_${index}`)
                        }
                        resolve()
                    }, 250)
                }
            }, Object.assign({ grupos : $scope.rangos }, $scope.produccion.params))
        })
    }

    $scope.saveImg = (canvas, name) => {
        return new Promise(async (resolve) => {

            canvas.style.height = 250
            canvas.style.width = 1100

            let base64 = canvas.toDataURL()
            await $request.saveImg({ name, finca : $scope.produccion.params.finca, fecha : $scope.produccion.params.fecha_inicial , data : base64 })
            resolve()
        })
    }

    $scope.init = async () => {
        await $scope.openGraficasAdicionales()
        await $scope.loadPasteles()
        window.location.href = 'pdfProduccionDiaRender.php?fecha_inicial='+$scope.produccion.params.fecha_inicial+'&finca='+$scope.produccion.params.finca
    }

    $scope.init()

}]);
