
app.service('request',[ '$http' , function($http){

    this.data = function(params , callback){
        var url = 'phrapi/marcel/perchas/data'
        var data = params || {}
        $http.post(url, data).then((r) => {
            callback(r.data)
        });
    }

    this.last = function(callback, params){
        var url = 'phrapi/marcel/perchas/last'
        var data = params || {}
        $http.post(url, data).then((r) => {
            callback(r.data)
        });
    }

}])

app.filter('inArray', () => {
    return (data, key, arr = []) => {
        if(data && key){
            if(arr)
                return data.filter((row) => arr.includes(row[key]))
            else
                return data
        } else
            return []
    }
})

app.controller('perchas', ['$scope', 'request', function ($scope, $request) {
    
    
    $scope.logged = "<?= Session::getInstance()->logged ?>";

    $scope.filters = {
        fecha_inicial : moment().startOf('month').format('YYYY-MM-DD'),
        fecha_final : moment().endOf('month').format('YYYY-MM-DD'),
    }

    $scope.changeRangeDate = (data) => {
        if(data){
            $scope.filters.fecha_inicial = data.hasOwnProperty("first_date") ? data.first_date : $scope.filters.fecha_inicial;
            $scope.filters.fecha_final = data.hasOwnProperty("second_date") ? data.second_date : $scope.filters.fecha_final;

            $scope.getDataTable()
        }
    }

    $scope.init = () => {
        $request.last((r) => {
            $scope.filters.fecha_inicial = r.fecha
            $scope.filters.fecha_final = r.fecha

            setTimeout(() => {
                $("#date-picker").html(`${r.fecha} - ${r.fecha}`)
                $scope.getDataTable()
            }, 500)
        })
    }

    $scope.getDataTable = () => {
        var response = (r) => {
            $scope.data = r.data
            $scope.locales = r.locales
            $scope.clientes = r.clientes

            printTags(r.tags)
            printGraficaPastelKilos(r.data.kilos_totales, r.data.kilos_destruidos)
            printGraficaPastelFruta(r.data.porc_fruta_lancofruit, r.data.porc_fruta_terceros, r.data.porc_percha_vacia)
            printGraficaPastelPerchas(r.data.total_perchas_frutas_verduras, r.data.total_perchas_solo_banano)
            printGraficaBarraGrados(r.data.grado_1, r.data.grado_2, r.data.grado_3, r.data.grado_4, r.data.grado_5, r.data.grado_6)
            printGraficaPastelCategorias(r.categorias.main)
            printGraficaPastelDefectos(r.categorias.defectos, r.categorias.categorias)
        }
        $request.data($scope.filters, response)
    }

    $scope.filterLocales = () => {
        var response = (r) => {
            $scope.data = r.data

            printTags(r.tags)
            printGraficaPastelKilos(r.data.kilos_totales, r.data.kilos_destruidos)
            printGraficaPastelFruta(r.data.porc_fruta_lancofruit, r.data.porc_fruta_terceros, r.data.porc_percha_vacia)
            printGraficaPastelPerchas(r.data.total_perchas_frutas_verduras, r.data.total_perchas_solo_banano)
            printGraficaBarraGrados(r.data.grado_1, r.data.grado_2, r.data.grado_3, r.data.grado_4, r.data.grado_5, r.data.grado_6)
            printGraficaPastelCategorias(r.categorias.main)
            printGraficaPastelDefectos(r.categorias.defectos, r.categorias.categorias)
        }
        $request.data($scope.filters, response)
    }

    printGraficaPastelCategorias = (data) => {
        var data = {
            data: data || [],
            id: "pastel-categorias",
            height: "charts-400",
            nameseries : "categorias"
        }

        let parent = $("#pastel-categorias").parent()
        $("#pastel-categorias").remove()
        parent.append(`<div id="pastel-categorias" class="charts"></div>`)
        ReactDOM.render(React.createElement(Pastel, data), document.getElementById('pastel-categorias'));
    }

    printGraficaPastelDefectos = (data, categorias) => {
        $scope.categorias = categorias
        // la categoria seleccionada existe
        if(categorias.indexOf($scope.filters.categoria) == -1){
            $scope.filters.categoria = categorias[0]
        }
        $scope.data_defectos = data

        $scope.reloadGraficaDefectos()
    }

    $scope.reloadGraficaDefectos = () => {
        var d = $scope.data_defectos[$scope.filters.categoria]
        var options = { 
            data: d,
            id: "pastel-defectos",
            height: "charts-400",
            nameseries : "defectos"
        }
        
        let parent = $("#pastel-defectos").parent()
        $("#pastel-defectos").remove()
        parent.append(`<div id="pastel-defectos" class="charts"></div>`)
        ReactDOM.render(React.createElement(Pastel, options), document.getElementById('pastel-defectos'));
    }

    printTags = (tags) => {
        var options = {
            colums : 3,
            tags: tags,
            withTheresholds: false
        }
        ReactDOM.render(React.createElement(ListTags, options), document.getElementById('indicadores'));
    }

    printGraficaPastelKilos = (kilosTotales, kilosDestruidos) => {
        var options = {
            data: [
                {
                    label : 'KILOS TOTALES',
                    value : kilosTotales || 0
                },
                {
                    label : 'KILOS DESTRUIDOS',
                    value : kilosDestruidos || 0
                }
            ],
            id: "pastel-kilos",
            height: "charts-400",
            nameseries : "kilos"
        }
        let parent = $("#pastel-kilos").parent()
        $("#pastel-kilos").remove()
        parent.append(`<div id="pastel-kilos" class="charts"></div>`)
        ReactDOM.render(React.createElement(Pastel, options), document.getElementById('pastel-kilos'));
    }

    printGraficaPastelPerchas = (frutasVerduras, banano) => {
        var options = {
            data: [
                {
                    label : 'PERCHAS FRUTAS Y VERDURAS',
                    value : frutasVerduras || 0
                },
                {
                    label : 'PERCHAS SOLO BANANO',
                    value : banano || 0
                }
            ],
            id: "pastel-perchas",
            height: "charts-400",
            nameseries : "perchas"
        }
        let parent = $("#pastel-perchas").parent()
        $("#pastel-perchas").remove()
        parent.append(`<div id="pastel-perchas" class="charts"></div>`)
        ReactDOM.render(React.createElement(Pastel, options), document.getElementById('pastel-perchas'));
    }

    printGraficaPastelFruta = (lancofruit, terceros, vacia) => {
        var options = {
            data: [
                {
                    label : '% Fruta Lancofruit',
                    value : lancofruit || 0
                },
                {
                    label : '% Fruta Terceros',
                    value : terceros || 0
                },
                {
                    label : '% Percha vacía',
                    value : vacia || 0
                }
            ],
            id: "pastel-fruta",
            height: "charts-400",
            nameseries : "fruta"
        }
        let parent = $("#pastel-fruta").parent()
        $("#pastel-fruta").remove()
        parent.append(`<div id="pastel-fruta" class="charts"></div>`)
        ReactDOM.render(React.createElement(Pastel, options), document.getElementById('pastel-fruta'));
    }

    printGraficaBarraGrados = (grado1, grado2, grado3, grado4, grado5, grado6) => {
        var options = {
            data: [
                { label : 'GRADO 1', value: grado1 },
                { label : 'GRADO 2', value: grado2 },
                { label : 'GRADO 3', value: grado3 },
                { label : 'GRADO 4', value: grado4 },
                { label : 'GRADO 5', value: grado5 },
                { label : 'GRADO 6', value: grado6 },
            ],
            umbral: 0,
            height: "charts-400",
            id: "barras-grados"
        }
        let parent = $("#barras-grados").parent()
        $("#barras-grados").remove()
        parent.append(`<div id="barras-grados" class="charts"></div>`)
        ReactDOM.render(React.createElement(Barras, options), document.getElementById('barras-grados'));
    }

    $scope.init()
}]);