const { app, load } = window

app.filter('sumOfValue', function () {
    return function (data, key) {
        if (angular.isUndefined(data) || angular.isUndefined(key))
            return 0;        
        var sum = 0;        
        angular.forEach(data,function(value){
            if(parseFloat(value[key]))
                sum = sum + parseFloat(value[key], 10);
        });        
        return sum;
    }
})

app.filter('orderObjectBy', function() {
	return function(items, field, reverse) {
    	var filtered = [];
    	angular.forEach(items, function(item) {
    		if(!isNaN(parseInt(item))){
    			item = parseInt(item);
    		}
      		filtered.push(item);
    	});
    	filtered.sort(function (a, b) {
            if(field == 'hora' || field == 'fecha'){
                return moment(a.date).isAfter(b.date) ? 1 : -1;
            }else if(parseFloat(a[field]) && parseFloat(b[field])){
				return (parseFloat(a[field]) > parseFloat(b[field]) ? 1 : -1);
			}else{
				return (a[field] > b[field] ? 1 : -1);
			}
    	});
    	if(reverse) filtered.reverse();
    	return filtered;
  	};
});

app.filter('orderBy', function() {
	return function(items, reverse) {
    	var filtered = [];
    	angular.forEach(items, function(item) {
    		if(!isNaN(parseInt(item))){
    			item = parseInt(item);
    		}
      		filtered.push(item);
    	});
    	filtered.sort(function (a, b) {
            if(parseFloat(a) && parseFloat(b)){
				return (parseFloat(a) > parseFloat(b) ? 1 : -1);
			}else{
				return (a > b ? 1 : -1);
			}
    	});
    	if(reverse) filtered.reverse();
    	return filtered;
  	};
});

app.service('request', ['$http', function($http){
    this.last = (callback, params) => {
        $http.post('phrapi/precalibracion/last', params ||  {}).then((r) => {
            callback(r.data)
        })
    }
    this.precalibracion = (callback, params) => {
        $http.post('phrapi/precalibracion/index', params ||  {}).then((r) => {
            callback(r.data)
        })
    }
    this.precalibrado = (callback, params) => {
        load.block('precalibrado')
        $http.post('phrapi/precalibracion/precalibrado', params ||  {}).then((r) => {
            load.unblock('precalibrado')
            callback(r.data)
        })
    }
    this.porprecalibrar = (callback, params) => {
        load.block('por_precalibrar')
        $http.post('phrapi/precalibracion/porprecalibrar', params ||  {}).then((r) => {
            load.unblock('por_precalibrar')
            callback(r.data)
        })
    }
    this.porcosechar = (callback, params) => {
        load.block('por_cosechar')
        $http.post('phrapi/precalibracion/porcosechar', params ||  {}).then((r) => {
            load.unblock('por_cosechar')
            callback(r.data)
        })
    }
    this.barrer = (callback, params) => {
        load.block('por_cosechar')
        $http.post('phrapi/precalibracion/barrer', params ||  {}).then((r) => {
            load.unblock('por_cosechar')
            callback(r.data)
        })
    }
    this.database = (callback, params) => {
        load.block('database')
        $http.post('phrapi/precalibracion/database', params ||  {}).then((r) => {
            load.unblock('database')
            callback(r.data)
        })
    }
    this.guardarEditar = (callback, params) => {
        load.block()
        $http.post('phrapi/precalibracion/guardarEditar', params ||  {}).then((r) => {
            load.unblock()
            callback(r.data)
        })
    }
    this.borrar = (callback, params) => {
        load.block()
        $http.post('phrapi/precalibracion/borrar', params ||  {}).then((r) => {
            load.unblock()
            callback(r.data)
        })
    }
}])

app.controller('produccion', ['$scope', 'request', '$filter', function($scope, $request, $filter){

    $scope.totales = {}
    $scope.filters = {
        year : moment().year(),
        week : moment().week()
    }

    $scope.coloresClass = {
        'VERDE' : 'bg-green-haze bg-font-green-haze',
        'AZUL' : 'bg-blue bg-font-blue',
        'BLANCO' : 'bg-white bg-font-white',
        'LILA' : 'bg-purple-studio bg-font-purple-studio',
        'ROJA' : 'bg-red-thunderbird bg-font-red-thunderbird',
        'CAFE' : 'bg-brown bg-font-brown',
        'AMARILLO' : 'bg-yellow-lemon bg-font-yellow-lemon',
        'NEGRO' : 'bg-dark bg-font-dark'
    }

    $scope.init = () => {
        /*load.block('precalibracion')
        /*$request.precalibracion((r) => {
            load.unblock('precalibracion')
            $scope.colorClass = r.color.class
            $scope.edades = r.edades
            $scope.precalibracion = r.data
            $scope.reloadTotales()
        }, $scope.filters)*/

        $request.porprecalibrar((r) => {
            $scope.por_precalibrar = r.data
            $scope.por_precalibrar_edades = r.edades

            setTimeout(() => {
                $('[data-toggle="tooltip"]').tooltip({html: true})
            }, 1000)
        }, $scope.filters)

        $request.porcosechar((r) => {
            $scope.por_cosechar = r.data

            setTimeout(() => {
                $('[data-toggle="tooltip"]').tooltip({html: true})
            }, 1000)
        }, $scope.filters)

        $request.precalibrado((r) => {
            $scope.precalibrado = r.data
            $scope.precalibrado_edades = r.edades
        }, $scope.filters)

        $request.database((r) => {
            $scope.lotes = r.lotes
            $scope.database = r.data
        }, $scope.filters)
    }

    const getUmbral = (value) => {
        if(value){
            if(value < 98)
                return 'bg-red-thunderbird bg-font-red-thunderbird'
            else if (value == 98)
                return 'bg-yellow-gold bg-font-yellow-gold'
            else
                return 'bg-green-haze bg-font-green-haze'
        }
    }
    $scope.getUmbral = getUmbral

    $scope.reloadTotales = () => {
        setTimeout(() => {
            $scope.$apply(() => {
                $scope.totales.racimos_enfunde = $filter('sumOfValue')($scope.precalibracion, 'racimos_enfunde')
                $scope.totales.recobro = $scope.totales.cosechados > 0 && $scope.totales.racimos_enfunde > 0 ? ($scope.totales.cosechados / $scope.totales.racimos_enfunde * 100) : 0
                $scope.totales.caidos = $filter('sumOfValue')($scope.precalibracion, 'caidos') || 0
                $scope.totales.porc_caidos = $scope.totales.caidos > 0 && $scope.totales.racimos_enfunde > 0 ? ($scope.totales.caidos / $scope.totales.racimos_enfunde * 100) : 0
            })
        }, 100)
    }

    $scope.select = (semana) => {
        $scope.enfunde = semana
    }

    $scope.getUmbral = (value) => {
        if(value){
            if(value < 98)
                return 'danger'
            else if (value == 98)
                return 'warning'
            else
                return 'success'
        }
    }

    $scope.exportExcel = function(id_table, title){
        var tableToExcel = (function() {
            var uri = 'data:application/vnd.ms-excel;base64,'
                , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body><table>{table}</table></body></html>'
                , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
                , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
            return function(table, name) {
                if (!table.nodeType) table = document.getElementById(table)
                var contentTable = table.innerHTML
                var ctx = {worksheet: name || 'Worksheet', table: contentTable}
                window.location.href = uri + base64(format(template, ctx))
            }
        })()
        tableToExcel(id_table, title)
    }

    $scope.barrer = () => {
        $request.barrer(() => {
            location.reload()
        }, $scope.enfunde)
    }

    $scope.editar = (row) => {
        let modal = $("#editarModal")
        modal.modal('show')

        row.cantidad = Number(row.cantidad) || 0
        row.edad = Number(row.edad) || 0
        $scope.rowSelected = angular.copy(row)
    }

    $scope.guardarEditar = () => {
        const data = angular.copy($scope.rowSelected)
        try {
            const validar = {
                calibrador :    'Favor de colocar calibrador',
                lote :          'Favor de colocar lote',
                edad :          'Favor de colocar edad',
                cantidad :      'Favor de colocar cantidad'
            }
            for(let key in validar){
                if(!data[key])
                    throw validar[key]
            }
            
            $request.guardarEditar((r) => {
                if(r.status === 200){
                    toastr.success('Guardado')
                }else{
                    toastr.error('Algo inesperado paso, Contacte a soporte')
                }
            }, data)
        }
        catch(e){
            toastr.error(e)
        }
    }

    $scope.borrar = async (row) => {
        const data = angular.copy(row)

        let res = await swal({
            text: '¿Seguro de eliminar?',
            buttons : ['No', 'Si']
        })
        if(res){
            load.block()
            $request.borrar((r) => {
                load.unblock()

                if(r.status === 200){
                    toastr.success('Eliminado')
                    $scope.init()
                }else{
                    toastr.error('Algo inesperado paso, Contacte a soporte')
                }
            }, data)
        }
    }

    $scope.last = () => {
        $request.last((r) => {
            $scope.years = r.years
            $scope.semanas = r.weeks
            $scope.semanas_enfunde = r.weeks_enfunde
            $scope.filters.week = r.last_week
            $scope.fincas = r.fincas
            $scope.filters.id_finca = 1
            $scope.init()
        }, $scope.filters)
    }

    $scope.last()
}]);

