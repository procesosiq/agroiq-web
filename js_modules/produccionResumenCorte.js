app.service('request', ['client' , '$http', function(client, $http){
    this.lastDay = function(callback, params){
        let data = params || {}
        let url = 'phrapi/marun/produccion2/last'
        client.post(url, callback, data)
    }

    this.filters = function(callback, params){
        let data = params || {}
        let url = 'phrapi/marun/resumencorte/filters'
        client.post(url, callback, data)
    }

    this.racimosEdad = function(callback, params){
        let data = params || {}
        let url = 'phrapi/marun/produccion2/edad'
        client.post(url, callback, data, 'table_por_edad')
    }

    this.guiasRemicion = function(callback, params){
        let url = 'phrapi/marun/cajas/cuadrar'
        let data = params || {}
        client.post(url, callback, data, 'table_4')
    }

    this.resumenCajas = function(callback, params){
        let url = 'phrapi/marun/resumencorte/cajas'
        let data = params || {}
        client.post(url, callback, data, 'div_table_2')
    }

    this.resumenProduccion = function(callback, params){
        let url = 'phrapi/marun/resumencorte/resumenProduccion'
        let data = params || {}
        client.post(url, callback, data, 'resumen_produccion')
    }

    this.racimosLote = function(callback, params){
        let data = params || {}
        let url = 'phrapi/marun/produccion2/lote'
        client.post(url, callback, data)
    }
}])

app.filter('sumOfValue', function () {
    return function (data, key) {        
        if (angular.isUndefined(data) || angular.isUndefined(key))
            return 0;        
        var sum = 0;
        angular.forEach(data,function(value){
            if(value[key] != "" && value[key] != undefined && parseFloat(value[key])){
                sum = sum + parseFloat(value[key], 10);
            }
        });
        return sum;
    }
})

app.filter('avgOfValue', function () {
    return function (data, key) {        
        if (angular.isUndefined(data) || angular.isUndefined(key))
            return 0;        
        var sum = 0;
        var count = 0;
        angular.forEach(data,function(value){
            if(value[key] != "" && value[key] != undefined && parseFloat(value[key])){
                sum = sum + parseFloat(value[key], 10);
                count++;
            }
        });
        sum = sum / count;
        return sum;
    }
})

app.filter('orderObjectBy', function() {
	return function(items, field, reverse) {
    	var filtered = [];
    	angular.forEach(items, function(item) {
            if(field == 'hora' || field == 'fecha'){
                item.date = moment(item.fecha  + ' ' + item.hora)
            }
    		if(!isNaN(parseInt(item))){
    			item = parseInt(item);
    		}
      		filtered.push(item);
    	});
    	filtered.sort(function (a, b) {
            if(field == 'hora' || field == 'fecha'){
                return moment(a.date).isAfter(b.date) ? 1 : -1;
            }else if(parseFloat(a[field]) && parseFloat(b[field])){
				return (parseFloat(a[field]) > parseFloat(b[field]) ? 1 : -1);
			}else{
				return (a[field] > b[field] ? 1 : -1);
			}
    	});
    	if(reverse) filtered.reverse();
    	return filtered;
  	};
});

let datesEnabled = []
app.controller('produccion', ['$scope', 'request', function($scope, $request){

    let datepickerHighlight = () => {
        $('#datepicker').datepicker({
            beforeShowDay: function (date) {
                let fecha = moment(date).format('YYYY-MM-DD')
                let has = datesEnabled.indexOf(fecha) > -1
                return has ? { classes: 'highlight', tooltip: 'Procesado', enabled : true } : { tooltip : 'Sin proceso', enabled : false }
            }
        });
        $('#datepicker').datepicker()
        .on('changeDate', function(e) {
            $scope.changeRangeDate({ first_date: moment(e.date).format('YYYY-MM-DD'), second_date : moment(e.date).format('YYYY-MM-DD') })
        });
        $("#datepicker").datepicker('setDate', $scope.filters.fecha_inicial)
        $('#datepicker').datepicker('update')
    }

    $scope.filters = {
        id_finca : '',
        finca : '',
        idFinca : '',
        fecha_inicial : moment().startOf('month').format('YYYY-MM-DD'),
        fecha_final : moment().endOf('month').format('YYYY-MM-DD'),
        unidad : 'kg'
    }

    $scope.tabla = {
        
    }

    $scope.changeRangeDate = function(data){
        if(data){
            $scope.filters.fecha_inicial = data.hasOwnProperty("first_date") ? data.first_date : $scope.wizardStep.params.fecha_inicial;
            $scope.filters.fecha_final = data.hasOwnProperty("second_date") ? data.second_date : $scope.wizardStep.params.fecha_final;
        }
        
        $scope.init()
    }

    $scope.getLastDay = function(){
        $request.lastDay(function(r, b){
            b()
            if(r){
                datesEnabled = r.days
                $scope.fincas = r.fincas
                
                $scope.fecha = r.last.fecha
                $scope.filters.fecha_inicial = r.last.fecha
                $scope.filters.fecha_final = r.last.fecha
                datepickerHighlight()
                //$scope.init()
            }
        })
    }
    
    $scope.init = () => {
        var data = angular.copy($scope.filters);

        $request.filters(function(r, b){
            $scope.fincas = r.fincas
            let ids = r.fincas.map((f) => f.id)
            if(ids.length > 0){
                if(ids.indexOf($scope.filters.id_finca) == -1){
                    $scope.filters.id_finca = ids[0]
                    $scope.filters.finca = ids[0]
                    $scope.filters.idFinca = ids[0]

                    data = angular.copy($scope.filters);
                }
            }

            setTimeout(() => {
                $("#finca").val($scope.filters.id_finca)
            }, 250)

            $request.racimosLote(function(r, b){
                b()
                if(r){
                    $scope.totales = r.totales;
                }
            }, data)
    
            $request.racimosEdad(function(r, b){
                b('table_por_edad')
                if(r){
                    $scope.tabla.edades = r.data;
                }
            }, data)
    
            $request.guiasRemicion(function(r, b){
                b('table_4')
                if(r){
                    $scope.cuadrarCajas = r.data
                    $scope.guias = r.guias
                }
            }, data)
    
            $request.resumenCajas(function(r, b){
                b('div_table_2')
                if(r){
                    $scope.resumen = r.data
                }
            }, data)
    
            $request.resumenProduccion(function(r, b){
                b('resumen_produccion')
                if(r){
                    $scope.resumenProduccion = r.data
                }
            }, data)
        }, data)
    }

    $scope.waitInit = async () => {
        $scope.filters.finca = $scope.filters.id_finca
        $scope.filters.idFinca = $scope.filters.id_finca
        $scope.promesa()
        .then(() => {
            $scope.init()
        })
    }

    $scope.promesa = () => {
        return new Promise((resolve, reject) => {
            setTimeout(()=> {
                resolve()
            }, 500)
        })
    }

    $scope.getLastDay()

}]);

