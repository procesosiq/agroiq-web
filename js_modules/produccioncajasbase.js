const { app, load } = window

app.service('request', ['client', '$http', function(client, $http){
    this.registros = function(callback, params){
        let url = 'phrapi/marun/cajas/registros'
        let data = params || {}
        client.post(url, callback, data, 'tabla_base')
    }
    
    this.last = function(callback, params){
        let url = 'phrapi/marun/cajas/last'
        let data = params || {}
        client.post(url, callback, data)
    }

    this.getMarcas = function(callback, params){
        let url = 'phrapi/marun/cajas/marcas'
        let data = params || {}
        client.post(url, callback, data)
    }
    
    this.eliminar = function(callback, params){
        let url = 'phrapi/marun/cajas/eliminar'
        let data = params || {}
        client.post(url, callback, data)
    }

    this.guardarCaja = function(callback, params){
        let url = 'phrapi/marun/cajas/guardarCaja'
        let data = params || {}
        client.post(url, callback, data)
    }

}])

app.filter('sumOfValue', function () {
    return function (data, key) {        
        if (angular.isUndefined(data) || angular.isUndefined(key))
            return 0;        
        var sum = 0;
        angular.forEach(data,function(value){
            if(value[key] != "" && value[key] != undefined){
                sum = sum + parseFloat(value[key], 10);
                count++;
            }
        });
        return sum;
    }
})

app.filter('avgOfValue', function () {
    return function (data, key) {        
        if (angular.isUndefined(data) || angular.isUndefined(key))
            return 0;        
        var sum = 0;
        var count = 0;
        angular.forEach(data,function(value){
            if(value[key] != "" && value[key] != undefined){
                sum = sum + parseFloat(value[key], 10);
                count++;
            }
        });
        sum = sum / count;
        if(isNaN(sum))
            return 0;
        return sum;
    }
})

app.filter('orderObjectBy', function() {
	return function(items, field, reverse) {
    	var filtered = [];
    	angular.forEach(items, function(item) {
            if(field == 'hora' || field == 'fecha'){
                item.date = moment(item.fecha  + ' ' + item.hora)
            }
    		if(!isNaN(parseInt(item))){
    			item = parseInt(item);
    		}
      		filtered.push(item);
    	});
    	filtered.sort(function (a, b) {
            if(field == 'hora' || field == 'fecha'){
                return moment(a.date).isAfter(b.date) ? 1 : -1;
            }else if(parseFloat(a[field]) && parseFloat(b[field])){
				return (parseFloat(a[field]) > parseFloat(b[field]) ? 1 : -1);
			}else{
				return (a[field] > b[field] ? 1 : -1);
			}
    	});
    	if(reverse) filtered.reverse();
    	return filtered;
  	};
});

function in_array(value, array){
    if(!Array.isArray(array)) return false
    return array.indexOf(value) >= 0
}

function getParam(name){
    let url_string = window.location.href
    let url = new URL(url_string);
    return url.searchParams.get(name);
}

let datesEnabled = []

app.controller('produccion', ['$scope','request', '$filter', function($scope, $request, $filter){

    const datepickerHighlight = () => {
        $('#datepicker').datepicker({
            language : 'es',
            beforeShowDay: function (date) {
                let fecha = moment(date).format('YYYY-MM-DD')
                let has = datesEnabled.indexOf(fecha) > -1
                return has ? { classes: 'highlight', tooltip: 'Procesado', enabled : true } : { tooltip : 'Sin proceso', enabled : false }
            }
        });
        $('#datepicker').datepicker()
        .on('changeDate', function(e) {
            $scope.table.fecha_inicial = moment(e.date).format('YYYY-MM-DD')
            $scope.changeRangeDate()
        });
        $("#datepicker").datepicker('setDate', $scope.table.fecha_inicial)
        $('#datepicker').datepicker('update')
    }
    
    $scope.registros = []
    $scope.seleccionados = []
    $scope.editar = {
        id_marca : '',
        peso : 0
    }
    $scope.table = {
        pagination : 10,
        startFrom : 0,
        actualPage : 1,
        fecha_inicial : moment().format('YYYY-MM-DD'),
        fecha_final : moment().format('YYYY-MM-DD'),
        search : {},
        year : moment().year(),
        semana : moment().week(),
        unidad : 'lb',
        finca : 0,
        var : 'CONV'
    }

    $scope.changeRangeDate = function () {
        $scope.table.fecha_final = $scope.table.fecha_inicial;
        $scope.getRegistros();
    }

    $scope.next = function(dataSource){
        if($scope.searchTable.actual_page < $scope.searchTable.numPages) {
            $scope.searchTable.actual_page++;
            $scope.searchTable.startFrom = ($scope.searchTable.actual_page - 1) * (parseInt($scope.searchTable.limit))
        }
    }

    $scope.prev = function(dataSource){
        if($scope.searchTable.actual_page > 1){
            $scope.searchTable.actual_page--;
            $scope.searchTable.startFrom = ($scope.searchTable.actual_page - 1) * (parseInt($scope.searchTable.limit))
        }
    }

    $scope.setOrderTable = function(field){
        var reverse = false
        if(field == $scope.searchTable.orderBy){
            reverse = !$scope.searchTable.reverse
        }else{
            $scope.searchTable.orderBy = field
        }
        $scope.searchTable.reverse = reverse

        $scope.registros = $filter('orderObjectBy')($scope.registros, $scope.searchTable.orderBy, $scope.searchTable.reverse)
    }

    $scope.getRegistros = function(){
        $scope.seleccionados = []
        
        let data = $scope.table
        load.block('marcas')
        $request.registros(function(r, b){
            b('tabla_base')
            load.unblock('marcas')
            if(r){
                $scope.table.finca = r.id_finca
                $scope.fincas = r.fincas
                $scope.marcas = r.marcas
                $scope.peso_prom_cajas = r.pesos_prom_cajas
                $scope.registros = r.data
                $scope.table.unidad = r.unidad
                setTimeout(function(){
                    $scope.searchTable.changePagination()
                }, 500)
            }
        }, data)
    }

    $scope.eliminar = () => {
        if($scope.seleccionados.length > 0){
            if(confirm(`Seguro deseas eliminar ${$scope.seleccionados.length} elementos?`)){
                $request.eliminar((r, b) => {
                    b()
                    if(r){
                        $scope.getRegistros()
                        alert(`Se borraron ${$scope.seleccionados.length} registros con éxito`, "", "success")
                    }
                }, { ids : $scope.seleccionados })
            }
        }else{
            alert("No has seleccionado ninguna fila")
        }
    }

    $scope.editar = (row, index, value = null) => {
        row.delete = value != null ? value : !row.delete
        row._delete = angular.copy(row.delete)

        if(!row.delete && $scope.seleccionados.includes(row.id)){
            $scope.seleccionados.splice($scope.seleccionados.indexOf(row.id), 1)
        }else if(row.delete){
            $scope.seleccionados.push(row.id)
        }
    }

    $scope.seleccionarTodo = () => {
        let listado = $scope.registros
        listado = $filter('filter')(listado, $scope.table.search)
        listado = $filter('startFrom')(listado, $scope.searchTable.startFrom)
        listado = $filter('limitTo')(listado, $scope.searchTable.limit)

        for(let i in $scope.registros){
            $scope.registros[i].delete = false
        }
        for(let i in listado){
            let row = listado[i]
            for(let j in $scope.registros){
                let row2 = $scope.registros[j]
                if(row.id && row2.id && row.id == row2.id){
                    $scope.editar(row2, i, true)
                    break;
                }
            }
        }
    }

    $scope.deseleccionarTodo = () => {
        let listado = $scope.registros
        listado = $filter('filter')(listado, $scope.table.search)
        listado = $filter('startFrom')(listado, $scope.searchTable.startFrom)
        listado = $filter('limitTo')(listado, $scope.searchTable.limit)

        for(let i in $scope.registros){
            $scope.registros[i].delete = false
        }
        for(let i in listado){
            let row = listado[i]
            for(let j in $scope.registros){
                let row2 = $scope.registros[j]
                if(row.id == row2.id){
                    console.log(row.id, row2.id)
                    $scope.editar(row2, i, false)
                    break;
                }
            }
        }
    }

    $scope.guardar = () => {
        let rows = $("tr.delete").length
        if(rows == $scope.seleccionados.length){
            if(confirm(`Seguro deseas editar ${$scope.seleccionados.length} elementos?`)){
                $request.guardarCaja((r, b) => {
                    b()
                    if(r.status == 200){
                        $("#editar-modal").modal('hide')
                        toastr.success('Guardado')
                        $scope.getRegistros()
                    }else{
                        alert(r.message)
                    }
                }, { ids : $scope.seleccionados, id_marca : $scope.editar.id_marca, peso : $scope.editar.peso })
            }
        }else{
            alert('Algo inesperado paso, favor refresque (F5)')
        }
    }

    $scope.last = function(newDate, fecha){
        return new Promise((resolve) => {
            $request.last(function(r, b){
                b()
                if(r){
                    $scope.fincas = r.fincas
                    $scope.available_fincas = r.available_fincas

                    if(Object.keys(r.fincas).length > 0)
                    if(Object.keys(r.fincas).indexOf($scope.table.finca) == -1){
                        $scope.table.finca = Object.keys(r.fincas)[0]
                        $scope.anioCambio = true
                    }

                    if(r.days){
                        datesEnabled = r.days
                    }
    
                    if(newDate){
                        $scope.table.fecha_inicial = fecha || r.last.fecha
                        $scope.table.fecha_final = fecha || r.last.fecha
                        $("#date-picker").html($scope.table.fecha_inicial + ' - '+$scope.table.fecha_final)
                        $scope.anioCambio = true
                    }
                }

                resolve()
            }, { newDate : newDate, filters: $scope.table })
        })
    }

    $scope.searchTable = {
        orderBy : "hora",
        reverse : true,
        limit : 10,
        actual_page : 1,
        startFrom : 0,
        optionsPagination : [
            10, 50, 100
        ],
        numPages : 0,
        calculatePages : function(){
            $scope.searchTable.numPages = 
                parseInt($filter('filter')($scope.registros, $scope.table.search).length / $scope.searchTable.limit)
                    + ($filter('filter')($scope.registros, $scope.table.search).length % $scope.searchTable.limit == 0 ? 0 : 1)
        },
        changePagination : function(){
            if($scope.searchTable.limit == 0)
                $scope.searchTable.limit = $scope.registros.length

            setTimeout(function(){
                $scope.$apply(function(){
                    $scope.searchTable.numPages = parseInt($filter('filter')($scope.registros, $scope.table.search).length / $scope.searchTable.limit)  + ($filter('filter')($scope.registros, $scope.table.search).length % $scope.searchTable.limit == 0 ? 0 : 1)
                    $scope.searchTable.actual_page = 1
                    $scope.searchTable.startFrom = 0
                })
            }, 250)
        }
    }

    $scope.getClassUmbral = (marca) => {
        if(marca.class != ''){
            return marca.class
        } else {
            return ''
        } 
    }

    $scope.exportExcel = function(id_table){
        var data = new Blob([document.getElementById(id_table).outerHTML], {
            type : 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf8'
        })
        saveAs(data, 'Cajas.xls')
    }

    $scope.exportPrint = function(id_table){
        
        let image = '<div style="display:block; height:55;"><img style="float: right;" width="100" height="50" src="./../logos/Logo.png" /></div><br>';
        let table = document.getElementById(id_table);
        var contentTable = table.outerHTML;

        if(id_table == 'table_1'){
            var cut = contentTable.search('<tr role="row" class="filter">')
            var cut2 = contentTable.search('</thead>')
            var part1 = contentTable.substring(0, cut)
            var part2 = contentTable.substring(cut2, contentTable.length)
            contentTable = part1 +part2
        }

        newWin = window.open("");
        newWin.document.write(contentTable);
        newWin.print();
        newWin.close();
    }

    $scope.hideActions = () => {
        $("#actions-listado").addClass("hide")
    }
    $scope.showActions = () => {
        $("#actions-listado").removeClass("hide")
    }
    $scope.not_selected_marca = (index) => {
        var array = []
        $scope.marcasSeleccionadas.map((value, i) => {
            if(i != index) array.push(value)
        })
        return in_array($scope.marcasSeleccionadas[index], array)
    }

    let fecha = getParam('fecha')
    $scope.last(true, fecha)
    .then(() => {
        datepickerHighlight()
    })

}]);