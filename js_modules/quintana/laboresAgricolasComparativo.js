function arrayMin(arr, key) {
    let min = null
    arr.forEach((val) => {
        if(val[key]){
            if(min == null || val[key] < min) min = val[key]
        }
    })
    return min
}

app.service('request', ['$http', ($http) => {
    var service = {}

    service.last = (callback, params) => {
        load.block()
        $http.post('phrapi/laboresAgricolasComparativo/last', params || {}).then((r) => {
            load.unblock()
            callback(r.data)
        })
    }

    service.variables = (callback, params) => {
        load.block()
        $http.post('phrapi/laboresAgricolasComparativo/variables', params || {}).then((r) => {
            load.unblock()
            callback(r.data)
        })
    }

    service.index = (callback, params) => {
        load.block()
        $http.post('phrapi/laboresAgricolasComparativo/index', params || {}).then((r) => {
            load.unblock()
            callback(r.data)
        })
    }

    service.tablaPorCausa = (callback, params) => {
        load.block('tablaPorCausa')
        load.block('calidadCausa')
        $http.post('phrapi/laboresAgricolasComparativo/tablaPorCausa', params || {}).then((r) => {
            load.unblock('tablaPorCausa')
            load.unblock('calidadCausa')
            callback(r.data)
        })
    }

    return service
}])

app.filter('avgOfValue', function () {
    return function (data, key) {        
        if (angular.isUndefined(data) || angular.isUndefined(key))
            return 0;        
        var sum = 0;
        var count = 0;
        angular.forEach(data,function(value){
            if(value[key] != "" && value[key] != undefined){
                sum = sum + parseFloat(value[key], 10);
                count++;
            }
        });
        sum = sum / count;
        if(isNaN(sum))
            return 0;
        return sum;
    }
})

app.filter('avg', () => {
    return (input) => {
        let count = 0, sum = 0
        input.forEach((val) => {
            if(parseFloat(val)){
                count++
                sum+=val
            }
        })
        return sum/count
    }
})

app.controller('controller', ['$scope', 'request', function($scope, $request){

    $scope.filters = {
        fecha_inicial : '',
        fecha_final : '',
        id_finca : 0,
        tablaPorCausa : { labor : 0 },
        calidadLabor : {},
        calidadCausa : {}
    }

    responseTablaPorCausa = (data) => {
        $scope.filters.calidadCausa.causa = ''
        $scope.tabla_por_causa = data
    }

    $scope.tablaPorCausa = () => {
        $scope.labor = $scope.tabla_por_labor.labores.filter((l) => l.idLabor == $scope.filters.tablaPorCausa.labor)[0].nombre
        $request.tablaPorCausa((r) => {
            responseTablaPorCausa(r)
            setTimeout(() => {
                $scope.getGraficaPorCausa(r.fincas, r.data)
                $scope.getGraficaPorLabor($scope.tabla_por_labor.fincas, $scope.tabla_por_labor.tabla_por_labor)
            }, 250)
        }, $scope.filters)
    }

    $scope.variables = () => {
        $request.variables((r) => {
            $scope.fincas = r.fincas
            $scope.labores = r.labores

            let e = r.labores.filter((c) => c.idLabor == $scope.filters.tablaPorCausa.labor)
            if(!e.length > 0){
                $scope.filters.tablaPorCausa.labor = r.labores[0].idLabor
            }

            $scope.index()
        }, $scope.filters)
    }

    $scope.index = () => {
        $request.index((r) => {
            $scope.tabla_por_labor = r.tabla_por_labor
            responseTablaPorCausa(r.tabla_por_causa)

            setTimeout(() => {
                $scope.getGraficaPorLabor(r.tabla_por_labor.fincas, r.tabla_por_labor.tabla_por_labor)  
                $scope.getGraficaPorCausa(r.tabla_por_causa.fincas, r.tabla_por_causa.data)
            }, 250)
        }, $scope.filters)
    }

    $scope.last = () => {
        $request.last((r) => {
            $scope.filters.fecha_inicial = r.fecha
            $scope.filters.fecha_final = r.fecha
            $("#datepicker").html(`${r.fecha} - ${r.fecha}`)

            $scope.variables()
        })
    }

    $scope.getGraficaPorLabor = (fincas, datatable) => {
        let series = [{
            name : 'Cantidad',
            type : 'bar',
            data : []
        }]
        let legends = []
        for(let i in fincas){
            let finca = fincas[i]
            legends.push(finca.nombre)

            let valor = 'promedio'
            if($scope.filters.tablaPorCausa.labor){
                valor = 'labor_'+$scope.filters.tablaPorCausa.labor
            }
            series[0].data.push({
                value : datatable.filter((r) => r.idFinca == finca.idFinca)[0][valor]
            })
        }

        ///----------------
        let props = {
            id : 'chart-calidad-by-labor',
            series : series,
            legend : legends,
            showLegends : false,
            orientation : 'horizontal',
            min : Math.round((arrayMin(series[0].data, 'value')-5)*100)/100
        }
        $("#chart-calidad-by-labor").empty()
        ReactDOM.render(
            React.createElement(BarrasStacked, props),
            document.getElementById('chart-calidad-by-labor')
        )
    }

    $scope.getGraficaPorCausa = (fincas, datatable) => {
        let series = [{
            name : 'Cantidad',
            type : 'bar',
            data : []
        }]
        let legends = []
        for(let i in fincas){
            let finca = fincas[i]
            legends.push(finca.nombre)

            let valor = 'promedio'
            if($scope.filters.calidadCausa.causa){
                valor = 'causa_'+$scope.filters.calidadCausa.causa
            }
            series[0].data.push({
                value : datatable.filter((r) => r.idFinca == finca.idFinca)[0][valor]
            })
        }

        ///----------------
        let min = Math.round((arrayMin(series[0].data, 'value')-0.8)*100)/100
        let props = {
            id : 'chart-calidad-by-causa',
            series : series,
            legend : legends,
            showLegends : false,
            orientation : 'horizontal',
            min : min >= 0 ? min : 0
        }
        $("#chart-calidad-by-causa").empty()
        ReactDOM.render(
            React.createElement(BarrasStacked, props),
            document.getElementById('chart-calidad-by-causa')
        )
    }

    $scope.last()

}]);

