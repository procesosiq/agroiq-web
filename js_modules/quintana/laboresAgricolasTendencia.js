app.service('request', ['$http', ($http) => {
    let service = {}

    service.last = (callback, params) => {
        load.block()
        $http.post('phrapi/laboresAgricolasTendencia/last', params || {}).then((r) => {
            load.unblock()
            callback(r.data)
        })
    }

    service.index = (callback, params) => {
        load.block()
        $http.post('phrapi/laboresAgricolasTendencia/index', params || {}).then((r) => {
            load.unblock()
            callback(r.data)
        })
    }

    service.variables = (callback, params) => {
        load.block()
        $http.post('phrapi/laboresAgricolasTendencia/variables', params || {}).then((r) => {
            load.unblock()
            callback(r.data)
        })
    }

    service.causasPeriodo = (callback, params) => {
        load.block()
        $http.post('phrapi/laboresAgricolasTendencia/causasPeriodo', params || {}).then((r) => {
            load.unblock()
            callback(r.data)
        })
    }

    service.calidadPeriodo = (callback, params) => {
        load.block()
        $http.post('phrapi/laboresAgricolasTendencia/calidadPeriodo', params || {}).then((r) => {
            load.unblock()
            callback(r.data)
        })
    }

    return service
}])

app.controller('controller', ['$scope', 'request', function($scope, $request){

    $scope.filters = {
        calidadPeriodo : {
            mode: 'fincas'
        },
        causasPeriodo: {
            finca : '',
            labor : ''
        }
    }

    $scope.last = () => {
        $request.last((r) => {
            $scope.filters.anio = r.anio
            $request.variables((r) => {
                responseVariables(r)
                $scope.index()
            }, $scope.filters)
        })
    }

    $scope.index = () => {
        $request.index((r) => {
            responseCalidadPeriodo(r.calidad_periodo)
            responseCausasPeriodo(r.causas_periodo)
        }, $scope.filters)
    }

    $scope.calidadPeriodo = () => {
        $request.calidadPeriodo((r) => {
            responseCalidadPeriodo(r)
        }, $scope.filters)
    }

    $scope.changeFilterCausa = () => {
        $request.variables((r) => {
            responseVariables(r)
            $scope.causasPeriodo()
        }, $scope.filters)
    }

    $scope.causasPeriodo = () => {
        $request.causasPeriodo((r) => {
            responseCausasPeriodo(r)
        }, $scope.filters)
    }

    responseCalidadPeriodo = (r) => {
        $scope.calidad_periodo = r
        renderTableCalidadPeriodo(r.periodos, r.data)
        renderChartCalidadPeriodo(r.periodos, r.data)
    }

    responseVariables = (r) => {
        $scope.labores = angular.copy(r.labores)
        $scope.fincas = angular.copy(r.fincas)

        let e = r.labores.filter((l) => l.idLabor == $scope.filters.causasPeriodo.labor)
        if(!e.length > 0){
            $scope.filters.causasPeriodo.labor = r.labores[0].idLabor
        }

        e = r.fincas.filter((l) => l.idFinca == $scope.filters.causasPeriodo.finca)
        if(!e.length > 0){
            $scope.filters.causasPeriodo.finca = r.fincas[0].idFinca
        }
    }

    responseCausasPeriodo = (r) => {
        renderTableCausasPeriodo(r.periodos, r.data)
        renderChartCausasPeriodo(r.periodos, r.data)
    }

    renderTableCalidadPeriodo = (periodos, data) => {
        let props = {
            header : [{
                   key : 'finca',
                   name : 'FINCA',
                   titleClass : 'text-center',
                   alignContent : 'left',
                   locked : true,
                   expandable : true,
                   resizable : true
                },{
                   key : 'avg',
                   name : 'PROM',
                   locked : true,
                   sortable : true,
                   titleClass : 'text-center',
                   alignContent : 'right',
                   filterable : true,
                   filterRenderer: 'NumericFilter',
                   formatter : 'Number',
                   resizable : true,
                },{
                   key : 'max',
                   name : 'MAX',
                   locked : true,
                   titleClass : 'text-center',
                   sortable : true,
                   alignContent : 'right',
                   filterable : true,
                   filterRenderer: 'NumericFilter',
                   resizable : true,
                },{
                   key : 'min',
                   name : 'MIN',
                   locked : true,
                   sortable : true,
                   titleClass : 'text-center',
                   alignContent : 'right',
                   filterable : true,
                   filterRenderer: 'NumericFilter',
                   resizable : true,
                }
            ],
            data : data,
            buttons : [
                {
                    title : 'Excel',
                    action : () => {
                        $scope.table1.exportToExcel()
                    },
                    className : ''
                }
            ],
            height : 450
        }
        periodos.map((per) => {
            props.header.push({
                key : `periodo_${per}`,
                name : `${per}`,
                sortable : true,
                alignContent : 'right',
                titleClass : 'text-center',
                filterable : true,
                filterRenderer: 'NumericFilter',
                resizable : true,
            })
        })
        document.getElementById('table-calidad-periodal').innerHTML = ""
        $scope.table1 = ReactDOM.render(React.createElement(ReactDataGrid, props), document.getElementById('table-calidad-periodal'))
    }

    renderChartCalidadPeriodo = (periodos, data) => {
        let series = []
        let legends = periodos

        for(let i in data){
            let finca = data[i]
            let s = {
                name : finca.finca,
                connectNulls : true,
                type : 'line',
                data : []
            }

            for(let j in periodos){
                let per = periodos[j]
                s.data.push(finca['periodo_'+per])
            }
            series.push(s)
        }

        let parent = $("#chart-calidad-periodal").parent()
        parent.empty()
        parent.append(`<div id="chart-calidad-periodal" class="chart"></div>`)

        let props = {
            series: series,
            legend: legends,
            id: "chart-calidad-periodal",
            legendBottom : true,
            zoom : false,
            type : 'line',
            min : 'dataMin'
        }
        ReactDOM.render(React.createElement(Historica, props), document.getElementById('chart-calidad-periodal'));
    }

    renderTableCausasPeriodo = (periodos, data) => {
        let props = {
            header : [{
                   key : 'causa',
                   name : 'CAUSA',
                   titleClass : 'text-center',
                   alignContent : 'left',
                   locked : true,
                   resizable : true
                },{
                   key : 'avg',
                   name : 'PROM',
                   locked : true,
                   sortable : true,
                   titleClass : 'text-center',
                   alignContent : 'right',
                   filterable : true,
                   filterRenderer: 'NumericFilter',
                   formatter : 'Number',
                   resizable : true,
                },{
                   key : 'max',
                   name : 'MAX',
                   locked : true,
                   titleClass : 'text-center',
                   sortable : true,
                   alignContent : 'right',
                   filterable : true,
                   filterRenderer: 'NumericFilter',
                   resizable : true,
                },{
                   key : 'min',
                   name : 'MIN',
                   locked : true,
                   sortable : true,
                   titleClass : 'text-center',
                   alignContent : 'right',
                   filterable : true,
                   filterRenderer: 'NumericFilter',
                   resizable : true,
                }
            ],
            data : data,
            buttons : [
                {
                    title : 'Excel',
                    action : () => {
                        $scope.table2.exportToExcel()
                    },
                    className : ''
                }
            ],
            height : 450
        }
        periodos.map((per) => {
            props.header.push({
                key : `periodo_${per}`,
                name : `${per}`,
                sortable : true,
                alignContent : 'right',
                titleClass : 'text-center',
                filterable : true,
                filterRenderer: 'NumericFilter',
                resizable : true,
            })
        })
        document.getElementById('table-causas-periodal').innerHTML = ""
        $scope.table2 = ReactDOM.render(React.createElement(ReactDataGrid, props), document.getElementById('table-causas-periodal'))
    }

    renderChartCausasPeriodo = (periodos, data) => {
        let series = []
        let legends = periodos

        for(let i in data){
            let causa = data[i]
            let s = {
                name : causa.causa,
                connectNulls : true,
                type : 'line',
                data : []
            }

            for(let j in periodos){
                let per = periodos[j]
                s.data.push(causa['periodo_'+per])
            }
            series.push(s)
        }

        let parent = $("#chart-causas-periodal").parent()
        parent.empty()
        parent.append(`<div id="chart-causas-periodal" class="chart"></div>`)

        let props = {
            series: series,
            legend: legends,
            id: "chart-causas-periodal",
            legendBottom : true,
            zoom : false,
            type : 'line',
            min : 'dataMin'
        }
        ReactDOM.render(React.createElement(Historica, props), document.getElementById('chart-causas-periodal'));
    }

    $scope.last()

}]);

