app.service('request', ['client', '$http', function(client, $http){
    this.registros = function(callback, params){
        let url = 'phrapi/quintana/cajas/registros'
        let data = params || {}
        client.post(url, callback, data, 'tabla_base')
    }
    
    this.last = function(callback, params){
        let url = 'phrapi/quintana/cajas/last'
        let data = params || {}
        client.post(url, callback, data)
    }

    this.resumenCajas = function(callback, params){
        let url = 'phrapi/quintana/cajas/resumen'
        let data = params || {}
        client.post(url, callback, data, 'div_table_2')
    }

    this.filters = function(callback, params){
        let url = 'phrapi/quintana/cajas/filters'
        let data = params || {}
        client.post(url, callback, data)
    }

    this.cajasSemanal = function(callback, params){
        let url = 'phrapi/quintana/cajas/cajasSemanal'
        let data = params || {}
        client.post(url, callback, data, 'cajas_semanal')
    }

    this.getMarcas = function(callback, params){
        let url = 'phrapi/quintana/cajas/marcas'
        let data = params || {}
        client.post(url, callback, data)
    }

    this.cuadrar = function(callback, params){
        let url = 'phrapi/quintana/cajas/cuadrar'
        let data = params || {}
        client.post(url, callback, data)
    }

    this.guardarCuadrar = function(callback, params){
        let url = 'phrapi/quintana/cajas/guardarCuadrar'
        let data = params || {}
        client.post(url, callback, data)
    }

    this.getGuiasDia = function(callback, params){
        let url = 'phrapi/quintana/cajas/guias'
        let data = params || {}
        client.post(url, callback, data)
    }

    this.procesar = function(callback, params){
        let url = 'phrapi/quintana/cajas/procesar'
        let data = params || {}
        client.post(url, callback, data)
    }
    
    this.eliminar = function(callback, params){
        let url = 'phrapi/quintana/cajas/eliminar'
        let data = params || {}
        client.post(url, callback, data)
    }

    this.graficasBarras = function(callback, params){
        let url = 'phrapi/quintana/cajas/graficasBarras'
        let data = params || {}
        client.post(url, callback, data, 'barras')
    }

    this.tablaDiferencias = function(callback, params){
        let url = 'phrapi/quintana/cajas/diferencias'
        let data = params || {}
        client.post(url, callback, data, 'tablas')
    }

    this.historicoExcedente = function(callback, params){
        let url = 'phrapi/quintana/cajas/excedente'
        let data = params || {}
        client.post(url, callback, data, 'collapse_3_3')
    }

    this.guardarCaja = function(callback, params){
        let url = 'phrapi/quintana/cajas/guardarCaja'
        let data = params || {}
        client.post(url, callback, data)
    }

    this.editGuia = (callback, params) => {
        let url = 'phrapi/sumifru/cajas/editGuia'
        let data = params || {}
        $http.post(url, data).then((r) => {
            callback(r.data)
        })
    }

    this.asignarGuia = (callback, params) => {
        let url = 'phrapi/sumifru/cajas/asignarGuia'
        let data = params || {}
        $http.post(url, data).then((r) => {
            callback(r.data)
        })
    }

    this.borrarGuiaMarca = function(callback, params){
        let url = 'phrapi/sumifru/cajas/borrarGuiaMarca'
        let data = params || {}
        client.post(url, callback, data)
    }

    // MARCAS
    this.deleteMarca = function(params){
        return new Promise((resolve) => {
            let url = 'phrapi/marcas/deleteMarca'
            let data = params || {}
            $http.post(url, data)
            .then((r) => {
                resolve(r.data)
            })
        })
    }
    this.saveMarca = function(params){
        return new Promise((resolve) => {
            let url = 'phrapi/marcas/saveMarca'
            let data = params || {}
            $http.post(url, data)
            .then((r) => {
                resolve(r.data)
            })
        })
    }
    this.addAlias = function(params){
        return new Promise((resolve) => {
            let url = 'phrapi/marcas/addAlias'
            let data = params || {}
            $http.post(url, data)
            .then((r) => {
                resolve(r.data)
            })
        })
    }
    this.deleteAlias = function(params){
        return new Promise((resolve) => {
            let url = 'phrapi/marcas/deleteAlias'
            let data = params || {}
            $http.post(url, data)
            .then((r) => {
                resolve(r.data)
            })
        })
    }
    this.saveAlias = function(params){
        return new Promise((resolve) => {
            let url = 'phrapi/marcas/saveAlias'
            let data = params || {}
            $http.post(url, data)
            .then((r) => {
                resolve(r.data)
            })
        })
    }
    this.addRango = function(params){
        return new Promise((resolve) => {
            let url = 'phrapi/marcas/addRango'
            let data = params || {}
            $http.post(url, data)
            .then((r) => {
                resolve(r.data)
            })
        })
    }
    this.deleteRango = function(params){
        return new Promise((resolve) => {
            let url = 'phrapi/marcas/deleteRango'
            let data = params || {}
            $http.post(url, data)
            .then((r) => {
                resolve(r.data)
            })
        })
    }
    this.saveRango = function(params){
        return new Promise((resolve) => {
            let url = 'phrapi/marcas/saveRango'
            let data = params || {}
            $http.post(url, data)
            .then((r) => {
                resolve(r.data)
            })
        })
    }
    // END MARCAS

}])

app.filter('num', function() {
    return function(input) {
    return parseInt(input, 10);
    };
});

app.filter('sumOfValue', function () {
    return function (data, key) {        
        if (angular.isUndefined(data) || angular.isUndefined(key))
            return 0;        
        var sum = 0;
        var count = 0;
        angular.forEach(data,function(value){
            if(value[key] != "" && value[key] != undefined){
                sum = sum + parseFloat(value[key], 10);
                count++;
            }
        });
        return sum;
    }
})

app.filter('avgOfValue', function () {
    return function (data, key) {        
        if (angular.isUndefined(data) || angular.isUndefined(key))
            return 0;        
        var sum = 0;
        var count = 0;
        angular.forEach(data,function(value){
            if(value[key] != "" && value[key] != undefined){
                sum = sum + parseFloat(value[key], 10);
                count++;
            }
        });
        sum = sum / count;
        if(isNaN(sum))
            return 0;
        return sum;
    }
})

app.filter('orderObjectBy', function() {
	return function(items, field, reverse) {
    	var filtered = [];
    	angular.forEach(items, function(item) {
            if(field == 'hora' || field == 'fecha'){
                item.date = moment(item.fecha  + ' ' + item.hora)
            }
    		if(!isNaN(parseInt(item))){
    			item = parseInt(item);
    		}
      		filtered.push(item);
    	});
    	filtered.sort(function (a, b) {
            if(field == 'hora' || field == 'fecha'){
                return moment(a.date).isAfter(b.date) ? 1 : -1;
            }else if(parseFloat(a[field]) && parseFloat(b[field])){
				return (parseFloat(a[field]) > parseFloat(b[field]) ? 1 : -1);
			}else{
				return (a[field] > b[field] ? 1 : -1);
			}
    	});
    	if(reverse) filtered.reverse();
    	return filtered;
  	};
});

app.directive('escape', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if(event.which === 27) {
                scope.$apply(function (){
                    scope.$eval(attrs.escape);
                });

                event.preventDefault();
            }
        });
    };
});

app.directive('enter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.enter);
                });

                event.preventDefault();
            }
        });
    };
});

function getOptionsGraficaReact(id, options, title){
    var newOptions = {
        series: options.series,
        legend: options.legends,
        umbral: null,
        id: id,
        titulo : title || null,
        actions : false,
        showLegends : false
    }
    return newOptions
}

function initGrafica(id, options, title){
    setTimeout(() => {
        let parent = $("#"+id).parent()
        parent.empty()
        parent.append(`<div id="${id}" class="chart"></div>`)

        var data = getOptionsGraficaReact(id, options, title)
        let chart = React.createElement(Historica, data)
        ReactDOM.render(chart, document.getElementById(id));
    }, 250)
}

function initPastel(id, series){
    var legends = []
    var newSeries = []
    Object.keys(series).map(key => {
        let label = series[key].label
        newSeries.push({
            label : series[key].label,
            value : parseFloat(series[key].value),
            color : (label.includes('>')) ? '#E43A45' : (label.includes('<')) ? '#C49F47' : '#26C281'
        })
        if(legends.indexOf(series[key]) != -1) legends.push(series[key]);
    })
    setTimeout(() => {
        data = {
            data : newSeries,
            nameseries : "Pastel",
            legend : legends,
            titulo : "",
            id : id
        }

        let parent = $("#"+id).parent()
        parent.empty()
        parent.append(`<div id="${id}" class="chart"></div>`)
        ReactDOM.render(React.createElement(Pastel, data), document.getElementById(id));
        
    }, 250)
}

function in_array(value, array){
    if(!Array.isArray(array)) return false
    return array.indexOf(value) >= 0
}

let datesEnabled = []

app.controller('produccion', ['$scope','request', '$filter', function($scope, $request, $filter){

    datepickerHighlight = () => {
        $('#datepicker').datepicker({
            beforeShowDay: function (date) {
                let fecha = moment(date).format('YYYY-MM-DD')
                let has = datesEnabled.indexOf(fecha) > -1
                return has ? { classes: 'highlight', tooltip: 'Procesado', enabled : true } : { tooltip : 'Sin proceso', enabled : false }
            }
        });
        $('#datepicker').datepicker()
        .on('changeDate', function(e) {
            $scope.table.fecha_inicial = moment(e.date).format('YYYY-MM-DD')
            $scope.changeRangeDate()
        });
        $("#datepicker").datepicker('setDate', $scope.table.fecha_inicial)
        $('#datepicker').datepicker('update')
    }

    $scope.marcasSeleccionadas = [
        '',
        '',
        '',
        '',
        '',
        ''
    ];

    checkUmbral = (value, cUmbral) => {
        if(parseFloat(value)){
            let umbral = cUmbral || parseFloat($scope.umbralExcedente[$scope.filters.var])
            if(value >= umbral && value <= umbral) return 'bg-green-jungle bg-font-green-jungle'
            else if (value > umbral) return 'bg-red-thunderbird bg-font-red-thunderbird'
            else return 'bg-yellow-gold bg-font-yellow-gold'
        }
        return ''
    }

    initTableExcedente = (id, r) => {
        let val = $scope.filters.var
        var props = {
            header : [{
                   key : 'marca',
                   name : 'MARCA',
                   titleClass : 'text-center',
                   locked : true,
                   expandable : true,
                   resizable : true,
                   width: 170
                },{
                    key : 'sum_'+val,
                    name : 'SUM',
                    locked : true,
                    sortable : true,
                    titleClass : 'text-center',
                    alignContent : 'right',
                    filterable : true,
                    filterRenderer: 'NumericFilter',
                    formatter : 'Number',
                    resizable : true,
                    customCell : function(rowData, isChildren){
                         let valNumber = parseFloat(rowData['sum_'+val])
                         let cUmbral = parseFloat(rowData['avg_'+val])
                         let valueCell = valNumber > 0 ? $filter('number')(valNumber, 2) : ''
                         return `
                             <div class="text-center" style="height: 100%">
                                 ${valueCell}
                             </div>
                         `;
                     }
                 },{
                   key : 'avg_'+val,
                   name : 'AVG',
                   locked : true,
                   sortable : true,
                   titleClass : 'text-center',
                   alignContent : 'right',
                   filterable : true,
                   filterRenderer: 'NumericFilter',
                   formatter : 'Number',
                   resizable : true,
                   customCell : function(rowData, isChildren){
                        let valNumber = parseFloat(rowData['avg_'+val])
                        let cUmbral = parseFloat(rowData['avg_'+val])
                        let valueCell = valNumber > 0 ? $filter('number')(valNumber, 2) : ''
                        return `
                            <div class="text-center ${checkUmbral(valNumber, cUmbral)}" style="height: 100%">
                                ${valueCell}
                            </div>
                        `;
                    }
                },{
                   key : 'max_'+val,
                   name : 'MAX',
                   locked : true,
                   titleClass : 'text-center',
                   sortable : true,
                   alignContent : 'right',
                   filterable : true,
                   filterRenderer: 'NumericFilter',
                   resizable : true,
                   customCell : function(rowData, isChildren){
                        let valNumber = parseFloat(rowData['max_'+val])
                        let cUmbral = parseFloat(rowData['avg_'+val])
                        let valueCell = valNumber > 0 ? $filter('number')(valNumber, 2) : ''
                        return `
                            <div class="text-center ${checkUmbral(valNumber, cUmbral)}" style="height: 100%">
                                ${valueCell}
                            </div>
                        `;
                    }
                },{
                   key : 'min_'+val,
                   name : 'MIN',
                   locked : true,
                   sortable : true,
                   titleClass : 'text-center',
                   alignContent : 'right',
                   filterable : true,
                   filterRenderer: 'NumericFilter',
                   resizable : true,
                   customCell : function(rowData, isChildren){
                        let valNumber = parseFloat(rowData['min_'+val])
                        let cUmbral = parseFloat(rowData['avg_'+val])
                        let valueCell = valNumber > 0 ? $filter('number')(valNumber, 2) : ''
                        return `
                            <div class="text-center ${checkUmbral(valNumber, cUmbral)}" style="height: 100%">
                                ${valueCell}
                            </div>
                        `;
                    }
                }
            ],
            data : r.data,
            buttons : [
                {
                    title : 'Excel',
                    action : () => {
                        $scope.table1.exportToExcel()
                    },
                    className : ''
                }
            ]
        }
        Object.keys(r.semanas).map((key) => {
            let value = r.semanas[key]
            props.header.push({
                key : `sem_${val}_${value}`,
                name : `${value}`,
                sortable : true,
                alignContent : 'right',
                titleClass : 'text-center',
                filterable : true,
                filterRenderer: 'NumericFilter',
                resizable : true,
                customCell : function(rowData, isChildren){
                    let valNumber = parseFloat(rowData['sem_'+val+'_'+value])
                    let cUmbral = parseFloat(rowData['avg_'+val])
                    let valueCell = valNumber > 0 ? number_format(valNumber, 2) : ''
                    return `
                        <div class="text-center ${checkUmbral(valNumber, cUmbral)}" style="height: 100%">
                            ${valueCell}
                        </div>
                    `;
                }
            })
        })
        document.getElementById(id).innerHTML = ""
        $scope.table1 = ReactDOM.render(React.createElement(ReactDataGrid, props), document.getElementById(id))
    }
    
    $scope.fechaPasada = moment().format('YYYY-MM-DD')
    $scope.anioCambio = true
    $scope.filters = {
        var: 'exce'
    }
    $scope.registros = []
    $scope.table = {
        pagination : 10,
        startFrom : 0,
        actualPage : 1,
        fecha_inicial : moment().format('YYYY-MM-DD'),
        fecha_final : moment().format('YYYY-MM-DD'),
        search : {},
        year : moment().year(),
        semana : moment().week(),
        unidad : 'lb',
        finca : 0,
        var : 'CONV'
    }
    $scope.charts = {
        cajasSemanal : new echartsPlv()
    }

    $scope.convertKg = () => {
        if($scope.table.unidad == 'lb'){
            $scope.table.unidad = 'kg'
        }else{
            $scope.table.unidad = 'lb'
        }
        $scope.anioCambio = true
        $scope.init()
    }

    $scope.StartEndDateDirectives = {
        startDate : moment(),
        endDate :moment(),
    }

    $scope.changeRangeDate = function () {
    
        if(!$("#expand_cuadrar").hasClass('collapsed')){
            $("#expand_cuadrar").addClass('collapsed')
            $("#expand_cuadrar").attr('aria-expanded', 'false')

            $("#collapse_3_2").attr('aria-expanded', 'false')
            $("#collapse_3_2").removeClass('in')
            $("#collapse_3_2").css('height', '0px')
        }
        $scope.anioCambio = false
        if(moment($scope.fechaPasada).year() != moment($scope.table.fecha_inicial).year()) $scope.anioCambio = true;
        $scope.fechaPasada = $scope.table.fecha_inicial

        $scope.table.fecha_final = $scope.table.fecha_inicial;
        $scope.init();
    }

    $scope.next = function(dataSource){
        if($scope.searchTable.actual_page < $scope.searchTable.numPages) {
            $scope.searchTable.actual_page++;
            $scope.searchTable.startFrom = ($scope.searchTable.actual_page - 1) * (parseInt($scope.searchTable.limit))
        }
    }

    $scope.prev = function(dataSource){
        if($scope.searchTable.actual_page > 1){
            $scope.searchTable.actual_page--;
            $scope.searchTable.startFrom = ($scope.searchTable.actual_page - 1) * (parseInt($scope.searchTable.limit))
        }
    }

    $scope.setOrderTable = function(field){
        var reverse = false
        if(field == $scope.searchTable.orderBy){
            reverse = !$scope.searchTable.reverse
        }else{
            $scope.searchTable.orderBy = field
        }
        $scope.searchTable.reverse = reverse
    }

    $scope.getFilters = function(){
        let data = $scope.table
        $request.filters(function(r, b){
            b()
            if(r){
                $scope.anios = r.years
                $scope.semanas = r.semanas
            }
        }, data)
    }

    $scope.changeExcedente = () => {
        initTableExcedente('table-historico-excedente', $scope.dataExcedente)
    }

    $scope.getRegistrosBase = function(){
        let data = $scope.table
        load.block('marcas')
        $request.registros(function(r, b){
            b('tabla_base')
            load.unblock('marcas')
            if(r){
                $scope.marcas = r.marcas
                $scope.peso_prom_cajas = r.pesos_prom_cajas
                $scope.registros = r.data
                setTimeout(function(){
                    $scope.searchTable.changePagination()
                }, 500)
            }
        }, data)

        $request.tablaDiferencias(function(r, b){
            b('tablas')
            $scope.tablasDiferencias = r.tablas
        }, data)

        $request.graficasBarras(function(r, b){
            b('barras')
            if(r){
                $scope.marcasBarras = r.marcas
                $scope.graficasBarrar = r.graficas
                Object.keys(r.graficas).map((key, index) => {
                    let min = r.graficas[key].series.Cantidad.umbral.min, 
                        max = r.graficas[key].series.Cantidad.umbral.max
                    
                    r.graficas[key].series.Cantidad.itemStyle.normal.color = function(e){
                        // range umbral 
                        if(min && max){
                            var min_peso = 0, max_peso = 0
                            if(parseFloat(e.name)){
                                min_peso = max_peso = parseFloat(e.name)
                            }else{
                                var min_max = e.name.split('-')
                                min_peso = parseFloat(min_max[0])
                                max_peso = parseFloat(max_max[0])
                            }

                            if(min_peso >= min && max_peso <= max){
                                return '#26C281'
                            }else if(max_peso < min){
                                return '#C49F47'
                            }else{
                                return '#E43A45'
                            }
                        }

                        return '#E43A45'
                    }
                    initGrafica(`barras_${index}`, r.graficas[key], key)
                    initPastel(`pastel_${index}`, r.pasteles[key])
                })
            }
        }, data)
    }

    $scope.eliminar = () => {
        var elements = $(".delete")
        if(elements.length > 0){
            if(confirm(`Seguro deseas eliminar ${elements.length} elementos?`)){
                var ids = []
                var index = []
                for(var x = 0; x < elements.length; x++){
                    ids.push({
                        id : $(elements[x]).attr('data-id'),
                        marca : $(elements[x]).attr('data-marca')
                    })
                }
                $request.eliminar((r, b) => {
                    b()
                    if(r){
                        $scope.getRegistros()
                        alert(`Se borraron ${elements.length} registros con éxito`, "", "success")
                    }
                }, { ids : ids })
            }
        }else{
            alert("No has seleccionado ninguna fila")
        }
    }

    $scope.editarGuia = (guia) => {
        setTimeout(() => {
            $scope.modalCuadrar()
            $scope.guia = {
                guia : guia.guia,
                fecha : guia.fecha,
                sello_seguridad : guia.sello_seguridad,
                magap : guia.codigo_magap,
                productor : guia.codigo_productor,
                finca : guia.id_finca,
                marca : {}
            }
            $scope.marcasSeleccionadas = ['', '', '', '', '', '']
            guia.detalle.map((value, index) => {
                $scope.marcasSeleccionadas[index] = value.marca
                $scope.guia.marca[value.marca] = Math.round(value.valor)
            })
            $scope.$apply()
        }, 100)
    }

    $scope.modalCuadrar = function(){
        $scope.guia = {
            marca : {}
        }
        $("#cuadrar").modal('toggle')
    }
    $scope.guardarCuadrar = function(){
        var data = angular.copy($scope.guia)
        Object.keys(data.marca).map((marca) => {
            if(!in_array(marca, $scope.marcasSeleccionadas)) delete data.marca[marca]
        })
        //data.fecha = $scope.table.fecha_inicial

        $request.guardarCuadrar(function(r, b){
            b()
            let data = $scope.table
            $request.cuadrar(function(r, b){
                b()
                if(r){
                    $scope.guia.procesable = true
                    $scope.guia = {}

                    setTimeout(() => {
                        alert("Guardado con éxito", "", "success")
                        $scope.$apply(() => {
                            console.log("apply")
                            $scope.cuadrarCajas = r.data
                            $scope.guias = r.guias
                            $scope.saldos = r.saldos
                        })
                    }, 500)
                }
            }, data)
        }, data)
    }

    $scope.giasDia = (fecha, guia) => {
        $scope.guia.procesable = false
        if(fecha && guia){
            $request.getGuiasDia((r, b) => {
                b()
                if(r){
                    $scope.guia.procesable = true
                    $scope.guia.marca = {}
                    r.marcas.forEach(value => {
                        $scope.guia.marca[value.marca] = parseInt(value.valor)
                    })
                }
            }, { fecha: fecha, guia: guia })
        }
    }

    $scope.getCuadrar = function(){
        setTimeout(() => {
            let showing = $("#collapse_3_2").attr('aria-expanded') == 'true'
            if(showing){
                let data = $scope.table
                $request.cuadrar(function(r, b){
                    b()
                    if(r){
                        $scope.cuadrarCajas = r.data
                        $scope.guias = r.guias
                        $scope.saldos = r.saldos
                    }
                }, data)
            }
        }, 150)
    }

    $scope.getRegistros = function(){
        $scope.getFilters()
        $scope.getRegistrosBase()

        let data = $scope.table
        $request.resumenCajas(function(r, b){
            b('div_table_2')
            if(r){
                $scope.resumen = r.data 
                $scope.tags = r.tags
                $scope.fincas = r.fincas

                if(!r.fincas.hasOwnProperty($scope.table.finca)){
                    $scope.table.finca = Object.keys(r.fincas)[0]
                }
            }
        }, data)

        
        if($scope.anioCambio){
            let parent = $("#cajas_semanal").parent()
            parent.empty()
            parent.append(`<div id="cajas_semanal" style="height: 400px;"></div>`)

            $scope.loadSemanal()
            $request.historicoExcedente(function(r, b){
                b('collapse_3_3')
                $scope.dataExcedente = r
                $scope.umbralExcedente = r.umbrales
                initTableExcedente('table-historico-excedente', r)
            }, data)
        }
    }

    $scope.loadSemanal = () => {
        let data = $scope.table
        $request.cajasSemanal(function(r, b){
            b('cajas_semanal')
            if(r){
                $scope.charts.cajasSemanal.init('cajas_semanal', r.chart)
            }
        }, data)
    }

    $scope.edit = (row, field) => {
        $scope.originalGuia = angular.copy(row)
        row.editing = field
    }

    $scope.cancel = (row) => {
        row.editing = null
        Object.assign(row, $scope.originalGuia)
    }

    $scope.save = (row) => {
        $request.editGuia((r) => {
            if(r.status === 200){
                row.editing = false
                $scope.getCuadrar()
            }else{
                alert(`Ocurrio algo inesperado`)
            }
        }, {
            fecha: $scope.table.fecha_inicial,
            marca : row.marca,
            pendiente : row.pendiente,
            asignada : row.asignada,
            id_finca : $scope.table.finca
        })
    }

    $scope.editar = (row, index) => {
        if(!$scope.editando){
            $scope.editando = true
            row.edit = true
            row.index = index
            $scope.originalFilaEditando = angular.copy(row)
            $scope.filaEditando = row
        }
    }

    $scope.cancelar = (row, index) => {
        if($scope.editando){
            setTimeout(() => {
                row.peso = $scope.originalFilaEditando.peso
                row.marca = $scope.originalFilaEditando.marca
                row.edit = false
                $scope.editando = false
                $scope.$apply()
            }, 100)
        }
    }

    $scope.guardar = (row, index) => {
        if($scope.editando){
            row.unidad = $scope.table.unidad
            $request.guardarCaja((r, b) => {
                b()
                if(r.status == 200){
                    row.edit = false
                    $scope.editando = false
                    $scope.$apply()
                    alert("Se modifico correctamente", "Modificación caja #"+row.id, "success")
                }else{
                    alert(r.message)
                }
            }, row)
        }
    }

    $scope.init = function(newDate){
        return new Promise((resolve) => {
            $request.last(function(r, b){
                b()
                if(r){
                    $scope.available_fincas = r.available_fincas

                    if(Object.keys(r.fincas).length > 0)
                    if(Object.keys(r.fincas).indexOf($scope.table.finca) == -1){
                        $scope.table.finca = Object.keys(r.fincas)[0]
                        $scope.anioCambio = true
                    }

                    if(r.days){
                        datesEnabled = r.days
                    }
    
                    if(newDate){
                        $scope.table.fecha_inicial = r.last.fecha
                        $scope.table.fecha_final = r.last.fecha
                        $("#date-picker").html(r.last.fecha + ' - '+r.last.fecha)
                        $scope.anioCambio = true
                    }
    
                    $scope.getRegistros()
                }

                resolve()
            }, { newDate : newDate, filters: $scope.table })
        })
    }

    $scope.searchTable = {
        orderBy : "hora",
        reverse : true,
        limit : 10,
        actual_page : 1,
        startFrom : 0,
        optionsPagination : [
            10, 50, 100
        ],
        numPages : 0,
        changePagination : function(){
            if($scope.searchTable.limit == 0)
                $scope.searchTable.limit = $scope.registros.length

            setTimeout(function(){
                $scope.$apply(function(){
                    $scope.searchTable.numPages = parseInt($scope.registros.length / $scope.searchTable.limit)  + ($scope.registros.length % $scope.searchTable.limit == 0 ? 0 : 1)
                    $scope.searchTable.actual_page = 1
                    $scope.searchTable.startFrom = 0
                })
            }, 250)
        }
    }

    /* CONFIGURACION RANGOS */
    $scope.showRango = function(){
        $scope.config.rangos.getMarcas(function(r, b){
            b()
            if(r){
                $scope.config.rangos.marcas = r.data
            }
            $("#rangos").modal('toggle')
        })
    }

    $scope.config = {
        rangos : {
            marcas : [],
            getMarcas : function(callback){
                $request.getMarcas(callback,  $scope.params)
            },
            newConfig : {
                marca : 0,
                min : 0,
                max : 0
            }
        }
    }

    $scope.procesarDia = () => {
        $request.procesar((r, b) => {
            b()
            alert("Procesado Completo", "", "success")
            $scope.getRegistros()
        }, { fecha : $scope.table.fecha_inicial, id_finca : $scope.table.finca } )
    }

    $scope.guia = {}
    $scope.isValidGuia = () => {
        if($scope.table.fecha_inicial)
            return true;
        return false;
    }

    $scope.getClassUmbral = (marca) => {
        if(marca.class != ''){
            return marca.class
        } else {
            let peso_prom = parseFloat($scope.peso_prom_cajas[marca.marca])
            var classBg = ''
            if(marca.peso < peso_prom){
                classBg = 'bg-yellow-gold bg-font-yellow-gold'
            }else if(marca.peso > peso_prom){
                classBg = 'bg-red-thunderbird bg-font-red-thunderbird'
            }else{
                classBg = 'bg-green-haze bg-font-green-haze'
            }
            marca.class = classBg
            return classBg
        } 
    }

    $scope.exportExcel = function(id_table){
        var data = new Blob([document.getElementById(id_table).outerHTML], {
            type : 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf8'
        })
        saveAs(data, 'Cajas.xls')
    }

    $scope.exportPrint = function(id_table){
        
        let image = '<div style="display:block; height:55;"><img style="float: right;" width="100" height="50" src="./../logos/Logo.png" /></div><br>';
        let table = document.getElementById(id_table);
        var contentTable = table.outerHTML;

        if(id_table == 'table_1'){
            var cut = contentTable.search('<tr role="row" class="filter">')
            var cut2 = contentTable.search('</thead>')
            var part1 = contentTable.substring(0, cut)
            var part2 = contentTable.substring(cut2, contentTable.length)
            contentTable = part1 +part2
        }

        newWin = window.open("");
        newWin.document.write(contentTable);
        newWin.print();
        newWin.close();
    }

    $scope.hideActions = () => {
        $("#actions-listado").addClass("hide")
    }
    $scope.showActions = () => {
        $("#actions-listado").removeClass("hide")
    }
    $scope.not_selected_marca = (index) => {
        var array = []
        $scope.marcasSeleccionadas.map((value, i) => {
            if(i != index) array.push(value)
        })
        return in_array($scope.marcasSeleccionadas[index], array)
    }

    $scope.modalAsignar = (row) => {
        $("#asignar").modal('show')
        $scope.rowAsignar = angular.copy(row)
    }

    $scope.asignarGuia = () => {
        $request.asignarGuia((r) => {
            if(r.status === 200){
                alert('Asignadas','','success')
                $scope.getCuadrar()
                $("#asignar").modal('hide')
            }else{
                alert(r.message)
            }
        }, Object.assign({ id_finca_origen : $scope.rowAsignar.id_finca, fecha : $scope.table.fecha_inicial }, $scope.asignar_form))
    }

    $scope.borrarGuiaMarca = (guia, marca) => {
        if(confirm('¿Estas seguro de eliminar esta marca?')){
            let data = {
                guia : guia.guia,
                marca : marca.marca,
                fecha :  $scope.table.fecha_inicial,
                id_finca : $scope.table.finca
            }
            $request.borrarGuiaMarca((r,b) => {
                b()
                $scope.getCuadrar()
            }, data)
        }
    }

    $scope.init(true)
    .then(() => {
        datepickerHighlight()
    })

    /* MARCAS */
    $scope.saveMarca = () => {
        $request.saveMarca($scope.marcaSelected)
        .then((r) => {
            if(r.status === 200){
                toastr.success('Guardado')
                $scope.getRegistrosBase()
                $("#marca-modal").modal('hide')
            }else if(r.message){
                swal("Error", r.message, "error");
            }else{
                swal("Contacte a soporte", "Ha ocurrido algo inesperado", "error");
            }
        })
    }

    $scope.editarMarca = (marca) => {
        $("#marca-modal").modal('show')
        $scope.marcaSelected = marca
    }

    $scope.deleteMarca = (marca, $index) => {
        swal({
            text : '¿Estas seguro de hacer esta acción?',
            buttons : ['No', 'Si']
        })
        .then((res) => {
            if(res){
                $request.deleteMarca(marca)
                .then((r) => {
                    if(r.status === 200){
                        toastr.success('Borrado')
                        $scope.marcas.splice([$index], 1)
                        $scope.getRegistrosBase()
                        $scope.$apply()
                    }else{
                        swal("Contacte a soporte", "Ha ocurrido algo inesperado", "error");
                    }
                })
            }
        })
    }

    $scope.addAlias = () => {
        $request.addAlias($scope.marcaSelected)
        .then((r) => {
            if(r.status === 200){
                $scope.marcaSelected.alias.push({
                    id : r.id
                })
                $scope.$apply()
            }else{
                swal("Contacte a soporte", "Ha ocurrido algo inesperado", "error");
            }
        })
    }
    
    $scope.addRango = () => {
        $request.addRango($scope.marcaSelected)
        .then((r) => {
            if(r.status === 200){
                $scope.marcaSelected.rangos.push({
                    id : r.id
                })
                $scope.$apply()
            }else{
                swal("Contacte a soporte", "Ha ocurrido algo inesperado", "error");
            }
        })
    }

    $scope.saveRango = (rango) => {
        $request.saveRango(rango)
        .then((r) => {
            if(r.status === 200){
                toastr.success('Guardado')
                $scope.getRegistrosBase()
            }else{
                swal("Contacte a soporte", "Ha ocurrido algo inesperado", "error");
            }
        })
    }

    $scope.deleteRango = (rango, $index) => {
        swal({
            text : '¿Estas seguro de hacer esta acción?',
            buttons : ['No', 'Si']
        })
        .then((res) => {
            if(res){
                $request.deleteRango(rango)
                .then((r) => {
                    if(r.status === 200){
                        toastr.success('Borrado')
                        $scope.marcaSelected.rangos.splice([$index], 1)
                        $scope.getRegistrosBase()
                        $scope.$apply()
                    }else{
                        swal("Contacte a soporte", "Ha ocurrido algo inesperado", "error");
                    }
                })
            }
        })
    }

    $scope.saveAlias = (alias) => {
        $request.saveAlias(alias)
        .then((r) => {
            if(r.status === 200){
                toastr.success('Guardado')
            }else{
                swal("Contacte a soporte", "Ha ocurrido algo inesperado", "error");
            }
        })
    }

    $scope.deleteAlias = (alias, $index) => {
        swal({
            text : '¿Estas seguro de hacer esta acción?',
            buttons : ['No', 'Si']
        })
        .then((res) => {
            if(res){
                $request.deleteAlias(alias)
                .then((r) => {
                    if(r.status === 200){
                        toastr.success('Borrado')
                        $scope.marcaSelected.alias.splice([$index], 1)
                        $scope.$apply()
                    }else{
                        swal("Contacte a soporte", "Ha ocurrido algo inesperado", "error");
                    }
                })
            }
        })
    }

    $scope.deleteProp = (data, prop) => {
        if(!data[prop]){
            delete data[prop]
        }
    }

    /* END MARCAS */

    $scope.sum = (...numbers) => {
        let sum = 0
        numbers.map((n) => {
            if(parseFloat(n)){
                sum += parseFloat(n)
            }
        })
        return sum
    }

}]);