app.controller('rpersonal', ['$scope','client', function($scope,client){

    $scope.filters = {
        finca : 1
    }
    $scope.umbrales = {}
    $scope.checks = function(data, periodo){
        var porcentaje = parseFloat(data['PERIODO '+periodo])
        if(!isNaN(porcentaje) && porcentaje >= 0){
            if(porcentaje > parseFloat($scope.umbrales.yellow_umbral_2))
                return 'bg-green-haze bg-font-green-haze';
            else if (porcentaje < parseFloat($scope.umbrales.yellow_umbral_1))
                return 'bg-red-thunderbird bg-font-red-thunderbird';
            else
                return 'bg-yellow-lemon bg-font-yellow-lemon';
        }
        return ''
    }

    $scope.init = function(){
        client.post('phrapi/quintana/laboresAgricolas/personal', $scope.printDetails, $scope.filters)
    }

    $scope.printDetails = (r, b) => {
        b()
        $scope.umbrales = r.umbrals || {}
        renderTablaReact(r.data, r.periodos)
    }

    const renderTablaReact = (data, periodos) => {
        let props = {
            header : [{
                    key : 'zona',
                    name : 'VARIABLE',
                    titleClass : 'text-center',
                    locked : true,
                    expandable : true,
                    resizable : true,
                    width: 400
                }
            ],
            data : data,
            buttons : [
                {
                    title : 'Excel',
                    action : () => {
                        $scope.table1.exportToExcel()
                    },
                    className : ''
                }
            ],
            height : 450
        }
        periodos.map((p) => {
            let value = p.periodo
            props.header.push({
                key : `PERIODO ${value}`,
                name : `${value}`,
                sortable : true,
                alignContent : 'right',
                titleClass : 'text-center',
                filterable : true,
                filterRenderer: 'NumericFilter',
                resizable : true,
                customCell : function(rowData){
                    let valNumber = parseFloat(rowData['PERIODO '+value])
                    let checkUmbral = $scope.checks(rowData, value)
                    if(rowData['tipo'] == 'CAUSA') checkUmbral = ''
                    return `
                        <div class="text-center ${checkUmbral}" style="height: 100%">
                            ${valNumber} %
                        </div>
                    `;
                }
            })
        })
        document.getElementById('table-react').innerHTML = ""
        $scope.table1 = ReactDOM.render(React.createElement(ReactDataGrid, props), document.getElementById('table-react'))
    }

    $scope.init()
}]);

