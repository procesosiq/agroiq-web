app.service('request',['$http', function($http){
    
    this.ventaDia = function(callback, params){
        let url = 'phrapi/lancofruit/rastreo/ventadia'
        load.block('tabla_venta_dia')
        load.block('collapse_3_1')
        $http.post(url, params || {}).then(r => {
            load.unblock('tabla_venta_dia')
            load.unblock('collapse_3_1')
            callback(r.data)
        })
    }
    this.ventaSemana = function(callback, params){
        let url = 'phrapi/lancofruit/rastreo/ventasemana'
        load.block('tabla_venta_semana')
        load.block('collapse_3_2')
        $http.post(url, params || {}).then(r => {
            load.unblock('tabla_venta_semana')
            load.unblock('collapse_3_2')
            callback(r.data)
        })
    }
    this.ventaMes = function(callback, params){
        let url = 'phrapi/lancofruit/rastreo/ventames'
        load.block('tabla_venta_mes')
        load.block('collapse_3_3')
        $http.post(url, params || {}).then(r => {
            load.unblock('tabla_venta_mes')
            load.unblock('collapse_3_3')
            callback(r.data)
        })
    }
    this.ventaAnio = function(callback, params){
        let url = 'phrapi/lancofruit/rastreo/ventaanio'
        load.block('tabla_venta_anio')
        load.block('collapse_3_4')
        $http.post(url, params || {}).then(r => {
            load.unblock('tabla_venta_anio')
            load.unblock('collapse_3_4')
            callback(r.data)
        })
    }
    this.dia = function(callback, params){
        let url = 'phrapi/lancofruit/rastreo/dia'
        $http.post(url, params || {}).then(r => {
            callback(r.data)
        })
    }
    this.semana = function(callback, params){
        let url = 'phrapi/lancofruit/rastreo/semana'
        $http.post(url, params || {}).then(r => {
            callback(r.data)
        })
    }
    this.mes = function(callback, params){
        let url = 'phrapi/lancofruit/rastreo/mes'
        $http.post(url, params || {}).then(r => {
            callback(r.data)
        })
    }
    this.anio = function(callback, params){
        let url = 'phrapi/lancofruit/rastreo/anio'
        $http.post(url, params || {}).then(r => {
            callback(r.data)
        })
    }
    this.last = function(callback, params){
        let url = 'phrapi/lancofruit/rastreo/last'
        $http.post(url, params || {}).then(r => {
            callback(r.data)
        })
    }
    this.loadPoints = function(callback, params){
        let url = 'phrapi/lancofruit/rastreo/points'

        load.block('map_box')
        $http.post(url, params || {}).then(r => {
            load.unblock('map_box')
            callback(r.data)
        })
    }
    this.loadMorePoints = function(callback, params){
        let url = 'phrapi/lancofruit/rastreo/points'
        $http.post(url, params || {}).then(r => {
            callback(r.data)
        })
    }
    this.tags = function(callback, params){
        let url = 'phrapi/lancofruit/rastreo/indicadores'
        load.block('indicadores')
        $http.post(url, params || {}).then(r => {
            load.unblock('indicadores')
            callback(r.data)
        })
    }
    this.eliminar = function(callback, params){
        let url = 'phrapi/lancofruit/rastreo/eliminar'
        $http.post(url, params || {}).then(r => {
            callback(r.data)
        })
    }
    this.editar = function(callback, params){
        let url = 'phrapi/lancofruit/rastreo/editar'
        $http.post(url, params || {}).then(r => {
            callback(r.data)
        })
    }
}])

function renderPie(id, _data, titulo){
    let options = {
        data : _data,
        nameseries : "Pastel",
        titulo : titulo,
        id : id
    }
    ReactDOM.render(React.createElement(Pastel, options), document.getElementById(id));
}

function printGrafica(id, r){
    if(r.legend.length > 1){
        type = 'line'
    } else {
        type = 'bar'
    }

    var data = {
        series: r.data,
        legend: r.legend,
        umbral: '',
        id: id,
        type : type,
        min : 'dataMin'
    }
    
    ReactDOM.render(React.createElement(Historica, data), document.getElementById(id));
}

app.controller('rastreo', ['$scope', 'request' , function($scope,request){
    // 27/04/2017 - TAG: GOOGLE MAPS
    $scope.user = {
        logged : "<?= Session::getInstance()->logged?>"
    }

    var lineSymbol = {
        path: google.maps.SymbolPath.FORWARD_OPEN_ARROW
    };
    $scope.now = moment().format('YYYY-MM-DD')
    $scope.mostrar = true
    var cargo_pestana_uno = false
    var cargo_pestana_dos = false
    var cargo_pestana_tres = false
    var cargo_pestana_cuatro = false
    var routes = [], map = {}
    $scope.data_ventas_dia = []
    $scope.data_ventas_semana = []
    $scope.data_ventas_mes = []
    $scope.data_ventas_anio = []
    $scope.data_semanas = []
    $scope.table = []
    $scope.filters = {
        sector : "",
        ruta : "",
        search : "",
        tipo : "",
        status : "",
        year : moment().year(),
        semana : "",
        dias : "",
        fecha : "",
        fecha_inicial : "",
        fecha_final : "",
    }
    $scope.createMap = () => {
        map = new GMaps({
            div : '#gmap_routes',
            lat : -2.188861, 
            lng : -79.898896,
            zoom : 19
        });
    }

    $scope.indicadores = function(){
        request.tags((r) =>{
            if(r.hasOwnProperty("tags")){
                var data = {
                    colums : 4,
                    tags: r.tags,
                    withTheresholds: false
                }
                ReactDOM.render(React.createElement(ListTags, data), document.getElementById('indicadores'));
            }
        }, $scope.filters)
    }

    $scope.last = () => {
        request.tags((r) =>{
            if(r.hasOwnProperty("tags")){
                var data = {
                    colums : 3,
                    tags: r.tags,
                    withTheresholds: false,
                }
                ReactDOM.render(React.createElement(ListTags, data), document.getElementById('indicadores'));
            }
        }, $scope.filters)
        request.last((r) => {
            $scope.anios = r.years
            $scope.semanas = r.semanas
            $scope.dias = r.dias
            $scope.filters.year = r.last_year
            $scope.fechas = r.fechas

            $scope.notToday = r.fechas.indexOf($scope.now) == -1
        }, $scope.filters)
    }

    $scope.semaforo = function(ruta, value){
        
    }

    $scope.lastSemana = function(){
        request.last((r) => {
            $scope.semanas = r.semanas
            setTimeout(() => {
                $scope.changeFilter()
            }, 300)
        }, $scope.filters)
    }

    $scope.lastDias = function(){
        request.last((r) => {
            $scope.dias = r.dias
            setTimeout(() => {
                $scope.changeFilter()
            }, 300)
        }, $scope.filters)
    }

    $scope.route = []
    $scope.routeObj = []
    $scope.camiones = {
        camion1 : null
    }

    $scope.StartEndDateDirectives = {
        startDate : moment(),
        endDate :moment(),
    }

    $scope.changeRangeDate = function (data) {
        if(data) {
            $scope.filters.fecha_inicial = data.hasOwnProperty("first_date") ? data.first_date : $scope.filters.fecha_inicial;
            $scope.filters.fecha_final = data.hasOwnProperty("second_date") ? data.second_date : $scope.filters.fecha_final;
            $scope.changeFilter();
        }
    }

    const addMarker = (lat, lng) => {
        var marker = map.addMarker({
            //icon : 'http://app.procesos-iq.com/images/map-camion.png',            
            lat: lat,
            lng: lng,
            title: 'Camión',
            click: function(e) {
              //alert('You clicked in this marker');
            },
            infoWindow: {
                content: '<p>HTML Content</p>'
            }
        });
        $scope.camiones.camion1 = marker
    }

    const moveMarker = (lat, lng) => {
        $scope.camiones.camion1.setPosition(new google.maps.LatLng(lat, lng))
    }
    
    $scope.trazarRuta = () => {
        $scope.removeAllSteps()

        let waypoints = angular.copy($scope.route)
        $scope.startRouting(waypoints)
    }

    $scope.startRouting = (waypoints) => {
        routes.push(map.drawPolyline({
                path: waypoints,
                strokeColor: '#f44256',
                strokeOpacity: 0.6,
                strokeWeight: 2,
                icons: [{
                    icon: lineSymbol,
                    offset: '100%',
                    scale : 10,
                    repeat: '120px'
                }],
            })
        );
    }

    const delay = () => {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                if($scope.filters.fecha == moment().format('YYYY-MM-DD'))
                    $scope.loadMorePoints(true)
            }, 5000)
        })
    }
    const wait = (millis) => {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                resolve()
            }, millis || 1000)
        })
    }
    $scope.volverATrazar = () => {
        wait(10000).then(() => {
            $scope.trazarRuta()
            $scope.volverATrazar()
        })
    }
    $scope.volverATrazar()

    $scope.changeDate = () => {
        if($scope.filters.fecha == moment().format('YYYY-MM-DD'))
            $("#live").removeClass('hide')
        else
            $("#live").addClass('hide')

        $scope.removeAllSteps()
        $scope.route = []
        $scope.routeObj = []
        $scope.loadMorePoints(false)
    }

    const responsePoints = (r) => {
        if(r.data.length > 0){
            if(!$scope.camiones.camion1)
                addMarker(r.data[r.data.length-1].lat, r.data[r.data.length-1].lng)
            else
                moveMarker(r.data[r.data.length-1].lat, r.data[r.data.length-1].lng)
            
            map.setCenter(r.data[r.data.length-1].lat, r.data[r.data.length-1].lng);

            var waypoints = []
            r.data.map((c) => {
                waypoints.push([c.lat, c.lng, false])
            })
            $scope.startRouting(waypoints)
            $scope.route = $scope.route.concat(waypoints)
            $scope.routeObj = $scope.routeObj.concat(r.data)
            renderTablaPosiciones($scope.routeObj)
        }
        delay()
    }

    $scope.loadMorePoints = (morePoints) => {
        if(morePoints){
            request.loadMorePoints(responsePoints, {
                hora : $scope.routeObj.length > 0 ? $scope.routeObj[$scope.routeObj.length-1].time : '',
                fecha : $scope.filters.fecha
            })
        }else{
            request.loadPoints(responsePoints, {
                hora : $scope.routeObj.length > 0 ? $scope.routeObj[$scope.routeObj.length-1].time : '',
                fecha : $scope.filters.fecha
            })
        }
    }

    $scope.removeAllSteps = () => {
        routes.map((value, index) => value.setMap(null))
        $("#gmap_routes_instructions").html("");
    }

    const renderTablaPosiciones = (data) => {
        var props = {
            header : [{
                   key : 'id',
                   name : 'ID',
                   titleClass : 'text-center',
                   alignContent : 'center',
                   sortable : true,
                   expandable : true,
                   resizable : true,
                   width: 170
                },{
                    key : 'lat',
                    name : 'Latitud',
                    titleClass : 'text-center',
                    alignContent : 'center',
                    expandable : true,
                    resizable : true,
                    sortable : true,
                },
                {
                    key : 'lng',
                    name : 'Longitud',
                    titleClass : 'text-center',
                    alignContent : 'center',
                    expandable : true,
                    resizable : true,
                    sortable : true,
                },
                {
                    key : 'speed',
                    name : 'Velocidad (km/h)',
                    titleClass : 'text-center',
                    alignContent : 'center',
                    expandable : true,
                    resizable : true,
                    sortable : true,
                },
                {
                    key : 'time',
                    name : 'Hora',
                    titleClass : 'text-center',
                    alignContent : 'center',
                    expandable : true,
                    resizable : true,
                    sortable : true,
                },
                {
                    key : 'distance',
                    name : 'Km Acumulado',
                    titleClass : 'text-center',
                    alignContent : 'center',
                    expandable : true,
                    resizable : true,
                    sortable : true,
                }
            ],
            data : data,
            buttons : [
                {
                    title : 'Excel',
                    action : () => {
                        $scope.table1.exportToExcel()
                    },
                    className : ''
                }
            ]
        }
        $("#table-posiciones").html("")
        $scope.table1 = ReactDOM.render(React.createElement(ReactDataGrid, props), document.getElementById('table-posiciones'))
    }

    /*$scope.semaforo = value => {
        if(value > 0)
        if($scope.isNumeric(value)){
            return 'bg-green-jungle bg-font-green-jungle'
        }else if(value == 'F'){
            return 'bg-red-thunderbird bg-font-red-thunderbird'
        }
        return ''
    }*/

    $scope.changeFilter = function(){
        $scope.indicadores()
        if(cargo_pestana_uno){
            cargo_pestana_uno = false
            $scope.request.all_pestana_uno()
        }else if(cargo_pestana_dos){
            cargo_pestana_dos = false
            $scope.request.all_pestana_dos()
        }else if(cargo_pestana_tres){
            cargo_pestana_tres = false
            $scope.request.all_pestana_tres()
        }else{
            cargo_pestana_cuatro = false
            $scope.request.all_pestana_cuatro()
        }
    }

	$scope.request = {
		all_pestana_uno : function(){
            if(!cargo_pestana_uno){
                cargo_pestana_uno = true
                cargo_pestana_dos = false
                cargo_pestana_tres = false
                cargo_pestana_cuatro = false
                this.ventaDia();
            }
        },
        all_pestana_dos : function(){
            if(!cargo_pestana_dos){
                cargo_pestana_dos = true
                cargo_pestana_uno = false
                cargo_pestana_tres = false
                cargo_pestana_cuatro = false
                this.ventaSemana();
            }
        },
        all_pestana_tres : function(){
            if(!cargo_pestana_tres){
                cargo_pestana_tres = true
                cargo_pestana_dos = false
                cargo_pestana_uno = false
                cargo_pestana_cuatro = false
                this.ventaMes();
            }
        },
        all_pestana_cuatro : function(){
            if(!cargo_pestana_cuatro){
                cargo_pestana_cuatro = true
                cargo_pestana_dos = false
                cargo_pestana_tres = false
                cargo_pestana_uno = false
                this.ventaAnio();
            }
        },
        ventaDia : function(){
            request.ventaDia(r => {
                if(r.data_venta_dia.length > 0){
                    $scope.data_ventas_dia = r.data_venta_dia
                    var data_chart = r.rutas.map((r) => {
                        return  {
                            label : r.ruta,
                            value : r.total
                        }
                    })
                    renderPie('pastel-ventadia', data_chart)
                    printGrafica('line-ventadia', r.data)
                }else{
                    renderPie('pastel-ventadia', [ { label : 'VENTAS POR DIA', value : 0 }])
                    printGrafica('line-ventadia', 
                    [
                        {"data" : {
                            "data": {
                                "NO HAY VENTAS": {
                                    "name": "NO HAY VENTAS",
                                    "type": "line",
                                    "connectNulls": true,
                                    "data": [
                                        1,
                                        1
                                    ],
                                    "itemStyle": {
                                        "normal": {
                                            "barBorderWidth": 5,
                                            "barBorderRadius": 0,
                                            "label": {
                                                "show": false,
                                                "position": "insideTop"
                                            }
                                        }
                                    }
                                },
                            "legend" : "NO HAY VENTAS"
                            }
                        }}
                    ])
                }
            }, $scope.filters)
        },
        ventaSemana : function(){
            request.ventaSemana(r => {
                if(r.data_venta_semana.length > 0){
                    $scope.data_ventas_semana = r.data_venta_semana
                    var data_chart = r.rutas.map((r) => {
                        return  {
                            label : r.ruta,
                            value : r.total
                        }
                    })
                    renderPie('pastel-ventasemana', data_chart)
                    printGrafica('line-ventasemana', r.data)
                }else{
                    renderPie('pastel-ventasemana', [ { label : 'VENTAS POR SEMANA', value : 0 }])
                }
            }, $scope.filters)
        },
        ventaMes : function(){
            request.ventaMes(r => {
                if(r.data_venta_mes.length > 0){
                    $scope.data_ventas_mes = r.data_venta_mes
                    var data_chart = r.rutas.map((r) => {
                        return  {
                            label : r.ruta,
                            value : r.total
                        }
                    })
                    renderPie('pastel-ventames', data_chart)
                    printGrafica('line-ventames', r.data)
                }else{
                    renderPie('pastel-ventames', [ { label : 'VENTAS POR MES', value : 0 }])
                }
            }, $scope.filters)
        },
        ventaAnio : function(){
            request.ventaAnio(r => {
                if(r.data_venta_anio.length > 0){
                    $scope.data_ventas_anio = r.data_venta_anio
                    var data_chart = r.rutas.map((r) => {
                        return  {
                            label : r.ruta,
                            value : r.total
                        }
                    })
                    renderPie('pastel-ventaanio', data_chart)
                    printGrafica('line-ventaanio', r.data)
                }else{
                    renderPie('pastel-ventaanio', [ { label : 'VENTAS POR AÑO', value : 0 }])
                }
            }, $scope.filters)
        },
        init : function(){
            $scope.createMap();
            $scope.last();
            $scope.loadMorePoints(false)
        }
    }

    $scope.editar = function(){
        request.editar(r => {
            if(r.status == 200){
                alert("Total actualizado correctamente","ALERTA","success")
                $scope.mostrar = true
                $scope.changeFilter()
            } else {
                alert("No se ha enviado ningun total a modificar")
            }
        }, $scope.filters)
    }

    $scope.activar = function(){
        $scope.mostrar = !$scope.mostrar
    }

    $scope.eliminar = function(id){
        $scope.filters.id_venta = id
        request.eliminar(r => {
            if(r.status == 200){
                alert("Registro eliminado correctamente","ALERTA","success")
                $scope.request.all_pestana_uno()
            } else {
                alert("No se ha podido completar la acción, intente de nuevo")
            }
        }, $scope.filters)
    }

    $scope.request.init();
}])