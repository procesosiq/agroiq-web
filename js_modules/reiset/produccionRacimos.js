app.service('produccion', ['client', '$http', function(client, $http){

    this.lastDay = function(callback, params){
        let data = params || {}
        let url = 'phrapi/reiset/racimos/last'
        client.post(url, callback, data)
    }

    this.resumen = function(callback, params){
        let data = params || {}
        let url = 'phrapi/reiset/racimos/resumen'
        client.post(url, callback, data, 'registros')
    }

    this.edades = function(callback, params){
        let data = params || {}
        let url = 'phrapi/reiset/racimos/edades'
        client.post(url, callback, data, 'registros')
    }

    this.registros = function(callback, params){
        let data = params || {}
        let url = 'phrapi/reiset/racimos/historico'
        client.post(url, callback, data, 'registros')
    }

    this.eliminar = function(callback, params){
        let data = params || {}
        let url = 'phrapi/reiset/racimos/eliminar'
        client.post(url, callback, data)
    }

    this.editar = function(callback, params){
        let data = params || {}
        let url = 'phrapi/reiset/racimos/editar'
        client.post(url, callback, data, 'edit_registros')
    }

    this.analisisRecusados = function(callback, params){
        let data = params || {}
        let url = 'phrapi/reiset/racimos/recusados'
        client.post(url, callback, data)
    }

    this.tags = function(callback, params){
        let data = params || {}
        let url = 'phrapi/reiset/racimos/tags'
        client.post(url, callback, data)
    }

    this.recusados = function(callback, params){
        $http.post('phrapi/reiset/racimos/diarecusados', params || {})
        .then((r) => {
            callback(r.data)
        })
    }

    this.muestreo = function(callback, params){
        $http.post('phrapi/reiset/racimos/muestreo', params || {})
        .then((r) => {
            callback(r.data)
        })
    }
}])

app.filter('num', function() {
    return function(input) {
    return parseInt(input, 10);
    };
});

app.filter('startFrom', function() {
    return function(input, start) {
        start = +start; //parse to int
        return input.slice(start);
    }
});

app.filter('sumOfValue', function () {
    return function (data, key) {        
        if (angular.isUndefined(data) || angular.isUndefined(key))
            return 0;        
        var sum = 0;
        angular.forEach(data,function(value){
            if(value[key] != "" && value[key] != undefined && parseFloat(value[key])){
                sum = sum + parseFloat(value[key], 10);
            }
        });
        return sum;
    }
})

app.filter('countOfValue', function () {
    return function (data, key) {
        if (angular.isUndefined(data) || angular.isUndefined(key))
            return 0;        
        var count = 0;
        angular.forEach(data,function(value){
            if(value[key] != undefined && value[key] != null && value[key] > 0){
                count++
            }
        });
        return count;
    }
})

app.filter('avgOfValue', function () {
    return function (data, key) {        
        if (angular.isUndefined(data) || angular.isUndefined(key))
            return 0;        
        var sum = 0;
        var count = 0;
        angular.forEach(data,function(value){
            if(value[key] != "" && value[key] != undefined && parseFloat(value[key])){
                sum = sum + parseFloat(value[key], 10);
                count++;
            }
        });
        sum = sum / count;
        return sum;
    }
})

app.filter('orderObjectBy', function() {
	return function(items, field, reverse) {
    	var filtered = [];
    	angular.forEach(items, function(item) {
            if(field == 'hora' || field == 'fecha'){
                item.date = moment(item.fecha  + ' ' + item.hora)
            }
    		if(!isNaN(parseInt(item))){
    			item = parseInt(item);
    		}
      		filtered.push(item);
    	});
    	filtered.sort(function (a, b) {
            if(field == 'hora' || field == 'fecha'){
                return moment(a.date).isAfter(b.date) ? 1 : -1;
            }else if(parseFloat(a[field]) && parseFloat(b[field])){
				return (parseFloat(a[field]) > parseFloat(b[field]) ? 1 : -1);
			}else{
				return (a[field] > b[field] ? 1 : -1);
			}
    	});
    	if(reverse) filtered.reverse();
    	return filtered;
  	};
});

app.directive('enter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.enter);
                });

                event.preventDefault();
            }
        });
    };
});

app.directive('escape', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if(event.which === 27) {
                scope.$apply(function (){
                    scope.$eval(attrs.escape);
                });

                event.preventDefault();
            }
        });
    };
});

function number_format(amount, decimals) {
    
    amount += ''; // por si pasan un numero en vez de un string
    amount = parseFloat(amount.replace(/[^0-9\.]/g, '')); // elimino cualquier cosa que no sea numero o punto

    decimals = decimals || 0; // por si la variable no fue fue pasada

    // si no es un numero o es igual a cero retorno el mismo cero
    if (isNaN(amount) || amount === 0) 
        return parseFloat(0).toFixed(decimals);

    // si es mayor o menor que cero retorno el valor formateado como numero
    amount = '' + amount.toFixed(decimals);

    var amount_parts = amount.split('.'),
        regexp = /(\d+)(\d{3})/;

    while (regexp.test(amount_parts[0]))
        amount_parts[0] = amount_parts[0].replace(regexp, '$1' + ',' + '$2');

    return amount_parts.join('.');
}

function checkUmbral (value, umbral) {
    if(value && umbral){
        if(value > umbral) return 'bg-red-thunderbird bg-font-red-thunderbird'
        else return 'bg-green-haze bg-font-green-haze'
    }
    return ''
}

function renderPie(id, _data){
    let options = {
        data : _data,
        nameseries : "Pastel",
        titulo : "",
        id : id
    }
    ReactDOM.render(React.createElement(Pastel, options), document.getElementById(id));
}

app.controller('produccion', ['$scope', 'produccion', function($scope, produccion){
    $scope.anioCambio = false
    $scope.id_company = 0;
    $scope.subTittle = ""
    $scope.registros = [];
    $scope.count = 0;

    $scope.produccion = {
        params : {
            finca : 1,
            idFinca : 1,
            idLote : 0,
            idLabor : 0,
            fecha_inicial : moment().startOf('month').format('YYYY-MM-DD'),
            fecha_final : moment().endOf('month').format('YYYY-MM-DD'),
            cliente: "",
            marca: "",
            palanca : "",
            initial : 0,
        },
        nocache : function(){
            $scope.getRegistros()
            if($scope.anioCambio){
                $scope.getAnalisisRecusados()
            }
        }
    }

    $scope.startDate = ''
    $scope.getLastDay = function(){
        produccion.lastDay(function(r, b){
            b()
            if(r){
                $scope.fincas = r.fincas

                $scope.produccion.params.fecha_inicial = r.last.fecha
                $scope.produccion.params.fecha_final = r.last.fecha
                $scope.produccion.params.finca = Object.keys(r.fincas)[0]
                $scope.table = {
                    fecha_inicial : r.last.fecha
                }
                $scope.startDate = r.last.fecha
                $scope.getAnalisisRecusados()
                $scope.produccion.nocache()
            }
        })
    }

    $scope.lastDay = {
        startDate : moment().startOf('month'),
        endDate : moment().endOf('month'),
    }

    $scope.changeRangeDate = function(data){
        if(data){
            $scope.anioCambio = moment(data.first_date).year() != moment($scope.produccion.params.fecha_inicial).year()

            $scope.produccion.params.fecha_inicial = data.hasOwnProperty("first_date") ? data.first_date : $scope.produccion.params.fecha_inicial;
            $scope.produccion.params.fecha_final = data.hasOwnProperty("second_date") ? data.second_date : $scope.produccion.params.fecha_final;
            $scope.subTittle = data.date_select || "Ultimo Registro"
        }
        $scope.produccion.nocache()
    }

    $scope.tabla = {
        produccion : []
    }

    $scope.enableEdit = (row, campo) => {
        if(!row.grupo_racimo > 0 || !row.form){
            if(campo == 'lote'){
                row.editingLote = true
            }
            else if(campo == 'cuadrilla'){
                row.editingCuadrilla = true
            }
        }
    }

    $scope.enter = (row, campo) => {
        if(campo == 'lote'){
            if(row.newLote == ''){
                alert("El lote no puede ser vacio")
            }else{
                produccion.cambiarLote((r, b) => {
                    b()
                    if(r.status == 200){
                        row.lote = row.newLote
                        $scope.check = {}
                        $scope.escape(row, campo)
                    }else{
                        alert('Hubo un problema favor intentelo mas tarde')
                    }
                }, { grupo_racimo : row.grupo_racimo, 
                    lote : row.newLote, 
                    id_finca : $scope.produccion.params.finca,
                    id_formulario : row.id_formulario
                })
            }
        }else if(campo == 'cuadrilla'){
            if(row.newCuadrilla == ''){
                alert("La cuadrilla no puede estar vacia")
            }else{
                produccion.cambiarCuadrilla((r, b) => {
                    b()
                    if(r.status == 200){
                        row.cuadrilla = row.newCuadrilla
                        $scope.check = {}
                        $scope.escape(row, campo)
                    }else{
                        alert('Hubo un problema favor intentelo mas tarde')
                    }
                }, { grupo_racimo : row.grupo_racimo, 
                    cuadrilla : row.newCuadrilla, 
                    id_finca : $scope.produccion.params.finca,
                    id_formulario : row.id_formulario 
                })
            }
        }
    }

    $scope.escape = (row, campo) => {
        if(campo == 'lote'){
            row.editingLote = false
        }
        else if(campo == 'cuadrilla'){
            row.editingCuadrilla = false
        }
    }

    $scope.eliminar = () => {
        var elements = $(".delete")
        if(elements.length > 0){
            if(confirm(`Seguro deseas eliminar ${elements.length} elementos?`)){
                var ids = []
                var index = []
                for(var x = 0; x < elements.length; x++){
                    ids.push({
                        id : $(elements[x]).attr('data-id')
                    })
                }
                produccion.eliminar((r, b) => { 
                    b()
                    if(r){
                        $scope.getRegistros()
                        alert(`Se borraron ${elements.length} registros con éxito`, "", "success")
                    }
                }, { ids : ids })
            }
        }else{
            alert("No has seleccionado ninguna fila")
        }
    }

    $scope.colores = [];
    $scope.totales = [];
    $scope.palancas = [];

    $scope.getRegistros = function(){
        $scope.cargandoHistorico = true
        let data = $scope.produccion.params;
        produccion.registros(function(r, b){
            b('registros')
            $scope.cargandoHistorico = false

            if(r){
                $scope.registros = r.data
                $scope.searchTable.changePagination()
            }
        }, data)

        produccion.resumen(function(r, b){
            b()
            if(r){
                $scope.resumen = r.data
                $scope.edades = r.edades
            }
        }, data)

        produccion.edades(function(r, b){
            b()
            if(r){
                $scope.resumen_edades = r.data
            }
        }, data)

        produccion.tags(function(r, b){
            b()
            if(r){
                $scope.tags = r.tags
            }
        }, data)

        produccion.recusados((r) => {
            if(r){
                $scope.recusados = r.data

                var data_chart = r.data.map((r) => {
                    return  {
                        label : r.causa,
                        value : r.cantidad
                    }
                })
                renderPie('grafica-defectos', data_chart)
            }
        }, data)

        if($scope.produccion.params.initial == 0){
            produccion.muestreo((r) => {
                $scope.muestreo = r.data
                $scope.openMuestreo()
            }, $scope.produccion.params)
        }
    }

    $scope.openMuestreo = () => {
        setTimeout(() => {
            if($("[href=#collapse_3_6]").attr('aria-expanded') == 'true'){
                new echartsPlv().init('grafica-muestreo', $scope.muestreo)
            }
        }, 250)
    }

    $scope.agregarRacimo = () => {

    }

    $scope.edit = function(row){
        if(!$scope.editing){
            $scope.editing = true;
            $scope.editRow = row;
            $scope.editRow.editing = true;
        }
    }

    $scope.guardar = function(){
        var valid = true
        if(!$scope.editing) valid = false;
        if($scope.editRow.peso <= 0) valid = false;
        if($scope.editRow.cuadrilla == "") valid = false;
        if($scope.editRow.lote == "") valid = false;
        if($scope.editRow.edad == "") valid = false;
        
        if(valid){
            $scope.editRow.editing = false;
            $scope.editing = false;
            produccion.editar((r, b) => {
                b('edit_registros')
                if(r){
                    $scope.editRow.class = r.class
                }else{
                    alert("Ocurrio un error al guardar")
                }
            }, $scope.editRow)
        }else{
            alert("Favor de completar los campos")
        }
    }

    $scope.next = function(dataSource){
        if($scope.searchTable.actual_page < parseInt(dataSource.length / parseInt($scope.searchTable.limit)) + (dataSource.length % parseInt($scope.searchTable.limit) == 0 ? 0 : 1)) 
            $scope.searchTable.actual_page++;
        
        $scope.searchTable.startFrom = ($scope.searchTable.actual_page - 1) * (parseInt($scope.searchTable.limit))
    }

    $scope.prev = function(dataSource){
        if($scope.searchTable.actual_page > 1)
            $scope.searchTable.actual_page--;
        
        $scope.searchTable.startFrom = ($scope.searchTable.actual_page - 1) * (parseInt($scope.searchTable.limit))
    }

    $scope.setOrderTable = function(field){
        var reverse = false
        if(field == $scope.searchTable.orderBy){
            reverse = !$scope.searchTable.reverse
        }else{
            $scope.searchTable.orderBy = field
        }
        $scope.searchTable.reverse = reverse
    }

    $scope.searchTable = {
        orderBy : "id",
        reverse : false,
        limit : 10,
        actual_page : 1,
        startFrom : 0,
        optionsPagination : [
            10, 50, 100
        ],
        changePagination : function(){
            if($scope.searchTable.limit == 0)
                $scope.searchTable.limit = $scope.registros.length

            $scope.searchTable.numPages = parseInt($scope.registros.length / $scope.searchTable.limit)  + ($scope.registros.length % $scope.searchTable.limit == 0 ? 0 : 1)
            $scope.searchTable.actual_page = 1
            $scope.searchTable.startFrom = 0
        }
    }

    $scope.exportExcel = function(id_table, title){
        var data = new Blob([document.getElementById(id_table).outerHTML], {
            type : 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf8'
        })
        saveAs(data, 'Base de datos racimos.xls')
    }

    $scope.exportPrint = function(id_table){
        $("#"+id_table).css("display", "")
        let image = '<div style="display:block; height:55;"><img style="float: right;" width="100" height="50" src="./../logos/Logo.png" /></div><br>';
        let table = document.getElementById(id_table);
        var contentTable = table.outerHTML;
        newWin = window.open("");
        newWin.document.write(contentTable);
        newWin.print();
        newWin.close();
        $("#"+id_table).css("display", "none")
    }

    $scope.exportarFormatoEspecial = () => {
        dayOfWeek = (day) => { return ["DOMINGO", "LUNES", "MARTES", "MIERCOLES", "JUEVES", "VIERNES", "SABADO"][day]  };
        embedInRow = (cols) => { return `<tr>${cols}</tr>` };
        generateRow = () => { var cols = []; for(var i = 0; i < 10; i ++) { cols.push("<td></td>") } return cols; };
        getCintaValida = (cinta) => { return { VERDE : 'VERDE', ROJA : 'ROJA', AMARILLO : 'AMARI', CAFE  : 'CAFE', AZUL : 'AZUL', BLANCO : 'BLANC', NEGRO : 'NEGRA', LILA : 'LILA' }[cinta]; }
        var table = []
        
        row = generateRow();
        row[0] = `<td>Archivo no.: ${moment($scope.produccion.params.fecha_inicial).format('DDD')}</td>`;
        table.push("<tr>" + row.join("") + "</tr>")

        row = generateRow();
        row[0] = `<td>Nombre: SEMANA ${moment($scope.produccion.params.fecha_inicial).format('WW')} ${dayOfWeek(moment($scope.produccion.params.fecha_inicial).format('d'))} ${moment($scope.produccion.params.fecha_inicial).format('DD')}</td>`;
        table.push("<tr>" + row.join("") + "</tr>")

        row = generateRow();
        row[0] = `<td>Fecha: ${moment($scope.produccion.params.fecha_inicial).format('DD-MM-YYYY')}</td>`;
        table.push("<tr>" + row.join("") + "</tr>")

        table.push([
            "<tr>",
            "<td>F01IDV(4)isID</td>",
            "<td>DW2Peso()</td>",
            "<td>C03LOTE()</td>",
            "<td>C13CINTA()</td>",
            "<td>F12MANOS(##)notID</td>",
            "<td>F22CALIBRE(##.##)notID</td>",
            "<td>F32L DEDOS(##.##)notID</td>",
            "<td>F43TIPO()notID</td>",
            "<td>F53NIVEL()notID</td>",
            "<td>F63CAUSA()notID</td>",
            "</tr>"
        ].join(""))

        table.push([
            "<tr>",
            "<td>IDV</td>",
            "<td>Peso</td>",
            "<td>LOTE</td>",
            "<td>CINTA</td>",
            "<td>MANOS</td>",
            "<td>CALIBRE</td>",
            "<td>L DEDOS</td>",
            "<td>TIPO</td>",
            "<td>NIVEL</td>",
            "<td>CAUSA</td>",
            "</tr>"
        ].join(""))

        $scope.registros.map((value, index) => {
            if(parseInt(value.id) > 0){
                table.push([
                    "<tr>",
                    `<td>${value.id}</td>`,
                    `<td>${value.peso}</td>`,
                    `<td>${value.lote}</td>`,
                    `<td>${getCintaValida(value.cinta)}</td>`,
                    `<td>${(value.manos > 0) ? value.manos : ''}</td>`,
                    `<td>${(value.calibre > 0) ? value.calibre : ''}</td>`,
                    `<td>${(value.dedos > 0) ? value.dedos : ''}</td>`,
                    `<td></td>`,
                    `<td>${(value.tipo == 'RECU') ? 'RECHA' : ''}</td>`,
                    `<td>${(value.tipo == 'RECU') ? value.causa : ''}</td>`,
                    "</tr>"
                ].join(""))
            }
        })

        // Añadie cuadre
        if($scope.dia_cuadrado){
            $scope.cuadre_proc.map((value, index) => {
                var diferencia = value.form - value.blz 
                for(var i = 0; i < diferencia; i++){
                    table.push([
                        "<tr>",
                        `<td></td>`,
                        `<td>${value.peso_prom}</td>`,
                        `<td>${value.lote}</td>`,
                        `<td>${getCintaValida(value.cinta)}</td>`,
                        `<td></td>`,
                        `<td></td>`,
                        `<td></td>`,
                        `<td></td>`,
                        `<td></td>`,
                        `<td></td>`,
                        "</tr>"
                    ].join(""))
                }
            })

            $scope.faltante_recu.map((value, index) => {
                var diferencia = value.cantidad - value.cantidad_blz 
                for(var i = 0; i < diferencia; i++){
                    table.push([
                        "<tr>",
                        `<td></td>`,
                        `<td>${value.peso_prom}</td>`,
                        `<td>${value.lote}</td>`,
                        `<td>${getCintaValida(value.color_cinta)}</td>`,
                        `<td></td>`,
                        `<td></td>`,
                        `<td></td>`,
                        `<td></td>`,
                        `<td>RECHA</td>`,
                        `<td>${value.causa}</td>`,
                        "</tr>"
                    ].join(""))
                }
            })
        }
        
        var tableToExcel = (function() {
            var uri = 'data:application/vnd.ms-excel;base64,'
                , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body><table>{table}</table></body></html>'
                , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
                , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
            return function() {
                var contentTable = table.join("")
                var ctx = {worksheet: name || 'Worksheet', table: contentTable}
                window.location.href = uri + base64(format(template, ctx))
            }
        })()
        tableToExcel()
    }

    $scope.getAnalisisRecusados = () => {
        produccion.analisisRecusados((r, b) => {
            b()
            r.data.map((value) => {
                if(value.dano == 'TOTAL') $scope.umbralRecusados = value.prom
            })
            printTableRecusados(r.data, r.semanas)
        }, $scope.produccion.params)
    }

    var printTableRecusados = (data, semanas) => {
        let id = 'table-defectos'
        var props = {
            header : [{
                   key : 'dano',
                   name : 'DEFECTO',
                   titleClass : 'text-center',
                   locked : true,
                   expandable : true,
                   resizable : true,
                   width: 170
                },{
                    key : 'sum',
                    name : 'SUM',
                    locked : true,
                    sortable : true,
                    titleClass : 'text-center',
                    alignContent : 'right',
                    filterable : true,
                    filterRenderer: 'NumericFilter',
                    formatter : 'Number',
                    resizable : true,
                    customCell : function(rowData, isChildren){
                        var valNumber = parseFloat(rowData['sum'])
                        if($scope.produccion.params.var_recusado == 'porc') valNumber = 0
                        let valueCell = valNumber > 0 ? number_format(valNumber, ($scope.var_recusado == 'cant') ? 0 : 2) : ''
                        return `
                             <div class="text-center ${checkUmbral(valNumber, $scope.umbralRecusados)}" style="height: 100%">
                                 ${valueCell}
                             </div>
                        `;
                     }
                 },{
                   key : 'prom',
                   name : 'AVG',
                   locked : true,
                   sortable : true,
                   titleClass : 'text-center',
                   alignContent : 'right',
                   filterable : true,
                   filterRenderer: 'NumericFilter',
                   formatter : 'Number',
                   resizable : true,
                   customCell : function(rowData, isChildren){
                        let valNumber = parseFloat(rowData['prom'])
                        let valueCell = valNumber > 0 ? number_format(valNumber, ($scope.var_recusado == 'cant') ? 0 : 2) : ''
                        return `
                            <div class="text-center ${checkUmbral(valNumber, $scope.umbralRecusados)}" style="height: 100%">
                                ${valueCell}
                            </div>
                        `;
                    }
                },{
                   key : 'max',
                   name : 'MAX',
                   locked : true,
                   titleClass : 'text-center',
                   sortable : true,
                   alignContent : 'right',
                   filterable : true,
                   filterRenderer: 'NumericFilter',
                   resizable : true,
                   customCell : function(rowData, isChildren){
                        let valNumber = parseFloat(rowData['max'])
                        let valueCell = valNumber > 0 ? number_format(valNumber, ($scope.var_recusado == 'cant') ? 0 : 2) : ''
                        return `
                            <div class="text-center ${checkUmbral(valNumber, $scope.umbralRecusados)}" style="height: 100%">
                                ${valueCell}
                            </div>
                        `;
                    }
                },{
                   key : 'min',
                   name : 'MIN',
                   locked : true,
                   sortable : true,
                   titleClass : 'text-center',
                   alignContent : 'right',
                   filterable : true,
                   filterRenderer: 'NumericFilter',
                   resizable : true,
                   customCell : function(rowData, isChildren){
                        let valNumber = parseFloat(rowData['min'])
                        let valueCell = valNumber > 0 ? number_format(valNumber, ($scope.var_recusado == 'cant') ? 0 : 2) : ''
                        return `
                            <div class="text-center ${checkUmbral(valNumber, $scope.umbralRecusados)}" style="height: 100%">
                                ${valueCell}
                            </div>
                        `;
                    }
                }
            ],
            data : data,
            buttons : [
                {
                    title : 'Excel',
                    action : () => {
                        $scope.table1.exportToExcel()
                    },
                    className : ''
                }
            ]
        }
        semanas.map((value) => {
            props.header.push({
                key : `sem_${value}`,
                name : `${value}`,
                sortable : true,
                alignContent : 'right',
                titleClass : 'text-center',
                filterable : true,
                filterRenderer: 'NumericFilter',
                resizable : true,
                customCell : function(rowData, isChildren){
                    let valNumber = parseFloat(rowData['sem_'+value])
                    let valueCell = valNumber > 0 ? number_format(valNumber, ($scope.var_recusado == 'cant') ? 0 : 2) : ''
                    return `
                        <div class="text-center ${checkUmbral(valNumber, $scope.umbralRecusados)}" style="height: 100%">
                            ${valueCell}
                        </div>
                    `;
                }
            })
        })
        document.getElementById(id).innerHTML = ""
        $scope.table1 = ReactDOM.render(React.createElement(ReactDataGrid, props), document.getElementById(id))
    }

}]);

