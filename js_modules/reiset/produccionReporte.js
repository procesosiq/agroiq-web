app.service('request', ['$http', function($http){
    
    this.data = function(callback, params){
        let url = 'phrapi/reiset/produccionreporte/data'
        load.block()
        $http.post(url, params || {}).then(r => {
            load.unblock()
            callback(r.data)
        })
    }

    this.data2 = function(callback, params){
        let url = 'phrapi/reiset/produccionreporte/reporte2'    
        $http.post(url, params || {}).then(r => {
            callback(r.data)
        })
    }

    this.getWeeks = function(callback, params){
        let url = 'phrapi/reiset/produccionreporte/weeks'
        $http.post(url, params || {}).then(r => {
            callback(r.data)
        })
    }

    this.last = function(callback, params){
        let url = 'phrapi/reiset/produccionreporte/last'
        $http.post(url, params || {}).then(r => {
            callback(r.data)
        })
    }
}])

app.filter('avgOfValue', function () {
    return function (data, key) {        
        if (angular.isUndefined(data) || angular.isUndefined(key))
            return 0;        
        var sum = 0;
        var count = 0;
        angular.forEach(data,function(value){
            if(value[key] != "" && value[key] != undefined){
                sum = sum + parseFloat(value[key], 10);
                count++;
            }
        });
        sum = sum / count;
        if(isNaN(sum))
            return 0;
        return sum;
    }
})

app.controller('controller', ['$scope','request', function($scope, $request){

    $scope.filters = {}
    $scope.changeRangeDate = (data) => {
        if(data){
            $scope.year = ''
            $scope.semana = ''
            $scope.filters.fecha_inicial = data.hasOwnProperty("first_date") ? data.first_date : $scope.filters.fecha_inicial;
            $scope.filters.fecha_final = data.hasOwnProperty("second_date") ? data.second_date : $scope.filters.fecha_final;
            getData()
        }
    }

    $scope.changeYear = () => {
        if($scope.filters.year != ''){
            $request.getWeeks((r) => {
                $scope.semanas = r.semanas
                if(r.semanas.indexOf($scope.filters.semana) == -1){
                    $scope.filters.semana = ''
                }
            }, {
                year: $scope.filters.year
            })
        }
    }
    $scope.changeWeek = () => {
        if($scope.filters.semana != '' && $scope.filters.year != ''){
            $("#date-picker").html(`N/A - N/A`)
            $scope.filters.fecha_inicial = ''
            $scope.filters.fecha_final = ''
            getData()
        }
    }
    $scope.changeSector = () => getData()

    getData = () => {
        $request.data((r) => {
            $scope.data = r.data
            $scope.totales = r.totales
            $scope.edades = r.edades
            $scope.sectores = r.sectores
        }, $scope.filters)

        $request.data2((r) => {
            renderReporte2(r.data)
        }, $scope.filters)
    }

    renderReporte2 = (data) => {
        var props = {
            header : [
                {
                   key : 'lote',
                   name : 'LOTE',
                   locked : true,
                   sortable : true,
                   titleClass : 'text-center',
                   alignContent : 'center',
                   filterable : true,
                   resizable : true,
                },{
                   key : 'hectareas',
                   name : 'HA',
                   titleClass : 'text-center',
                   sortable : true,
                   alignContent : 'center',
                   resizable : true,
                }/*,{
                   key : 'enfunde',
                   name : 'TOT. ENF.',
                   sortable : true,
                   titleClass : 'text-center',
                   alignContent : 'center',
                   filterable : true,
                   filterRenderer: 'NumericFilter',
                   resizable : true,
                },{
                    key : 'enfunde_ha',
                    name : 'TOT. ENFU. / HA',
                    sortable : true,
                    titleClass : 'text-center',
                    alignContent : 'center',
                    filterable : true,
                    filterRenderer: 'NumericFilter',
                    resizable : true,
                    width : 150
                 }*/,{
                    key : 'peso_prom_racimo',
                    name : 'PESO PROM.',
                    sortable : true,
                    titleClass : 'text-center',
                    alignContent : 'center',
                    filterable : true,
                    filterRenderer: 'NumericFilter',
                    resizable : true,
                    width : 100
                 },{
                    key : 'manos',
                    name : 'NO. MANOS',
                    sortable : true,
                    titleClass : 'text-center',
                    alignContent : 'center',
                    filterable : true,
                    filterRenderer: 'NumericFilter',
                    resizable : true,
                    width : 100
                 },{
                    key : 'calibre',
                    name : 'CALIB.',
                    sortable : true,
                    titleClass : 'text-center',
                    alignContent : 'center',
                    filterable : true,
                    filterRenderer: 'NumericFilter',
                    resizable : true,
                 }/*,{
                    key : 'dedos',
                    name : 'L. DEDOS',
                    sortable : true,
                    titleClass : 'text-center',
                    alignContent : 'center',
                    filterable : true,
                    filterRenderer: 'NumericFilter',
                    resizable : true,
                 }*/,{
                    key : 'merma_total',
                    name : 'MERMA TOT.',
                    sortable : true,
                    titleClass : 'text-center',
                    alignContent : 'center',
                    filterable : true,
                    filterRenderer: 'NumericFilter',
                    resizable : true,
                    width: 100
                 },{
                    key : 'racimo_ha',
                    name : 'RAC. /HA',
                    sortable : true,
                    titleClass : 'text-center',
                    alignContent : 'center',
                    filterable : true,
                    filterRenderer: 'NumericFilter',
                    resizable : true,
                 },{
                    key : 'porc_racimos_recu',
                    name : '% RAC. RECH.',
                    sortable : true,
                    titleClass : 'text-center',
                    alignContent : 'center',
                    filterable : true,
                    filterRenderer: 'NumericFilter',
                    resizable : true,
                    width: 100
                 },{
                    key : 'racimos_recu_ha',
                    name : 'RAC. RECH. /HA',
                    sortable : true,
                    titleClass : 'text-center',
                    alignContent : 'center',
                    filterable : true,
                    filterRenderer: 'NumericFilter',
                    resizable : true,
                    width: 120
                 },{
                    key : 'racimos_proc_ha',
                    name : 'RAC. PROC. /HA',
                    sortable : true,
                    titleClass : 'text-center',
                    alignContent : 'center',
                    filterable : true,
                    filterRenderer: 'NumericFilter',
                    resizable : true,
                    width: 120
                 },{
                    key : 'cajas_ha',
                    name : 'CAJAS /HA',
                    sortable : true,
                    titleClass : 'text-center',
                    alignContent : 'center',
                    filterable : true,
                    filterRenderer: 'NumericFilter',
                    resizable : true,
                 },{
                    key : 'ratio_cortado',
                    name : 'RATIO CORTADO',
                    sortable : true,
                    titleClass : 'text-center',
                    alignContent : 'center',
                    filterable : true,
                    filterRenderer: 'NumericFilter',
                    resizable : true,
                    width: 100
                 },{
                    key : 'cajas_ha_proyeccion',
                    name : 'CAJAS /HA PROYEC',
                    sortable : true,
                    titleClass : 'text-center',
                    alignContent : 'center',
                    filterable : true,
                    filterRenderer: 'NumericFilter',
                    resizable : true,
                    width: 100
                 }
            ],
            data : data,
            buttons : [
                {
                    title : 'Excel',
                    action : () => {
                        $scope.table1.exportToExcel()
                    },
                    className : ''
                }
            ]
        }
        $("#table-reporte").html("")
        $scope.table1 = ReactDOM.render(React.createElement(ReactDataGrid, props), document.getElementById('table-reporte'))
    }

    $request.last((r) => {
        $scope.filters = {
            fecha_inicial : r.fecha_inicial,
            fecha_final : r.fecha_final,
            year : '',
            semana : '',
            sector : ''
        }
        $scope.years = r.years
        $scope.semanas = r.weeks
        //$("#date-picker").html(`${r.fecha_inicial} - ${r.fecha_final}`)
        $scope.filters.year = r.last_year
        $scope.filters.semana = r.last_week
        getData()
    })
    
}]);