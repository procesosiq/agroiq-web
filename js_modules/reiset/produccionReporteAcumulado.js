app.service('request', ['$http', function($http){
    
    this.data2 = function(callback, params){
        let url = 'phrapi/reiset/produccionreporteacumulado/data2'
        load.block()
        $http.post(url, params || {}).then(r => {
            load.unblock()
            callback(r.data)
        })
    }

    this.getWeeks = function(callback, params){
        let url = 'phrapi/reiset/produccionreporteacumulado/weeks'
        $http.post(url, params || {}).then(r => {
            callback(r.data)
        })
    }

    this.last = function(callback, params){
        let url = 'phrapi/reiset/produccionreporteacumulado/last'
        $http.post(url, params || {}).then(r => {
            callback(r.data)
        })
    }
}])

app.filter('avgOfValue', function () {
    return function (data, key) {        
        if (angular.isUndefined(data) || angular.isUndefined(key))
            return 0;        
        var sum = 0;
        var count = 0;
        angular.forEach(data,function(value){
            if(value[key] != "" && value[key] != undefined){
                sum = sum + parseFloat(value[key], 10);
                count++;
            }
        });
        sum = sum / count;
        if(isNaN(sum))
            return 0;
        return sum;
    }
})

app.controller('controller', ['$scope','request', function($scope, $request){

    $scope.filters = {
        desde : { semana : 0, anio : 0},
        hasta : { semana : 0, anio : 0}
    }
    $scope.semanas = []

    $scope.changeSector = () => {
        getData()
    }

    $scope.changeRangeDate = () => {
        setTimeout(() => {
            let desde = $scope.filters.desde_concated.split('-')
            let hasta = $scope.filters.hasta_concated.split('-')
            let filters = {
                desde_concated : $scope.filters.desde_concated,
                hasta_concated : $scope.filters.hasta_concated,
                desde : {
                    anio : desde[0],
                    semana : desde[1]
                },
                hasta : {
                    anio : hasta[0],
                    semana : hasta[1]
                },
                sector : $scope.filters.sector
            }
            $scope.filters = filters
            console.log($scope.filters)
            getData(filters)
        }, 200)
    }

    $scope.StartEndDateDirectives = {
        startDate : moment().startOf('week'),
        endDate :moment().endOf('week'),
    }

    const renderReporte2 = (data) => {
        var props = {
            header : [
                {
                   key : 'lote',
                   name : 'LOTE',
                   locked : true,
                   sortable : true,
                   titleClass : 'text-center',
                   alignContent : 'center',
                   filterable : true,
                   resizable : true,
                },{
                   key : 'hectareas',
                   name : 'HA',
                   titleClass : 'text-center',
                   sortable : true,
                   alignContent : 'center',
                   resizable : true,
                }/*,{
                   key : 'enfunde',
                   name : 'TOT. ENF.',
                   sortable : true,
                   titleClass : 'text-center',
                   alignContent : 'center',
                   filterable : true,
                   filterRenderer: 'NumericFilter',
                   resizable : true,
                },{
                    key : 'enfunde_ha',
                    name : 'TOT. ENFU. / HA',
                    sortable : true,
                    titleClass : 'text-center',
                    alignContent : 'center',
                    filterable : true,
                    filterRenderer: 'NumericFilter',
                    resizable : true,
                    width : 150
                 }*/,{
                    key : 'peso_prom_racimo',
                    name : 'PESO PROM.',
                    sortable : true,
                    titleClass : 'text-center',
                    alignContent : 'center',
                    filterable : true,
                    filterRenderer: 'NumericFilter',
                    resizable : true,
                    width : 100
                 },{
                    key : 'manos',
                    name : 'NO. MANOS',
                    sortable : true,
                    titleClass : 'text-center',
                    alignContent : 'center',
                    filterable : true,
                    filterRenderer: 'NumericFilter',
                    resizable : true,
                    width : 100
                 },{
                    key : 'calibre',
                    name : 'CALIB.',
                    sortable : true,
                    titleClass : 'text-center',
                    alignContent : 'center',
                    filterable : true,
                    filterRenderer: 'NumericFilter',
                    resizable : true,
                 },/*{
                    key : 'dedos',
                    name : 'L. DEDOS',
                    sortable : true,
                    titleClass : 'text-center',
                    alignContent : 'center',
                    filterable : true,
                    filterRenderer: 'NumericFilter',
                    resizable : true,
                 },*/{
                    key : 'merma_total',
                    name : 'MERMA TOT.',
                    sortable : true,
                    titleClass : 'text-center',
                    alignContent : 'center',
                    filterable : true,
                    filterRenderer: 'NumericFilter',
                    resizable : true,
                    width: 100
                 },{
                    key : 'racimo_ha',
                    name : 'RAC. /HA',
                    sortable : true,
                    titleClass : 'text-center',
                    alignContent : 'center',
                    filterable : true,
                    filterRenderer: 'NumericFilter',
                    resizable : true,
                 },{
                    key : 'porc_racimos_recu',
                    name : '% RAC. RECH.',
                    sortable : true,
                    titleClass : 'text-center',
                    alignContent : 'center',
                    filterable : true,
                    filterRenderer: 'NumericFilter',
                    resizable : true,
                    width: 100
                 },{
                    key : 'racimos_recu_ha',
                    name : 'RAC. RECH. /HA',
                    sortable : true,
                    titleClass : 'text-center',
                    alignContent : 'center',
                    filterable : true,
                    filterRenderer: 'NumericFilter',
                    resizable : true,
                    width: 120
                 },{
                    key : 'racimos_proc_ha',
                    name : 'RAC. PROC. /HA',
                    sortable : true,
                    titleClass : 'text-center',
                    alignContent : 'center',
                    filterable : true,
                    filterRenderer: 'NumericFilter',
                    resizable : true,
                    width: 120
                 },{
                    key : 'cajas_ha',
                    name : 'CAJAS /HA',
                    sortable : true,
                    titleClass : 'text-center',
                    alignContent : 'center',
                    filterable : true,
                    filterRenderer: 'NumericFilter',
                    resizable : true,
                 },{
                    key : 'ratio_cortado',
                    name : 'RATIO CORTADO',
                    sortable : true,
                    titleClass : 'text-center',
                    alignContent : 'center',
                    filterable : true,
                    filterRenderer: 'NumericFilter',
                    resizable : true,
                    width: 100
                 },{
                    key : 'cajas_ha_proyeccion',
                    name : 'CAJAS /HA PROYEC',
                    sortable : true,
                    titleClass : 'text-center',
                    alignContent : 'center',
                    filterable : true,
                    filterRenderer: 'NumericFilter',
                    resizable : true,
                    width: 100
                 }
            ],
            data : data,
            buttons : [
                {
                    title : 'Excel',
                    action : () => {
                        $scope.table1.exportToExcel()
                    },
                    className : ''
                }
            ]
        }
        $("#table-reporte").html("")
        $scope.table1 = ReactDOM.render(React.createElement(ReactDataGrid, props), document.getElementById('table-reporte'))
    }

    const getData = (filters = $scope.filters) => {
        $request.data2((r) => {
            renderReporte2(r.data)
        }, filters)
    }

    $request.last((r) => {
        $scope.semanas = r.weeks
        $scope.sectores = r.sectores
        $scope.filters.desde_concated = r.weeks[0].concated
        $scope.filters.hasta_concated = r.weeks[0].concated
        $scope.filters.desde = r.weeks[0]
        $scope.filters.hasta = r.weeks[0]
        setTimeout(() => {
            $("#date-picker").html(`${r.last_date} - ${r.last_date}`)
        }, 200)
        getData()
    })
    
}]);