'use strict';

app.filter('startfrom', function () {
    return function (input, start) {
        if (input === undefined) {
            return "loading...";
        }
        start = +start; //parse to int
        console.log(input);
        return input.slice(start);
    };
});

app.filter('num', function () {
    return function (input) {
        return parseInt(input, 10);
    };
});

app.service('productos', function ($http) {
    var service = {};
    var options = {
        url: "phrapi/productos/index",
        method: "POST"
    };

    service.listado = function () {
        var option = options;
        option.url = "phrapi/productos/index";
        return $http(option);
    };

    service.formuladoras = function () {
        var option = options;
        option.url = "phrapi/productos/formuladoras/get";
        return $http(option);
    };

    service.proveedores = function () {
        var option = options;
        option.url = "phrapi/productos/proveedores/get";
        return $http(option);
    };

    service.tipoProductos = function () {
        var option = options;
        option.url = "phrapi/productos/tipoProductos/get";
        return $http(option);
    };

    service.frac = function () {
        var option = options;
        option.url = "phrapi/productos/frac/get";
        return $http(option);
    };

    // 26/04/2017 - TAG: PRECIOS
    service.precios = function (id_producto) {
        var option = options;
        options.url = "phrapi/productos/getPrecios";
        option.data = {
            id_producto: id_producto
        };
        return $http(option);
    };
    service.addPrecios = function (precios) {
        var option = options;
        option.url = "phrapi/productos/savePrecios";
        option.data = precios;
        return $http(option);
    };

    service.acciones = function () {
        var option = options;
        option.url = "phrapi/productos/getAccion";
        return $http(option);
    };

    service.show = function (id_producto) {
        var option = options;
        option.url = "phrapi/productos/show";
        option.data = {
            id_producto: id_producto
        };
        return $http(option);
    };

    service.create = function (producto) {
        var option = options;
        option.url = "/phrapi/productos/create";
        option.data = producto;
        return $http(option);
    };

    service.update = function (producto) {
        var option = options;
        option.url = "phrapi/productos/update";
        option.data = producto;
        return $http(option);
    };

    return service;
});

app.service('configuracion', function ($http) {
    var configuracion = {};
    var options = {
        url: "./phrapi/",
        method: "POST"
    };

    configuracion.save = function (detalle) {
        var option = options;
        option.url = "phrapi/productos/saveConfiguracion";
        option.data = detalle;
        return $http(option);
    };

    return configuracion;
});

app.controller('productos', ['$scope', '$http', '$interval', 'productos', '$controller', 'configuracion', function ($scope, $http, $interval, productos, $controller, configuracion) {

    // 26/04/2017 - TAG: CONFIGURACION
    $scope.configuracion = {
        state: "listado",
        modo: "",
        nombre: "",
        campo: "",
        save: function save() {
            if (this.campo != "") {
                var nombre = this.nombre;
                var modo = this.modo;
                var campo = this.campo;
                var data = {
                    nombre: nombre,
                    modo: modo,
                    campo: campo
                };
                $('#myModal').modal('hide');
                configuracion.save(data).then(function (data) {
                    if ($scope.configuracion.state != "listado") {
                        if ($scope.configuracion.nombre == "Formuladora") $scope.getFormuladoras();
                        if ($scope.configuracion.nombre == "Proveedor") $scope.getProveedores();
                        if ($scope.configuracion.nombre == "Tipo de Producto") $scope.getTipoProductos();
                    }
                }).catch(function (error) {
                    return console.log(error);
                });
            }
        }
    };

    $scope.setTipo = function (tipo) {
        $scope.configuracion.nombre = tipo;
        if ($scope.configuracion.nombre == "Formuladora") $scope.configuracion.modo = "formuladora";
        if ($scope.configuracion.nombre == "Proveedor") $scope.configuracion.modo = "proveedor";
        if ($scope.configuracion.nombre == "Tipo de Producto") $scope.configuracion.modo = "tipo_producto";
    };
    // 26/04/2017 - TAG: CONFIGURACION
    $scope.formuladoras = [];
    $scope.proveedores = [];
    $scope.tipoProductos = [];
    $scope.ingredientes_actios = [];
    $scope.fracs = [];
    $scope.acciones = [];

    // 26/04/2017 - TAG: PRECIO
    $scope.precio = {
        precio: "",
        fecha: ""
    };

    $scope.precios = [];

    // 25/04/2017 - TAG: MODEL FOR PRODUCTS
    $scope.clear = function () {
        $scope.producto = $scope.oldProducto;
        $scope.precio.precio = "";
        $scope.precio.fecha = "";
    };

    $scope.producto = {
        id_producto: 0,
        codigo: "",
        nombreComercial: "",
        formuladora: "",
        proveedor: "",
        tipo_producto: "",
        ingrediente_activo: "",
        frac: "",
        action: "",
        dosis: "",
        dosis_2: "",
        dosis_3: ""
    };

    $scope.oldProducto = angular.copy($scope.producto);

    $scope.proccess = function (r) {
        if (r.hasOwnProperty("data")) {
            return r.data;
        }
    };

    $scope.getFormuladoras = function () {
        productos.formuladoras().then(function (data) {
            return $scope.formuladoras = $scope.proccess(data);
        }).catch(function (error) {
            return console.log(error);
        });
    };

    $scope.getProveedores = function () {
        productos.proveedores().then(function (data) {
            return $scope.proveedores = $scope.proccess(data);
        }).catch(function (error) {
            return console.log(error);
        });
    };

    $scope.getTipoProductos = function () {
        productos.tipoProductos().then(function (data) {
            return $scope.tipoProductos = $scope.proccess(data);
        }).catch(function (error) {
            return console.log(error);
        });
    };

    $scope.frac = function () {
        productos.frac().then(function (data) {
            return $scope.fracs = $scope.proccess(data);
        }).catch(function (error) {
            return console.log(error);
        });
    };

    $scope.getAcciones = function () {
        productos.acciones().then(function (data) {
            return $scope.acciones = $scope.proccess(data);
        }).catch(function (error) {
            return console.log(error);
        });
    };

    $scope.getPrecios = function () {
        productos.precios($scope.producto.id_producto).then(function (data) {
            var response = $scope.proccess(data);
            if (response.hasOwnProperty("data")) {
                $scope.precios = response.data;
            }
        }).catch(function (error) {
            return console.log(error);
        });
    };

    $scope.show = function () {
        productos.show($scope.producto.id_producto).then(function (data) {
            var response = $scope.proccess(data);
            if (response.hasOwnProperty("success") && response.success == 200) {
                $scope.producto = response.data;
                $scope.getPrecios();
            }
        }).catch(function (error) {
            return console.log(error);
        });
    };

    $scope.init = function () {
        $scope.getFormuladoras();
        $scope.getProveedores();
        $scope.getTipoProductos();
        $scope.frac();
        $scope.getAcciones();
        $scope.show();
        if ($('.date-picker').length > 0) {
            $('.date-picker').datepicker({
                rtl: App.isRTL(),
                autoclose: true
            });
        }

        $scope.configuracion.state = "producto";
    };

    // 26/04/2017 - TAG: PRECIO
    $scope.add = function () {
        var precio = parseFloat($scope.precio.precio);
        var fecha = $scope.precio.fecha;
        if (isNaN(precio)) {
            alert("Favor de ingresar un precio valido");
        } else if (precio <= 0) {
            alert("El precio debe ser mayor a 0");
        } else {
            var data = {
                precio: precio,
                fecha: fecha,
                id_producto: $scope.producto.id_producto
            };
            $scope.precio.precio = "";
            $scope.precio.fecha = "";
            productos.addPrecios(data).then(function (data) {
                var response = $scope.proccess(data);
                if (!response.hasOwnProperty("error")) {
                    $scope.precios = response.data;
                }
            }).catch(function (error) {
                return console.log(error);
            });
        }
    };

    $scope.cancel = function () {
        var tab = "listado";
        $scope.producto.id_producto = 0;
        $('.nav-tabs a[href="#' + tab + '"]').tab('show');
        $scope.clear();
        $scope.index();
    };

    $scope.save = function () {
        if (!angular.isObject($scope.producto)) {
            return false;
        } else if ($scope.producto.nombreComercial.length <= 0 || $scope.producto.nombreComercial == "") {
            alert("Favor de ingresar Nombre de Producto");
        } else if ($scope.producto.proveedor.length <= 0 || $scope.producto.proveedor == "") {
            alert("Favor de ingresar Nombre del Proveedor");
        } else if ($scope.producto.tipo_producto.length <= 0 || $scope.producto.tipo_producto == "") {
            alert("Favor de ingresar Tipo de Producto");
        } else if ($scope.producto.ingrediente_activo.length <= 0 || $scope.producto.ingrediente_activo == "") {
            alert("Favor de ingresar Ingrediente Activo");
        } else if ($scope.producto.frac.length <= 0 || $scope.producto.frac == "") {
            alert("Favor de ingresar FRAC");
        } else if ($scope.producto.action.length <= 0 || $scope.producto.action == "") {
            alert("Favor de ingresar Acción");
        } else {
            if ($scope.producto.id_producto <= 0) {
                productos.create($scope.producto).then(function (data) {
                    return $scope.success(data);
                }).catch(function (error) {
                    return console.log(error);
                });
            } else {
                productos.update($scope.producto).then(function (data) {
                    return $scope.success(data);
                }).catch(function (error) {
                    return console.log(error);
                });
            }
        }
    };

    $scope.success = function (r) {
        var response = $scope.proccess(r);
        $scope.producto.id_producto = response.data;
        $scope.producto.codigo = response.data;
        alert("Registro registrado/modificado con el ID " + $scope.producto.id_producto, "Productos", "success");
    };

    $scope.getProduct = function (id_producto) {
        var tab = "createproductos";
        $scope.clear();
        $scope.producto.id_producto = id_producto || 0;
        $('.nav-tabs a[href="#' + tab + '"]').tab('show');
        $scope.init();
    };

    // 25/04/2017 - TAG: SECCION FOR TABLE 
    $scope.table = [];
    $scope.index = function () {
        productos.listado().then(function (data) {
            return $scope.table = $scope.proccess(data);
        }).catch(function (error) {
            return console.log(error);
        });
        $scope.configuracion.state = "listado";
    };

    //parametros de busqueda
    $scope.search = {
        nombre: "",
        orderBy: "codigo",
        reverse: false,
        limit: 10,
        actual_page: 1 // las paginas comienzan apartir del 1
    };

    //ordenamiento por columnas
    $scope.changeSort = function (column) {
        if ($scope.search.orderBy != column) {
            var previous = $("th.selected")[0];
            $(previous).removeClass("selected");
            $(previous).removeClass("sorting_asc");
            $(previous).removeClass("sorting_desc");
        }
        $scope.search.reverse = $scope.search.orderBy != column ? false : !$scope.search.reverse;
        $scope.search.orderBy = column;
        var actual_select = $("#" + column + "_column");
        if (!actual_select.hasClass("selected")) {
            actual_select.addClass("selected");
        }
        if ($scope.search.reverse) {
            actual_select.addClass("sorting_desc");
            actual_select.removeClass("sorting_asc");
        } else {
            actual_select.addClass("sorting_asc");
            actual_select.removeClass("sorting_desc");
        }
    };

    //ir a la siguiente pagina
    $scope.next = function (dataSource) {
        if ($scope.search.actual_page < parseInt(dataSource.length / parseInt($scope.search.limit)) + (dataSource.length % parseInt($scope.search.limit) == 0 ? 0 : 1)) $scope.search.actual_page++;
    };

    //ir a la pagina anterior
    $scope.prev = function (dataSource) {
        if ($scope.search.actual_page > 1) $scope.search.actual_page--;
    };
}]);
