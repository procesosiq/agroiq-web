function graficas(){
    var self = this;
    self = {
        options : {},
        id : "",
        theme : "infographic",
        required : function(){
            if(!this.id) throw Error("id es Requida");
            if(!this.options) throw Error("url es Requido");
        }
    }

    var printGrafica = function(){    
        var grafica = new echartsPlv();  
        grafica.init(self.id , self.options);
    }

    graficas.prototype.create = function(id , options ){
        self.options = options;
        self.id = id;
        self.required();
        return printGrafica();
    }
}

function createGraficas(options , id){
    var grafica = new graficas();
    grafica.create(id , options);
}

function getMax(datasource) {
    var val = 0
    $.each(datasource, function(index, value){
        if(value != undefined && value != null){
            if(parseFloat(value) && parseFloat(value) > val){
                val = parseFloat(value)
            }
        }
    })
    return val
};

function getMin(datasource) {
  var val = 1000
    $.each(datasource, function(index, value){
        if(value != undefined && value != null){
            if(parseFloat(value) && parseFloat(value) < val){
                val = parseFloat(value)
            }
        }
    })
    return val
};

function calidad(id , options , margen){
    var margen = margen;
    var options_historico = options.historico;
    var options_historico_legends = options.historico_legends;
    var category = [];
    var legend = []; 
    var series = [];
    var legend = [];
    var legend_data = [];
    var series = [];
    legend = Object.keys(options_historico_legends);
    for(var i in options_historico){
        series.push(options_historico[i]);
        legend_data.push(options_historico[i].name);
    }

    var option = {   
            title : {
                text: '',
                x: 'center',
                align: 'right'
            },
            grid: {
                bottom: 80
            },
            toolbox: {
                feature: {
                    dataZoom: {
                        yAxisIndex: 'none'
                    },
                    magicType: {
                        type: ['line','bar']
                    },
                    restore: {},
                    saveAsImage: {}
                }
            },
            tooltip : {
                trigger: 'axis',
                axisPointer: {
                    animation: false
                }
            },
            legend: {
                data: legend_data,
                x: 'left',
                orient : 'vertical'
            },
            dataZoom: [
                {
                    show: true,
                    realtime: true,
                    // start: 65,
                    // end: 85
                },
                {
                    type: 'inside',
                    realtime: true,
                    // start: 65,
                    // end: 85
                }
            ],
            xAxis : [
                {
                    type : 'category',
                    boundaryGap : true,
                    axisLine: {onZero: true},
                    data : legend
                }
            ],
            yAxis: [
                {
                    type: 'value',
                    min : margen.min,
                    max : margen.max
                }
            ],
            series: series
        };
    

    createGraficas(option ,id);
}

function clusterCahas(id , options , margen){
    var margen = margen;
    var options_historico = options.historico;
    var options_historico_legends = options.historico_legends;
    var category = [];
    var legend = []; 
    var series = [];
    var legend = [];
    var legend_data = [];
    var series = [];
    legend = Object.keys(options_historico_legends);
    for(var i in options_historico){
        series.push(options_historico[i]);
        legend_data.push(options_historico[i].name);
    }

    var option = {   
            title : {
                text: '',
                x: 'center',
                align: 'right'
            },
            grid: {
                y : 90,
                bottom: 80
            },
            toolbox: {
                feature: {
                    dataZoom: {
                        yAxisIndex: 'none'
                    },
                    magicType: {
                        type: ['line','bar']
                    },
                    restore: {},
                    saveAsImage: {}
                }
            },
            tooltip : {
                trigger: 'axis',
                axisPointer: {
                    animation: false
                },
            },
            legend: {
                data: legend_data,
                x: 'left',
                orient : 'vertical',
                padding : 10
            },
            dataZoom: [
                {
                    show: true,
                    realtime: true,
                    // start: 65,
                    // end: 85
                },
                {
                    type: 'inside',
                    realtime: true,
                    // start: 65,
                    // end: 85
                }
            ],
            xAxis : [
                {
                    type : 'category',
                    boundaryGap : true,
                    axisLine: {onZero: true},
                    data : legend
                }
            ],
            yAxis: [
                {
                    type: 'value',
                    min : margen.min,
                    max : margen.max
                }
            ],
            series: series
        };
    

    createGraficas(option ,id);
}

app.directive('tooltip', function(){
    return {
        restrict: 'A',
        link: function(scope, element, attrs){
            $(element).hover(function(){
                // on mouseenter
                $(element).tooltip('show');
            }, function(){
                // on mouseleave
                $(element).tooltip('hide');
            });
        }
    };
});

app.filter('sumOfValue', function () {
    return function (data, key) {        
        if (angular.isUndefined(data) || angular.isUndefined(key))
            return 0;        
        var sum = 0;
        angular.forEach(data,function(value){
            if(value[key] != "" && value[key] != undefined && parseFloat(value[key])){
                sum = sum + parseFloat(value[key], 10);
            }
        });
        return sum;
    }
})

app.filter('orderObjectBy', function() {
  return function(items, field, reverse) {
    var filtered = [];
    angular.forEach(items, function(item) {
        //// console.log(item)
      filtered.push(item);
    });
    filtered.sort(function (a, b) {
        //alert(a[field]);
      return (a[field] > b[field] ? 1 : -1);
    });
    if(reverse) filtered.reverse();
    return filtered;
  };
});

app.controller('informe_calidad', ['$scope','$http','client', function($scope,$http,client){
	$scope.id_company = 0;
    $scope.tags = {
        calidad_maxima : {
            value : 0,
            label : ""
        },
        calidad_minima : {
            value : 0,
            label : ""
        },
        cluster : {
            value : 0,
            label : ""
        },
        desviacion_estandar : {
            value : 0,
            label : ""
        },
        peso : {
            value : 0,
            label : ""
        },
        calidad_dedos : {
            value : 0,
            label : ""
        },
        calidad_cluster : {
            value : 0,
            label : ""
        },
        dedos_promedio : {
            value : 0,
            label : ""
        }
    };

    // 03/03/2017 - TAG: Selects
    $scope.fincas = [];
    $scope.fincaSelected = "";
    $scope.exportadores = [];
    $scope.exportadorSelected = "";
    $scope.clientes = [];
    $scope.clienteSelected = "";
    $scope.marcas = [];
    $scope.marcaSelected = "";
    $scope.contenedores = [];
    $scope.contenedorSelected = "";
    // 03/03/2017 - TAG: Selects
    
    
    $scope.umbrales = {};
    $scope.principal = {};
    $scope.defectos = [];
    $scope.tittleDefectos = [];
    $scope.clusters = [];
    $scope.dedos = [];
    $scope.categorias = [];
    $scope.totales = [];
    $scope.muestras = [];

    $scope.StartEndDateDirectives = {
        startDate : moment().startOf('year'),
        endDate : moment().endOf("year")
    }
    $scope.calidad = {
        params : {
            idFinca : 0,
            idExportador : 0,
            idCliente : 0,
            idMarca : 0,
            contenedor : "",
            fecha_inicial : moment().startOf('year').format('YYYY-MM-DD'),
            fecha_final : moment().endOf("year").format('YYYY-MM-DD'),
            cliente: "",
            marca: "",
            finca : ""
        },
        step : 0,
        templatePath : [],
        nocache : function(){
            this.templatePath.push('/views/sumifru/templetes/calidad/principal.html?' +Math.random());
        }
    }

    // 22/02/2017 - TAG: Filtros
    $scope.paramsGrafica = function(){    
        var response = {
            graficas : {},
            margen : {
                min : 0 ,
                max : 10,
                umbral : 4,
            }
        }
        return response;
    }

    $scope.request = {
    	tags : function(){
    		var data = $scope.calidad.params;
            client.post('phrapi/sumifru/calidad/home' , $scope.printTags , data , "contentTags");
    	},
    	principal : function(){
    		var data = $scope.calidad.params;
            client.post('phrapi/sumifru/calidad/main' , $scope.printPrincipal , data , "contentMain");	
    	},
        clusterCajas : function(){
            var data = $scope.calidad.params;
            client.post('phrapi/sumifru/calidad/cluster/caja' , $scope.printClusterCajas , data , "contentClusterCaja");	
        },
        tableCluster : function(){
            var data = $scope.calidad.params;
            client.post('phrapi/sumifru/calidad/table/cluster' , $scope.printTableCluster, data , "clusterContent");	
        },
        tableDedos : function(){
            var data = $scope.calidad.params;
            client.post('phrapi/sumifru/calidad/table/dedos' , $scope.printTableDedos , data , "dedosConten");	
        },
        defectos : function(){
            var data = $scope.calidad.params;
            client.post('phrapi/sumifru/calidad/defectos' , $scope.printDefectos , data , "contentDefectos");	
        },
        defectosDetailsTable : function(){
            var data = $scope.calidad.params;
            client.post('phrapi/sumifru/calidad/table/defectos' , $scope.printTableSubFinca , data , "contentTableSubFinca");	
        },
        last : function(){
            //ULTIMO DIA DE DATOS
            client.post('phrapi/sumifru/calidad/last', function(r, b){
                b()
                if(r){
                    $scope.calidad.params.fecha_inicial = r.fecha
                    $scope.calidad.params.fecha_final = r.fecha
                    $("#date-picker").html(`${r.fecha} - ${r.fecha}`)
                    $scope.request.all()
                }
            })
        },
        fotos : function(){
            $http.post('phrapi/sumifru/calidad/fotos', $scope.calidad.params || {})
            .then((r, b) => {
                $scope.fincas_fotos = r.data.data
            })
        },
        pesoCluster : function(){
            $http.post('phrapi/sumifru/calidad/pesoCluster', $scope.calidad.params || {})
            .then((r, b) => {
                new echartsPlv().init('peso_prom_cluster', r.data.chart)
                renderTablePesoCluster(r.data.table, r.data.semanas)
            })
        },
        empaque : function(){
            $http.post('phrapi/sumifru/calidad/empaque', $scope.calidad.params || {})
            .then((r, b) => {
                new echartsPlv().init('empaque', r.data.chart)
            })
        },
    	all : function(){
    		this.tags();
    		this.principal();
            this.clusterCajas();
            this.tableCluster();
            this.tableDedos();
            this.defectos();
            this.fotos();
            this.pesoCluster();
            this.empaque();
    	}

    }

    renderTablePesoCluster = (data, semanas) => {
        var props = {
            header : [{
                   key : 'variable',
                   name : 'VAR',
                   titleClass : 'text-center',
                   locked : true,
                   expandable : true,
                   resizable : true,
                },{
                   key : 'prom_total',
                   name : 'PROM ANUAL',
                   locked : true,
                   sortable : true,
                   titleClass : 'text-center',
                   alignContent : 'right',
                   filterable : true,
                   filterRenderer: 'NumericFilter',
                   formatter : 'Number',
                   resizable : true,
                }
            ],
            data : data,
            buttons : [
                {
                    title : 'Excel',
                    action : () => {
                        $scope.table1.exportToExcel()
                    },
                    className : ''
                }
            ]
        }
        semanas.map((sem) => {
            props.header.push({
                key : `sem_${sem}`,
                name : `${sem}`,
                sortable : true,
                alignContent : 'right',
                titleClass : 'text-center',
                filterable : true,
                filterRenderer: 'NumericFilter',
                resizable : true,
            })
        })
        $("#table-peso_prom_cluster").html("")
        $scope.table1 = ReactDOM.render(React.createElement(ReactDataGrid, props), document.getElementById('table-peso_prom_cluster'))
    }

    $scope.printTags = function(r,b){
    	b("contentTags");
    	if(r){
    		if(r.hasOwnProperty("tags")){
	            $scope.tags.calidad_maxima.value = Number(r.tags.calidad_maxima).toFixed(2);
	            $scope.tags.calidad_minima.value = Number(r.tags.calidad_minima).toFixed(2);
                $scope.tags.cluster.value = new Number(r.tags.cluster).toFixed(2);
	            $scope.tags.desviacion_estandar.value = Number(r.tags.desviacion_estandar).toFixed(2);
	            $scope.tags.peso.value = Number(r.tags.peso).toFixed(2);
				$scope.tags.calidad_dedos.value = Number(r.tags.calidad_dedos).toFixed(2);
				$scope.tags.calidad_cluster.value = Number(r.tags.calidad_cluster).toFixed(2);
				$scope.tags.dedos_promedio.value = new Number(r.tags.dedos_promedio).toFixed(2);
                // 22/02/2017 - TAG: FILTROS
                $scope.fincas = r.filters.fincas || [];
                $scope.exportadores = r.filters.exportadores || [];
                $scope.clientes = r.filters.clientes || [];
                $scope.marcas = r.filters.marcas || [];
                $scope.contenedores = r.filters.contenedores || [];
                // 22/02/2017 - TAG: FILTROS
                setTimeout(function(){
                    $(".counter_tags").counterUp({
                        delay: 10,
                        time: 1000
                    });
                } , 1000);
    		}
    	}
    }

    // 22/02/2017 - TAG: Grafica Calidad Finca
    $scope.printPrincipal = function(r , b){
        b("contentMain")
        var data = $scope.paramsGrafica();
    	if(r){
            let filter = $("#filterPrincipal").val()

            $scope.principal = r;
            data.graficas.historico = r[filter].data || [];
            data.graficas.historico_avg = r[filter].avg || [];
            data.graficas.historico_legends = r[filter].legend || [];

            let keys = Object.keys(r[filter].data)
            data.margen.min = 100
            data.margen.max = 0
            $.each(keys, function(index, key){
                let min = getMin(r[filter].data[key].data)
                let max = getMax(r[filter].data[key].data)
                
                if(min < data.margen.min)
                    data.margen.min = min
                if(max > data.margen.max)
                    data.margen.max = max
            })
			calidad("principal" ,data.graficas , data.margen );
    	}
    }

    $scope.changePrincipal = function(){
        let filter = $("#filterPrincipal").val()
        var data = $scope.paramsGrafica();
        
        if($scope.principal[filter]){
            let keys = Object.keys($scope.principal[filter].data)
            data.margen.min = 100
            data.margen.max = 0
            $.each(keys, function(index, key){
                let min = getMin($scope.principal[filter].data[key].data)
                let max = getMax($scope.principal[filter].data[key].data)
                
                if(min < data.margen.min)
                    data.margen.min = min
                if(max > data.margen.max)
                    data.margen.max = max
            })

            data.graficas.historico = $scope.principal[filter].data || [];
            data.graficas.historico_avg = $scope.principal[filter].avg || [];
            data.graficas.historico_legends = $scope.principal[filter].legend || [];
        }else{
            data.graficas.historico = [];
            data.graficas.historico_avg = [];
            data.graficas.historico_legends = [];
        }
        $("#spanLabel").html($("#filterPrincipal :selected").text().toUpperCase() + " FINCAS")
        calidad("principal" ,data.graficas , data.margen );
    }

   // 22/02/2017 - TAG: Grafica Cluster por Caja
    $scope.printClusterCajas = function(r , b){
        b("contentClusterCaja")
        var data = $scope.paramsGrafica();
    	if(r){
            data.graficas.historico = r.data || [];
            data.graficas.historico_avg = r.avg || [];
            data.graficas.historico_legends = r.legend || [];
            data.margen.min = Math.min(r.data);
            data.margen.max = Math.max(r.data);
			clusterCahas("clusterCajas" ,data.graficas , data.margen );
    	}
    }
    // 22/02/2017 - TAG: Tabla de Clusters
    $scope.printTableCluster = function(r , b){
        b("clusterContent")
    	if(r){
           $scope.clusters = r.data || [];
    	} 
    }
    // 22/02/2017 - TAG: Tabla de Dedos
    $scope.printTableDedos = function(r , b){
        b("dedosConten")
    	if(r){
           $scope.dedos = r.data || [];
    	}
    }
    // 22/02/2017 - TAG: Grafica de Defectos
    $scope.printDefectos =  function(r , b){
        b("contentDefectos")
        if(r){
            $scope.defectos = r.data || {};
            $scope.categoriasFilters = r.categoriasFilters || {};
            $scope.categorias = angular.copy(r.categorias) || {};
            createGraficas($scope.defectos, "CategoriasDefectos")
            var position = Object.keys($scope.categorias)[0];
            var category = $scope.categorias[position];
            createGraficas(category , "defectos")
    	}
    }
    // 24/02/2017 - TAG: Table Sub Finca
    $scope.printTableSubFinca = function(r , b){
        b("contentTableSubFinca")
        console.log("tabla")
    	if(r){
            $scope.tittleDefectos = r.defectos || {};
            $scope.totales = r.totales || {};
            $scope.muestras = r.muestras || {};
            $('.tooltips').tooltip();
    	}
    }

    // 07/03/2017 - TAG: Change Category
    cambiosCategoria = function(){
        var position = $("#categoria").val();
        var category = $scope.categorias[position];
        createGraficas(category, "defectos")
    }

    // 03/03/2017 - TAG: Select Fincas
    cambiosFincas = function(){
        $scope.calidad.params.palanca = "";
        $scope.calidad.params.idExportador = "";
        $scope.calidad.params.idCliente = "";
        $scope.calidad.params.idMarca = "";
        $scope.request.defectosDetailsTable();
        $scope.loadExternal();
    }    

    // 03/03/2017 - TAG: Select Expotador
    cambiosExportador = function(){
        $scope.calidad.params.idCliente = "";
        $scope.calidad.params.idMarca = "";
        $scope.request.all();
    }
    
    // 03/03/2017 - TAG: Select Cliente
    cambiosCliente = function () {
        $scope.calidad.params.idMarca = "";
        $scope.request.all();
    }
   
   // 03/03/2017 - TAG: Select Marca
    cambiosMarca = function () {
        $scope.request.all();
        //$scope.request.defectosDetailsTable($("#idMarca").val());
    }

    $scope.changeContenedor = function(){
        $scope.loadExternal();
    }

    $scope.openDetalle = function(data){
        data.expand = !data.expand;
    }

	$scope.changeRangeDate = function(data){
		if(data){
			$scope.calidad.params.fecha_inicial = data.hasOwnProperty("first_date") ? data.first_date : $scope.wizardStep.params.fecha_inicial;
			$scope.calidad.params.fecha_final = data.hasOwnProperty("second_date") ? data.second_date : $scope.wizardStep.params.fecha_final;
			$scope.loadExternal();
		}
	}
    $scope.loadExternal = function(){
		$scope.request.all();
    }

    $scope.toNumber = function(value){
        return parseFloat(value);
    }

    $scope.clickImage = (path, calidad, type) => {
        var modal = document.getElementById('myModal');
        var modalImg = document.getElementById("img01");
        var captionText = document.getElementById("caption");
        var span = document.getElementsByClassName("close")[0];
        span.onclick = function() { 
            modal.style.display = "none";
        }
        modal.onclick = function(){
            modal.style.display = "none";
        }

        modal.style.display = "block";
        modalImg.src = path;
        captionText.innerHTML = `${type} - ${calidad}%`;
    }

    setTimeout(() => {
        $scope.request.last()
    }, 1000)
}]);