app.filter('sumOfValue', function () {
    return function (data, key) {        
        if (angular.isUndefined(data) || angular.isUndefined(key))
            return 0;        
        var sum = 0;
        angular.forEach(data,function(value){
            if(value[key] != "" && value[key] != undefined && parseFloat(value[key])){
                sum = sum + parseFloat(value[key], 10);
            }
        });
        return sum;
    }
})

app.filter('sumOfValueDouble', function () {
    return function (data, key, key2) {        
        if (angular.isUndefined(data) || angular.isUndefined(key) || angular.isUndefined(key2))
            return 0;        
        var sum = 0;
        angular.forEach(data,function(value){
            if(value[key][key2] != "" && value[key][key2] != undefined && parseFloat(value[key][key2])){
                sum = sum + parseFloat(value[key][key2], 10);
            }
        });
        return sum;
    }
})

app.filter('getNotRepeat', function () {
    return function (data, key) {        
        if (angular.isUndefined(data) || angular.isUndefined(key))
            return [];
        
        let _arr = []
        data.forEach((value) => {
            if(value[key]){
                if(!_arr.includes(value[key])) _arr.push(value[key])
            }
        })
        return _arr;
    }
})

app.filter('avgOfValue', function () {
    return function (data, key) {        
        if (angular.isUndefined(data) || angular.isUndefined(key))
            return 0;        
        var sum = 0;
        var count = 0;
        angular.forEach(data,function(value){
            if(value[key] != "" && value[key] != undefined && value[key] > 0){
                sum = sum + parseFloat(value[key], 10);
                count++;
            }
        });
        sum = sum / count;
        if(isNaN(sum))
            return 0;
        return sum;
    }
})

app.filter('keys', function () {
    return function (data) {        
        return Object.keys(data).length
    }
})

app.service('request', ['$http', ($http) => {
    let service = {}

    service.index = (callback, params) => {
        load.block('defectos')
        load.block('empaque')
        load.block('cluster')
        load.block('fotos')
        $http.post('phrapi/sumifru/calidad2/index', params)
        .then((r) => {
            load.unblock('defectos')
            load.unblock('empaque')
            load.unblock('cluster')
            load.unblock('fotos')
            callback(r.data)
        })
    }

    service.variables = (callback, params) => {
        load.block()
        $http.post('phrapi/sumifru/calidad2/variables', params)
        .then((r) => {
            load.unblock()
            callback(r.data)
        })
    }

    service.last = (callback, params) => {
        load.block()
        $http.post('phrapi/sumifru/calidad2/last', params)
        .then((r) => {
            load.unblock()
            callback(r.data)
        })
    }

    return service
}])

function initPastel(id, series){
    var legends = []
    series.map(key => {
        if(legends.indexOf(series[key]) != -1) legends.push(series[key]);
    })
    setTimeout(() => {
        let data = {
            data : series,
            nameseries : "Pastel",
            legend : legends,
            titulo : "",
            id : id
        }
        let parent = $("#"+id).parent()
        parent.empty()
        parent.append(`<div id="${id}" class="chart"></div>`)
        ReactDOM.render(React.createElement(Pastel, data), document.getElementById(id));
    }, 250)
}

app.controller('controller', ['$scope','request', function($scope, $request){

    // UMBRAL

    $scope.loadCalidadUmbral = () => {
        let c = localStorage.getItem('banano_calidad_cluster_umbral')
        if(c){
            $scope.umbral_cluster = JSON.parse(c)
        }else{
            $scope.umbral_cluster = {
                max : 92,
                min : 90
            }
        }

        let d = localStorage.getItem('banano_calidad_dedos_umbral')
        if(d){
            $scope.umbral_dedos = JSON.parse(d)
        }else{
            $scope.umbral_dedos = {
                max : 92,
                min : 90
            }
        }

        let e = localStorage.getItem('banano_calidad_empaque_umbral')
        if(e){
            $scope.umbral_empaque = JSON.parse(e)
        }else{
            $scope.umbral_empaque = {
                max : 92,
                min : 90
            }
        }
    }
    $scope.saveCalidadUmbral = () => {
        toastr.success('Umbral guardado')
        localStorage.setItem('banano_calidad_cluster_umbral', JSON.stringify($scope.umbral_cluster))
        localStorage.setItem('banano_calidad_dedos_umbral', JSON.stringify($scope.umbral_dedos))
        localStorage.setItem('banano_calidad_empaque_umbral', JSON.stringify($scope.umbral_empaque))
        $scope.reRenderMarkers()
        $scope.closeMenu()
    }

    $scope.openMenu = () => {
        $(".toggler-close, .theme-options").show()
    }
    $scope.closeMenu = () => {
        $(".toggler-close, .theme-options").hide()
    }

    $scope.umbral_cluster = {}
    const getUmbralClusterMin = () => {
        return $scope.umbral_cluster.min
    }

    const getUmbralClusterHigh = () => {
        return $scope.umbral_cluster.max
    }
    $scope.getClusterUmbral = (value) => {
        if(value <= getUmbralClusterMin()){
            return 'font-red-thunderbird'
        }
        else if(value >= getUmbralClusterHigh()){
            return 'font-green-haze'
        }else {
            return 'font-yellow-gold'
        }
    }

    $scope.umbral_dedos = {}
    const getUmbralDedosMin = () => {
        return $scope.umbral_dedos.min
    }

    const getUmbralDedosHigh = () => {
        return $scope.umbral_dedos.max
    }
    $scope.getDedosUmbral = (value) => {
        if(value <= getUmbralDedosMin()){
            return 'font-red-thunderbird'
        }
        else if(value >= getUmbralDedosHigh()){
            return 'font-green-haze'
        }else {
            return 'font-yellow-gold'
        }
    }

    $scope.umbral_empaque = {}
    const getUmbralEmpaqueMin = () => {
        return $scope.umbral_empaque.min
    }

    const getUmbralEmpaqueHigh = () => {
        return $scope.umbral_empaque.max
    }
    $scope.getEmpaqueUmbral = (value) => {
        if(value <= getUmbralEmpaqueMin()){
            return 'font-red-thunderbird'
        }
        else if(value >= getUmbralEmpaqueHigh()){
            return 'font-green-haze'
        }else {
            return 'font-yellow-gold'
        }
    }

    // 

    // BEGIN CONFIG
    $scope.config = {
        calidad_empaque : true,
        peso_prom_cluster : true
    }

    $scope.tag_md = $scope.config.calidad_empaque ? 4 : 3
    // END CONFIG

    $scope.datesEnabled = []
    $scope.total_empaque = { cantidad : 0}
    $scope.total_defectos = { cantidad_cluster_caja : 0 }
    $scope.fincas = []
    $scope.tags = {}
    $scope.filters = {
        fecha_inicial : moment().format('YYYY-MM-DD'),
        fecha_final : moment().format('YYYY-MM-DD'),
        id_finca : 0,
        marca : '',
        fotos : {}
    }

    $scope.changeRangeDate = function(data){
		if(data){
			$scope.filters.fecha_inicial = data.hasOwnProperty("first_date") ? data.first_date : $scope.filters.fecha_inicial;
			$scope.filters.fecha_final = data.hasOwnProperty("second_date") ? data.second_date : $scope.filters.fecha_final;
            $scope.variables()
		}
	}

    $scope.init = () => {
        $request.index((r) => {
            $scope.tags = r.tags
            $scope.defectos = r.defectos
            $scope.empaque = r.empaque
            $scope.cluster = r.cluster
            $scope.fotos = r.fotos

            initPastel('empaque-pie', r.empaque.data)
            initPastel('cluster-pie', r.cluster.data.map((c) => { return { label : `${c.tipo} Dedos`, value : c.cantidad } }))
        }, $scope.filters)
    }

    $scope.reRenderEmpaquePie = () => {
        setTimeout(() => {
            initPastel('empaque-pie', $scope.empaque.data)
        }, 100)
    }

    $scope.reRenderClusterPie = () => {
        setTimeout(() => {
            initPastel('cluster-pie', $scope.cluster.data.map((c) => { return { label : `${c.tipo} Dedos`, value : c.cantidad } }))
        }, 100)
    }

    $scope.variables = () => {
        $request.variables((r) => {
            $scope.marcas = r.marcas
            // seleccionar marca
            if(r.marcas){
                if($scope.filters.marca){
                    if(!r.marcas.includes($scope.filters.marca)){
                        $scope.filters.marca = r.marcas[0]

                        // no continuar y volver a pedir informacion de fincas
                        $scope.variables()
                        return;
                    }
                }
            }else{
                // todas
                $scope.filters.marca = ''
            }

            $scope.fincas = r.fincas
            // seleccionar una finca default
            if(r.fincas){
                if($scope.filters.id_finca){
                    let finca = r.fincas.filter((f) => f.id === $scope.filters.id_finca)
                    if(!finca){
                        $scope.filters.id_finca = r.fincas[0].id
                    }
                }
            }else{
                // todas
                $scope.filters.id_finca = 0
            }

            $scope.init()
        }, $scope.filters)
    }

    $scope.last = () => {
        $request.last((r) => {
            $scope.datesEnabled = r.days
            if(r.fecha){
                $scope.filters.fecha_inicial = r.fecha
                $scope.filters.fecha_final = r.fecha
                setTimeout(() => {
                    $("#date-picker").html(`${r.fecha} - ${r.fecha}`)
                }, 100)
            }
            $scope.filters.id_finca = r.id_finca
            $scope.variables()
        })
    }

    $scope.deleteFilter = (campo) => {
        delete $scope.filters.fotos[campo]
    }

    $scope.exportExcel = function(id_table, title){
        var data = new Blob([document.getElementById(id_table).outerHTML], {
            type : 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf8'
        })
        saveAs(data, title+'.xls')
    }

    $scope.last()
	$scope.loadCalidadUmbral()
}]);