import echarts from 'echarts'

/*----------  UTILIDADES SOBRE ARRAYS  ----------*/

app.filter('orderObjectBy', function() {
    return function(items, field, reverse) {
      var filtered = [];
      angular.forEach(items, function(item) {
          //// console.log(item)
          /*if(item.hasOwnProperty("lote")){
              if(!isNaN(parseFloat(item.lote))){
                  item.lote = parseFloat(item.lote);
              }else{
                  item.lote = item.lote;
              }
          }*/
          if(item.hasOwnProperty("total_peso_merma")){
              item.total_peso_merma = parseFloat(item.total_peso_merma);
          }
          if(item.hasOwnProperty("total_defectos")){
              if(!isNaN(parseFloat(item.total_defectos)))
                  item.total_defectos = parseFloat(item.total_defectos);
          }
          if(item.hasOwnProperty("merma")){
              if(!isNaN(parseFloat(item.merma)))
                  item.merma = parseFloat(item.merma);
          }
          if(item.hasOwnProperty("danhos_peso")){
              if(!isNaN(parseFloat(item.danhos_peso)))
                  item.danhos_peso = parseFloat(item.danhos_peso);
          }
          if(item.hasOwnProperty("filter")){
              if(!isNaN(parseFloat(item.filter)))
                  item.filter = parseFloat(item.filter);
          }
          if(item.hasOwnProperty("cantidad")){
              if(!isNaN(parseFloat(item.cantidad)))
                  item.cantidad = parseFloat(item.cantidad);
          }
  
        filtered.push(item);
      });
      filtered.sort(function (a, b) {
          if(parseFloat(a[field]) && parseFloat(b[field]))
            return (parseFloat(a[field]) > parseFloat(b[field]) ? 1 : -1);
          else
            return (a[field] > b[field] ? 1 : -1);
      });
      if(reverse) filtered.reverse();
      return filtered;
    };
});

app.filter('unique', function() {
   return function(collection, keyname) {
      var output = [], 
          keys = [];

      angular.forEach(collection, function(item) {
          var key = item[keyname];
          if(keys.indexOf(key) === -1) {
              keys.push(key);
              output.push(item);
          }
      });

      return output;
   };
});

app.filter('sumOfValue', function(){
    return function(data, field) {
        if (angular.isUndefined(data) || angular.isUndefined(field))
            return 0;
        
        var sum = 0;
        data.forEach((value, index) => {
            if(value[field]) if(parseFloat(value[field]))
                sum += parseFloat(value[field]);
        });
        return sum;
    }
});

app.filter('avgOfValue', function(){
    return function(data, field) {
        if (angular.isUndefined(data) || angular.isUndefined(field))
            return 0;
        
        var sum = 0;
        var count = 0;
        data.forEach((value, index) => {
            if(value[field]) if(parseFloat(value[field])){
                sum += parseFloat(value[field]);
                count++;
            }
        });
        return sum / count;
    }
});

/*----------  OBJETO PARA GRAFICAS ECHARTS  ----------*/
var globalEcharts;
var appEcharts = {
    require : require,
    options_historico : [],
    options_historico_legends : [],
    options_tendencia : [],
    options_tendencia_legends : [],
    options_dia : [],
    dia_title : "",
    historico_avg : 0,
    margen : {
        min : 0,
        min_tendencia : 0,
        max : 5,
        max_tendencia : 2,
        umbral : 2,
        umbral_tendencia : .5,
    },
    options_danos : [],
    options_danos_detalle : [],
    type : "ENFUNDE",
    init : function(callback){
        callback = callback || this.loadModules;
        callback(echarts)
    },
    // Este metodo existe por que aun no completo la function de parseo para que funcione directo
    danos : function(){
        var series = this.options_danos;
        var options = {
            title : {
                text: series.title.text,
                subtext: series.title.subtext,
                x:'center'
            },
            tooltip : {
                trigger: 'item',
                formatter: "{a} <br/>{b} : {c} ({d}%)"
            },
            legend: {
                show : true,
                // orient : 'vertical',
                x : 'center',
                y : 'bottom',
                data: series.legend.data
            },
            toolbox: {
                show : true,
                feature : {
                    mark : {show: false},
                    dataView : {show: false, readOnly: false},
                    magicType : {
                        show: true, 
                        type: ['pie', 'funnel'],
                        option: {
                            funnel: {
                                x: '25%',
                                width: '50%',
                                funnelAlign: 'left',
                                max: 1548
                            }
                        }
                    },
                    restore : {show: true},
                    saveAsImage : {show: true, name : series.toolbox.feature.saveAsImage.name}
                }
            },
            calculable : true,
            series : [
                {
                    name:series.series.name,
                    type:'pie',
                    radius : '40%',
                    center: ['50%', '40%'],
                    data:series.series.data
                }
            ]
        };
        return options;
    },
    // Este metodo existe por que aun no completo la function de parseo para que funcione directo
    // Adicionalmente es aqui donde se cambia entre tipo de detalle por medio de la flag 'type'
    danos_detalle : function(){
        var options = {};
        ////console.log(this.type);
        if(this.options_danos_detalle.hasOwnProperty(this.type)){
            var series = this.options_danos_detalle[this.type];
            options = {
                title : {
                    text: series.title.text,
                    subtext: series.title.subtext,
                    x:'center'
                },
                tooltip : {
                    trigger: 'item',
                    formatter: "{a} <br/>{b} : {c} ({d}%)"
                },
                legend: {
                    show : true,
                    // orient : 'vertical',
                    x : 'center',
                    y : 'bottom',
                    data: series.legend.data
                },
                toolbox: {
                    show : true,
                    feature : {
                        mark : {show: false},
                        dataView : {show: false, readOnly: false},
                        magicType : {
                            show: true, 
                            type: ['pie', 'funnel'],
                            option: {
                                funnel: {
                                    x: '25%',
                                    width: '50%',
                                    funnelAlign: 'left',
                                    max: 1548
                                }
                            }
                        },
                        restore : {show: true},
                        saveAsImage : {show: true, name : series.toolbox.feature.saveAsImage.name}
                    }
                },
                calculable : true,
                series : [
                    {
                        name:series.series.name,
                        type:'pie',
                        radius : '40%',
                        center: ['50%', '40%'],
                        data:series.series.data
                    }
                ]
            };
        }
        return options;
    },
    historico : function(){
        //// console.log(this.options_historico);
        var category = [];
        var legend = []; 
        var series = [];
        var legend = [];
        var legend_data = [];
        var series = [];
        legend = Object.keys(this.options_historico_legends);

        for(var i in this.options_historico){
            let serie = this.options_historico[i];
            serie.data.map((s, j) => {
                if($.isNumeric(s)) serie.data[j] = Number(s)
                else serie.data[j] = undefined
            })

            series.push(serie);
            legend_data.push(this.options_historico[i].name);
        }

        var option = {
            grid : {
                y2 : 100
            },
            tooltip : {
                trigger: 'axis'
            },
            legend: {
                show : true,
                x : 'center',
                y : 'bottom',
                data: legend_data
            },
            toolbox: {
                show : true,
                feature : {        
                    restore : {show: true},
                    saveAsImage : {show: true}
                }
            },
            xAxis : [
                {
                    type : 'category',
                    boundaryGap : false,
                    data : legend
                },
            
            ],
            yAxis : [
                {
                    type : 'value',
                    axisLabel : {
                        formatter: '{value}'
                    },
                    min : this.margen.min,
                    max : this.margen.max
                }
            ],
            series : series
        };
        return option;
    },
    dia : function(){
        var legend_tmp = [];
        var legend = [];
        var series = []
        var options_dia = this.options_dia;

        legend_tmp = Object.keys(options_dia.label);

        legend_tmp.sort(function(a,b){
            return new Date(a) - new Date(b);
        });

        legend = legend_tmp.map(function(value,index,arr){
            var date = new Date(value);
            var horas = date.getHours();
            var minutos = date.getMinutes();
            if(parseInt(minutos) == 0){
                minutos = "00";
            }
            else if(parseInt(minutos) < 10){
                minutos = "0" + minutos;
            }
            series.push(options_dia.series[value]);
            return horas + ":" + minutos ;
        });
        var option = {
            grid : {
                y2 : 100
            },
            title : {
                text: this.dia_title,
                x:'center'
            },
            tooltip: {
                show: true
            },
            xAxis: [
                {
                    type: 'category',
                    data : legend
                }
            ],
            yAxis: [
                {
                    type: 'value',
                    min : this.margen.min,
                    max : this.margen.max
                }
            ],
            series: [
                {
                    name : "Merma",
                    type : "bar",
                    itemStyle: {
                        normal: {
                        color: '#007537',
                        barBorderColor: '#007537',
                        barBorderWidth: 6,
                        barBorderRadius:0,
                        label : {
                                show: true, position: 'insideTop'
                            }
                        }
                    },
                    markLine : {
                        data : [
                            [
                                {name: 'Umbral', value: 10, xAxis: -1, yAxis: 10},
                                {xAxis: 52, yAxis: 10}
                            ],
                        ]
                    },
                    data : series
                }
            ]
        };
        return option;
    },
    tendencia : function(){
        var category = [];
        var legend = this.options_tendencia_legends;
        var series = [];
        var legend_data = [];

        for(var i in this.options_tendencia){
            series.push(this.options_tendencia[i]);
            legend_data.push(this.options_tendencia[i].name);
        }

        var option = {   
            tooltip : {
                trigger: 'axis'
            },
            legend: {
                show : true,
                x : 'center',
                y : 'bottom',
                data: legend_data
            },
            toolbox: {
                    show : true,
                    feature : {
                        magicType : {show: true, type: ['line', 'bar']},
                        restore : {show: true},
                        saveAsImage : {show: true}
                    }
                },
                calculable : true,
                xAxis : [
                    {
                        type : 'category',
                        boundaryGap : false,
                        data : legend           
                    },
                
                ],
                yAxis : [
                    {
                        type : 'value',
                        axisLabel : {
                            formatter: '{value}'
                        },
                        min : this.margen.min_tendencia,
                        max : this.margen.max_tendencia
                    }
                ],
                series : series
            };
        return option;
    },
    loadModules : function(echarts){
        globalEcharts = echarts;
        var historico = [], options_historico = appEcharts.historico();
        var tendencia = [], options_tendencia = appEcharts.tendencia();
        var dia = [], options_dia = appEcharts.dia();
        var danos = [] , options_danos = appEcharts.danos();
        var danos_detalle = [] , options_danos_detalle = appEcharts.danos_detalle();
        
        // Creamos la grafica
        historico = echarts.init(document.getElementById('historico') , 'infographic');
        // Asignamos las series , titulos , etc
        historico.setOption(options_historico);

        // Creamos la grafica
        tendencia = echarts.init(document.getElementById('tendencia') , 'infographic');
        // Asignamos las series , titulos , etc
        tendencia.setOption(options_tendencia);

        // Creamos la grafica
        dia = echarts.init(document.getElementById('dia') , 'infographic');
        // Asignamos las series , titulos , etc
        dia.setOption(options_dia);

        // Creamos la grafica
        danos = echarts.init(document.getElementById('echarts_generales') , 'infographic');
        // Asignamos las series , titulos , etc
        danos.setOption(options_danos);

        // Creamos la grafica
        danos_detalle = echarts.init(document.getElementById('echarts_generales_detalles') , 'infographic');
        // Validamos que exista data para el detalle
        if(options_danos_detalle.hasOwnProperty("title")){
            // Asignamos las series , titulos , etc
            danos_detalle.setOption(options_danos_detalle);
        }

        // Asignamos el 'responsive'
        window.onresize = function(){
            historico.resize();
            tendencia.resize();
            dia.resize();
            danos_detalle.resize();
            danos.resize();
        }
    }
}
/*----------  OBJETO PARA GRAFICAS ECHARTS  ----------*/

function loadScript(options , margen){
    appEcharts.options_historico = options.historico;
    appEcharts.options_historico_legends = options.historico_legends;
    appEcharts.options_tendencia = options.tendencia;
    appEcharts.options_tendencia_legends = options.tendencia_legends;
    appEcharts.historico_avg = options.historico_avg;
    appEcharts.options_dia = options.dia;
    appEcharts.dia_title = options.dia_title;
    appEcharts.options_danos = options.danos;
    appEcharts.options_danos_detalle = options.danos_detalle;
    appEcharts.margen = margen;
    appEcharts.init();
}

app.controller('informe_calidad', ['$scope', '$http', 'client', function($scope, $http, client){
    
    $scope.leyendaGeneralTitle = 'Merma';
    $scope.orderByField = 'lote';
    $scope.reverseSort = false;
    $scope.orderByField2 = 'type';
    $scope.reverseSort2 = false;
    $scope.labelPeso = "";

    $scope.id_company = 7;
    $scope.tags = {
        merma : { value : 0, label : "" },
        tallo : { value : 0, label : "" },
        merma_cortada : { value : 0, label : "" },
        tallo : { value : 0, label : "" },
        merma_cortada : { value : 0, label : "" },
        cajas : { value : 0, label : "" },
        usd : { value : 0, label : "" },
        empaque : { value : 0, label : "" },
        cosecha : { value: 0, label : "" },
        practicas_agricolas : { value: 0, label : "" },
        fisiologicos : { value: 0, label : "" }
    };

    $scope.factor = 1;

    $scope.graficas = {
        historico : {},
        tendencia : {},
        historico_avg : 0,
        dia : {},
        dia_title : "",
        danos : {},
        danos_detalle : {},
        historico_legends : {},
        tendencia_legends : {},
        tallo : { chart : {} }
    }

    $scope.StartEndDateDirectives = {
        startDate : moment().startOf('month'),
        endDate :moment().endOf('month'),
    }

    $scope.datesEnabled = []
    $scope.umbrales = {};
    $scope.calidad = {
        params : {
            idFinca : 1,
            idFincaDia : "",
            idMerma : "MATERIA PRIMA",
            idLote : 0,
            idLabor : 0,
            fecha_inicial : moment().startOf('month').format('YYYY-MM-DD'),
            fecha_final : moment().endOf('month').format('YYYY-MM-DD'),
            cliente: "",
            marca: "",
            palanca : "",
            statusLbKg : true,
            year : `${moment().year()}`,
            yearTendencia : `${moment().year()}`,
            var : 'Cirugía'
        },
        step : 0,
        path : ['phrapi/sumifru/merma/index' , 'phrapi/merma/labores' , 'phrapi/merma/causas'],
        templatePath : [],
        nocache : function(){
            this.templatePath.push('/views/sumifru/templetes/merma/step1.html?' +Math.random());
        }
    }

    $scope.historico_tabla = [];
    $scope.semanas_historico = [];
    
    $scope.openDetalleHistorico = function(data){
        data.expanded = !data.expanded;
    }

    $scope.bgTagsFlags = function(value){
        var className = ""
        if(!isNaN(parseFloat(value))){
            value = parseFloat(value)
            let umbral = 5

            if(value == umbral){
                className = "bg-yellow-gold bg-font-yellow-gold"
            }else if(value < umbral ){
                className = "bg-green-haze bg-font-green-haze"
            }else if(value > umbral){
                className = "bg-red-thunderbird bg-font-red-thunderbird"
            }
        }
        return className;
    }
    $scope.fontUmbral = function(value){
        var className = ""
        if(!isNaN(parseFloat(value))){
            value = parseFloat(value)
            let umbral = 5

            if(value == umbral){
                className = "yellow-gold"
            }else if(value < umbral ){
                className = "green-haze"
            }else if(value > umbral){
                className = "red-thunderbird"
            }
        }
        return className;
    }
    $scope.umbralMostHigh = function(value){
        let values = [
            parseFloat($scope.tags.cosecha.value),
            parseFloat($scope.tags.empaque.value),
            parseFloat($scope.tags.practicas_agricolas.value),
            parseFloat($scope.tags.fisiologicos.value),
        ];
        values.sort(function(a, b){
            return a - b;
        });
        let highs = [values[values.length-1], values[values.length-2]];
        return (highs.indexOf(parseFloat(value)) != -1) ? 'red-thunderbird' : 'green-jungle';
    }

    const convertModeSelect = function(){
        var mode = $("#mode").val();
        $scope.historicoSemanal(mode);
    }

    // 24/06/2017 - TAG: HISTORICO SEMANAL

    $scope.fincas = [];
    $scope.mermas = {
        NETA : "Merma Neta",
        "MATERIA PRIMA" : "Merma Prima",
    };

    $scope.palancas = [];

    $scope.loadExternal = function(){
        if($scope.calidad.path[$scope.calidad.step] != ""){
            var data = $scope.calidad.params;
            client.post($scope.calidad.path[$scope.calidad.step] , $scope.startDetails , data);

            $scope.graficaVariables()
        }
    }

    $scope.changeYears = function(){
        $scope.loadExternal();
    }

    const cambiosMerma = function(){
        $scope.calidad.params.palanca = "";
        $scope.calidad.params.idFincaDia = "";
        $scope.loadExternal();
    }

    const cambiosPalanca = function(){
        $scope.loadExternal();
    }

    $scope.cambiosFincas = function(){
        $scope.calidad.params.palanca = "";
        $scope.calidad.params.idFincaDia = "";
        $scope.loadExternal();
    }

	$scope.cambiarDanosDetalle = function(){
        let keys = Object.keys($scope.graficas.danos_detalle)
        if($scope.dano_detalle === undefined || keys.indexOf($scope.dano_detalle) === -1){
            $scope.dano_detalle = angular.copy(keys[0])
            $("#detalles_danhos").val(keys[0])
        }
		appEcharts.type = $("#detalles_danhos").val()
        appEcharts.init();
	}

    $scope.fincasDia = function(){
        $scope.loadExternal();
    }

    $scope.changeYearTendencia = function(){
        $scope.loadExternal();   
    }

    $scope.changeRangeDate = function(data){
        if(data){
            $scope.calidad.params.fecha_inicial = data.hasOwnProperty("first_date") ? data.first_date : $scope.wizardStep.params.fecha_inicial;
            $scope.calidad.params.fecha_final = data.hasOwnProperty("second_date") ? data.second_date : $scope.wizardStep.params.fecha_final;
            $scope.loadExternal();
        }
    }

    $scope.tabla_danos_merma = [];
    $scope.tabla_lote_merma = [];
    $scope.tabla_danos_merma_danhos_merma = [];

    $scope.classColumns = "col-md-6 col-sm-6";

    $scope.disableColumns = function(column , e){
        if(column){
            if($("." + column).hasClass('column_hide')){
                $("." + column).removeClass( column + ' column_hide').addClass( column );
                $(e.target).parent().removeClass('active')
            }else{
                $("." + column).addClass( column + ' column_hide');
                $(e.target).parent().addClass('active')
            }
        }
    }

    /*
        MODIFICACION JAVI 
    */
    const has = Object.prototype.hasOwnProperty;

    const second_level = (params) => {
        //load.block("lote_table");
        return $http.post('phrapi/sumifru/merma/second' , params);
    }
    const third_level = (params) => {
        //load.block("lote_table");
        return $http.post('phrapi/sumifru/merma/third' , params);
    }

    $scope.openDetalle= function(data , position){
        if(has.call(data ,"detalle")){
            let { detalle } = data;
            if(detalle.length <= 0 ){
                data.expanded = !data.expanded;
                let params = {
                    fecha_inicial : $scope.calidad.params.fecha_inicial,
                    fecha_final:  $scope.calidad.params.fecha_final,
                    bloque : data.lote,
                    palanca : $scope.calidad.params.palanca,
                    idFinca : $scope.calidad.params.idFinca
                }

                second_level(params).then( (r) => {
                    data.detalle = r.data
                    setTimeout( ()=> { /*load.unblock("lote_table");*/ console.log(data); } , 1000);
                });
            }else{
                data.expanded = !data.expanded;
            }
        }
        if(has.call(data ,"details")){
            let { details } = data;
            if(details.length <= 0 ){
                data.expanded = !data.expanded;
                let params = {
                    fecha_inicial : $scope.calidad.params.fecha_inicial,
                    fecha_final:  $scope.calidad.params.fecha_final,
                    bloque : data.bloque,
                    type : data.type,
                    danhos_peso : data.danhos_peso,
                    racimos_lote : data.racimos_lote,
                    cantidad : data.cantidad,
                    palanca : $scope.calidad.params.palanca,
                    idFinca : $scope.calidad.params.idFinca,
                }

                third_level(params).then( (r) => {
                    data.details = r.data
                    setTimeout( ()=> { /*load.unblock("lote_table");*/ console.log(data); } , 1000);
                });
            }else{
                data.expanded = !data.expanded;
            }
        }

        if(position == 1){
            var generateId = ".detalle_"+data.bloque+"_"+data.type.replace(' ','_');
            if($(generateId).length > 0){
                if(data.expanded){
                    $(generateId).css('display', '');
                }else{
                    $(generateId).css('display', 'none');
                }
            }else{
                var row = $scope.generateRow(data);
                $("#2nivel_" +data.bloque +"_"+data.type.replace(' ','_')).after(row);
            }
        }
    }
    /*
        MODIFICACION JAVI 
    */

    $scope.generateRow = function(data){
        var row = "";
        var generateId = "detalle_"+data.bloque+"_"+data.type.replace(' ','_');
        if(data.hasOwnProperty("details")){
            var table = data.details;
            for(var i in table){
                row += "<tr class='"+generateId+"' ng-show='category.expanded' style='cursor: pointer; text-align: right;'>";
                row += "<td style='text-align: left !important;'>"+table[i].campo+"</td>";
                row += "<td>"+parseFloat(table[i].cantidad).toFixed(2)+"</td>";
                row += "<td>"+parseFloat(table[i].danhos_peso).toFixed(2)+"</td>";
                row += "<td></td>";
                row +="</tr>";
            }
        }

        return row;
    }

    var $table_transform = $("#lote_table");

    $scope.startDetails = function(r , b){
        b();
        if(r){
        	$scope.factor = 1;
            $scope.classColumns = "col-md-6 col-sm-6";
			var options = {};
            // company 
            $scope.id_company = r.id_company
            // data grafica
            $scope.palancas = r.palanca || [];
            $scope.fincas = r.fincas || [];

            let data_graficas = r.historico || [];
            $.each(data_graficas, (index, value) => {
                let newData = []
                Object.keys(value.data).map((val, i) => {
                    /*if(val > newData.length+1) {
                        for(var j = 0; j < (val - newData.length); j++){
                            newData.push(undefined);
                        }
                    }*/
                    newData.push(value.data[val]);
                })
                data_graficas[index].data = []
                data_graficas[index].data = newData
            })
            $scope.graficas.historico = angular.copy(data_graficas);

            $scope.graficas.historico_avg = r.historico_avg || [];
            $scope.graficas.historico_legends = r.historico_legends || [];
            $scope.graficas.dia = r.dia || [];
            $scope.graficas.dia_title = r.dia_title || "";
            $scope.graficas.danos = r.danos || [];
            // La propiedad danos_detalle es un objeto con todos los detalles de daños 
            $scope.graficas.danos_detalle = r.danos_detalle || [];
            // Tags 
            $scope.umbrales = r.umbrals || {};
            $scope.tags.merma.value = Number(r.tags.merma.porc).toFixed(2);
            $scope.tags.merma.peso = Number(r.tags.merma.peso).toFixed(2);
            var margen = {
                min : 0 ,
                min_tendencia : 0 ,
                max : 5,
                max_tendencia : 100,
                umbral : 2,
                umbral_tendencia : 50,
            }
            
            $scope.tags.tallo.value = new Number(r.tags.tallo).toFixed(2);
            $scope.tags.merma_cortada.value = new Number(r.tags.merma_cortada).toFixed(2);
            $scope.tags.tallo.value = new Number(r.tags.tallo).toFixed(2);

            $scope.tags.empaque.peso = r.tags.empaque.peso;
            $scope.tags.empaque.value = new Number(r.tags.empaque.porc).toFixed(2);
            $scope.tags.practicas_agricolas.peso = r.tags.practicas_agricolas.peso;
            $scope.tags.practicas_agricolas.value = new Number(r.tags.practicas_agricolas.porc).toFixed(2);
            $scope.tags.fisiologicos.peso = r.tags.fisiologicos.peso;
            $scope.tags.fisiologicos.value = new Number(r.tags.fisiologicos.porc).toFixed(2);
            $scope.tags.cosecha.peso = r.tags.cosecha.peso;
            $scope.tags.cosecha.value = new Number(r.tags.cosecha.porc).toFixed(2);
            

            $scope.tags.cajas.value = new Number(r.tags.cajas).toFixed(2);
            $scope.tags.usd.value = new Number(r.tags.usd).toFixed(2);
            if(r.hasOwnProperty("factor")){
                $scope.factor = new Number(r.factor).toFixed(2);
            }
            $scope.labelPeso = "(KG)";
            margen.max = 25;
            margen.umbral = 10;


            $scope.graficas.tendencia = r.tendencia || [];
            $scope.graficas.tendencia_legends = r.tendencia_legends || [];

            // tablas
            $scope.tabla_lote_merma = r.tabla_lote_merma;
            $scope.tabla_danos_merma = r.tabla_danos_merma;
            $scope.tabla_danos_merma_danhos_merma = r.tabla_danos_merma_danhos_merma;
            // header
            if(r.data_header){
                $scope.param_peso    = r.data_header.peso;
                $scope.param_cluster = r.data_header.cluster;
                $scope.param_logo = r.data_header.logo;
            }

            setTimeout(function(){
                loadScript($scope.graficas, margen)
                //generateSelect($scope.graficas.danos_detalle);
                $scope.cambiarDanosDetalle();
                $(".counter_tags").counterUp({
                    delay: 10,
                    time: 1000
                });
                 $table_transform.bootstrapTable();
            } , 1000);
        }
    }

    /*const generateSelect = function(data){
        var html = [];
        var keys = Object.keys(data);
        for(var i in keys){
            if(typeof keys[i] !== 'function')
                html.push("<option value='"+keys[i]+"'>"+keys[i]+"</option>");
        }

        $('#detalles_danhos').empty();
        $('#detalles_danhos').append(html.join(' '));

        setTimeout(function(){
            $scope.cambiarDanosDetalle();
        } , 1000);
    }*/

    $scope.companiesDefect = [];
    $scope.companiesDefectProm = [2, 7];

    const renderTablaCierreEnfundeLote = (data, semanas) => {
        var props = {
            header : [{
                   key : 'lote',
                   name : 'LOTE',
                   titleClass : 'text-center',
                   alignContent : 'center',
                   locked : true,
                   expandable : true,
                   resizable : true,
                },{
                    key : 'avg',
                    name : 'AVG',
                    titleClass : 'text-center',
                    alignContent : 'center',
                    locked : true,
                    resizable : true
                },{
                    key : 'min',
                    name : 'MIN',
                    titleClass : 'text-center',
                    alignContent : 'center',
                    locked : true,
                    resizable : true
                },{
                    key : 'max',
                    name : 'MAX',
                    titleClass : 'text-center',
                    alignContent : 'center',
                    locked : true,
                    resizable : true
                }

            ],
            data : data,
            buttons : [
                {
                    title : 'Excel',
                    action : () => {
                        $scope.table1.exportToExcel()
                    },
                    className : ''
                }
            ]
        }
        semanas.map((semana) => {
            props.header.push({
                key : `sem_${semana}`,
                name : `${semana}`,
                sortable : true,
                alignContent : 'right',
                titleClass : 'text-center',
                filterable : true,
                filterRenderer: 'NumericFilter',
                resizable : true,
                customCell : function(rowData, isChildren){
                    let valueNum = parseFloat(rowData['sem_'+semana])
                    var valueCell = valueNum
                    if(!valueNum > 0) valueCell = ''
                    return `
                        <div class="text-center" style="height: 100%">
                            ${valueCell}
                        </div>
                    `;
                }
            })
        })

        setTimeout(() => {
            $("#tabla-cierre-enfunde").html("")
            $scope.table1 = ReactDOM.render(React.createElement(ReactDataGrid, props), document.getElementById('tabla-cierre-enfunde'))
        }, 500)
    }

    const renderTableCierreEnfundeVar = (data, semanas) => {
        var props = {
            header : [{
                   key : 'lote',
                   name : 'VARIABLE',
                   titleClass : 'text-center',
                   alignContent : 'center',
                   locked : true,
                   expandable : true,
                   resizable : true,
                },{
                    key : 'avg',
                    name : 'AVG',
                    titleClass : 'text-center',
                    alignContent : 'center',
                    locked : true,
                    resizable : true
                },{
                    key : 'min',
                    name : 'MIN',
                    titleClass : 'text-center',
                    alignContent : 'center',
                    locked : true,
                    resizable : true
                },{
                    key : 'max',
                    name : 'MAX',
                    titleClass : 'text-center',
                    alignContent : 'center',
                    locked : true,
                    resizable : true
                }

            ],
            data : data,
            buttons : [
                {
                    title : 'Excel',
                    action : () => {
                        $scope.table2.exportToExcel()
                    },
                    className : ''
                }
            ]
        }
        semanas.map((semana) => {
            props.header.push({
                key : `sem_${semana}`,
                name : `${semana}`,
                sortable : true,
                alignContent : 'right',
                titleClass : 'text-center',
                filterable : true,
                filterRenderer: 'NumericFilter',
                resizable : true,
                customCell : function(rowData, isChildren){
                    let valueNum = parseFloat(rowData['sem_'+semana])
                    var valueCell = valueNum
                    if(!valueNum > 0) valueCell = ''
                    return `
                        <div class="text-center" style="height: 100%">
                            ${valueCell}
                        </div>
                    `;
                }
            })
        })

        setTimeout(() => {
            $("#tabla-cierre-enfunde-var").html("")
            $scope.table2 = ReactDOM.render(React.createElement(ReactDataGrid, props), document.getElementById('tabla-cierre-enfunde-var'))
        }, 500)
    }

    const printTablaCierreEnfunde = (r) => {
        renderTablaCierreEnfundeLote(r.lote.data, r.lote.semanas)
        renderTableCierreEnfundeVar(r.variables.data, r.variables.semanas)
    }

    $scope.loadTablaCierreEnfunde = () => {
        let data = $scope.calidad.params
        $http.post('phrapi/sumifru/merma/cierreEnfunde', data).then(r => {
            if(r.data){
                printTablaCierreEnfunde(r.data)
            }
        })
    }

    $scope.inArray = function(needed , arr){
        return $.inArray(needed , arr) > -1 ? true : false; 
    }

    $scope.exportExcel = function(id_table, title){
        var data = new Blob([document.getElementById(id_table).outerHTML], {
            type : 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf8'
        })
        saveAs(data, 'Merma.xls')
    }

    $scope.openTendencia = () => {
        setTimeout(() => {
            let v = $("[href=#collapse_3_3]").attr('aria-expanded') == 'true'
            if(v){
                let legends = Object.keys(appEcharts.options_tendencia_legends).map((e) => appEcharts.options_tendencia_legends[e])
                let data = {
                    series: appEcharts.options_tendencia,
                    legend: legends,
                    id: "tendencia",
                    legendBottom : false,
                    zoom : true,
                    umbral : 50,
                    type : 'line',
                    min : 'dataMin',
                    actions : false
                }
                $("#tendencia").remove()
                $("#collapse_3_3 .panel-body").append('<div id="tendencia" style="margin-top: 30px; height:500px;"></div>')
                ReactDOM.render(React.createElement(Historica, data), document.getElementById('tendencia'));
            }
        }, 100)
    }
    $scope.graficaTendenciaSemanal = function(){
        client.post('phrapi/sumifru/merma/tendenciaSemanal', (r, b) => {
            b('tendencia')
            if(r){
                appEcharts.options_tendencia = r.tendencia || [];
                appEcharts.options_tendencia_legends = r.tendencia_legends || [];
                $scope.openTendencia()
            }
        }, $scope.calidad.params, 'tendencia')
    }

    $scope.openTallo = () => {
        setTimeout(() => {
            let v = $("[href=#collapse_3_4]").attr('aria-expanded') == 'true'
            if(v){
                let data = {
                    series: $scope.graficas.tallo.chart.data,
                    legend: $scope.graficas.tallo.chart.legend,
                    umbral : $scope.graficas.tallo.umbral,
                    id: "tallo",
                    legendBottom : false,
                    zoom : true,
                    type : 'line',
                    min : 'dataMin',
                    actions : false
                }

                let parent = $("#tallo").parent()
                $("#tallo").remove()
                parent.append(`<div id="tallo" style="padding-top: 30px; height:500px;"></div>`)
                ReactDOM.render(React.createElement(Historica, data), document.getElementById('tallo'));
                renderTableTallo($scope.graficas.tallo.datatable, $scope.graficas.tallo.chart.legend)
            }
        }, 250)
    }
    const renderTableTallo = function(data, semanas){
        const _data = angular.copy(data)
        let props = {
            header : [{
                key : 'lote',
                name : 'LOTE',
                titleClass : 'text-center',
                alignContent : 'center',
                locked : true,
                expandable : true,
                resizable : true,
                width : 100
                },{
                    key : 'avg',
                    name : 'AVG',
                    titleClass : 'text-center',
                    alignContent : 'center',
                    locked : true,
                    resizable : true
                },{
                    key : 'min',
                    name : 'MIN',
                    titleClass : 'text-center',
                    alignContent : 'center',
                    locked : true,
                    resizable : true
                },{
                    key : 'max',
                    name : 'MAX',
                    titleClass : 'text-center',
                    alignContent : 'center',
                    locked : true,
                    resizable : true
                }

            ],
            data : _data.map((row) => {
                for(let i in semanas){
                    let sem = semanas[i]
                    if(!row[`sem_${sem}`]){
                        row[`sem_${sem}`] = ''
                    }
                }
                return row
            }),
            buttons : [
                {
                    title : 'Excel',
                    action : () => {
                        $scope.table3.exportToExcel()
                    },
                    className : ''
                }
            ]
        }
        semanas.map((semana) => {
            props.header.push({
                key : `sem_${semana}`,
                name : `${semana}`,
                sortable : true,
                alignContent : 'right',
                titleClass : 'text-center',
                filterable : true,
                filterRenderer: 'NumericFilter',
                resizable : true
            })
        })
        $("#table-tallo").html("")
        $scope.table3 = ReactDOM.render(React.createElement(ReactDataGrid, props), document.getElementById('table-tallo'));
    }

    $scope.graficaVariables = function(){
        client.post('phrapi/palmar/merma/variables', (r, b) => {
            b('tallo_block')
            if(r){
                $scope.graficas.tallo.chart = r.chart
                $scope.graficas.tallo.data = r.data
                $scope.graficas.tallo.umbral = r.umbral
                $scope.graficas.tallo.datatable = r.datatable
            }
        }, $scope.calidad.params , 'tallo_block')
    }

    $scope.load = function(){
        $http.post('phrapi/sumifru/merma/last').then(r => {
            $scope.datesEnabled = r.data.days
            $scope.years = r.data.years
            $scope.calidad.params.idFinca = r.data.finca
            $scope.calidad.params.fecha_inicial = r.data.fecha
            $scope.calidad.params.fecha_final = r.data.fecha

            document.getElementById('date-picker').innerHTML = `${r.data.fecha} - ${r.data.fecha}`

            $scope.loadExternal();
        })
    }
}]);