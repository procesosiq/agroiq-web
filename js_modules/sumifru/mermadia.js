/*----------  UTILIDADES SOBRE ARRAYS  ----------*/
app.filter('unique', function() {
   return function(collection, keyname) {
      var output = [], 
          keys = [];

      angular.forEach(collection, function(item) {
          var key = item[keyname];
          if(keys.indexOf(key) === -1) {
              keys.push(key);
              output.push(item);
          }
      });

      return output;
   };
});

function loadScript(options){

}


app.filter('orderObjectBy', function() {
  return function(items, field, reverse) {
    var filtered = [];
    angular.forEach(items, function(item) {
        // console.log(item)
        if(item.hasOwnProperty("lote")){
            if(!isNaN(parseFloat(item.lote)))
                item.lote = parseFloat(item.lote);
        }
        if(item.hasOwnProperty("total_peso_merma")){
            item.total_peso_merma = parseFloat(item.total_peso_merma);
        }
        if(item.hasOwnProperty("total_defectos")){
            if(!isNaN(parseFloat(item.total_defectos)))
                item.total_defectos = parseFloat(item.total_defectos);
        }
        if(item.hasOwnProperty("merma")){
            if(!isNaN(parseFloat(item.merma)))
                item.merma = parseFloat(item.merma);
        }
        if(item.hasOwnProperty("danhos_peso")){
            if(!isNaN(parseFloat(item.danhos_peso)))
                item.danhos_peso = parseFloat(item.danhos_peso);
        }
        if(item.hasOwnProperty("filter")){
            if(!isNaN(parseFloat(item.filter)))
                item.filter = parseFloat(item.filter);
        }
        if(item.hasOwnProperty("cantidad")){
            if(!isNaN(parseFloat(item.cantidad)))
                item.cantidad = parseFloat(item.cantidad);
        }

      filtered.push(item);
    });
    filtered.sort(function (a, b) {
        //alert(a[field]);
      return (a[field] > b[field] ? 1 : -1);
    });
    if(reverse) filtered.reverse();
    return filtered;
  };
});

let datesEnabled = []

app.controller('informe_merma_dia', ['$scope','client', function($scope,client){

    const datepickerHighlight = () => {
        $('#datepicker').datepicker({
            beforeShowDay: function (date) {
                let fecha = moment(date).format('YYYY-MM-DD')
                let has = datesEnabled.indexOf(fecha) > -1
                return has ? { classes: 'highlight', tooltip: 'Procesado', enabled : true } : { tooltip : 'Sin proceso', enabled : false }
            }
        });
        $('#datepicker').datepicker()
        .on('changeDate', function(e) {
            $scope.changeRangeDate({ first_date: moment(e.date).format('YYYY-MM-DD'), second_date : moment(e.date).format('YYYY-MM-DD') })
        });
        $("#datepicker").datepicker('setDate', $scope.calidad.params.fecha_inicial)
        $('#datepicker').datepicker('update')
    }
    
    $scope.leyendaGeneralTitle = 'Merma';
    $scope.id_company = 0;
    $scope.labelLbKg = "Kilos";
    $scope.statusLbKg = true;
    $scope.labelPeso = "";
    $scope.umbrales = {};
    $scope.rangesDirectives = {
        'Hoy': [moment(), moment()],
        'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Hace 2 Días': [moment().subtract(2, 'days'), moment().subtract(2, 'days')],
    }

    $scope.limitDirectives = {
        days : 0
    }

    $scope.StartEndDateDirectives = {
        startDate : moment(),
        endDate :moment(),
    }

    $scope.singleDatePickerDirective = true;

    $scope.table = {
        merma : [],
        merma_travel : [],
        merma_details : [],
        merma_campos : [],
        merma_widthCell : "10%",
        defectos : [],
        defectos_travel : [],
        defectos_details : [],
        details_campos : [],
        peso : [],
        peso_travel : [],
        peso_details : [],
        peso_campos : [],
        peso_widthCell : "10%",
        adicional : [],
    }

    $scope.calidad = {
        params : {
            idFinca : "",
            idFincaDia : "",
            idMerma : "MATERIA PRIMA",
            idLote : 0,
            idLabor : 0,
            fecha_inicial : moment().format('YYYY-MM-DD'),
            fecha_final : moment().format('YYYY-MM-DD'),
            cliente: "",
            marca: "",
            palanca : "",
            statusLbKg : true,
            categoria : "COSECHA"
        },
        step : 0,
        path : ['phrapi/dia/index'],
        templatePath : [],
        nocache : function(){
            this.templatePath.push('/views/templetes/mermadia/step1.html?' +Math.random());
        }
    }

    $scope.palancas = [];

    $scope.mermas = {
        NETA : "Merma Neta",
        "MATERIA PRIMA" : "Merma Prima",
    };

    const cambiosMerma = function(){
        $scope.calidad.params.palanca = "";
        $scope.calidad.params.idFincaDia = "";
        $scope.loadExternal();
    }

    $scope.loadExternal = function(){
        //console.log($scope.calidad.path[$scope.calidad.step]);
        if($scope.calidad.path[$scope.calidad.step] != ""){
            var data = $scope.calidad.params;
            client.post($scope.calidad.path[$scope.calidad.step] , $scope.startDetails , data);
        }
    }

    const cambiosCategoria = function(){
        $scope.calidad.params.categoria = $('#categoria').val();
        $scope.loadExternal()
    }

    $scope.changeRangeDate = function(data){
        if(data){
            $scope.calidad.params.fecha_inicial = data.hasOwnProperty("first_date") ? data.first_date : $scope.wizardStep.params.fecha_inicial;
            $scope.calidad.params.fecha_final = data.hasOwnProperty("second_date") ? data.second_date : $scope.wizardStep.params.fecha_final;
            $scope.loadExternal();
        }
    }

    $scope.convertKg = function(){
        $scope.statusLbKg = !$scope.statusLbKg;
        if($scope.statusLbKg === true)
            $scope.labelLbKg = "Kilos";
        else if($scope.statusLbKg === false)
            $scope.labelLbKg = "Libras";

        $scope.calidad.params.statusLbKg = $scope.statusLbKg;
        $scope.loadExternal();
    }

    $scope.getKey = function(data , key){
        return Object.keys(data)[key];
    }

    $scope.totalDefectos = {};


    $scope.fincas = [];

    $scope.cambiosFincas = function(){
        $scope.calidad.params.palanca = "";
        $scope.calidad.params.idFincaDia = "";
        $scope.loadExternal();
    }

    $scope.startDetails = function(r , b){
        b();
        if(r){
			var options = {};
            // company 
            $scope.id_company = r.id_company
            // data grafica
            $scope.palancas = r.palanca || [];
            $scope.fincas = r.fincas || [];
            // merma : [],
            // defectos : [],
            // peso : [],
            $scope.table.merma = r.merma || [];
            $scope.table.merma_travel = r.merma_travel || [];
            $scope.table.merma_details = r.merma_details || [];
            $scope.table.merma_campos = r.merma_campos || [];
            $scope.table.defectos = r.defectos || [];
            $scope.table.defectos_travel = r.defectos_travel || [];
            $scope.table.defectos_details = r.defectos_details || [];
            $scope.table.details_campos = r.details_campos || [];
            $scope.table.peso = r.peso || [];
            $scope.table.dedos_prom = r.dedos_prom || [];
            $scope.table.dedos_details = r.dedos_details || [];
            $scope.table.peso_travel = r.peso_travel || [];
            $scope.table.peso_details = r.peso_details || [];
            $scope.table.peso_campos = r.peso_campos || [];

            console.log($scope.table.dedos_details)

            if($scope.id_company == 2){
    
            }
            if($scope.id_company == 6){
               $scope.table.merma_widthCell = r.merma_widthCell + "%";
               $scope.table.peso_widthCell = r.peso_widthCell + "%";
            }
            if($scope.id_company == 7){
               $scope.table.adicional.maxTravel = r.adicional.maxTravel || []
               $scope.table.adicional.data = r.adicional.data || []
               $scope.table.adicional.details = r.adicional.details || []
               $scope.table.adicional.details_campos = r.adicional.details_campos || []
            }
            if($scope.id_company == 3){
               $scope.table.adicional.maxTravel = r.adicional.maxTravel || []
               $scope.table.adicional.data = r.adicional.data || []
               $scope.table.adicional.details = r.adicional.details || []
               $scope.table.adicional.details_campos = r.adicional.details_campos || []
            }

        }
    }

    $scope.last = () => {
        client.post('phrapi/palmar/merma/last', (r, b) => {
            b()
            if(r.fecha){
                $scope.calidad.params.fecha_inicial = r.fecha
                $scope.calidad.params.fecha_final = r.fecha
                
                datesEnabled = r.days
                datepickerHighlight()

                $("#date-picker").html(`${r.fecha} - ${r.fecha}`)
            }
            $scope.loadExternal()
        })
    }

}]);