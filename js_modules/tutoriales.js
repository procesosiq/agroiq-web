app.service('request', ['$http', ($http) => {
	var service = {}

	service.index = (params) => {
		return new Promise((resolve, reject) => {
			$http.post('phrapi/tutoriales/index', params || {})
			.then((r) => {
				resolve(r.data)
			})
			.catch((r) => {
				reject()
			})
		})
	}

	return service
}])

app.filter('bypass', ['$sce', ($sce) => {
    return function(html) {
      	return $sce.trustAsHtml(html);
    };
}])

app.controller('controller', ['$scope', 'request', ($scope, $request) => {


	$scope.init = () => {
		load.block()
		$request.index()
		.then((r) => {
			$scope.tutoriales = r.data
			setTimeout(() => {
				load.unblock()
				$scope.$apply()
			}, 200)
		})
	}

	$scope.init()
}])