<?php
    /*=============================================================
    =            Insertar aqui validacion  de usuarios            =
    =============================================================*/ 
    $session = Session::getInstance();

    $calidad = [
        'calidad',
        'calidad2',
        'calidadComparacion',
        'calidadTendencia'
    ];
    $agroaudit = [
        "laboresAgricolasDia",
        "laboresAgricolasComparativo",
        "laboresAgricolasTendencia",
        "laboresAgricolas",
        "reportes",
        "geoposicion",
        "rpersonal"
    ];
    $validate = [
        "tthhPersonal",
        "asistencia",
        "tthhFPersonal",
        "bonificacion",
    ];

    $merma = [
        "merma",
        "mermadia",
    ];
    
    $lancofruit = [
        'revisionLancofruit',
        'lancofruit',
        'perchas',
        'lancofruitTemperatura',
        'ventas',
        'rastreo',
        'revisionLancofruitOnlyExcel',
        'importarInformacion',
        'cercaLancofruit',
        'configLancofruit',
        'rastreoLancofruit',
        'lancofruitVentasDia',
        'lancofruitVentasComparativo',
        'lancofruitVentasTendencia',
        'perchasDia',
        'perchasBD',
        'perchasComparacion',
        'perchasTendencia'
    ];
    $lancofruit_ventas = [
        'lancofruitVentasDia',
        'lancofruitVentasComparativo',
        'lancofruitVentasTendencia',
    ];
    $perchas = [
        'perchasDia',
        'perchasBD',
        'perchasComparacion',
        'perchasTendencia'
    ];

    $cacao = [
        'consolidadoCosecha',
        'analisisSensorial',
        'asignacionMallas',
        'formularioAsignacionArea',
        'balanzaCosecha',
    ];
    $cacao_produccion = [
        'cacaoReporteCosecha',
        'cacaoReporteCosechaPeriodal',
        'cacaoReporteCosechaSector',
        'cosechaLotes'
    ];
    $cacao_config = [
        'cacaoConfigLotes'
    ];
    $cacao_tthh = [
        "cacaotthhPersonal",
        "cacaotthhFPersonal"
    ];

    $sigat_revision = [
        'fincas',
        'revisionFumigadoras',
        'revisionGerentes',
        'revisionProveedores',
        'revisionSectores',
        'revisionTipoProductos',
        'productos',
        'revisionDosis',
        'revisionRankingCocteles'
    ];

    $produccion_reportes = [
        "produccionsemana",
        "recobro",
        "producciongerencia",
        "produccionReporte",
        "produccionResumenCorte",
        "produccionDemo",
        "produccionComparacion",
        "produccionReporteAcumulado",
        "produccionComparacionAnual"
    ];
    $produccion_fuentes = [
        "produccion",
        "produccioncajas",
        "produccioncajasbase",
        "produccionenfunde",
        "produccionRacimos",
        "produccionRacimosFormularios",
        "produccionPrecalibracion",
        "nuevoEnfundeSemanal"
    ];

    $clima = [
        "clima",
        "climaDiario"
    ];

    $configuracion = [
        'configLotes',
        'configPlantas'
    ];

    $usuario = [
        'tutoriales'
    ];
    
    $palma = [
        'palmaLaboresAgricolasDia'
    ];

    $monitor_prontoforms = [
        "monitorProntoforms"
    ];
    $monitor_balanzas = [
        "monitorBalanzas"
    ];
    /*=====  End of Insertar aqui validacion  de usuarios  ======*/
?>

<style>
    .page-sidebar-footer{
        position: fixed !important;
        width : 100%;
    }
    #select-cultivo {

        margin: 0 !important;
        width: 100%;
    }
</style>

<!-- BEGIN SIDEBAR -->
<div class="page-sidebar-wrapper">
    <!-- BEGIN SIDEBAR -->
    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
    <div id="primary-page-sidebar" class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
        <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
        <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
        <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
        <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
        <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
        <?php if($session->cultivos->banano == 'Activo'):?>
        <ul class="page-sidebar-menu BANANO page-header-fixed page-sidebar-menu-compact" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
            <?php if($session->settings->sigat == 'Activo'):?>
                <li class="nav-item  <?php if($carga_modulo->modulo == 'sigat'){ ?> active start <? } ?>">
                    <a href="http://sigat.procesos-iq.com/controllers/Aceso.php?token=<?=$session->settings->token?>" target="_blank" class="nav-link nav-toggle">
                        <i class="fab fa-pagelines"></i>
                        <span class="title">Sigat</span>
                        <span class="selected"></span>
                    </a>
                </li>
            <?php endif;?>
            <?php if($session->settings->sigat_module == 'Activo'): ?>
                <li class="nav-item <?php if(in_array($carga_modulo->modulo, $sigat_revision)){ ?> active start <? } ?>">
                    <a href="javascript:;" class="nav-link ">
                        <i class="icon-social-dribbble"></i>
                        <span class="title">Sigat</span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <li class="<?php if(in_array($carga_modulo->modulo, $sigat_revision)){ ?> active start <? } ?>">
                            <a href="javascript:;" class="nav-link ">
                                <span class="title">Configuración</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="<?= ($carga_modulo->modulo == 'fincas') ? 'active': '' ?>">
                                    <a href="fincas" class="nav-link">
                                        <span class="title">Fincas</span>
                                    </a>
                                </li>
                                <li class="<?= ($carga_modulo->modulo == 'revisionFumigadoras') ? 'active': '' ?>">
                                    <a href="revisionFumigadoras" class="nav-link">
                                        <span class="title">Fumigadoras</span>
                                    </a>
                                </li>
                                <li class="<?= ($carga_modulo->modulo == 'revisionGerentes') ? 'active': '' ?>">
                                    <a href="revisionGerentes" class="nav-link">
                                        <span class="title">Gerentes</span>
                                    </a>
                                </li>
                                <li class="<?= ($carga_modulo->modulo == 'revisionProveedores') ? 'active': '' ?>">
                                    <a href="revisionProveedores" class="nav-link">
                                        <span class="title">Proveedores</span>
                                    </a>
                                </li>
                                <li class="<?= ($carga_modulo->modulo == 'revisionSectores') ? 'active': '' ?>">
                                    <a href="revisionSectores" class="nav-link">
                                        <span class="title">Sectores</span>
                                    </a>
                                </li>
                                <li class="<?= ($carga_modulo->modulo == 'revisionTipoProductos') ? 'active': '' ?>">
                                    <a href="revisionTipoProductos" class="nav-link">
                                        <span class="title">Tipo de productos</span>
                                    </a>
                                </li>
                                <li class="<?= ($carga_modulo->modulo == 'productos') ? 'active': '' ?>">
                                    <a href="productos" class="nav-link">
                                        <span class="title">Productos</span>
                                    </a>
                                </li>
                                <li class="<?= ($carga_modulo->modulo == 'revisionDosis') ? 'active': '' ?>">
                                    <a href="revisionDosis" class="nav-link">
                                        <span class="title">Dosis</span>
                                    </a>
                                </li>
                                <li class="<?= ($carga_modulo->modulo == 'revisionRankingCocteles') ? 'active': '' ?>">
                                    <a href="revisionRankingCocteles" class="nav-link">
                                        <span class="title">Ranking Cocteles</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
            <?php endif; ?>
            <?php if($session->settings->agroaudit == 'Activo'):?>
                <li class="nav-item <?= in_array($carga_modulo->modulo, $agroaudit) ? 'open active' : '' ?>">
                    <a href="javascript:;" class="nav-link ">
                        <img src="assets/icons/farmer.svg" width="20" height="18">
                        <span class="title">Labores Agrícolas</span>
                        <span class="arrow <?= in_array($carga_modulo->modulo, $agroaudit) ? 'open active' : '' ?>"></span>
                    </a>
                    <ul class="sub-menu" <?= in_array($carga_modulo->modulo, $agroaudit) ? 'style="display:block;" ' : '' ?>>
                        <?php if($session->id_company == 3 && false):?>
                        <li class="nav-item <?= $carga_modulo->modulo == 'reportes' ? 'open active' : '' ?>'">
                            <a href="labores" class="nav-link">
                                <i class="icon-bar-chart"></i>
                                <span class="title">Reporte</span>
                            </a>
                        </li>
                        <?php endif;?>
                        <?php if(in_array($session->id_company, [4, 12])):?>
                        <li class="nav-item <?php if($carga_modulo->modulo == 'laboresAgricolas'){ ?> active open <? } ?>">
                            <a href="laboresAgricolas" class="nav-link">
                                <i class="icon-bar-chart"></i>
                                <span class="title">Reporte</span>
                            </a>
                        </li>
                        <?php endif;?>
                        <?php if(!in_array($session->id_company,[3,4,6,12,13,17,3])):?>    
                        <li class="nav-item <?php if($carga_modulo->modulo == 'reportes'){ ?> active open <? } ?>">
                            <a href="reportes" class="nav-link">
                                <i class="icon-bar-chart"></i>
                                <span class="title">Reporte</span>
                            </a>
                        </li>
                        <?php endif;?>
                        <?php if(!in_array($session->id_company, [3,7,6,17,3])): ?>
                            <li class="nav-item <?php if($carga_modulo->modulo == 'rpersonal'){ ?> active open <? } ?>">
                            <a href="rpersonal" class="nav-link">
                                <i class="fa fa-user"></i>
                                <span class="title">Resultados Personal</span>
                            </a>
                        </li>
                        <?php endif;?>
                        
                        <?php if(!in_array($session->id_company, [6,17,3])): ?>
                        <li class="nav-item <?php if($carga_modulo->modulo == 'geoposicion'){ ?> active open <? } ?>">
                            <a href="geoposicion" class="nav-link">
                                <i class="icon-pointer"></i>
                                <span class="title">Geolocalización</span>
                            </a>
                        </li>
                        <?php endif ; ?>

                        <?php if($session->id_company == 6): ?>
                        <li class="nav-item <?php if(in_array($carga_modulo->modulo, ['laboresAgricolas', 'geoposicion', 'rpersonal'])){ ?> open active <? } ?>">
                            <a href="javascript:;" class="nav-link ">
                                <i class="icon-social-dribbble"></i>
                                <span class="title">Reportes</span>
                                <span class="arrow <?php if(in_array($carga_modulo->modulo, ['laboresAgricolas', 'geoposicion', 'rpersonal'])){ ?> open active <? } ?>"></span>
                            </a>
                            <ul class="sub-menu <?php if(in_array($carga_modulo->modulo, ['laboresAgricolas', 'geoposicion', 'rpersonal'])){ ?> block <? } ?>">
                                <li class="nav-item <?= $carga_modulo->modulo == 'laboresAgricolas' ? 'active open' : '' ?>">
                                    <a href="laboresAgricolas" class="nav-link">
                                        <i class="fa fa-line-chart"></i>
                                        <span class="title">Reporte</span>
                                    </a>
                                </li>
                                <li class="nav-item <?= $carga_modulo->modulo == 'rpersonal' ? 'active open' : '' ?>">
                                    <a href="rpersonal" class="nav-link">
                                        <i class="fa fa-user"></i>
                                        <span class="title">Resultados Personal</span>
                                    </a>
                                </li>
                                <li class="nav-item hide <?php if($carga_modulo->modulo == 'geoposicion'){ ?> active open <? } ?>">
                                    <a href="geoposicion" class="nav-link">
                                        <i class="icon-pointer"></i>
                                        <span class="title">Geolocalización</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <?php endif ; ?>

                        <?php if(in_array($session->id_company, [17, 3])): ?>
                        <li class="nav-item <?= in_array($carga_modulo->modulo, $agroaudit) ? 'active open' : '' ?>">
                            <a href="javascript:;" class="nav-link ">
                                <i class="icon-social-dribbble"></i>
                                <span class="title">Reportes</span>
                                <span class="arrow <?= in_array($carga_modulo->modulo, $agroaudit) ? 'active open' : '' ?>"></span>
                            </a>
                            <ul class="sub-menu <?= in_array($carga_modulo->modulo, $agroaudit) ? 'block' : '' ?>">
                                <li class="nav-item <?= $carga_modulo->modulo == 'laboresAgricolasDia' ? 'active open' : '' ?>">
                                    <a href="laboresAgricolasDia" class="nav-link">
                                        <i class="fa fa-calendar-o"></i>
                                        <span class="title">Día</span>
                                    </a>
                                </li>
                                <li class="nav-item <?= $carga_modulo->modulo == 'laboresAgricolasComparativo' ? 'active open' : '' ?>">
                                    <a href="laboresAgricolasComparativo" class="nav-link">
                                        <i class="icon-shuffle"></i>
                                        <span class="title">Comparativo</span>
                                    </a>
                                </li>
                                <li class="nav-item <?php if($carga_modulo->modulo == 'laboresAgricolasTendencia'){ ?> active open <? } ?>">
                                    <a href="laboresAgricolasTendencia" class="nav-link">
                                        <i class="fa fa-area-chart"></i>
                                        <span class="title">Tendencia</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <?php endif ; ?>
                    </ul>
                </li>
            <?php endif;?>
            <?php if($session->settings->calidad == 'Activo'):?>
                <li class="nav-item  <?php if(in_array($carga_modulo->modulo, $calidad)){ ?> active start <? } ?>">
                    <a href="javascript:;" class="nav-link ">
                        <img src="assets/icons/winner.svg" width="20" height="18"/>
                        <span class="title">Calidad</span>
                        <span class="selected"></span>
                        <span class="arrow <?php if(in_array($carga_modulo->modulo, $calidad)){ ?> open active <? } ?>"></span>
                    </a>
                    <ul class="sub-menu"<?php if($carga_modulo->modulo == 'calidad'){ ?> style="display:block;" <?php } ?>>
                        <?php if($session->type_users == 'ADMIN' && !in_array($session->id_company, [7,11,12])):?>
                            <li class="nav-item <?= $carga_modulo->modulo == 'calidad' ? 'active start' : '' ?>">
                                <a href="calidad" class="nav-link">
                                    <i class="icon-bar-chart"></i>
                                    <span class="title">Reporte</span>
                                </a>
                            </li>
                        <?php endif; ?>
                        <?php if(in_array($session->id_company, [7,11,12])):?>
                            <li class="nav-item <?= $carga_modulo->modulo == 'calidad2' ? 'active start' : '' ?>">
                                <a href="calidad2" class="nav-link">
                                    <i class="icon-bar-chart"></i>
                                    <span class="title">Reporte Día</span>
                                </a>
                            </li>
                        <?php endif; ?>
                        <?php if(in_array($session->id_company, [7,11,12])):?>
                            <li class="nav-item <?= $carga_modulo->modulo == 'calidadComparacion' ? 'active start' : '' ?>">
                                <a href="calidadComparacion" class="nav-link">
                                    <i class="icon-bar-chart"></i>
                                    <span class="title">Comparación</span>
                                </a>
                            </li>
                            <li class="nav-item <?= $carga_modulo->modulo == 'calidadTendencia' ? 'active start' : '' ?>">
                                <a href="calidadTendencia" class="nav-link">
                                    <i class="icon-bar-chart"></i>
                                    <span class="title">Tendencia</span>
                                </a>
                            </li>
                        <?php endif; ?>
                        <?php if(($session->type_users == 'ADMIN' || $session->privileges->calidad_comparacion == 'Activo') && $session->id_company == 2):?>
                            <li class="nav-item">
                                <a href="calidadComparativo" class="nav-link">
                                    <i class="icon-bar-chart"></i>
                                    <span class="title">Comparación</span>
                                </a>
                            </li>
                        <?php endif; ?>
                        <?php if($session->logged == 31):?>
                            <li class="nav-item">
                                <a href="qualitat?c=REWE&m=REWE" class="nav-link">
                                    <i class="icon-bar-chart"></i>
                                    <span class="title">REWE</span>
                                </a>
                            </li>
                        <?php endif; ?>
                        <?php if(isset($session->references) && count($session->references->brands) > 0): ?>
                            <?php 
                                foreach ($session->references->brands as $key => $value) : 
                                    $value = (object)$value;
                                    if(!($value->cliente == "REWE" && $value->marca == "REWE")):
                                        $url = "calidad?c=".$value->cliente."&m=".$value->marca;
                            ?>
                                        <li class="nav-item">
                                            <a href="<?=$url?>" class="nav-link">
                                                <i class="icon-bar-chart"></i>
                                                <span class="title"><small><?=$value->cliente?> - <?=$value->marca?></small></span>
                                            </a>
                                        </li>
                            <?php 
                                endif; 
                            endforeach; ?>
                        <?php endif; ?>
                    </ul>
                </li>
            <?php endif;?>
            <?php if($session->settings->benchmark == 'Activo'):?>
                <li class="nav-item active start">
                    <a href="javascript:;" class="nav-link ">
                        <i class="icon-bar-chart"></i>
                        <span class="title">Benchmark</span>
                        <span class="selected"></span>
                        <span class="arrow open active"></span>
                    </a>
                    <ul class="sub-menu" style="display:block;">
                        <li class="nav-item">
                            <a href="produccionComparacion" class="nav-link">
                                
                                <span class="title">Grafica</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="produccionComparacionAnalisis" class="nav-link">
                                
                                <span class="title">Analisis</span>
                            </a>
                        </li>
                    </ul>
                </li>
            <?php endif;?>
            <?php if($session->settings->merma == 'Activo'):?>
                <li class="nav-item  <?php if(in_array($carga_modulo->modulo,$merma)){ ?> active start <? } ?>">
                    <a href="javascript:;" class="nav-link">
                        <img src="assets/icons/decrease.svg" width="20" height="18"/>
                        <span class="title">Merma</span>
                        <span class="selected"></span>
                        <span class="arrow <?php if(in_array($carga_modulo->modulo,$merma)){ ?> open active <? } ?>"></span>
                    </a>
                    <ul class="sub-menu"<?php if(in_array($carga_modulo->modulo,$merma)){ ?>
                                style="display:block;" 
                        <?php } ?>
                    >
                        <li class="nav-item <?php if($carga_modulo->modulo == 'merma'){ ?> active open <? } ?>">
                            <a href="merma" class="nav-link merma" data-id_company="<?php echo($session->id_company)?>">
                                <i class="icon-bar-chart"></i>
                                <span class="title">General</span>
                            </a>
                        </li>
                        <?php if($session->id_company > 0):?>
                            <li class="nav-item <?php if($carga_modulo->modulo == 'mermadia'){ ?> active open <? } ?>">
                                <a href="mermadia" class="nav-link">
                                    <i class="icon-bar-chart"></i>
                                    <span class="title">Merma por Dia</span>
                                </a>
                            </li>
                        <?php endif;?>
                        <!--<li>
                            <a href="javascript:;" class="nav-link">
                                <span class="title">Revisión</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item <?php if($carga_modulo->modulo == 'revisionRacimos'){ ?> active open <? } ?>">
                                    <a href="revisionRacimos" class="nav-link">
                                        <span class="title">Racimos</span>
                                    </a>
                                </li>
                                <li class="nav-item <?php if($carga_modulo->modulo == 'revisionMerma'){ ?> active open <? } ?>">
                                    <a href="revisionMerma" class="nav-link">
                                        <span class="title">Merma</span>
                                    </a>
                                </li>
                            </ul>
                        </li>-->
                    </ul>
                </li>
            <?php endif;?>
            <?php if($session->settings->produccion == 'Activo'):?>
                <li class="nav-item <?php if(in_array($carga_modulo->modulo, $produccion_fuentes) || in_array($carga_modulo->modulo, $produccion_reportes)){ ?> active start <? } ?>">
                    <a href="produccion" class="nav-link nav-toggle">
                        <img src="assets/icons/bananas.svg" width="20" height="18"/>
                        <span class="title">Producción</span>
                        <span class="selected"></span>
                        <span class="arrow <?php if(in_array($carga_modulo->modulo, $produccion_fuentes) || in_array($carga_modulo->modulo, $produccion_reportes)){ ?> open active <? } ?>"></span>
                    </a>
                    <ul class="sub-menu"<?php if(in_array($carga_modulo->modulo, $produccion_fuentes) || in_array($carga_modulo->modulo, $produccion_reportes)){ ?> style="display:block;" <?php } ?>>
                        
                        <li class="nav-item <?= in_array($carga_modulo->modulo, $produccion_reportes) ? 'active open' : '' ?>">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <img src="assets/icons/analysis.svg" width="25" height="30"/>
                                <span class="title">Reportes</span>
                                <span class="selected"></span>
                                <span class="arrow <?= in_array($carga_modulo->modulo, $produccion_reportes) ? 'active open' : '' ?>"></span>
                            </a>
                            <ul class="sub-menu" style="display : <?= in_array($carga_modulo->modulo, $produccion_reportes) ? 'block' : 'none' ?>">
                                <?php if(in_array($session->id_company, [2, 3, 6, 4, 7, 10, 11, 12, 13, 14, 15, 16, 18, 19, 20])):?>
                                    <li class="nav-item <?= $carga_modulo->modulo == 'produccionsemana' ? 'active open' : ''  ?>">
                                        <a href="produccionsemana" class="nav-link">
                                            <i class="fas fa-chart-line"></i>
                                            <span class="title">Resumen General</span>
                                        </a>
                                    </li>
                                <?php endif;?>
                                <?php if(in_array($session->id_company, [2])):?>
                                    <li class="nav-item <?= $carga_modulo->modulo == 'produccionComparacionAnual' ? 'active open' : ''  ?>">
                                        <a href="produccionComparacionAnual" class="nav-link">
                                            <i class="fas fa-chart-line"></i>
                                            <span class="title">Resumen Comparativo Anual</span>
                                        </a>
                                    </li>
                                <?php endif;?>
                                <?php if(in_array($session->id_company, [2, 3, 6, 7, 10, 11, 12, 13, 15, 16, 19, 20])):?>
                                    <li class="nav-item <?php if($carga_modulo->modulo == 'produccionResumenCorte'){ ?> active open <? } ?>">
                                        <a href="produccionResumenCorte" class="nav-link">
                                            <i class="far fa-clock"></i>
                                            <span class="title">Reporte de Corte Día</span>
                                        </a>
                                    </li>
                                <?php endif;?>
                                <?php if(in_array($session->id_company, [11])):?>
                                    <li class="nav-item <?php if($carga_modulo->modulo == 'produccionComparacion'){ ?> active open <? } ?>">
                                        <a href="produccionComparacion" class="nav-link">
                                            <i class="icon-bar-chart"></i>
                                            <span class="title">Comparación</span>
                                        </a>
                                    </li>
                                <?php endif;?>
                                <?php if(in_array($session->id_company, [20])):?>
                                    <li class="nav-item <?= $carga_modulo->modulo == 'tableauruforcorp' ? 'active open' : ''  ?>">
                                        <a href="tableauruforcorp" class="nav-link">
                                            <i class="fas fa-chart-line"></i>
                                            <span class="title">Tableau Dashboard</span>
                                        </a>
                                    </li>
                                <?php endif;?>
                                <?php if(in_array($session->id_company, [])):?>
                                    <li class="nav-item <?php if($carga_modulo->modulo == 'produccionComparacion'){ ?> active open <? } ?>">
                                        <a href="produccionComparacion" class="nav-link">
                                            <i class="icon-bar-chart"></i>
                                            <span class="title">Comparacion</span>
                                        </a>
                                    </li>
                                <?php endif;?>
                                <?php if(in_array($session->id_company, [2, 6, 7, 10, 11, 12, 13, 16, 19, 20])):?>
                                    <li class="nav-item <?php if($carga_modulo->modulo == 'recobro'){ ?> active open <? } ?>">
                                        <a href="recobro" class="nav-link">
                                            <i class="fas fa-registered"></i>
                                            <span class="title">Recobro</span>
                                        </a>
                                    </li>
                                <?php endif;?>
                                <?php if(in_array($session->id_company, [2, 3, 6, 7, 10, 11, 12, 13, 15, 16, 19, 20])):?>
                                    <li class="nav-item <?php if($carga_modulo->modulo == 'produccionReporte'){ ?> active open <? } ?>">
                                        <a href="produccionReporte" class="nav-link">
                                            <i class="icon-bar-chart"></i>
                                            <span class="title">Reporte ADM</span>
                                        </a>
                                    </li>
                                <?php endif;?>
                                <?php if(in_array($session->id_company, [10])):?>
                                    <li class="nav-item <?php if($carga_modulo->modulo == 'produccionReporteAcumulado'){ ?> active open <? } ?>">
                                        <a href="produccionReporteAcumulado" class="nav-link">
                                            <i class="icon-bar-chart"></i>
                                            <span class="title">Reporte ADM. Acum</span>
                                        </a>
                                    </li>
                                <?php endif;?>
                            </ul>
                        </li>

                        <li class="nav-item" <?= in_array($carga_modulo->modulo, $produccion_fuentes) ? 'active open' : '' ?>>
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="fas fa-database"></i>
                                <span class="title">Fuentes de Información</span>
                                <span class="selected"></span>
                                <span class="arrow <?= in_array($carga_modulo->modulo, $produccion_fuentes) ? 'active open' : '' ?>"></span>
                            </a>
                            <ul class="sub-menu" style="display : <?= in_array($carga_modulo->modulo, $produccion_fuentes) ? 'block' : 'none' ?>">
                                <?php if(in_array($session->id_company, [2])):?>
                                    <li class="nav-item <?php if($carga_modulo->modulo == 'produccion'){ ?> active open <? } ?>">
                                        <a href="produccion" class="nav-link">
                                            <i class="fas fa-balance-scale"></i>
                                            <span class="title">Balanza de Racimos</span>
                                        </a>
                                    </li>
                                <?php endif;?>
                                <?php if(in_array($session->id_company, [])):?>
                                <li class="nav-item <?php if($carga_modulo->modulo == 'produccionRacimosFormularios'){ ?> active open <? } ?>">
                                    <a href="produccionRacimosFormularios" class="nav-link">
                                        <i class="fas fa-mobile-alt"></i>
                                        <span class="title">Balanza de Racimos Form</span>
                                    </a>
                                </li>
                                <?php endif;?>
                                <?php if(in_array($session->id_company, [3, 6, 7, 10, 11, 12, 13, 14, 15, 16, 18, 19, 20])):?>
                                <li class="nav-item <?php if($carga_modulo->modulo == 'produccionRacimos'){ ?> active open <? } ?>">
                                    <a href="produccionRacimos" class="nav-link">
                                        <i class="fas fa-balance-scale"></i>
                                        <span class="title">Balanza de Racimos</span>
                                    </a>
                                </li>
                                <?php endif;?>
                                <?php if(in_array($session->id_company, [])):?>
                                <li class="nav-item <?php if($carga_modulo->modulo == 'produccionRacimosFormularios'){ ?> active open <? } ?>">
                                    <a href="produccionRacimosFormularios" class="nav-link">
                                        <i class="fas fa-balance-scale"></i>
                                        <span class="title">Balanza de Racimos</span>
                                    </a>
                                </li>
                                <?php endif;?>
                                <?php if(in_array($session->id_company, [2, 3, 6, 7, 10, 11, 12, 13, 14, 15, 16, 18, 19, 20])):?>
                                    <li class="nav-item <?php if($carga_modulo->modulo == 'produccioncajas'){ ?> active <? } ?>">
                                        <a href="produccioncajas" class="nav-link">
                                            <i class="fas fa-boxes"></i>
                                            <span class="title">Balanza de Cajas</span>
                                            
                                        </a>
                                    </li>
                                <?php endif;?>
                                <?php if(in_array($session->id_company, [2, 3, 19])):?>
                                    <li class="nav-item <?php if($carga_modulo->modulo == 'produccionPrecalibracion'){ ?> active open <? } ?>">
                                        <a href="produccionPrecalibracion" class="nav-link">
                                            <i class="fas fa-ruler-combined"></i>
                                            <span class="title">Precalibración</span>
                                        </a>
                                    </li>
                                <?php endif;?>
                                <?php if($session->settings->produccion_enfunde == 'Activo'): ?>
                                    <li class="nav-item <?php if($carga_modulo->modulo == 'produccionenfunde'){ ?> active open <? } ?>">
                                        <a href="produccionenfunde" class="nav-link">
                                            <i class="fa fa-bookmark"></i>
                                            <span class="title">Enfunde</span>
                                        </a>
                                    </li>
                                <?php endif;?>
                            </ul>
                        </li>
                    </ul>
                </li>
            <?php endif;?>
            <?php if($session->settings->tthh == 'Activo'):?>
                <li class="nav-item <?php if(in_array($carga_modulo->modulo, $validate)){ ?> open active <? } ?>">
                    <a href="javascript:;" class="nav-link ">
                        <i class="icon-user"></i>
                        <span class="title">TTHH</span>
                        <span class="arrow <?php if(in_array($carga_modulo->modulo, $validate)){ ?> open active <? } ?>"></span>
                    </a>
                    <ul class="sub-menu"<?php if(in_array($carga_modulo->modulo, $validate)){ ?>
                                style="display:block;" 
                        <?php } ?>
                    >
                    <?php if($session->id_company != 6): ?>
                    <li class="nav-item <?php if($carga_modulo->modulo == 'bonificacion'){ ?> active open <? } ?>">
                        <a href="bonificacion" class="nav-link">
                            <i class="icon-user"></i>
                            <span class="title">Bonificación</span>
                        </a>
                    </li>
                    <?php endif; ?>
                    <?php
                        if($session->id_company != 2 && $session->id_company != 10) {
                    ?>
                        <li class="nav-item <?php if($carga_modulo->modulo == 'asistencia'){ ?> active open <? } ?>">
                            <a href="asistencia" class="nav-link">
                                <i class="icon-user"></i>
                                <span class="title">Asistencia</span>
                            </a>
                        </li>
                    <?php } ?>
                    <?php
                        if($session->id_company != 10) {
                    ?>
                    <li class="nav-item <?php if($carga_modulo->modulo == 'tthhPersonal' || $carga_modulo->modulo == 'tthhFPersonal'){ ?> active open <? } ?>">
                        <a href="tthhPersonal" class="nav-link">
                            <i class="icon-user"></i>
                            <span class="title">Personal</span>
                        </a>
                    </li>
                    <?php if($session->id_company == 2) : ?>
                    <li class="nav-item <?php if($carga_modulo->modulo == 'tthhExpediente'){ ?> active open <? } ?>">
                        <a href="tthhExpediente" class="nav-link">
                            <i class="icon-user"></i>
                            <span class="title">Expediente</span>
                        </a>
                    </li>
                    <?php endif; ?>
                    <?php if($session->id_company == 2) : ?>
                    <li class="nav-item <?php if($carga_modulo->modulo == 'tthhEnfunde'){ ?> active open <? } ?>">
                        <a href="tthhEnfunde" class="nav-link">
                            <i class="fa fa-bookmark"></i>
                            <span class="title">Enfunde</span>
                        </a>
                    </li>
                    <?php endif; ?>
                    <li class="nav-item <?php if($carga_modulo->modulo == 'revisionAsistenciarebo'){ ?> active open <? } ?>">
                        <a href="javascript:;" class="nav-link">
                            <i class="icon-user"></i>
                            <span class="title">Revisión</span>
                            <span class="arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li class="nav-item">
                                <a href="revisionAsistencia" class="nav-link">
                                    <span class="title">Asistencia</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <?php } ?>
                    </ul>
                </li>
            <?php endif;?>
            <?php if($session->settings->lancofruit == 'Activo'):?>
                <li class="nav-item <?php if(in_array($carga_modulo->modulo, $lancofruit)){ ?> open active <? } ?>">
                    <a href="lancofruit" class="nav-link nav-toggle">
                        <i class="icon-bar-chart"></i>
                        <span class="title">Lancofruit</span>
                        <span class="arrow <?php if(in_array($carga_modulo->modulo, $lancofruit)){ ?> open active <? } ?>"></span>
                    </a>
                    <ul class="sub-menu">
                        <?php if($session->logged != 39 && (!isset($session->menu_modules) || $session->menu_modules["geolocalizacion"] == 'Activo')): ?>
                        <li class="nav-item <?php if($carga_modulo->modulo == 'revisionLancofruit'){ ?> active open <? } ?>">
                            <a href="revisionLancofruit" class="nav-link">
                                <i class="fas fa-map-marker-alt"></i>
                                <span class="title">Geolocalización</span>
                            </a>
                        </li>
                        <?php endif; ?>
                        <?php if($session->logged == 39 && $session->menu_modules["geolocalizacion"] == 'Activo'): ?>
                        <li class="nav-item <?php if($carga_modulo->modulo == 'revisionLancofruitOnlyExcel'){ ?> active open <? } ?>">
                            <a href="revisionLancofruitOnlyExcel" class="nav-link">
                                <i class="fas fa-map-marker-alt"></i>
                                <span class="title">Geolocalización</span>
                            </a>
                        </li>
                        <?php endif; ?>
                        <?php if($session->logged != 2) :?>
                            <?php if($session->logged != 2 && !isset($session->menu_modules) || $session->menu_modules["rastreo"] == 'Activo'): ?>
                                <li class="nav-item <?php if($carga_modulo->modulo == 'rastreoLancofruit'){ ?> active open <? } ?>">
                                    <a href="rastreoLancofruit" class="nav-link">
                                        <i class="fas fa-dollar-sign"></i>
                                        <span class="title">Ventas</span>
                                    </a>
                                </li>
                            <?php endif; ?>
                        <?php endif;?>
                        <?php if(in_array($session->id_company, [2]) && $session->logged != 36 && $session->logged != 49 && $session->logged != 50 && $session->logged != 51):?>
                            <li class="nav-item" <?= in_array($carga_modulo->modulo, $lancofruit_ventas) ? 'active open' : '' ?>>
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="fas fa-area-chart"></i>
                                    <span class="title">Reporte (Ventas)</span>
                                    <span class="selected"></span>
                                    <span class="arrow <?= in_array($carga_modulo->modulo, $lancofruit_ventas) ? 'active open' : '' ?>"></span>
                                </a>
                                <ul class="sub-menu" style="display : <?= in_array($carga_modulo->modulo, $lancofruit_ventas) ? 'block' : 'none' ?>">
                                    <?php if(in_array($session->id_company, [2])):?>
                                        <li class="nav-item <?php if($carga_modulo->modulo == 'lancofruitVentasDia'){ ?> active open <? } ?>">
                                            <a href="lancofruitVentasDia" class="nav-link">
                                                <i class="fas fa-pie-chart"></i>
                                                <span class="title">Día</span>
                                            </a>
                                        </li>
                                    <?php endif;?>
                                    <?php if(in_array($session->id_company, [2])):?>
                                    <li class="nav-item <?php if($carga_modulo->modulo == 'lancofruitVentasComparativo'){ ?> active open <? } ?>">
                                        <a href="lancofruitVentasComparativo" class="nav-link">
                                            <i class="fas fa-bar-chart"></i>
                                            <span class="title">Comparativo</span>
                                        </a>
                                    </li>
                                    <?php endif;?>
                                    <?php if(in_array($session->id_company, [2])):?>
                                    <li class="nav-item <?php if($carga_modulo->modulo == 'lancofruitVentasTendencia'){ ?> active open <? } ?>">
                                        <a href="lancofruitVentasTendencia" class="nav-link">
                                            <i class="fas fa-line-chart"></i>
                                            <span class="title">Tendencia</span>
                                        </a>
                                    </li>
                                    <?php endif;?>
                                </ul>
                            </li>
                        <?php endif;?>
                        <?php if(!isset($session->menu_modules) || $session->menu_modules["cercaLancofruit"] == 'Activo'): ?>
                            <!-- <li class="nav-item <?php if($carga_modulo->modulo == 'cercaLancofruit'){ ?> active open <? } ?>">
                                <a href="cercaLancofruit" class="nav-link">
                                    <i class="fas fa-map-marked"></i>
                                    <span class="title">Cerca</span>
                                </a>
                            </li> -->
                        <?php
                            endif;
                            if(!isset($session->menu_modules) || $session->menu_modules["ventas"] == 'Activo'): ?>
                        <!-- <li class="nav-item <?php if($carga_modulo->modulo == 'ventas'){ ?> active open <? } ?>">
                            <a href="ventas" class="nav-link">
                                <i class="icon-user"></i>
                                <span class="title">Ventas</span>
                            </a>
                        </li> -->
                        <?php endif;?>
                        <?php if(in_array($session->id_company, [2]) && $session->logged == 2):?>
                            <li class="nav-item" <?= in_array($carga_modulo->modulo, $perchas) ? 'active open' : '' ?>>
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="fas fa-area-chart"></i>
                                    <span class="title">Perchas</span>
                                    <span class="selected"></span>
                                    <span class="arrow <?= in_array($carga_modulo->modulo, $perchas) ? 'active open' : '' ?>"></span>
                                </a>
                                <ul class="sub-menu" style="display : <?= in_array($carga_modulo->modulo, $perchas) ? 'block' : 'none' ?>">
                                    <?php if(in_array($session->id_company, [2]) && $session->logged == 2):?>
                                        <li class="nav-item <?php if($carga_modulo->modulo == 'perchasDia'){ ?> active open <? } ?>">
                                            <a href="perchasDia" class="nav-link">
                                                <i class="fas fa-pie-chart"></i>
                                                <span class="title">Día</span>
                                            </a>
                                        </li>
                                    <?php endif;?>
                                    <?php if(in_array($session->id_company, [2]) && $session->logged == 2):?>
                                        <li class="nav-item <?php if($carga_modulo->modulo == 'perchasComparacion'){ ?> active open <? } ?>">
                                            <a href="perchasComparacion" class="nav-link">
                                                <i class="fas fa-pie-chart"></i>
                                                <span class="title">Comparativo</span>
                                            </a>
                                        </li>
                                    <?php endif;?>
                                    <?php if(in_array($session->id_company, [2]) && $session->logged == 2):?>
                                        <li class="nav-item <?php if($carga_modulo->modulo == 'perchasTendencia'){ ?> active open <? } ?>">
                                            <a href="perchasTendencia" class="nav-link">
                                                <i class="fas fa-pie-chart"></i>
                                                <span class="title">Tendencia</span>
                                            </a>
                                        </li>
                                    <?php endif;?>
                                    <?php if(in_array($session->id_company, [2])):?>
                                    <li class="nav-item <?php if($carga_modulo->modulo == 'perchasBD'){ ?> active open <? } ?>">
                                        <a href="perchasBD" class="nav-link">
                                            <i class="fas fa-bar-chart"></i>
                                            <span class="title">Base de datos</span>
                                        </a>
                                    </li>
                                    <?php endif;?>
                                </ul>
                            </li>
                        <?php endif;?>
                        <?php if($session->logged != 2 && $session->logged != 41 && $session->logged != 43) :?>
                            <?php if(isset($session->menu_modules) || $session->menu_modules["perchas"] == 'Activo'): ?>
                                <li class="nav-item <?php if($carga_modulo->modulo == 'perchasDia'){ ?> active open <? } ?>">
                                    <a href="perchasDia" class="nav-link">
                                        <i class="icon-user"></i>
                                        <span class="title">Perchas Día</span>
                                    </a>
                                </li>
                                <li class="nav-item <?php if($carga_modulo->modulo == 'perchasComparacion'){ ?> active open <? } ?>">
                                    <a href="perchasComparacion" class="nav-link">
                                        <i class="icon-user"></i>
                                        <span class="title">Perchas Comparativo</span>
                                    </a>
                                </li>
                                <li class="nav-item <?php if($carga_modulo->modulo == 'perchasTendencia'){ ?> active open <? } ?>">
                                    <a href="perchasTendencia" class="nav-link">
                                        <i class="icon-user"></i>
                                        <span class="title">Perchas Tendencia</span>
                                    </a>
                                </li>
                            <?php endif; ?>
                        <?php endif;?>
                        <?php if(!isset($session->menu_modules) || $session->menu_modules["temperatura"] == 'Activo'): ?>
                            <li class="nav-item <?php if($carga_modulo->modulo == 'lancofruitTemperatura'){ ?> active open <? } ?>">
                                <a href="lancofruitTemperatura" class="nav-link">
                                    <i class="fas fa-thermometer-empty"></i>
                                    <span class="title">Temperatura</span>
                                </a>
                            </li>
                        <?php endif; ?>
                        <?php if(!isset($session->menu_modules) || $session->menu_modules["importar"] == 'Activo'): ?>
                        <li class="nav-item <?php if($carga_modulo->modulo == 'importarInformacion'){ ?> active open <? } ?>">
                            <a href="importarInformacion" class="nav-link">
                                <i class="fas fa-file-import"></i>
                                <span class="title">Importar información</span>
                            </a>
                        </li>
                        <?php endif; ?>
                        <?php if(!isset($session->menu_modules) || $session->menu_modules["configLancofruit"] == 'Activo'): ?>
                        <li class="nav-item <?php if($carga_modulo->modulo == 'configLancofruit'){ ?> active open <? } ?>">
                            <a href="configLancofruit" class="nav-link">
                                <i class="icon-settings"></i>
                                <span class="title">Configuración</span>
                            </a>
                        </li>
                        <?php endif; ?>
                    </ul>
                </li>
            <?php endif;?>
            <?php if($session->settings->membresias == 'Activo'):?>
                <li class="nav-item  <?php if($carga_modulo->modulo == 'membresias'){ ?> active start <? } ?>">
                    <a href="membresias" class="nav-link nav-toggle">
                        <i class="icon-user"></i>
                        <span class="title">Membresías</span>
                        <span class="selected"></span>
                    </a>
                </li>
            <?php endif; ?>
            <?php if($session->settings->clima == 'Activo'):?>
                <li class="nav-item <?= (in_array($carga_modulo->modulo, $clima) ? 'open active' : '') ?>">
                    <a href="clima" class="nav-link">
                        <i class="fa fa-cloud"></i>
                        <span class="title">Clima</span>
                        <i class="arrow"></i>
                    </a>
                    <ul class="sub-menu">
                        <li class="nav-item <?= ($carga_modulo->modulo == 'climaDiario' ? 'active' : '') ?>">
                            <a href="climaDiario" class="nav-link">
                                <i class="fa fa-cloud"></i>
                                <span class="title">Diario</span>
                            </a>
                        </li>
                        <li class="nav-item <?= ($carga_modulo->modulo == 'clima' ? 'active' : '') ?>">
                            <a href="clima" class="nav-link">
                                <i class="fa fa-cloud"></i>
                                <span class="title">Historico</span>
                            </a>
                        </li>
                    </ul>
                </li>
            <?php endif; ?>
            <?php if($session->settings->configuracion == 'Activo'):?>
                <li class="nav-item <?= (in_array($carga_modulo->modulo, $configuracion) ? 'open active' : '') ?>">
                    <a href="javascript:;" class="nav-link">
                        <i class="icon-settings"></i>
                        <span class="title">Configuración</span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <li class="nav-item <?= $carga_modulo->modulo == 'configLotes' ? 'active' : '' ?>">
                            <a href="configLotes" class="nav-link nav-toggle">
                                <span class="title">Lotes</span>
                            </a>
                        </li>
                        <li class="nav-item <?php if($carga_modulo->modulo == 'configPlantas'){ ?> active open <? } ?>">
                            <a href="configPlantas" class="nav-link">
                                <span class="title">Plantas/Ha</span>
                            </a>
                        </li>
                        <li class="nav-item hide">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <span class="title">Fincas</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item ">
                                    <a href="fincas" class="nav-link "> Listado Fincas </a>
                                </li>
                                <li class="nav-item ">
                                    <a href="finca" class="nav-link "> Nueva Finca </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item hide">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <span class="title">Empleado</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item ">
                                    <a href="empleados" class="nav-link "> Listado Emplados </a>
                                </li>
                                <li class="nav-item ">
                                    <a href="empleado" class="nav-link "> Nuevo Empleado </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item hide">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <span class="title">Auditores</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item ">
                                    <a href="auditores" class="nav-link "> Listado Auditores</a>
                                </li>
                                <li class="nav-item ">
                                    <a href="auditor" class="nav-link "> Nuevo Auditore </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item hide">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <span class="title">Responsables</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item ">
                                    <a href="responsables" class="nav-link "> Listado Responsables </a>
                                </li>
                                <li class="nav-item ">
                                    <a href="responsable" class="nav-link "> Nuevo Responsable </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item hide">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <span class="title">Calidad</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item">
                                    <a href="javascript:;" class="nav-link nav-toggle">
                                        <span class="title">Marcas</span>
                                        <span class="arrow"></span>
                                    </a>
                                    <ul class="sub-menu">
                                        <li class="nav-item ">
                                            <a href="configMarcas" class="nav-link "> Listado de Marcas </a>
                                        </li>
                                        <li class="nav-item ">
                                            <a href="editMarca" class="nav-link "> Nueva Marca </a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="nav-item">
                                    <a href="javascript:;" class="nav-link nav-toggle">
                                        <span class="title">Categorias</span>
                                        <span class="arrow"></span>
                                    </a>
                                    <ul class="sub-menu">
                                        <li class="nav-item ">
                                            <a href="configCategorias" class="nav-link "> Listado Categorias </a>
                                        </li>
                                        <li class="nav-item ">
                                            <a href="editCategoria" class="nav-link "> Nueva Categoria </a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="nav-item">
                                    <a href="javascript:;" class="nav-link nav-toggle">
                                        <span class="title">Defectos</span>
                                        <span class="arrow"></span>
                                    </a>
                                    <ul class="sub-menu">
                                        <li class="nav-item ">
                                            <a href="configDefectos" class="nav-link "> Listado Defectos </a>
                                        </li>
                                        <li class="nav-item ">
                                            <a href="editDefecto" class="nav-link "> Nuevo Defecto </a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="nav-item">
                                    <a href="javascript:;" class="nav-link nav-toggle">
                                        <span class="title">Exportadores</span>
                                        <span class="arrow"></span>
                                    </a>
                                    <ul class="sub-menu">
                                        <li class="nav-item ">
                                            <a href="configExportadores" class="nav-link "> Listado Exportadores </a>
                                        </li>
                                        <li class="nav-item ">
                                            <a href="editExportador" class="nav-link "> Nuevo Exportador </a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="nav-item">
                                    <a href="javascript:;" class="nav-link nav-toggle">
                                        <span class="title">Destinos</span>
                                        <span class="arrow"></span>
                                    </a>
                                    <ul class="sub-menu">
                                        <li class="nav-item ">
                                            <a href="configDestinos" class="nav-link "> Listado Destinos </a>
                                        </li>
                                        <li class="nav-item ">
                                            <a href="editDestinos" class="nav-link "> Nuevo Destino </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
            <?php endif;?>
            <?php if($session->settings->usuario == 'Activo'):?>
                <li class="nav-item <?= (in_array($carga_modulo->modulo, $usuario) ? 'open active' : '') ?>">
                    <a href="javascript:;" class="nav-link">
                        <i class="icon-settings"></i>
                        <span class="title">Usuario</span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <li class="nav-item <?= $carga_modulo->modulo == 'tutoriales' ? 'active' : '' ?>">
                            <a href="tutoriales" class="nav-link nav-toggle">
                                <span class="title">Tutoriales</span>
                            </a>
                        </li>
                    </ul>
                </li>
            <?php endif;?>
        </ul>
        <?php endif;?>

        <?php if($session->cultivos->cacao == 'Activo'):?>
        <ul class="page-sidebar-menu CACAO page-header-fixed page-sidebar-menu-compact" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
            <li class="nav-item 
                <?php 
                    if(in_array($carga_modulo->modulo, $cacao) || in_array($carga_modulo->modulo, $cacao_produccion) || in_array($carga_modulo->modulo, $cacao_config)){ ?> open active <? } 
                ?>">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-bar-chart"></i>
                    <span class="title">Cacao</span>
                    <span class="arrow <?php if(in_array($carga_modulo->modulo, $cacao)){ ?> open active <? } ?>"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item <?php if($carga_modulo->modulo == 'analisisSensorial'){ ?> active open <? } ?>">
                        <a href="analisisSensorial" class="nav-link">
                            <i class="fa fa-pie-chart"></i>
                            <span class="title">Análisis Sensorial</span>
                        </a>
                    </li>
                    <li class="nav-item <?php if(in_array($carga_modulo->modulo, $cacao_produccion)){ ?> open active <? } ?>">
                        <a href="javascript:;" class="nav-link nav-toggle">
                            <i class="fa fa-line-chart"></i>
                            <span class="title">Produccón</span>
                            <span class="arrow <?php if(in_array($carga_modulo->modulo, $cacao_produccion)){ ?> open <? } ?>"></span>
                        </a>
                        <ul class="sub-menu">
                            <li class="nav-item <?php if($carga_modulo->modulo == 'cosechaLotes'){ ?> active open <? } ?>">
                                <a href="cosechaLotes" class="nav-link">
                                    <span class="title">Cosecha por Lotes</span>
                                </a>
                            </li>
                            <li class="nav-item <?php if(in_array($carga_modulo->modulo, $cacao_produccion)){ ?> open active <? } ?>">
                                <a href="javascript:;" class="nav-link">
                                    <span class="title">Reporte Cosecha</span>
                                    <span class="arrow <?php if(in_array($carga_modulo->modulo, $cacao_produccion)){ ?> open <? } ?>"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item <?php if($carga_modulo->modulo == 'cacaoReporteCosecha'){ ?> active open <? } ?>">
                                        <a href="cacaoReporteCosecha" class="nav-link">
                                            <span class="title">Semanal</span>
                                        </a>
                                    </li>
                                    <li class="nav-item <?php if($carga_modulo->modulo == 'cacaoReporteCosechaPeriodal'){ ?> active open <? } ?>">
                                        <a href="cacaoReporteCosechaPeriodal" class="nav-link">
                                            <span class="title">Periodal</span>
                                        </a>
                                    </li>
                                    <li class="nav-item <?php if($carga_modulo->modulo == 'cacaoReporteCosechaSector'){ ?> active open <? } ?>">
                                        <a href="cacaoReporteCosechaSector" class="nav-link">
                                            <span class="title">Por Sector</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item <?php if(in_array($carga_modulo->modulo, $cacao)){ ?> open active <? } ?>">
                        <a href="javascript:;" class="nav-link nav-toggle">
                            <i class="fa fa-database"></i>
                            <span class="title">Revisión BD</span>
                            <span class="arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li class="nav-item <?php if($carga_modulo->modulo == 'consolidadoCosecha'){ ?> active open <? } ?>">
                                <a href="consolidadoCosecha" class="nav-link">
                                    <span class="title">Consolidado de Cosecha</span>
                                </a>
                            </li>
                            <li class="nav-item <?php if($carga_modulo->modulo == 'asignacionMallas'){ ?> active open <? } ?>">
                                <a href="asignacionMallas" class="nav-link">
                                    <span class="title">BD - Asignación Mallas</span>
                                </a>
                            </li>
                            <li class="nav-item <?php if($carga_modulo->modulo == 'formularioAsignacionArea'){ ?> active open <? } ?>">
                                <a href="formularioAsignacionArea" class="nav-link">
                                    <span class="title">BD - Asignacion Área</span>
                                </a>
                            </li>
                            <li class="nav-item <?php if($carga_modulo->modulo == 'balanzaCosecha'){ ?> active open <? } ?>">
                                <a href="balanzaCosecha" class="nav-link">
                                    <span class="title">BD - Balanza de Cosecha</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item <?php if(in_array($carga_modulo->modulo, $cacao_config)){ ?> open active <? } ?>">
                        <a href="javascript:;" class="nav-link nav-toggle">
                            <i class="fa fa-cog"></i>
                            <span class="title">Configuración</span>
                            <span class="arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li class="nav-item <?php if($carga_modulo->modulo == 'cacaoConfigLotes'){ ?> active open <? } ?>">
                                <a href="cacaoConfigLotes" class="nav-link">
                                    <span class="title">Lotes</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li class="nav-item <?php if(in_array($carga_modulo->modulo, $cacao_tthh)){ ?> open active <? } ?>">
                <a href="javascript:;" class="nav-link ">
                    <i class="icon-user"></i>
                    <span class="title">TTHH</span>
                    <span class="arrow <?php if(in_array($carga_modulo->modulo, $cacao_tthh)){ ?> open active <? } ?>"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item <?php if($carga_modulo->modulo == 'cacaotthhPersonal' || $carga_modulo->modulo == 'cacaotthhFPersonal'){ ?> active open <? } ?>">
                        <a href="cacaotthhPersonal" class="nav-link">
                            <i class="icon-user"></i>
                            <span class="title">Personal</span>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
        <?php endif;?>

        <?php if($session->cultivos->palma == 'Activo'):?>
        <ul class="page-sidebar-menu PALMA page-header-fixed page-sidebar-menu-compact" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
            <li class="nav-item 
                <?php 
                    if(in_array($carga_modulo->modulo, $palma)){ ?> open active <? } 
                ?>">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-bar-chart"></i>
                    <span class="title">Labores Agrciolas</span>
                    <span class="arrow <?php if(in_array($carga_modulo->modulo, $palma)){ ?> open active <? } ?>"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item <?php if($carga_modulo->modulo == 'palmaLaboresAgricolasDia'){ ?> active open <? } ?>">
                        <a href="palmaLaboresAgricolasDia" class="nav-link">
                            <i class="fa fa-pie-chart"></i>
                            <span class="title">Reporte Día</span>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
        <?php endif;?>

        <?php if($session->settings->monitor == 'Activo'):?>
        <ul class="page-sidebar-menu MONITOR page-header-fixed page-sidebar-menu-compact" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
            <li class="nav-item 
                <?php 
                    if(in_array($carga_modulo->modulo, $monitor_prontoforms)){ ?> open active <? } 
                ?>">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-bar-chart"></i>
                    <span class="title">Prontoforms</span>
                    <span class="arrow <?php if(in_array($carga_modulo->modulo, $monitor_prontoforms)){ ?> open active <? } ?>"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item <?php if($carga_modulo->modulo == 'monitorProntoforms'){ ?> active open <? } ?>">
                        <a href="monitorProntoforms" class="nav-link">
                            <i class="fa fa-pie-chart"></i>
                            <span class="title">Reporte Día</span>
                        </a>
                    </li>
                </ul>
            </li>

            <li class="nav-item 
                <?php 
                    if(in_array($carga_modulo->modulo, $monitor_balanzas)){ ?> open active <? } 
                ?>">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-bar-chart"></i>
                    <span class="title">Balanzas</span>
                    <span class="arrow <?php if(in_array($carga_modulo->modulo, $monitor_balanzas)){ ?> open active <? } ?>"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item <?php if($carga_modulo->modulo == 'monitorBalanzas'){ ?> active open <? } ?>">
                        <a href="monitorBalanzas" class="nav-link">
                            <i class="fa fa-pie-chart"></i>
                            <span class="title">Monitor</span>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
        <?php endif;?>
        <!-- END SIDEBAR MENU -->
    </div>
    <!-- END SIDEBAR -->
    <div id="primary-page-sidebar-footer" class="page-sidebar-footer">
        <select class="form-control" id="select-cultivo" style="background: #c7c7c7 !important;">
            <?php if($session->cultivos->banano == 'Activo'):?>
            <option value="BANANO">BANANO</option>
            <?php endif; ?>
            <?php if($session->cultivos->cacao == 'Activo'):?>
            <option value="CACAO">CACAO</option>
            <?php endif; ?>
            <?php if($session->cultivos->palma == 'Activo'):?>
            <option value="PALMA">PALMA</option>
            <?php endif; ?>
            <?php if($session->settings->monitor == 'Activo'):?>
            <option value="MONITOR">MONITOR</option>
            <?php endif; ?>
        </select>
    </div>
</div>
<!-- END SIDEBAR -->

<script type="text/javascript">
    moduleSelected = '<?= strtoupper($session->moduleSystem) ?>'
    document.getElementById('primary-page-sidebar-footer').style.display = 'none'
    reloadSubMenu = () => {
        setTimeout(() => {
            let menus = document.getElementsByClassName('page-sidebar-menu')
            for(var i = 0; i < menus.length; i++){
                let cont = menus[i].classList.contains(moduleSelected)
                if(cont){
                    menus[i].parentElement.classList.remove('hide')
                    menus[i].classList.remove('hide')
                }else{
                    menus[i].parentElement.classList.add('hide')
                    menus[i].classList.add('hide')
                }
            }
        }, 100)
    }
    window.addEventListener('resize', () => {
        reloadSubMenu()
    }, false)
    document.getElementById('menu-toggler').addEventListener('click', function(){
        setTimeout(() => {
            var width = $("#primary-page-sidebar").width()
            $("#primary-page-sidebar-footer").width(width)
        }, 100)

        reloadSubMenu()
    })
    $(document).ready(() => {
        var el = $("#select-cultivo")
        el.val(moduleSelected)
        el.change(function(){
            moduleSelected = el.val()
            reloadSubMenu()
        })

        var width = $("#primary-page-sidebar").width()
        document.getElementById('primary-page-sidebar-footer').style.display = ''
        document.getElementById('primary-page-sidebar-footer').style.bottom = 0
        document.getElementById('primary-page-sidebar-footer').style.width = width
        reloadSubMenu()
        
        var $merma = $('.merma');
        var LARA_COMPANY = 3;
        var RUFCORP_COMPANY = 20;

        $merma.click(function (e) {
            try {
                var id_company = $merma.data('id_company');
                console.info('Id de la compania ', id_company)
                if(id_company == LARA_COMPANY) {
                    e.preventDefault();
                    window.location = 'http://agroiq-lara.procesosiq.com/';
                } else if(id_company == RUFCORP_COMPANY) {
                    e.preventDefault();
                    window.location = 'http://agroiq-ruforcorp.procesosiq.com/';
                }
            } catch(e) {
                console.error(e);
            }
        });
    })
</script>