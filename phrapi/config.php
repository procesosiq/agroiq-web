<?php defined("PHRAPI") or die("Direct access not allowed!");

$config = [
	'gmt' => '-05:00',
	'locale' => 'es_MX',
	'php_locale' => 'es_MX',
	'timezone' => 'America/Mexico_City',
	'offline' => false,
	'servers' => [
		'agroaudit.procesos-iq.com' => [
			'url' => 'http://agroaudit.procesos-iq.com/',
			// 'urlssl' => 'https://YOUR-DOMAIN/YOUR-INSTALL-PATH/',
			'agent_user' => 'global',
			'sigat' => 'global',
			'agroaudit' => 'global',
			'merma' => 'global',
			'calidad' => 'global',
			'produccion' => 'global',
			'db' => [
				[
					'host' => 'localhost',
					'name' => 'procesosiq',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'marcel' => [
					'host' => 'localhost',
					'name' => 'agroaudit_marcel',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'mario' => [
					'host' => 'localhost',
					'name' => 'agroaudit_mario',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'marlon' => [
					'host' => 'localhost',
					'name' => 'agroaudit_marlon',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'bonita' => [
					'host' => 'localhost',
					'name' => 'agroaudit_bonita',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'quintana' => [
					'host' => 'localhost',
					'name' => 'agroaudit_quintana',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'marun' => [
					'host' => 'localhost',
					'name' => 'agroaudit_marun',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
                ],
                'cacao_marun' => [
					'host' => 'localhost',
					'name' => 'agroaudit_cacao_marun',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'demo' => [
					'host' => 'localhost',
					'name' => 'agroaudit_demo',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'francisco' => [
					'host' => 'localhost',
					'name' => 'agroaudit_francisco',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'dole' => [
					'host' => 'localhost',
					'name' => 'agroaudit_dole',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'sigat' => [
					'host' => 'localhost',
					'name' => 'sigat',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'sigat_marlon' => [
					'host' => 'localhost',
					'name' => 'sigat_marlon',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'sumifru' => [
					'host' => 'localhost',
					'name' => 'agroaudit_sumifru',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'arregui' => [
					'host' => 'localhost',
					'name' => 'agroaudit_arregui',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				]
			]
		],
		'app.procesos-iq.com' => [
			'url' => 'http://app.procesos-iq.com/',
			// 'urlssl' => 'https://YOUR-DOMAIN/YOUR-INSTALL-PATH/',
			'agent_user' => 'global',
			'sigat' => 'global',
			'agroaudit' => 'global',
			'merma' => 'global',
			'calidad' => 'global',
			'produccion' => 'global',
			'db' => [
				[
					'host' => 'localhost',
					'name' => 'procesosiq',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'dagcoin' => [
					'host' => 'localhost',
					'name' => 'dagcoin',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'marcel' => [
					'host' => 'localhost',
					'name' => 'agroaudit_marcel',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'mario' => [
					'host' => 'localhost',
					'name' => 'agroaudit_mario',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'victor' => [
					'host' => 'localhost',
					'name' => 'agroaudit_victor',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'marlon' => [
					'host' => 'localhost',
					// 'name' => 'agroaudit_bonita',
					'name' => 'agroaudit_marlon',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'bonita' => [
					'host' => 'localhost',
					'name' => 'agroaudit_bonita',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'quintana' => [
					'host' => 'localhost',
					'name' => 'agroaudit_quintana',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'marun' => [
					'host' => 'localhost',
					'name' => 'agroaudit_marun',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
                ],
                'cacao_marun' => [
					'host' => 'localhost',
					'name' => 'agroaudit_cacao_marun',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'demo' => [
					'host' => 'localhost',
					'name' => 'agroaudit_demo',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'francisco' => [
					'host' => 'localhost',
					'name' => 'agroaudit_francisco',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'dole' => [
					'host' => 'localhost',
					'name' => 'agroaudit_dole',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'sigat' => [
					'host' => 'localhost',
					'name' => 'sigat',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'sigat_marlon' => [
					'host' => 'localhost',
					'name' => 'sigat_marlon',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'agroarriba' => [
					'host' => 'localhost',
					'name' => 'agroarriba',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'agroarriba_dev' => [
					'host' => 'localhost',
					'name' => 'agroarriba_dev',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'natural_habitats' => [
					'host' => 'localhost',
					'name' => 'natural_habitats',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
                ],
				'reiset' => [
                    'host' => 'localhost',
					'name' => 'agroaudit_reiset',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'sumifru' => [
					'host' => 'localhost',
					'name' => 'agroaudit_sumifru',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
                'palmar' => [
					'host' => 'localhost',
					'name' => 'agroaudit_palmar',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
                ],
                'palosanto' => [
                    'host' => 'localhost',
					'name' => 'saenz',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
                ],
                'climat' => [
                    'host' => 'localhost',
					'name' => 'climat',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'clean_star' => [
                    'host' => 'localhost',
					'name' => 'clean_star',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'arregui' => [
					'host' => 'localhost',
					'name' => 'agroaudit_arregui',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'clementina' => [
					'host' => 'localhost',
					'name' => 'agroaudit_clementina',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'orodelti' => [
					'host' => 'localhost',
					'name' => 'agroaudit_orodelti',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'agroaereo' => [
					'host' => 'localhost',
					'name' => 'agroaudit_agroaereo',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'digital' => [
					'host' => 'procesosiq.com',
					'name' => 'ticket_programador',
					'user' => 'readers',
					'pass' => 'oQ4Ro19xTd6d'
				],
				'agroban' => [
					'host' => 'localhost',
					'name' => 'agroaudit_agroban',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'eugenia' => [
					'host' => 'localhost',
					'name' => 'agroaudit_eugenia',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'allweights' => [
					'host' => 'hcdamariamaria.com',
					'name' => 'bdbalanza',
					'user' => 'procesosiq',
					'pass' => 'Pr0c3s0s1Q@2018'
				],
				'rufcorp' => [
					'host' => 'localhost',
					'name' => 'agroaudit_rufcorp',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'mejia' => [
					'host' => 'localhost',
					'name' => 'agroaudit_mejia',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				]
			]
		],
		'app-web01.procesosiq.com' => [
			'url' => 'http://app-web01.procesosiq.com/',
			// 'urlssl' => 'https://YOUR-DOMAIN/YOUR-INSTALL-PATH/',
			'agent_user' => 'global',
			'sigat' => 'global',
			'agroaudit' => 'global',
			'merma' => 'global',
			'calidad' => 'global',
			'produccion' => 'global',
			'db' => [
				[
					'host' => 'procesos-iq.com',
					'name' => 'procesosiq',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'dagcoin' => [
					'host' => 'procesos-iq.com',
					'name' => 'dagcoin',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'marcel' => [
					'host' => 'procesos-iq.com',
					'name' => 'agroaudit_marcel',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'mario' => [
					'host' => 'procesos-iq.com',
					'name' => 'agroaudit_mario',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'victor' => [
					'host' => 'procesos-iq.com',
					'name' => 'agroaudit_victor',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'marlon' => [
					'host' => 'procesos-iq.com',
					// 'name' => 'agroaudit_bonita',
					'name' => 'agroaudit_marlon',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'bonita' => [
					'host' => 'procesos-iq.com',
					'name' => 'agroaudit_bonita',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'quintana' => [
					'host' => 'procesos-iq.com',
					'name' => 'agroaudit_quintana',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'marun' => [
					'host' => 'procesos-iq.com',
					'name' => 'agroaudit_marun',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
                ],
                'cacao_marun' => [
					'host' => 'procesos-iq.com',
					'name' => 'agroaudit_cacao_marun',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'demo' => [
					'host' => 'procesos-iq.com',
					'name' => 'agroaudit_demo',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'francisco' => [
					'host' => 'procesos-iq.com',
					'name' => 'agroaudit_francisco',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'dole' => [
					'host' => 'procesos-iq.com',
					'name' => 'agroaudit_dole',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'sigat' => [
					'host' => 'procesos-iq.com',
					'name' => 'sigat',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'sigat_marlon' => [
					'host' => 'procesos-iq.com',
					'name' => 'sigat_marlon',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'agroarriba' => [
					'host' => 'procesos-iq.com',
					'name' => 'agroarriba',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'agroarriba_dev' => [
					'host' => 'procesos-iq.com',
					'name' => 'agroarriba_dev',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'natural_habitats' => [
					'host' => 'procesos-iq.com',
					'name' => 'natural_habitats',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
                ],
				'reiset' => [
                    'host' => 'procesos-iq.com',
					'name' => 'agroaudit_reiset',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'sumifru' => [
					'host' => 'procesos-iq.com',
					'name' => 'agroaudit_sumifru',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
                'palmar' => [
					'host' => 'procesos-iq.com',
					'name' => 'agroaudit_palmar',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
                ],
                'palosanto' => [
                    'host' => 'procesos-iq.com',
					'name' => 'saenz',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
                ],
                'climat' => [
                    'host' => 'procesos-iq.com',
					'name' => 'climat',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'clean_star' => [
                    'host' => 'procesos-iq.com',
					'name' => 'clean_star',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'arregui' => [
					'host' => 'procesos-iq.com',
					'name' => 'agroaudit_arregui',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'clementina' => [
					'host' => 'procesos-iq.com',
					'name' => 'agroaudit_clementina',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'orodelti' => [
					'host' => 'procesos-iq.com',
					'name' => 'agroaudit_orodelti',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'agroaereo' => [
					'host' => 'procesos-iq.com',
					'name' => 'agroaudit_agroaereo',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'digital' => [
					'host' => 'procesosiq.com',
					'name' => 'ticket_programador',
					'user' => 'readers',
					'pass' => 'oQ4Ro19xTd6d'
				],
				'agroban' => [
					'host' => 'procesos-iq.com',
					'name' => 'agroaudit_agroban',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'eugenia' => [
					'host' => 'procesos-iq.com',
					'name' => 'agroaudit_eugenia',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'allweights' => [
					'host' => 'hcdamariamaria.com',
					'name' => 'bdbalanza',
					'user' => 'procesosiq',
					'pass' => 'Pr0c3s0s1Q@2018'
				]
			]
		],
		'app-dev01.procesosiq.com' => [
			'url' => 'http://app-dev01.procesosiq.com/',
			// 'urlssl' => 'https://YOUR-DOMAIN/YOUR-INSTALL-PATH/',
			'agent_user' => 'global',
			'sigat' => 'global',
			'agroaudit' => 'global',
			'merma' => 'global',
			'calidad' => 'global',
			'produccion' => 'global',
			'db' => [
				[
					'host' => '142.93.7.91',
					'name' => 'procesosiq',
					'user' => 'backups',
					'pass' => 'vgaN6wQT48UgMZCG'
				],
				'dagcoin' => [
					'host' => '142.93.7.91',
					'name' => 'dagcoin',
					'user' => 'backups',
					'pass' => 'vgaN6wQT48UgMZCG'
				],
				'marcel' => [
					'host' => '142.93.7.91',
					'name' => 'agroaudit_marcel',
					'user' => 'backups',
					'pass' => 'vgaN6wQT48UgMZCG'
				],
				'mario' => [
					'host' => '142.93.7.91',
					'name' => 'agroaudit_mario',
					'user' => 'backups',
					'pass' => 'vgaN6wQT48UgMZCG'
				],
				'marlon' => [
					'host' => '142.93.7.91',
					// 'name' => 'agroaudit_bonita',
					'name' => 'agroaudit_marlon',
					'user' => 'backups',
					'pass' => 'vgaN6wQT48UgMZCG'
				],
				'bonita' => [
					'host' => '142.93.7.91',
					'name' => 'agroaudit_bonita',
					'user' => 'backups',
					'pass' => 'vgaN6wQT48UgMZCG'
				],
				'quintana' => [
					'host' => '142.93.7.91',
					'name' => 'agroaudit_quintana',
					'user' => 'backups',
					'pass' => 'vgaN6wQT48UgMZCG'
				],
				'marun' => [
					'host' => '142.93.7.91',
					'name' => 'agroaudit_marun',
					'user' => 'backups',
					'pass' => 'vgaN6wQT48UgMZCG'
                ],
                'cacao_marun' => [
					'host' => '142.93.7.91',
					'name' => 'agroaudit_cacao_marun',
					'user' => 'backups',
					'pass' => 'vgaN6wQT48UgMZCG'
				],
				'demo' => [
					'host' => '142.93.7.91',
					'name' => 'agroaudit_demo',
					'user' => 'backups',
					'pass' => 'vgaN6wQT48UgMZCG'
				],
				'francisco' => [
					'host' => '142.93.7.91',
					'name' => 'agroaudit_francisco',
					'user' => 'backups',
					'pass' => 'vgaN6wQT48UgMZCG'
				],
				'dole' => [
					'host' => '142.93.7.91',
					'name' => 'agroaudit_dole',
					'user' => 'backups',
					'pass' => 'vgaN6wQT48UgMZCG'
				],
				'sigat' => [
					'host' => '142.93.7.91',
					'name' => 'sigat',
					'user' => 'backups',
					'pass' => 'vgaN6wQT48UgMZCG'
				],
				'sigat_marlon' => [
					'host' => '142.93.7.91',
					'name' => 'sigat_marlon',
					'user' => 'backups',
					'pass' => 'vgaN6wQT48UgMZCG'
				],
				'agroarriba' => [
					'host' => '142.93.7.91',
					'name' => 'agroarriba',
					'user' => 'backups',
					'pass' => 'vgaN6wQT48UgMZCG'
				],
				'agroarriba_dev' => [
					'host' => '142.93.7.91',
					'name' => 'agroarriba_dev',
					'user' => 'backups',
					'pass' => 'vgaN6wQT48UgMZCG'
				],
				'natural_habitats' => [
					'host' => '142.93.7.91',
					'name' => 'natural_habitats',
					'user' => 'backups',
					'pass' => 'vgaN6wQT48UgMZCG'
                ],
				'reiset' => [
                    'host' => '142.93.7.91',
					'name' => 'agroaudit_reiset',
					'user' => 'backups',
					'pass' => 'vgaN6wQT48UgMZCG'
				],
				'sumifru' => [
					'host' => '142.93.7.91',
					'name' => 'agroaudit_sumifru',
					'user' => 'backups',
					'pass' => 'vgaN6wQT48UgMZCG'
				],
                'palmar' => [
					'host' => '142.93.7.91',
					'name' => 'agroaudit_palmar',
					'user' => 'backups',
					'pass' => 'vgaN6wQT48UgMZCG'
                ],
                'palosanto' => [
                    'host' => '142.93.7.91',
					'name' => 'saenz',
					'user' => 'backups',
					'pass' => 'vgaN6wQT48UgMZCG'
                ],
                'climat' => [
                    'host' => '142.93.7.91',
					'name' => 'climat',
					'user' => 'backups',
					'pass' => 'vgaN6wQT48UgMZCG'
				],
				'clean_star' => [
                    'host' => '142.93.7.91',
					'name' => 'clean_star',
					'user' => 'backups',
					'pass' => 'vgaN6wQT48UgMZCG'
				],
				'arregui' => [
					'host' => '142.93.7.91',
					'name' => 'agroaudit_arregui',
					'user' => 'backups',
					'pass' => 'vgaN6wQT48UgMZCG'
				],
				'clementina' => [
					'host' => '142.93.7.91',
					'name' => 'agroaudit_clementina',
					'user' => 'backups',
					'pass' => 'vgaN6wQT48UgMZCG'
				],
				'orodelti' => [
					'host' => '142.93.7.91',
					'name' => 'agroaudit_orodelti',
					'user' => 'backups',
					'pass' => 'vgaN6wQT48UgMZCG'
				],
				'agroaereo' => [
					'host' => '142.93.7.91',
					'name' => 'agroaudit_agroaereo',
					'user' => 'backups',
					'pass' => 'vgaN6wQT48UgMZCG'
				],
				'digital' => [
					'host' => 'procesosiq.com',
					'name' => 'ticket_programador',
					'user' => 'readers',
					'pass' => 'oQ4Ro19xTd6d'
				],
				'agroban' => [
					'host' => '142.93.7.91',
					'name' => 'agroaudit_agroban',
					'user' => 'backups',
					'pass' => 'vgaN6wQT48UgMZCG'
				],
				'eugenia' => [
					'host' => '142.93.7.91',
					'name' => 'agroaudit_eugenia',
					'user' => 'backups',
					'pass' => 'vgaN6wQT48UgMZCG'
				],
				'allweights' => [
					'host' => 'hcdamariamaria.com',
					'name' => 'bdbalanza',
					'user' => 'procesosiq',
					'pass' => 'Pr0c3s0s1Q@2018'
				]
			]
		],
		'dev-app.procesos-iq.com' => [
			'url' => 'http://dev-app.procesos-iq.com/',
			'env' => 'DEV',
			'agent_user' => 'global',
			'sigat' => 'global',
			'agroaudit' => 'global',
			'merma' => 'global',
			'calidad' => 'global',
			'produccion' => 'global',
			'db' => [
				[
					'host' => 'localhost',
					'name' => 'procesosiq',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'dagcoin' => [
					'host' => 'localhost',
					'name' => 'dagcoin',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'marcel' => [
					'host' => 'localhost',
					'name' => 'agroaudit_marcel',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'mario' => [
					'host' => 'localhost',
					'name' => 'agroaudit_mario',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'victor' => [
					'host' => 'localhost',
					'name' => 'agroaudit_victor',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'marlon' => [
					'host' => 'localhost',
					// 'name' => 'agroaudit_bonita',
					'name' => 'agroaudit_marlon',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'bonita' => [
					'host' => 'localhost',
					'name' => 'agroaudit_bonita',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'quintana' => [
					'host' => 'localhost',
					'name' => 'agroaudit_quintana',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'marun' => [
					'host' => 'localhost',
					'name' => 'agroaudit_marun',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
                ],
                'cacao_marun' => [
					'host' => 'localhost',
					'name' => 'agroaudit_cacao_marun',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'demo' => [
					'host' => 'localhost',
					'name' => 'agroaudit_demo',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'francisco' => [
					'host' => 'localhost',
					'name' => 'agroaudit_francisco',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'dole' => [
					'host' => 'localhost',
					'name' => 'agroaudit_dole',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'sigat' => [
					'host' => 'localhost',
					'name' => 'sigat',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'sigat_marlon' => [
					'host' => 'localhost',
					'name' => 'sigat_marlon',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'agroarriba' => [
					'host' => 'localhost',
					'name' => 'agroarriba',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'agroarriba_dev' => [
					'host' => 'localhost',
					'name' => 'agroarriba_dev',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'natural_habitats' => [
					'host' => 'localhost',
					'name' => 'natural_habitats',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
                ],
				'reiset' => [
                    'host' => 'localhost',
					'name' => 'agroaudit_reiset',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'sumifru' => [
					'host' => 'localhost',
					'name' => 'agroaudit_sumifru',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
                'palmar' => [
					'host' => 'localhost',
					'name' => 'agroaudit_palmar',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
                ],
                'palosanto' => [
                    'host' => 'localhost',
					'name' => 'saenz',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
                ],
                'climat' => [
                    'host' => 'localhost',
					'name' => 'climat',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'clean_star' => [
                    'host' => 'localhost',
					'name' => 'clean_star',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'arregui' => [
					'host' => 'localhost',
					'name' => 'agroaudit_arregui',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'clementina' => [
					'host' => 'localhost',
					'name' => 'agroaudit_clementina',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'orodelti' => [
					'host' => 'localhost',
					'name' => 'agroaudit_orodelti',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'agroaereo' => [
					'host' => 'localhost',
					'name' => 'agroaudit_agroaereo',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'digital' => [
					'host' => 'procesosiq.com',
					'name' => 'ticket_programador',
					'user' => 'readers',
					'pass' => 'oQ4Ro19xTd6d'
				],
				'agroban' => [
					'host' => 'localhost',
					'name' => 'agroaudit_agroban',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'eugenia' => [
					'host' => 'localhost',
					'name' => 'agroaudit_eugenia',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
			]
		],
		'app.test' => [
			'url' => 'http://app.test/',
			// 'urlssl' => 'https://YOUR-DOMAIN/YOUR-INSTALL-PATH/',
			'agent_user' => 'global',
			'sigat' => 'global',
			'agroaudit' => 'global',
			'merma' => 'global',
			'calidad' => 'global',
			'produccion' => 'global',
			'env' => 'dev',
			'db' => [
				[
					'host' => 'localhost',
					'name' => 'procesosiq',
					'user' => 'root',
					'pass' => ''
				],
				'dagcoin' => [
					'host' => 'localhost',
					'name' => 'dagcoin',
					'user' => 'root',
					'pass' => ''
				],
				'marcel' => [
					'host' => 'localhost',
					'name' => 'agroaudit_marcel',
					'user' => 'root',
					'pass' => ''
				],
				'mario' => [
					'host' => 'localhost',
					'name' => 'agroaudit_mario',
					'user' => 'root',
					'pass' => ''
				],
				'marlon' => [
					'host' => 'localhost',
					// 'name' => 'agroaudit_bonita',
					'name' => 'agroaudit_marlon',
					'user' => 'root',
					'pass' => ''
				],
				'bonita' => [
					'host' => 'localhost',
					'name' => 'agroaudit_bonita',
					'user' => 'root',
					'pass' => ''
				],
				'quintana' => [
					'host' => 'localhost',
					'name' => 'agroaudit_quintana',
					'user' => 'root',
					'pass' => ''
				],
				'marun' => [
					'host' => 'localhost',
					'name' => 'agroaudit_marun',
					'user' => 'root',
					'pass' => ''
                ],
                'cacao_marun' => [
					'host' => 'localhost',
					'name' => 'agroaudit_cacao_marun',
					'user' => 'root',
					'pass' => ''
				],
				'demo' => [
					'host' => 'localhost',
					'name' => 'agroaudit_demo',
					'user' => 'root',
					'pass' => ''
				],
				'francisco' => [
					'host' => 'localhost',
					'name' => 'agroaudit_francisco',
					'user' => 'root',
					'pass' => ''
				],
				'dole' => [
					'host' => 'localhost',
					'name' => 'agroaudit_dole',
					'user' => 'root',
					'pass' => ''
				],
				'sigat' => [
					'host' => 'procesos-iq.com',
					'name' => 'sigat',
					'user' => 'auditoriasbonita',
					'pass' => 'u[V(fTIUbcVb'
				],
				'sigat_marlon' => [
					'host' => 'localhost',
					'name' => 'sigat_marlon',
					'user' => 'root',
					'pass' => ''
				],
				'agroarriba' => [
					'host' => 'localhost',
					'name' => 'agroarriba',
					'user' => 'root',
					'pass' => ''
				],
				'agroarriba_dev' => [
					'host' => 'localhost',
					'name' => 'agroarriba_dev',
					'user' => 'root',
					'pass' => ''
				],
				'natural_habitats' => [
					'host' => 'localhost',
					'name' => 'natural_habitats',
					'user' => 'root',
					'pass' => ''
                ],
				'reiset' => [
                    'host' => 'localhost',
					'name' => 'agroaudit_reiset',
					'user' => 'root',
					'pass' => ''
				],
				'sumifru' => [
					'host' => 'localhost',
					'name' => 'agroaudit_sumifru',
					'user' => 'root',
					'pass' => ''
				],
                'palmar' => [
					'host' => 'localhost',
					'name' => 'agroaudit_palmar',
					'user' => 'root',
					'pass' => ''
                ],
                'palosanto' => [
                    'host' => 'localhost',
					'name' => 'saenz',
					'user' => 'root',
					'pass' => ''
                ],
                'climat' => [
                    'host' => 'localhost',
					'name' => 'climat',
					'user' => 'root',
					'pass' => ''
				],
				'clean_star' => [
                    'host' => 'localhost',
					'name' => 'clean_star',
					'user' => 'root',
					'pass' => ''
				],
				'arregui' => [
					'host' => 'localhost',
					'name' => 'agroaudit_arregui',
					'user' => 'root',
					'pass' => ''
				],
				'clementina' => [
					'host' => 'localhost',
					'name' => 'agroaudit_clementina',
					'user' => 'root',
					'pass' => ''
				],
				'orodelti' => [
					'host' => 'localhost',
					'name' => 'agroaudit_orodelti',
					'user' => 'root',
					'pass' => ''
				],
				'agroaereo' => [
					'host' => 'localhost',
					'name' => 'agroaudit_agroaereo',
					'user' => 'root',
					'pass' => ''
				],
				'digital' => [
					'host' => 'procesosiq.com',
					'name' => 'ticket_programador',
					'user' => 'readers',
					'pass' => 'oQ4Ro19xTd6d'
				],
				'agroban' => [
					'host' => 'localhost',
					'name' => 'agroaudit_agroban',
					'user' => 'root',
					'pass' => ''
				],
				'eugenia' => [
					'host' => 'localhost',
					'name' => 'agroaudit_eugenia',
					'user' => 'root',
					'pass' => ''
				],
				'allweights' => [
					'host' => 'hcdamariamaria.com',
					'name' => 'bdbalanza',
					'user' => 'procesosiq',
					'pass' => 'Pr0c3s0s1Q@2018'
				],
				'rufcorp' => [
					'host' => 'localhost',
					'name' => 'agroaudit_rufcorp',
					'user' => 'root',
					'pass' => ''
				]
			]
		],
		'app.agro-iq.com' => [
			'url' => 'https://app.agro-iq.com/',
			'cdn' => '//cdn.agro-iq.com/',
			'agent_user' => 'global',
			'sigat' => 'global',
			'agroaudit' => 'global',
			'merma' => 'global',
			'calidad' => 'global',
			'produccion' => 'global',
			'db' => [
				[
					'host' => '35.202.152.188',
					'name' => 'procesosiq',
					'user' => 'webdev',
					'pass' => 'ud7Y4z@\3YTf"h#p'
				],
				'marcel' => [
					'host' => '35.202.152.188',
					'name' => 'agroaudit_marcel',
					'user' => 'webdev',
					'pass' => 'ud7Y4z@\3YTf"h#p'
				],
				'mario' => [
					'host' => '35.202.152.188',
					'name' => 'agroaudit_mario',
					'user' => 'webdev',
					'pass' => 'ud7Y4z@\3YTf"h#p'
				],
				'marlon' => [
					'host' => '35.202.152.188',
					// 'name' => 'agroaudit_bonita',
					'name' => 'agroaudit_marlon',
					'user' => 'webdev',
					'pass' => 'ud7Y4z@\3YTf"h#p'
				],
				'bonita' => [
					'host' => '35.202.152.188',
					'name' => 'agroaudit_bonita',
					'user' => 'webdev',
					'pass' => 'ud7Y4z@\3YTf"h#p'
				],
				'quintana' => [
					'host' => '35.202.152.188',
					'name' => 'agroaudit_quintana',
					'user' => 'webdev',
					'pass' => 'ud7Y4z@\3YTf"h#p'
				],
				'marun' => [
					'host' => '35.202.152.188',
					'name' => 'agroaudit_marun',
					'user' => 'webdev',
					'pass' => 'ud7Y4z@\3YTf"h#p'
                ],
                'cacao_marun' => [
					'host' => '35.202.152.188',
					'name' => 'agroaudit_cacao_marun',
					'user' => 'webdev',
					'pass' => 'ud7Y4z@\3YTf"h#p'
				],
				'demo' => [
					'host' => '35.202.152.188',
					'name' => 'agroaudit_demo',
					'user' => 'webdev',
					'pass' => 'ud7Y4z@\3YTf"h#p'
				],
				'francisco' => [
					'host' => '35.202.152.188',
					'name' => 'agroaudit_francisco',
					'user' => 'webdev',
					'pass' => 'ud7Y4z@\3YTf"h#p'
				],
				'dole' => [
					'host' => '35.202.152.188',
					'name' => 'agroaudit_dole',
					'user' => 'webdev',
					'pass' => 'ud7Y4z@\3YTf"h#p'
				],
				'sigat' => [
					'host' => '35.202.152.188',
					'name' => 'sigat',
					'user' => 'webdev',
					'pass' => 'ud7Y4z@\3YTf"h#p'
				],
				'sigat_marlon' => [
					'host' => '35.202.152.188',
					'name' => 'sigat_marlon',
					'user' => 'webdev',
					'pass' => 'ud7Y4z@\3YTf"h#p'
				],
				'agroarriba' => [
					'host' => '35.202.152.188',
					'name' => 'agroarriba',
					'user' => 'webdev',
					'pass' => 'ud7Y4z@\3YTf"h#p'
				],
				'agroarriba_dev' => [
					'host' => '35.202.152.188',
					'name' => 'agroarriba_dev',
					'user' => 'webdev',
					'pass' => 'ud7Y4z@\3YTf"h#p'
				],
				'natural_habitats' => [
					'host' => '35.202.152.188',
					'name' => 'natural_habitats',
					'user' => 'webdev',
					'pass' => 'ud7Y4z@\3YTf"h#p'
                ],
                'reiset' => [
                    'host' => '35.202.152.188',
					'name' => 'agroaudit_reiset',
					'user' => 'webdev',
					'pass' => 'ud7Y4z@\3YTf"h#p'
				],
				'sumifru' => [
					'host' => '35.202.152.188',
					'name' => 'agroaudit_sumifru',
					'user' => 'webdev',
					'pass' => 'ud7Y4z@\3YTf"h#p'
                ],
                'palmar' => [
					'host' => '35.202.152.188',
					'name' => 'agroaudit_palmnar',
					'user' => 'webdev',
					'pass' => 'ud7Y4z@\3YTf"h#p'
                ],
                'palosanto' => [
                    'host' => '35.202.152.188',
					'name' => 'saenz',
					'user' => 'webdev',
					'pass' => 'ud7Y4z@\3YTf"h#p'
                ],
                'climat' => [
                    'host' => '35.202.152.188',
					'name' => 'climat',
					'user' => 'webdev',
					'pass' => 'ud7Y4z@\3YTf"h#p'
				],
			]
        ],
        'app.mc' => [
			'url' => 'http://app.mc/',
			// 'urlssl' => 'https://YOUR-DOMAIN/YOUR-INSTALL-PATH/',
			'agent_user' => 'global',
			'sigat' => 'global',
			'agroaudit' => 'global',
			'merma' => 'global',
			'calidad' => 'global',
			'produccion' => 'global',
			'db' => [
				[
					'host' => 'mysql',
					'name' => 'procesosiq',
					'user' => 'root',
					'pass' => 'tiger'
				],
				'marcel' => [
					'host' => 'mysql',
					'name' => 'agroaudit_marcel',
					'user' => 'root',
					'pass' => 'tiger'
				],
				'mario' => [
					'host' => 'mysql',
					'name' => 'agroaudit_mario',
					'user' => 'root',
					'pass' => 'tiger'
				],
				'marlon' => [
					'host' => 'mysql',
					// 'name' => 'agroaudit_bonita',
					'name' => 'agroaudit_marlon',
					'user' => 'root',
					'pass' => 'tiger'
				],
				'bonita' => [
					'host' => 'mysql',
					'name' => 'agroaudit_bonita',
					'user' => 'root',
					'pass' => 'tiger'
				],
				'quintana' => [
					'host' => 'mysql',
					'name' => 'agroaudit_quintana',
					'user' => 'root',
					'pass' => 'tiger'
				],
				'marun' => [
					'host' => 'mysql',
					'name' => 'agroaudit_marun',
					'user' => 'root',
					'pass' => 'tiger'
                ],
                'cacao_marun' => [
					'host' => 'mysql',
					'name' => 'agroaudit_cacao_marun',
					'user' => 'root',
					'pass' => 'tiger'
				],
				'demo' => [
					'host' => 'mysql',
					'name' => 'agroaudit_demo',
					'user' => 'root',
					'pass' => 'tiger'
				],
				'francisco' => [
					'host' => 'mysql',
					'name' => 'agroaudit_francisco',
					'user' => 'root',
					'pass' => 'tiger'
				],
				'dole' => [
					'host' => 'mysql',
					'name' => 'agroaudit_dole',
					'user' => 'root',
					'pass' => 'tiger'
				],
				'sigat' => [
					'host' => 'mysql',
					'name' => 'sigat',
					'user' => 'root',
					'pass' => 'tiger'
				],
				'sigat_marlon' => [
					'host' => 'mysql',
					'name' => 'sigat_marlon',
					'user' => 'root',
					'pass' => 'tiger'
				],
				'agroarriba' => [
					'host' => 'mysql',
					'name' => 'agroarriba',
					'user' => 'root',
					'pass' => 'tiger'
				],
				'agroarriba_dev' => [
					'host' => 'mysql',
					'name' => 'agroarriba_dev',
					'user' => 'root',
					'pass' => 'tiger'
				],
				'natural_habitats' => [
					'host' => 'mysql',
					'name' => 'natural_habitats',
					'user' => 'root',
					'pass' => 'tiger'
                ],
				'reiset' => [
                    'host' => 'mysql',
					'name' => 'agroaudit_reiset',
					'user' => 'root',
					'pass' => 'tiger'
				],
				'sumifru' => [
					'host' => 'mysql',
					'name' => 'agroaudit_sumifru',
					'user' => 'root',
					'pass' => 'tiger'
				],
                'palmar' => [
					'host' => 'mysql',
					'name' => 'agroaudit_palmar',
					'user' => 'root',
					'pass' => 'tiger'
                ],
                'palosanto' => [
                    'host' => 'mysql',
					'name' => 'saenz',
					'user' => 'root',
					'pass' => 'tiger'
                ],
                'climat' => [
                    'host' => 'mysql',
					'name' => 'climat',
					'user' => 'root',
					'pass' => 'tiger'
				],
				'clean_star' => [
                    'host' => 'mysql',
					'name' => 'clean_star',
					'user' => 'root',
					'pass' => 'tiger'
				],
				'arregui' => [
					'host' => 'mysql',
					'name' => 'agroaudit_arregui',
					'user' => 'root',
					'pass' => 'tiger'
				],
				'clementina' => [
					'host' => 'mysql',
					'name' => 'agroaudit_clementina',
					'user' => 'root',
					'pass' => 'tiger'
				],
				'orodelti' => [
					'host' => 'mysql',
					'name' => 'agroaudit_orodelti',
					'user' => 'root',
					'pass' => 'tiger'
				],
				'agroaereo' => [
					'host' => 'mysql',
					'name' => 'agroaudit_agroaereo',
					'user' => 'root',
					'pass' => 'tiger'
				],
				'digital' => [
					'host' => 'procesosiq.com',
					'name' => 'ticket_programador',
					'user' => 'readers',
					'pass' => 'oQ4Ro19xTd6d'
				],
				'agroban' => [
					'host' => 'mysql',
					'name' => 'agroaudit_agroban',
					'user' => 'root',
					'pass' => 'tiger'
				],
				'eugenia' => [
					'host' => 'mysql',
					'name' => 'agroaudit_eugenia',
					'user' => 'root',
					'pass' => 'tiger'
				],
			]
		],
	],
	'smtp' => [
		'host' => '',
		'pass' => '',
		'from' => [
			'Contacto' => ''
		]
	],
	'routing' => [
		/* BEGIN NOTIFICACIONES */
		'notificaciones/delete' => ['controller' => 'Notificaciones', 'action' => 'delete'],
		'consolelog/add' => ['controller' => 'Notificaciones', 'action' => 'consolelog'],
		/* END NOTIFICACIONES */

        'access/fincasProceso' => ['controller' => 'Access', 'action' => 'saveFincasProceso'],
        'access/dontFincasProceso' => ['controller' => 'Access', 'action' => 'dontFincasProceso'],

		'access/login' => ['controller' => 'Access', 'action' => 'login'],
        'access/logout' => ['controller' => 'Access', 'action' => 'logout'],
        'access/ping' => ['controller' => 'Access', 'action' => 'ping'],
        'export/pdf' => ['controller' => 'Exportar', 'action' => 'pdf'],
		'api/execute' => ['controller' => 'ApiProntoforms', 'action' => 'execute'],
		'data/execute' => ['controller' => 'Data', 'action' => 'execute', 'module' => 'banano'],
		'data/zonas' => ['controller' => 'Data', 'action' => 'zonas', 'module' => 'banano'],
        'data/fincas' => ['controller' => 'Data', 'action' => 'fincas', 'module' => 'banano'],
        'data/racimosFincas' => ['controller' => 'Data', 'action' => 'fincasRacimos', 'module' => 'banano'],
		'data/labores' => ['controller' => 'Data', 'action' => 'labores', 'module' => 'banano'],
		'data/asistencia/labores' => ['controller' => 'Data', 'action' => 'laboresAsistencia', 'module' => 'banano'],
		'data/tipolabores' => ['controller' => 'Data', 'action' => 'TipoLabores', 'module' => 'banano'],
        'data/lotes' => ['controller' => 'Data', 'action' => 'lotes', 'module' => 'banano'],
        'data/nameLotes' => ['controller' => 'Data', 'action' => 'lotesName', 'module' => 'banano'],
		'data/causas' => ['controller' => 'Data', 'action' => 'causas', 'module' => 'banano'],
		'data/racimosCausas' => ['controller' => 'Data', 'action' => 'causasRacimos', 'module' => 'banano'],
		'data/puntal' => ['controller' => 'Data', 'action' => 'puntal', 'module' => 'banano'],
		'data/clientes' => ['controller' => 'Data', 'action' => 'clientes', 'module' => 'banano'],
		'data/marcas' => ['controller' => 'Data', 'action' => 'marcas', 'module' => 'banano'],
		'data/cables' => ['controller' => 'Data', 'action' => 'Cables', 'module' => 'banano'],
        'data/personal' => ['controller' => 'Data', 'action' => 'personal', 'module' => 'banano'],
        /* DATA CONFIG LANCOFRUIT */
        'data/lancofruitCategorias' => ['controller' => 'Data', 'action' => 'lancofruitCategorias', 'module' => 'banano'],
        'data/lancofruitSubcategorias' => ['controller' => 'Data', 'action' => 'lancofruitSubcategorias', 'module' => 'banano'],
        /* DATA CONFIG LANCOFRUIT */
		/* BEGIN MIGRAR */
		'data/categorias' => ['controller' => 'Data', 'action' => 'categorias', 'module' => 'banano'],
		'data/defectos' => ['controller' => 'Data', 'action' => 'defectos', 'module' => 'banano'],
		'data/exportadores' => ['controller' => 'Data', 'action' => 'exportadores', 'module' => 'banano'],
		'data/cajas' => ['controller' => 'Data', 'action' => 'cajas', 'module' => 'banano'],
		'data/destinos' => ['controller' => 'Data', 'action' => 'destinos', 'module' => 'banano'],
		'data/subfincas' => ['controller' => 'Data', 'action' => 'subfincas', 'module' => 'banano'],
		'data/rutasLancofruit' => ['controller' => 'Data', 'action' => 'lancofruitRutas', 'module' => 'banano'],
		'data/vendedoresLancofruit' => ['controller' => 'Data', 'action' => 'lancofruitVendedores', 'module' => 'banano'],
		'data/perchasLancofruit' => ['controller' => 'Data', 'action' => 'perchas', 'module' => 'banano'],
		/* END MIGRAR */
		/*----------  DATA MARUN  ----------*/
		'merma/fincas' => ['controller' => 'Data', 'action' => 'fincasMerma', 'module' => 'banano'],
		'merma/palanca' => ['controller' => 'Data', 'action' => 'Palanca', 'module' => 'banano'],
		/*----------  DATA MARUN  ----------*/
		'general/index' => ['controller' => 'Demo', 'action' => 'index', 'module' => 'banano'],
		'General/index' => ['controller' => 'General', 'action' => 'index', 'module' => 'banano'],
		'Fincas/index' => ['controller' => 'Fincas', 'action' => 'index', 'module' => 'banano'],
		'Reportes/index' => ['controller' => 'Reportes', 'action' => 'index', 'module' => 'banano'],
		'Reportes/causas' => ['controller' => 'Agroaudit', 'action' => 'getCausas', 'module' => 'banano'],
		'Reportes/labores' => ['controller' => 'Agroaudit', 'action' => 'getLaboresLotes', 'module' => 'banano'],
		'Reportes/lotes' => ['controller' => 'Agroaudit', 'action' => 'getFincaLotes', 'module' => 'banano'],
		'Reportes/fincas' => ['controller' => 'Agroaudit', 'action' => 'getZonaFinca', 'module' => 'banano'],
        'Reportes/zonas' => ['controller' => 'Agroaudit', 'action' => 'getHaciendaZonas', 'module' => 'banano'],
        
        'marlon/reportes/index' => ['controller' => 'Reportes', 'action' => 'index', 'path' => 'marlon', 'module' => 'banano'],
        'marlon/reportes/fincas' => ['controller' => 'Agroaudit', 'action' => 'getZonaFinca', 'path' => 'marlon', 'module' => 'banano'],
        'marlon/reportes/lotes' => ['controller' => 'Agroaudit', 'action' => 'getFincaLotes', 'path' => 'marlon', 'module' => 'banano'],
        'marlon/reportes/causas' => ['controller' => 'Agroaudit', 'action' => 'getCausas', 'path' => 'marlon', 'module' => 'banano'],
        'marlon/reportes/labores' => ['controller' => 'Agroaudit', 'action' => 'getLaboresLotes', 'path' => 'marlon', 'module' => 'banano'],
        'marlon/reportes/zonas' => ['controller' => 'Agroaudit', 'action' => 'getHaciendaZonas', 'path' => 'marlon', 'module' => 'banano'],
        'marlon/labores/zonas' => ['controller' => 'RLabores', 'action' => 'zonas', 'path' => 'marlon', 'module' => 'banano'],
		'marlon/labores/fincas' => ['controller' => 'RLabores', 'action' => 'fincas', 'path' => 'marlon', 'module' => 'banano'],
		'marlon/labores/lotes' => ['controller' => 'RLabores', 'action' => 'lotes', 'path' => 'marlon', 'module' => 'banano'],
		'marlon/labores/last' => ['controller' => 'RLabores', 'action' => 'last', 'path' => 'marlon', 'module' => 'banano'],

		/* -------------- LABORES MARIO --------------*/
		'labores/data' => ['path' => 'mario','controller' => 'Labores', 'action' => 'data', 'module' => 'banano'],
		/* -------------- LABORES MARIO --------------*/
		
		'mapas/index' => ['controller' => 'Geoposicion', 'action' => 'index', 'module' => 'banano'],
		'mapas/markers' => ['controller' => 'Geoposicion', 'action' => 'Markers', 'module' => 'banano'],
		'mario/mapas/index' => ['controller' => 'Geoposicion', 'action' => 'index', 'module' => 'banano', 'path' => 'mario'],
		'mario/mapas/markers' => ['controller' => 'Geoposicion', 'action' => 'Markers', 'module' => 'banano', 'path' => 'mario'],
		'reader/index' => ['controller' => 'Reader', 'action' => 'index', 'module' => 'banano'],
		'membresias/index' => ['controller' => 'Membresias', 'action' => 'index', 'module' => 'banano'],
		'membresias/edit' => ['controller' => 'Membresias', 'action' => 'edit', 'module' => 'banano'],
		/*----------  PERSONAL  ----------*/
		'personal/index' => ['controller' => 'Personal', 'action' => 'index', 'module' => 'banano'],
		'personalcacao/index' => ['controller' => 'Personal', 'action' => 'index', 'module' => 'cacao', 'path' => 'marun'],
		'details/personal' => ['controller' => 'Personal', 'action' => 'getPersonal', 'module' => 'banano'],
		'details/cacaopersonal' => ['controller' => 'Personal', 'action' => 'getPersonal', 'module' => 'cacao', 'path' => 'marun'],
		'save/personal' => ['controller' => 'Personal', 'action' => 'savePersonal', 'module' => 'banano'],
		'save/cacaopersonal' => ['controller' => 'Personal', 'action' => 'savePersonal', 'module' => 'cacao', 'path' => 'marun'],
		'guardar/sueldo' => ['controller' => 'Personal', 'action' => 'saveSuledo', 'module' => 'banano'],
		'cargo/tipo' => ['controller' => 'Personal', 'action' => 'getLabores', 'module' => 'banano'],
		'changecacao/status' => ['controller' => 'Personal', 'action' => 'changeStatus', 'module' => 'cacao', 'path' => 'marun'],
		'marcel/personal' => ['controller' => 'PersonalMarcel', 'action' => 'getPersonal', 'module' => 'banano'],
		'marcel/savePersonal' => ['controller' => 'PersonalMarcel', 'action' => 'savePersonal', 'module' => 'banano'],
		/*----------  PERSONAL  ----------*/

		/*----------  EXPEDIENTE  ----------*/
		'expediente/index' => ['controller' => 'Expediente', 'action' => 'index', 'module' => 'banano'],
		'expediente/show' => ['controller' => 'Expediente', 'action' => 'show', 'module' => 'banano'],
		'expediente/save' => ['controller' => 'Expediente', 'action' => 'save', 'module' => 'banano'],
		'expediente/delete' => ['controller' => 'Expediente', 'action' => 'delete', 'module' => 'banano'],
		/*----------  EXPEDIENTE  ----------*/
        'mario/merma/index' => ['controller' => 'Merma', 'action' => 'index' , 'path' => 'mario', 'module' => 'banano'],
        'quintana/merma/last' => ['controller' => 'Merma', 'action' => 'last' , 'path' => 'quintana', 'module' => 'banano'],
        'quintana/merma/index' => ['controller' => 'Merma', 'action' => 'index' , 'path' => 'quintana', 'module' => 'banano'],
        /*----------  MERMA MARLON ----------*/
        'marlon/merma/index' => ['controller' => 'Merma', 'action' => 'index' , 'path' => 'marlon', 'module' => 'banano'],
        'marlon/merma/last' => ['controller' => 'Merma', 'action' => 'last' , 'path' => 'marlon', 'module' => 'banano'],
        /*----------  MERMA MARLON ----------*/
        /*----------  MERMA MARCEL ----------*/
		'marcel/merma/index' => ['controller' => 'Merma', 'action' => 'index' , 'path' => 'marcel', 'module' => 'banano'],
		'marcel/merma/last' => ['controller' => 'Merma', 'action' => 'last' , 'path' => 'marcel', 'module' => 'banano'],
        'marcel/merma/semanal' => ['controller' => 'Merma', 'action' => 'weeksHistories' , 'path' => 'marcel', 'module' => 'banano'],
        'marcel/merma/mermaSemanal' => ['controller' => 'Merma', 'action' => 'historicoSemanal', 'module' => 'banano', 'path' => 'marcel'],
        'marcel/merma/tendenciaSemanal' => ['controller' => 'Merma', 'action' => 'tendenciaSemanal', 'module' => 'banano', 'path' => 'marcel'],
		'marcel/merma/variables' => ['controller' => 'Merma', 'action' => 'tablaVariables' , 'path' => 'marcel', 'module' => 'banano'],
        'marcel/merma/last' => ['controller' => 'Merma', 'action' => 'last' , 'path' => 'marcel', 'module' => 'banano'],
		'marcel/merma/second' => ['controller' => 'Merma', 'action' => 'getSecondLevel' , 'path' => 'marcel', 'module' => 'banano'],
		'marcel/merma/third' => ['controller' => 'Merma', 'action' => 'getThirdLevel' , 'path' => 'marcel', 'module' => 'banano'],
		/*----------  MERMA MARCEL ----------*/
		/*----------  MERMA CLEMENTINA ----------*/
		'clementina/merma/last' => ['controller' => 'Merma', 'action' => 'last' , 'path' => 'clementina', 'module' => 'banano'],
        'clementina/merma/index' => ['controller' => 'Merma', 'action' => 'index' , 'path' => 'clementina', 'module' => 'banano'],
        'clementina/merma/semanal' => ['controller' => 'Merma', 'action' => 'weeksHistories' , 'path' => 'clementina', 'module' => 'banano'],
        'clementina/merma/mermaSemanal' => ['controller' => 'Merma', 'action' => 'historicoSemanal', 'module' => 'banano', 'path' => 'clementina'],
        'clementina/merma/tendenciaSemanal' => ['controller' => 'Merma', 'action' => 'tendenciaSemanal', 'module' => 'banano', 'path' => 'clementina'],
		'clementina/merma/variables' => ['controller' => 'Merma', 'action' => 'tablaVariables' , 'path' => 'clementina', 'module' => 'banano'],
        'clementina/merma/last' => ['controller' => 'Merma', 'action' => 'last' , 'path' => 'clementina', 'module' => 'banano'],
		'clementina/merma/second' => ['controller' => 'Merma', 'action' => 'getSecondLevel' , 'path' => 'clementina', 'module' => 'banano'],
		'clementina/merma/third' => ['controller' => 'Merma', 'action' => 'getThirdLevel' , 'path' => 'clementina', 'module' => 'banano'],
		/*----------  MERMA CLEMENTINA ----------*/
		/*----------  MERMA ARREGUI ----------*/
		'arregui/merma/last' => ['controller' => 'Merma', 'action' => 'last' , 'path' => 'arregui', 'module' => 'banano'],
        'arregui/merma/index' => ['controller' => 'Merma', 'action' => 'index' , 'path' => 'arregui', 'module' => 'banano'],
        'arregui/merma/semanal' => ['controller' => 'Merma', 'action' => 'weeksHistories' , 'path' => 'arregui', 'module' => 'banano'],
        'arregui/merma/mermaSemanal' => ['controller' => 'Merma', 'action' => 'historicoSemanal', 'module' => 'banano', 'path' => 'arregui'],
        'arregui/merma/tendenciaSemanal' => ['controller' => 'Merma', 'action' => 'tendenciaSemanal', 'module' => 'banano', 'path' => 'arregui'],
		'arregui/merma/variables' => ['controller' => 'Merma', 'action' => 'tablaVariables' , 'path' => 'arregui', 'module' => 'banano'],
        'arregui/merma/last' => ['controller' => 'Merma', 'action' => 'last' , 'path' => 'arregui', 'module' => 'banano'],
		'arregui/merma/second' => ['controller' => 'Merma', 'action' => 'getSecondLevel' , 'path' => 'arregui', 'module' => 'banano'],
		'arregui/merma/third' => ['controller' => 'Merma', 'action' => 'getThirdLevel' , 'path' => 'arregui', 'module' => 'banano'],
        /*----------  MERMA ARREGUI ----------*/
		/*----------  MERMA MARUN ----------*/
        'marun/merma/index' => ['controller' => 'Merma', 'action' => 'index' , 'path' => 'marun', 'module' => 'banano'],
		'marun/merma/variables' => ['controller' => 'Merma', 'action' => 'tablaVariables' , 'path' => 'marun', 'module' => 'banano'],
		'marun/merma/tallo' => ['controller' => 'Merma', 'action' => 'tallo' , 'path' => 'marun', 'module' => 'banano'],
		'marun/merma/tendenciaSemanal' => ['controller' => 'Merma', 'action' => 'tendenciaSemanal', 'module' => 'banano', 'path' => 'marun'],
        'marun/merma/last' => ['controller' => 'Merma', 'action' => 'last' , 'path' => 'marun', 'module' => 'banano'],
		'marun/merma/second' => ['controller' => 'Merma', 'action' => 'getSecondLevel' , 'path' => 'marun', 'module' => 'banano'],
		'marun/merma/third' => ['controller' => 'Merma', 'action' => 'getThirdLevel' , 'path' => 'marun', 'module' => 'banano'],
        /*----------  MERMA MARUN ----------*/
        /*----------  MERMA REISET ----------*/
        'reiset/merma/index' => ['controller' => 'Merma', 'action' => 'index' , 'path' => 'reiset', 'module' => 'banano'],
        'reiset/merma/last' => ['controller' => 'Merma', 'action' => 'last' , 'path' => 'reiset', 'module' => 'banano'],
        'reiset/merma/cierreEnfunde' => ['controller' => 'Merma', 'action' => 'tablaCierreEnfunde' , 'path' => 'reiset', 'module' => 'banano'],
		'reiset/merma/tableDedosPromedio' => ['controller' => 'Merma', 'action' => 'tableDedosPromedio' , 'path' => 'reiset', 'module' => 'banano'],
		'reiset/merma/tendenciaSemanal' => ['controller' => 'Merma', 'action' => 'tendenciaSemanal', 'module' => 'banano', 'path' => 'reiset'],
		'reiset/merma/variables' => ['controller' => 'Merma', 'action' => 'tablaVariables' , 'path' => 'reiset', 'module' => 'banano'],
		'reiset/merma/second' => ['controller' => 'Merma', 'action' => 'getSecondLevel' , 'path' => 'reiset', 'module' => 'banano'],
		'reiset/merma/third' => ['controller' => 'Merma', 'action' => 'getThirdLevel' , 'path' => 'reiset', 'module' => 'banano'],
		/*----------  MERMA REISET ----------*/
		/*----------  MERMA PALMAR ----------*/
        'palmar/merma/index' => ['controller' => 'Merma', 'action' => 'index' , 'path' => 'palmar', 'module' => 'banano'],
        'palmar/merma/last' => ['controller' => 'Merma', 'action' => 'last' , 'path' => 'palmar', 'module' => 'banano'],
        'palmar/merma/cierreEnfunde' => ['controller' => 'Merma', 'action' => 'tablaCierreEnfunde' , 'path' => 'palmar', 'module' => 'banano'],
        'palmar/merma/tableDedosPromedio' => ['controller' => 'Merma', 'action' => 'tableDedosPromedio' , 'path' => 'palmar', 'module' => 'banano'],
		'palmar/merma/second' => ['controller' => 'Merma', 'action' => 'getSecondLevel' , 'path' => 'palmar', 'module' => 'banano'],
		'palmar/merma/third' => ['controller' => 'Merma', 'action' => 'getThirdLevel' , 'path' => 'palmar', 'module' => 'banano'],
		'palmar/merma/variables' => ['controller' => 'Merma', 'action' => 'tablaVariables' , 'path' => 'palmar', 'module' => 'banano'],
		'palmar/merma/tendenciaSemanal' => ['controller' => 'Merma', 'action' => 'tendenciaSemanal', 'module' => 'banano', 'path' => 'palmar'],
        /*----------  MERMA PALMAR ----------*/
        /*----------  MERMA SUMIFRU ----------*/
        'sumifru/merma/index' => ['controller' => 'Merma', 'action' => 'index' , 'path' => 'sumifru', 'module' => 'banano'],
        'sumifru/merma/last' => ['controller' => 'Merma', 'action' => 'last' , 'path' => 'sumifru', 'module' => 'banano'],
        'sumifru/merma/cierreEnfunde' => ['controller' => 'Merma', 'action' => 'tablaCierreEnfunde' , 'path' => 'sumifru', 'module' => 'banano'],
        'sumifru/merma/tableDedosPromedio' => ['controller' => 'Merma', 'action' => 'tableDedosPromedio' , 'path' => 'sumifru', 'module' => 'banano'],
		'sumifru/merma/second' => ['controller' => 'Merma', 'action' => 'getSecondLevel' , 'path' => 'sumifru', 'module' => 'banano'],
		'sumifru/merma/third' => ['controller' => 'Merma', 'action' => 'getThirdLevel' , 'path' => 'sumifru', 'module' => 'banano'],
		'sumifru/merma/tendenciaSemanal' => ['controller' => 'Merma', 'action' => 'tendenciaSemanal', 'module' => 'banano', 'path' => 'sumifru'],
        /*----------  MERMA SUMIFRU ----------*/
        /*----------  MERMA MARIO ----------*/
        'mario/merma/last' => ['controller' => 'Merma', 'action' => 'last' , 'path' => 'mario', 'module' => 'banano'],
		/*----------  MERMA MARIO ----------*/
		/*----------  BONIFICACION MARUN ----------*/
		'marun/bonificacion/index' => ['controller' => 'Bonificacion', 'action' => 'index' , 'path' => 'marun', 'module' => 'banano'],
		'marun/bonificacion/configuracion' => ['controller' => 'Bonificacion', 'action' => 'configuracion' , 'path' => 'marun', 'module' => 'banano'],
		'marun/bonificacion/save/config' => ['controller' => 'Bonificacion', 'action' => 'save' , 'path' => 'marun', 'module' => 'banano'],
		'marun/bonificacion/save/umbrals' => ['controller' => 'Bonificacion', 'action' => 'saveUmbrals' , 'path' => 'marun', 'module' => 'banano'],
		'marun/bonificacion/historico' => ['controller' => 'Bonificacion', 'action' => 'getWeekYear' , 'path' => 'marun', 'module' => 'banano'],
        
        'marun/bonif/semanal' => ['controller' => 'Bonif', 'action' => 'getHistoricoSemanal', 'path' => 'marun', 'module' => 'banano'],
        'marun/bonif/detalle' => ['controller' => 'Bonif', 'action' => 'getDanos', 'path' => 'marun', 'module' => 'banano'],
        'marun/bonif/filters' => ['controller' => 'Bonif', 'action' => 'filters', 'path' => 'marun', 'module' => 'banano'],
        'marun/bonif/diario' => ['controller' => 'Bonif', 'action' => 'getDiario', 'path' => 'marun', 'module' => 'banano'],

		/*----------  BONIFICACION MARUN ----------*/
		
		/*----------  BONIFICACION MARCEL ----------*/
		'marcel/bonif/semanal' => ['controller' => 'Bonificacion', 'action' => 'getWeekYear', 'path' => 'marcel', 'module' => 'banano'],
        'marcel/bonif/detalle' => ['controller' => 'Bonificacion', 'action' => 'getDanos', 'path' => 'marcel', 'module' => 'banano'],
        'marcel/bonif/filters' => ['controller' => 'Bonificacion', 'action' => 'filters', 'path' => 'marcel', 'module' => 'banano'],
        'marcel/bonif/diario' => ['controller' => 'Bonificacion', 'action' => 'getDiario', 'path' => 'marcel', 'module' => 'banano'],
        'marcel/bonif/saveUmbral' => ['controller' => 'Bonificacion', 'action' => 'saveUmbral', 'path' => 'marcel', 'module' => 'banano'],
        /*----------  BONIFICACION MARCEL ----------*/

        /*----------  BONIFICACION MARUN REISET ----------*/
        'reiset/bonif/semanal' => ['controller' => 'Bonif', 'action' => 'getHistoricoSemanal', 'path' => 'reiset', 'module' => 'banano'],
        'reiset/bonif/detalle' => ['controller' => 'Bonif', 'action' => 'getDanos', 'path' => 'reiset', 'module' => 'banano'],
        'reiset/bonif/filters' => ['controller' => 'Bonif', 'action' => 'filters', 'path' => 'reiset', 'module' => 'banano'],
        'reiset/bonif/diario' => ['controller' => 'Bonif', 'action' => 'getDiario', 'path' => 'reiset', 'module' => 'banano'],
        'reiset/bonif/saveUmbral' => ['controller' => 'Bonif', 'action' => 'saveUmbral', 'path' => 'reiset', 'module' => 'banano'],
        /*----------  BONIFICACION MARUN REISET ----------*/
		/*----------  BONIFICACION  ----------*/
		'bonificacion/index' => ['controller' => 'Bonificacion', 'action' => 'index', 'module' => 'banano'],
		'bonificacion/configuracion' => ['controller' => 'Bonificacion', 'action' => 'configuracion', 'module' => 'banano'],
		'bonificacion/save/config' => ['controller' => 'Bonificacion', 'action' => 'save', 'module' => 'banano'],
		'bonificacion/save/umbrals' => ['controller' => 'Bonificacion', 'action' => 'saveUmbrals', 'module' => 'banano'],
		'bonificacion/historico' => ['controller' => 'Bonificacion', 'action' => 'getWeekYear', 'module' => 'banano'],
		/*----------  BONIFICACION  ----------*/
		/*----------  CALIDAD  ----------*/
        'calidad/index' => ['controller' => 'Calidad', 'action' => 'index', 'module' => 'banano'],
        'calidad/importar' => ['controller' => 'Qualitat', 'action' => 'importar', 'module' => 'banano'],
        'marcel/calidad/calidadLast' => ['controller' => 'Calidad', 'action' => 'calidadLast', 'path' => 'marcel', 'module' => 'banano'],
        'marcel/calidad/inicio' => ['controller' => 'Calidad', 'action' => 'index', 'path' => 'marcel', 'module' => 'banano'],
        'marcel/calidad/comentarios' => ['controller' => 'Calidad', 'action' => 'getComentariosImagenes', 'path' => 'marcel', 'module' => 'banano'],
        'marcel/calidad/last' => ['controller' => 'Calidad', 'action' => 'getLastWeek', 'path' => 'marcel', 'module' => 'banano'],
        'marcel/calidad/fotos' => ['controller' => 'Calidad', 'action' => 'fotos', 'path' => 'marcel', 'module' => 'banano'],
        'marcel/calidad/asignar' => ['controller' => 'Qualitat', 'action' => 'asignarIndices', 'module' => 'banano'],
		'marcel/calidad/comparativo' => ['controller' => 'Qualitat', 'action' => 'grafica_comparativo', 'module' => 'banano'],
		'palmar/calidad/calidadLast' => ['controller' => 'Calidad', 'action' => 'calidadLast', 'path' => 'palmar', 'module' => 'banano'],
        'palmar/calidad/inicio' => ['controller' => 'Calidad', 'action' => 'index', 'path' => 'palmar', 'module' => 'banano'],
        'palmar/calidad/comentarios' => ['controller' => 'Calidad', 'action' => 'getComentariosImagenes', 'path' => 'palmar', 'module' => 'banano'],
        'palmar/calidad/last' => ['controller' => 'Calidad', 'action' => 'getLastWeek', 'path' => 'palmar', 'module' => 'banano'],
		'palmar/calidad/fotos' => ['controller' => 'Calidad', 'action' => 'fotos', 'path' => 'palmar', 'module' => 'banano'],
		'palmar/calidad/tablaPrincipal' => ['controller' => 'Calidad', 'action' => 'tabla_principal', 'path' => 'palmar', 'module' => 'banano'],
		'save/barco/index' => ['controller' => 'Calidad', 'action' => 'saveBarco', 'module' => 'banano'],
		/*----------  CALIDAD  ----------*/
		/*----------  MERMA  ----------*/
		'merma/index' => ['controller' => 'Merma', 'action' => 'index', 'module' => 'banano'],
        'merma/lotes' => ['controller' => 'Merma', 'action' => 'Lotes', 'module' => 'banano'],
		'dia/index' => ['controller' => 'DiaMerma', 'action' => 'index', 'module' => 'banano'],
		'dia/last' => ['controller' => 'DiaMerma', 'action' => 'last', 'module' => 'banano'],

		'palmar/diamermaaaaa/index' => ['controller' => 'DiaMerma', 'action' => 'index', 'module' => 'banano', 'path' => 'palmar', 'module' => 'banano'],
		/*----------  MERMA  ----------*/
		/*----------  MERMA MARCEL  ----------*/
        'marun/merma/semanal' => ['controller' => 'Merma', 'action' => 'weeksHistories' , 'path' => 'marun', 'module' => 'banano'],
		/*----------  MERMA MARCEL ----------*/

		'cron/index' => ['controller' => 'CronJobs', 'action' => 'index', 'module' => 'banano'],
		'labores/zonas' => ['controller' => 'RLabores', 'action' => 'zonas', 'module' => 'banano'],
		'labores/fincas' => ['controller' => 'RLabores', 'action' => 'fincas', 'module' => 'banano'],
		'labores/lotes' => ['controller' => 'RLabores', 'action' => 'lotes', 'module' => 'banano'],
		/*----------  CALIDAD - EUROPA  ----------*/
		'qualitat/index' => ['controller' => 'Qualitat', 'action' => 'index', 'module' => 'banano'],
		/*----------  PRODUCCION  ----------*/
		'beneficio/costo/add' => ['controller' => 'Resumen', 'action' => 'costoBeneficio', 'module' => 'banano'],
		'produccion/index' => ['controller' => 'Produccion', 'action' => 'index', 'module' => 'banano'],
		'weekproduction/index' => ['controller' => 'ProduccionWeek', 'action' => 'index', 'module' => 'banano'],
		/*----------  PERSONAL LABORES AGRICOLAS  ----------*/
		'agricolas/index' => ['controller' => 'LPersonal', 'action' => 'index', 'module' => 'banano'],
		/*----------  PERSONAL LABORES AGRICOLAS  ----------*/
		/*----------  REFACTORIZAR  ----------*/
		'factorizar/index' => ['controller' => 'Refactorizar', 'action' => 'index', 'module' => 'banano'],
		'factorizar/agroaudit/index' => ['controller' => 'RefactorizarAgroaudit', 'action' => 'index', 'module' => 'banano'],
		/*----------  REFACTORIZAR  ----------*/
		/*----------  REVISION  ----------*/
		'revision/racimos/index' => ['controller' => 'Revision', 'action' => 'racimos', 'module' => 'banano'],
		'cambiar/revision/racimos' => ['controller' => 'Revision', 'action' => 'saveRacimos', 'module' => 'banano'],
		'revision/asistencia/index' => ['controller' => 'Revision', 'action' => 'indexAsistencia', 'module' => 'banano'],
		'cambiar/revision/asistencia' => ['controller' => 'Revision', 'action' => 'saveAsistencia', 'module' => 'banano'],
		'revision/asistencia/excel/index' => ['controller' => 'Revision', 'action' => 'indexAsistencia2', 'module' => 'banano'],

		/*----------  REVISION  ----------*/
		/*----------  REVISION ASISTENCIA QUINTANA  ----------*/
		'revision/quintana/asistencia/index' => ['controller' => 'Asistencia', 'action' => 'index' , 'path' => 'quintana', 'module' => 'banano'],
		/*----------  REVISION ASISTENCIA QUINTANA  ----------*/

		/*----------  REVISION ASISTENCIA MARCEL  ----------*/
		'marcel/asistencia/index' => ['controller' => 'Asistencia', 'action' => 'index' , 'path' => 'marcel', 'module' => 'banano'],
		'marcel/asistencia/save' => ['controller' => 'Asistencia', 'action' => 'save', 'path' => 'marcel', 'module' => 'banano'],
		/*----------  REVISION ASISTENCIA MARCEL  ----------*/

		/*----------  ASISTENCIA QUINTANA  ----------*/
		'quintana/asistencia/index' => ['controller' => 'Asistencia', 'action' => 'indexAsistencia', 'path' => 'quintana', 'module' => 'banano'],
		'quintana/asistencia/save' => ['controller' => 'Asistencia', 'action' => 'save', 'path' => 'quintana', 'module' => 'banano'],
		/*----------  ASISTENCIA QUINTANA  ----------*/

		/*----------  BEGIN PRONTOFORMS  ----------*/
		/*----------  END PRONTOFORMS  ----------*/

		/* BEGIN MIGRAR */
		/* BEGIN ASISTENCIA */
		'asistencia/index' => ['controller' => 'Asistencia', 'action' => 'index', 'module' => 'banano'],
		'asistencia/save' => ['controller' => 'Asistencia', 'action' => 'save', 'module' => 'banano'],
		/* END ASISTENCIA */

		/*----------  BEGIN MARIO  ----------*/
		'prontoforms/mario/list/personal' => ['controller' => 'Prontoforms', 'action' => 'getPersonalMario', 'module' => 'banano'],
		'prontoforms/mario/list/labores' => ['controller' => 'Prontoforms', 'action' => 'getLaboresMario', 'module' => 'banano'],
		'prontoforms/mario/list/cables' => ['controller' => 'Prontoforms', 'action' => 'getCablesMario', 'module' => 'banano'],
		/*----------  END MARIO  ----------*/

		/* BEGIN CONFIGURACION */
		'configuracion/marcas/list' => ['controller' => 'Configuracion', 'action' => 'listMarcas', 'module' => 'banano'],
		'configuracion/marcas/changeStatus' => ['controller' => 'Configuracion', 'action' => 'changeStatusMarca', 'module' => 'banano'],
		'configuracion/marcas/archive' => ['controller' => 'Configuracion', 'action' => 'archiveMarca', 'module' => 'banano'],
		'configuracion/get/marca' => ['controller' => 'Configuracion', 'action' => 'indexEditMarca', 'module' => 'banano'],
		'configuracion/save/marca' => ['controller' => 'Configuracion', 'action' => 'saveMarca', 'module' => 'banano'],

		'configuracion/categorias/list' => ['controller' => 'Configuracion', 'action' => 'listCategorias', 'module' => 'banano'],
		'configuracion/categorias/changeStatus' => ['controller' => 'Configuracion', 'action' => 'changeStatusCategoria', 'module' => 'banano'],
		'configuracion/categorias/archive' => ['controller' => 'Configuracion', 'action' => 'archiveCategoria', 'module' => 'banano'],
		'configuracion/get/categoria' => ['controller' => 'Configuracion', 'action' => 'indexEditCategoria', 'module' => 'banano'],
		'configuracion/save/categoria' => ['controller' => 'Configuracion', 'action' => 'saveCategoria', 'module' => 'banano'],

		'configuracion/defectos/list' => ['controller' => 'Configuracion', 'action' => 'listDefectos', 'module' => 'banano'],
		'configuracion/defectos/changeStatus' => ['controller' => 'Configuracion', 'action' => 'changeStatusDefecto', 'module' => 'banano'],
		'configuracion/defectos/archive' => ['controller' => 'Configuracion', 'action' => 'archiveDefecto', 'module' => 'banano'],
		'configuracion/get/defecto' => ['controller' => 'Configuracion', 'action' => 'indexEditDefecto', 'module' => 'banano'],
		'configuracion/save/defecto' => ['controller' => 'Configuracion', 'action' => 'saveDefecto', 'module' => 'banano'],

		'configuracion/exportadores/list' => ['controller' => 'Configuracion', 'action' => 'listExportadores', 'module' => 'banano'],
		'configuracion/exportadores/changeStatus' => ['controller' => 'Configuracion', 'action' => 'changeStatusExportador', 'module' => 'banano'],
		'configuracion/exportadores/archive' => ['controller' => 'Configuracion', 'action' => 'archiveExportador', 'module' => 'banano'],
		'configuracion/get/exportador' => ['controller' => 'Configuracion', 'action' => 'indexEditExportador', 'module' => 'banano'],
		'configuracion/save/exportador' => ['controller' => 'Configuracion', 'action' => 'saveExportador', 'module' => 'banano'],

		'configuracion/destinos/list' => ['controller' => 'Configuracion', 'action' => 'listDestinos', 'module' => 'banano'],
		'configuracion/destinos/changeStatus' => ['controller' => 'Configuracion', 'action' => 'changeStatusDestino', 'module' => 'banano'],
		'configuracion/destinos/archive' => ['controller' => 'Configuracion', 'action' => 'archiveDestino', 'module' => 'banano'],
		'configuracion/get/destino' => ['controller' => 'Configuracion', 'action' => 'indexEditDestino', 'module' => 'banano'],
		'configuracion/save/destino' => ['controller' => 'Configuracion', 'action' => 'saveDestino', 'module' => 'banano'],
		/* END CONFIGURACION */

        /* CALIDAD MARUN */
        'marun/calidad/home' => ['controller' => 'CalidadMarun', 'action' => 'index', 'module' => 'banano'],
		'marun/calidad/main' => ['controller' => 'CalidadMarun', 'action' => 'Principal', 'module' => 'banano'],
		'marun/calidad/cluster/caja' => ['controller' => 'CalidadMarun', 'action' => 'historicaClusterCaja', 'module' => 'banano'],
		'marun/calidad/table/cluster' => ['controller' => 'CalidadMarun', 'action' => 'tablePromedioClusters', 'module' => 'banano'],
		'marun/calidad/table/dedos' => ['controller' => 'CalidadMarun', 'action' => 'tablePromedioDedos', 'module' => 'banano'],
		'marun/calidad/defectos' => ['controller' => 'CalidadMarun', 'action' => 'graficaDanhos', 'module' => 'banano'],
		'marun/calidad/table/defectos' => ['controller' => 'CalidadMarun', 'action' => 'getTableSubFinca', 'module' => 'banano'],
        /* CALIDAD MARUN */

        /* CALIDAD QUINTANA */
        'quintana/calidad/last' => ['controller' => 'CalidadQuintana', 'action' => 'last', 'module' => 'banano'],
        'quintana/calidad/home' => ['controller' => 'CalidadQuintana', 'action' => 'index', 'module' => 'banano'],
		'quintana/calidad/main' => ['controller' => 'CalidadQuintana', 'action' => 'Principal', 'module' => 'banano'],
		'quintana/calidad/cluster/caja' => ['controller' => 'CalidadQuintana', 'action' => 'historicaClusterCaja', 'module' => 'banano'],
		'quintana/calidad/table/cluster' => ['controller' => 'CalidadQuintana', 'action' => 'tablePromedioClusters', 'module' => 'banano'],
		'quintana/calidad/table/dedos' => ['controller' => 'CalidadQuintana', 'action' => 'tablePromedioDedos', 'module' => 'banano'],
		'quintana/calidad/defectos' => ['controller' => 'CalidadQuintana', 'action' => 'graficaDanhos', 'module' => 'banano'],
        'quintana/calidad/table/defectos' => ['controller' => 'CalidadQuintana', 'action' => 'getTableSubFinca', 'module' => 'banano'],
        
        /* CALIDAD SUMIFRU */
        'sumifru/calidad/last' => ['controller' => 'Calidad', 'action' => 'last', 'module' => 'banano', 'path' => 'sumifru'],
        'sumifru/calidad/home' => ['controller' => 'Calidad', 'action' => 'index', 'module' => 'banano', 'path' => 'sumifru'],
        'sumifru/calidad/main' => ['controller' => 'Calidad', 'action' => 'Principal', 'module' => 'banano', 'path' => 'sumifru'],
        'sumifru/calidad/fotos' => ['controller' => 'Calidad', 'action' => 'fotos', 'module' => 'banano', 'path' => 'sumifru'],
        'sumifru/calidad/pesoCluster' => ['controller' => 'Calidad', 'action' => 'pesoCluster', 'module' => 'banano', 'path' => 'sumifru'],
        'sumifru/calidad/empaque' => ['controller' => 'Calidad', 'action' => 'empaque', 'module' => 'banano', 'path' => 'sumifru'],
		'sumifru/calidad/cluster/caja' => ['controller' => 'Calidad', 'action' => 'historicaClusterCaja', 'module' => 'banano', 'path' => 'sumifru'],
		'sumifru/calidad/table/cluster' => ['controller' => 'Calidad', 'action' => 'tablePromedioClusters', 'module' => 'banano', 'path' => 'sumifru'],
		'sumifru/calidad/table/dedos' => ['controller' => 'Calidad', 'action' => 'tablePromedioDedos', 'module' => 'banano', 'path' => 'sumifru'],
		'sumifru/calidad/defectos' => ['controller' => 'Calidad', 'action' => 'graficaDanhos', 'module' => 'banano', 'path' => 'sumifru'],
		'sumifru/calidad/table/defectos' => ['controller' => 'Calidad', 'action' => 'getTableSubFinca', 'module' => 'banano', 'path' => 'sumifru'],

		'sumifru/calidad2/last' => ['controller' => 'Calidad2', 'action' => 'last', 'module' => 'banano', 'path' => 'sumifru'],
		'sumifru/calidad2/variables' => ['controller' => 'Calidad2', 'action' => 'variables', 'module' => 'banano', 'path' => 'sumifru'],
        'sumifru/calidad2/index' => ['controller' => 'Calidad2', 'action' => 'index', 'module' => 'banano', 'path' => 'sumifru'],
		/*----------  PRODUCCION  ----------*/
		'quintana/beneficio/costo/add' => ['controller' => 'Resumen', 'action' => 'costoBeneficio' , 'path' => 'quintana', 'module' => 'banano'],
		'quintana/semanaproduction/index' => ['controller' => 'ProduccionWeek', 'action' => 'index' , 'path' => 'quintana', 'module' => 'banano'],
		'quintana/save/ha' => ['controller' => 'ProduccionWeek', 'action' => 'saveHA' , 'path' => 'quintana', 'module' => 'banano'],

		'quintana/production/index' => ['controller' => 'Produccion', 'action' => 'index' , 'path' => 'quintana', 'module' => 'banano'],
		'quintana/produccion/last' => ['controller' => 'Produccion', 'action' => 'lastDay' , 'path' => 'quintana', 'module' => 'banano'],
        'quintana/produccion/historico' => ['controller' => 'Produccion', 'action' => 'historico' , 'path' => 'quintana', 'module' => 'banano'],
        'quintana/produccion/formularios' => ['controller' => 'Produccion', 'action' => 'racimosFolmularios' , 'path' => 'quintana', 'module' => 'banano'],
        'quintana/produccion/cuadre' => ['controller' => 'Produccion', 'action' => 'cuadreRacimos' , 'path' => 'quintana', 'module' => 'banano'],
        'quintana/produccion/cuadrar' => ['controller' => 'Produccion', 'action' => 'cuadrarRacimos' , 'path' => 'quintana', 'module' => 'banano'],
        'quintana/produccion/eliminar' => ['controller' => 'Produccion', 'action' => 'eliminar' , 'path' => 'quintana', 'module' => 'banano'],
        'quintana/produccion/editar' => ['controller' => 'Produccion', 'action' => 'editar' , 'path' => 'quintana', 'module' => 'banano'],
        'quintana/produccion/recusados' => ['controller' => 'Produccion', 'action' => 'analizisRecusados', 'path' => 'quintana', 'module' => 'banano'],
		'quintana/produccion/diarecusados' => ['controller' => 'Produccion', 'action' => 'defectos', 'path' => 'quintana', 'module' => 'banano'],
		'quintana/produccion/edad' => ['controller' => 'Produccion', 'action' => 'racimosEdad' , 'path' => 'quintana', 'module' => 'banano'],
		'quintana/produccion/lote' => ['controller' => 'Produccion', 'action' => 'racimosLote' , 'path' => 'quintana', 'module' => 'banano'],

        'marcel/produccion/importar' => ['controller' => 'Produccion', 'action' => 'importar' , 'path' => 'marcel', 'module' => 'banano'],
		'marcel/produccion/lote' => ['controller' => 'Produccion', 'action' => 'index' , 'path' => 'marcel', 'module' => 'banano'],
		'marcel/produccion/palanca' => ['controller' => 'Produccion', 'action' => 'racimosPalanca' , 'path' => 'marcel', 'module' => 'banano'],
		'marcel/produccion/edad' => ['controller' => 'Produccion', 'action' => 'racimosEdad' , 'path' => 'marcel', 'module' => 'banano'],
		'marcel/produccion/last' => ['controller' => 'Produccion', 'action' => 'lastDay' , 'path' => 'marcel', 'module' => 'banano'],
        'marcel/produccion/historico' => ['controller' => 'Produccion', 'action' => 'historico' , 'path' => 'marcel', 'module' => 'banano'],
        'marcel/produccion/formularios' => ['controller' => 'Produccion', 'action' => 'racimosFolmularios' , 'path' => 'marcel', 'module' => 'banano'],
        'marcel/produccion/cuadre' => ['controller' => 'Produccion', 'action' => 'cuadreRacimos' , 'path' => 'marcel', 'module' => 'banano'],
        'marcel/produccion/cuadrar' => ['controller' => 'Produccion', 'action' => 'cuadrarRacimos' , 'path' => 'marcel', 'module' => 'banano'],
        'marcel/produccion/eliminar' => ['controller' => 'Produccion', 'action' => 'eliminar' , 'path' => 'marcel', 'module' => 'banano'],
        'marcel/produccion/editar' => ['controller' => 'Produccion', 'action' => 'editar' , 'path' => 'marcel', 'module' => 'banano'],
        'marcel/produccion/promedios' => ['controller' => 'Produccion', 'action' => 'promediosLotes', 'path' => 'marcel', 'module' => 'banano'],
        'marcel/produccion/recusados' => ['controller' => 'Produccion', 'action' => 'analizisRecusados', 'path' => 'marcel', 'module' => 'banano'],
        'marcel/produccion/viajes' => ['controller' => 'Produccion', 'action' => 'racimosPorViaje', 'path' => 'marcel', 'module' => 'banano'],
        'marcel/produccion/guardarViaje' => ['controller' => 'Produccion', 'action' => 'guardarViaje', 'path' => 'marcel', 'module' => 'banano'],
        'marcel/produccion/procesarViaje' => ['controller' => 'Produccion', 'action' => 'procesarViaje', 'path' => 'marcel', 'module' => 'banano'],
        'marcel/produccion/desprocesarViaje' => ['controller' => 'Produccion', 'action' => 'desprocesarViaje', 'path' => 'marcel', 'module' => 'banano'],
        'marcel/produccion/dataByEdad' => ['controller' => 'ProduccionDemo', 'action' => 'racimosEdad' , 'path' => 'marcel', 'module' => 'banano'],
        'marcel/produccion/crearViaje' => ['controller' => 'Produccion', 'action' => 'crearBalazaDeFormulario', 'path' => 'marcel', 'module' => 'banano'],
		'marcel/produccion/unir' => ['controller' => 'Produccion', 'action' => 'unirBalanzaFormulario', 'path' => 'marcel', 'module' => 'banano'],
		'marcel/produccion/cambiarLote' => ['controller' => 'Produccion', 'action' => 'cambiarLote', 'path' => 'marcel', 'module' => 'banano'],
		'marcel/produccion/cambiarCuadrilla' => ['controller' => 'Produccion', 'action' => 'cambiarCuadrilla', 'path' => 'marcel', 'module' => 'banano'],
		'marcel/produccion/diarecusados' => ['controller' => 'Produccion', 'action' => 'defectos', 'path' => 'marcel', 'module' => 'banano'],

		'marcel/formulariosracimos/resumen' => ['controller' => 'ProduccionRacimosFormularios', 'action' => 'resumen' , 'path' => 'marcel', 'module' => 'banano'],
        'marcel/formulariosracimos/edades' => ['controller' => 'ProduccionRacimosFormularios', 'action' => 'racimosEdad' , 'path' => 'marcel', 'module' => 'banano'],
        'marcel/formulariosracimos/historico' => ['controller' => 'ProduccionRacimosFormularios', 'action' => 'historico' , 'path' => 'marcel', 'module' => 'banano'],
        'marcel/formulariosracimos/last' => ['controller' => 'ProduccionRacimosFormularios', 'action' => 'lastDay' , 'path' => 'marcel', 'module' => 'banano'],
        'marcel/formulariosracimos/tags' => ['controller' => 'ProduccionRacimosFormularios', 'action' => 'tags' , 'path' => 'marcel', 'module' => 'banano'],
        'marcel/formulariosracimos/eliminar' => ['controller' => 'ProduccionRacimosFormularios', 'action' => 'eliminar' , 'path' => 'marcel', 'module' => 'banano'],
        'marcel/formulariosracimos/editar' => ['controller' => 'ProduccionRacimosFormularios', 'action' => 'editar' , 'path' => 'marcel', 'module' => 'banano'],
        'marcel/formulariosracimos/recusados' => ['controller' => 'ProduccionRacimosFormularios', 'action' => 'analizisRecusados', 'path' => 'marcel', 'module' => 'banano'],
		'marcel/formulariosracimos/diarecusados' => ['controller' => 'ProduccionRacimosFormularios', 'action' => 'defectos', 'path' => 'marcel', 'module' => 'banano'],
		'marcel/formulariosracimos/muestreo' => ['controller' => 'ProduccionRacimosFormularios', 'action' => 'muestreo', 'path' => 'marcel', 'module' => 'banano'],
		'marcel/formulariosracimos/variableViaje' => ['controller' => 'ProduccionRacimosFormularios', 'action' => 'editarVariableViaje', 'path' => 'marcel', 'module' => 'banano'],
		'marcel/formulariosracimos/borrarViaje' => ['controller' => 'ProduccionRacimosFormularios', 'action' => 'borrarViaje', 'path' => 'marcel', 'module' => 'banano'],
		'marcel/racimos/borrarViaje' => ['controller' => 'ProduccionRacimosFormularios', 'action' => 'borrarViajeBalanza', 'path' => 'marcel', 'module' => 'banano'],
		'marcel/formulariosracimos/agregarRacimo' => ['controller' => 'ProduccionRacimosFormularios', 'action' => 'agregarRacimo', 'path' => 'marcel', 'module' => 'banano'],
		'marcel/formulariosracimos/procesar' => ['controller' => 'ProduccionRacimosFormularios', 'action' => 'procesarRacimos', 'path' => 'marcel', 'module' => 'banano'],

		'palmar/formulariosracimos/resumen' => ['controller' => 'ProduccionRacimosFormularios', 'action' => 'resumen' , 'path' => 'palmar', 'module' => 'banano'],
        'palmar/formulariosracimos/edades' => ['controller' => 'ProduccionRacimosFormularios', 'action' => 'racimosEdad' , 'path' => 'palmar', 'module' => 'banano'],
        'palmar/formulariosracimos/historico' => ['controller' => 'ProduccionRacimosFormularios', 'action' => 'historico' , 'path' => 'palmar', 'module' => 'banano'],
        'palmar/formulariosracimos/last' => ['controller' => 'ProduccionRacimosFormularios', 'action' => 'lastDay' , 'path' => 'palmar', 'module' => 'banano'],
        'palmar/formulariosracimos/tags' => ['controller' => 'ProduccionRacimosFormularios', 'action' => 'tags' , 'path' => 'palmar', 'module' => 'banano'],
        'palmar/formulariosracimos/eliminar' => ['controller' => 'ProduccionRacimosFormularios', 'action' => 'eliminar' , 'path' => 'palmar', 'module' => 'banano'],
        'palmar/formulariosracimos/editar' => ['controller' => 'ProduccionRacimosFormularios', 'action' => 'editar' , 'path' => 'palmar', 'module' => 'banano'],
        'palmar/formulariosracimos/recusados' => ['controller' => 'ProduccionRacimosFormularios', 'action' => 'analizisRecusados', 'path' => 'palmar', 'module' => 'banano'],
		'palmar/formulariosracimos/diarecusados' => ['controller' => 'ProduccionRacimosFormularios', 'action' => 'defectos', 'path' => 'palmar', 'module' => 'banano'],
		'palmar/formulariosracimos/muestreo' => ['controller' => 'ProduccionRacimosFormularios', 'action' => 'muestreo', 'path' => 'palmar', 'module' => 'banano'],
		'palmar/formulariosracimos/variableViaje' => ['controller' => 'ProduccionRacimosFormularios', 'action' => 'editarVariableViaje', 'path' => 'palmar', 'module' => 'banano'],
		'palmar/formulariosracimos/borrarViaje' => ['controller' => 'ProduccionRacimosFormularios', 'action' => 'borrarViaje', 'path' => 'palmar', 'module' => 'banano'],

        'reiset/produccion/filters' => ['controller' => 'Produccion', 'action' => 'filters' , 'path' => 'reiset', 'module' => 'banano'],
        'reiset/produccion/importar' => ['controller' => 'Produccion', 'action' => 'importar' , 'path' => 'reiset', 'module' => 'banano'],
		'reiset/produccion/lote' => ['controller' => 'Produccion', 'action' => 'index' , 'path' => 'reiset', 'module' => 'banano'],
		'reiset/produccion/palanca' => ['controller' => 'Produccion', 'action' => 'racimosPalanca' , 'path' => 'reiset', 'module' => 'banano'],
		'reiset/produccion/edad' => ['controller' => 'Produccion', 'action' => 'racimosEdad' , 'path' => 'reiset', 'module' => 'banano'],
		'reiset/produccion/last' => ['controller' => 'Produccion', 'action' => 'lastDay' , 'path' => 'reiset', 'module' => 'banano'],
        'reiset/produccion/historico' => ['controller' => 'Produccion', 'action' => 'historico' , 'path' => 'reiset', 'module' => 'banano'],
        'reiset/produccion/formularios' => ['controller' => 'Produccion', 'action' => 'racimosFolmularios' , 'path' => 'reiset', 'module' => 'banano'],
        'reiset/produccion/cuadre' => ['controller' => 'Produccion', 'action' => 'cuadreRacimos' , 'path' => 'reiset', 'module' => 'banano'],
        'reiset/produccion/cuadrar' => ['controller' => 'Produccion', 'action' => 'cuadrarRacimos' , 'path' => 'reiset', 'module' => 'banano'],
        'reiset/produccion/eliminar' => ['controller' => 'Produccion', 'action' => 'eliminar' , 'path' => 'reiset', 'module' => 'banano'],
        'reiset/produccion/editar' => ['controller' => 'Produccion', 'action' => 'editar' , 'path' => 'reiset', 'module' => 'banano'],
        'reiset/produccion/promedios' => ['controller' => 'Produccion', 'action' => 'promediosLotes', 'path' => 'reiset', 'module' => 'banano'],
		'reiset/produccion/recusados' => ['controller' => 'Produccion', 'action' => 'analizisRecusados', 'path' => 'reiset', 'module' => 'banano'],
		
		'sumifru/produccion/lote' => ['controller' => 'Produccion', 'action' => 'index' , 'path' => 'sumifru', 'module' => 'banano'],
		'sumifru/produccion/palanca' => ['controller' => 'Produccion', 'action' => 'racimosPalanca' , 'path' => 'sumifru', 'module' => 'banano'],
		'sumifru/produccion/edad' => ['controller' => 'Produccion', 'action' => 'racimosEdad' , 'path' => 'sumifru', 'module' => 'banano'],
		'sumifru/produccion/last' => ['controller' => 'Produccion', 'action' => 'lastDay' , 'path' => 'sumifru', 'module' => 'banano'],
        'sumifru/produccion/historico' => ['controller' => 'Produccion', 'action' => 'historico' , 'path' => 'sumifru', 'module' => 'banano'],
        'sumifru/produccion/cuadre' => ['controller' => 'Produccion', 'action' => 'cuadreRacimos' , 'path' => 'sumifru', 'module' => 'banano'],
        'sumifru/produccion/cuadrar' => ['controller' => 'Produccion', 'action' => 'cuadrarRacimos' , 'path' => 'sumifru', 'module' => 'banano'],
        'sumifru/produccion/eliminar' => ['controller' => 'Produccion', 'action' => 'eliminar' , 'path' => 'sumifru', 'module' => 'banano'],
        'sumifru/produccion/editar' => ['controller' => 'Produccion', 'action' => 'editar' , 'path' => 'sumifru', 'module' => 'banano'],
        'sumifru/produccion/promedios' => ['controller' => 'Produccion', 'action' => 'promediosLotes', 'path' => 'sumifru', 'module' => 'banano'],
        'sumifru/produccion/recusados' => ['controller' => 'Produccion', 'action' => 'analizisRecusados', 'path' => 'sumifru', 'module' => 'banano'],
		'sumifru/produccion/diarecusados' => ['controller' => 'Produccion', 'action' => 'defectos', 'path' => 'sumifru', 'module' => 'banano'],
		'sumifru/produccion/muestreo' => ['controller' => 'Produccion', 'action' => 'muestreo', 'path' => 'sumifru', 'module' => 'banano'],
		'sumifru/racimos/pesoCalibre' => ['controller' => 'Produccion', 'action' => 'pesoCalibre', 'path' => 'sumifru', 'module' => 'banano'],
		'sumifru/racimos/pesoMano' => ['controller' => 'Produccion', 'action' => 'pesoMano', 'path' => 'sumifru', 'module' => 'banano'],
		'sumifru/racimos/pesoRacimo' => ['controller' => 'Produccion', 'action' => 'pesoRacimo', 'path' => 'sumifru', 'module' => 'banano'],
		'sumifru/racimos/cantidadRacimoCalibreManos' => ['controller' => 'Produccion', 'action' => 'cantidadRacimoCalibreManos', 'path' => 'sumifru', 'module' => 'banano'],

		'orodelti/produccion/lote' => ['controller' => 'Produccion', 'action' => 'index' , 'path' => 'orodelti', 'module' => 'banano'],
		'orodelti/produccion/palanca' => ['controller' => 'Produccion', 'action' => 'racimosPalanca' , 'path' => 'orodelti', 'module' => 'banano'],
		'orodelti/produccion/edad' => ['controller' => 'Produccion', 'action' => 'racimosEdad' , 'path' => 'orodelti', 'module' => 'banano'],
		'orodelti/produccion/last' => ['controller' => 'Produccion', 'action' => 'lastDay' , 'path' => 'orodelti', 'module' => 'banano'],
        'orodelti/produccion/historico' => ['controller' => 'Produccion', 'action' => 'historico' , 'path' => 'orodelti', 'module' => 'banano'],
        'orodelti/produccion/cuadre' => ['controller' => 'Produccion', 'action' => 'cuadreRacimos' , 'path' => 'orodelti', 'module' => 'banano'],
        'orodelti/produccion/cuadrar' => ['controller' => 'Produccion', 'action' => 'cuadrarRacimos' , 'path' => 'orodelti', 'module' => 'banano'],
        'orodelti/produccion/eliminar' => ['controller' => 'Produccion', 'action' => 'eliminar' , 'path' => 'orodelti', 'module' => 'banano'],
        'orodelti/produccion/editar' => ['controller' => 'Produccion', 'action' => 'editar' , 'path' => 'orodelti', 'module' => 'banano'],
        'orodelti/produccion/promedios' => ['controller' => 'Produccion', 'action' => 'promediosLotes', 'path' => 'orodelti', 'module' => 'banano'],
        'orodelti/produccion/recusados' => ['controller' => 'Produccion', 'action' => 'analizisRecusados', 'path' => 'orodelti', 'module' => 'banano'],
		'orodelti/produccion/diarecusados' => ['controller' => 'Produccion', 'action' => 'defectos', 'path' => 'orodelti', 'module' => 'banano'],
		'orodelti/produccion/muestreo' => ['controller' => 'Produccion', 'action' => 'muestreo', 'path' => 'orodelti', 'module' => 'banano'],

		'arregui/produccion/lote' => ['controller' => 'Produccion', 'action' => 'index' , 'path' => 'arregui', 'module' => 'banano'],
		'arregui/produccion/palanca' => ['controller' => 'Produccion', 'action' => 'racimosPalanca' , 'path' => 'arregui', 'module' => 'banano'],
		'arregui/produccion/edad' => ['controller' => 'Produccion', 'action' => 'racimosEdad' , 'path' => 'arregui', 'module' => 'banano'],
		'arregui/produccion/last' => ['controller' => 'Produccion', 'action' => 'lastDay' , 'path' => 'arregui', 'module' => 'banano'],
        'arregui/produccion/historico' => ['controller' => 'Produccion', 'action' => 'historico' , 'path' => 'arregui', 'module' => 'banano'],
        'arregui/produccion/cuadre' => ['controller' => 'Produccion', 'action' => 'cuadreRacimos' , 'path' => 'arregui', 'module' => 'banano'],
        'arregui/produccion/cuadrar' => ['controller' => 'Produccion', 'action' => 'cuadrarRacimos' , 'path' => 'arregui', 'module' => 'banano'],
        'arregui/produccion/eliminar' => ['controller' => 'Produccion', 'action' => 'eliminar' , 'path' => 'arregui', 'module' => 'banano'],
        'arregui/produccion/editar' => ['controller' => 'Produccion', 'action' => 'editar' , 'path' => 'arregui', 'module' => 'banano'],
        'arregui/produccion/promedios' => ['controller' => 'Produccion', 'action' => 'promediosLotes', 'path' => 'arregui', 'module' => 'banano'],
        'arregui/produccion/recusados' => ['controller' => 'Produccion', 'action' => 'analizisRecusados', 'path' => 'arregui', 'module' => 'banano'],
		'arregui/produccion/diarecusados' => ['controller' => 'Produccion', 'action' => 'defectos', 'path' => 'arregui', 'module' => 'banano'],
		'arregui/produccion/muestreo' => ['controller' => 'Produccion', 'action' => 'muestreo', 'path' => 'arregui', 'module' => 'banano'],

        'marun/produccion/lote' => ['controller' => 'Produccion', 'action' => 'index' , 'path' => 'marun', 'module' => 'banano'],
		'marun/produccion/palanca' => ['controller' => 'Produccion', 'action' => 'racimosPalanca' , 'path' => 'marun', 'module' => 'banano'],
		'marun/produccion/edad' => ['controller' => 'Produccion', 'action' => 'racimosEdad' , 'path' => 'marun', 'module' => 'banano'],
		'marun/produccion/last' => ['controller' => 'Produccion', 'action' => 'lastDay' , 'path' => 'marun', 'module' => 'banano'],
        'marun/produccion/historico' => ['controller' => 'Produccion', 'action' => 'historico' , 'path' => 'marun', 'module' => 'banano'],
        'marun/produccion/formularios' => ['controller' => 'Produccion', 'action' => 'racimosFolmularios' , 'path' => 'marun', 'module' => 'banano'],
        'marun/produccion/cuadre' => ['controller' => 'Produccion', 'action' => 'cuadreRacimos' , 'path' => 'marun', 'module' => 'banano'],
        'marun/produccion/cuadrar' => ['controller' => 'Produccion', 'action' => 'cuadrarRacimos' , 'path' => 'marun', 'module' => 'banano'],
        'marun/produccion/eliminar' => ['controller' => 'Produccion', 'action' => 'eliminar' , 'path' => 'marun', 'module' => 'banano'],
        'marun/produccion/editar' => ['controller' => 'Produccion', 'action' => 'editar' , 'path' => 'marun', 'module' => 'banano'],
        'marun/produccion/promedios' => ['controller' => 'Produccion', 'action' => 'promediosLotes', 'path' => 'marun', 'module' => 'banano'],
        'marun/produccion/recusados' => ['controller' => 'Produccion', 'action' => 'analizisRecusados', 'path' => 'marun', 'module' => 'banano'],
        'marun/produccion/viajes' => ['controller' => 'Produccion', 'action' => 'racimosPorViaje', 'path' => 'marun', 'module' => 'banano'],
        'marun/produccion/guardarViaje' => ['controller' => 'Produccion', 'action' => 'guardarViaje', 'path' => 'marun', 'module' => 'banano'],
        'marun/produccion/procesarViaje' => ['controller' => 'Produccion', 'action' => 'procesarViaje', 'path' => 'marun', 'module' => 'banano'],
		'marun/produccion/desprocesarViaje' => ['controller' => 'Produccion', 'action' => 'desprocesarViaje', 'path' => 'marun', 'module' => 'banano'],
		'marun/produccion/crearViaje' => ['controller' => 'Produccion', 'action' => 'crearBalazaDeFormulario', 'path' => 'marun', 'module' => 'banano'],
		'marun/produccion/unir' => ['controller' => 'Produccion', 'action' => 'unirBalanzaFormulario', 'path' => 'marun', 'module' => 'banano'],
		'marun/produccion/cambiarLote' => ['controller' => 'Produccion', 'action' => 'cambiarLote', 'path' => 'marun', 'module' => 'banano'],
        'marun/produccion/cambiarCuadrilla' => ['controller' => 'Produccion', 'action' => 'cambiarCuadrilla', 'path' => 'marun', 'module' => 'banano'],

        'marun/produccion2/filters' => ['controller' => 'Produccion2', 'action' => 'filters' , 'path' => 'marun', 'module' => 'banano'],
        'marun/produccion2/lote' => ['controller' => 'Produccion2', 'action' => 'index' , 'path' => 'marun', 'module' => 'banano'],
		'marun/produccion2/palanca' => ['controller' => 'Produccion2', 'action' => 'racimosPalanca' , 'path' => 'marun', 'module' => 'banano'],
		'marun/produccion2/edad' => ['controller' => 'Produccion2', 'action' => 'racimosEdad' , 'path' => 'marun', 'module' => 'banano'],
		'marun/produccion2/last' => ['controller' => 'Produccion2', 'action' => 'lastDay' , 'path' => 'marun', 'module' => 'banano'],
        'marun/produccion2/historico' => ['controller' => 'Produccion2', 'action' => 'historico' , 'path' => 'marun', 'module' => 'banano'],
        'marun/produccion2/formularios' => ['controller' => 'Produccion2', 'action' => 'racimosFolmularios' , 'path' => 'marun', 'module' => 'banano'],
        'marun/produccion2/cuadre' => ['controller' => 'Produccion2', 'action' => 'cuadreRacimos' , 'path' => 'marun', 'module' => 'banano'],
        'marun/produccion2/cuadrar' => ['controller' => 'Produccion2', 'action' => 'cuadrarRacimos' , 'path' => 'marun', 'module' => 'banano'],
        'marun/produccion2/eliminar' => ['controller' => 'Produccion2', 'action' => 'eliminar' , 'path' => 'marun', 'module' => 'banano'],
        'marun/produccion2/editar' => ['controller' => 'Produccion2', 'action' => 'editar' , 'path' => 'marun', 'module' => 'banano'],
        'marun/produccion2/promedios' => ['controller' => 'Produccion2', 'action' => 'promediosLotes', 'path' => 'marun', 'module' => 'banano'],
        'marun/produccion2/recusados' => ['controller' => 'Produccion2', 'action' => 'analizisRecusados', 'path' => 'marun', 'module' => 'banano'],
        'marun/produccion2/viajes' => ['controller' => 'Produccion2', 'action' => 'racimosPorViaje', 'path' => 'marun', 'module' => 'banano'],
        'marun/produccion2/guardarViaje' => ['controller' => 'Produccion2', 'action' => 'guardarViaje', 'path' => 'marun', 'module' => 'banano'],
        'marun/produccion2/procesarViaje' => ['controller' => 'Produccion2', 'action' => 'procesarViaje', 'path' => 'marun', 'module' => 'banano'],
		'marun/produccion2/desprocesarViaje' => ['controller' => 'Produccion2', 'action' => 'desprocesarViaje', 'path' => 'marun', 'module' => 'banano'],
		'marun/produccion2/crearViaje' => ['controller' => 'Produccion2', 'action' => 'crearBalazaDeFormulario', 'path' => 'marun', 'module' => 'banano'],
		'marun/produccion2/unir' => ['controller' => 'Produccion2', 'action' => 'unirBalanzaFormulario', 'path' => 'marun', 'module' => 'banano'],
		'marun/produccion2/cambiarLote' => ['controller' => 'Produccion2', 'action' => 'cambiarLote', 'path' => 'marun', 'module' => 'banano'],
        'marun/produccion2/cambiarCuadrilla' => ['controller' => 'Produccion2', 'action' => 'cambiarCuadrilla', 'path' => 'marun', 'module' => 'banano'],
		
		'marun/racimos/saveImg' => ['controller' => 'ProduccionRacimos', 'action' => 'saveImg' , 'path' => 'marun', 'module' => 'banano'],

        'marun/racimos/resumen' => ['controller' => 'ProduccionRacimos', 'action' => 'resumen' , 'path' => 'marun', 'module' => 'banano'],
        'marun/racimos/historico' => ['controller' => 'ProduccionRacimos', 'action' => 'historico' , 'path' => 'marun', 'module' => 'banano'],
        'marun/racimos/last' => ['controller' => 'ProduccionRacimos', 'action' => 'lastDay' , 'path' => 'marun', 'module' => 'banano'],
        'marun/racimos/tags' => ['controller' => 'ProduccionRacimos', 'action' => 'tags' , 'path' => 'marun', 'module' => 'banano'],
        'marun/racimos/eliminar' => ['controller' => 'ProduccionRacimos', 'action' => 'eliminar' , 'path' => 'marun', 'module' => 'banano'],
        'marun/racimos/editar' => ['controller' => 'ProduccionRacimos', 'action' => 'editar' , 'path' => 'marun', 'module' => 'banano'],
        'marun/racimos/recusados' => ['controller' => 'ProduccionRacimos', 'action' => 'analizisRecusados', 'path' => 'marun', 'module' => 'banano'],
		'marun/racimos/diarecusados' => ['controller' => 'ProduccionRacimos', 'action' => 'defectos', 'path' => 'marun', 'module' => 'banano'],
		'marun/racimos/muestreo' => ['controller' => 'ProduccionRacimos', 'action' => 'muestreo', 'path' => 'marun', 'module' => 'banano'],
		'marun/racimos/viajes' => ['controller' => 'ProduccionRacimos', 'action' => 'racimosPorViaje', 'path' => 'marun', 'module' => 'banano'],
		'marun/racimos/pesoCalibre' => ['controller' => 'ProduccionRacimos', 'action' => 'pesoCalibre', 'path' => 'marun', 'module' => 'banano'],
		'marun/racimos/pesoMano' => ['controller' => 'ProduccionRacimos', 'action' => 'pesoMano', 'path' => 'marun', 'module' => 'banano'],
		'marun/racimos/pesoRacimo' => ['controller' => 'ProduccionRacimos', 'action' => 'pesoRacimo', 'path' => 'marun', 'module' => 'banano'],
		'marun/racimos/cantidadRacimoCalibreManos' => ['controller' => 'ProduccionRacimos', 'action' => 'cantidadRacimoCalibreManos', 'path' => 'marun', 'module' => 'banano'],

		'marun/racimos/borrarViaje' => ['controller' => 'ProduccionRacimos', 'action' => 'borrarViaje', 'path' => 'marun', 'module' => 'banano'],
		'marun/racimos/moverViajes' => ['controller' => 'ProduccionRacimos', 'action' => 'moverViajes', 'path' => 'marun', 'module' => 'banano'],
		'marun/racimos/moverViajesOtraFinca' => ['controller' => 'ProduccionRacimos', 'action' => 'moverViajesOtraFinca', 'path' => 'marun', 'module' => 'banano'],
		'marun/racimos/moverCrearViajes' => ['controller' => 'ProduccionRacimos', 'action' => 'moverCrearViajes', 'path' => 'marun', 'module' => 'banano'],
		'marun/racimos/borrarRacimosViaje' => ['controller' => 'ProduccionRacimos', 'action' => 'borrarRacimosViaje', 'path' => 'marun', 'module' => 'banano'],
		'marun/racimos/crearRacimosViaje' => ['controller' => 'ProduccionRacimos', 'action' => 'crearRacimosViaje', 'path' => 'marun', 'module' => 'banano'],
		'marun/racimos/cambiarPalanca' => ['controller' => 'ProduccionRacimos', 'action' => 'cambiarPalanca', 'path' => 'marun', 'module' => 'banano'],
		'marun/racimos/cambiarLote' => ['controller' => 'ProduccionRacimos', 'action' => 'cambiarLote', 'path' => 'marun', 'module' => 'banano'],
		'marun/racimos/saveEditRacimo' => ['controller' => 'ProduccionRacimos', 'action' => 'saveEditRacimo', 'path' => 'marun', 'module' => 'banano'],
		'marun/racimos/getViajesFinca' => ['controller' => 'ProduccionRacimos', 'action' => 'getViajesFinca', 'path' => 'marun', 'module' => 'banano'],
		'marun/racimos/desbloquear' => ['controller' => 'ProduccionRacimos', 'action' => 'desbloquearViajes', 'path' => 'marun', 'module' => 'banano'],
		'marun/racimos/changeUnidad' => ['controller' => 'ProduccionRacimos', 'action' => 'changeUnidad', 'path' => 'marun', 'module' => 'banano'],
		'marun/racimos/getEdadCinta' => ['controller' => 'ProduccionRacimos', 'action' => 'getEdadCinta', 'path' => 'marun', 'module' => 'banano'],
		'marun/racimos/getPesoPromLote' => ['controller' => 'ProduccionRacimos', 'action' => 'getPesoPromLote', 'path' => 'marun', 'module' => 'banano'],
		'marun/racimos/crearFincaProceso' => ['controller' => 'ProduccionRacimos', 'action' => 'crearFincaProceso', 'path' => 'marun', 'module' => 'banano'],
		
        'reiset/racimos/resumen' => ['controller' => 'ProduccionRacimos', 'action' => 'resumen' , 'path' => 'reiset', 'module' => 'banano'],
        'reiset/racimos/edades' => ['controller' => 'ProduccionRacimos', 'action' => 'racimosEdad' , 'path' => 'reiset', 'module' => 'banano'],
        'reiset/racimos/historico' => ['controller' => 'ProduccionRacimos', 'action' => 'historico' , 'path' => 'reiset', 'module' => 'banano'],
        'reiset/racimos/last' => ['controller' => 'ProduccionRacimos', 'action' => 'lastDay' , 'path' => 'reiset', 'module' => 'banano'],
        'reiset/racimos/tags' => ['controller' => 'ProduccionRacimos', 'action' => 'tags' , 'path' => 'reiset', 'module' => 'banano'],
        'reiset/racimos/eliminar' => ['controller' => 'ProduccionRacimos', 'action' => 'eliminar' , 'path' => 'reiset', 'module' => 'banano'],
        'reiset/racimos/editar' => ['controller' => 'ProduccionRacimos', 'action' => 'editar' , 'path' => 'reiset', 'module' => 'banano'],
        'reiset/racimos/recusados' => ['controller' => 'ProduccionRacimos', 'action' => 'analizisRecusados', 'path' => 'reiset', 'module' => 'banano'],
		'reiset/racimos/diarecusados' => ['controller' => 'ProduccionRacimos', 'action' => 'defectos', 'path' => 'reiset', 'module' => 'banano'],
		'reiset/racimos/muestreo' => ['controller' => 'ProduccionRacimos', 'action' => 'muestreo', 'path' => 'reiset', 'module' => 'banano'],
		'reiset/formulariosracimos/resumen' => ['controller' => 'ProduccionRacimosFormularios', 'action' => 'resumen' , 'path' => 'reiset', 'module' => 'banano'],
        'reiset/formulariosracimos/edades' => ['controller' => 'ProduccionRacimosFormularios', 'action' => 'racimosEdad' , 'path' => 'reiset', 'module' => 'banano'],
        'reiset/formulariosracimos/historico' => ['controller' => 'ProduccionRacimosFormularios', 'action' => 'historico' , 'path' => 'reiset', 'module' => 'banano'],
        'reiset/formulariosracimos/last' => ['controller' => 'ProduccionRacimosFormularios', 'action' => 'lastDay' , 'path' => 'reiset', 'module' => 'banano'],
        'reiset/formulariosracimos/tags' => ['controller' => 'ProduccionRacimosFormularios', 'action' => 'tags' , 'path' => 'reiset', 'module' => 'banano'],
        'reiset/formulariosracimos/eliminar' => ['controller' => 'ProduccionRacimosFormularios', 'action' => 'eliminar' , 'path' => 'reiset', 'module' => 'banano'],
        'reiset/formulariosracimos/editar' => ['controller' => 'ProduccionRacimosFormularios', 'action' => 'editar' , 'path' => 'reiset', 'module' => 'banano'],
        'reiset/formulariosracimos/recusados' => ['controller' => 'ProduccionRacimosFormularios', 'action' => 'analizisRecusados', 'path' => 'reiset', 'module' => 'banano'],
		'reiset/formulariosracimos/diarecusados' => ['controller' => 'ProduccionRacimosFormularios', 'action' => 'defectos', 'path' => 'reiset', 'module' => 'banano'],
		'reiset/formulariosracimos/muestreo' => ['controller' => 'ProduccionRacimosFormularios', 'action' => 'muestreo', 'path' => 'reiset', 'module' => 'banano'],
		'reiset/formulariosracimos/variableViaje' => ['controller' => 'ProduccionRacimosFormularios', 'action' => 'editarVariableViaje', 'path' => 'reiset', 'module' => 'banano'],
		'reiset/formulariosracimos/borrarViaje' => ['controller' => 'ProduccionRacimosFormularios', 'action' => 'borrarViaje', 'path' => 'reiset', 'module' => 'banano'],
		'reiset/racimos/borrarViaje' => ['controller' => 'ProduccionRacimosFormularios', 'action' => 'borrarViajeBalanza', 'path' => 'reiset', 'module' => 'banano'],
		'reiset/formulariosracimos/agregarRacimo' => ['controller' => 'ProduccionRacimosFormularios', 'action' => 'agregarRacimo', 'path' => 'reiset', 'module' => 'banano'],
		'reiset/formulariosracimos/procesar' => ['controller' => 'ProduccionRacimosFormularios', 'action' => 'procesarRacimos', 'path' => 'reiset', 'module' => 'banano'],

        'mario/racimos/resumen' => ['controller' => 'ProduccionRacimos', 'action' => 'resumen' , 'path' => 'mario', 'module' => 'banano'],
        'mario/racimos/edades' => ['controller' => 'ProduccionRacimos', 'action' => 'racimosEdad' , 'path' => 'mario', 'module' => 'banano'],
        'mario/racimos/historico' => ['controller' => 'ProduccionRacimos', 'action' => 'historico' , 'path' => 'mario', 'module' => 'banano'],
        'mario/racimos/last' => ['controller' => 'ProduccionRacimos', 'action' => 'lastDay' , 'path' => 'mario', 'module' => 'banano'],
        'mario/racimos/tags' => ['controller' => 'ProduccionRacimos', 'action' => 'tags' , 'path' => 'mario', 'module' => 'banano'],
        'mario/racimos/eliminar' => ['controller' => 'ProduccionRacimos', 'action' => 'eliminar' , 'path' => 'mario', 'module' => 'banano'],
        'mario/racimos/editar' => ['controller' => 'ProduccionRacimos', 'action' => 'editar' , 'path' => 'mario', 'module' => 'banano'],
        'mario/racimos/recusados' => ['controller' => 'ProduccionRacimos', 'action' => 'analizisRecusados', 'path' => 'mario', 'module' => 'banano'],
		'mario/racimos/diarecusados' => ['controller' => 'ProduccionRacimos', 'action' => 'defectos', 'path' => 'mario', 'module' => 'banano'],
		'mario/racimos/pesoCalibre' => ['controller' => 'ProduccionRacimos', 'action' => 'pesoCalibre', 'path' => 'mario', 'module' => 'banano'],
		'mario/racimos/pesoMano' => ['controller' => 'ProduccionRacimos', 'action' => 'pesoMano', 'path' => 'mario', 'module' => 'banano'],
		'mario/racimos/pesoRacimo' => ['controller' => 'ProduccionRacimos', 'action' => 'pesoRacimo', 'path' => 'mario', 'module' => 'banano'],
		'mario/racimos/cantidadRacimoCalibreManos' => ['controller' => 'ProduccionRacimos', 'action' => 'cantidadRacimoCalibreManos', 'path' => 'mario', 'module' => 'banano'],

        'marun/produccionsemana/index' => ['controller' => 'ProduccionWeek', 'action' => 'index', 'path' => 'marun', 'module' => 'banano'],
        'marun/produccionsemana/tags' => ['controller' => 'ProduccionWeek', 'action' => 'tags', 'path' => 'marun', 'module' => 'banano'],
        'marun/produccionsemana/graficaEdadPromedio' => ['controller' => 'ProduccionWeek', 'action' => 'graficaEdadPromedio', 'path' => 'marun', 'module' => 'banano'],
        'marun/produccionsemana/resumenAcumulados' => ['controller' => 'ProduccionWeek', 'action' => 'resumenAcumulados', 'path' => 'marun', 'module' => 'banano'],
        'marun/produccionsemana/reporteProduccion' => ['controller' => 'ProduccionWeek', 'action' => 'reporteProduccion', 'path' => 'marun', 'module' => 'banano'],
        'marun/produccionsemana/graficaVariables' => ['controller' => 'ProduccionWeek', 'action' => 'graficaVariables', 'path' => 'marun', 'module' => 'banano'],
        'marun/produccionsemana/lastWeek' => ['controller' => 'ProduccionWeek', 'action' => 'getLastWeek', 'path' => 'marun', 'module' => 'banano'],

        'marcel/produccionsemana/index' => ['controller' => 'ProduccionWeek', 'action' => 'index', 'path' => 'marcel', 'module' => 'banano'],
        'marcel/produccionsemana/tags' => ['controller' => 'ProduccionWeek', 'action' => 'tags', 'path' => 'marcel', 'module' => 'banano'],
        'marcel/produccionsemana/graficaEdadPromedio' => ['controller' => 'ProduccionWeek', 'action' => 'graficaEdadPromedio', 'path' => 'marcel', 'module' => 'banano'],
        'marcel/produccionsemana/resumenAcumulados' => ['controller' => 'ProduccionWeek', 'action' => 'resumenAcumulados', 'path' => 'marcel', 'module' => 'banano'],
        'marcel/produccionsemana/reporteProduccion' => ['controller' => 'ProduccionWeek', 'action' => 'reporteProduccion', 'path' => 'marcel', 'module' => 'banano'],
        'marcel/produccionsemana/graficaVariables' => ['controller' => 'ProduccionWeek', 'action' => 'graficaVariables', 'path' => 'marcel', 'module' => 'banano'],
		'marcel/produccionsemana/lastWeek' => ['controller' => 'ProduccionWeek', 'action' => 'getLastWeek', 'path' => 'marcel', 'module' => 'banano'],
		'comparacion/graficaVariables' => ['controller' => 'ProduccionComparacionClientes', 'action' => 'graficaVariables', 'module' => 'banano'],
		'comparacion/hectareas' => ['controller' => 'ProduccionComparacionClientes', 'action' => 'porHectareas', 'module' => 'banano'],
		'comparacion/semanal' => ['controller' => 'ProduccionComparacionClientes', 'action' => 'semanal', 'module' => 'banano'],

        'reiset/produccionsemana/index' => ['controller' => 'ProduccionWeek', 'action' => 'index', 'path' => 'reiset', 'module' => 'banano'],
        'reiset/produccionsemana/tags' => ['controller' => 'ProduccionWeek', 'action' => 'tags', 'path' => 'reiset', 'module' => 'banano'],
        'reiset/produccionsemana/graficaEdadPromedio' => ['controller' => 'ProduccionWeek', 'action' => 'graficaEdadPromedio', 'path' => 'reiset', 'module' => 'banano'],
        'reiset/produccionsemana/resumenAcumulados' => ['controller' => 'ProduccionWeek', 'action' => 'resumenAcumulados', 'path' => 'reiset', 'module' => 'banano'],
        'reiset/produccionsemana/reporteProduccion' => ['controller' => 'ProduccionWeek', 'action' => 'reporteProduccion', 'path' => 'reiset', 'module' => 'banano'],
        'reiset/produccionsemana/graficaVariables' => ['controller' => 'ProduccionWeek', 'action' => 'graficaVariables', 'path' => 'reiset', 'module' => 'banano'],
        'reiset/produccionsemana/lastWeek' => ['controller' => 'ProduccionWeek', 'action' => 'getLastWeek', 'path' => 'reiset', 'module' => 'banano'],
		'reiset/produccionsemana/changeHectareas' => ['controller' => 'ProduccionWeek', 'action' => 'changeHectareas', 'path' => 'reiset', 'module' => 'banano'],
		
		'sumifru/produccionsemana/index' => ['controller' => 'ProduccionWeek', 'action' => 'index', 'path' => 'sumifru', 'module' => 'banano'],
        'sumifru/produccionsemana/tags' => ['controller' => 'ProduccionWeek', 'action' => 'tags', 'path' => 'sumifru', 'module' => 'banano'],
        'sumifru/produccionsemana/graficaEdadPromedio' => ['controller' => 'ProduccionWeek', 'action' => 'graficaEdadPromedio', 'path' => 'sumifru', 'module' => 'banano'],
        'sumifru/produccionsemana/reporteProduccion' => ['controller' => 'ProduccionWeek', 'action' => 'reporteProduccion', 'path' => 'sumifru', 'module' => 'banano'],
        'sumifru/produccionsemana/graficaVariables' => ['controller' => 'ProduccionWeek', 'action' => 'graficaVariables', 'path' => 'sumifru', 'module' => 'banano'],
        'sumifru/produccionsemana/last' => ['controller' => 'ProduccionWeek', 'action' => 'last', 'path' => 'sumifru', 'module' => 'banano'],
		'sumifru/produccionsemana/changeHectareas' => ['controller' => 'ProduccionWeek', 'action' => 'changeHectareas', 'path' => 'sumifru', 'module' => 'banano'],
		'sumifru/comparacion/historico' => ['controller' => 'ProduccionComparacion', 'action' => 'index', 'module' => 'banano', 'path' => 'sumifru'],
		'sumifru/comparacion/last' => ['controller' => 'ProduccionComparacion', 'action' => 'last', 'module' => 'banano', 'path' => 'sumifru'],
		'sumifru/comparacion/grafica' => ['controller' => 'ProduccionComparacion', 'action' => 'grafica', 'module' => 'banano', 'path' => 'sumifru'],
		
		'orodelti/produccionsemana/index' => ['controller' => 'ProduccionWeek', 'action' => 'index', 'path' => 'orodelti', 'module' => 'banano'],
        'orodelti/produccionsemana/tags' => ['controller' => 'ProduccionWeek', 'action' => 'tags', 'path' => 'orodelti', 'module' => 'banano'],
        'orodelti/produccionsemana/graficaEdadPromedio' => ['controller' => 'ProduccionWeek', 'action' => 'graficaEdadPromedio', 'path' => 'orodelti', 'module' => 'banano'],
        'orodelti/produccionsemana/reporteProduccion' => ['controller' => 'ProduccionWeek', 'action' => 'reporteProduccion', 'path' => 'orodelti', 'module' => 'banano'],
        'orodelti/produccionsemana/graficaVariables' => ['controller' => 'ProduccionWeek', 'action' => 'graficaVariables', 'path' => 'orodelti', 'module' => 'banano'],
        'orodelti/produccionsemana/last' => ['controller' => 'ProduccionWeek', 'action' => 'last', 'path' => 'orodelti', 'module' => 'banano'],
		'orodelti/produccionsemana/changeHectareas' => ['controller' => 'ProduccionWeek', 'action' => 'changeHectareas', 'path' => 'orodelti', 'module' => 'banano'],

		'clementina/produccionsemana/index' => ['controller' => 'ProduccionWeek', 'action' => 'index', 'path' => 'clementina', 'module' => 'banano'],
        'clementina/produccionsemana/tags' => ['controller' => 'ProduccionWeek', 'action' => 'tags', 'path' => 'clementina', 'module' => 'banano'],
        'clementina/produccionsemana/graficaEdadPromedio' => ['controller' => 'ProduccionWeek', 'action' => 'graficaEdadPromedio', 'path' => 'clementina', 'module' => 'banano'],
        'clementina/produccionsemana/reporteProduccion' => ['controller' => 'ProduccionWeek', 'action' => 'reporteProduccion', 'path' => 'clementina', 'module' => 'banano'],
        'clementina/produccionsemana/graficaVariables' => ['controller' => 'ProduccionWeek', 'action' => 'graficaVariables', 'path' => 'clementina', 'module' => 'banano'],
        'clementina/produccionsemana/last' => ['controller' => 'ProduccionWeek', 'action' => 'last', 'path' => 'clementina', 'module' => 'banano'],
		'clementina/produccionsemana/changeHectareas' => ['controller' => 'ProduccionWeek', 'action' => 'changeHectareas', 'path' => 'clementina', 'module' => 'banano'],
		
		'palmar/produccionsemana/index' => ['controller' => 'ProduccionWeek', 'action' => 'index', 'path' => 'palmar', 'module' => 'banano'],
        'palmar/produccionsemana/tags' => ['controller' => 'ProduccionWeek', 'action' => 'tags', 'path' => 'palmar', 'module' => 'banano'],
        'palmar/produccionsemana/graficaEdadPromedio' => ['controller' => 'ProduccionWeek', 'action' => 'graficaEdadPromedio', 'path' => 'palmar', 'module' => 'banano'],
        'palmar/produccionsemana/reporteProduccion' => ['controller' => 'ProduccionWeek', 'action' => 'reporteProduccion', 'path' => 'palmar', 'module' => 'banano'],
        'palmar/produccionsemana/graficaVariables' => ['controller' => 'ProduccionWeek', 'action' => 'graficaVariables', 'path' => 'palmar', 'module' => 'banano'],
        'palmar/produccionsemana/last' => ['controller' => 'ProduccionWeek', 'action' => 'last', 'path' => 'palmar', 'module' => 'banano'],
        'palmar/produccionsemana/changeHectareas' => ['controller' => 'ProduccionWeek', 'action' => 'changeHectareas', 'path' => 'palmar', 'module' => 'banano'],

        'quintana/produccionsemana/index' => ['controller' => 'ProduccionWeek', 'action' => 'index', 'path' => 'quintana', 'module' => 'banano'],
        'quintana/produccionsemana/last' => ['controller' => 'ProduccionWeek', 'action' => 'last', 'path' => 'quintana', 'module' => 'banano'],
        'quintana/produccionsemana/tags' => ['controller' => 'ProduccionWeek', 'action' => 'tags', 'path' => 'quintana', 'module' => 'banano'],
        'quintana/produccionsemana/graficaEdadPromedio' => ['controller' => 'ProduccionWeek', 'action' => 'graficaEdadPromedio', 'path' => 'quintana', 'module' => 'banano'],
        'quintana/produccionsemana/resumenAcumulados' => ['controller' => 'ProduccionWeek', 'action' => 'resumenAcumulados', 'path' => 'quintana', 'module' => 'banano'],
        'quintana/produccionsemana/reporteProduccion' => ['controller' => 'ProduccionWeek', 'action' => 'reporteProduccion', 'path' => 'quintana', 'module' => 'banano'],
        'quintana/produccionsemana/graficaVariables' => ['controller' => 'ProduccionWeek', 'action' => 'graficaVariables', 'path' => 'quintana', 'module' => 'banano'],
        'quintana/produccionsemana/lastWeek' => ['controller' => 'ProduccionWeek', 'action' => 'getLastWeek', 'path' => 'quintana', 'module' => 'banano'],
		/*----------  PRODUCCION  ----------*/
        /* CALIDAD QUINTANA */

        /* END MIGRAR */

		/* PRODUCTOS - SIGAT */
        'productos/index' => ['controller' => 'Productos', 'action' => 'index' , 'path' => 'sigat', 'module' => 'banano'],
		'productos/create' => ['controller' => 'Productos', 'action' => 'create', 'path' => 'sigat', 'module' => 'banano'],
		'productos/formuladoras/get' => ['controller' => 'Productos', 'action' => 'getFormuladoras', 'path' => 'sigat', 'module' => 'banano'],
		'productos/proveedores/get' => ['controller' => 'Productos', 'action' => 'getProveedores', 'path' => 'sigat', 'module' => 'banano'],
		'productos/tipoProductos/get' => ['controller' => 'Productos', 'action' => 'getTipoProductos', 'path' => 'sigat', 'module' => 'banano'],
		'productos/frac/get' => ['controller' => 'Productos', 'action' => 'getFrac', 'path' => 'sigat', 'module' => 'banano'],
		'productos/getPrecios' => ['controller' => 'Productos', 'action' => 'getPrecios', 'path' => 'sigat', 'module' => 'banano'],
		'productos/savePrecios' => ['controller' => 'Productos', 'action' => 'savePrecios', 'path' => 'sigat', 'module' => 'banano'],
		'productos/getAccion' => ['controller' => 'Productos', 'action' => 'getAccion', 'path' => 'sigat', 'module' => 'banano'],
		'productos/show' => ['controller' => 'Productos', 'action' => 'show', 'path' => 'sigat', 'module' => 'banano'],
		'productos/update' => ['controller' => 'Productos', 'action' => 'update', 'path' => 'sigat', 'module' => 'banano'],
		'productos/saveConfiguracion' => ['controller' => 'Productos', 'action' => 'saveConfiguracion', 'path' => 'sigat', 'module' => 'banano'],
		/* PRODUCTOS - SIGAT */
		/* FINCAS - SIGAT */
		'sigat/fincas/index' => ['controller' => 'Fincas', 'action' => 'index', 'path' => 'sigat', 'module' => 'banano'],
		'sigat/fincas/changeStatus' => ['controller' => 'Fincas', 'action' => 'changeStatus', 'path' => 'sigat', 'module' => 'banano'],
		/* FINCAS - SIGAT */
		/* API PRONTOFORMS */
		'api/prontoforms/all' => ['controller' => 'ApiProntoforms', 'action' => 'refreshAll', 'module' => 'banano'],
		'api/prontoforms/personal' => ['controller' => 'ApiProntoforms', 'action' => 'actualizarPersonal', 'module' => 'banano'],
		/* API PRONTOFORMS */
		'data/produccion/actual' => ['controller' => 'ProduccionWeek', 'action' => 'produccionActual', 'module' => 'banano'],
		/* LANCOFRUIT */
		'lacofruit/index' => ['controller' => 'Lancofruit', 'action' => 'index', 'module' => 'banano'],
		'lacofruit/markers' => ['controller' => 'Lancofruit', 'action' => 'markers', 'module' => 'banano'],
		'lacofruit/filters' => ['controller' => 'Lancofruit', 'action' => 'filters', 'module' => 'banano'],
		'lancofruit/edit' => ['controller' => 'Lancofruit', 'action' => 'edit', 'module' => 'banano'],
		'lancofruit/pasteles' => ['controller' => 'Lancofruit', 'action' => 'pasteles', 'module' => 'banano'],
		'lancofruit/save' => ['controller' => 'Lancofruit', 'action' => 'save', 'module' => 'banano'],
		'lancofruit/vendedor' => ['controller' => 'Lancofruit', 'action' => 'asignarVendedor', 'module' => 'banano'],
		'lancofruit/config' => ['controller' => 'Lancofruit', 'action' => 'getConfiguracion', 'module' => 'banano'],
		'lancofruit/addRuta' => ['controller' => 'Lancofruit', 'action' => 'addRuta', 'module' => 'banano'],
		'lancofruit/addVendedor' => ['controller' => 'Lancofruit', 'action' => 'addVendedor', 'module' => 'banano'],
		'lancofruit/rutas/save' => ['controller' => 'Lancofruit', 'action' => 'saveConfig', 'module' => 'banano'],

        'lancofruit/ventas/last' => ['controller' => 'VentasLancofruit', 'action' => 'last', 'module' => 'banano'],
        'lancofruit/ventas/index' => ['controller' => 'VentasLancofruit', 'action' => 'index', 'module' => 'banano'],
        'lancofruit/ventas/dia' => ['controller' => 'VentasLancofruit', 'action' => 'ventasDia', 'module' => 'banano'],
        'lancofruit/ventas/semana' => ['controller' => 'VentasLancofruit', 'action' => 'ventasSemana', 'module' => 'banano'],
        'lancofruit/ventas/mes' => ['controller' => 'VentasLancofruit', 'action' => 'ventasMes', 'module' => 'banano'],
        'lancofruit/ventas/anio' => ['controller' => 'VentasLancofruit', 'action' => 'ventasAnio', 'module' => 'banano'],
        'lancofruit/rastreo/last' => ['controller' => 'RastreoLancofruit', 'action' => 'last', 'module' => 'banano'],
        'lancofruit/rastreo/points' => ['controller' => 'RastreoLancofruit', 'action' => 'posiciones', 'module' => 'banano'],
		'lancofruit/rastreo/ventadia' => ['controller' => 'RastreoLancofruit', 'action' => 'ventaDia', 'module' => 'banano'],
		'lancofruit/rastreo/ventasemana' => ['controller' => 'RastreoLancofruit', 'action' => 'ventaSemana', 'module' => 'banano'],
		'lancofruit/rastreo/ventames' => ['controller' => 'RastreoLancofruit', 'action' => 'ventaMes', 'module' => 'banano'],
		'lancofruit/rastreo/ventaanio' => ['controller' => 'RastreoLancofruit', 'action' => 'ventaAnio', 'module' => 'banano'],
		'lancofruit/rastreo/dia' => ['controller' => 'RastreoLancofruit', 'action' => 'dia', 'module' => 'banano'],
		'lancofruit/rastreo/semana' => ['controller' => 'RastreoLancofruit', 'action' => 'semana', 'module' => 'banano'],
		'lancofruit/rastreo/mes' => ['controller' => 'RastreoLancofruit', 'action' => 'mes', 'module' => 'banano'],
		'lancofruit/rastreo/anio' => ['controller' => 'RastreoLancofruit', 'action' => 'anio', 'module' => 'banano'],
		'lancofruit/rastreo/indicadores' => ['controller' => 'RastreoLancofruit', 'action' => 'indicadores', 'module' => 'banano'],
		'lancofruit/rastreo/eliminar' => ['controller' => 'RastreoLancofruit', 'action' => 'eliminar', 'module' => 'banano'],
		'lancofruit/rastreo/editar' => ['controller' => 'RastreoLancofruit', 'action' => 'editar', 'module' => 'banano'],
		'lancofruit/upXmls' => ['controller' => 'ImportarInformacion', 'action' => 'upXmls'],
		
		'lancofruit/reportes/indicadores' => ['controller' => 'VentasLancofruitReportes', 'action' => 'indicadores', 'module' => 'banano'],
		'lancofruit/reportes/last' => ['controller' => 'VentasLancofruitReportes', 'action' => 'last', 'module' => 'banano'],
		'lancofruit/reportes/ventaDia' => ['controller' => 'VentasLancofruitReportes', 'action' => 'ventaDia', 'module' => 'banano'],
		'lancofruit/reportes/ventaComparativo' => ['controller' => 'VentasLancofruitReportes', 'action' => 'ventaComparativo', 'module' => 'banano'],
		'lancofruit/reportes/eliminar' => ['controller' => 'VentasLancofruitReportes', 'action' => 'eliminar', 'module' => 'banano'],
		'lancofruit/reportes/editar' => ['controller' => 'VentasLancofruitReportes', 'action' => 'editar', 'module' => 'banano'],
		'lancofruit/reportes/ventaTendencia' => ['controller' => 'VentasLancofruitReportes', 'action' => 'ventaTendencia', 'module' => 'banano'],
		'lancofruit/reportes/tendenciaGrafica' => ['controller' => 'VentasLancofruitReportes', 'action' => 'ventaTendenciaGrafica', 'module' => 'banano'],
		
		
		/** CERCAS */
		'lancofruit/polygon/index' => ['controller' => 'Cercas', 'action' => 'index', 'module' => 'banano'],
		'lancofruit/polygon/save' => ['controller' => 'Cercas', 'action' => 'save', 'module' => 'banano'],
		/** CERCAS */

		'perchas/tags' => ['controller' => 'Perchas', 'action' => 'index', 'module' => 'banano'],
		'marcel/perchas/data' => ['controller' => 'Perchas', 'action' => 'data', 'module' => 'banano', 'path' => 'marcel'],
		'marcel/perchas/last' => ['controller' => 'Perchas', 'action' => 'last', 'module' => 'banano', 'path' => 'marcel'],
		'temperatura/index' => ['controller' => 'Temperatura', 'action' => 'index', 'module' => 'banano'],
		/* LANCOFRUIT */
		/* ADD DATA MARCEL LANIADO */
		'marcel/data/index' => ['controller' => 'AddData', 'action' => 'index' , 'path' => 'marcel', 'module' => 'banano'],
		/* ADD DATA MARCEL LANIADO */

		'marcel/cajas/registros' => ['controller' => 'ProduccionCajas', 'action' => 'registros', 'path' => 'marcel', 'module' => 'banano'],
		'marcel/cajas/last' => ['controller' => 'ProduccionCajas', 'action' => 'last', 'path' => 'marcel', 'module' => 'banano'],
		'marcel/cajas/resumen' => ['controller' => 'ProduccionCajas', 'action' => 'resumenMarca', 'path' => 'marcel', 'module' => 'banano'],
		'marcel/cajas/filters' => ['controller' => 'ProduccionCajas', 'action' => 'filters', 'path' => 'marcel', 'module' => 'banano'],
		'marcel/cajas/cajasSemanal' => ['controller' => 'ProduccionCajas', 'action' => 'historicoCajasSemanal', 'path' => 'marcel', 'module' => 'banano'],
		'marcel/cajas/marcas' => ['controller' => 'ProduccionCajas', 'action' => 'getMarcas', 'path' => 'marcel', 'module' => 'banano'],
        'marcel/cajas/cuadrar' => ['controller' => 'ProduccionCajas', 'action' => 'cuadreCajas', 'path' => 'marcel', 'module' => 'banano'],
        'marcel/cajas/guardarCuadrar' => ['controller' => 'ProduccionCajas', 'action' => 'guardarCuadrar', 'path' => 'marcel', 'module' => 'banano'],
        'marcel/cajas/guias' => ['controller' => 'ProduccionCajas', 'action' => 'getGuias', 'path' => 'marcel', 'module' => 'banano'],
        'marcel/cajas/procesar' => ['controller' => 'ProduccionCajas', 'action' => 'procesar', 'path' => 'marcel', 'module' => 'banano'],
        'marcel/cajas/eliminar' => ['controller' => 'ProduccionCajas', 'action' => 'eliminar', 'path' => 'marcel', 'module' => 'banano'],
        'marcel/cajas/graficasBarras' => ['controller' => 'ProduccionCajas', 'action' => 'graficasBarras', 'path' => 'marcel', 'module' => 'banano'],
        'marcel/cajas/diferencias' => ['controller' => 'ProduccionCajas', 'action' => 'tablasDiferecias', 'path' => 'marcel', 'module' => 'banano'],
        'marcel/cajas/excedente' => ['controller' => 'ProduccionCajas', 'action' => 'historicoExcedente', 'path' => 'marcel', 'module' => 'banano'],
        'marcel/cajas/guardarCaja' => ['controller' => 'ProduccionCajas', 'action' => 'guardarCaja', 'path' => 'marcel', 'module' => 'banano'],
        'marcel/cajas/guardarUmbral' => ['controller' => 'ProduccionCajas', 'action' => 'guardarUmbral', 'path' => 'marcel', 'module' => 'banano'],

		'marun/cajas/registros' => ['controller' => 'ProduccionCajas', 'action' => 'registros', 'module' => 'banano'],
		'marun/cajas/regMarcas' => ['controller' => 'ProduccionCajas', 'action' => 'marcas', 'module' => 'banano'],
		'marun/cajas/last' => ['controller' => 'ProduccionCajas', 'action' => 'last', 'module' => 'banano'],
		'marun/cajas/resumen' => ['controller' => 'ProduccionCajas', 'action' => 'resumenMarca', 'module' => 'banano'],
		'marun/cajas/filters' => ['controller' => 'ProduccionCajas', 'action' => 'filters', 'module' => 'banano'],
		'marun/cajas/cajasSemanal' => ['controller' => 'ProduccionCajas', 'action' => 'historicoCajasSemanal', 'module' => 'banano'],
		'marun/cajas/marcas' => ['controller' => 'ProduccionCajas', 'action' => 'getMarcas', 'module' => 'banano'],
        'marun/cajas/cuadrar' => ['controller' => 'ProduccionCajas', 'action' => 'cuadreCajas', 'module' => 'banano'],
        'marun/cajas/guardarCuadrar' => ['controller' => 'ProduccionCajas', 'action' => 'guardarCuadrar', 'module' => 'banano'],
        'marun/cajas/guias' => ['controller' => 'ProduccionCajas', 'action' => 'getGuias', 'module' => 'banano'],
        'marun/cajas/procesar' => ['controller' => 'ProduccionCajas', 'action' => 'procesar', 'module' => 'banano'],
        'marun/cajas/eliminar' => ['controller' => 'ProduccionCajas', 'action' => 'eliminar', 'module' => 'banano'],
        'marun/cajas/guardarRegistro' => ['controller' => 'ProduccionCajas', 'action' => 'guardarRegistro', 'module' => 'banano'],
        'marun/cajas/graficasBarras' => ['controller' => 'ProduccionCajas', 'action' => 'graficasBarras', 'module' => 'banano'],
        'marun/cajas/diferencias' => ['controller' => 'ProduccionCajas', 'action' => 'tablasDiferecias', 'module' => 'banano'],
        'marun/cajas/excedente' => ['controller' => 'ProduccionCajas', 'action' => 'historicoExcedente', 'module' => 'banano'],
		'marun/cajas/guardarCaja' => ['controller' => 'ProduccionCajas', 'action' => 'guardarCaja', 'module' => 'banano'],
		'marun/cajas/marcaProcesarPeso' => ['controller' => 'ProduccionCajas', 'action' => 'procesarMarca', 'module' => 'banano'],
		'marun/cajas/eficienciaBalanza' => ['controller' => 'ProduccionCajas', 'action' => 'graficaEficiencia', 'module' => 'banano'],
		'marun/cajas/changeUnidad' => ['controller' => 'ProduccionCajas', 'action' => 'changeUnidad', 'module' => 'banano'],
		'marun/cajas/editGuia' => ['controller' => 'ProduccionCajas', 'action' => 'editGuia', 'module' => 'banano'],
		'marun/cajas/asignarGuia' => ['controller' => 'ProduccionCajas', 'action' => 'asignarGuia', 'module' => 'banano'],
		'marun/cajas/borrarGuiaMarca' => ['controller' => 'ProduccionCajas', 'action' => 'borrarGuiaMarca', 'module' => 'banano'],
		'marun/cajas/crearFincaProceso' => ['controller' => 'ProduccionCajas', 'action' => 'crearFincaProceso', 'module' => 'banano'],
		'marun/cajas/crearCajas' => ['controller' => 'ProduccionCajas', 'action' => 'crearCajas', 'module' => 'banano'],
		'marun/cajas/movimientos' => ['controller' => 'ProduccionCajas', 'action' => 'movimientos', 'module' => 'banano'],
		'marun/cajas/comentarios' => ['controller' => 'ProduccionCajas', 'action' => 'guardarComentarios', 'module' => 'banano'],

        'quintana/cajas/registros' => ['controller' => 'ProduccionCajas', 'action' => 'registros', 'path' => 'quintana', 'module' => 'banano'],
		'quintana/cajas/last' => ['controller' => 'ProduccionCajas', 'action' => 'last', 'path' => 'quintana', 'module' => 'banano'],
		'quintana/cajas/resumen' => ['controller' => 'ProduccionCajas', 'action' => 'resumenMarca', 'path' => 'quintana', 'module' => 'banano'],
		'quintana/cajas/filters' => ['controller' => 'ProduccionCajas', 'action' => 'filters', 'path' => 'quintana', 'module' => 'banano'],
        'quintana/cajas/cajasSemanal' => ['controller' => 'ProduccionCajas', 'action' => 'historicoCajasSemanal', 'path' => 'quintana', 'module' => 'banano'],
        'quintana/cajas/cajasHectarea' => ['controller' => 'ProduccionCajas', 'action' => 'historicoCajasSemanalHectarea', 'path' => 'quintana', 'module' => 'banano'],
		'quintana/cajas/marcas' => ['controller' => 'ProduccionCajas', 'action' => 'getMarcas', 'path' => 'quintana', 'module' => 'banano'],
        'quintana/cajas/cuadrar' => ['controller' => 'ProduccionCajas', 'action' => 'cuadreCajas', 'path' => 'quintana', 'module' => 'banano'],
        'quintana/cajas/guardarCuadrar' => ['controller' => 'ProduccionCajas', 'action' => 'guardarCuadrar', 'path' => 'quintana', 'module' => 'banano'],
        'quintana/cajas/guias' => ['controller' => 'ProduccionCajas', 'action' => 'getGuias', 'path' => 'quintana', 'module' => 'banano'],
        'quintana/cajas/procesar' => ['controller' => 'ProduccionCajas', 'action' => 'procesar', 'path' => 'quintana', 'module' => 'banano'],
        'quintana/cajas/eliminar' => ['controller' => 'ProduccionCajas', 'action' => 'eliminar', 'path' => 'quintana', 'module' => 'banano'],
        'quintana/cajas/graficasBarras' => ['controller' => 'ProduccionCajas', 'action' => 'graficasBarras', 'path' => 'quintana', 'module' => 'banano'],
        'quintana/cajas/diferencias' => ['controller' => 'ProduccionCajas', 'action' => 'tablasDiferecias', 'path' => 'quintana', 'module' => 'banano'],
        'quintana/cajas/excedente' => ['controller' => 'ProduccionCajas', 'action' => 'historicoExcedente', 'path' => 'quintana', 'module' => 'banano'],
        'quintana/cajas/guardarCaja' => ['controller' => 'ProduccionCajas', 'action' => 'guardarCaja', 'path' => 'quintana', 'module' => 'banano'],
        'quintana/cajas/guardarUmbral' => ['controller' => 'ProduccionCajas', 'action' => 'guardarUmbral', 'path' => 'quintana', 'module' => 'banano'],

        'reiset/cajas/registros' => ['controller' => 'ProduccionCajas', 'action' => 'registros', 'path' => 'reiset', 'module' => 'banano'],
		'reiset/cajas/last' => ['controller' => 'ProduccionCajas', 'action' => 'last', 'path' => 'reiset', 'module' => 'banano'],
		'reiset/cajas/resumen' => ['controller' => 'ProduccionCajas', 'action' => 'resumenMarca', 'path' => 'reiset', 'module' => 'banano'],
		'reiset/cajas/filters' => ['controller' => 'ProduccionCajas', 'action' => 'filters', 'path' => 'reiset', 'module' => 'banano'],
        'reiset/cajas/cajasSemanal' => ['controller' => 'ProduccionCajas', 'action' => 'historicoCajasSemanal', 'path' => 'reiset', 'module' => 'banano'],
        'reiset/cajas/cajasHectarea' => ['controller' => 'ProduccionCajas', 'action' => 'historicoCajasSemanalHectarea', 'path' => 'reiset', 'module' => 'banano'],
		'reiset/cajas/marcas' => ['controller' => 'ProduccionCajas', 'action' => 'getMarcas', 'path' => 'reiset', 'module' => 'banano'],
        'reiset/cajas/cuadrar' => ['controller' => 'ProduccionCajas', 'action' => 'cuadreCajas', 'path' => 'reiset', 'module' => 'banano'],
        'reiset/cajas/guardarCuadrar' => ['controller' => 'ProduccionCajas', 'action' => 'guardarCuadrar', 'path' => 'reiset', 'module' => 'banano'],
        'reiset/cajas/guias' => ['controller' => 'ProduccionCajas', 'action' => 'getGuias', 'path' => 'reiset', 'module' => 'banano'],
        'reiset/cajas/procesar' => ['controller' => 'ProduccionCajas', 'action' => 'procesar', 'path' => 'reiset', 'module' => 'banano'],
        'reiset/cajas/eliminar' => ['controller' => 'ProduccionCajas', 'action' => 'eliminar', 'path' => 'reiset', 'module' => 'banano'],
        'reiset/cajas/graficasBarras' => ['controller' => 'ProduccionCajas', 'action' => 'graficasBarras', 'path' => 'reiset', 'module' => 'banano'],
        'reiset/cajas/diferencias' => ['controller' => 'ProduccionCajas', 'action' => 'tablasDiferecias', 'path' => 'reiset', 'module' => 'banano'],
		'reiset/cajas/excedente' => ['controller' => 'ProduccionCajas', 'action' => 'historicoExcedente', 'path' => 'reiset', 'module' => 'banano'],
		'reiset/cajas/guardarUmbral' => ['controller' => 'ProduccionCajas', 'action' => 'guardarUmbral', 'path' => 'reiset', 'module' => 'banano'],
		'reiset/cajas/guardarCaja' => ['controller' => 'ProduccionCajas', 'action' => 'guardarCaja', 'path' => 'reiset', 'module' => 'banano'],
		'reiset/cajas/borrarGuiaMarca' => ['controller' => 'ProduccionCajas', 'action' => 'borrarGuiaMarca', 'path' => 'reiset', 'module' => 'banano'],
		
		'sumifru/cajas/registros' => ['controller' => 'ProduccionCajas', 'action' => 'registros', 'path' => 'sumifru', 'module' => 'banano'],
		'sumifru/cajas/last' => ['controller' => 'ProduccionCajas', 'action' => 'last', 'path' => 'sumifru', 'module' => 'banano'],
		'sumifru/cajas/resumen' => ['controller' => 'ProduccionCajas', 'action' => 'resumenMarca', 'path' => 'sumifru', 'module' => 'banano'],
		'sumifru/cajas/filters' => ['controller' => 'ProduccionCajas', 'action' => 'filters', 'path' => 'sumifru', 'module' => 'banano'],
        'sumifru/cajas/cajasSemanal' => ['controller' => 'ProduccionCajas', 'action' => 'historicoCajasSemanal', 'path' => 'sumifru', 'module' => 'banano'],
        'sumifru/cajas/cajasHectarea' => ['controller' => 'ProduccionCajas', 'action' => 'historicoCajasSemanalHectarea', 'path' => 'sumifru', 'module' => 'banano'],
		'sumifru/cajas/marcas' => ['controller' => 'ProduccionCajas', 'action' => 'getMarcas', 'path' => 'sumifru', 'module' => 'banano'],
        'sumifru/cajas/cuadrar' => ['controller' => 'ProduccionCajas', 'action' => 'cuadreCajas', 'path' => 'sumifru', 'module' => 'banano'],
        'sumifru/cajas/guardarCuadrar' => ['controller' => 'ProduccionCajas', 'action' => 'guardarCuadrar', 'path' => 'sumifru', 'module' => 'banano'],
        'sumifru/cajas/guias' => ['controller' => 'ProduccionCajas', 'action' => 'getGuias', 'path' => 'sumifru', 'module' => 'banano'],
        'sumifru/cajas/procesar' => ['controller' => 'ProduccionCajas', 'action' => 'procesar', 'path' => 'sumifru', 'module' => 'banano'],
        'sumifru/cajas/eliminar' => ['controller' => 'ProduccionCajas', 'action' => 'eliminar', 'path' => 'sumifru', 'module' => 'banano'],
        'sumifru/cajas/graficasBarras' => ['controller' => 'ProduccionCajas', 'action' => 'graficasBarras', 'path' => 'sumifru', 'module' => 'banano'],
        'sumifru/cajas/diferencias' => ['controller' => 'ProduccionCajas', 'action' => 'tablasDiferecias', 'path' => 'sumifru', 'module' => 'banano'],
		'sumifru/cajas/excedente' => ['controller' => 'ProduccionCajas', 'action' => 'historicoExcedente', 'path' => 'sumifru', 'module' => 'banano'],
		'sumifru/cajas/hasData' => ['controller' => 'ProduccionCajas', 'action' => 'hasData', 'path' => 'sumifru', 'module' => 'banano'],
		'sumifru/cajas/editGuia' => ['controller' => 'ProduccionCajas', 'action' => 'editGuia', 'path' => 'sumifru', 'module' => 'banano'],
		'sumifru/cajas/asignarGuia' => ['controller' => 'ProduccionCajas', 'action' => 'asignarGuia', 'path' => 'sumifru', 'module' => 'banano'],
		'sumifru/cajas/borrarGuiaMarca' => ['controller' => 'ProduccionCajas', 'action' => 'borrarGuiaMarca', 'path' => 'sumifru', 'module' => 'banano'],
		
		'palmar/cajas/registros' => ['controller' => 'ProduccionCajas', 'action' => 'registros', 'path' => 'palmar', 'module' => 'banano'],
		'palmar/cajas/last' => ['controller' => 'ProduccionCajas', 'action' => 'last', 'path' => 'palmar', 'module' => 'banano'],
		'palmar/cajas/resumen' => ['controller' => 'ProduccionCajas', 'action' => 'resumenMarca', 'path' => 'palmar', 'module' => 'banano'],
		'palmar/cajas/filters' => ['controller' => 'ProduccionCajas', 'action' => 'filters', 'path' => 'palmar', 'module' => 'banano'],
        'palmar/cajas/cajasSemanal' => ['controller' => 'ProduccionCajas', 'action' => 'historicoCajasSemanal', 'path' => 'palmar', 'module' => 'banano'],
        'palmar/cajas/cajasHectarea' => ['controller' => 'ProduccionCajas', 'action' => 'historicoCajasSemanalHectarea', 'path' => 'palmar', 'module' => 'banano'],
		'palmar/cajas/marcas' => ['controller' => 'ProduccionCajas', 'action' => 'getMarcas', 'path' => 'palmar', 'module' => 'banano'],
        'palmar/cajas/cuadrar' => ['controller' => 'ProduccionCajas', 'action' => 'cuadreCajas', 'path' => 'palmar', 'module' => 'banano'],
        'palmar/cajas/guardarCuadrar' => ['controller' => 'ProduccionCajas', 'action' => 'guardarCuadrar', 'path' => 'palmar', 'module' => 'banano'],
        'palmar/cajas/guias' => ['controller' => 'ProduccionCajas', 'action' => 'getGuias', 'path' => 'palmar', 'module' => 'banano'],
        'palmar/cajas/procesar' => ['controller' => 'ProduccionCajas', 'action' => 'procesar', 'path' => 'palmar', 'module' => 'banano'],
        'palmar/cajas/eliminar' => ['controller' => 'ProduccionCajas', 'action' => 'eliminar', 'path' => 'palmar', 'module' => 'banano'],
        'palmar/cajas/graficasBarras' => ['controller' => 'ProduccionCajas', 'action' => 'graficasBarras', 'path' => 'palmar', 'module' => 'banano'],
        'palmar/cajas/diferencias' => ['controller' => 'ProduccionCajas', 'action' => 'tablasDiferecias', 'path' => 'palmar', 'module' => 'banano'],
		'palmar/cajas/excedente' => ['controller' => 'ProduccionCajas', 'action' => 'historicoExcedente', 'path' => 'palmar', 'module' => 'banano'],
		'palmar/cajas/borrarGuiaMarca' => ['controller' => 'ProduccionCajas', 'action' => 'borrarGuiaMarca', 'path' => 'palmar', 'module' => 'banano'],

        'quintana/clima/index' => ['controller' => 'ClimaSigat', 'action' => 'index', 'path' => 'quintana', 'module' => 'banano'],
        'quintana/clima/horas' => ['controller' => 'ClimaSigat', 'action' => 'graficaHorasDia', 'path' => 'quintana', 'module' => 'banano'],

        'quintana/climaDiario/last' => ['controller' => 'ClimaDiario', 'action' => 'last', 'path' => 'quintana', 'module' => 'banano'],
        'quintana/climaDiario/tags' => ['controller' => 'ClimaDiario', 'action' => 'tags', 'path' => 'quintana', 'module' => 'banano'],
        'quintana/climaDiario/datatable' => ['controller' => 'ClimaDiario', 'action' => 'detalle', 'path' => 'quintana', 'module' => 'banano'],
        'quintana/climaDiario/horasluz' => ['controller' => 'ClimaDiario', 'action' => 'horasLuz', 'path' => 'quintana', 'module' => 'banano'],
        'quintana/climaDiario/viento' => ['controller' => 'ClimaDiario', 'action' => 'graficasViento', 'path' => 'quintana', 'module' => 'banano'],
        'marun/climaDiario/last' => ['controller' => 'ClimaDiario', 'action' => 'last', 'path' => 'marun', 'module' => 'banano'],
        'marun/climaDiario/tags' => ['controller' => 'ClimaDiario', 'action' => 'tags', 'path' => 'marun', 'module' => 'banano'],
        'marun/climaDiario/datatable' => ['controller' => 'ClimaDiario', 'action' => 'detalle', 'path' => 'marun', 'module' => 'banano'],
        'marun/climaDiario/horasluz' => ['controller' => 'ClimaDiario', 'action' => 'horasLuz', 'path' => 'marun', 'module' => 'banano'],
        'marun/climaDiario/viento' => ['controller' => 'ClimaDiario', 'action' => 'graficasViento', 'path' => 'marun', 'module' => 'banano'],
        'marcel/climaDiario/last' => ['controller' => 'ClimaDiario', 'action' => 'last', 'path' => 'marcel', 'module' => 'banano'],
        'marcel/climaDiario/tags' => ['controller' => 'ClimaDiario', 'action' => 'tags', 'path' => 'marcel', 'module' => 'banano'],
        'marcel/climaDiario/datatable' => ['controller' => 'ClimaDiario', 'action' => 'detalle', 'path' => 'marcel', 'module' => 'banano'],
        'marcel/climaDiario/horasluz' => ['controller' => 'ClimaDiario', 'action' => 'horasLuz', 'path' => 'marcel', 'module' => 'banano'],
        'marcel/climaDiario/viento' => ['controller' => 'ClimaDiario', 'action' => 'graficasViento', 'path' => 'marcel', 'module' => 'banano'],
        'reiset/climaDiario/last' => ['controller' => 'ClimaDiario', 'action' => 'last', 'path' => 'reiset', 'module' => 'banano'],
        'reiset/climaDiario/tags' => ['controller' => 'ClimaDiario', 'action' => 'tags', 'path' => 'reiset', 'module' => 'banano'],
        'reiset/climaDiario/datatable' => ['controller' => 'ClimaDiario', 'action' => 'detalle', 'path' => 'reiset', 'module' => 'banano'],
        'reiset/climaDiario/horasluz' => ['controller' => 'ClimaDiario', 'action' => 'horasLuz', 'path' => 'reiset', 'module' => 'banano'],
        'reiset/climaDiario/viento' => ['controller' => 'ClimaDiario', 'action' => 'graficasViento', 'path' => 'reiset', 'module' => 'banano'],

        'marcel/enfunde/last' => ['controller' => 'ProduccionEnfunde', 'action' => 'last', 'path' => 'marcel', 'module' => 'banano'],
        'marcel/enfunde/index' => ['controller' => 'ProduccionEnfunde', 'action' => 'index', 'path' => 'marcel', 'module' => 'banano'],
        'marcel/enfunde/tags' => ['controller' => 'ProduccionEnfunde', 'action' => 'tags', 'path' => 'marcel', 'module' => 'banano'],
        'marcel/enfunde/grafica' => ['controller' => 'ProduccionEnfunde', 'action' => 'graficaSemanal', 'path' => 'marcel', 'module' => 'banano'],
        'marcel/enfunde/loteGrafica' => ['controller' => 'ProduccionEnfunde', 'action' => 'graficaPorLote', 'path' => 'marcel', 'module' => 'banano'],
        'marcel/enfunde/lotesSemanal' => ['controller' => 'ProduccionEnfunde', 'action' => 'lotesSemanal', 'path' => 'marcel', 'module' => 'banano'],
        'marcel/enfunde/enfundadoresSemanal' => ['controller' => 'ProduccionEnfunde', 'action' => 'enfundadoresSemanal', 'path' => 'marcel', 'module' => 'banano'],
        'marcel/enfunde/enfundadoresLote' => ['controller' => 'ProduccionEnfunde', 'action' => 'enfundadoresLote', 'path' => 'marcel', 'module' => 'banano'],
        'marcel/enfunde/edit/index' => ['controller' => 'ProduccionNuevoEnfunde', 'action' => 'index', 'path' => 'marcel', 'module' => 'banano'],
        'marcel/enfunde/edit/cinta' => ['controller' => 'ProduccionNuevoEnfunde', 'action' => 'getColorCinta', 'path' => 'marcel', 'module' => 'banano'],
        'marcel/enfunde/edit/save' => ['controller' => 'ProduccionNuevoEnfunde', 'action' => 'save', 'path' => 'marcel', 'module' => 'banano'],
		'marcel/enfunde/edit/saldo' => ['controller' => 'ProduccionNuevoEnfunde', 'action' => 'saldo_inicial', 'path' => 'marcel', 'module' => 'banano'],

		'marcel/tthhenfunde/last' => ['controller' => 'TTHHProduccionEnfunde', 'action' => 'last', 'path' => 'marcel', 'module' => 'banano'],
        'marcel/tthhenfunde/index' => ['controller' => 'TTHHProduccionEnfunde', 'action' => 'index', 'path' => 'marcel', 'module' => 'banano'],
        'marcel/tthhenfunde/tags' => ['controller' => 'TTHHProduccionEnfunde', 'action' => 'tags', 'path' => 'marcel', 'module' => 'banano'],
        'marcel/tthhenfunde/grafica' => ['controller' => 'TTHHProduccionEnfunde', 'action' => 'graficaSemanal', 'path' => 'marcel', 'module' => 'banano'],
        'marcel/tthhenfunde/loteGrafica' => ['controller' => 'TTHHProduccionEnfunde', 'action' => 'graficaPorLote', 'path' => 'marcel', 'module' => 'banano'],
        'marcel/tthhenfunde/lotesSemanal' => ['controller' => 'TTHHProduccionEnfunde', 'action' => 'lotesSemanal', 'path' => 'marcel', 'module' => 'banano'],
        'marcel/tthhenfunde/enfundadoresSemanal' => ['controller' => 'TTHHProduccionEnfunde', 'action' => 'enfundadoresSemanal', 'path' => 'marcel', 'module' => 'banano'],
        'marcel/tthhenfunde/enfundadoresLote' => ['controller' => 'TTHHProduccionEnfunde', 'action' => 'enfundadoresLote', 'path' => 'marcel', 'module' => 'banano'],
        'marcel/tthhenfunde/edit/index' => ['controller' => 'ProduccionNuevoEnfunde', 'action' => 'index', 'path' => 'marcel', 'module' => 'banano'],
        'marcel/tthhenfunde/edit/cinta' => ['controller' => 'ProduccionNuevoEnfunde', 'action' => 'getColorCinta', 'path' => 'marcel', 'module' => 'banano'],
        'marcel/tthhenfunde/edit/save' => ['controller' => 'ProduccionNuevoEnfunde', 'action' => 'save', 'path' => 'marcel', 'module' => 'banano'],
		'marcel/tthhenfunde/edit/saldo' => ['controller' => 'ProduccionNuevoEnfunde', 'action' => 'saldo_inicial', 'path' => 'marcel', 'module' => 'banano'],
		
		'sumifru/enfunde/last' => ['controller' => 'ProduccionEnfunde', 'action' => 'last', 'path' => 'sumifru', 'module' => 'banano'],
        'sumifru/enfunde/index' => ['controller' => 'ProduccionEnfunde', 'action' => 'index', 'path' => 'sumifru', 'module' => 'banano'],
        'sumifru/enfunde/tags' => ['controller' => 'ProduccionEnfunde', 'action' => 'tags', 'path' => 'sumifru', 'module' => 'banano'],
        'sumifru/enfunde/grafica' => ['controller' => 'ProduccionEnfunde', 'action' => 'graficaSemanal', 'path' => 'sumifru', 'module' => 'banano'],
        'sumifru/enfunde/loteGrafica' => ['controller' => 'ProduccionEnfunde', 'action' => 'graficaPorLote', 'path' => 'sumifru', 'module' => 'banano'],
        'sumifru/enfunde/lotesSemanal' => ['controller' => 'ProduccionEnfunde', 'action' => 'lotesSemanal', 'path' => 'sumifru', 'module' => 'banano'],
        'sumifru/enfunde/enfundadoresSemanal' => ['controller' => 'ProduccionEnfunde', 'action' => 'enfundadoresSemanal', 'path' => 'sumifru', 'module' => 'banano'],
        'sumifru/enfunde/enfundadoresLote' => ['controller' => 'ProduccionEnfunde', 'action' => 'enfundadoresLote', 'path' => 'sumifru', 'module' => 'banano'],
        'sumifru/enfunde/edit/index' => ['controller' => 'ProduccionNuevoEnfunde', 'action' => 'index', 'path' => 'sumifru', 'module' => 'banano'],
        'sumifru/enfunde/edit/cinta' => ['controller' => 'ProduccionNuevoEnfunde', 'action' => 'getColorCinta', 'path' => 'sumifru', 'module' => 'banano'],
        'sumifru/enfunde/edit/save' => ['controller' => 'ProduccionNuevoEnfunde', 'action' => 'save', 'path' => 'sumifru', 'module' => 'banano'],
		'sumifru/enfunde/edit/saldo' => ['controller' => 'ProduccionNuevoEnfunde', 'action' => 'saldo_inicial', 'path' => 'sumifru', 'module' => 'banano'],
		
		'palmar/enfunde/last' => ['controller' => 'ProduccionEnfunde', 'action' => 'last', 'path' => 'palmar', 'module' => 'banano'],
        'palmar/enfunde/index' => ['controller' => 'ProduccionEnfunde', 'action' => 'index', 'path' => 'palmar', 'module' => 'banano'],
        'palmar/enfunde/tags' => ['controller' => 'ProduccionEnfunde', 'action' => 'tags', 'path' => 'palmar', 'module' => 'banano'],
        'palmar/enfunde/grafica' => ['controller' => 'ProduccionEnfunde', 'action' => 'graficaSemanal', 'path' => 'palmar', 'module' => 'banano'],
        'palmar/enfunde/loteGrafica' => ['controller' => 'ProduccionEnfunde', 'action' => 'graficaPorLote', 'path' => 'palmar', 'module' => 'banano'],
        'palmar/enfunde/lotesSemanal' => ['controller' => 'ProduccionEnfunde', 'action' => 'lotesSemanal', 'path' => 'palmar', 'module' => 'banano'],
        'palmar/enfunde/enfundadoresSemanal' => ['controller' => 'ProduccionEnfunde', 'action' => 'enfundadoresSemanal', 'path' => 'palmar', 'module' => 'banano'],
        'palmar/enfunde/enfundadoresLote' => ['controller' => 'ProduccionEnfunde', 'action' => 'enfundadoresLote', 'path' => 'palmar', 'module' => 'banano'],
        'palmar/enfunde/edit/index' => ['controller' => 'ProduccionNuevoEnfunde', 'action' => 'index', 'path' => 'palmar', 'module' => 'banano'],
        'palmar/enfunde/edit/cinta' => ['controller' => 'ProduccionNuevoEnfunde', 'action' => 'getColorCinta', 'path' => 'palmar', 'module' => 'banano'],
        'palmar/enfunde/edit/save' => ['controller' => 'ProduccionNuevoEnfunde', 'action' => 'save', 'path' => 'palmar', 'module' => 'banano'],
        'palmar/enfunde/edit/saldo' => ['controller' => 'ProduccionNuevoEnfunde', 'action' => 'saldo_inicial', 'path' => 'palmar', 'module' => 'banano'],

        'reiset/enfunde/last' => ['controller' => 'ProduccionEnfunde', 'action' => 'last', 'path' => 'reiset', 'module' => 'banano'],
        'reiset/enfunde/index' => ['controller' => 'ProduccionEnfunde', 'action' => 'index', 'path' => 'reiset', 'module' => 'banano'],
        'reiset/enfunde/tags' => ['controller' => 'ProduccionEnfunde', 'action' => 'tags', 'path' => 'reiset', 'module' => 'banano'],
        'reiset/enfunde/grafica' => ['controller' => 'ProduccionEnfunde', 'action' => 'graficaSemanal', 'path' => 'reiset', 'module' => 'banano'],
        'reiset/enfunde/loteGrafica' => ['controller' => 'ProduccionEnfunde', 'action' => 'graficaPorLote', 'path' => 'reiset', 'module' => 'banano'],
        'reiset/enfunde/lotesSemanal' => ['controller' => 'ProduccionEnfunde', 'action' => 'lotesSemanal', 'path' => 'reiset', 'module' => 'banano'],
        'reiset/enfunde/enfundadoresSemanal' => ['controller' => 'ProduccionEnfunde', 'action' => 'enfundadoresSemanal', 'path' => 'reiset', 'module' => 'banano'],
        'reiset/enfunde/enfundadoresLote' => ['controller' => 'ProduccionEnfunde', 'action' => 'enfundadoresLote', 'path' => 'reiset', 'module' => 'banano'],
        'reiset/enfunde/edit/index' => ['controller' => 'ProduccionNuevoEnfunde', 'action' => 'index', 'path' => 'reiset', 'module' => 'banano'],
        'reiset/enfunde/edit/cinta' => ['controller' => 'ProduccionNuevoEnfunde', 'action' => 'getColorCinta', 'path' => 'reiset', 'module' => 'banano'],
        'reiset/enfunde/edit/save' => ['controller' => 'ProduccionNuevoEnfunde', 'action' => 'save', 'path' => 'reiset', 'module' => 'banano'],
        'reiset/enfunde/edit/saldo' => ['controller' => 'ProduccionNuevoEnfunde', 'action' => 'saldo_inicial', 'path' => 'reiset', 'module' => 'banano'],
        
        'quintana/produccion/promedios' => ['controller' => 'Produccion', 'action' => 'promediosLotes', 'path' => 'quintana', 'module' => 'banano'],

        'marlon/laboresAgricolas/tendencia' => ['controller' => 'LaboresAgricolas', 'action' => 'tendencia', 'path' => 'marlon', 'module' => 'banano'],
        'marlon/laboresAgricolas/tablaHistorica' => ['controller' => 'LaboresAgricolas', 'action' => 'tablaHistorica', 'path' => 'marlon', 'module' => 'banano'],
        'marlon/laboresAgricolas/graficaLabores' => ['controller' => 'LaboresAgricolas', 'action' => 'graficaLabores', 'path' => 'marlon', 'module' => 'banano'],
		'marlon/laboresAgricolas/fotos' => ['controller' => 'LaboresAgricolas', 'action' => 'getFotos', 'path' => 'marlon', 'module' => 'banano'],
		'marlon/laboresAgricolas/last' => ['controller' => 'LaboresAgricolas', 'action' => 'last', 'path' => 'marlon', 'module' => 'banano'],

		'quintana/laboresAgricolas/tendencia' => ['controller' => 'LaboresAgricolas', 'action' => 'tendencia', 'path' => 'quintana', 'module' => 'banano'],
        'quintana/laboresAgricolas/tablaHistorica' => ['controller' => 'LaboresAgricolas', 'action' => 'tablaHistorica', 'path' => 'quintana', 'module' => 'banano'],
        'quintana/laboresAgricolas/graficaLabores' => ['controller' => 'LaboresAgricolas', 'action' => 'graficaLabores', 'path' => 'quintana', 'module' => 'banano'],
		'quintana/laboresAgricolas/fotos' => ['controller' => 'LaboresAgricolas', 'action' => 'getFotos', 'path' => 'quintana', 'module' => 'banano'],
		'quintana/laboresAgricolas/personal' => ['controller' => 'LaboresAgricolasPersonal', 'action' => 'index', 'path' => 'quintana', 'module' => 'banano'],
		
		'palmar/laboresAgricolas/tendencia' => ['controller' => 'LaboresAgricolas', 'action' => 'tendencia', 'path' => 'palmar', 'module' => 'banano'],
        'palmar/laboresAgricolas/tablaHistorica' => ['controller' => 'LaboresAgricolas', 'action' => 'tablaHistorica', 'path' => 'palmar', 'module' => 'banano'],
        'palmar/laboresAgricolas/graficaLabores' => ['controller' => 'LaboresAgricolas', 'action' => 'graficaLabores', 'path' => 'palmar', 'module' => 'banano'],
        'palmar/laboresAgricolas/fotos' => ['controller' => 'LaboresAgricolas', 'action' => 'getFotos', 'path' => 'palmar', 'module' => 'banano'],

        'mario/laboresAgricolas/tendencia' => ['controller' => 'LaboresAgricolas', 'action' => 'tendencia', 'path' => 'mario', 'module' => 'banano'],
        'mario/laboresAgricolas/tablaHistorica' => ['controller' => 'LaboresAgricolas', 'action' => 'tablaHistorica', 'path' => 'mario', 'module' => 'banano'],
        'mario/laboresAgricolas/graficaLabores' => ['controller' => 'LaboresAgricolas', 'action' => 'graficaLabores', 'path' => 'mario', 'module' => 'banano'],
        'mario/laboresAgricolas/fotos' => ['controller' => 'LaboresAgricolas', 'action' => 'getFotos', 'path' => 'mario', 'module' => 'banano'],

        'marun/cacao/last' => ['controller' => 'CacaoAnalisis', 'action' => 'last', 'path' => 'marun', 'module' => 'cacao'],
        'marun/cacao/filters' => ['controller' => 'CacaoAnalisis', 'action' => 'filters', 'path' => 'marun', 'module' => 'cacao'],
        'marun/cacao/analisis' => ['controller' => 'CacaoAnalisis', 'action' => 'analisis', 'path' => 'marun', 'module' => 'cacao'],
        'marun/asignacionCodigosMallas/data' => ['controller' => 'AsignacionCodigosMallas', 'action' => 'revision', 'path' => 'marun', 'module' => 'cacao'],
        'marun/asignacionCodigosMallas/save' => ['controller' => 'AsignacionCodigosMallas', 'action' => 'save', 'path' => 'marun', 'module' => 'cacao'],
        'marun/formularioCosecha/data' => ['controller' => 'FormularioCosecha', 'action' => 'revision', 'path' => 'marun', 'module' => 'cacao'],
		'marun/formularioCosecha/save' => ['controller' => 'FormularioCosecha', 'action' => 'save', 'path' => 'marun', 'module' => 'cacao'],
		'marun/balanzaMallas/data' => ['controller' => 'BalanzaMallas', 'action' => 'revision', 'path' => 'marun', 'module' => 'cacao'],
		'marun/balanzaMallas/save' => ['controller' => 'BalanzaMallas', 'action' => 'save', 'path' => 'marun', 'module' => 'cacao'],
		'marun/reporteCosecha/data' => ['controller' => 'ReporteCosecha', 'action' => 'index', 'path' => 'marun', 'module' => 'cacao'],
		'marun/reporteCosecha/chart' => ['controller' => 'ReporteCosecha', 'action' => 'chart', 'path' => 'marun', 'module' => 'cacao'],
		'marun/reporteCosecha/save' => ['controller' => 'ReporteCosecha', 'action' => 'save', 'path' => 'marun', 'module' => 'cacao'],
		'marun/reporteCosechaPeriodal/data' => ['controller' => 'ReporteCosechaPeriodal', 'action' => 'index', 'path' => 'marun', 'module' => 'cacao'],
		'marun/reporteCosechaPeriodal/chart' => ['controller' => 'ReporteCosechaPeriodal', 'action' => 'chart', 'path' => 'marun', 'module' => 'cacao'],
		'marun/reporteCosechaPeriodal/save' => ['controller' => 'ReporteCosechaPeriodal', 'action' => 'save', 'path' => 'marun', 'module' => 'cacao'],
		'marun/reporteCosechaSector/data' => ['controller' => 'ReporteCosechaSector', 'action' => 'index', 'path' => 'marun', 'module' => 'cacao'],
		'marun/reporteCosechaSector/chart' => ['controller' => 'ReporteCosechaSector', 'action' => 'chart', 'path' => 'marun', 'module' => 'cacao'],
		'marun/reporteCosechaSector/save' => ['controller' => 'ReporteCosechaSector', 'action' => 'save', 'path' => 'marun', 'module' => 'cacao'],
		'marun/comparativoCosecha/data' => ['controller' => 'ComparativoCosecha', 'action' => 'index', 'path' => 'marun', 'module' => 'cacao'],
		'marun/consolidadoCosecha/data' => ['controller' => 'ConsolidadoCosecha', 'action' => 'index', 'path' => 'marun', 'module' => 'cacao'],

		'marcel/recobro/data' => ['controller' => 'Recobro', 'action' => 'data', 'path' => 'marcel', 'module' => 'banano'],
		'marcel/recobro/saveCaidos' => ['controller' => 'Recobro', 'action' => 'saveCaidos', 'path' => 'marcel', 'module' => 'banano'],
		'marcel/recobro/papiro' => ['controller' => 'Recobro', 'action' => 'papiro', 'path' => 'marcel', 'module' => 'banano'],
		'orodelti/recobro/data' => ['controller' => 'Recobro', 'action' => 'data', 'path' => 'orodelti', 'module' => 'banano'],
		'orodelti/recobro/papiro' => ['controller' => 'Recobro', 'action' => 'papiro', 'path' => 'orodelti', 'module' => 'banano'],
		'reiset/recobro/data' => ['controller' => 'Recobro', 'action' => 'data', 'path' => 'reiset', 'module' => 'banano'],
		'quintana/recobro/data' => ['controller' => 'Recobro', 'action' => 'data', 'path' => 'quintana', 'module' => 'banano'],
		'sumifru/recobro/data' => ['controller' => 'Recobro', 'action' => 'data', 'path' => 'sumifru', 'module' => 'banano'],
		'palmar/recobro/data' => ['controller' => 'Recobro', 'action' => 'data', 'path' => 'palmar', 'module' => 'banano'],
		'marun/recobro/data' => ['controller' => 'Recobro', 'action' => 'data', 'path' => 'marun', 'module' => 'banano'],
		'marun/recobro/papiro' => ['controller' => 'Recobro', 'action' => 'papiro', 'path' => 'marun', 'module' => 'banano'],
        
		'marun/produccionreporte/data' => ['controller' => 'ReporteProduccion', 'action' => 'main', 'path' => 'marun', 'module' => 'banano'],
		'marun/produccionreporte/muestreo' => ['controller' => 'ReporteProduccion', 'action' => 'muestreo', 'path' => 'marun', 'module' => 'banano'],
		'marun/produccionreporte/weeks' => ['controller' => 'ReporteProduccion', 'action' => 'getWeeks', 'path' => 'marun', 'module' => 'banano'],
        'marun/produccionreporte/last' => ['controller' => 'ReporteProduccion', 'action' => 'last', 'path' => 'marun', 'module' => 'banano'],

        'marcel/produccionreporte/data' => ['controller' => 'ReporteProduccion', 'action' => 'main', 'path' => 'marcel', 'module' => 'banano'],
		'marcel/produccionreporte/weeks' => ['controller' => 'ReporteProduccion', 'action' => 'getWeeks', 'path' => 'marcel', 'module' => 'banano'],
		'marcel/produccionreporte/last' => ['controller' => 'ReporteProduccion', 'action' => 'last', 'path' => 'marcel', 'module' => 'banano'],

		'orodelti/produccionreporte/data' => ['controller' => 'ReporteProduccion', 'action' => 'main', 'path' => 'orodelti', 'module' => 'banano'],
		'orodelti/produccionreporte/weeks' => ['controller' => 'ReporteProduccion', 'action' => 'getWeeks', 'path' => 'orodelti', 'module' => 'banano'],
		'orodelti/produccionreporte/last' => ['controller' => 'ReporteProduccion', 'action' => 'last', 'path' => 'orodelti', 'module' => 'banano'],
		
		'sumifru/produccionreporte/data' => ['controller' => 'ReporteProduccion', 'action' => 'main', 'path' => 'sumifru', 'module' => 'banano'],
		'sumifru/produccionreporte/weeks' => ['controller' => 'ReporteProduccion', 'action' => 'getWeeks', 'path' => 'sumifru', 'module' => 'banano'],
		'sumifru/produccionreporte/last' => ['controller' => 'ReporteProduccion', 'action' => 'last', 'path' => 'sumifru', 'module' => 'banano'],
		'sumifru/produccionreporte/fincas' => ['controller' => 'ReporteProduccion', 'action' => 'getFincas', 'path' => 'sumifru', 'module' => 'banano'],

		'palmar/produccionreporte/data' => ['controller' => 'ReporteProduccion', 'action' => 'main', 'path' => 'palmar', 'module' => 'banano'],
		'palmar/produccionreporte/weeks' => ['controller' => 'ReporteProduccion', 'action' => 'getWeeks', 'path' => 'palmar', 'module' => 'banano'],
		'palmar/produccionreporte/last' => ['controller' => 'ReporteProduccion', 'action' => 'last', 'path' => 'palmar', 'module' => 'banano'],
		'palmar/produccionreporte/fincas' => ['controller' => 'ReporteProduccion', 'action' => 'getFincas', 'path' => 'palmar', 'module' => 'banano'],

		'marcel/resumencorte/resumenProduccion' => ['controller' => 'ProduccionResumenCorte', 'action' => 'resumenProceso', 'path' => 'marcel', 'module' => 'banano'],
		'sumifru/resumencorte/resumenProduccion' => ['controller' => 'ProduccionResumenCorte', 'action' => 'resumenProceso', 'path' => 'sumifru', 'module' => 'banano'],
		'orodelti/resumencorte/resumenProduccion' => ['controller' => 'ProduccionResumenCorte', 'action' => 'resumenProceso', 'path' => 'orodelti', 'module' => 'banano'],
		'palmar/resumencorte/resumenProduccion' => ['controller' => 'ProduccionResumenCorte', 'action' => 'resumenProceso', 'path' => 'palmar', 'module' => 'banano'],
		'quintana/resumencorte/resumenProduccion' => ['controller' => 'ProduccionResumenCorte', 'action' => 'resumenProceso', 'path' => 'quintana', 'module' => 'banano'],
		'marun/resumencorte/cajas' => ['controller' => 'ProduccionResumenCorte', 'action' => 'resumenCajas', 'path' => 'marun', 'module' => 'banano'],
        'marun/resumencorte/resumenProduccion' => ['controller' => 'ProduccionResumenCorte', 'action' => 'resumenProceso', 'path' => 'marun', 'module' => 'banano'],
		'marun/resumencorte/filters' => ['controller' => 'ProduccionResumenCorte', 'action' => 'filters' , 'path' => 'marun', 'module' => 'banano'],
		'reiset/resumencorte/resumenProduccion' => ['controller' => 'ProduccionResumenCorte', 'action' => 'resumenProceso', 'path' => 'reiset', 'module' => 'banano'],
        'reiset/resumencorte/filters' => ['controller' => 'ProduccionResumenCorte', 'action' => 'filters' , 'path' => 'reiset', 'module' => 'banano'],
        
		'reiset/produccionreporte/data' => ['controller' => 'ReporteProduccion', 'action' => 'main', 'path' => 'reiset', 'module' => 'banano'],
		'reiset/produccionreporte/reporte2' => ['controller' => 'ReporteProduccion', 'action' => 'reporte2', 'path' => 'reiset', 'module' => 'banano'],
		'reiset/produccionreporte/weeks' => ['controller' => 'ReporteProduccion', 'action' => 'getWeeks', 'path' => 'reiset', 'module' => 'banano'],
		'reiset/produccionreporte/last' => ['controller' => 'ReporteProduccion', 'action' => 'last', 'path' => 'reiset', 'module' => 'banano'],

		'reiset/produccionreporteacumulado/data2' => ['controller' => 'ReporteProduccionAcumulado', 'action' => 'reporte2', 'path' => 'reiset', 'module' => 'banano'],
		'reiset/produccionreporteacumulado/weeks' => ['controller' => 'ReporteProduccionAcumulado', 'action' => 'getWeeks', 'path' => 'reiset', 'module' => 'banano'],
        'reiset/produccionreporteacumulado/last' => ['controller' => 'ReporteProduccionAcumulado', 'action' => 'last', 'path' => 'reiset', 'module' => 'banano'],
        /* CONFIG LANCOFRUIT */
        'lancofruit/categoriasConfig/index' => ['controller' => 'ConfigLancofruit', 'action' => 'index', 'module' => 'banano'],
        'lancofruit/categoriasConfig/save' => ['controller' => 'ConfigLancofruit', 'action' => 'guardarCategoria', 'module' => 'banano'],
        'lancofruit/categoriasConfig/borrar' => ['controller' => 'ConfigLancofruit', 'action' => 'archivarCategoria', 'module' => 'banano'],
        'lancofruit/categoriasConfig/toggleStatus' => ['controller' => 'ConfigLancofruit', 'action' => 'toggleStatusCategoria', 'module' => 'banano'],
        'lancofruit/subcategoriasConfig/save' => ['controller' => 'ConfigLancofruit', 'action' => 'guardarSubcategoria', 'module' => 'banano'],
        'lancofruit/subcategoriasConfig/borrar' => ['controller' => 'ConfigLancofruit', 'action' => 'archivarSubcategoria', 'module' => 'banano'],
        'lancofruit/subcategoriasConfig/toggleStatus' => ['controller' => 'ConfigLancofruit', 'action' => 'toggleStatusSubcategoria', 'module' => 'banano'],

        'marcel/resumenenfunde/last' => ['controller' => 'Enfunde', 'action' => 'last', 'path' => 'marcel', 'module' => 'banano'],
		'marcel/resumenenfunde/index' => ['controller' => 'Enfunde', 'action' => 'index', 'path' => 'marcel', 'module' => 'banano'],
		'orodelti/resumenenfunde/last' => ['controller' => 'Enfunde', 'action' => 'last', 'path' => 'orodelti', 'module' => 'banano'],
		'orodelti/resumenenfunde/index' => ['controller' => 'Enfunde', 'action' => 'index', 'path' => 'orodelti', 'module' => 'banano'],
		'sumifru/resumenenfunde/last' => ['controller' => 'Enfunde', 'action' => 'last', 'path' => 'sumifru', 'module' => 'banano'],
        'sumifru/resumenenfunde/index' => ['controller' => 'Enfunde', 'action' => 'index', 'path' => 'sumifru', 'module' => 'banano'],
        'reiset/resumenenfunde/last' => ['controller' => 'Enfunde', 'action' => 'last', 'path' => 'reiset', 'module' => 'banano'],
		'reiset/resumenenfunde/index' => ['controller' => 'Enfunde', 'action' => 'index', 'path' => 'reiset', 'module' => 'banano'],
		'quintana/resumenenfunde/last' => ['controller' => 'Enfunde', 'action' => 'last', 'path' => 'quintana', 'module' => 'banano'],
		'quintana/resumenenfunde/index' => ['controller' => 'Enfunde', 'action' => 'index', 'path' => 'quintana', 'module' => 'banano'],
		'palmar/resumenenfunde/last' => ['controller' => 'Enfunde', 'action' => 'last', 'path' => 'palmar', 'module' => 'banano'],
		'palmar/resumenenfunde/index' => ['controller' => 'Enfunde', 'action' => 'index', 'path' => 'palmar', 'module' => 'banano'],
		'marun/resumenenfunde/last' => ['controller' => 'Enfunde', 'action' => 'last', 'path' => 'marun', 'module' => 'banano'],
		'marun/resumenenfunde/index' => ['controller' => 'Enfunde', 'action' => 'index', 'path' => 'marun', 'module' => 'banano'],
		
		'config/lotes/last' => ['controller' => 'ConfiguracionApp', 'action' => 'last'],
		'config/lotes/index' => ['controller' => 'ConfiguracionApp', 'action' => 'index'],
		'config/lotes/saveha' => ['controller' => 'ConfiguracionApp', 'action' => 'saveHa'],

		'config/plantas/last' => ['controller' => 'ConfiguracionPlantas', 'action' => 'last'],
		'config/plantas/index' => ['controller' => 'ConfiguracionPlantas', 'action' => 'index'],
		'config/plantas/saveha' => ['controller' => 'ConfiguracionPlantas', 'action' => 'saveHa'],

		'marcel/precalibracion/last' => ['controller' => 'PreCalibracion', 'action' => 'last', 'path' => 'marcel'],
		'marcel/precalibracion/index' => ['controller' => 'PreCalibracion', 'action' => 'index', 'path' => 'marcel'],
		'marcel/precalibracion/barrer' => ['controller' => 'PreCalibracion', 'action' => 'barrer', 'path' => 'marcel'],
		'marcel/precalibracion/porprecalibrar' => ['controller' => 'PreCalibracion', 'action' => 'porPrecalibrar', 'path' => 'marcel'],
		'marcel/precalibracion/porcosechar' => ['controller' => 'PreCalibracion', 'action' => 'porCosechar', 'path' => 'marcel'],
		'marcel/precalibracion/precalibrado' => ['controller' => 'PreCalibracion', 'action' => 'precalibrado', 'path' => 'marcel'],
		'marcel/precalibracion/database' => ['controller' => 'PreCalibracion', 'action' => 'database', 'path' => 'marcel'],
		'marcel/precalibracion/guardarEditar' => ['controller' => 'PreCalibracion', 'action' => 'guardarEditar', 'path' => 'marcel'],
		'marcel/precalibracion/borrar' => ['controller' => 'PreCalibracion', 'action' => 'borrar', 'path' => 'marcel'],

		'precalibracion/last' => ['controller' => 'PreCalibracion', 'action' => 'last'],
		'precalibracion/index' => ['controller' => 'PreCalibracion', 'action' => 'index'],
		'precalibracion/barrer' => ['controller' => 'PreCalibracion', 'action' => 'barrer'],
		'precalibracion/porprecalibrar' => ['controller' => 'PreCalibracion', 'action' => 'porPrecalibrar'],
		'precalibracion/porcosechar' => ['controller' => 'PreCalibracion', 'action' => 'porCosechar'],
		'precalibracion/precalibrado' => ['controller' => 'PreCalibracion', 'action' => 'precalibrado'],
		'precalibracion/database' => ['controller' => 'PreCalibracion', 'action' => 'database'],
		'precalibracion/guardarEditar' => ['controller' => 'PreCalibracion', 'action' => 'guardarEditar'],
		'precalibracion/borrar' => ['controller' => 'PreCalibracion', 'action' => 'borrar'],

		'marun/cacaoConfig/lotes' => ['controller' => 'ConfiguracionLotes', 'action' => 'index', 'path' => 'marun', 'module' => 'cacao'],

		'tutoriales/index' => ['controller' => 'Tutoriales', 'action' => 'index', 'module' => 'banano'],

		'calidadComparacion/last' => ['controller' => 'CalidadComparacion', 'action' => 'last', 'module' => 'banano', 'path' => 'sumifru'],
		'calidadComparacion/variables' => ['controller' => 'CalidadComparacion', 'action' => 'variables', 'module' => 'banano', 'path' => 'sumifru'],
		'calidadComparacion/principal' => ['controller' => 'CalidadComparacion', 'action' => 'principal', 'module' => 'banano', 'path' => 'sumifru'],
		'calidadComparacion/graficaZona' => ['controller' => 'CalidadComparacion', 'action' => 'graficaZona', 'module' => 'banano', 'path' => 'sumifru'],

		'calidadTendencia/last' => ['controller' => 'CalidadTendencia', 'action' => 'last', 'module' => 'banano', 'path' => 'sumifru'],
		'calidadTendencia/index' => ['controller' => 'CalidadTendencia', 'action' => 'index', 'module' => 'banano', 'path' => 'sumifru'],

		'agroaereo/laboresAgricolasDia/last' => ['controller' => 'LaboresAgricolasDia', 'action' => 'last', 'module' => 'banano', 'path' => 'agroaereo'],
		'agroaereo/laboresAgricolasDia/index' => ['controller' => 'LaboresAgricolasDia', 'action' => 'index', 'module' => 'banano' , 'path' => 'agroaereo'],
		'agroaereo/laboresAgricolasDia/variables' => ['controller' => 'LaboresAgricolasDia', 'action' => 'variables', 'module' => 'banano' , 'path' => 'agroaereo'],
		'agroaereo/laboresAgricolasDia/pieCausas' => ['controller' => 'LaboresAgricolasDia', 'action' => 'pieCausas', 'module' => 'banano' , 'path' => 'agroaereo'],
		'agroaereo/laboresAgricolasDia/calidadLabor' => ['controller' => 'LaboresAgricolasDia', 'action' => 'calidadLabor', 'module' => 'banano' , 'path' => 'agroaereo'],
		'agroaereo/laboresAgricolasDia/calidadLote' => ['controller' => 'LaboresAgricolasDia', 'action' => 'calidadLote', 'module' => 'banano' , 'path' => 'agroaereo'],
		'agroaereo/laboresAgricolasDia/markers' => ['controller' => 'LaboresAgricolasDia', 'action' => 'markers', 'module' => 'banano', 'path' => 'agroaereo'],

		'laboresAgricolasDia/last' => ['controller' => 'LaboresAgricolasDia', 'action' => 'last', 'module' => 'banano'],
		'laboresAgricolasDia/index' => ['controller' => 'LaboresAgricolasDia', 'action' => 'index', 'module' => 'banano'],
		'laboresAgricolasDia/variables' => ['controller' => 'LaboresAgricolasDia', 'action' => 'variables', 'module' => 'banano'],
		'laboresAgricolasDia/pieCausas' => ['controller' => 'LaboresAgricolasDia', 'action' => 'pieCausas', 'module' => 'banano'],
		'laboresAgricolasDia/calidadLabor' => ['controller' => 'LaboresAgricolasDia', 'action' => 'calidadLabor', 'module' => 'banano'],
		'laboresAgricolasDia/calidadLote' => ['controller' => 'LaboresAgricolasDia', 'action' => 'calidadLote', 'module' => 'banano'],
		'laboresAgricolasDia/markers' => ['controller' => 'LaboresAgricolasDia', 'action' => 'markers', 'module' => 'banano'],

		'laboresAgricolasComparativo/last' => ['controller' => 'LaboresAgricolasComparativo', 'action' => 'last', 'module' => 'banano'],
		'laboresAgricolasComparativo/index' => ['controller' => 'LaboresAgricolasComparativo', 'action' => 'index', 'module' => 'banano'],
		'laboresAgricolasComparativo/variable' => ['controller' => 'LaboresAgricolasComparativo', 'action' => 'variable', 'module' => 'banano'],
		'laboresAgricolasComparativo/markers' => ['controller' => 'LaboresAgricolasComparativo', 'action' => 'markers', 'module' => 'banano'],
		'laboresAgricolasComparativo/tablaPorCausa' => ['controller' => 'LaboresAgricolasComparativo', 'action' => 'tablaPorCausa', 'module' => 'banano'],

		'laboresAgricolasTendencia/last' => ['controller' => 'LaboresAgricolasTendencia', 'action' => 'last', 'module' => 'banano'],
		'laboresAgricolasTendencia/index' => ['controller' => 'LaboresAgricolasTendencia', 'action' => 'index', 'module' => 'banano'],
		'laboresAgricolasTendencia/variables' => ['controller' => 'LaboresAgricolasTendencia', 'action' => 'variablesCausas', 'module' => 'banano'],
		'laboresAgricolasTendencia/causasPeriodo' => ['controller' => 'LaboresAgricolasTendencia', 'action' => 'causasPeriodo', 'module' => 'banano'],
		'laboresAgricolasTendencia/calidadPeriodo' => ['controller' => 'LaboresAgricolasTendencia', 'action' => 'calidadPeriodo', 'module' => 'banano'],

		'palmaLaboresAgricolasDia/last' => ['controller' => 'PalmaLaboresAgricolasDia', 'action' => 'last', 'module' => 'palma'],
		'palmaLaboresAgricolasDia/index' => ['controller' => 'PalmaLaboresAgricolasDia', 'action' => 'index', 'module' => 'palma'],
		'palmaLaboresAgricolasDia/variables' => ['controller' => 'PalmaLaboresAgricolasDia', 'action' => 'variables', 'module' => 'palma'],
		'palmaLaboresAgricolasDia/pieCausas' => ['controller' => 'PalmaLaboresAgricolasDia', 'action' => 'pieCausas', 'module' => 'palma'],
		'palmaLaboresAgricolasDia/calidadLabor' => ['controller' => 'PalmaLaboresAgricolasDia', 'action' => 'calidadLabor', 'module' => 'palma'],
		'palmaLaboresAgricolasDia/calidadLote' => ['controller' => 'PalmaLaboresAgricolasDia', 'action' => 'calidadLote', 'module' => 'palma'],
		'palmaLaboresAgricolasDia/markers' => ['controller' => 'PalmaLaboresAgricolasDia', 'action' => 'markers', 'module' => 'palma'],

		'monitor/last' => ['controller' => 'MonitorProntoforms', 'action' => 'last'],
		'monitor/index' => ['controller' => 'MonitorProntoforms', 'action' => 'index'],
		'monitor/comentario' => ['controller' => 'MonitorProntoforms', 'action' => 'comentario'],

		'monitor/balanzas/index' => ['controller' => 'MonitorBalanzas', 'action' => 'index'],
		'monitor/balanzas/comentario' => ['controller' => 'MonitorBalanzas', 'action' => 'comentario'],
		'monitor/balanzas/getcomentario' => ['controller' => 'MonitorBalanzas', 'action' => 'getComentario'],

		'enfunde/semanal/index' => ['controller' => 'NuevoEnfundeSemanal', 'action' => 'index', 'path' => 'sumifru'],
		'enfunde/semanal/last' => ['controller' => 'NuevoEnfundeSemanal', 'action' => 'last', 'path' => 'sumifru'],
		'enfunde/semanal/guardar' => ['controller' => 'NuevoEnfundeSemanal', 'action' => 'guardar', 'path' => 'sumifru'],

		'marcas/saveMarca' => ['controller' => 'Marcas', 'action' => 'saveMarca'],
		'marcas/deleteMarca' => ['controller' => 'Marcas', 'action' => 'deleteMarca'],
		'marcas/addAlias' => ['controller' => 'Marcas', 'action' => 'addAlias'],
		'marcas/saveAlias' => ['controller' => 'Marcas', 'action' => 'saveAlias'],
		'marcas/deleteAlias' => ['controller' => 'Marcas', 'action' => 'deleteAlias'],
		'marcas/addRango' => ['controller' => 'Marcas', 'action' => 'addRango'],
		'marcas/saveRango' => ['controller' => 'Marcas', 'action' => 'saveRango'],
		'marcas/deleteRango' => ['controller' => 'Marcas', 'action' => 'deleteRango'],

		'perchasDia/last' => ['controller' => 'PerchasDia', 'action' => 'last', 'module' => 'banano'],
		'perchasDia/variables' => ['controller' => 'PerchasDia', 'action' => 'variables', 'module' => 'banano'],
		'perchasDia/index' => ['controller' => 'PerchasDia', 'action' => 'index', 'module' => 'banano'],
		'perchasComparacion/last' => ['controller' => 'PerchasComparacion', 'action' => 'last', 'module' => 'banano'],
		'perchasComparacion/variables' => ['controller' => 'PerchasComparacion', 'action' => 'variables', 'module' => 'banano'],
		'perchasComparacion/principal' => ['controller' => 'PerchasComparacion', 'action' => 'principal', 'module' => 'banano'],
		'perchasComparacion/graficaZona' => ['controller' => 'PerchasComparacion', 'action' => 'graficaZona', 'module' => 'banano'],
		'perchasTendencia/last' => ['controller' => 'PerchasTendencia', 'action' => 'last', 'module' => 'banano'],
		'perchasTendencia/index' => ['controller' => 'PerchasTendencia', 'action' => 'index', 'module' => 'banano'],
		
		'bdPerchas/list' => ['controller' => 'PerchasBD', 'action' => 'listado', 'module' => 'banano'],

		'comparacionAnual/grafica' => ['controller' => 'ProduccionComparacionAnual', 'action' => 'grafica', 'module' => 'banano'],
		'comparacionAnual/variables' => ['controller' => 'ProduccionComparacionAnual', 'action' => 'variables', 'module' => 'banano'],
	],
	'modules' => [
		'general' => [
			'views' => ['general', 'maintenance', 'tutoriales', 'modulo'],
			'control' => ['General'],
            'rules' => [''],
            'moduleSystem' => 'banano'
		],
		'configuracion' => [
			'views' => ['fincas' , 'finca' , 'empleados' , 'empleado' , 'auditores' , 'auditor' , 'responsables' , 'responsable', 'configMarcas', 'editMarca', 'configCategorias', 'editCategoria', 'configDefectos', 'editDefecto', 'configExportadores', 'editExportador', 'configDestinos', 'editDestino', 'configLotes', 'configPlantas'],
			'control' => ['Fincas' , 'Empleados' , 'Auditores' , 'Responsables'],
            'rules' => [''],
            'moduleSystem' => 'banano'
		],
		'agroaudit' => [
			'views' => ['reportes' , 'geoposicion' , 'rpersonal','labores', 'laboresAgricolas', 'laboresAgricolasDia', 'laboresAgricolasComparativo', 'laboresAgricolasTendencia',
						'laboresAgricolasComparacion', 'laboresAgricolasTendencia'],
			'control' => ['Reportes' , 'Geoposicion' , 'LPersonal'],
            'rules' => [''],
            'moduleSystem' => 'banano'
		],
		'merma' => [
			'views' => ['merma' , 'mermadia', 'revisionRacimos', 'revisionMerma', 'mermaDedos', 'bonificacion'],
			'control' => ['Merma' , 'DiaMerma'],
            'rules' => [''],
            'moduleSystem' => 'banano'
		],
		'produccion' => [
			'views' => ['produccion', 'produccionReporte', 'produccionsemana', 'produccioncajas', 'produccionenfunde', 'produccionrecobro', 'producciongerencia', 'nuevoEnfunde', 'recobro', 'produccionResumenCorte', 'produccionDemo', 'produccion2', 'produccionRacimos',
						'produccionComparacion', 'produccionRacimosFormularios', 'produccionReporteAcumulado', 'produccionPrecalibracion', 'produccionComparacionAnual',
						'produccionPrecalibracion2', 'produccionComparacionAnalisis', 'nuevoEnfundeSemanal', 'produccioncajasbase', 'produccionRacimosDemo', 'pdfProduccionDia', 'pdfProduccionDiaRender' , 'tableauruforcorp'],
			'control' => ['Produccion' , 'ProduccionWeek'],
            'rules' => [''],
            'moduleSystem' => 'banano'
		],
		'calidad' => [
			'views' => ['calidad' , 'calidadComparativo', 'calidadFotos', 'calidad2', 'calidadComparacion', 'calidadTendencia'],
			'control' => ['Calidad' , 'CalidadMarun'],
            'rules' => [''],
            'moduleSystem' => 'banano'
		],
		'tthh' => [
			'views' => ['tthhPersonal' , 'tthhFPersonal' , 'tthhFPersonalExpediente', 'bonificacion' , 'bonificacionViejo', 'revisionAsistencia', 'agregarAsistencia' , 'asistencia', 'tthhExpediente',
						'tthhEnfunde', 'tthhNuevoEnfunde'],
			'control' => ['Personal' , 'Bonificacion'],
            'rules' => [''],
            'moduleSystem' => 'banano'
		],
		'quality' => [
			'views' => ['quality' , ''],
			'control' => ['Calidad' , ''],
            'rules' => [''],
            'moduleSystem' => 'banano'
		],
		'qualitat' => [
			'views' => ['qualitat' , ''],
			'control' => ['Calidad' , ''],
            'rules' => [''],
            'moduleSystem' => 'banano'
		],
		'membresias' => [
			'views' => ['membresias' , ''],
			'control' => ['' , ''],
            'rules' => [''],
            'moduleSystem' => 'banano'
		],
		'lancofruit' => [
            'views' => ['lancofruit', 'revisionLancofruit', 'perchas', 'perchas2', 'lancofruitTemperatura', 'ventas', 'rastreoLancofruit', 'rastreoLancofruit2','importarInformacion', 'lancofruitVentasDia', 'lancofruitVentasComparativo', 'lancofruitVentasTendencia',
                        'revisionLancofruitOnlyExcel', 'cercaLancofruit', 'configLancofruit', 'perchasDia', 'perchasBD', 'perchasComparacion', 'perchasTendencia'],
			'control' => ['Lancofruit'],
            'rules' => [''],
            'moduleSystem' => 'banano'
		],
		'sigat' => [
			'views' => ['sigat', 'fincas' , 'finca', 'productos'],
			'control' => ['Sigat'],
            'rules' => [''],
            'moduleSystem' => 'banano'
        ],
        'clima' => [
            'views' => ['clima', 'climaDiario'],
            'control' => [''],
            'rules' => [''],
            'moduleSystem' => 'banano'
        ],
        'cacao' => [
			'views' => ['analisisSensorial', 'asignacionMallas', 'formularioAsignacionArea', 'balanzaCosecha', 'cacaoReporteCosecha', 'comparativoCosecha',
						'consolidadoCosecha', 'cacaotthhPersonal', 'cacaotthhFPersonal', 'cacaoReporteCosechaPeriodal', 'cacaoReporteCosechaSector',
						'cosechaLotes', 'cacaoConfigLotes', 'cacaoConfigLotesMapa'],
            'control' => [''],
            'rules' => [''],
			'moduleSystem' => 'cacao'
		],
		'benchmark' => [
			'views' => ['produccionComparacion', 'produccionComparacionAnalisis'],
			'moduleSystem' => 'banano'
		],
		'palma_labores_agricolas' => [
			'views' => ['palmaLaboresAgricolasDia'],
			'moduleSystem' => 'palma'
		],
		'monitor' => [
			'views' => ['monitorProntoforms', 'monitorBalanzas'],
			'moduleSystem' => 'MONITOR'
		]
	]
];
