<?php

class ApiProntoforms { 

    private $origenes = [
        "causas" =>     ["file" => "causas", "formspace" => "formspaces/192686008/sources/164379036"], #revision_instalacion, revision_mantenimiento, instalacion, mantenimiento
        "fincas" =>     ["file" => "fincas", "formspace" => "formspaces/192686008/sources/164378057" ], #cronograma
        "lotes" =>      ["file" => "lotes", "formspace" => "formspaces/192686008/sources/164378059"], #correctivo
        "subfincas" =>  ["file" => "ds_subfincas", "formspace" =>"formspaces/192686008/sources/164378058"], #instalacion
        "personal" =>   ["file" => "personal/labores", "formspace" => "formspaces/192686008/sources/164378059"]
    ];

    private $llave_api = [
        "usuario" => [
            "quintana" => "a2141405003",
            "marcel" => "a2141419002",
            "cacao_marun" => "PFK-9b933b4c-82cc-4b6c-90be-23f2693c2777"
        ],
        "contraseña" => [
            "quintana" => "jTsh6SLr9W4PRaTUv57J7lhV35ieCVy/6yGYz4dh1zyUmCB5ajNckSg1Rhhv3JNd",
            "marcel" => "t+bDxbiT3/Nw/yMrKgagydF4FU5tB7hajEPl6pHtLCTwiGzO4mcCfSVBA2BwbiJ4",
            "cacao_marun" => "qFDMW6j6KjKGTB0rhhrJzlWqIEOUqMno13zMTdRweMrk5Ix38U+E/itQWCALLtjR"
        ]
    ];
     
    private $formularios = [
        "labores" => [
            "origenes" => [
                "causas",
                "fincas",
                "lotes",
                "personal",
                "subfincas",
            ]
        ],
        "merma" => [
            "origenes" => [
                "fincas",
            ]
        ],
        "personal" => [
            "quintana" => "192507002/sources/164642057",
            "marcel" => "192908002/sources/164684009"
        ],
        "labores_asistencia" => [
            "quintana" => "192507002/sources/164646038"
        ],
        "rutas_lancofruit" => [
            "marcel" => "192822002/sources/164752066"
        ],
        "vendedores_lancofruit" => [
            "marcel" => "192822002/sources/164750032"
        ],
    ];

    public function actualizarLancofruit($agent = "marcel"){
        $this->actualizar($agent, "rutas_lancofruit", $this->formularios["rutas_lancofruit"][$agent]);
        $this->actualizar($agent, "vendedores_lancofruit", $this->formularios["vendedores_lancofruit"][$agent]);
        $this->actualizar("marcel", "perchasLancofruit", "192822002/sources/165481071");
    }

    public function actualizarCategoriasLancofruit(){
        $this->actualizar("marcel", "lancofruitCategorias", "192822002/sources/165357049");
        $this->actualizar("marcel", "lancofruitSubcategorias", "192822002/sources/165357050");
        $this->actualizar("marcel", "perchasLancofruit", "192822002/sources/165481071");
    }
    
    public function actualizarPersonal($agent = "quintana"){
        $form = "personal";
        $this->actualizar($agent, $form, $this->formularios[$form][$agent]);
    }

    public function actualizarLaboresAsistencia($agent = "quintana"){
        $form = "labores_asistencia";
        $this->actualizar($agent, $form, $this->formularios[$form][$agent]);
    }

    public function execute(){
        $function = getString('func' , '');
        $this->{$function}();
    }

    public function actualizar($agent, $file, $form){
        $path_file = "/home/procesosiq/public_html/agroaudit/phrapi/controllers/{$file}.json";
        $content = file_get_contents("http://app.procesos-iq.com/phrapi/data/{$file}?agent={$agent}");
        file_put_contents($path_file, $content);

        #ejecutar curl
        $url_upload = "https://api.prontoforms.com/api/[version]/formspaces/{$form}/upload.json";
        $url_upload = str_replace("[version]", "1", $url_upload);

        $user = $this->llave_api["usuario"][$agent];
        $pass = $this->llave_api["contraseña"][$agent];
        
        $cmd = 'curl -v -k -u user:password -X PUT --upload-file '.$path_file.' '.$url_upload;
        $cmd = str_replace("user", $user, $cmd);
        $cmd = str_replace("password", $pass, $cmd);
        exec($cmd, $output);
        unlink($path_file);
    }
    

    /*
    * DOLE
    */
    public function refreshAll(){ ## DOLEEE.... no mover
        foreach($this->formularios["labores"]["origenes"] as $origin){
            #generar archivo
            $path_file = "/home/procesosiq/public_html/agroaudit/phrapi/controllers/".$this->origenes[$origin]["file"].".json";
            $content = file_get_contents("http://app.procesosiq.com/phrapi/data/".$this->origenes[$origin]["file"]."?agent=dole");
            file_put_contents($path_file, $content);

            #ejecutar curl
            $url_upload = "https://api.prontoforms.com/api/[version]/".$this->origenes[$origin]["formspace"]."/upload.json";
            $url_upload = str_replace("[version]", "1", $url_upload);

            $user = "a2141327007";
            $pass = "vHyOpU3sZNFztVYGKMTDkQJU0RvXHE7u3GsN79HA7EXKPCCec2dVjf7bwReORzWY";
            
            $cmd = 'curl -v -k -u user:password -X PUT --upload-file '.$path_file.' '.$url_upload;
            $cmd = str_replace("user", $user, $cmd);
            $cmd = str_replace("password", $pass, $cmd);
            exec($cmd);
            unlink($path_file);
        }
        echo true;
    }
}
?>