<?php defined('PHRAPI') or die("Direct access not allowed!");

class Asistencia {
	public $name;
	private $db;
	private $agent_user;
    private $session;

	public function __construct(){
        $this->session = Session::getInstance();
        $this->db = DB::getInstance($this->session->agent_user);
		// D($this->db);
	}

	public function index(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $filters = (object)[
            "id" => getValueFrom($postdata , "id", 0, FILTER_SANITIZE_INT),
        ];
        $response = new stdClass;

        if($filters->id > 0){
            $labores = $this->db->queryAll("SELECT id, nombre FROM cat_labores");
            $user = $this->db->queryRow("SELECT * 
                                            FROM asistencia 
                                            INNER JOIN asistencia_detalle ON id_asistencia = asistencia_detalle.id 
                                            WHERE asistencia.id = {$filters->id}");
            foreach($labores as $key => $value){
                $options_labores = explode("|", $user->labor_realizar);

                if(in_array(trim($value->nombre), $options_labores)){
                    $value->selected = 1;
                }else{
                    $value->selected = 0;
                }
            }
            $response->user_data = $user;
        }else{
            $labores = $this->db->queryAll("SELECT id, nombre, 0 as selected FROM cat_labores");
        }

        $response->labores = $labores;
        return $response;
    }

    public function save(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $data = (object)[
            "id" => getValueFrom($postdata , "id", 0, FILTER_SANITIZE_INT),
            "hora" => getValueFrom($postdata , "hora", "", FILTER_SANITIZE_STRING),
            "fecha" => getValueFrom($postdata , "fecha", "", FILTER_SANITIZE_STRING),
            "hacienda" => getValueFrom($postdata , "hacienda", "", FILTER_SANITIZE_STRING),
            "responsable" => getValueFrom($postdata , "responsable", "", FILTER_SANITIZE_STRING),
            "trabajador" => getValueFrom($postdata , "trabajador", "", FILTER_SANITIZE_STRING),
            "cedula" => getValueFrom($postdata , "cedula", "", FILTER_SANITIZE_STRING),
            "labores" => getValueFrom($postdata , "labores", [], FILTER_SANITIZE_PHRAPI_ARRAY),
            "lote" => getValueFrom($postdata , "lote", "", FILTER_SANITIZE_STRING),
            "cable" => getValueFrom($postdata , "cable", [], FILTER_SANITIZE_PHRAPI_ARRAY),
            "almuerzo" => getValueFrom($postdata , "almuerzo", 0, FILTER_SANITIZE_INT),
            "nombre_trabajador_nuevo" => getValueFrom($postdata , "nombre_trabajador_nuevo", "", FILTER_SANITIZE_STRING),
        ];

        if($data->id > 0){
            $sql = "UPDATE asistencia SET hora = '{$data->hora}', fecha = '{$data->fecha}', hacienda = '{$data->hacienda}', responsable = '{$data->responsable}' WHERE id = '{$data->id}'";
            #$this->db->query($sql);

            $sql = "UPDATE asistencia_detalle SET trabajador = '{$data->trabajador}', hora = '{$data->hora}', cedula = '{$data->cedula}', labor_realizar = '".implode("|", $data->labores)."', lote = '{$data->lote}', cable = '".implode("|", $data->cable)."', almuerzo = '{$data->almuerzo}', nombre_trabajador_nuevo = '{$data->nombre_trabajador_nuevo}' WHERE id_asistencia = '{$data->id}'";
            #$this->db->query($sql);
        }else{
            $labores = "";
            foreach($data->labores as $key => $value){
                $labores .= ($this->db->queryRow("SELECT nombre FROM cat_labores WHERE id  = '{$value}'")->nombre."|");
            }
            $labores = substr($labores, 0, -1);

            $sql = "INSERT INTO asistencia SET hora = '{$data->hora}', fecha = '{$data->fecha}', hacienda = '{$data->hacienda}', responsable = '{$data->responsable}'";
            $this->db->query($sql);
            $id_asistencia = $this->db->getLastID();

            $sql = "INSERT INTO asistencia_detalle SET id_asistencia = '{$id_asistencia}', trabajador = '{$data->trabajador}', hora = '{$data->hora}', cedula = '{$data->cedula}', labor_realizar = '{$labores}', lote = '{$data->lote}', cable = '".implode("|", $data->cable)."', almuerzo = '{$data->almuerzo}', nombre_trabajador_nuevo = '{$data->nombre_trabajador_nuevo}'";
            $this->db->query($sql);
        }
        return true;
    }
}