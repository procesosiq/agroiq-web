<?php defined('PHRAPI') or die("Direct access not allowed!");

class Configuracion {
    private $db;
    private $config;
    private $token;
    private $Service;

    public function __construct(){
        $this->config = $GLOBALS['config'];
        $this->session = Session::getInstance();
        $this->db = DB::getInstance($this->session->agent_user);
    }

    public function listMarcas(){
        $response = new stdClass;
        $response->data = [];
        $response->recordsTotal = 0;

        $sWhere = "";
        $sOrder = " ORDER BY brands.id";
        $DesAsc = "ASC";
        $sOrder .= " {$DesAsc}";
        $sLimit = "";
        $sHaving = "";

        if(isset($_POST)){
            /*----------  ORDER BY ----------*/
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 1){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY brands.id {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 2){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY clients.nombre {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 3){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY exportadores.nombre {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 4){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY brands.nombre {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 5){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY cajas.nombre {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 6){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY brands.status {$DesAsc}";
            }

            /*----------  WHERE  ----------*/            
            if(isset($_POST['id']) && trim($_POST['id']) != ""){
                $sWhere .= " AND brands.id = '".$_POST["id"]."'";
            }   
            if(isset($_POST['cliente']) && trim($_POST['cliente']) != ""){
                $sWhere .= " AND clients.nombre like '%".$_POST["cliente"]."%'";
            }   
            if(isset($_POST['exportador']) && trim($_POST['exportador']) != ""){
                $sWhere .= " AND exportadores.nombre LIKE '%".$_POST["exportador"]."%'";
            }    
            if(isset($_POST['marca']) && trim($_POST['marca']) != ""){
                $sWhere .= " AND brands.nombre LIKE '%".$_POST["marca"]."%'";
            } 
            if(isset($_POST['tipo_caja']) && trim($_POST['tipo_caja']) != ""){
                $sWhere .= " AND cajas.nombre LIKE '%".$_POST["tipo_caja"]."%'";
            } 
            if(isset($_POST['status']) && trim($_POST['status']) != ""){
                $sWhere .= " AND brands.status LIKE '%".$_POST["status"]."%'";
            } 

            /*----------  LIMIT  ----------*/
            if(isset($_POST['length']) && $_POST['length'] > 0){
                $sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];
            }
        }

        $sql = "SELECT brands.id, clients.nombre as cliente, exportadores.nombre as exportador, brands.status, brands.nombre as marca, cajas.nombre as tipo_caja,
                    (SELECT COUNT(*) FROM brands INNER JOIN clients ON id_client = clients.id
                    INNER JOIN exportadores ON id_exportador = exportadores.id
                    INNER JOIN cajas ON id_marca = brands.id WHERE brands.status != 'Archivado' $sWhere) as totalRows 
                FROM brands
                INNER JOIN clients ON id_client = clients.id
                INNER JOIN exportadores ON id_exportador = exportadores.id
                INNER JOIN cajas ON id_marca = brands.id
                WHERE brands.status != 'Archivado' $sWhere $sOrder $sLimit";
        $data = $this->db->queryAll($sql);

        foreach($data as $key => $value){
            $lblStatus = [
                "Inactivo" => 
                    '<button id="status" data-title="Marcas" data-id='.$value->id.' class="form-control btn btn-sm yellow" action="phrapi/configuracion/marcas/changeStatus">Inactivo</button>',
                "Activo" => 
                    '<button id="status" data-title="Marcas" data-id='.$value->id.' class="form-control btn btn-sm green-jungle" action="phrapi/configuracion/marcas/changeStatus">Activo</button>'
            ];
            $response->data[] = [
                $value->id,
                $value->id,
                $value->exportador,
                $value->cliente,
                $value->marca,
                $value->tipo_caja,
                $lblStatus[$value->status],
                '<button id="edit" data-title="Marcas" class="form-control btn btn-sm blue-steel" action="editMarca?id='.$value->id.'">Editar</button><button id="archive" data-title="Marcas" data-id="'.$value->id.'" class="form-control red-thunderbird btn btn-sm" action="phrapi/configuracion/marcas/archive">Eliminar</button>'
            ];
            $response->recordsTotal = $value->totalRows;
        }

        $response->recordsFiltered = count($response->data);
        $response->customActionStatus = "OK";
        return $response;
    }

    public function changeStatusMarca(){
        return $this->db->query("UPDATE brands SET status = IF(status = 'Activo', 'Inactivo', 'Activo') WHERE id = '{$_POST["_id"]}'");
    }

    public function archiveMarca(){
        return $this->db->query("UPDATE brands SET status = 'Archivado' WHERE id = '{$_POST["_id"]}'");
    }

    public function saveMarca(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        D($postdata);
        if(isset($postdata->id) && $postdata->id > 0){
            $sql = "UPDATE brands SET id_client = '{$postdata->cliente}', id_exportador = '{$postdata->exportador}', nombre = '{$postdata->marca}' WHERE id = '{$postdata->id}'";
            $this->db->query($sql);
            $this->db->query("UPDATE cajas SET nombre = '{$postdata->tipo_caja}' WHERE id_marca = '{$postdata->id}'");
        }else{
            $sql = "INSERT INTO brands SET id_client = '{$postdata->cliente}', id_exportador = '{$postdata->exportador}', nombre = '{$postdata->marca}'";
            $this->db->query($sql);
            $id = $this->db->getLastID();
            $this->db->query("INSERT INTO cajas SET id_marca = '{$id}', nombre = '{$postdata->tipo_caja}'");
        }
        return true;
    }

    public function indexEditMarca(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;

        #D($postdata);
        $response->clientes = $this->db->queryAllSpecial("SELECT id, nombre as label FROM clients WHERE status = 'Activo'");
        if(isset($postdata->cliente) && (int)$postdata->cliente > 0){
            $id_client = $postdata->cliente;
        }else{
            if(isset($postdata->id) && $postdata->id > 0){
                $response->marca = $this->db->queryRow("SELECT brands.id, id_client, id_exportador, brands.nombre, cajas.nombre as tipo_caja
                                                        FROM brands 
                                                        INNER JOIN cajas ON id_marca = brands.id
                                                        WHERE brands.id = '{$postdata->id}'");
                $id_client = $response->marca->id_client;
            }else{
                $id_client = 0;
                foreach($response->clientes as $key => $value){
                    $id_client = $key;
                    break;
                }
            }
        }
        $response->id_cliente = $id_client;
        $response->exportadores = $this->db->queryAllSpecial("SELECT id, nombre as label FROM exportadores WHERE status = 'Activo' 
                AND id_cliente = '{$id_client}'");
        return $response;
    }

    public function listCategorias(){
        $response = new stdClass;
        $response->data = [];
        $response->recordsTotal = 0;

        $sWhere = "";
        $sOrder = " ORDER BY id";
        $DesAsc = "ASC";
        $sOrder .= " {$DesAsc}";
        $sLimit = "";
        $sHaving = "";

        if(isset($_POST)){
            /*----------  ORDER BY ----------*/
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 1){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY id {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 2){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY nombre {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 3){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY status {$DesAsc}";
            }

            /*----------  WHERE  ----------*/            
            if(isset($_POST['id']) && trim($_POST['id']) != ""){
                $sWhere .= " AND id = '".$_POST["id"]."'";
            }   
            if(isset($_POST['nombre']) && trim($_POST['nombre']) != ""){
                $sWhere .= " AND nombre like '%".$_POST["nombre"]."%'";
            }
            if(isset($_POST['status']) && trim($_POST['status']) != ""){
                $sWhere .= " AND status LIKE '%".$_POST["status"]."%'";
            } 

            /*----------  LIMIT  ----------*/
            if(isset($_POST['length']) && $_POST['length'] > 0){
                $sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];
            }
        }

        $sql = "SELECT *, (SELECT COUNT(*) FROM calidad_categorias WHERE status >= 0 $sWhere) as totalRows 
                FROM calidad_categorias
                WHERE status >= 0 $sWhere $sOrder $sLimit";
        $data = $this->db->queryAll($sql);

        foreach($data as $key => $value){
            $lblStatus = [
                    '<button id="status" data-title="Categorias" data-id='.$value->id.' class="form-control btn btn-sm yellow" action="phrapi/configuracion/categorias/changeStatus">Inactivo</button>',
                    '<button id="status" data-title="Categorias" data-id='.$value->id.' class="form-control btn btn-sm green-jungle" action="phrapi/configuracion/categorias/changeStatus">Activo</button>'
            ];
            $response->data[] = [
                $value->id,
                $value->id,
                $value->nombre,
                $lblStatus[$value->status],
                '<button id="edit" data-title="Categorias" class="form-control btn btn-sm blue-steel" action="editCategoria?id='.$value->id.'">Editar</button><button id="archive" data-title="Categorias" data-id="'.$value->id.'" class="form-control red-thunderbird btn btn-sm" action="phrapi/configuracion/categorias/archive">Eliminar</button>'
            ];
            $response->recordsTotal = $value->totalRows;
        }

        $response->recordsFiltered = count($response->data);
        $response->customActionStatus = "OK";
        return $response;
    }

    public function changeStatusCategoria(){
        return $this->db->query("UPDATE calidad_categorias SET status = IF(status = '1', '0', '1') WHERE id = '{$_POST["_id"]}'");
    }

    public function archiveCategoria(){
        return $this->db->query("UPDATE calidad_categorias SET status = '-1' WHERE id = '{$_POST["_id"]}'");
    }

    public function saveCategoria(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        D($postdata);
        if(isset($postdata->id) && $postdata->id > 0){
            $sql = "UPDATE calidad_categorias SET nombre = '{$postdata->nombre}' WHERE id = '{$postdata->id}'";
            $this->db->query($sql);
        }else{
            $sql = "INSERT INTO calidad_categorias SET nombre = '{$postdata->nombre}'";
            $this->db->query($sql);
        }
        return true;
    }

    public function indexEditCategoria(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;

        if(isset($postdata->id) && $postdata->id > 0){
            $response->categoria = $this->db->queryRow("SELECT * FROM calidad_categorias WHERE id = '{$postdata->id}'");
        }

        return $response;
    }

    public function listDefectos(){
        $response = new stdClass;
        $response->data = [];
        $response->recordsTotal = 0;

        $sWhere = "";
        $sOrder = " ORDER BY calidad_defectos.id";
        $DesAsc = "ASC";
        $sOrder .= " {$DesAsc}";
        $sLimit = "";
        $sHaving = "";

        if(isset($_POST)){
            /*----------  ORDER BY ----------*/
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 1){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY calidad_defectos.id {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 2){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY calidad_categorias.nombre {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 3){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY calidad_defectos.nombre {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 4){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY calidad_defectos.valor {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 5){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY calidad_defectos.siglas {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 6){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY calidad_defectos.status {$DesAsc}";
            }

            /*----------  WHERE  ----------*/            
            if(isset($_POST['id']) && trim($_POST['id']) != ""){
                $sWhere .= " AND calidad_defectos.id = '".$_POST["id"]."'";
            }   
            if(isset($_POST['categoria']) && trim($_POST['categoria']) != ""){
                $sWhere .= " AND calidad_categorias.nombre like '%".$_POST["categoria"]."%'";
            }
            if(isset($_POST['defecto']) && trim($_POST['defecto']) != ""){
                $sWhere .= " AND calidad_defectos.nombre like '%".$_POST["defecto"]."%'";
            }
            if(isset($_POST['valor']) && trim($_POST['valor']) != ""){
                $sWhere .= " AND valor like '%".$_POST["valor"]."%'";
            }
            if(isset($_POST['siglas']) && trim($_POST['siglas']) != ""){
                $sWhere .= " AND siglas like '%".$_POST["siglas"]."%'";
            }
            if(isset($_POST['status']) && trim($_POST['status']) != ""){
                $sWhere .= " AND calidad_defectos.status LIKE '%".$_POST["status"]."%'";
            } 

            /*----------  LIMIT  ----------*/
            if(isset($_POST['length']) && $_POST['length'] > 0){
                $sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];
            }
        }

        $sql = "SELECT calidad_defectos.*, calidad_categorias.nombre as categoria, (SELECT COUNT(*) FROM calidad_defectos INNER JOIN calidad_categorias ON id_categoria = calidad_categorias.id WHERE calidad_defectos.status >= 0 $sWhere) as totalRows 
                FROM calidad_defectos
                INNER JOIN calidad_categorias ON id_categoria = calidad_categorias.id
                WHERE calidad_defectos.status >= 0 $sWhere $sOrder $sLimit";
        $data = $this->db->queryAll($sql);

        foreach($data as $key => $value){
            $lblStatus = [
                    '<button id="status" data-title="Defectos" data-id='.$value->id.' class="form-control btn btn-sm yellow" action="phrapi/configuracion/defectos/changeStatus">Inactivo</button>',
                    '<button id="status" data-title="Defectos" data-id='.$value->id.' class="form-control btn btn-sm green-jungle" action="phrapi/configuracion/defectos/changeStatus">Activo</button>'
            ];
            $response->data[] = [
                $value->id,
                $value->id,
                $value->categoria,
                $value->nombre,
                $value->valor,
                $value->siglas,
                $lblStatus[$value->status],
                '<button id="edit" data-title="Defectos" class="form-control btn btn-sm blue-steel" action="editDefecto?id='.$value->id.'">Editar</button><button id="archive" data-title="Defectos" data-id="'.$value->id.'" class="form-control red-thunderbird btn btn-sm" action="phrapi/configuracion/defectos/archive">Eliminar</button>'
            ];
            $response->recordsTotal = $value->totalRows;
        }

        $response->recordsFiltered = count($response->data);
        $response->customActionStatus = "OK";
        return $response;
    }

    public function changeStatusDefecto(){
        return $this->db->query("UPDATE calidad_defectos SET status = IF(status = '1', '0', '1') WHERE id = '{$_POST["_id"]}'");
    }

    public function archiveDefecto(){
        return $this->db->query("UPDATE calidad_defectos SET status = '-1' WHERE id = '{$_POST["_id"]}'");
    }

    public function saveDefecto(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        // D($postdata);
        if(isset($postdata->id) && $postdata->id > 0){
            $sql = "UPDATE calidad_defectos SET nombre = '{$postdata->nombre}', id_categoria = '{$postdata->id_categoria}', valor = '{$postdata->valor}', siglas = '{$postdata->siglas}' WHERE id = '{$postdata->id}'";
            $this->db->query($sql);
        }else{
            $sql = "INSERT INTO calidad_defectos SET nombre = '{$postdata->nombre}', id_categoria = '{$postdata->id_categoria}', valor = '{$postdata->valor}', siglas = '{$postdata->siglas}'";
            $this->db->query($sql);
        }
        return true;
    }

    public function indexEditDefecto(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;

        if(isset($postdata->id) && $postdata->id > 0){
            $response->defecto = $this->db->queryRow("SELECT * FROM calidad_defectos WHERE id = '{$postdata->id}'");
        }
        $response->categorias = $this->db->queryAllSpecial("SELECT id, nombre as label FROM calidad_categorias WHERE status > 0");

        return $response;
    }

    public function listExportadores(){
        $response = new stdClass;
        $response->data = [];
        $response->recordsTotal = 0;

        $sWhere = "";
        $sOrder = " ORDER BY exportadores.id";
        $DesAsc = "ASC";
        $sOrder .= " {$DesAsc}";
        $sLimit = "";
        $sHaving = "";

        if(isset($_POST)){
            /*----------  ORDER BY ----------*/
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 1){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY exportadores.id {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 2){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY clients.nombre {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 3){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY exportadores.nombre {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 4){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY calidad_defectos.status {$DesAsc}";
            }

            /*----------  WHERE  ----------*/            
            if(isset($_POST['id']) && trim($_POST['id']) != ""){
                $sWhere .= " AND exportadores.id = '".$_POST["id"]."'";
            }   
            if(isset($_POST['exportador']) && trim($_POST['exportador']) != ""){
                $sWhere .= " AND exportadores.nombre like '%".$_POST["exportador"]."%'";
            }
            if(isset($_POST['cliente']) && trim($_POST['cliente']) != ""){
                $sWhere .= " AND clients.nombre like '%".$_POST["cliente"]."%'";
            }
            if(isset($_POST['status']) && trim($_POST['status']) != ""){
                $sWhere .= " AND exportadores.status LIKE '%".$_POST["status"]."%'";
            } 

            /*----------  LIMIT  ----------*/
            if(isset($_POST['length']) && $_POST['length'] > 0){
                $sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];
            }
        }

        $sql = "SELECT 
                exportadores.id,
                id_cliente , 
                id_exportador ,
                clients.nombre AS cliente ,
                exportadores.nombre AS nombre,
                exportadores.status
                FROM 
                exportadores_clientes
                INNER JOIN exportadores ON id_exportador = exportadores.id
                INNER JOIN clients ON id_cliente = clients.id
                WHERE exportadores.status >= 0 $sWhere $sOrder $sLimit";
        $data = $this->db->queryAll($sql);

        foreach($data as $key => $value){
            $lblStatus = [
                    "Inactivo" => '<button id="status" data-title="Exportadores" data-id='.$value->id.' class="form-control btn btn-sm yellow" action="phrapi/configuracion/exportadores/changeStatus">Inactivo</button>',
                    "Activo" => '<button id="status" data-title="Exportadores" data-id='.$value->id.' class="form-control btn btn-sm green-jungle" action="phrapi/configuracion/exportadores/changeStatus">Activo</button>'
            ];
            $response->data[] = [
                $value->id,
                $value->id,
                $value->cliente,
                $value->nombre,
                $lblStatus[$value->status],
                '<button id="edit" data-title="Exportadores" class="form-control btn btn-sm blue-steel" action="editExportador?id='.$value->id.'">Editar</button><button id="archive" data-title="Exportadores" data-id="'.$value->id.'" class="form-control red-thunderbird btn btn-sm" action="phrapi/configuracion/exportadores/archive">Eliminar</button>'
            ];
            $response->recordsTotal = $value->totalRows;
        }

        $response->recordsFiltered = count($response->data);
        $response->customActionStatus = "OK";
        return $response;
    }

    public function changeStatusExportador(){
        return $this->db->query("UPDATE exportadores SET status = IF(status = 'Activo', 'Inactivo', 'Activo') WHERE id = '{$_POST["_id"]}'");
    }

    public function archiveExportador(){
        return $this->db->query("UPDATE exportadores SET status = 'Archivado' WHERE id = '{$_POST["_id"]}'");
    }

    public function saveExportador(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        D($postdata);
        if(isset($postdata->id) && $postdata->id > 0){
            $sql = "UPDATE exportadores SET nombre = '{$postdata->exportador}', id_cliente = '{$postdata->cliente}' WHERE id = '{$postdata->id}'";
            $this->db->query($sql);
        }else{
            $sql = "INSERT INTO exportadores SET nombre = '{$postdata->exportador}', id_cliente = '{$postdata->cliente}'";
            $this->db->query($sql);
        }
        return true;
    }

    public function indexEditExportador(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;

        if(isset($postdata->id) && $postdata->id > 0){
            $response->exportador = $this->db->queryRow("SELECT * FROM exportadores WHERE id = '{$postdata->id}'");
        }
        $response->clientes = $this->db->queryAllSpecial("SELECT id, nombre as label FROM clients WHERE status = 'Activo'");

        return $response;
    }

    public function listDestinos(){
        $response = new stdClass;
        $response->data = [];
        $response->recordsTotal = 0;

        $sWhere = "";
        $sOrder = " ORDER BY id";
        $DesAsc = "ASC";
        $sOrder .= " {$DesAsc}";
        $sLimit = "";
        $sHaving = "";

        if(isset($_POST)){
            /*----------  ORDER BY ----------*/
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 1){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY id {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 2){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY nombre {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 3){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY status {$DesAsc}";
            }

            /*----------  WHERE  ----------*/            
            if(isset($_POST['id']) && trim($_POST['id']) != ""){
                $sWhere .= " AND id = '".$_POST["id"]."'";
            }   
            if(isset($_POST['destino']) && trim($_POST['destino']) != ""){
                $sWhere .= " AND nombre like '%".$_POST["destino"]."%'";
            }
            if(isset($_POST['status']) && trim($_POST['status']) != ""){
                $sWhere .= " AND status LIKE '%".$_POST["status"]."%'";
            } 

            /*----------  LIMIT  ----------*/
            if(isset($_POST['length']) && $_POST['length'] > 0){
                $sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];
            }
        }

        $sql = "SELECT destinos.*, (SELECT COUNT(*) FROM destinos WHERE status >= 0 $sWhere) 
                FROM destinos
                WHERE status >= 0 $sWhere $sOrder $sLimit";
        $data = $this->db->queryAll($sql);

        foreach($data as $key => $value){
            $lblStatus = [
                '<button id="status" data-title="Destinos" data-id='.$value->id.' class="form-control btn btn-sm yellow" action="phrapi/configuracion/destinos/changeStatus">Inactivo</button>',
                '<button id="status" data-title="Destinos" data-id='.$value->id.' class="form-control btn btn-sm green-jungle" action="phrapi/configuracion/destinos/changeStatus">Activo</button>'
            ];
            $response->data[] = [
                $value->id,
                $value->id,
                $value->nombre,
                $lblStatus[$value->status],
                '<button id="edit" data-title="Destinos" class="form-control btn btn-sm blue-steel" action="editExportador?id='.$value->id.'">Editar</button><button id="archive" data-title="Destinos" data-id="'.$value->id.'" class="form-control red-thunderbird btn btn-sm" action="phrapi/configuracion/destinos/archive">Eliminar</button>'
            ];
            $response->recordsTotal = $value->totalRows;
        }

        $response->recordsFiltered = count($response->data);
        $response->customActionStatus = "OK";
        return $response;
    }

    public function changeStatusDestino(){
        return $this->db->query("UPDATE destinos SET status = IF(status = '1', '0', '1') WHERE id = '{$_POST["_id"]}'");
    }

    public function archiveDestino(){
        return $this->db->query("UPDATE destinos SET status = '-1' WHERE id = '{$_POST["_id"]}'");
    }

    public function saveDestino(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        D($postdata);
        if(isset($postdata->id) && $postdata->id > 0){
            $sql = "UPDATE destinos SET nombre = '{$postdata->destinos}' WHERE id = '{$postdata->id}'";
            $this->db->query($sql);
        }else{
            $sql = "INSERT INTO destinos SET nombre = '{$postdata->destinos}'";
            $this->db->query($sql);
        }
        return true;
    }

    public function indexEditDestino(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;

        if(isset($postdata->id) && $postdata->id > 0){
            $response->destino = $this->db->queryRow("SELECT * FROM destinos WHERE id = '{$postdata->id}'");
        }

        return $response;
    }
    
}