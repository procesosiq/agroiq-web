<?php defined('PHRAPI') or die("Direct access not allowed!");

class Fincas {
	public $name;
	private $db;

	public function __construct(){
		$this->db = DB::getInstance();
		// D($this->db);
	}

	public function index(){

		$sWhere = "";
        $sOrder = " ORDER BY nombre";
        $DesAsc = "ASC";
        $sOrder .= " {$DesAsc}";
        $sLimit = "";
        // print_r($_POST);
        if(isset($_POST)){

                /*----------  ORDER BY ----------*/
                
                if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 1){
                    $DesAsc = $_POST['order'][0]['dir'];
                    $sOrder = " ORDER BY fincas.id {$DesAsc}";
                }
                if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 2){
                    $DesAsc = $_POST['order'][0]['dir'];
                    $sOrder = " ORDER BY fincas.id {$DesAsc}";
                }
                if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 3){
                    $DesAsc = $_POST['order'][0]['dir'];
                    $sOrder = " ORDER BY fincas.nombre {$DesAsc}";
                }
                if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 4){
                    $DesAsc = $_POST['order'][0]['dir'];
                    $sOrder = " ORDER BY jefeSector {$DesAsc}";
                }
                if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 5){
                    $DesAsc = $_POST['order'][0]['dir'];
                    $sOrder = " ORDER BY (SELECT COUNT(*) FROM cat_sucursales WHERE id_cliente=fincas.id) {$DesAsc}";
                }
                if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 6){
                    $DesAsc = $_POST['order'][0]['dir'];
                    $sOrder = " ORDER BY COUNT(cat_equipos.id) {$DesAsc}";
                }
                if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 7){
                    $DesAsc = $_POST['order'][0]['dir'];
                    $sOrder = " ORDER BY fincas.status {$DesAsc}";
                }
                /*----------  ORDER BY ----------*/

                if(isset($_POST['order_id']) && trim($_POST['order_id']) != ""){
                    $sWhere .= " AND fincas.id = ".$_POST["order_id"];
                }               
                if((isset($_POST['order_date_from']) && trim($_POST['order_date_from']) != "") && (isset($_POST['order_date_to']) && trim($_POST['order_date_to']) != "")){
                    // $sWhere .= " AND fincas.fecha BETWEEN '".$_POST["order_date_from"]."' AND '".$_POST["order_date_to"]."'";
                }
                if(isset($_POST['order_customer_name']) && trim($_POST['order_customer_name']) != ""){
                    $sWhere .= " AND nombre LIKE '%".$_POST['order_customer_name']."%'";
                }
                if(isset($_POST['order_ship_to']) && trim($_POST['order_ship_to']) != ""){
                    $sWhere .= " AND jefeSector  LIKE '%".$_POST['order_ship_to']."%'";
                }
                if(isset($_POST['order_status']) && trim($_POST['order_status']) != ""){
                    // $sWhere .= " AND fincas.status = ".$_POST["order_status"];
                }

                /*----------  LIMIT  ----------*/
                if(isset($_POST['length']) && $_POST['length'] > 0){
                    $sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];
                }
            }

		$fincas = $this->db->queryAll("SELECT * ,(SELECT COUNT(*) FROM fincas) AS totalRows FROM fincas WHERE 1=1 $sWhere $sOrder $sLimit");
        $datos = (object)[];
        $datos->data = [];
        foreach ($fincas as $key => $fila) {
            $fila = (object)$fila;
            $datos->data[] = array (
                $fila->id,
                $fila->id,
                $fila->fecha,
                strtoupper($fila->nombre),
                strtoupper($fila->jefeSector),
                $fila->totSucursal,
                $fila->totEquipos,
                ($fila->idCliente == 'Inactivo') ? '<button id="status" class="btn btn-sm red-thunderbird">'.$fila->idCliente.'</button>' : '<button class="btn btn-sm green-jungle" id="status">'.$fila->idCliente.'</button>',
                '<button id="edit" class="btn btn-sm green btn-outline filter-submit margin-bottom"><i class="fa fa-plus"></i> Editar</button>',
                $botonn
            );

            $datos->recordsTotal = $fila->totalRows;
        }

        $datos->recordsFiltered = count($datos->data);
        #$datos->customActionMessage = "Informacion completada con exito";
        $datos->customActionStatus = "OK";

        return $datos;
	}
}