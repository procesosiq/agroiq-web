<?php defined('PHRAPI') or die("Direct access not allowed!");

class General {
	public $name;
	private $db;

	public function __construct(){
        $this->db = DB::getInstance();
		$this->session = Session::getInstance();
		// D($this->db);
	}

	public function index(){
		$response = new stdClass;
		$response->success = 400;
        $sql = "SELECT * FROM datosGenerales WHERE id_usuario = {$this->session->logged}";
        $fincas = $this->db->queryRow($sql);
        if(count($fincas) > 0){
        	$response->success = 200;
        	$response->data = $fincas;
        }
        return $response;
	}
}