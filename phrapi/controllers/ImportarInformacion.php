<?php defined('PHRAPI') or die("Direct access not allowed!");

class ImportarInformacion {
    private $db;
    private $config;
    private $token;
    private $Service;

    public function __construct(){
        $this->config = $GLOBALS['config'];
        $this->session = Session::getInstance();
        $this->db = DB::getInstance($this->session->agent_user);
    }

    function move_xml($nameFile){
        $dirname = PHRAPI_PATH."../excel/xmls/marcel/";
        if(!rename($dirname."xmlNew/".$nameFile, $dirname."completados/$nameFile")){
            echo 'error';
        }else{
           //echo "{$nameFile} movido";
        }
    }

    public function upXmls() {
        $count = (int) $this->db->queryOne("SELECT MAX(id) FROM lancofruit_folios_facturas");
        $count = $count + 1;
        $fileData = json_decode(file_get_contents('php://input'), true)["upload"];
        $fileData = explode(",", $fileData);
        $fileData = base64_decode($fileData[1]);
        file_put_contents(PHRAPI_PATH."../excel/xmls/marcel/xmlNew/FACTURA_".$count.'.xml', $fileData);
        $xml = simplexml_load_file(PHRAPI_PATH."../excel/xmls/marcel/xmlNew/FACTURA_".$count.'.xml');

        foreach($xml as $key => $node){
            $myXML[$key] = [];
            foreach($node as $key2 => $child){
                if($key2 == 'campoAdicional')
                    $myXML[$key][$key2][] = (string) $child; 
                else if(count($child) == 0)
                    $myXML[$key][$key2] = (string) $child; 
                else
                    foreach($child as $key3 => $child2){
                        if(count($child2) == 0)
                            $myXML[$key][$key2][$key3] = (string) $child2;
                        else
                            foreach($child2 as $key4 => $child3){
                                if($key4 != 'impuesto')
                                    $myXML[$key][$key2][$key4] = (string) $child3;
                                else
                                    foreach($child3 as $key5 => $child4){
                                        $myXML[$key][$key2][$key4][$key5] = (string) $child4;
                                    }
                            }
                    }
            }
        }

        $fecha = explode("/", $myXML["infoFactura"]["fechaEmision"]);
        $fecha = "{$fecha[2]}-{$fecha[1]}-{$fecha[0]}";
        $ruta = str_replace("0", "", $myXML["infoTributaria"]["estab"]);

        $id_ruta = 0;
        if("{$myXML["infoTributaria"]["ptoEmi"]}" == '002'){
            $id_ruta = 1;
        }else if("{$myXML["infoTributaria"]["ptoEmi"]}" == '003'){
            $id_ruta = 2;
        }else if("{$myXML["infoTributaria"]["ptoEmi"]}" == '004'){
            $id_ruta = 3;
        }else if("{$myXML["infoTributaria"]["ptoEmi"]}" == '005'){
            $id_ruta = 4;
        }else if("{$myXML["infoTributaria"]["ptoEmi"]}" == '006'){
            $id_ruta = 5;
        }

        $sql = "INSERT INTO lancofruit_ventas 
                SET
                fecha = '{$fecha}',
                anio = YEAR('{$fecha}'),
                mes = MONTH('{$fecha}'),
                semana = getWeek('{$fecha}'),
                id_ruta = {$id_ruta},
                total = '{$myXML["infoFactura"]["importeTotal"]}',
                ambiente = '{$myXML["infoTributaria"]["ambiente"]}',
                tipo_emision = '{$myXML["infoTributaria"]["tipoEmision"]}',
                razon_social = '{$myXML["infoTributaria"]["razonSocial"]}',
                nombre_comercial = '{$myXML["infoTributaria"]["nombreComercial"]}',
                ruc = '{$myXML["infoTributaria"]["ruc"]}',
                clave_acceso = '{$myXML["infoTributaria"]["claveAcceso"]}',
                cod_doc = '{$myXML["infoTributaria"]["codDoc"]}',
                estab = '{$myXML["infoTributaria"]["estab"]}',
                pto_emi = '{$myXML["infoTributaria"]["ptoEmi"]}',
                secuencial = '{$myXML["infoTributaria"]["secuencial"]}',
                dir_matriz = '{$myXML["infoTributaria"]["dirMatriz"]}',
                fecha_emision = '{$fecha}',
                dir_establecimiento = '{$myXML["infoFactura"]["dirEstablecimiento"]}',
                obligado_contabilidad = '{$myXML["infoFactura"]["obligadoContabilidad"]}',
                tipo_identificacion_comprador = '{$myXML["infoFactura"]["tipoIdentificacionComprador"]}',
                razon_social_comprador = '{$myXML["infoFactura"]["razonSocialComprador"]}',
                identificacion_comprador = '{$myXML["infoFactura"]["identificacionComprador"]}',
                total_sin_impuestos = '{$myXML["infoFactura"]["totalSinImpuestos"]}',
                total_descuento = '{$myXML["infoFactura"]["totalDescuento"]}',
                codigo = '{$myXML["infoFactura"]["totalConImpuestos"]["codigo"]}',
                codigo_porcentaje = '{$myXML["infoFactura"]["totalConImpuestos"]["codigoPorcentaje"]}',
                base_imponible = '{$myXML["infoFactura"]["totalConImpuestos"]["baseImponible"]}',
                valor = '{$myXML["infoFactura"]["totalConImpuestos"]["valor"]}',
                propina = '{$myXML["infoFactura"]["propina"]}',
                importe_total =  '{$myXML["infoFactura"]["importeTotal"]}',
                moneda = '{$myXML["infoFactura"]["moneda"]}',
                forma_pago =  '{$myXML["infoFactura"]["pagos"]["formaPago"]}',
                plazo = '{$myXML["infoFactura"]["pagos"]["plazo"]}',
                unidad_tiempo = '{$myXML["infoFactura"]["pagos"]["unidadTiempo"]}',
                codigo_principal = '{$myXML["detalles"]["detalle"]["codigoPrincipal"]}',
                codigo_auxiliar = '{$myXML["detalles"]["detalle"]["codigoAuxiliar"]}',
                descripcion = '{$myXML["detalles"]["detalle"]["descripcion"]}',
                cantidad = '{$myXML["detalles"]["detalle"]["cantidad"]}',
                precio_unitario = '{$myXML["detalles"]["detalle"]["precioUnitario"]}',
                descuento = '{$myXML["detalles"]["detalle"]["descuento"]}',
                precio_total_sin_impuestos = '{$myXML["detalles"]["detalle"]["precioTotalSinImpuesto"]}',
                impuestos_dos = '{$myXML["detalles"]["detalle"]["impuesto"]["codigo"]}',
                codigo_porcentaje_dos = '{$myXML["detalles"]["detalle"]["impuesto"]["codigoPorcentaje"]}',
                tarifa = '{$myXML["detalles"]["detalle"]["impuesto"]["tarifa"]}',
                base_imponible_dos = '{$myXML["detalles"]["detalle"]["impuesto"]["baseImponible"]}',
                valor_dos = '{$myXML["detalles"]["detalle"]["impuesto"]["valor"]}',
                adicional1 = '".addslashes($myXML["infoAdicional"]["campoAdicional"][0])."',
                adicional2 = '".addslashes($myXML["infoAdicional"]["campoAdicional"][1])."',
                adicional3 = '".addslashes($myXML["infoAdicional"]["campoAdicional"][2])."'";
        $this->db->query($sql);
        $id_venta = $this->db->getLastID();
        $this->move_xml('FACTURA_'.$count.'.xml');
        $this->db->query("INSERT INTO lancofruit_folios_facturas SET factura = 'FACTURA_$count.xml', folio = '{$count}', id_ventas = '{$id_venta}'");
    }

}



