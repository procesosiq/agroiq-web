<?php defined('PHRAPI') or die("Direct access not allowed!");

class LPersonal {
    public $name;
    private $db;
    private $config;
    private $token;
    private $Service;

    public function __construct(){
        $this->config = $GLOBALS['config'];
        $this->session = Session::getInstance();
        // D($this->session);
        // $this->token = '289150995ed28e8860f9161d3cc9f259';
        // $this->Service = new ServicesNode('http://procesos-iq.com:3000/' , $this->token);
        $this->maindb = DB::getInstance();
        $this->db = DB::getInstance($this->session->agent_user);
    }

    public function index(){
        $response = new stdClass;
        $response->id_company = $this->session->id_company;
        $response->umbrals = $this->session->umbrals;
        $response->data = $this->getPersonal();
        $response->fincas = $this->db->queryAllSpecial("SELECT id , nombre as label FROM fincas");
        return $response;
    }
    
    /*===============================================
    =            SECCION NIVEL HACIENDA [ ZONAS ]            =
    ===============================================*/
    
    public function getPersonal(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $idfinca = getValueFrom($postdata,'idFinca',1,FILTER_SANITIZE_PHRAPI_INT);
        $fecha_fin = getValueFrom($postdata,'fecha_final','',FILTER_SANITIZE_STRING);
        $fecha_inicio = getValueFrom($postdata,'fecha_inicial','',FILTER_SANITIZE_STRING);

        $response = [];

        $sql = "SELECT WEEK(m.fecha) AS semana , m.idLabor , m.semana AS semanaPronto,  p.idFinca ,p.personal , p.idPersonal , LOWER(tipo_labor) AS  tipo_labor,AVG(promedio) AS promedio
                ,p.lote
                FROM muestras_anual AS m
                INNER JOIN lotelabor_personal AS p ON m.idLote = p.idLote AND m.idTipoLabor = p.idTipoLabor
                WHERE m.idFinca = {$idfinca} AND anio = YEAR(CURRENT_DATE)
                GROUP BY m.semana , p.idPersonal 
                ORDER BY m.semana , p.personal";
                // AND m.fecha BETWEEN '$fecha_inicio' AND '$fecha_fin'
        $data = $this->db->queryAll($sql);
        $personal = "";
        $count = [];
        foreach ($data as $key => $value) {
            $response["semana"][$value->semana] = $value->semana;
            $response[$value->tipo_labor][$value->personal]["lotero"] = $value->personal;
            $response[$value->tipo_labor][$value->personal]["lote"] = $value->lote;
            $response[$value->tipo_labor][$value->personal]["umbral"][$value->semana] = $this->umbrals(ROUND((float)$value->promedio , 2));
            $response[$value->tipo_labor][$value->personal]["campo"][$value->semana] = ROUND((float)$value->promedio , 2);
            $response[$value->tipo_labor][$value->personal]["labores"]  = $this->getLabores($postdata , $value->idPersonal);
            $suma[$value->tipo_labor][$value->personal] += (float)$value->promedio;
            $count[$value->tipo_labor][$value->personal]++;
            $response[$value->tipo_labor][$value->personal]["campo"]["total"] = ROUND(
                    ($suma[$value->tipo_labor][$value->personal] / $count[$value->tipo_labor][$value->personal])
                ,2);
            $response[$value->tipo_labor][$value->personal]["umbral"]["total"] = $this->umbrals(
                ROUND(
                    ($suma[$value->tipo_labor][$value->personal] / $count[$value->tipo_labor][$value->personal])
                ,2)
            );
        }
        $response["semana"]["total"] = "total";
        $response["colspan"] = ROUND((count($response["semana"]) + 4),2);
        $response["withTable"] = ROUND((100 / (count($response["semana"]) + 4)),2);
        
        $totals = $this->db->queryAll("SELECT semana , AVG(promedio) AS promedio FROM
            (SELECT WEEK(m.fecha) AS semana , m.idLabor , m.semana AS semanaPronto,  p.idFinca ,p.personal , tipo_labor ,AVG(promedio) AS promedio,
            p.lote
            FROM muestras_anual AS m
            INNER JOIN lotelabor_personal AS p ON m.idLote = p.idLote AND m.idTipoLabor = p.idTipoLabor
            WHERE m.idFinca = {$idfinca} AND anio = YEAR(CURRENT_DATE)
            GROUP BY m.semana , p.idPersonal 
            ORDER BY m.semana , p.personal) AS l
            GROUP BY semana");
        $count = 0;
        $sum = 0;
        foreach ($totals as $key => $value) {
            $value = (object)$value;
            $response["totales"]["campo"][$value->semana] = ROUND((float)$value->promedio , 2);
            $response["umbral"]["campo"][$value->semana] = $this->umbrals(ROUND((float)$value->promedio , 2));
            $sum += (float)$value->promedio;
            $count++;
        }

        $response["totales"]["campo"]["total"] = ROUND(($sum / $count) , 2);
        $response["umbral"]["campo"]["total"] = $this->umbrals(ROUND(($sum / $count) , 2));
        return $response;
    }

    private function umbrals($value){
        $umbrals = $this->session->umbrals;
        $indicador = new stdClass;
        $indicador->red = "fa fa-arrow-down font-red-thunderbird";
        $indicador->green = "fa fa-arrow-up font-green-jungle";
        $indicador->orange = "fa fa-minus font-yellow-lemon";
        $tag = $indicador->green;
        if($value > $umbrals->green_umbral_1){
            $tag = $indicador->green;
        }
        elseif($value >= $umbrals->orange_umbral_1 && $value <= $umbrals->yellow_umbral_2){
            $tag = $indicador->orange;
        }
        elseif($value <= $umbrals->red_umbral_2){
            $tag = $indicador->red;
        }

        return $tag;
    }

    private function getLabores($postdata , $idPersonal){
        $idfinca = getValueFrom($postdata,'idFinca',1,FILTER_SANITIZE_PHRAPI_INT);
        $fecha_fin = getValueFrom($postdata,'fecha_final','',FILTER_SANITIZE_STRING);
        $fecha_inicio = getValueFrom($postdata,'fecha_inicial','',FILTER_SANITIZE_STRING);
        $response = [];

        $sql = "SELECT WEEK(m.fecha) AS semana , m.idLabor , m.semana AS semanaPronto,  p.idFinca ,p.personal ,
                 LOWER(tipo_labor) AS  tipo_labor,AVG(promedio) AS promedio
                ,p.lote, (SELECT nombre FROM labores WHERE id =  m.idLabor) AS labor
                FROM muestras_anual AS m
                INNER JOIN lotelabor_personal AS p ON m.idLote = p.idLote AND m.idTipoLabor = p.idTipoLabor
                WHERE m.idFinca = {$idfinca} AND p.idPersonal = {$idPersonal}
                GROUP BY m.semana , p.idPersonal ,m.idLabor
                ORDER BY m.semana , p.personal";
                // AND m.fecha BETWEEN '$fecha_inicio' AND '$fecha_fin'
        $data = $this->db->queryAll($sql);
        $personal = "";
        foreach ($data as $key => $value) {
            $response[$value->tipo_labor][$value->labor]["lotero"] = "";
            $response[$value->tipo_labor][$value->labor]["lote"] = "";
            $response[$value->tipo_labor][$value->labor]["labor"] = $value->labor;
            $response[$value->tipo_labor][$value->labor]["umbral"][$value->semana] = $this->umbrals(ROUND((float)$value->promedio , 2));
            $response[$value->tipo_labor][$value->labor]["campo"][$value->semana] = ROUND((float)$value->promedio , 2);
            $response[$value->tipo_labor][$value->labor]["labores"]  = [];
            $suma[$value->tipo_labor][$value->labor] += (float)$value->promedio;
            $count[$value->tipo_labor][$value->labor]++;
            $response[$value->tipo_labor][$value->labor]["campo"]["total"] = ROUND(
                    ($suma[$value->tipo_labor][$value->labor] / $count[$value->tipo_labor][$value->labor])
                ,2);
        }
        $response["semana"]["total"] = "total";

        return $response;
    }
    /*=====  End of SECCION NIVEL HACIENDA [ ZONAS ]  ======*/

}