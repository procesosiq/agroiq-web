<?php defined('PHRAPI') or die("Direct access not allowed!");

class LaboresAgricolasComparativo {
    public $name;
    private $db;
    private $config;

    public function __construct(){
        $this->session = Session::getInstance();
        $this->db = DB::getInstance($this->session->agent_user);
        $this->postdata = (object)json_decode(file_get_contents("php://input"));
        $this->num_plantas_per_json = 10;
        $this->colors = ['#c23531','#2f4554', '#61a0a8', '#d48265', '#91c7ae','#749f83',  '#ca8622', '#bda29a','#6e7074', '#546570', '#c4ccd3'];
    }

    public function last(){
        $response = new stdClass;
        $response->fecha = $this->db->queryOne("SELECT MAX(fecha) FROM muestras");
        return $response;
    }

    public function variable(){
        $response = new stdClass;

        $sWhere = "";
        if($this->postdata->fecha_inicial != ''){
            $sWhere .= " AND j.fecha BETWEEN '{$this->postdata->fecha_inicial}' AND '{$this->postdata->fecha_final}'";
        }
        
        $sql = "SELECT idFinca, fincas.nombre
                FROM muestras_jsons j
                INNER JOIN muestras ON id_muestra_json = j.id
                INNER JOIN fincas ON idFinca = fincas.id
                WHERE 1=1 $sWhere
                GROUP BY idFinca";
        $response->fincas = $this->db->queryAll($sql);

        $sql = "SELECT idLabor, labores.nombre
                FROM muestras_jsons j
                INNER JOIN muestras ON id_muestra_json = j.id
                INNER JOIN labores ON idLabor = labores.id
                WHERE 1=1 $sWhere
                GROUP BY idLabor";
        $response->labores = $this->db->queryAll($sql);

        return $response;
    }

    public function index(){
        $response = new stdClass;
        $response->tabla_por_labor = $this->tablaPorLabor();
        $response->tabla_por_causa = $this->tablaPorCausa();
        return $response;
    }

    public function tablaPorLabor(){
        $response = new stdClass;

        $sWhere = "";
        if($this->postdata->fecha_inicial != ''){
            $sWhere .= " AND j.fecha BETWEEN '{$this->postdata->fecha_inicial}' AND '{$this->postdata->fecha_final}'";
        }

        $sql = "SELECT idLabor, labores.nombre
                FROM muestras_jsons j
                INNER JOIN muestras ON id_muestra_json = j.id
                INNER JOIN labores ON idLabor = labores.id
                WHERE 1=1 $sWhere
                GROUP BY idLabor";
        $response->labores = $this->db->queryAll($sql);

        $sql = "SELECT idFinca, fincas.nombre
                FROM muestras_jsons j
                INNER JOIN muestras ON id_muestra_json = j.id
                INNER JOIN fincas ON idFinca = fincas.id
                WHERE 1=1 $sWhere
                GROUP BY idFinca";
        $response->fincas = $this->db->queryAll($sql);

        $response->tabla_por_labor = [];
        foreach($response->fincas as $finca){
            $fila = [
                "idFinca" => $finca->idFinca,
                "finca" => $finca->nombre
            ];

            $fila["lotes"] = $this->db->queryAll("SELECT idLote, lotes.nombre AS lote, CONCAT('LOTE ', lotes.nombre) AS zona, 'LOTE' as tipo FROM muestras_jsons j INNER JOIN muestras ON id_muestra_json = j.id INNER JOIN lotes ON idLote = lotes.id WHERE  muestras.idFinca = {$finca->idFinca} $sWhere GROUP BY idLote");
            foreach($fila["lotes"] as $lote){
                foreach($response->labores as $labor){
                    $ponderacion_labor = $this->db->queryOne("SELECT SUM(valor) FROM labores_causas WHERE idLabor = $labor->idLabor");
                    $suma_ponderacion = $this->db->queryOne("SELECT SUM(valor)
                        FROM muestras_jsons j
                        INNER JOIN muestras ON id_muestra_json = j.id
                        WHERE idLote = {$lote->idLote} 
                            AND idLabor = {$labor->idLabor} 
                            AND idFinca = {$finca->idFinca}
                            $sWhere
                    ");
                    $lote->{"labor_{$labor->idLabor}"} = round(100 - ($suma_ponderacion / ($ponderacion_labor * $this->num_plantas_per_json) * 100), 2);
                    $lote->valores[] = $lote->{"labor_{$labor->idLabor}"};
                }
            }

            $response->tabla_por_labor[] = $fila;
        }

        return $response;
    }

    public function tablaPorCausa(){
        $response = new stdClass;

        $sWhere = "";
        if($this->postdata->fecha_inicial != ''){
            $sWhere .= " AND j.fecha BETWEEN '{$this->postdata->fecha_inicial}' AND '{$this->postdata->fecha_final}'";
        }
        if(isset($this->postdata->tablaPorCausa->labor) && $this->postdata->tablaPorCausa->labor > 0){
            $sWhere .= " AND muestras.idLabor = {$this->postdata->tablaPorCausa->labor}";
        }

        $sql = "SELECT idLaborCausa, muestras.idLabor, c.causa nombre
                FROM muestras_jsons j
                INNER JOIN muestras ON id_muestra_json = j.id
                INNER JOIN labores_causas c ON idLaborCausa = c.id
                WHERE 1=1 $sWhere
                GROUP BY idLaborCausa";
        $response->causas = $this->db->queryAll($sql);

        $sql = "SELECT idFinca, fincas.nombre
                FROM muestras_jsons j
                INNER JOIN muestras ON id_muestra_json = j.id
                INNER JOIN fincas ON idFinca = fincas.id
                WHERE 1=1 $sWhere
                GROUP BY idFinca";
        $response->fincas = $this->db->queryAll($sql);

        $response->data = [];
        foreach($response->fincas as $finca){
            $fila = [
                "idFinca" => $finca->idFinca,
                "finca" => $finca->nombre
            ];

            $fila["lotes"] = $this->db->queryAll("SELECT idLote, lotes.nombre AS lote, CONCAT('LOTE ', lotes.nombre) AS zona, 'LOTE' as tipo FROM muestras_jsons j INNER JOIN muestras ON id_muestra_json = j.id INNER JOIN lotes ON idLote = lotes.id WHERE  muestras.idFinca = {$finca->idFinca} $sWhere GROUP BY idLote");
            foreach($fila["lotes"] as $lote){
                foreach($response->causas as $causa){
                    $ponderacion_labor = $this->db->queryOne("SELECT SUM(valor) FROM labores_causas WHERE idLabor = {$causa->idLabor}");
                    $suma_ponderacion = $this->db->queryOne("SELECT SUM(valor)
                        FROM muestras_jsons j
                        INNER JOIN muestras ON id_muestra_json = j.id
                        WHERE idLote = {$lote->idLote} 
                            AND idLaborCausa = {$causa->idLaborCausa} 
                            AND idFinca = {$finca->idFinca}
                            $sWhere
                    ");
                    $lote->{"causa_{$causa->idLaborCausa}"} = round(($suma_ponderacion / ($ponderacion_labor * $this->num_plantas_per_json) * 100), 2);
                    $lote->valores[] = $lote->{"causa_{$causa->idLaborCausa}"};
                }
            }

            $response->data[] = $fila;
        }

        return $response;
    }
}