<?php defined('PHRAPI') or die("Direct access not allowed!");

class LaboresAgricolasDia {
    public $name;
    private $db;
    private $config;

    public function __construct(){
        $this->config = $GLOBALS['config'];
        $this->session = Session::getInstance();
        $this->db = DB::getInstance($this->session->agent_user);
        $this->piq = DB::getInstance();
        $this->postdata = (object)json_decode(file_get_contents("php://input"));
        $this->num_plantas_per_json = 10;
        $this->colors = ['#c23531','#2f4554', '#61a0a8', '#d48265', '#91c7ae','#749f83',  '#ca8622', '#bda29a','#6e7074', '#546570', '#c4ccd3'];
    }

    public function last(){
        $response = new stdClass;
        $response->fecha = $this->db->queryOne("SELECT MAX(fecha) FROM muestras");
        $response->days = $this->db->queryAllOne("SELECT fecha FROM muestras_jsons GROUP BY fecha");
        $response->periodos = $this->db->queryAllOne("SELECT periodo FROM muestras_jsons GROUP BY periodo");
        $response->semanas = $this->db->queryAllOne("SELECT semana FROM muestras_jsons GROUP BY semana");
        return $response;
    }

    public function variables(){
        $response = new stdClass;

        $sWhere = "";
        if($this->postdata->fecha_inicial != ''){
            $sWhere .= " AND fecha BETWEEN '{$this->postdata->fecha_inicial}' AND '{$this->postdata->fecha_final}'";
        }

        $response->fincas = $this->db->queryAll("SELECT idFinca as id, fincas.nombre FROM muestras INNER JOIN fincas ON fincas.id = idFinca WHERE 1=1 $sWhere GROUP BY idFinca");
        return $response;
    }

    public function index(){
        $response = new stdClass;
        $response->tablaResumen = $this->tablaResumen();
        $response->pieCausas = $this->pieCausas();
        $response->calidadLabor = $this->calidadLabor();
        $response->calidadLote = $this->calidadLote();
        $response->markers = $this->markers();
        $response->fotos = $this->fotos();
        return $response;
    }

    public function fotos(){
        $response = new stdClass;

        $sWhere = "";
        if($this->postdata->periodo > 0){
            $sWhere .= " AND j.periodo = {$this->postdata->periodo}";
        }
        if($this->postdata->semana > 0){
            $sWhere .= " AND j.semana = {$this->postdata->semana}";
        }
        if($this->postdata->fecha_inicial != ''){
            $sWhere .= " AND j.fecha BETWEEN '{$this->postdata->fecha_inicial}' AND '{$this->postdata->fecha_final}'";
        }
        if(isset($this->postdata->id_finca) && $this->postdata->id_finca > 0){
            $sWhere .= " AND id_finca = {$this->postdata->id_finca}";
        }

        $response->path_s3 = $this->piq->queryOne("SELECT laboresagricolas_form_url_pdf FROM companies WHERE id = {$this->session->id_company}");
        $sql = "SELECT labores.nombre as labor, fotos, lotes.nombre as lote
                FROM muestras_jsons j
                INNER JOIN muestras_imagenes i ON id_muestra_json = j.id
                INNER JOIN labores ON id_labor = labores.id
                INNER JOIN lotes ON id_lote = lotes.id
                where 1=1 {$sWhere}
                GROUP BY lotes.id, labores.id";
        $data = $this->db->queryAll($sql);
        
        $response->data = [];
        foreach($data as $row){
            $_dd = explode("|", $row->fotos);
            $_ddd = [];
            for($x = 0; $x < count($_dd); $x++){
                $_ddd[] = [
                    "labor" => $row->labor,
                    "lote" => $row->lote,
                    "url" => $response->path_s3.$_dd[$x]
                ];
            }
            $response->data = array_merge($response->data, $_ddd);
        }
        return $response;
    }

    public function markers(){
        $response = new stdClass;

        $sWhere = "";
        $sWherePeriodo = "";
        $sWhereLote = "";
        $sWhereLabor = "";
        
        if($this->postdata->periodo > 0){
            $sWhere .= " AND j.periodo = {$this->postdata->periodo}";
        }
        if($this->postdata->semana > 0){
            $sWhere .= " AND j.semana = {$this->postdata->periodo}";
        }
        if($this->postdata->fecha_inicial != ''){
            $sWhere .= " AND j.fecha BETWEEN '{$this->postdata->fecha_inicial}' AND '{$this->postdata->fecha_final}'";
        }
        if(isset($this->postdata->id_finca) && $this->postdata->id_finca > 0){
            $sWhere .= " AND id_finca = {$this->postdata->id_finca}";
        }
        if(isset($this->postdata->markers)){
            if(isset($this->postdata->markers->periodo) && $this->postdata->markers->periodo > 0){
                $sWherePeriodo .= " AND j.periodo = {$this->postdata->markers->periodo}";
            }

            if(isset($this->postdata->markers->lote) && $this->postdata->markers->lote > 0){
                $sWhereLote .= " AND id_lote = {$this->postdata->markers->lote}";
            }

            if(isset($this->postdata->markers->labor) && $this->postdata->markers->labor > 0){
                $sWhereLabor .= " AND id_labor = {$this->postdata->markers->labor}";
            }
        }

        $sql = "SELECT id_labor, lat, lng, labores.nombre as labor, j.id as id_muestra_json, lotes.nombre as lote
                FROM muestras_jsons j
                INNER JOIN muestras_coords c ON id_muestra_json = j.id
                INNER JOIN muestras m ON m.id_muestra_json = j.id
                INNER JOIN labores ON id_labor = labores.id
                INNER JOIN lotes ON id_lote = lotes.id
                WHERE 1=1 $sWhere $sWherePeriodo $sWhereLote $sWhereLabor";
        $response->data = $this->db->queryAll($sql);
        foreach($response->data as $marker){
            $marker->plantas = [];
            for($x = 1; $x <= 10; $x++){
                $limit = "$x, 1";
                if($x ==  1) $limit = 1;
                $marker->plantas[] = 100 - ((float)$this->db->queryOne("SELECT SUM(valor) FROM muestras WHERE id_muestra_json = {$marker->id_muestra_json} ORDER BY id LIMIT $limit"));
            }
            $marker->promedio = round(array_sum($marker->plantas)/count($marker->plantas), 2);
        }

        $sql = "SELECT j.periodo
                FROM muestras_jsons j
                INNER JOIN muestras_coords c ON id_muestra_json = j.id
                INNER JOIN muestras m ON m.id_muestra_json = j.id
                WHERE 1=1 $sWhere $sWhereLote $sWhereLabor
                GROUP BY j.periodo";
        $response->periodos = $this->db->queryAllOne($sql);

        $sql = "SELECT id_lote as idLote, lotes.nombre
                FROM muestras_jsons j
                INNER JOIN muestras_coords c ON id_muestra_json = j.id
                INNER JOIN lotes ON id_lote = lotes.id
                INNER JOIN muestras m ON m.id_muestra_json = j.id
                WHERE 1=1 $sWhere $sWherePeriodo $sWhereLabor
                GROUP BY id_lote";
        $response->lotes = $this->db->queryAll($sql);

        $sql = "SELECT id_labor as idLabor, labores.nombre
                FROM muestras_jsons j
                INNER JOIN muestras_coords c ON id_muestra_json = j.id
                INNER JOIN labores ON id_labor = labores.id
                INNER JOIN muestras m ON m.id_muestra_json = j.id
                WHERE 1=1 $sWhere $sWherePeriodo $sWhereLote
                GROUP BY id_labor";
        $response->labores = $this->db->queryAll($sql);

        return $response;
    }

    public function tablaResumen(){
        $response = new stdClass;

        $sWhere = "";
        if($this->postdata->periodo > 0){
            $sWhere .= " AND periodo = {$this->postdata->periodo}";
        }
        if($this->postdata->semana > 0){
            $sWhere .= " AND semana = {$this->postdata->semana}";
        }
        if($this->postdata->fecha_inicial != ''){
            $sWhere .= " AND fecha BETWEEN '{$this->postdata->fecha_inicial}' AND '{$this->postdata->fecha_final}'";
        }
        if(isset($this->postdata->id_finca) && $this->postdata->id_finca > 0){
            $sWhere .= " AND muestras.idFinca = {$this->postdata->id_finca}";
        }

        $sql = "SELECT idLabor, labores.nombre
                FROM muestras
                INNER JOIN labores ON idLabor = labores.id
                WHERE 1=1 {$sWhere}
                GROUP BY idLabor";
        $response->tabla_labores_defectos = $this->db->queryAll($sql);

        $sql = "SELECT idLote, lotes.nombre as lote
                FROM muestras 
                INNER JOIN lotes ON idLote = lotes.id
                WHERE 1=1 {$sWhere}
                GROUP BY idLote";
        $response->lotes = $this->db->queryAll($sql);

        foreach($response->tabla_labores_defectos as $row){
            $ponderacion_labor = $this->db->queryOne("SELECT SUM(valor) FROM labores_causas WHERE idLabor = {$row->idLabor}");
            $row->values = [];

            $sql = "SELECT idLaborCausa, labores_causas.causa
                    FROM muestras
                    INNER JOIN labores_causas ON idLaborCausa = labores_causas.id
                    WHERE muestras.idLabor = {$row->idLabor}
                        {$sWhere}
                    GROUP BY idLaborCausa";
            $defectos = $this->db->queryAll($sql);
            $row->detalle = [];

            // CALIDAD LABOR
            foreach($response->lotes as $lote){
                $suma_ponderacion = $this->db->queryRow("SELECT COUNT(DISTINCT id_muestra_json) jsons, SUM(valor) valor
                    FROM muestras
                    WHERE idLote = {$lote->idLote} 
                        AND idLabor = {$row->idLabor}
                        {$sWhere}
                ");
                $row->{"lote_{$lote->idLote}"} = round(100 - ($suma_ponderacion->valor / ($ponderacion_labor * ($this->num_plantas_per_json * $suma_ponderacion->jsons)) * 100), 2);
                $row->values[] = $row->{"lote_{$lote->idLote}"};
            }

            // DEFECTOS
            foreach($defectos as $causa){
                $_defecto = [
                    "causa" => $causa->causa
                ];

                foreach($response->lotes as $lote){
                    $suma_ponderacion = $this->db->queryRow("SELECT COUNT(DISTINCT id_muestra_json) jsons, SUM(valor) valor
                        FROM muestras
                        WHERE idLote = {$lote->idLote}
                            AND idLabor = {$row->idLabor} 
                            AND idLaborCausa = '{$causa->idLaborCausa}'
                            {$sWhere}");
                    $_defecto["lote_{$lote->idLote}"] = round($suma_ponderacion->valor / ($ponderacion_labor * ($this->num_plantas_per_json * $suma_ponderacion->jsons)) * 100, 2);
                }

                $row->detalle[] = $_defecto;
            }
        }

        return $response;
    }

    public function pieCausas(){
        $response = new stdClass;

        $sWhere = "";
        $sWhereLabor = "";
        $sWhereLote = "";

        if($this->postdata->periodo > 0){
            $sWhere .= " AND periodo = {$this->postdata->periodo}";
        }
        if($this->postdata->semana > 0){
            $sWhere .= " AND semana = {$this->postdata->semana}";
        }
        if($this->postdata->fecha_inicial != ''){
            $sWhere .= " AND fecha BETWEEN '{$this->postdata->fecha_inicial}' AND '{$this->postdata->fecha_final}'";
        }
        if(isset($this->postdata->id_finca) && $this->postdata->id_finca > 0){
            $sWhere .= " AND muestras.idFinca = {$this->postdata->id_finca}";
        }
        if(isset($this->postdata->pieCausa)){
            if(isset($this->postdata->pieCausa->lote) && $this->postdata->pieCausa->lote > 0){
                $sWhereLote .= " AND idLote = {$this->postdata->pieCausa->lote}";
            }
            if(isset($this->postdata->pieCausa->labor) && $this->postdata->pieCausa->labor > 0){
                $sWhereLabor .= " AND idLabor = {$this->postdata->pieCausa->labor}";
            }
        }

        $sql = "SELECT idLabor, labores.nombre
                FROM muestras
                INNER JOIN labores ON idLabor = labores.id
                WHERE 1=1 $sWhere $sWhereLote
                GROUP BY idLabor";
        $response->labores = $this->db->queryAll($sql);

        $sql = "SELECT idLote, lotes.nombre
                FROM muestras
                INNER JOIN lotes ON idLote = lotes.id
                WHERE 1=1 $sWhere $sWhereLabor
                GROUP BY idLote";
        $response->lotes = $this->db->queryAll($sql);

        $sql = "SELECT laborCausaDesc AS label, SUM(valor) 'value'
                FROM muestras
                WHERE 1=1 $sWhere $sWhereLabor $sWhereLote
                GROUP BY laborCausaDesc";
        $response->data = $this->db->queryAll($sql);

        return $response;
    }

    public function calidadLabor(){
        $response = new stdClass;

        $sWhere = "";
        if($this->postdata->periodo > 0){
            $sWhere .= " AND periodo = {$this->postdata->periodo}";
        }
        if($this->postdata->semana > 0){
            $sWhere .= " AND semana = {$this->postdata->semana}";
        }
        if($this->postdata->fecha_inicial != ''){
            $sWhere .= " AND fecha BETWEEN '{$this->postdata->fecha_inicial}' AND '{$this->postdata->fecha_final}'";
        }
        if(isset($this->postdata->id_finca) && $this->postdata->id_finca > 0){
            $sWhere .= " AND muestras.idFinca = {$this->postdata->id_finca}";
        }

        $sql = "SELECT idLote, lotes.nombre
                FROM muestras
                INNER JOIN lotes ON idLote = lotes.id
                WHERE 1=1 $sWhere
                GROUP BY idLote";
        $response->lotes = $this->db->queryAll($sql);

        if(isset($this->postdata->calidadLabor)){
            if(isset($this->postdata->calidadLabor->lote) && $this->postdata->calidadLabor->lote > 0){
                $e = false;
                foreach($response->lotes as $lote){
                    if($lote->idLote == $this->postdata->calidadLabor->lote){
                        $e = true;
                        break;
                    }
                }
                if(!$e) {
                    $response->selected_lote = $response->lotes[0]->idLote;
                    $this->postdata->calidadLabor = (object)[
                        "lote" => $response->selected_lote
                    ];
                }
            }else{
                $response->selected_lote = $response->lotes[0]->idLote;
                $this->postdata->calidadLabor = (object)[
                    "lote" => $response->selected_lote
                ];
            }
        }else{
            $response->selected_lote = $response->lotes[0]->idLote;
            $this->postdata->calidadLabor = (object)[
                "lote" => $response->selected_lote
            ];
        }

        $sql = "SELECT idLabor, labores.nombre
                FROM muestras
                INNER JOIN labores ON idLabor = labores.id
                WHERE idLote = {$this->postdata->calidadLabor->lote} $sWhere
                GROUP BY idLabor";
        $response->labores = $this->db->queryAll($sql);

        $response->series = [
            [
                "name" => "Cantidad",
                "type" => 'bar',
                "data" => []
            ]
        ];
        $response->legends = [];

        foreach($response->labores as $index => $labor){
            $response->legends[] = $labor->nombre;

            $ponderacion_labor = $this->db->queryOne("SELECT SUM(valor) FROM labores_causas WHERE idLabor = {$labor->idLabor}");
            $suma_ponderacion = $this->db->queryRow("SELECT COUNT(DISTINCT id_muestra_json) jsons, SUM(valor) valor
                FROM muestras
                WHERE idLabor = {$labor->idLabor}
                    AND idLote = {$this->postdata->calidadLabor->lote}
                    {$sWhere}
            ");
            $val = round(100 - ($suma_ponderacion->valor / ($ponderacion_labor * ($this->num_plantas_per_json * $suma_ponderacion->jsons)) * 100), 2);
            $response->series[0]["data"][] = ["value" => $val, "itemStyle" => ['color' => $this->colors[$index]]];
        }

        return $response;
    }

    public function calidadLote(){
        $response = new stdClass;

        $sWhere = "";
        if($this->postdata->periodo > 0){
            $sWhere .= " AND periodo = {$this->postdata->periodo}";
        }
        if($this->postdata->semana > 0){
            $sWhere .= " AND semana = {$this->postdata->semana}";
        }
        if($this->postdata->fecha_inicial != ''){
            $sWhere .= " AND fecha BETWEEN '{$this->postdata->fecha_inicial}' AND '{$this->postdata->fecha_final}'";
        }
        if(isset($this->postdata->id_finca) && $this->postdata->id_finca > 0){
            $sWhere .= " AND muestras.idFinca = {$this->postdata->id_finca}";
        }

        $sql = "SELECT idLabor, labores.nombre
                FROM muestras
                INNER JOIN labores ON idLabor = labores.id
                WHERE 1=1 $sWhere
                GROUP BY idLabor";
        $response->labores = $this->db->queryAll($sql);

        if(isset($this->postdata->calidadLote)){
            if(isset($this->postdata->calidadLote->labor) && $this->postdata->calidadLote->labor > 0){
                $e = false;
                foreach($response->labores as $labor){
                    if($labor->idLabor == $this->postdata->calidadLote->labor){
                        $e = true;
                        break;
                    }
                }
                if(!$e) {
                    $response->selected_labor = $response->labores[0]->idLabor;
                    $this->postdata->calidadLote = (object)[
                        "labor" => $response->selected_labor
                    ];
                }
            }else{
                $response->selected_labor = $response->labores[0]->idLabor;
                $this->postdata->calidadLote = (object)[
                    "labor" => $response->selected_labor
                ];
            }
        }else{
            $response->selected_labor = $response->labores[0]->idLabor;
            $this->postdata->calidadLote = (object)[
                "labor" => $response->selected_labor
            ];
        }

        $sql = "SELECT idLote, lotes.nombre
                FROM muestras
                INNER JOIN lotes ON idLote = lotes.id
                WHERE 1=1 $sWhere
                GROUP BY idLote";
        $response->lotes = $this->db->queryAll($sql);

        $response->series = [
            [
                "name" => "Cantidad",
                "type" => 'bar',
                "data" => []
            ]
        ];
        $response->legends = [];

        foreach($response->lotes as $index => $lote){
            $response->legends[] = $lote->nombre;

            $ponderacion_labor = $this->db->queryOne("SELECT SUM(valor) FROM labores_causas WHERE idLabor = {$this->postdata->calidadLote->labor}");
            $suma_ponderacion = $this->db->queryRow("SELECT COUNT(DISTINCT id_muestra_json) jsons, SUM(valor) valor
                FROM muestras
                WHERE idLabor = {$this->postdata->calidadLote->labor}
                    AND idLote = {$lote->idLote}
                    {$sWhere}
            ");
            $val = round(100 - ($suma_ponderacion->valor / ($ponderacion_labor * ($this->num_plantas_per_json * $suma_ponderacion->jsons)) * 100), 2);
            $response->series[0]["data"][] = ["value" => $val, "itemStyle" => ['color' => $this->colors[$index]]];
        }

        return $response;
    }
}