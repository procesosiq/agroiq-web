<?php defined('PHRAPI') or die("Direct access not allowed!");

class Marcas {
    private $db;
    private $config;
    private $token;

    public function __construct(){
        $this->session = Session::getInstance();
        $this->db = DB::getInstance($this->session->agent_user);
        $this->piq = DB::getInstance();
        $this->unidad = $this->piq->queryOne("SELECT balanza_cajas_unidad FROM companies WHERE id = {$this->session->id_company}");
        $this->postdata = (object)json_decode(file_get_contents("php://input"));
    }

    public function saveMarca(){
        $response = new stdClass;
        $response->status = 400;

        if(isset($this->postdata->id) && $this->postdata->id > 0){
            $e = (int) $this->db->queryOne("SELECT COUNT(1) FROM marcas_cajas WHERE nombre = TRIM('{$this->postdata->nombre}') AND id != {$this->postdata->id}");
            if($e > 0){
                $response->message = "Ya existe otra marca con el mismo nombre";
                return $response;
            }

            $response->status = 200;
            $sql = "UPDATE marcas_cajas SET 
                        nombre = TRIM('{$this->postdata->nombre}'),
                        codigo = '{$this->postdata->codigo}',
                        nombre_alias = TRIM('{$this->postdata->nombre_alias}'),
                        cliente = '{$this->postdata->cliente}',
                        exportador = '{$this->postdata->exportador}',
                        tipo = '{$this->postdata->tipo}'
                    WHERE id = {$this->postdata->id}";
            $this->db->query($sql);
        }else{
            $e = (int) $this->db->queryOne("SELECT COUNT(1) FROM marcas_cajas WHERE nombre = TRIM('{$this->postdata->nombre}') AND status != 'Archivado'");
            if($e > 0){
                $response->message = "Ya existe otra marca con el mismo nombre";
                return $response;
            }

            $response->status = 200;
            $sql = "INSERT INTO marcas_cajas SET 
                        nombre = TRIM('{$this->postdata->nombre}'),
                        codigo = '{$this->postdata->codigo}',
                        nombre_alias = '{$this->postdata->nombre_alias}',
                        cliente = '{$this->postdata->cliente}',
                        exportador = '{$this->postdata->exportador}',
                        tipo = '{$this->postdata->tipo}'";
            $this->db->query($sql);
            $id_marca = $this->db->getLastID();

            // ACTUALIZAR BASE DE DATOS DE CAJA
            $sql = "UPDATE produccion_cajas
                    SET id_marca = {$id_marca}
                    WHERE marca = TRIM('{$this->postdata->nombre}') AND id_marca IS NULL";
            $this->db->query($sql);
            // ACTUALIZAR BASE DE DATOS DE GUIAS DE REMISION
            $sql = "UPDATE produccion_cajas_real
                    SET id_marca = {$id_marca}
                    WHERE marca = TRIM('{$this->postdata->alias}') AND id_marca IS NULL";
            $this->db->query($sql);
        }

        return $response;
    }

    public function deleteMarca(){
        $response = new stdClass;
        $response->status = 400;

        if(isset($this->postdata->id) && $this->postdata->id > 0){
            $response->status = 200;
            $sql = "UPDATE marcas_cajas SET status = 'Archivado' WHERE id = {$this->postdata->id}";
            $this->db->query($sql);
        }

        return $response;
    }

    public function addAlias(){
        $response = new stdClass;
        $response->status = 400;

        if(isset($this->postdata->id) && $this->postdata->id > 0){
            $alias = isset($this->postdata->alias) && $this->postdata->alias != '' ? $this->postdata->alias : '';
            $response->status = 200;
            $sql = "INSERT INTO marcas_cajas_alias SET id_marca_caja = {$this->postdata->id}, alias = '{$alias}'";
            $this->db->query($sql);
            $response->id = $this->db->getLastID();

            if($alias != ''){
                    // ACTUALIZAR BASE DE DATOS DE CAJA
                $sql = "UPDATE produccion_cajas
                        SET id_marca = {$this->postdata->id}
                        WHERE marca = TRIM('{$alias}') AND id_marca IS NULL";
                $this->db->query($sql);
                // ACTUALIZAR BASE DE DATOS DE GUIAS DE REMISION
                $sql = "UPDATE produccion_cajas_real
                        SET id_marca = {$this->postdata->id}
                        WHERE marca = TRIM('{$alias}') AND id_marca IS NULL";
                $this->db->query($sql);
                // ACTUALIZAR BASE DE DATOS DE MOVIMIENTOS
                $marca_name = $this->db->queryOne("SELECT IF(nombre_alias != '', nombre_alias, nombre) FROM marcas_cajas WHERE id = {$this->postdata->id}");
                $sql = "UPDATE produccion_cajas_pendiente_movimientos
                        SET marca = '{$marca_name}'
                        WHERE marca = TRIM('{$alias}')";
                $this->db->query($sql);
            }
        }

        return $response;
    }

    public function deleteAlias(){
        $response = new stdClass;
        $response->status = 400;

        if(isset($this->postdata->id) && $this->postdata->id > 0){
            $response->status = 200;
            $sql = "DELETE FROM marcas_cajas_alias WHERE id = {$this->postdata->id}";
            $this->db->query($sql);
        }

        return $response;
    }

    public function saveAlias(){
        $response = new stdClass;
        $response->status = 400;

        if(isset($this->postdata->id) && $this->postdata->id > 0){
            $response->status = 200;
            $sql = "UPDATE marcas_cajas_alias SET alias = '{$this->postdata->alias}' WHERE id = {$this->postdata->id}";
            $this->db->query($sql);

            $id_marca = $this->db->queryOne("SELECT id_marca_caja FROM marcas_cajas_alias WHERE id = {$this->postdata->id}");

            // ACTUALIZAR BASE DE DATOS DE CAJA
            $sql = "UPDATE produccion_cajas
                    SET id_marca = {$id_marca}
                    WHERE marca = TRIM('{$this->postdata->alias}') AND id_marca IS NULL";
            $this->db->query($sql);
            // ACTUALIZAR BASE DE DATOS DE GUIAS DE REMISION
            $sql = "UPDATE produccion_cajas_real
                    SET id_marca = {$id_marca}
                    WHERE marca = TRIM('{$this->postdata->alias}') AND id_marca IS NULL";
            $this->db->query($sql);
            // ACTUALIZAR BASE DE DATOS DE MOVIMIENTOS
            $marca_name = $this->db->queryOne("SELECT IF(nombre_alias != '', nombre_alias, nombre) FROM marcas_cajas WHERE id = {$this->postdata->id}");
            $sql = "UPDATE produccion_cajas_pendiente_movimientos
                    SET marca = '{$marca_name}'
                    WHERE marca = TRIM('{$this->postdata->alias}')";
            $this->db->query($sql);
        }

        return $response;
    }

    public function addRango(){
        $response = new stdClass;
        $response->status = 400;

        if(isset($this->postdata->id) && $this->postdata->id > 0){
            $response->status = 200;
            $sql = "INSERT INTO marcas_cajas_rangos SET id_marca_caja = {$this->postdata->id}";
            $this->db->query($sql);
            $response->id = $this->db->getLastID();
        }

        return $response;
    }

    public function saveRango(){
        $response = new stdClass;
        $response->status = 400;

        if(isset($this->postdata->id) && $this->postdata->id > 0){
            $response->status = 200;
            $last = $this->db->queryRow("SELECT * FROM marcas_cajas_rangos WHERE id = {$this->postdata->id}");

            $rango_lb = (object)[
                "max" => $this->postdata->max != 0 ? $this->db->queryOne("SELECT ROUND(convertir({$this->postdata->max}, '{$this->unidad}', 'lb'), 2)") : 0,
                "min" => $this->postdata->min != 0 ? $this->db->queryOne("SELECT ROUND(convertir({$this->postdata->min}, '{$this->unidad}', 'lb'), 2)") : 0,
                "requerimiento" => $this->postdata->requerimiento != 0 ? $this->db->queryOne("SELECT ROUND(convertir({$this->postdata->requerimiento}, '{$this->unidad}', 'lb'), 2)") : 0,
                "umbral_minimo" => $this->postdata->umbral_minimo != 0 ? $this->db->queryOne("SELECT ROUND(convertir({$this->postdata->umbral_minimo}, '{$this->unidad}', 'lb'), 2)") : 0
            ];

            if($last->umbral_minimo > $this->postdata->umbral_minimo){
                $limite_rango = $this->db->queryRow("SELECT * FROM marcas_cajas_rangos WHERE fecha > '{$last->fecha}' ORDER BY fecha");
                $sWhere = "";
                if($limite_rango->fecha){
                    $sWhere .= " AND fecha <= '{$limite_rango->fecha}'";
                }

                // CAJAS CON PESO BAJO (PESO ORIGINAL)
                $sql = "UPDATE produccion_cajas   
                        SET lb = peso_lb_real,
                            peso_corregido = 0
                        WHERE id_marca = {$last->id_marca_caja} 
                            AND peso_lb_real > {$rango_lb->umbral_minimo} 
                            AND peso_corregido = 1
                            AND fecha >= '{$last->fecha}'
                            $sWhere";
                $this->db->query($sql);

                // CAJAS CON BAJO PESO & CUADRE DE CAJAS (NUEVO PESO PROMEDIO)
                $sql = "UPDATE produccion_cajas c
                        SET lb = getPesoPromCaja(fecha, id_marca, {$this->postdata->id})
                        WHERE id_marca = {$last->id_marca_caja} 
                            AND (
                                peso_lb_real <= {$rango_lb->umbral_minimo} 
                                OR
                                tipo = 'cuadre'
                            )
                            AND fecha >= '{$last->fecha}'
                            $sWhere";
                $this->db->query($sql);
            }

            $sql = "UPDATE marcas_cajas_rangos 
                    SET
                        fecha = '{$this->postdata->fecha}',
                        requerimiento = IF('{$this->postdata->requerimiento}' != 0, '{$rango_lb->requerimiento}', NULL),
                        max = IF('{$this->postdata->max}' != 0, '{$rango_lb->max}', NULL),
                        min = IF('{$this->postdata->min}' != 0, '{$rango_lb->min}', NULL),
                        umbral_minimo = IF('{$this->postdata->umbral_minimo}' != 0, '{$rango_lb->umbral_minimo}', NULL)
                    WHERE id = {$this->postdata->id}";
            $this->db->query($sql);
        }

        return $response;
    }

    public function deleteRango(){
        $response = new stdClass;
        $response->status = 400;

        if(isset($this->postdata->id) && $this->postdata->id > 0){
            $response->status = 200;
            $sql = "DELETE FROM marcas_cajas_rangos WHERE id = {$this->postdata->id}";
            $this->db->query($sql);
        }

        return $response;
    }
}