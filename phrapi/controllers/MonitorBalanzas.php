<?php defined('PHRAPI') or die("Direct access not allowed!");

class MonitorBalanzas {
    private $db;
    private $config;
    private $token;

    public function __construct(){
        $this->config = $GLOBALS['config'];
        $this->session = Session::getInstance();
        $this->db = DB::getInstance('digital');
        $this->godaddy = DB::getInstance($this->session->agent_user);
        $this->postdata = (object)json_decode(file_get_contents("php://input"));
    }

    public function index(){
        $response = new stdClass;
        $response->data = $this->godaddy->queryAllOne("SELECT id_finca FROM fincas_dias_proceso WHERE fecha = CURRENT_DATE AND proceso = 1 GROUP BY id_finca");
        return $response;
    }

    public function getComentario(){
        $response = new stdClass;
        $response->status = 200;
        $response->data = $this->godaddy->queryOne("SELECT comentarios FROM monitor_balanzas_comentarios WHERE fecha = CURRENT_DATE AND usuario = '{$this->session->usuario}' AND id_finca = {$this->postdata->id_finca}");
        return $response;
    }

    public function comentario(){
        $response = new stdClass;
        $response->status = 200;

        $sql = "SELECT COUNT(1) FROM monitor_balanzas_comentarios WHERE fecha = CURRENT_DATE AND usuario = '{$this->session->usuario}' AND id_finca = '{$this->postdata->id_finca}'";
        $e = (int) $this->godaddy->queryOne($sql);

        if($e == 0){
            $sql = "INSERT INTO monitor_balanzas_comentarios SET comentarios = '{$this->postdata->comentarios}', fecha = CURRENT_DATE, usuario = '{$this->session->usuario}', id_finca = '{$this->postdata->id_finca}'";
            $this->godaddy->query($sql);
        }
        else {
            $sql = "UPDATE monitor_balanzas_comentarios SET comentarios = '{$this->postdata->comentarios}' WHERE fecha = CURRENT_DATE AND usuario = '{$this->session->usuario}' AND id_finca = '{$this->postdata->id_finca}'";
            $this->godaddy->query($sql);
        }

        return $response;
    }
}