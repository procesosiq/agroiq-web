<?php defined('PHRAPI') or die("Direct access not allowed!");

class MonitorProntoforms extends ProntoformsReader {
    private $db;
    private $config;
    private $token;

    public function __construct(){
        $this->config = $GLOBALS['config'];
        $this->session = Session::getInstance();
        $this->db = DB::getInstance('digital');
        $this->godaddy = DB::getInstance($this->session->agent_user);
        $this->postdata = (object)json_decode(file_get_contents("php://input"));
    }

    public function last(){
        $response = new stdClass;

        $sql = "SELECT MAX(fecha) FROM prontoforms_logs";
        $response->date = $this->db->queryOne($sql);

        $sql = "SELECT fecha FROM prontoforms_logs GROUP BY fecha";
        $response->days =  $this->db->queryAllOne($sql);

        return $response;
    }

    private function getValuePageQuestion($json, $page_name, $question_name){
        $q = null;

        $page_name = strtoupper($page_name);
        $question_name = strtoupper($question_name);

        foreach($json['pages'] as $page){
            if(strtoupper($page['name']) == $page_name){
                foreach($page['answers'] as $question){
                    if(strtoupper($question['question']) == $question_name){
                        $q = $question;
                        break;
                    }
                }
            }
        }

        if($q){
            return $this->getValueFromDataType($q['dataType'], $q['values'], $json['referenceNumber']);
        }else{
            return null;
        }
    }

    private function getForms(){
        $forms = [
            "Calidad" => [
                'path' => PHRAPI_PATH.'/../json/calidad/sumifru/completados/calidad3',
                'url' => 'http://app.procesos-iq.com/json/calidad/sumifru/completados/calidad3/'
            ],
            "Merma" => [
                'path' => PHRAPI_PATH.'/../json/merma/sumifru/completados/',
                'url' => 'http://app.procesos-iq.com/json/merma/sumifru/completados/'
            ]
        ];

        return $forms;
    }

    private function getFiles($fecha, $path){
        $date = implode("", explode("-", $fecha));
        $_path = realpath($path);
        return glob($_path."/{$date}-*");
    }

    private function getJsons($names){
        $jsons = [];
        foreach($names as $path){
            $json = json_decode(file_get_contents($path), true);
            $jsons[] = $json;
        }
        return $jsons;
    }

    private function getMuestrasByUser($files){
        $jsons = $this->getJsons($files);
        $muestras = [];

        foreach($jsons as $json){
            $user = $json['user']['username'];
            
            $finca = $this->getValuePageQuestion($json, 'ENCABEZADO', 'HACIENDA');
            if(!$finca) $finca = $this->getValuePageQuestion($json, 'ENCABEZADO', 'FINCA');
            if(!$finca) $finca = 'SIN DATOS';

            $user_finca = $user.'-'.$finca;
            if(!isset($muestras[$user_finca])) $muestras[$user_finca] = 0;
            $muestras[$user_finca]++;
        }

        return $muestras;
    }

    private function getDataByUser($data){
        $response = new stdClass;
        $response->data = [];
        $response->users = [];
        $response->fincas = [];
        $users_fincas = [];

        foreach($data as $form){
            foreach($form['cantidad'] as $us_fn => $value){
                if(!in_array($us_fn, $users_fincas)) $users_fincas[] = $us_fn;

                $user = explode("-", $us_fn)[0];
                $finca = explode("-", $us_fn)[1];
                if(!in_array($user, $response->users)) $response->users[] = $user;
                if(!in_array($user, $response->fincas)) $response->fincas[] = $finca;

                $index = array_search($us_fn, $users_fincas);
                if(!isset($response->data[$index])) $response->data[] = [ "usuario" => $user, "finca" => $finca ];
                if(!isset($response->data[$index][$form['form']])) $response->data[$index][$form['form']] = 0;
                
                $response->data[$index][$form['form']] += $value;
            }
        }

        return $response;
    }

    public function index(){
        $response = new stdClass;

        $response->data_by_form = [];
        $response->data_by_user = [];

        $fecha = $this->postdata->fecha;
        $mode = 'Muestras';
        $response->forms = $this->getForms();

        foreach($response->forms as $form_name => $form){
            $files = $this->getFiles($fecha, $form['path']);

            if($mode == 'Muestras'){
                // data by form
                $response->data_by_form[] = [
                    "form" => $form_name,
                    "cantidad" => $this->getMuestrasByUser($files)
                ];
                // data by user
                $response->data_by_user = $this->getDataByUser($response->data_by_form);

                // get comentarios
                foreach($response->data_by_user->data as &$row){
                    $c = $this->godaddy->queryOne("SELECT comentarios FROM monitor_prontoforms_comentarios WHERE fecha = '{$fecha}' AND usuario = '{$row['usuario']}' AND finca = '{$row['finca']}'");
                    if($c) $row['comentarios'] = $c;
                    else $row['comentarios'] = '';
                }
            }
        }

        return $response;
    }

    public function comentario(){
        $response = new stdClass;
        $response->status = 200;

        $sql = "SELECT COUNT(1) FROM monitor_prontoforms_comentarios WHERE fecha = '{$this->postdata->fecha}' AND usuario = '{$this->postdata->usuario}' AND finca = '{$this->postdata->finca}'";
        $e = (int) $this->godaddy->queryOne($sql);

        if($e == 0){
            $sql = "INSERT INTO monitor_prontoforms_comentarios SET comentarios = '{$this->postdata->comentarios}', fecha = '{$this->postdata->fecha}', usuario = '{$this->postdata->usuario}', finca = '{$this->postdata->finca}'";
            $this->godaddy->query($sql);
        }
        else {
            $sql = "UPDATE monitor_prontoforms_comentarios SET comentarios = '{$this->postdata->comentarios}' WHERE fecha = '{$this->postdata->fecha}' AND usuario = '{$this->postdata->usuario}' AND finca = '{$this->postdata->finca}'";
            $this->godaddy->query($sql);
        }

        return $response;
    }
}