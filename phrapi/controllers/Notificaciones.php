<?php defined('PHRAPI') or die("Direct access not allowed!");

class Notificaciones {

	private $session;
	private $db;

	public function __construct(){
		$this->config = $GLOBALS['config'];
		$this->session = Session::getInstance();
		$this->db = DB::getInstance($this->session->agent_user);
		$this->postdata = (object)json_decode(file_get_contents("php://input"));
	}

	public function delete(){
		$response = new stdClass;

		$sql = "UPDATE procesosiq.notificaciones SET status = 'Archivado' WHERE id = {$_POST['id']}";
		$this->db->query($sql);
		$response->status = 200;

		return $response;
	}

	public function consolelog(){
		$response = new stdClass;

		$error = addslashes($_POST['error']);
		$url = addslashes($_POST['url']);
		
		$sql = "INSERT INTO procesosiq.consolelog SET 
					error = '{$error}',
					url = '{$url}', 
					user_login = '{$this->session->usuario}'";
		$this->db->query($sql);
		$response->status = 200;

		return $response;
	}
}
