<?php defined('PHRAPI') or die("Direct access not allowed!");

class PalmaLaboresAgricolasDia {
    public $name;
    private $db;
    private $config;

    public function __construct(){
        $this->config = $GLOBALS['config'];
        $this->session = Session::getInstance();
        $this->db = DB::getInstance($this->session->agent_user);
        $this->postdata = (object)json_decode(file_get_contents("php://input"));
        $this->num_plantas_per_json = 10;
        $this->colors = ['#c23531','#2f4554', '#61a0a8', '#d48265', '#91c7ae','#749f83',  '#ca8622', '#bda29a','#6e7074', '#546570', '#c4ccd3'];
    }

    public function last(){
        $response = new stdClass;
        $response->fecha = $this->db->queryOne("SELECT MAX(fecha) FROM palma_labores_agricolas");
        $response->days = $this->db->queryAllOne("SELECT fecha FROM palma_labores_agricolas GROUP BY fecha");
        $response->periodos = $this->db->queryAllOne("SELECT periodo FROM palma_labores_agricolas GROUP BY periodo");
        $response->semanas = $this->db->queryAllOne("SELECT semana FROM palma_labores_agricolas GROUP BY semana");
        return $response;
    }

    public function variables(){
        $response = new stdClass;

        $sWhere = "";
        if($this->postdata->fecha_inicial != ''){
            $sWhere .= " AND fecha BETWEEN '{$this->postdata->fecha_inicial}' AND '{$this->postdata->fecha_final}'";
        }

        $response->fincas = $this->db->queryAll("SELECT id_finca as id, fincas.nombre FROM palma_labores_agricolas INNER JOIN fincas ON fincas.id = id_finca WHERE 1=1 $sWhere GROUP BY id_finca");
        return $response;
    }

    public function index(){
        $response = new stdClass;
        $response->tablaResumen = $this->tablaResumen();
        $response->pieCausas = $this->pieCausas();
        $response->calidadLabor = $this->calidadLabor();
        $response->calidadLote = $this->calidadLote();
        $response->markers = $this->markers();
        $response->fotos = $this->fotos();
        $response->lineas_plantas = $this->lineasPlantas();
        return $response;
    }

    public function lineasPlantas(){
        $response = new stdClass;
        $sWhere = "";
        if($this->postdata->periodo > 0){
            $sWhere .= " AND periodo = {$this->postdata->periodo}";
        }
        if($this->postdata->semana > 0){
            $sWhere .= " AND semana = {$this->postdata->periodo}";
        }
        if($this->postdata->fecha_inicial != ''){
            $sWhere .= " AND fecha BETWEEN '{$this->postdata->fecha_inicial}' AND '{$this->postdata->fecha_final}'";
        }
        if(isset($this->postdata->id_finca) && $this->postdata->id_finca > 0){
            $sWhere .= " AND id_finca = {$this->postdata->id_finca}";
        }

        $sql = "SELECT id_lote, lotes.nombre
                FROM palma_labores_agricolas j
                INNER JOIN palma_labores_agricolas_plantas p ON p.id_muestra_json = j.id
                INNER JOIN palma_labores_agricolas_plantas_labores l ON p.id = l.id_planta
                INNER JOIN labores_agricolas_labores labores ON id_labor = labores.id
                INNER JOIN lotes ON id_lote = lotes.id
                WHERE 1=1 $sWhere
                GROUP BY id_lote";
        $response->lotes = $this->db->queryAll($sql);

        $sql = "SELECT linea
                FROM palma_labores_agricolas j
                INNER JOIN palma_labores_agricolas_plantas p ON p.id_muestra_json = j.id
                INNER JOIN palma_labores_agricolas_plantas_labores l ON p.id = l.id_planta
                WHERE 1=1 $sWhere
                GROUP BY linea
                ORDER BY linea";
        $response->lineas = $this->db->queryAllOne($sql);

        foreach($response->lotes as $row){
            $row->values = [];
            foreach($response->lineas as $linea){
                $sql = "SELECT COUNT(1)
                        FROM palma_labores_agricolas j
                        INNER JOIN palma_labores_agricolas_plantas p ON p.id_muestra_json = j.id
                        WHERE id_lote = {$row->id_lote} AND linea = {$linea} {$sWhere}";
                $row->{"linea_{$linea}"} = $this->db->queryOne($sql);
                $row->values[] = $row->{"linea_{$linea}"};
            }
        }

        return $response;
    }

    public function fotos(){
        $response = new stdClass;

        $sWhere = "";
        if($this->postdata->periodo > 0){
            $sWhere .= " AND j.periodo = {$this->postdata->periodo}";
        }
        if($this->postdata->semana > 0){
            $sWhere .= " AND j.semana = {$this->postdata->semana}";
        }
        if($this->postdata->fecha_inicial != ''){
            $sWhere .= " AND j.fecha BETWEEN '{$this->postdata->fecha_inicial}' AND '{$this->postdata->fecha_final}'";
        }
        if(isset($this->postdata->id_finca) && $this->postdata->id_finca > 0){
            $sWhere .= " AND id_finca = {$this->postdata->id_finca}";
        }

        $response->path_s3 = "https://s3.amazonaws.com/json-publicos/palma/agroaereo/labores_agricolas/";
        $sql = "SELECT referencia, labores.nombre as labor, images as fotos, lotes.nombre as lote, linea
                FROM palma_labores_agricolas j
                INNER JOIN palma_labores_agricolas_plantas p ON p.id_muestra_json = j.id
                INNER JOIN palma_labores_agricolas_plantas_labores l ON p.id = l.id_planta
                INNER JOIN labores_agricolas_labores labores ON id_labor = labores.id
                INNER JOIN lotes ON id_lote = lotes.id
                where images != '' {$sWhere}
                GROUP BY p.id";
        $data = $this->db->queryAll($sql);
        $response->data = [];
        foreach($data as $row){
            $_dd = explode("|", $row->fotos);
            $_ddd = [];
            for($x = 0; $x < count($_dd); $x++){
                $_ddd[] = [
                    "labor" => $row->labor,
                    "lote" => $row->lote,
                    "linea" => $row->linea,
                    "url" => $response->path_s3.$row->referencia."_".$_dd[$x]
                ];
            }
            $response->data = array_merge($response->data, $_ddd);
        }
        return $response;
    }

    public function markers(){
        $response = new stdClass;

        $sWhere = "";
        $sWherePeriodo = "";
        $sWhereLote = "";
        $sWhereLabor = "";
        
        if($this->postdata->periodo > 0){
            $sWhere .= " AND periodo = {$this->postdata->periodo}";
        }
        if($this->postdata->semana > 0){
            $sWhere .= " AND semana = {$this->postdata->periodo}";
        }
        if($this->postdata->fecha_inicial != ''){
            $sWhere .= " AND fecha BETWEEN '{$this->postdata->fecha_inicial}' AND '{$this->postdata->fecha_final}'";
        }
        if(isset($this->postdata->id_finca) && $this->postdata->id_finca > 0){
            $sWhere .= " AND id_finca = {$this->postdata->id_finca}";
        }
        if(isset($this->postdata->markers)){
            if(isset($this->postdata->markers->periodo) && $this->postdata->markers->periodo > 0){
                $sWherePeriodo .= " AND periodo = {$this->postdata->markers->periodo}";
            }

            if(isset($this->postdata->markers->lote) && $this->postdata->markers->lote > 0){
                $sWhereLote .= " AND lotes.id = {$this->postdata->markers->lote}";
            }

            if(isset($this->postdata->markers->labor) && $this->postdata->markers->labor > 0){
                $sWhereLabor .= " AND labores.id = {$this->postdata->markers->labor}";
            }
        }

        $sql = "SELECT p.id as id_planta, lat, lng, labores.nombre as labor, j.id as id_muestra_json, lotes.nombre as lote, linea, fincas.nombre as finca
                FROM palma_labores_agricolas j
                INNER JOIN palma_labores_agricolas_plantas p ON p.id_muestra_json = j.id
                INNER JOIN palma_labores_agricolas_plantas_labores l ON p.id = l.id_planta
                INNER JOIN labores_agricolas_labores labores ON id_labor = labores.id
                INNER JOIN fincas ON id_finca = fincas.id
                INNER JOIN lotes ON id_lote = lotes.id
                WHERE lat IS NOT NULL AND lng IS NOT NULL $sWhere $sWherePeriodo $sWhereLote $sWhereLabor
                GROUP BY p.id";
        $response->data = $this->db->queryAll($sql);
        foreach($response->data as $marker){

            $sql = "SELECT REPLACE(labores.nombre, 'EVALUACIÓN', '') as labor, causas.causa, ROUND(ponderacion/(COUNT(DISTINCT p.id)*70)*100, 2) as valor
                    FROM palma_labores_agricolas j
                    INNER JOIN palma_labores_agricolas_plantas p ON p.id_muestra_json = j.id
                    INNER JOIN palma_labores_agricolas_plantas_labores l ON p.id = l.id_planta
                    INNER JOIN labores_agricolas_labores labores ON id_labor = labores.id
                    INNER JOIN palma_labores_agricolas_plantas_labores_causas c ON c.id_planta_labor = l.id
                    INNER JOIN lotes ON id_lote = lotes.id
                    INNER JOIN labores_agricolas_labores_causas causas ON id_causa = causas.id
                    WHERE p.id = {$marker->id_planta} $sWhere $sWherePeriodo $sWhereLote $sWhereLabor
                    GROUP BY id_causa";
            $marker->plantas = $this->db->queryAll($sql);

            $marker->total = sumOfValue($marker->plantas, "valor");
        }

        $sql = "SELECT j.periodo
                FROM palma_labores_agricolas j
                INNER JOIN palma_labores_agricolas_plantas p ON p.id_muestra_json = j.id
                INNER JOIN palma_labores_agricolas_plantas_labores l ON p.id = l.id_planta
                INNER JOIN labores_agricolas_labores labores ON id_labor = labores.id
                INNER JOIN lotes ON id_lote = lotes.id
                WHERE 1=1 $sWhere $sWhereLote $sWhereLabor
                GROUP BY j.periodo";
        $response->periodos = $this->db->queryAllOne($sql);

        $sql = "SELECT id_lote, lotes.nombre
                FROM palma_labores_agricolas j
                INNER JOIN palma_labores_agricolas_plantas p ON p.id_muestra_json = j.id
                INNER JOIN palma_labores_agricolas_plantas_labores l ON p.id = l.id_planta
                INNER JOIN labores_agricolas_labores labores ON id_labor = labores.id
                INNER JOIN lotes ON id_lote = lotes.id
                WHERE 1=1 $sWhere $sWherePeriodo $sWhereLabor
                GROUP BY id_lote";
        $response->lotes = $this->db->queryAll($sql);

        $sql = "SELECT id_labor, labores.nombre
                FROM palma_labores_agricolas j
                INNER JOIN palma_labores_agricolas_plantas p ON p.id_muestra_json = j.id
                INNER JOIN palma_labores_agricolas_plantas_labores l ON p.id = l.id_planta
                INNER JOIN labores_agricolas_labores labores ON id_labor = labores.id
                INNER JOIN lotes ON id_lote = lotes.id
                WHERE 1=1 $sWhere $sWherePeriodo $sWhereLote
                GROUP BY id_labor";
        $response->labores = $this->db->queryAll($sql);

        return $response;
    }

    public function tablaResumen(){
        $response = new stdClass;

        $sWhere = "";
        if($this->postdata->periodo > 0){
            $sWhere .= " AND periodo = {$this->postdata->periodo}";
        }
        if($this->postdata->semana > 0){
            $sWhere .= " AND semana = {$this->postdata->semana}";
        }
        if($this->postdata->fecha_inicial != ''){
            $sWhere .= " AND fecha BETWEEN '{$this->postdata->fecha_inicial}' AND '{$this->postdata->fecha_final}'";
        }
        if(isset($this->postdata->id_finca) && $this->postdata->id_finca > 0){
            $sWhere .= " AND id_finca = {$this->postdata->id_finca}";
        }

        $sql = "SELECT labores.id id_labor, labores.nombre
                FROM palma_labores_agricolas j
                INNER JOIN palma_labores_agricolas_plantas p ON p.id_muestra_json = j.id
                INNER JOIN palma_labores_agricolas_plantas_labores l ON p.id = l.id_planta
                INNER JOIN palma_labores_agricolas_plantas_labores_causas c ON c.id_planta_labor = l.id
                INNER JOIN labores_agricolas_labores labores ON l.id_labor = labores.id
                INNER JOIN lotes ON id_lote = lotes.id
                WHERE 1=1 {$sWhere}
                GROUP BY labores.id";
        $response->tabla_labores_defectos = $this->db->queryAll($sql);

        $sql = "SELECT lotes.id id_lote, lotes.nombre as lote
                FROM palma_labores_agricolas j
                INNER JOIN palma_labores_agricolas_plantas p ON p.id_muestra_json = j.id
                INNER JOIN palma_labores_agricolas_plantas_labores l ON p.id = l.id_planta
                INNER JOIN palma_labores_agricolas_plantas_labores_causas c ON c.id_planta_labor = l.id
                INNER JOIN labores_agricolas_labores labores ON l.id_labor = labores.id
                INNER JOIN lotes ON id_lote = lotes.id
                WHERE 1=1 {$sWhere}
                GROUP BY lotes.id";
        $response->lotes = $this->db->queryAll($sql);

        foreach($response->tabla_labores_defectos as $row){
            $ponderacion_labor = $this->db->queryOne("SELECT SUM(valor) FROM labores_agricolas_labores_causas WHERE id_labor = {$row->id_labor}");
            $row->values = [];

            $sql = "SELECT causas.id as id_causa, causas.causa
                    FROM palma_labores_agricolas j
                    INNER JOIN palma_labores_agricolas_plantas p ON p.id_muestra_json = j.id
                    INNER JOIN palma_labores_agricolas_plantas_labores l ON p.id = l.id_planta
                    INNER JOIN palma_labores_agricolas_plantas_labores_causas c ON c.id_planta_labor = l.id
                    INNER JOIN labores_agricolas_labores labores ON l.id_labor = labores.id
                    INNER JOIN lotes ON id_lote = lotes.id
                    INNER JOIN labores_agricolas_labores_causas causas ON id_causa = causas.id
                    WHERE labores.id = {$row->id_labor}
                        {$sWhere}
                    GROUP BY causas.id";
            $defectos = $this->db->queryAll($sql);
            $row->detalle = [];
            
            // CALIDAD LABOR
            foreach($response->lotes as $lote){
                $sql = "SELECT (SUM(total_ponderacion)/(COUNT(DISTINCT p.id)*70)*100)
                        FROM palma_labores_agricolas j
                        INNER JOIN palma_labores_agricolas_plantas p ON p.id_muestra_json = j.id
                        INNER JOIN palma_labores_agricolas_plantas_labores l ON p.id = l.id_planta
                        INNER JOIN labores_agricolas_labores labores ON l.id_labor = labores.id
                        INNER JOIN lotes ON id_lote = lotes.id
                        WHERE lotes.id = {$lote->id_lote} AND labores.id = {$row->id_labor}";
                $val = (float) $this->db->queryOne($sql);
                $row->{"lote_{$lote->idLote}"} = round($val, 2);
                $row->values[] = $row->{"lote_{$lote->idLote}"};
            }

            // DEFECTOS
            foreach($defectos as $causa){
                $_defecto = [
                    "causa" => $causa->causa,
                    "values" => []
                ];

                foreach($response->lotes as $lote){
                    $sql = "SELECT ROUND(SUM(ponderacion)/(COUNT(DISTINCT p.id)*70)*100, 2)
                            FROM palma_labores_agricolas j
                            INNER JOIN palma_labores_agricolas_plantas p ON p.id_muestra_json = j.id
                            INNER JOIN palma_labores_agricolas_plantas_labores l ON p.id = l.id_planta
                            INNER JOIN palma_labores_agricolas_plantas_labores_causas c ON c.id_planta_labor = l.id

                            INNER JOIN labores_agricolas_labores labores ON l.id_labor = labores.id
                            INNER JOIN lotes ON id_lote = lotes.id
                            INNER JOIN labores_agricolas_labores_causas causas ON id_causa = causas.id

                            WHERE lotes.id = {$lote->id_lote} AND labores.id = {$row->id_labor} AND causas.id = {$causa->id_causa}";
                    $val = (float) $this->db->queryOne($sql);
                    $_defecto["lote_{$lote->idLote}"] = round($val, 2);
                    $_defecto["values"][] = $_defecto["lote_{$lote->idLote}"];
                }

                $row->detalle[] = $_defecto;
            }
        }

        return $response;
    }

    public function pieCausas(){
        $response = new stdClass;

        $sWhere = "";
        $sWhereLabor = "";
        $sWhereLote = "";

        if($this->postdata->periodo > 0){
            $sWhere .= " AND periodo = {$this->postdata->periodo}";
        }
        if($this->postdata->semana > 0){
            $sWhere .= " AND semana = {$this->postdata->semana}";
        }
        if($this->postdata->fecha_inicial != ''){
            $sWhere .= " AND fecha BETWEEN '{$this->postdata->fecha_inicial}' AND '{$this->postdata->fecha_final}'";
        }
        if(isset($this->postdata->id_finca) && $this->postdata->id_finca > 0){
            $sWhere .= " AND id_finca = {$this->postdata->id_finca}";
        }
        if(isset($this->postdata->pieCausa)){
            if(isset($this->postdata->pieCausa->lote) && $this->postdata->pieCausa->lote > 0){
                $sWhereLote .= " AND lotes.id = {$this->postdata->pieCausa->lote}";
            }
            if(isset($this->postdata->pieCausa->labor) && $this->postdata->pieCausa->labor > 0){
                $sWhereLabor .= " AND labores.id = {$this->postdata->pieCausa->labor}";
            }
        }

        $sql = "SELECT labores.id as id_labor, labores.nombre
                FROM palma_labores_agricolas j
                INNER JOIN palma_labores_agricolas_plantas p ON p.id_muestra_json = j.id
                INNER JOIN palma_labores_agricolas_plantas_labores l ON p.id = l.id_planta
                INNER JOIN palma_labores_agricolas_plantas_labores_causas c ON c.id_planta_labor = l.id
                INNER JOIN labores_agricolas_labores labores ON l.id_labor = labores.id
                INNER JOIN lotes ON id_lote = lotes.id
                WHERE 1=1 $sWhere $sWhereLote
                GROUP BY labores.id";
        $response->labores = $this->db->queryAll($sql);

        $sql = "SELECT lotes.id id_lote, lotes.nombre
                FROM palma_labores_agricolas j
                INNER JOIN palma_labores_agricolas_plantas p ON p.id_muestra_json = j.id
                INNER JOIN palma_labores_agricolas_plantas_labores l ON p.id = l.id_planta
                INNER JOIN labores_agricolas_labores labores ON l.id_labor = labores.id
                INNER JOIN palma_labores_agricolas_plantas_labores_causas c ON c.id_planta_labor = l.id
                INNER JOIN lotes ON id_lote = lotes.id
                WHERE 1=1 $sWhere $sWhereLabor
                GROUP BY lotes.id";
        $response->lotes = $this->db->queryAll($sql);

        $sql = "SELECT causa AS label, SUM(ponderacion) 'value'
                FROM palma_labores_agricolas j
                INNER JOIN palma_labores_agricolas_plantas p ON p.id_muestra_json = j.id
                INNER JOIN palma_labores_agricolas_plantas_labores l ON p.id = l.id_planta
                INNER JOIN palma_labores_agricolas_plantas_labores_causas c ON c.id_planta_labor = l.id AND ponderacion > 0
                INNER JOIN labores_agricolas_labores labores ON l.id_labor = labores.id
                INNER JOIN lotes ON id_lote = lotes.id
                WHERE 1=1 $sWhere $sWhereLabor $sWhereLote
                GROUP BY causa";
        $response->data = $this->db->queryAll($sql);

        return $response;
    }

    public function calidadLabor(){
        $response = new stdClass;

        $sWhere = "";
        if($this->postdata->periodo > 0){
            $sWhere .= " AND periodo = {$this->postdata->periodo}";
        }
        if($this->postdata->semana > 0){
            $sWhere .= " AND semana = {$this->postdata->semana}";
        }
        if($this->postdata->fecha_inicial != ''){
            $sWhere .= " AND fecha BETWEEN '{$this->postdata->fecha_inicial}' AND '{$this->postdata->fecha_final}'";
        }
        if(isset($this->postdata->id_finca) && $this->postdata->id_finca > 0){
            $sWhere .= " AND id_finca = {$this->postdata->id_finca}";
        }

        $sql = "SELECT lotes.id id_lote, lotes.nombre
                FROM palma_labores_agricolas j
                INNER JOIN palma_labores_agricolas_plantas p ON p.id_muestra_json = j.id
                INNER JOIN palma_labores_agricolas_plantas_labores l ON p.id = l.id_planta
                INNER JOIN palma_labores_agricolas_plantas_labores_causas c ON c.id_planta_labor = l.id
                INNER JOIN labores_agricolas_labores labores ON l.id_labor = labores.id
                INNER JOIN lotes ON id_lote = lotes.id
                WHERE 1=1 $sWhere
                GROUP BY lotes.id";
        $response->lotes = $this->db->queryAll($sql);

        if(isset($this->postdata->calidadLabor)){
            if(isset($this->postdata->calidadLabor->lote) && $this->postdata->calidadLabor->lote > 0){
                $e = false;
                foreach($response->lotes as $lote){
                    if($lote->id_lote == $this->postdata->calidadLabor->lote){
                        $e = true;
                        break;
                    }
                }
                if(!$e) {
                    $response->selected_lote = $response->lotes[0]->id_lote;
                    $this->postdata->calidadLabor = (object)[
                        "lote" => $response->selected_lote
                    ];
                }
            }else{
                $response->selected_lote = $response->lotes[0]->id_lote;
                $this->postdata->calidadLabor = (object)[
                    "lote" => $response->selected_lote
                ];
            }
        }else{
            $response->selected_lote = $response->lotes[0]->id_lote;
            $this->postdata->calidadLabor = (object)[
                "lote" => $response->selected_lote
            ];
        }

        $sql = "SELECT labores.id id_labor, labores.nombre
                FROM palma_labores_agricolas j
                INNER JOIN palma_labores_agricolas_plantas p ON p.id_muestra_json = j.id
                INNER JOIN palma_labores_agricolas_plantas_labores l ON p.id = l.id_planta
                INNER JOIN palma_labores_agricolas_plantas_labores_causas c ON c.id_planta_labor = l.id
                INNER JOIN labores_agricolas_labores labores ON l.id_labor = labores.id
                INNER JOIN lotes ON id_lote = lotes.id
                WHERE lotes.id = {$this->postdata->calidadLabor->lote} $sWhere
                GROUP BY labores.id";

        $response->labores = $this->db->queryAll($sql);

        $response->series = [
            [
                "name" => "Cantidad",
                "type" => 'bar',
                "data" => []
            ]
        ];
        $response->legends = [];

        foreach($response->labores as $index => $labor){
            $response->legends[] = implode("\n", explode(" ", $labor->nombre));

            $sql = "SELECT (SUM(total_ponderacion)/(COUNT(DISTINCT p.id)*70)*100)
                    FROM palma_labores_agricolas j
                    INNER JOIN palma_labores_agricolas_plantas p ON p.id_muestra_json = j.id
                    INNER JOIN palma_labores_agricolas_plantas_labores l ON p.id = l.id_planta
                    INNER JOIN labores_agricolas_labores labores ON l.id_labor = labores.id
                    INNER JOIN lotes ON id_lote = lotes.id
                    WHERE labores.id = {$labor->id_labor}
                        AND lotes.id = {$this->postdata->calidadLabor->lote} 
                        $sWhere";
            $val = (float) $this->db->queryOne($sql);
            $response->series[0]["data"][] = ["value" => $val, "itemStyle" => ['color' => $this->colors[$index]]];
        }

        return $response;
    }

    public function calidadLote(){
        $response = new stdClass;

        $sWhere = "";
        if($this->postdata->periodo > 0){
            $sWhere .= " AND periodo = {$this->postdata->periodo}";
        }
        if($this->postdata->semana > 0){
            $sWhere .= " AND semana = {$this->postdata->semana}";
        }
        if($this->postdata->fecha_inicial != ''){
            $sWhere .= " AND fecha BETWEEN '{$this->postdata->fecha_inicial}' AND '{$this->postdata->fecha_final}'";
        }
        if(isset($this->postdata->id_finca) && $this->postdata->id_finca > 0){
            $sWhere .= " AND id_finca = {$this->postdata->id_finca}";
        }

        $sql = "SELECT labores.id id_labor, labores.nombre
                FROM palma_labores_agricolas j
                INNER JOIN palma_labores_agricolas_plantas p ON p.id_muestra_json = j.id
                INNER JOIN palma_labores_agricolas_plantas_labores l ON p.id = l.id_planta
                INNER JOIN labores_agricolas_labores labores ON l.id_labor = labores.id
                INNER JOIN lotes ON id_lote = lotes.id
                WHERE 1=1 $sWhere
                GROUP BY labores.id";
        $response->labores = $this->db->queryAll($sql);

        if(isset($this->postdata->calidadLote)){
            if(isset($this->postdata->calidadLote->labor) && $this->postdata->calidadLote->labor > 0){
                $e = false;
                foreach($response->labores as $labor){
                    if($labor->id_labor == $this->postdata->calidadLote->labor){
                        $e = true;
                        break;
                    }
                }
                if(!$e) {
                    $response->selected_labor = $response->labores[0]->id_labor;
                    $this->postdata->calidadLote = (object)[
                        "labor" => $response->selected_labor
                    ];
                }
            }else{
                $response->selected_labor = $response->labores[0]->id_labor;
                $this->postdata->calidadLote = (object)[
                    "labor" => $response->selected_labor
                ];
            }
        }else{
            $response->selected_labor = $response->labores[0]->id_labor;
            $this->postdata->calidadLote = (object)[
                "labor" => $response->selected_labor
            ];
        }

        $sql = "SELECT lotes.id id_lote, lotes.nombre
                FROM palma_labores_agricolas j
                INNER JOIN palma_labores_agricolas_plantas p ON p.id_muestra_json = j.id
                INNER JOIN palma_labores_agricolas_plantas_labores l ON p.id = l.id_planta
                INNER JOIN labores_agricolas_labores labores ON l.id_labor = labores.id
                INNER JOIN lotes ON id_lote = lotes.id
                WHERE 1=1 $sWhere
                GROUP BY lotes.id";
        $response->lotes = $this->db->queryAll($sql);

        $response->series = [
            [
                "name" => "Cantidad",
                "type" => 'bar',
                "data" => []
            ]
        ];
        $response->legends = [];

        foreach($response->lotes as $index => $lote){
            $response->legends[] = $lote->nombre;

            $sql = "SELECT (SUM(total_ponderacion)/(COUNT(DISTINCT p.id)*70)*100)
                    FROM palma_labores_agricolas j
                    INNER JOIN palma_labores_agricolas_plantas p ON p.id_muestra_json = j.id
                    INNER JOIN palma_labores_agricolas_plantas_labores l ON p.id = l.id_planta
                    INNER JOIN labores_agricolas_labores labores ON l.id_labor = labores.id
                    INNER JOIN lotes ON id_lote = lotes.id
                    WHERE labores.id = {$this->postdata->calidadLote->labor}
                        AND lotes.id = {$lote->id_lote} 
                        $sWhere";
            $val = (float) $this->db->queryOne($sql);
            $response->series[0]["data"][] = ["value" => $val, "itemStyle" => ['color' => $this->colors[$index]]];
        }

        return $response;
    }
}