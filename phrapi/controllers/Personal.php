<?php defined('PHRAPI') or die("Direct access not allowed!");

class Personal {
	public $name;
	private $db;
	private $config;

	public function __construct(){
		$this->config = $GLOBALS['config'];
		$this->session = Session::getInstance();
		// D($this->session);
        $this->maindb = DB::getInstance();
        $this->db = DB::getInstance($this->session->agent_user);
	}

	public function index(){
		$postdata = (object)json_decode(file_get_contents("php://input"));

		$filters = (object)[

		];

		$response = new stdClass;
		$response->data = [];
		$sql = "SELECT * , IF(status = 1 ,'Activo' , 'Inactivo') AS tittle_status FROM personal";
		$response->data = $this->db->queryAll($sql);

		return $response;
	}

	public function getLabores(){
		$response = new stdClass;
		$sql = "SELECT nombre AS id , nombre AS label FROM labores";
		$response = $this->db->queryAllSpecial($sql);
		return $response;
	}

	public function getPersonal($idPersonal = 0){
		$postdata = (object)json_decode(file_get_contents("php://input"));
		$data = (object)[
			"idPersonal" => getValueFrom($postdata , "idPersonal" , $idPersonal , FILTER_SANITIZE_PHRAPI_INT)
		];
		$sWhere = "";
		$response = new stdClass;
		$response->data = [];
		$response->labores = $this->getLabores();
		$response->sueldos = [];
		$response->fincas = [];
		$response->showFinca = 0;
		if($this->session->id_company == 7){
			$response->fincas = (array)$this->db->queryAllSpecial("SELECT IF(id > 1 , 3 , 1) AS id , IF(nombre != 'Carolina','San José','Carolina') AS label FROM fincas GROUP BY nombre LIKE '%San%' ");
			$response->showFinca = 1;
		}
		if($data->idPersonal > 0){
			$sql = "SELECT * , IF(DATE(fecha_contrato) != '', 1 , 0) AS id_contrato , id AS idPersonal FROM personal WHERE id = {$data->idPersonal}";
			$response->data = $this->db->queryRow($sql);
			$response->sueldos = $this->getSueldo($data->idPersonal);
		}

		return $response;
	}

	public function saveSuledo(){
		$postdata = (object)json_decode(file_get_contents("php://input"));
		$data = (object)[
			"idPersonal" => getValueFrom($postdata , "idPersonal", 0 , FILTER_SANITIZE_PHRAPI_INT),
			"sueldo" => getValueFrom($postdata , "sueldo", 0 , FILTER_SANITIZE_PHRAPI_INT),
			"fecha" => getValueFrom($postdata , "fecha", "" , FILTER_SANITIZE_STRING)
		];
		if($data->idPersonal > 0 && $data->sueldo > 0){
			$row = $this->db->queryRow("SELECT id , salario  FROM persona_salario WHERE id_personal = {$data->idPersonal} ORDER BY id DESC LIMIT 1");
			$update = false;
			if(isset($row->salario)){
				$update = true;
			}
			if($data->sueldo > 0){
				if($row->sueldo != $data->sueldo){
					if($update){
						$sql_old = "UPDATE persona_salario SET fecha_end = '{$data->fecha}' WHERE id_personal = {$data->idPersonal} AND id = {$row->id} ";
						$this->db->query($sql_old);
					}
					$sql_sueldo = "INSERT INTO persona_salario SET id_personal = '{$data->idPersonal}',salario = '{$data->sueldo}' ,fecha_start = '{$data->fecha}'";
					$this->db->query($sql_sueldo);
				}
			}
		}

		return $this->getSueldo($data->idPersonal);
	}

	// public function listarSueldo(){
	// 	$postdata = (object)json_decode(file_get_contents("php://input"));
	// 	$data = (object)[
	// 		"idPersonal" => getValueFrom($postdata , "idPersonal", 0 , FILTER_SANITIZE_PHRAPI_INT)
	// 	];

	// 	$response = new stdClass;
	// 	$response->data = [];
	// 	if($data->idPersonal > 0){
	// 		$sql = "SELECT id_personal , salario , fecha_start AS fecha FROM persona_salario WHERE id_personal = {$data->idPersonal}";
	// 		$response->data = $this->db->queryAll($sql);
	// 	}
	// 	return $response->data;
	// }

	private function getSueldo($id_personal = 0){
		$response = new stdClass;
		$response->data = [];
		if($id_personal > 0){
			$sql = "SELECT id_personal , salario , fecha_start AS fecha FROM persona_salario WHERE id_personal = {$id_personal}";
			$response->data = $this->db->queryAll($sql);
		}
		return $response->data;
	}

	public function changeStatus(){
		$data = (object)[
			"id_personal" => getValueFrom($_POST , "id_personal" , 0 , FILTER_SANITIZE_PHRAPI_INT),
			"status" => getValueFrom($_POST , "status" , 0 , FILTER_SANITIZE_PHRAPI_INT),
		];

		$status = 1;
		if($data->id_personal > 0){
			$sql = "UPDATE personal SET status = IF(status = 1 , 0 ,1) WHERE id = {$data->id_personal}";
			$this->db->query($sql);

			$status = $this->db->queryOne("SELECT status FROM personal WHERE id = {$data->id_personal}");
		}

		return ["success" => $status];
	}

	public function savePersonal(){
		$postdata = (object)json_decode(file_get_contents("php://input"));
		$data = (object)[
			"idFinca" => getValueFrom($postdata , "idFinca", 0 , FILTER_SANITIZE_PHRAPI_INT),
			"idPersonal" => getValueFrom($postdata , "idPersonal", 0 , FILTER_SANITIZE_PHRAPI_INT),
			"ingreso" => getValueFrom($postdata , "ingreso", "" , FILTER_SANITIZE_STRING),
			"contrato" => getValueFrom($postdata , "contrato", "" , FILTER_SANITIZE_STRING),
			"id_contrato" => getValueFrom($postdata , "id_contrato", "" , FILTER_SANITIZE_STRING),
			"salida" => getValueFrom($postdata , "salida", "" , FILTER_SANITIZE_STRING),
			"nombre" => getValueFrom($postdata , "nombre", "" , FILTER_SANITIZE_STRING),
			"codigo" => getValueFrom($postdata , "codigo", "" , FILTER_SANITIZE_STRING),
			"cedula" => getValueFrom($postdata , "cedula", "" , FILTER_SANITIZE_STRING),
			"sexo" => getValueFrom($postdata , "sexo", "" , FILTER_SANITIZE_STRING),
			"cargo" => getValueFrom($postdata , "cargo", "" , FILTER_SANITIZE_STRING),
			"labor" => getValueFrom($postdata , "labor", "" , FILTER_SANITIZE_STRING),
			"discapacitado" => getValueFrom($postdata , "discapacitado", "" , FILTER_SANITIZE_STRING),
			"afiliado" => getValueFrom($postdata , "afiliado", "" , FILTER_SANITIZE_STRING),
			"civil" => getValueFrom($postdata , "civil", "" , FILTER_SANITIZE_STRING),
			"familiares" => getValueFrom($postdata , "familiares", 0 , FILTER_SANITIZE_PHRAPI_INT),
			"beneficios" => getValueFrom($postdata , "beneficios", "" , FILTER_SANITIZE_STRING),
			"horas" => getValueFrom($postdata , "horas", 8 , FILTER_SANITIZE_PHRAPI_INT),
			"sueldo" => getValueFrom($postdata , "sueldo", 0 , FILTER_SANITIZE_PHRAPI_FLOAT),
			"nacimiento" => getValueFrom($postdata , "nacimiento", "" , FILTER_SANITIZE_STRING),
			"provincia" => getValueFrom($postdata , "provincia", "" , FILTER_SANITIZE_STRING),
			"canton" => getValueFrom($postdata , "canton", "" , FILTER_SANITIZE_STRING),
			"sector" => getValueFrom($postdata , "sector", "" , FILTER_SANITIZE_STRING),
			"direccion" => getValueFrom($postdata , "direccion", "" , FILTER_SANITIZE_STRING),
			"telefono" => getValueFrom($postdata , "telefono", "" , FILTER_SANITIZE_STRING),
		];

		if($data->nombre != ""){
			$sql = "INSERT INTO personal SET
					codigo = '{$data->codigo}',
					cedula = '{$data->cedula}',
					nombre = '{$data->nombre}',
					labor = '{$data->labor}',
					cargo = '{$data->cargo}',
					sexo = '{$data->sexo}',
					discapacitado = '{$data->discapacitado}',
					fecha_ingreso = '{$data->ingreso}',
					fecha_contrato = '{$data->contrato}',
					fecha_salida = '{$data->salida}',
					afiliado = '{$data->afiliado}',
					civil = '{$data->civil}',
					beneficios = '{$data->beneficios}',
					nacimiento = '{$data->nacimiento}',
					provincia = '{$data->provincia}',
					canton = '{$data->canton}',
					sector = '{$data->sector}',
					direccion = '{$data->direccion}',
					telefono = '{$data->telefono}',
					familiares = '{$data->familiares}'";

			if($data->idPersonal > 0){
				$sql = "UPDATE personal SET
						codigo = '{$data->codigo}',
						cedula = '{$data->cedula}',
						nombre = '{$data->nombre}',
						labor = '{$data->labor}',
						cargo = '{$data->cargo}',
						sexo = '{$data->sexo}',
						discapacitado = '{$data->discapacitado}',
						fecha_ingreso = '{$data->ingreso}',
						fecha_contrato = '{$data->contrato}',
						fecha_salida = '{$data->salida}',
						afiliado = '{$data->afiliado}',
						civil = '{$data->civil}',
						beneficios = '{$data->beneficios}',
						horas = '{$data->horas}',
						nacimiento = '{$data->nacimiento}',
						provincia = '{$data->provincia}',
						canton = '{$data->canton}',
						sector = '{$data->sector}',
						direccion = '{$data->direccion}',
						telefono = '{$data->telefono}',
						familiares = '{$data->familiares}'
						WHERE id = {$data->idPersonal}";
			}
			$this->db->query($sql);
			$idPersonal =  $this->db->getLastID();
			$data->idPersonal = $idPersonal;
		}

		return $this->getPersonal($data->idPersonal);
	}
}






