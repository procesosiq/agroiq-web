<?php defined('PHRAPI') or die("Direct access not allowed!");

class ProduccionCajas {
    
    public $name;
    private $db;
    private $config;

    public function __construct(){
        $this->config = $GLOBALS['config'];
        $this->session = Session::getInstance();
        $this->db = DB::getInstance($this->session->agent_user);
        $this->piq = DB::getInstance();
        $this->unidad = $this->session->unidad_cajas;
        $this->convertidas_default = (float) $this->piq->queryOne("SELECT unidad_default_convertidas_cajas FROM companies WHERE id = {$this->session->id_company}");
    }

    public function convertir($unidad_from, $unidad_to, $valor){
        if($unidad_from == $unidad_to){
            return $valor;
        }

        if($unidad_from == 'kg' && $unidad_to == 'lb'){
            $formula = $this->piq->queryOne("SELECT convertir_kg_lb FROM companies_variables WHERE id_company = {$this->session->id_company}");
            $formula = str_replace('{kg}', $valor, $formula);
            return '('.$formula.')';
        }

        if($unidad_from == 'lb' && $unidad_to == 'kg'){
            $formula = $this->piq->queryOne("SELECT convertir_lb_kg FROM companies_variables WHERE id_company = {$this->session->id_company}");
            $formula = str_replace('{lb}', $valor, $formula);
            return '('.$formula.')';
        }

        return null;
    }

    // CAMBIAR LA UNIDAD DE VIAZUALIZAR (KG Y LB)
    public function changeUnidad(){
        $response = new stdClass;
        $response->status = 200;
        $this->session->unidad_cajas = $this->session->unidad_cajas == 'lb' ? 'kg' : 'lb';
        return $response;
    }

    public function last(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));

        $sWhere = "";
        if(!$postdata->newDate){
            $sWhere .= " AND fecha = '{$postdata->filters->fecha_inicial}'";
        }

        $response->days = $this->db->queryAllOne("SELECT fecha FROM produccion_cajas WHERE fecha IS NOT NULL AND fecha != '0000-00-00' GROUP BY fecha");
        $response->last = $this->db->queryRow("SELECT MAX(fecha) as fecha 
            FROM (
                SELECT MAX(fecha) AS fecha
                FROM produccion_cajas
                WHERE 1=1 $sWhere
            ) AS tbl");
        $response->fincas = $this->db->queryAllSpecial("SELECT id_finca AS id, fincas.nombre AS label FROM produccion_cajas cajas INNER JOIN fincas ON id_finca = fincas.id WHERE cajas.fecha = '{$response->last->fecha}'");
        $response->available_fincas = $this->db->queryAllSpecial("SELECT id, nombre AS label FROM fincas WHERE status = 1");
        $response->produccion_fincas = $this->db->queryAllSpecial("SELECT id, nombre AS label FROM fincas WHERE status_produccion = 'Activo'");
        $response->unidad = $this->unidad;

        $variables = $this->piq->queryRow("SELECT * FROM companies_variables WHERE id_company = {$this->session->id_company}");
        $response->enabled = [
            "procesar" => isset($variables->cajas_procesar) ? ((int) $variables->cajas_procesar) : 0,
            "eliminar_marca" => isset($variables->cajas_boton_eliminar_marca) ? ((int) $variables->cajas_boton_eliminar_marca) : 0
        ];
        return $response;
    }

    // CREAR CAJAS PARA UNA FINCA DONDE NO HUBO PROCESO DE LA BALANZA
    public function crearFincaProceso(){
        $response = new stdClass;
        $response->status = 400;
        $response->message = "Información incorrecta";
        $postdata = (object)json_decode(file_get_contents("php://input"));

        $unidad = $this->piq->queryOne("SELECT balanza_cajas_unidad FROM companies WHERE id = {$this->session->id_company}");

        if($postdata->id_finca > 0 && $postdata->fecha != '' && count($postdata->marcas) > 0){
            foreach($postdata->marcas as $id_marca => $cantidad){
                $last_fecha = $this->db->queryOne("SELECT MAX(fecha) FROM produccion_cajas WHERE id_marca = {$id_marca} AND fecha <= '{$postdata->fecha}'");
                if($last_fecha == '' || $last_fecha == null){
                    $marca = $this->db->queryRow("SELECT IF(nombre_alias != '', nombre_alias, nombre) nombre, requerimiento FROM marcas_cajas marcas INNER JOIN marcas_cajas_rangos rango ON rango.id = getRangoMarca(marcas.id, '{$postdata->fecha}') WHERE marcas.id = {$id_marca}");
                    if($marca->requerimiento){
                        $peso = $marca->requerimiento;
                        $message_list[] = "Hemos puesto a la marca {$marca->nombre} el peso referencial ({$peso}) debido a falta de información no ha sido posible colocar el peso prom";
                    }else{
                        error_log("ERROR AL CREAR CAJAS ".json_encode($postdata));
                        $response->message = "No tenemos información para esta marca de caja ($id_marca)";
                        return $response;
                    }
                }else{
                    $sql = "SELECT ROUND(AVG({$unidad}), 2) 
                            FROM produccion_cajas cajas
                            INNER JOIN marcas_cajas m ON id_marca = m.id
                            LEFT JOIN marcas_cajas_rangos rango ON rango.id = getRangoMarca(m.id, '{$last_fecha}')
                            WHERE cajas.fecha = '{$last_fecha}' AND m.id = {$id_marca} AND IF(rango.umbral_minimo > 0, lb > rango.umbral_minimo, TRUE)";
                    $peso = $this->db->queryOne($sql);
                }

                $lb = $this->convertir($unidad, 'lb', $peso);
                $kg = $this->convertir($unidad, 'kg', $peso);
                $sql = "INSERT INTO produccion_cajas SET
                            fecha = '{$postdata->fecha}',
                            id_marca = {$id_marca},
                            marca = (SELECT nombre FROM marcas_cajas WHERE id = {$id_marca}),
                            id_finca = {$postdata->id_finca},
                            finca = (SELECT nombre FROM fincas WHERE id = {$postdata->id_finca}),
                            semana = getWeek('{$postdata->fecha}'),
                            year = getYear('{$postdata->fecha}'),
                            convertidas_415 = {$lb}/41.5,
                            convertidas_custom = {$lb}/{$this->convertidas_default},
                            peso_lb_real = {$lb},
                            lb = {$lb},
                            kg = {$kg},
                            tipo = 'creadas',
                            unidad_peso_real = '{$unidad}',
                            updated_at = CURRENT_TIMESTAMP";

                for($x = 1; $x <= $cantidad; $x++){
                    $this->db->query($sql);
                }
            }
            $response->status = 200;
            $response->message = "Registrado con éxito";
        }

        return $response;
    }

    // CREAR CAJAS PARA UNA FINCA DONDE SI HUBO PROCESO DE LA BALANZA
    public function crearCajas(){
        $response = new stdClass;
        $response->status = 400;
        $response->message = "Información incorrecta";
        $postdata = (object)json_decode(file_get_contents("php://input"));

        $message_list = [];
        $unidad = $this->piq->queryOne("SELECT balanza_cajas_unidad FROM companies WHERE id = {$this->session->id_company}");

        if($postdata->id_finca > 0 && $postdata->fecha != '' && count($postdata->marcas) > 0){
            foreach($postdata->marcas as $id_marca => $cantidad){
                
                $last_fecha = $this->db->queryOne("SELECT MAX(fecha) FROM produccion_cajas WHERE id_marca = {$id_marca} AND fecha <= '{$postdata->fecha}'");
                if($last_fecha == '' || $last_fecha == null){
                    $marca = $this->db->queryRow("SELECT IF(nombre_alias != '', nombre_alias, nombre) nombre, requerimiento FROM marcas_cajas marcas INNER JOIN marcas_cajas_rangos rango ON rango.id = getRangoMarca(marcas.id, '{$postdata->fecha}') WHERE marcas.id = {$id_marca}");
                    if($marca->requerimiento){
                        $peso = $marca->requerimiento;
                        $message_list[] = "Hemos puesto a la marca {$marca->nombre} el peso referencial ({$peso}) debido a falta de información no ha sido posible colocar el peso prom";
                    }else{
                        error_log("ERROR AL CREAR CAJAS ".json_encode($postdata));
                        $response->message = "No tenemos información para esta marca de caja ($id_marca)";
                        return $response;
                    }
                }else{
                    $sql = "SELECT ROUND(AVG({$unidad}), 2) 
                            FROM produccion_cajas cajas
                            INNER JOIN marcas_cajas m ON id_marca = m.id
                            LEFT JOIN marcas_cajas_rangos rango ON rango.id = getRangoMarca(m.id, '{$last_fecha}')
                            WHERE cajas.fecha = '{$last_fecha}' AND m.id = {$id_marca} AND IF(rango.umbral_minimo > 0, lb > rango.umbral_minimo, TRUE)";
                    $peso = $this->db->queryOne($sql);
                }

                $kg = $this->convertir($unidad, 'kg', $peso);
                $lb = $this->convertir($unidad, 'lb', $peso);

                $sql = "INSERT INTO produccion_cajas SET
                            fecha = '{$postdata->fecha}',
                            id_marca = {$id_marca},
                            marca = (SELECT nombre FROM marcas_cajas WHERE id = {$id_marca}),
                            id_finca = {$postdata->id_finca},
                            finca = (SELECT nombre FROM fincas WHERE id = {$postdata->id_finca}),
                            semana = getWeek('{$postdata->fecha}'),
                            year = getYear('{$postdata->fecha}'),
                            convertidas_415 = {$lb}/41.5,
                            convertidas_custom = {$lb}/{$this->convertidas_default},
                            peso_lb_real = {$lb},
                            lb = {$lb},
                            kg = {$kg},
                            tipo = 'creadas',
                            unidad_peso_real = '{$unidad}',
                            updated_at = CURRENT_TIMESTAMP";

                $values = [];
                $fields_values = [
                    $postdata->fecha,
                    $id_marca,
                    "(SELECT nombre FROM marcas_cajas WHERE id = {$id_marca})",
                    $postdata->id_finca,
                    "(SELECT nombre FROM fincas WHERE id = {$postdata->id_finca})",
                    "getWeek('{$postdata->fecha}')",
                    "getYear('{$postdata->fecha}')",
                    "IF('{$unidad}' = 'lb', {$peso}, {$peso}/0.4536)/41.5",
                    "IF('{$unidad}' = 'lb', {$peso}, {$peso}/0.4536)",
                    "IF('{$unidad}' = 'lb', {$peso}, {$peso}/0.4536)",
                    "IF('{$unidad}' = 'lb', {$peso}*0.4536, {$peso})",
                    "'creadas'",
                    "'{$unidad}'"
                ];
                for($x = 1; $x <= $cantidad; $x++){
                    #$values[] = "(". implode(",", $fields_values) .")";
                    $this->db->query($sql);
                }
                #$sql_values = "INSERT INTO produccion_cajas (fecha, id_marca, marca, id_finca, finca, semana, year, convertidas_415, peso_lb_real, lb, kg, tipo, unidad_peso_real) VALUES " . implode(",", $values);
                #$this->db->query($sql_values);
            }
            $response->status = 200;
            $response->message = "Registrado con éxito\n".implode("\n", $message_list);
        }

        return $response;
    }

    // PROCESAR TODAS LAS CAJAS QUE ESTAN BAJO DE PESO DE UNA MARCA DE UN DIA (PONERLES EL PESO PROMEDIO)
    public function procesarMarca(){
        $response = new stdClass;
        $response->status = 400;
        $postdata = (object)json_decode(file_get_contents("php://input"));

        if($postdata->id_marca > 0 && $postdata->fecha != ''){
            $response->status = 200;
            $peso_min = $this->db->queryOne("SELECT umbral_minimo FROM marcas_cajas_rangos WHERE id = getRangoMarca({$postdata->id_marca}, '{$postdata->fecha}')");
            $peso_prom = $this->db->queryOne("SELECT AVG(lb) FROM produccion_cajas WHERE fecha = '{$postdata->fecha}' AND id_marca = '{$postdata->id_marca}' AND lb > {$peso_min}");

            $unidad = 'lb';
            $kg = $this->convertir($unidad, 'kg', $peso_prom);
            $lb = $this->convertir($unidad, 'lb', $peso_prom);

            $sql = "UPDATE produccion_cajas
                    SET
                        updated_at = CURRENT_TIMESTAMP,
                        peso_corregido = 1,
                        lb = {$lb},
                        kg = {$kg},
                        convertidas_415 = {$lb} / 41.5,
                        convertidas_custom = {$lb} / {$this->convertidas_default}
                    WHERE fecha = '{$postdata->fecha}' AND id_marca = {$postdata->id_marca} AND lb < $peso_min";
            $this->db->query($sql);
        }

        return $response;
    }

    public function eliminar(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        if(count($postdata->ids) > 0){
            foreach($postdata->ids as $id){
                $this->db->query("DELETE FROM produccion_cajas WHERE id = $id");
            }
            return true;
        }
        return false;
    }

    public function filters(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));
        if(isset($postdata->year) && $postdata->year != ""){
            $sYear = " AND year = '{$postdata->year}'";
        }

        $response->years = $this->db->queryAllSpecial("SELECT DISTINCT year as id, year as label FROM produccion_cajas WHERE 1=1 AND year > 0");
        
        $sql = "SELECT semana as id, semana as label 
                FROM(
                    SELECT semana FROM produccion_cajas  WHERE 1=1 $sYear GROUP BY semana 
                ) AS tbl
                GROUP BY semana";
        $response->semanas = $this->db->queryAll($sql);
        return $response;
    }

    public function marcas(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $filters = (object)[
            'actualPage' => (int) getValueFrom($postdata, 'actualPage', 0),
            'pagination' => (int) getValueFrom($postdata, 'pagination', 10),
            'fecha_inicial' => getValueFrom($postdata, 'fecha_inicial', ''),
            'fecha_final' => getValueFrom($postdata, 'fecha_final', ''),
            'finca' => getValueFrom($postdata, 'finca', 0)
        ];

        $response = new stdClass;

        $requerimiento = $this->convertir('lb', $this->unidad, 'mr.requerimiento');
        $max = $this->convertir('lb', $this->unidad, 'mr.max');
        $min = $this->convertir('lb', $this->unidad, 'mr.min');
        $umbral_minimo = $this->convertir('lb', $this->unidad, 'mr.umbral_minimo');

        $sql = "SELECT m.*, 
                    ROUND({$max}, 2) 'max', 
                    ROUND({$min}, 2) 'min', 
                    ROUND({$requerimiento}, 2) 'requerimiento', 
                    ROUND({$umbral_minimo}, 2) 'umbral_minimo',
                    mr.id as id_rango
                FROM marcas_cajas m 
                LEFT JOIN marcas_cajas_rangos mr ON mr.id = getRangoMarca(m.id, '{$filters->fecha_inicial}') 
                WHERE status != 'Archivado' 
                ORDER BY codigo+0, nombre";
        $response->marcas = $this->db->queryAll($sql);
        
        foreach($response->marcas as $m){
            $m->alias = $this->db->queryAll("SELECT id, alias FROM marcas_cajas_alias WHERE id_marca_caja = $m->id");

            $requerimiento = $this->convertir('lb', $this->unidad, 'requerimiento');
            $max = $this->convertir('lb', $this->unidad, 'max');
            $min = $this->convertir('lb', $this->unidad, 'min');
            $umbral_minimo = $this->convertir('lb', $this->unidad, 'umbral_minimo');

            $sql = "SELECT 
                        id, 
                        fecha,
                        ROUND({$max}, 2) 'max',
                        ROUND({$min}, 2) 'min',
                        ROUND({$requerimiento}, 2) 'requerimiento',
                        ROUND({$umbral_minimo}, 2) 'umbral_minimo'
                    FROM marcas_cajas_rangos 
                    WHERE id_marca_caja = {$m->id}";
                    
            $m->rangos = $this->db->queryAll($sql);
            foreach($m->rangos as $r){
                $r->max = (float) $r->max;
                $r->min = (float) $r->min;
                $r->requerimiento = (float) $r->requerimiento;
                $r->umbral_minimo = (float) $r->umbral_minimo;
            }
        }
        return $response;   
    }

    public function registros(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $filters = (object)[
            'fecha' => getValueFrom($postdata, 'fecha_inicial', ''),
            'finca' => getValueFrom($postdata, 'finca', 0),
            "marca" => getValueFrom($postdata, 'marca', '')
        ];
        $sWhere = "";

        $id_marca = '';
        if($filters->marca != ''){
            $id_marca = $this->db->queryOne("SELECT id_marca FROM brands WHERE nombre = '{$filters->marca}'");
            if(!$id_marca > 0) $id_marca = '';
            else {
                $sWhere .= " AND id_marca = {$id_marca}";
            }
        }

        $response = new stdClass;

        $fincas = $this->db->queryAllSpecial("SELECT id_finca AS id, fincas.nombre AS label FROM produccion_cajas cajas INNER JOIN fincas ON fincas.id = id_finca WHERE cajas.fecha = '{$filters->fecha}' GROUP BY id_finca");
        $response->fincas = $fincas;
        if(!isset($fincas[$filters->finca])){
            $filters->finca = array_keys($fincas)[0];
        }
        $response->id_finca = $filters->finca;
        $sWhere .= " AND id_finca = $filters->finca";
        $response->unidad = $this->unidad;

        $max = $this->convertir('lb', $this->unidad, 'r.max');
        $min = $this->convertir('lb', $this->unidad, 'r.min');

        $sql = "SELECT id_marca AS id, ROUND({$max}, 2) 'max', ROUND({$min}, 2) 'min'
                FROM (
                    SELECT id_marca
                    FROM produccion_cajas cajas
                    WHERE cajas.fecha = '{$filters->fecha}' AND id_marca IS NOT NULL $sWhere
                    GROUP BY id_marca
                ) cajas
                LEFT JOIN marcas_cajas_rangos r ON r.id = getRangoMarca(id_marca, '{$filters->fecha}')";
        $rangos = $this->db->queryAllIntoSpecial($sql);
        $response->rangos = $rangos;

        $response->data = $this->db->queryAll("SELECT * 
            FROM (
                SELECT cajas.id, IF(m.nombre_alias != '', m.nombre_alias, m.nombre) AS marca, {$this->unidad} AS peso, m.tipo, hora, peso_corregido, id_marca, '' AS class, UPPER(cajas.tipo) as tipo_creacion
                FROM produccion_cajas cajas
                INNER JOIN marcas_cajas m ON id_marca = m.id
                WHERE cajas.fecha = '{$filters->fecha}' AND id_marca IS NOT NULL $sWhere
                UNION ALL
                SELECT cajas.id, marca, {$this->unidad} AS peso, 'CAJA' AS tipo, hora, peso_corregido, null AS id_marca, '' AS class, UPPER(cajas.tipo) as tipo_creacion
                FROM produccion_cajas cajas
                WHERE fecha = '{$filters->fecha}' AND id_marca IS NUll $sWhere
            ) AS tbl
            ORDER BY hora DESC");
        foreach($response->data as $row){
            $row->peso = (float) round($row->peso, 2);

            if($row->id_marca > 0){
                $rango = $rangos[$row->id_marca];
                $rango->max = (float) $rango->max;
                $rango->min = (float) $rango->min;

                if($rango->max || $rango->min){
                    if($row->peso >= $rango->min && $row->peso <= $rango->max){
                        $row->class = 'bg-green-haze bg-font-green-haze';
                    }
                    else if($row->peso > $rango->max){
                        $row->class =  'bg-red-thunderbird bg-font-red-thunderbird';
                    }else{
                        $row->class =  'bg-yellow-gold bg-font-yellow-gold';
                    }
                }
            }
        }

        $max = $this->convertir('lb', $this->unidad, 'mr.max');
        $min = $this->convertir('lb', $this->unidad, 'mr.min');
        $requerimiento = $this->convertir('lb', $this->unidad, 'mr.requerimiento');
        $umbral_minimo = $this->convertir('lb', $this->unidad, 'mr.umbral_minimo');

        $sql = "SELECT m.*, 
                    ROUND({$requerimiento}, 2) 'requerimiento', 
                    ROUND({$max}, 2) 'max', 
                    ROUND({$min}, 2) 'min', 
                    ROUND({$umbral_minimo}, 2) 'umbral_minimo' 
                FROM marcas_cajas m 
                LEFT JOIN marcas_cajas_rangos mr ON mr.id = getRangoMarca(m.id, '{$filters->fecha}') 
                WHERE status != 'Archivado' 
                ORDER BY codigo+0, nombre";
        $response->marcas = $this->db->queryAll($sql);
        foreach($response->marcas as $m){
            $m->alias = $this->db->queryAll("SELECT id, alias FROM marcas_cajas_alias WHERE id_marca_caja = $m->id");

            $max = $this->convertir('lb', $this->unidad, 'max');
            $min = $this->convertir('lb', $this->unidad, 'min');
            $requerimiento = $this->convertir('lb', $this->unidad, 'requerimiento');
            $umbral_minimo = $this->convertir('lb', $this->unidad, 'umbral_minimo');

            $sql = "SELECT id, 
                        fecha,
                        ROUND({$max}, 2) 'max',
                        ROUND({$min}, 2) 'min',
                        ROUND({$requerimiento}, 2) 'requerimiento',
                        ROUND({$umbral_minimo}, 2) 'umbral_minimo'
                    FROM marcas_cajas_rangos 
                    WHERE id_marca_caja = $m->id";
            $m->rangos = $this->db->queryAll($sql);
            foreach($m->rangos as $r){
                $r->max = (float) $r->max;
                $r->min = (float) $r->min;
                $r->requerimiento = (float) $r->requerimiento;
                $r->umbral_minimo = (float) $r->umbral_minimo;
            }
        }
        return $response;   
    }

    // GUARCAR CAJAS EDITADAS
    public function guardarCaja(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;
        $response->status = 400;

        if($postdata->id_marca > 0){
            $sFields[] = "id_marca = {$postdata->id_marca}, marca = (SELECT nombre FROM marcas_cajas WHERE id = {$postdata->id_marca})";
        }
        if($postdata->peso > 0){
            $peso = $this->convertir($this->unidad, 'lb', $postdata->peso);
            $peso_kg = $this->convertir($this->unidad, 'kg', $postdata->peso);
            $sFields[] = "
                lb = ROUND({$peso}, 2),
                kg = ROUND({$peso_kg}, 2),
                peso_lb_real = ROUND({$peso}, 2)
            ";
        }

        if(count($sFields) == 0){
            $response->message = "Datos incorrectos";
            return $response;
        }

        if($postdata->ids > 0){
            $sFields = implode(",", $sFields);
            $response->status = 200;

            foreach($postdata->ids as $id){
                $sql = "UPDATE produccion_cajas
                        SET $sFields
                        WHERE id = {$id}";
                $this->db->query($sql);
            }
        }else{
            $response->message = "No selecciono filas";
        }

        return $response;
    }

    public function resumenMarca(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $filters = (object)[
            'fecha_inicial' => getValueFrom($postdata, 'fecha_inicial', getValueFrom($_GET, 'fecha_inicial', '')),
            'fecha_final' => getValueFrom($postdata, 'fecha_final', getValueFrom($_GET, 'fecha_final', '')),
            'semana' => (int) getValueFrom($postdata, 'semana', 0),
            'unidad' => getValueFrom($postdata, 'unidad', 'lb'),
            "finca" => (int) getValueFrom($postdata, 'finca', getValueFrom($_GET, 'finca', 0)),
        ];

        if($filters->fecha_final == ''){
            $filters->fecha_final = $filters->fecha_inicial;
        }
        
        $sWhere = "";
        $response = new stdClass;
        $response->fincas = $this->db->queryAllSpecial("SELECT id_finca AS id, fincas.nombre AS label FROM produccion_cajas INNER JOIN fincas ON id_finca = fincas.id WHERE fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}' GROUP BY id_finca");
        
        if(!isset($response->fincas[$filters->finca])){
            if(count($response->fincas) > 0)
                $filters->finca = array_keys($response->fincas)[0];
        }
        $sWhere .= " AND cajas.id_finca = $filters->finca";

        $sql = "SELECT 
                    id_marca,
                    marca, 
                    cantidad, 
                    total_kg, 
                    CONV AS 'conv', 
                    tipo, 
                    IF(id_marca IS NOT NULL AND umbral_minimo > 0,
                        (SELECT AVG({$this->unidad}) FROM produccion_cajas WHERE id_marca = tbl.id_marca AND fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}' AND lb > umbral_minimo), 
                        promedio
                    ) promedio, 
                    maximo, 
                    minimo, 
                    desviacion,
                    umbral_minimo,
                    IF(id_marca IS NOT NULL AND umbral_minimo > 0,
                        (SELECT COUNT(1) FROM produccion_cajas WHERE id_marca = tbl.id_marca AND fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}' AND lb <= umbral_minimo),
                        0
                    ) cajas_bajo_peso
                FROM(
                    SELECT
                        marca.id AS id_marca,
                        IF(marca.nombre_alias != '', marca.nombre_alias, marca.nombre) AS `marca`,
                        COUNT(1) AS `cantidad`,
                        ROUND(SUM(cajas.`{$this->unidad}`), 2) AS total_kg,
                        SUM(convertidas_custom) AS conv,
                        marca.tipo,
                        ROUND(AVG(cajas.`{$this->unidad}`), 2) AS promedio,
                        MAX(cajas.`{$this->unidad}`) AS maximo,
                        MIN(cajas.`{$this->unidad}`) AS minimo,
                        ROUND(STD(cajas.{$this->unidad}), 2) AS desviacion
                    FROM produccion_cajas cajas
                    INNER JOIN marcas_cajas marca ON marca.id = cajas.id_marca
                    WHERE id_marca IS NOT NULL AND cajas.fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}' $sWhere
                    GROUP BY cajas.id_marca
                    UNION ALL
                    SELECT
                        NULL AS id_marca,
                        marca,
                        COUNT(1) AS `cantidad`,
                        ROUND(SUM(cajas.`{$this->unidad}`), 2) AS total_kg,
                        SUM(convertidas_custom) AS conv,
                        'CAJA' AS tipo,
                        ROUND(AVG(cajas.`{$this->unidad}`), 2) AS promedio,
                        MAX(cajas.`{$this->unidad}`) AS maximo,
                        MIN(cajas.`{$this->unidad}`) AS minimo,
                        ROUND(STD(cajas.{$this->unidad}), 2) AS desviacion
                    FROM produccion_cajas cajas
                    WHERE id_marca IS NULL AND cajas.fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}' $sWhere
                    GROUP BY marca
                ) AS tbl
                LEFT JOIN marcas_cajas_rangos rango ON rango.id = getRangoMarca(id_marca, '{$filters->fecha_inicial}')";
                
        $response->data = $this->db->queryAll($sql);
        
        $response->convertidas = $this->convertidas_default;
        /* LOAD TAGS */
        $response->tags = $this->tags($filters);
        $response->tags["cajas40"] = $this->sumOfValue($response->data, 'conv');

        return $response;
    }

    private function sumOfValue($data, $prop){
        $sum = 0;
        foreach($data as $row)
            if(is_object($row))
                $sum += (double) $row->{$prop};
            else
                $sum += (double) $row[$prop];
        return $sum;
    }

    private function getSemanasVariable($var, $year){
        $round = 0;
        if($var == 'CONV/HA') $round = 2;
		$sql = [];
		for($i = 1; $i <= 53; $i++){
			$sql[] = "SELECT $i AS semana, ROUND(AVG(sem_{$i}), $round) cantidad, fincas.nombre AS 'name' FROM produccion_resumen_tabla INNER JOIN fincas ON id_finca = fincas.id WHERE sem_{$i} > 0 AND variable = '$var' AND anio = $year GROUP BY id_finca";
		}
		return implode("
		UNION ALL
		", $sql);
	}

    public function historicoCajasSemanal(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));

        $namesVar = [
            'CONV' => 'CAJAS 41.5',
            'CONV/HA' => 'CONV/HA'
        ];

        $data_chart = $this->db->queryAll("SELECT semana AS label_x, sum(cantidad) as value, name, 0 index_y
            FROM(
                {$this->getSemanasVariable("{$namesVar[$postdata->var]}", $postdata->year)}
            ) AS tbl
            WHERE cantidad > 0
            GROUP BY label_x, name
            ORDER BY label_x, name");

        $groups = [
            [
                "name" => $postdata->var,
                "type" => 'line',
                'format' => ''
            ]
        ];
        $response->chart = $this->grafica_z($data_chart, $groups);
        return $response;
    }

    public function getMarcas(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));
        if(isset($postdata->year) && $postdata->year != ""){
            $sYear = " AND year = '{$postdata->year}'";
        }

        $response->data = $this->db->queryAllSpecial("SELECT marca as id, marca as label FROM(
            SELECT marca
            FROM produccion_cajas
            WHERE 1=1 $sYear
            GROUP BY marca
        ) AS tbl");
        return $response;
    }

    public function getGuias(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));
        
        if($postdata->guia != "" && $postdata->fecha != ""){
            $sql = "SELECT IF(marcas.id IS NOT NULL, IF(marcas.nombre_alias != '', marcas.nombre_alias, marcas.nombre), marca) AS marca, cantidad AS valor, id_finca 
                    FROM produccion_cajas_guias_remision guia
                    INNER JOIN produccion_cajas_guias_remision_marca ON guia.id = id_guia_remision 
                    LEFT JOIN marcas_cajas marcas ON id_marca = marcas.id
                    WHERE fecha = '{$postdata->fecha}' AND id = '{$postdata->id}'";
            $response->marcas = $this->db->queryAll($sql);
        }
        return $response;
    }

    public function tags($filters){
        $response = [];
        $sWhere = "";
        /*if($filters->semana > 0){
            $sWhere = " AND semana = {$filters->semana}";
        }*/
        if($filters->finca > 0){
            $sWhere .= " AND id_finca = {$filters->finca}";
        }
        $rows = $this->db->queryAll("SELECT hora, fecha
                                    FROM(
                                        SELECT hora, fecha
                                        FROM produccion_cajas 
                                        WHERE fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}' AND hora != '' AND marca != '' $sWhere
                                    ) AS tbl
                                    ORDER BY hora");
        if(count($rows)>0){
            $p = $rows[0];
            $u = $rows[count($rows)-1];
            $response["primera_caja"] = $p->hora;
            $response["ultima_caja"] = $u->hora;
            $response["fecha_primera"] = $p->fecha;
            $response["fecha_ultima"] = $u->fecha;
            $response["diferencia"] = $this->db->queryRow("SELECT TIMEDIFF('{$u->hora}}',  '{$p->hora}') AS dif")->dif;
        }else{
            $response["primera_caja"] = "";
            $response["ultima_caja"] = "";
            $response["fecha_primera"] = "";
            $response["fecha_ultima"] = "";
        }
        return $response;
    }

    public function graficasBarras(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $filters = (object)[
            'grupos' => getValueFrom($postdata, 'grupos', [], FILTER_SANITIZE_PHRAPI_ARRAY),
            "marca" => getValueFrom($postdata, 'marca', ''),
            'fecha_inicial' => getValueFrom($postdata, 'fecha_inicial', getValueFrom($_GET, 'fecha_inicial', '')),
            'finca' => (int) getValueFrom($postdata, 'finca', getValueFrom($_GET, 'finca', '')),
        ];

        $id_marca = '';
        if($filters->marca != ''){
            $id_marca = $this->db->queryOne("SELECT id_marca FROM brands WHERE nombre = '{$filters->marca}'");
            if(!$id_marca > 0) $id_marca = '';
        }

        $requerimiento = $this->convertir('lb', $this->unidad, 'r.requerimiento');
        $max = $this->convertir('lb', $this->unidad, 'r.max');
        $min = $this->convertir('lb', $this->unidad, 'r.min');

        $sql = "SELECT 
                    IF(m.id IS NULL, marca, IF(m.nombre_alias != '', m.nombre_alias, m.nombre)) marca, 
                    id_marca,
                    ROUND({$requerimiento}, 2) AS requerimiento, 
                    ROUND({$max}, 2) AS maximo, 
                    ROUND({$min}, 2) AS minimo,
                    desviacion
                FROM (
                    SELECT id_marca, marca, 1 AS sec, ROUND(STD({$this->unidad}), 2) desviacion
                    FROM produccion_cajas 
                    WHERE fecha = '{$filters->fecha_inicial}' 
                        AND id_finca = $filters->finca 
                        AND IF('{$id_marca}' != '', id_marca = '{$id_marca}', TRUE)
                        AND id_marca IS NOT NULL
                    GROUP BY id_marca
                    UNION ALL
                    SELECT NULL, marca, 1 AS sec, ROUND(STD({$this->unidad}), 2) desviacion
                    FROM produccion_cajas 
                    WHERE fecha = '{$filters->fecha_inicial}' 
                        AND id_finca = $filters->finca 
                        AND IF('{$id_marca}' != '', id_marca = '{$id_marca}', TRUE)
                        AND id_marca IS NULL
                    GROUP BY marca
                ) AS tbl
                LEFT JOIN marcas_cajas m ON id_marca = m.id
                LEFT JOIN marcas_cajas_rangos r ON r.id = getRangoMarca(m.id, '{$filters->fecha_inicial}')";
        $response->marcas = $this->db->queryAll($sql);

        $response->graficas = [];
        $response->pasteles = [];
        foreach($response->marcas as $marca){
            # BARRAS
            $response->graficas[$marca->marca] = (object)[
                "legends" => [],
                "series" => [
                    "Cantidad" => [
                        "name" => "Cantidad",
                        "connectNulls" => true,
                        "type" => "bar",
                        "itemStyle" => [
                            "normal" => [
                                "barBorderRadius" => 0,
                                "barBorderWidth" => 6
                            ]
                        ],
                        "label" => [
                            "normal" => [
                                "show" => true,
                                "position" => "inside"
                            ]
                        ],
                        "data" => [],
                    ]
                ]
            ];

            $grupo = $marca->desviacion;
            if(isset($filters->grupos[$marca->marca])){
                $grupo = $filters->grupos[$marca->marca];
            }
            if($grupo == 0){
                $grupo = 1;
            }

            $sGroup = [];
            if($marca->minimo){
                $sGroup[] = "CEIL(peso/{$marca->minimo})";
            }
            if($marca->maximo){
                $sGroup[] = "CEIL(peso/{$marca->maximo})";
            }
            $sGroup = implode(",", $sGroup);
            if($sGroup){
                $sGroup = ",".$sGroup;
            }
            $marca_p = (object)[
                "max" => $marca->maximo ? $marca->maximo : 0,
                "min" => $marca->minimo ? $marca->minimo : 0,
            ];
            
            $sql = "SELECT
                        CONCAT(
                            '[',
                            IF({$marca_p->min} > 0 AND peso > {$marca_p->min} AND ((grupo*{$grupo})+0.01-{$grupo}) < {$marca_p->min}, 
                                {$marca_p->min}+0.01, 
                                IF({$marca_p->max} > 0 AND peso > {$marca_p->max} AND {$marca_p->max} BETWEEN ((grupo*{$grupo})+0.01-{$grupo}) AND (grupo*{$grupo}), 
                                    {$marca_p->max}+0.01, 
                                    (grupo*{$grupo})+0.01-{$grupo}
                                )
                            ),
                            ', ', 
                            IF({$marca_p->min} > 0 AND peso <= {$marca_p->min} AND {$marca_p->min} BETWEEN ((grupo*{$grupo})+0.01-{$grupo}) AND (grupo*{$grupo}),
                                {$marca_p->min},
                                IF({$marca_p->max} > 0 AND peso < {$marca_p->max} AND (grupo*{$grupo}) > {$marca_p->max}, 
                                    {$marca_p->max}, 
                                    grupo*{$grupo}
                                )
                            ),
                            ']'
                        ) id,
                        COUNT(1) AS label
                    FROM (
                        SELECT peso, FLOOR(peso/{$grupo})+1 grupo
                        FROM (
                            SELECT {$this->unidad} peso
                            FROM produccion_cajas
                            WHERE fecha = '{$filters->fecha_inicial}' AND id_finca = $filters->finca AND IF('{$marca->id_marca}' != '', id_marca = '{$marca->id_marca}', marca = '{$marca->marca}' )
                        ) tbl
                        ORDER BY grupo
                    ) tbl
                    GROUP BY grupo $sGroup";
                    
            $pesos = $this->db->queryAllSpecial($sql);
            
            foreach($pesos as $peso => $cantidad){
                $response->graficas[$marca->marca]->legends[] = $peso;
                $response->graficas[$marca->marca]->series["Cantidad"]["umbral"] = ["max" => $marca->maximo, "min" => $marca->minimo];
                $response->graficas[$marca->marca]->series["Cantidad"]["data"][] = $cantidad;
            }
            
            # PASTELES
            $response->pasteles[$marca->marca] = [];
            
            $cajas_registradas = $this->db->queryAllOne("SELECT id FROM marcas_cajas WHERE status = 'Activo'");
            if(in_array($marca->id_marca, $cajas_registradas) && ($marca->minimo > 0 && $marca->maximo > 0)){
                $sql = "SELECT value, label
                        FROM (
                            SELECT '{$marca->minimo}-{$marca->maximo}' AS label, COUNT(1) AS value
                            FROM produccion_cajas
                            WHERE id_marca = '{$marca->id_marca}' AND id_finca = $filters->finca AND fecha = '{$filters->fecha_inicial}' AND {$this->unidad} >= {$marca->minimo} AND {$this->unidad} <= {$marca->maximo}
                            UNION ALL
                            SELECT '> {$marca->maximo}' AS label, COUNT(1) AS value
                            FROM produccion_cajas
                            WHERE id_marca = '{$marca->id_marca}' AND id_finca = $filters->finca AND fecha = '{$filters->fecha_inicial}' AND {$this->unidad} > {$marca->maximo}
                            UNION ALL
                            SELECT '< {$marca->minimo}' AS label, COUNT(1) AS value
                            FROM produccion_cajas
                            WHERE id_marca = '{$marca->id_marca}' AND id_finca = $filters->finca AND fecha = '{$filters->fecha_inicial}' AND {$this->unidad} < {$marca->minimo}
                        ) AS tbl
                        HAVING value > 0";
                $response->pasteles[$marca->marca] = $this->db->queryAll($sql);
            }else{

                $peso_prom = $this->db->queryOne("SELECT ROUND(AVG(peso), 2) FROM (
                    SELECT AVG({$this->unidad}) AS peso FROM produccion_cajas WHERE marca = '{$marca->marca}' AND fecha = '{$filters->fecha_inicial}'
                ) AS tbl");
                if($peso_prom){
                    $sql = "SELECT value, label
                            FROM (
                                SELECT '{$peso_prom}' AS label, COUNT(1) AS value
                                FROM produccion_cajas
                                WHERE marca = '{$marca->marca}' AND id_finca = $filters->finca AND fecha = '{$filters->fecha_inicial}' AND {$this->unidad} = {$peso_prom}
                                UNION ALL
                                SELECT '> {$peso_prom}' AS label, COUNT(1) AS value
                                FROM produccion_cajas
                                WHERE marca = '{$marca->marca}' AND id_finca = $filters->finca AND fecha = '{$filters->fecha_inicial}' AND {$this->unidad} > {$peso_prom}
                                UNION ALL
                                SELECT '< {$peso_prom}' AS label, COUNT(1) AS value
                                FROM produccion_cajas
                                WHERE marca = '{$marca->marca}' AND id_finca = $filters->finca AND fecha = '{$filters->fecha_inicial}' AND {$this->unidad} < {$peso_prom}
                            ) AS tbl";
                    $response->pasteles[$marca->marca] = $this->db->queryAll($sql);
                }
            }
        }

        $response->rangos = [];
        foreach($response->marcas as $marca){
            $response->rangos[$marca->marca] = (float) isset($filters->grupos[$marca->marca]) ? $filters->grupos[$marca->marca] : $marca->desviacion;
        }
        return $response;
    }

    public function tablasDiferecias(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));

        $filters = (object)[
            'fecha_inicial' => getValueFrom($postdata, 'fecha_inicial', getValueFrom($_GET, 'fecha_inicial', '')),
            'fecha_final' => getValueFrom($postdata, 'fecha_final', getValueFrom($_GET, 'fecha_final', '')),
            'semana' => (int) getValueFrom($postdata, 'semana', 0),
            'unidad' => getValueFrom($postdata, 'unidad', 'lb'),
            "finca" => (int) getValueFrom($postdata, 'finca', getValueFrom($_GET, 'finca', 0)),
        ]; 
        $sWhere = "";
        $response = new stdClass;
        $response->tablas = [];
        $response->fincas = $this->db->queryAllSpecial("SELECT id_finca AS id, fincas.nombre AS label FROM produccion_cajas INNER JOIN fincas ON id_finca = fincas.id WHERE fecha = '{$filters->fecha_inicial}' GROUP BY id_finca");
        if(!isset($response->fincas[$filters->finca])){
            $filters->finca = array_keys($response->fincas)[0];
        }
        $sWhere .= " AND cajas.id_finca = $filters->finca";

        $sql = "SELECT
                    marca.id AS id_marca,
                    IF(marca.nombre_alias != '', marca.nombre_alias, marca.nombre) AS `marca`
                FROM produccion_cajas cajas
                INNER JOIN marcas_cajas marca ON marca.id = cajas.id_marca
                WHERE id_marca IS NOT NULL AND cajas.fecha = '{$filters->fecha_inicial}' $sWhere
                GROUP BY cajas.id_marca";
        $marcas = $this->db->queryAll($sql);
        foreach($marcas as $marca){
            $max = $this->convertir('lb', $this->unidad, 'max');
            $sql = "SELECT
                        ROUND({$max}, 2) max
                    FROM marcas_cajas_rangos
                    WHERE id = getRangoMarca($marca->id_marca, '{$filters->fecha_inicial}')";
            $rango = $this->db->queryRow($sql);
            if($rango->max > 0){
                $sql = "SELECT marca, ROUND(SUM({$this->unidad} - {$rango->max}), 2) AS kg_diff
                        FROM produccion_cajas cajas
                        WHERE id_marca = '{$marca->id_marca}' AND fecha = '{$filters->fecha_inicial}' AND {$this->unidad} > {$rango->max} $sWhere";
                $row = $this->db->queryRow($sql);
                $row->cajas = round($row->kg_diff / $this->convertidas_default, 2);
                $row->dolares = round($row->cajas * 6.2, 2);
                if($row->kg_diff > 0) $response->tablas[] = $row;
            }
        }

        return $response;
    }

    public function historicoExcedente(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $filters = (object)[
            'unidad' => getValueFrom($postdata, 'unidad', 'lb'),
        ];
        $response->data = [];
        $response->umbrales = new stdClass;

        $sql = "SELECT m.*, r.id AS id_rango, r.max
                FROM (
                    SELECT 
                        DISTINCT c.id_marca AS id,
                        IF(m.nombre_alias != '', m.nombre_alias, m.nombre) AS label
                    FROM produccion_cajas c
                    INNER JOIN marcas_cajas m ON c.id_marca = m.id
                    WHERE c.year = {$postdata->year}
                        AND c.id_finca = {$postdata->finca}
                ) m
                INNER JOIN marcas_cajas_rangos r ON r.id = getRangoMarca(m.id, '{$postdata->year}-12-31')
                WHERE r.max > 0";
        $marcas = $this->db->queryAll($sql);

        $sql = "SELECT DISTINCT c.semana as id, c.semana as label
                FROM produccion_cajas c
                INNER JOIN marcas_cajas m ON c.id_marca = m.id
                WHERE c.year = {$postdata->year} 
                    AND c.id_finca = $postdata->finca";
        $semanas = $this->db->queryAllSpecial($sql);

        $response->semanas = $semanas;

        $max_exce = []; $max_cajas = []; $max_dolares = [];
        $total = new stdClass;
        foreach($marcas as $marca){
            $row = new stdClass;
            $row->id_marca = $marca->id;
            $row->marca = $marca->label;
            $row->requerimiento = $marca->max;
            $row->cantidad = 0;

            foreach($semanas as $semana){
                $sql = "SELECT ROUND(SUM(IF(lb > {$row->requerimiento}, lb - {$row->requerimiento}, 0)), 2) 
                        FROM produccion_cajas 
                        WHERE YEAR = {$postdata->year} 
                            AND id_finca = $postdata->finca 
                            AND id_marca = '{$row->id_marca}' 
                            AND semana = {$semana}";
                $val = (float) $this->db->queryOne($sql);
                $formula = $this->convertir('lb', $this->unidad, 'valor');
                $val = (float) $this->db->queryOne("SELECT {$formula} FROM ( SELECT {$val} AS valor ) tbl");
                $row->cantidad += $val;

                if($val > 0){
                    $row->{"sem_exce_{$semana}"} = $val;
                    $total->{"sem_exce_{$semana}"} += $val;
                    $row->{"sem_cajas_{$semana}"} = round($val / $row->requerimiento, 2);
                    $total->{"sem_cajas_{$semana}"} += round($val / $row->requerimiento, 2);
                    $row->{"sem_dolares_{$semana}"} = round($row->{"sem_cajas_{$semana}"} * 6.2, 2);
                    $total->{"sem_dolares_{$semana}"} += round($row->{"sem_cajas_{$semana}"} * 6.2, 2);

                    if($val > 0) if(!isset($row->{"min_exce"}) || ($row->{"min_exce"} > $val)) $row->{"min_exce"} = $val;
                    if($val > 0) if(!isset($row->{"max_exce"}) || ($row->{"max_exce"} < $val)) $row->{"max_exce"} = $val;
                    $row->{"sum_exce"} += $val;
                    $total->{"sum_exce"} += $val;

                    if($val > 0) if((!isset($row->{"min_cajas"}) && $val > 0) || ($row->{"min_cajas"} > $row->{"sem_cajas_{$semana}"})) $row->{"min_cajas"} = $row->{"sem_cajas_{$semana}"};
                    if($val > 0) if((!isset($row->{"max_cajas"}) && $val > 0) || ($row->{"max_cajas"} < $row->{"sem_cajas_{$semana}"})) $row->{"max_cajas"} = $row->{"sem_cajas_{$semana}"};
                    $row->{"sum_cajas"} += $row->{"sem_cajas_{$semana}"};
                    $total->{"sum_cajas"} += $row->{"sem_cajas_{$semana}"};

                    if($val > 0) if(!isset($row->{"min_dolares"}) || ($row->{"min_dolares"} > $row->{"sem_dolares_{$semana}"})) $row->{"min_dolares"} = $row->{"sem_dolares_{$semana}"};
                    if($val > 0) if(!isset($row->{"max_dolares"}) || ($row->{"max_dolares"} < $row->{"sem_dolares_{$semana}"})) $row->{"max_dolares"} = $row->{"sem_dolares_{$semana}"};
                    $row->{"sum_dolares"} += $row->{"sem_dolares_{$semana}"};
                    $total->{"sum_dolares"} += $row->{"sem_dolares_{$semana}"};

                    if($val > 0) $row->{"count"} += 1;
                }
            }

            if($row->cantidad > 0){
                $row->{"avg_exce"} = round($row->{"sum_exce"} / $row->{"count"}, 2);
                $row->{"avg_cajas"} = round($row->{"sum_cajas"} / $row->{"count"}, 2);
                $row->{"avg_dolares"} = round($row->{"sum_dolares"} / $row->{"count"}, 2);

                $response->data[] = $row;
            }
        }
        $total->marca = 'TOTAL';
        foreach($total as $key => $value){
            if(strpos($key, "sem_exce") !== false){
                if($value > 0) if(!isset($total->{"max_exce"}) || $total->{"max_exce"} < $value) $total->{"max_exce"} = $value;
                if($value > 0) if(!isset($total->{"min_exce"}) || $total->{"min_exce"} > $value) $total->{"min_exce"} = $value;
            }
            if(strpos($key, "sem_cajas") !== false){
                if($value > 0) if(!isset($total->{"max_cajas"}) || $total->{"max_cajas"} < $value) $total->{"max_cajas"} = $value;
                if($value > 0) if(!isset($total->{"min_cajas"}) || $total->{"min_cajas"} > $value) $total->{"min_cajas"} = $value;
            }
            if(strpos($key, "sem_dolares") !== false){
                if($value > 0) if(!isset($total->{"max_dolares"}) || $total->{"max_dolares"} < $value) $total->{"max_dolares"} = $value;
                if($value > 0) if(!isset($total->{"min_dolares"}) || $total->{"min_dolares"} > $value) $total->{"min_dolares"} = $value;
            }
        }
        $total->{"avg_exce"} = round($total->{"sum_exce"} / count($semanas), 2);
        $total->{"avg_cajas"} = round($total->{"sum_cajas"} / count($semanas), 2);
        $total->{"avg_dolares"} = round($total->{"sum_dolares"} / count($semanas), 2);
        $response->data[] = $total;
        return $response;
    }

    public function graficaEficiencia(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));


        $sql = "SELECT 
                    CONCAT(semanas.anio, ' ', semanas.semana) label_x,
                    0 AS index_y,
                    vars.variable AS 'name',
                    100 - IFNULL(datos.value, 0) AS 'value'
                FROM (
                    SELECT YEAR AS anio, semana
                    FROM produccion_cajas
                    WHERE year = YEAR('{$postdata->fecha_inicial}')
                    GROUP BY YEAR, semana
                ) semanas
                JOIN (
                    SELECT '% EFICIENCIA CUADRE DE CAJAS' AS variable
                    UNION ALL
                    SELECT '% EFICIENCIA DE PESO' AS variable
                    UNION ALL
                    SELECT '% EFICIENCIA TOTAL' AS variable
                ) vars
                LEFT JOIN (
                    SELECT 
                        anio, semana,
                        ROUND(cantidad/(SELECT COUNT(1) FROM produccion_cajas WHERE YEAR = tbl.anio AND semana = tbl.semana)*100, 2) AS 'value', 
                        variable
                    FROM(
                        SELECT YEAR anio, semana, COUNT(1) cantidad, '% EFICIENCIA DE PESO' variable
                        FROM produccion_cajas
                        WHERE peso_corregido = 1 AND year = YEAR('{$postdata->fecha_inicial}')
                        GROUP BY anio, semana
                    ) AS tbl
                    WHERE cantidad > 0
                    UNION ALL
                    SELECT 
                        anio, semana,
                        ROUND(cantidad/(SELECT COUNT(1) FROM produccion_cajas WHERE YEAR = tbl.anio AND semana = tbl.semana)*100, 2) AS 'value', 
                        variable
                    FROM(
                        SELECT YEAR anio, semana, COUNT(1) cantidad, '% EFICIENCIA CUADRE DE CAJAS' variable
                        FROM produccion_cajas
                        WHERE tipo = 'cuadre' AND year = YEAR('{$postdata->fecha_inicial}')
                        GROUP BY anio, semana
                    ) AS tbl
                    WHERE cantidad > 0
                    UNION ALL
                    SELECT 
                        anio, semana,
                        ROUND(cantidad/(SELECT COUNT(1) FROM produccion_cajas WHERE YEAR = tbl.anio AND semana = tbl.semana)*100, 2) AS 'value', 
                        variable
                    FROM(
                        SELECT YEAR anio, semana, COUNT(1) cantidad, '% EFICIENCIA TOTAL' variable
                        FROM produccion_cajas
                        WHERE (tipo = 'cuadre' OR peso_corregido = 1) AND year = YEAR('{$postdata->fecha_inicial}')
                        GROUP BY anio, semana
                    ) AS tbl
                    WHERE cantidad > 0
                ) datos ON semanas.anio = datos.anio AND semanas.semana = datos.semana AND vars.variable = datos.variable
                ORDER BY semanas.anio, semanas.semana";
        $data_chart = $this->db->queryAll($sql);

        $groups = [
            [
                "name" => '%',
                "type" => 'line',
                'format' => ''
            ]
        ];
        $response->chart = $this->grafica_z($data_chart, $groups);

        return $response;
    }

    /* BEGIN CUADRE DE CAJAS */
    public function cuadreCajas(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));
        
        $fincas = $this->db->queryAllOne("SELECT id_finca FROM produccion_cajas cajas INNER JOIN fincas ON id_finca = fincas.id WHERE cajas.fecha = '{$postdata->fecha_inicial}' AND id_finca != '' GROUP BY id_finca");
        if($postdata->finca != ''){
            if(!in_array($postdata->finca, $fincas)){
                $postdata->finca = $fincas[0];
            }
        }

        $sWhere = "";
        if($postdata->finca != ''){
            $sWhere .= " AND id_finca = $postdata->finca";
        }

        // SUMA MARCA
        $sql = "SELECT *,
                    (SELECT SUM(cantidad) 
                        FROM produccion_cajas_guias_remision guia
                        INNER JOIN produccion_cajas_guias_remision_marca ON guia.id = id_guia_remision
                        WHERE fecha = '{$postdata->fecha_inicial}'
                            AND id_finca = {$postdata->finca}
                            AND IF(
                                tbl.id_marca IS NOT NULL,
                                id_marca = tbl.id_marca,
                                marca = tbl.marca
                            )
                    ) AS suma_real,
                    IFNULL((SELECT SUM(cantidad) FROM produccion_cajas_pendiente_movimientos WHERE fecha = '{$postdata->fecha_inicial}' AND marca = tbl.marca AND id_finca = {$postdata->finca} AND tipo = 'PENDIENTE'), 0) pendiente,
                    IFNULL((SELECT SUM(cantidad) FROM produccion_cajas_pendiente_movimientos WHERE fecha = '{$postdata->fecha_inicial}' AND marca = tbl.marca AND id_finca = {$postdata->finca} AND tipo = 'ASIGNADA'), 0) asignada
                FROM (
                    SELECT marcas.tipo AS tipo, id_marca, IF(marcas.nombre_alias != '', marcas.nombre_alias, marcas.nombre) marca, id_finca, COUNT(1) AS balanza
                    FROM produccion_cajas 
                    INNER JOIN marcas_cajas marcas ON marcas.id = id_marca
                    WHERE fecha = '{$postdata->fecha_inicial}' $sWhere AND id_marca IS NOT NULL
                    GROUP BY id_marca

                    UNION ALL

                    SELECT 'CAJA' AS tipo, NULL id_marca, produccion_cajas.marca, id_finca, COUNT(1) AS balanza
                    FROM produccion_cajas 
                    WHERE fecha = '{$postdata->fecha_inicial}' $sWhere AND id_marca IS NULL
                    GROUP BY marca

                    UNION ALL

                    SELECT 'CAJA' AS tipo, NULL AS id_marca, marca, id_finca, 0 AS balanza
                    FROM produccion_cajas_guias_remision vreal
                    INNER JOIN produccion_cajas_guias_remision_marca marcas ON vreal.id = id_guia_remision
                    WHERE fecha = '{$postdata->fecha_inicial}' $sWhere
                        AND NOT EXISTS (SELECT * FROM produccion_cajas WHERE fecha = '{$postdata->fecha_inicial}' $sWhere AND marca = marcas.marca)
                        AND id_marca IS NULL
                    GROUP BY marca

                    UNION ALL

                    SELECT marcas.tipo, id_marca, IF(marcas.nombre_alias != '', marcas.nombre_alias, marcas.nombre) AS marca, id_finca, 0 AS balanza
                    FROM produccion_cajas_guias_remision vreal
                    INNER JOIN produccion_cajas_guias_remision_marca ON vreal.id = id_guia_remision
                    INNER JOIN marcas_cajas marcas ON marcas.id = id_marca
                    WHERE fecha = '{$postdata->fecha_inicial}' $sWhere
                        AND NOT EXISTS (SELECT * FROM produccion_cajas WHERE fecha = '{$postdata->fecha_inicial}' $sWhere AND id_marca = marcas.id)
                        AND id_marca IS NOT NULL
                    GROUP BY marca

                    UNION ALL

                    SELECT 'CAJA' AS tipo, NULL AS id_marca, marca, id_finca, 0 AS balanza
                    FROM produccion_cajas_pendiente_movimientos
                    WHERE fecha = '{$postdata->fecha_inicial}' AND id_finca = '{$postdata->finca}'
                    GROUP BY marca
                ) AS tbl
                GROUP BY marca";
                
        $response->data = $this->db->queryAll($sql);
        foreach($response->data as $row){
            if($row->balanza > 0 || $row->suma_real > 0)
                $row->porcentaje = $row->balanza / ($row->suma_real ? $row->suma_real : 1) * 100;
        }

        // GUIAS
        $sql = "SELECT guia.id, SUM(cantidad) AS cajas, guia, fecha, codigo_productor, codigo_magap, sello_seguridad, id_finca, numero_contenedor
                FROM produccion_cajas_guias_remision guia
                INNER JOIN produccion_cajas_guias_remision_marca marca ON marca.id_guia_remision = guia.id
                WHERE fecha = '{$postdata->fecha_inicial}' $sWhere
                GROUP BY guia.id";
        $response->guias = $this->db->queryAll($sql);
        foreach($response->guias as $row){
            $sql = "SELECT id_marca, marca, cantidad AS valor 
                    FROM produccion_cajas_guias_remision_marca 
                    WHERE id_guia_remision = '{$row->id}'";
            $row->detalle = $this->db->queryAll($sql);
        }

        // SALDOS POR ASIGNAR A OTRA FINCA
        $sql = "SELECT
                    marca, finca, id_finca,
                    (pendiente-IFNULL((SELECT SUM(cantidad) FROM produccion_cajas_pendiente_movimientos WHERE tipo = 'ASIGNADA' AND id_finca_origen = tbl.id_finca AND marca = tbl.marca),0)) pendiente
                FROM (
                    SELECT 
                        marca, 
                        SUM(cantidad) pendiente, 
                        (SELECT nombre FROM fincas WHERE id_finca = fincas.id) finca,
                        id_finca
                    FROM produccion_cajas_pendiente_movimientos
                    WHERE tipo = 'PENDIENTE'
                    GROUP BY id_finca, marca
                ) tbl
                HAVING pendiente > 0";
        $response->saldos = $this->db->queryAll($sql);

        return $response;
    }

    public function guardarCuadrar(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $response->status = 400;
        $response->message = "Algo inesperado paso, contacte a soporte";

        if(isset($postdata->fecha) && $postdata->fecha != "" && $postdata->finca != ''){
            $response->status = 200;
            if(!$postdata->id > 0){
                $sql = "INSERT INTO produccion_cajas_guias_remision SET
                            id_finca = '{$postdata->finca}',
                            fecha = '{$postdata->fecha}',
                            guia = '{$postdata->guia}',
                            codigo_productor = '{$postdata->productor}', 
                            codigo_magap = '{$postdata->magap}', 
                            numero_contenedor = '{$postdata->numero_contenedor}',
                            sello_seguridad = '{$postdata->sello_seguridad}'";
                $this->db->query($sql);
                $postdata->id = $this->db->getLastID();
            }

            // BORRAR LAS MARCAS QUE YA NO ESTAN EN LA LISTA
            $sql = "SELECT *
                    FROM produccion_cajas_guias_remision_marca
                    WHERE id_guia_remision = {$postdata->id}";
            $marcas = $this->db->queryAll($sql);
            foreach($marcas as $m){
                $e = false;
                foreach($postdata->marca as $id_marca => $value){
                    if($m->id_marca == $id_marca){
                        $e = true;
                        break;
                    }
                }
                if(!$e){
                    $sql = "DELETE FROM produccion_cajas_guias_remision_marca WHERE id_guia_remision = {$postdata->id} AND id_marca = {$m->id_marca}";
                }
            }

            // INSERTAR MARCAS NUEVAS O MODIFICAR
            foreach($postdata->marca as $marca => $value){
                if($marca != '' && $value > 0){
                    $e = (int) $this->db->queryOne("SELECT COUNT(1) FROM produccion_cajas_guias_remision_marca WHERE id_guia_remision = {$postdata->id} AND id_marca = {$marca}");
                    if($e == 0){
                        $sql = "INSERT INTO produccion_cajas_guias_remision_marca SET 
                                    id_guia_remision = {$postdata->id},
                                    cantidad = '{$value}', 
                                    id_marca = '{$marca}', 
                                    marca = (SELECT IF(nombre_alias != '', nombre_alias, nombre) FROM marcas_cajas WHERE id = {$marca})";
                        $this->db->query($sql);
                    }else{
                        $sql = "UPDATE produccion_cajas_guias_remision_marca SET 
                                    cantidad = '{$value}'
                                WHERE id_guia_remision = {$postdata->id} AND id_marca = '{$marca}'";
                        $this->db->query($sql);
                    }
                }
            }
            $response->message = "Se inserto con éxito";
        }else{
            error_log("Datos vacios");
            $response->message = "Datos vacios";
        }
        return $response;
    }

    public function procesar(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));

        $marcas = $this->db->queryAll(" SELECT id_marca, marca, SUM(cantidad) AS cantidad, id_finca, 
                                            (SELECT SUM(cantidad) FROM produccion_cajas_pendiente_movimientos WHERE fecha = '{$postdata->fecha}' AND id_finca = guia.id_finca AND tipo = 'PENDIENTE') pendientes,
                                            (SELECT SUM(cantidad) FROM produccion_cajas_pendiente_movimientos WHERE fecha = '{$postdata->fecha}' AND id_finca = guia.id_finca AND tipo = 'ASIGNADA') asignadas
                                        FROM produccion_cajas_guias_remision guia
                                        INNER JOIN produccion_cajas_guias_remision_marca ON guia.id = id_guia_remision
                                        WHERE fecha = '{$postdata->fecha}' AND id_finca = {$postdata->id_finca}
                                        GROUP BY id_marca, marca, id_finca");
        foreach($marcas as $mr){
            $sql = "SELECT 
                        marca,
                        SUM(cantidad) cantidad, 
                        IF(id_marca IS NOT NULL AND umbral_minimo > 0,
                            (SELECT AVG(lb) FROM produccion_cajas WHERE id_marca = tbl.id_marca AND fecha = '{$postdata->fecha}' AND lb > umbral_minimo), 
                            promedio
                        ) peso
                    FROM(
                        SELECT
                            marca.id AS id_marca,
                            IF(marca.nombre_alias != '', marca.nombre_alias, marca.nombre) AS `marca`,
                            COUNT(1) AS `cantidad`,
                            ROUND(AVG(cajas.lb), 2) AS promedio
                        FROM produccion_cajas cajas
                        INNER JOIN marcas_cajas marca ON marca.id = cajas.id_marca
                        WHERE id_marca IS NOT NULL AND cajas.fecha = '{$postdata->fecha}' AND id_finca = $mr->id_finca AND id_marca = {$mr->id_marca} AND id_marca IS NOT NULL
                        GROUP BY cajas.id_marca
                        UNION ALL
                        SELECT
                            NULL AS id_marca,
                            marca,
                            COUNT(1) AS `cantidad`,
                            ROUND(AVG(cajas.`lb`), 2) AS promedio
                        FROM produccion_cajas cajas
                        WHERE id_marca IS NULL AND cajas.fecha = '{$postdata->fecha}' AND id_finca = $mr->id_finca AND marca = '{$mr->marca}' AND id_marca IS NULL
                        GROUP BY marca
                    ) AS tbl
                    LEFT JOIN marcas_cajas_rangos rango ON rango.id = getRangoMarca(id_marca, '{$postdata->fecha}')";
            $blz = $this->db->queryRow($sql);
            
            if($blz->cantidad < ($mr->cantidad + $mr->pendientes - $mr->asignadas)){
                //INSERTAR DIFERENCIA
                $diff = $mr->cantidad - $blz->cantidad;
                if(!$blz->peso > 0){
                    if($mr->id_marca > 0){
                        $last_fecha = $this->db->queryOne("SELECT MAX(fecha) FROM produccion_cajas WHERE fecha < '{$postdata->fecha}' AND id_finca = $mr->id_finca AND id_marca = {$mr->id_marca} AND id_marca IS NOT NULL");
                        $blz->peso = $this->db->queryOne("SELECT AVG(lb) FROM produccion_cajas WHERE fecha = '{$last_fecha}' AND id_finca = $mr->id_finca AND id_marca = {$mr->id_marca} AND id_marca IS NOT NULL");
                    }else{
                        $last_fecha = $this->db->queryOne("SELECT MAX(fecha) FROM produccion_cajas WHERE fecha < '{$postdata->fecha}' AND id_finca = $mr->id_finca AND marca = '{$mr->marca}' AND id_marca IS NULL");
                        $blz->peso = $this->db->queryOne("SELECT AVG(lb) FROM produccion_cajas WHERE fecha = '{$last_fecha}' AND id_finca = $mr->id_finca AND marca = '{$mr->marca}' AND id_marca IS NULL");
                    }
                }
                for($x = 0; $x < $diff; $x++){
                    $kg = $this->convertir('lb', 'kg', $blz->peso);
                    $sql = "INSERT INTO produccion_cajas SET
                                fecha = '{$postdata->fecha}',
                                semana = getWeek('{$postdata->fecha}'),
                                year = getYear('{$postdata->fecha}'),
                                id_marca = '{$mr->id_marca}',
                                marca = '{$mr->marca}',
                                lb = {$blz->peso},
                                kg = {$kg},
                                convertidas_415 = ROUND({$blz->peso} / 41.5, 2),
                                convertidas_custom = ROUND({$blz->peso} / {$this->convertidas_default}, 2),
                                tipo = 'cuadre',
                                id_finca = $mr->id_finca,
                                updated_at = CURRENT_TIMESTAMP";
                    $this->db->query($sql);
                }
            }
        }
        return $response;
    }

    public function borrarGuiaMarca(){
        $postdata = (object)json_decode(file_get_contents("php://input"));

        if($postdata->id > 0 && $postdata->id_marca){
            $sql = "DELETE FROM produccion_cajas_guias_remision_marca WHERE id_guia_remision = {$postdata->id} AND id_marca = {$postdata->id_marca}";
            $this->db->query($sql);

            $sql = "SELECT COUNT(1) FROM produccion_cajas_guias_remision_marca WHERE id_guia_remision = {$postdata->id}";
            $e = (int) $this->db->queryOne($sql);
            if($e == 0){
                $sql = "DELETE FROM produccion_cajas_guias_remision WHERE id = {$postdata->id}";
                $this->db->query($sql);
            }
            return true;
        }
        return false;
    }

    public function guardarComentarios(){
        $response = new stdClass;
        $response->status = 200;
        $postdata = (object)json_decode(file_get_contents("php://input"));

        $e = (int) $this->db->queryOne("SELECT COUNT(1) FROM produccion_cajas_comentarios");
        if($e > 0){
            $sql = "UPDATE produccion_cajas_comentarios SET 
                        comentarios = '{$postdata->comentarios}' 
                    WHERE fecha = '{$postdata->fecha_inicial}' AND id_finca = {$postdata->id_finca}";
            $response->data = $this->db->query($sql);
        }else{
            $sql = "INSERT INTO produccion_cajas_comentarios SET 
                        comentarios = '{$postdata->comentarios}',
                        fecha = '{$postdata->fecha_inicial}', 
                        id_finca = {$postdata->id_finca}";
            $response->data = $this->db->query($sql);
        }

        return $response;
    }

    // PENDIENTES SALDOS

    public function movimientos(){
        $response = new stdClass;
        $response->status = 200;
        $postdata = (object)json_decode(file_get_contents("php://input"));

        $sql = "SELECT *
                FROM(
                    SELECT timestamp, fincas.nombre as finca, cantidad, tipo, '' finca_destino, marca
                    FROM produccion_cajas_pendiente_movimientos
                    INNER JOIN fincas ON id_finca = fincas.id
                    WHERE fecha = '{$postdata->fecha_inicial}' AND tipo = 'PENDIENTE'
                    UNION ALL
                    SELECT timestamp, fincas.nombre as finca, cantidad, tipo, (SELECT nombre FROM fincas WHERE id = id_finca) finca_destino, marca
                    FROM produccion_cajas_pendiente_movimientos
                    INNER JOIN fincas ON id_finca_origen = fincas.id
                    WHERE fecha = '{$postdata->fecha_inicial}' AND tipo = 'ASIGNADA'
                ) tbl
                ORDER BY timestamp";
        $response->data = $this->db->queryAll($sql);

        $response->comentarios = $this->db->queryOne("SELECT comentarios FROM produccion_cajas_comentarios WHERE fecha = '{$postdata->fecha_inicial}' AND id_finca = {$postdata->finca}");

        return $response;
    }

    public function editGuia(){
        $response = new stdClass;
        $response->status = 200;
        $postdata = (object)json_decode(file_get_contents("php://input"));

        $sql = "SELECT COUNT(1) 
                FROM produccion_cajas_pendiente_movimientos 
                WHERE fecha = '{$postdata->fecha}' 
                    AND marca = '{$postdata->marca}' 
                    AND id_finca = '{$postdata->id_finca}' 
                    AND tipo = 'PENDIENTE'";
        $e = (int) $this->db->queryOne($sql);
        if($e > 0){
            $sql = "UPDATE produccion_cajas_pendiente_movimientos 
                    SET
                        cantidad = '{$postdata->pendiente}'
                    WHERE fecha = '{$postdata->fecha}' AND marca = '{$postdata->marca}' AND tipo = 'PENDIENTE' AND id_finca = '{$postdata->id_finca}'";
            $this->db->query($sql);
        }else{
            $sql = "INSERT INTO produccion_cajas_pendiente_movimientos 
                    SET
                        fecha = '{$postdata->fecha}',
                        cantidad = '{$postdata->pendiente}',
                        marca = '{$postdata->marca}',
                        id_finca = '{$postdata->id_finca}'";
            $this->db->query($sql);
        }

        return $response;
    }

    public function asignarGuia(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $response->status = 200;

        $stock = (int) $this->db->queryOne("SELECT getStockCaja($postdata->id_finca_origen, '{$postdata->marca}')");
        if($stock >= $postdata->cantidad){
            $sql = "INSERT INTO produccion_cajas_pendiente_movimientos SET
                        fecha = '{$postdata->fecha}',
                        id_finca = {$postdata->finca},
                        tipo = 'ASIGNADA',
                        cantidad = {$postdata->cantidad},
                        marca = '{$postdata->marca}',
                        id_finca_origen = '{$postdata->id_finca_origen}'";
            $this->db->query($sql);
        }else{
            $response->status = 400;
            $response->message = "No hay stock";
            $response->stock = $stock;
        }

        return $response;
    }
    /* END CUADRE DE CAJAS */

    private function grafica_z($data = [], $group_y = []){
        $options = [];
        $options["tooltip"] = [
            "trigger" => 'axis',
            "axisPointer" => [
                "type" => 'cross',
                "crossStyle" => [
                    "color" => '#999'
                ]
            ]
        ];
        $options["toolbox"] = [
            "feature" => [
                "dataView" => [
                    "show" => true,
                    "readOnly" => false
                ],
                "magicType" => [
                    "show" => true,
                    "type" => ['line', 'bar']
                ],
                "restore" => [
                    "show" => true
                ],
                "saveAsImage" => [
                    "show" => true
                ]
            ]
        ];
        $options["legend"]["data"] = [];
        $options["legend"]["bottom"] = "0%";
        $options["legend"]["left"] = "center";
        $options["xAxis"] = [
            [
                "type" => 'category',
                "data" => [],
                "axisPointer" => [
                    "type" => 'shadow'
                ]
            ]
        ];
        /*
            [
                type => 'value',
                name => {String},
                min => 0,
                max => 200,
                interval => 5,
                axisLabel => [
                    formatter => {value} KG
                ]
            ]
        */
        $options["yAxis"] = [];
        /*
            [
                name => {String},
                type => 'line',
                data => [
                    {double}, {double}, {double}
                ]
            ]
        */
        $options["series"] = [];

        $maxs = [];
        $mins = [];
        $prepare_data = [];
        $_x = [];
        $_names = [];
        $_namess = [];
        foreach($data as $d){
            $d = (object) $d;
            if(!isset($maxs[$d->index_y])) if($d->value > 0)
                $maxs[$d->index_y] = $d->value;
            if($d->value > $maxs[$d->index_y]) if($d->value > 0)
                $maxs[$d->index_y] = $d->value;

            if(!isset($mins[$d->index_y])) if($d->value > 0)
                $mins[$d->index_y] = $d->value;
            if($d->value < $mins[$d->index_y]) if($d->value > 0)
                $mins[$d->index_y] = $d->value;

            if(!in_array($d->label_x, $_x)){
                $_x[] = $d->label_x;
            }
            if(!in_array($d->name, $_namess)){
                $_namess[] = $d->name;
                 
                $n = ["name" => $d->name, "group" => $d->index_y];
                if(isset($d->line)){
                    $n["line"] = $d->line;
                }
                $_names[] = $n;
            }
            $prepare_data[$d->label_x][$d->name] = $d->value;
        }

        foreach($group_y as $key => $col){
            $col = (object) $col;
            $options["yAxis"][] = [
                'type' => 'value',
                'name' => $col->name,
                'min' => 'dataMin',
                'axisLabel' => [
                    'formatter' => "{value} $col->format"
                ]
            ];
        }

        foreach($_x as $row){
            $options["xAxis"][0]["data"][] = $row;
        }

        foreach($_names as $name){
            $name = (object) $name;

            if(!in_array($name->name, $options["legend"]["data"]))
                $options["legend"]["data"][] = $name->name;

            $serie = [
                "name" => $name->name,
                "type" => 'line',
                "connectNulls" => true,
                "data" => []
            ];
            if($name->group > 0)
                $serie["yAxisIndex"] = $name->group;

            if(isset($name->line)){
                $serie["itemStyle"]["normal"]["lineStyle"]["width"] = 5;
            }

            foreach($_x as $row){
                $val = 0;
                if(isset($prepare_data[$row][$name->name]))
                    $val = $prepare_data[$row][$name->name];

                if($val > 0)
                    $serie["data"][] = $val;
                else
                    $serie["data"][] = null;
            }
            $options["series"][] = $serie;
        }

        return $options;
    }
}
