<?php defined('PHRAPI') or die("Direct access not allowed!");

class ProduccionComparacionClientes {
	public $name;
	private $db;
	private $config;

	public function __construct(){
		$this->config = $GLOBALS['config'];
		$this->session = Session::getInstance();
		
		$this->db = DB::getInstance();
        $this->marcel = DB::getInstance('marcel');
        $this->marun = DB::getInstance('marun');
        $this->quintana = DB::getInstance('quintana');
        $this->reiset = DB::getInstance('reiset');
	}

    private function params(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
		$filters = (object)[
            'year' => 2018,
            'var1' => getValueFrom($postdata, 'var1', ''),
            'var2' => getValueFrom($postdata, 'var2', ''),
            'type1' => getValueFrom($postdata, 'type1', ''),
			'type2' => getValueFrom($postdata, 'type2', ''),
			'por_hectareas' => getValueFrom($postdata, 'por_hectareas', ''),
			'por_semana' => getValueFrom($postdata, 'por_semana', 0),
        ];
        return $filters;
	}
	
	private function getSemanasVariable($var, $year){
		$sql = [];
		for($i = 1; $i <= 53; $i++){
			$sql[] = "SELECT $i AS semana, AVG(sem_{$i}) cantidad FROM produccion_resumen_tabla WHERE sem_{$i} > 0 AND variable = '$var' AND anio = $year";
		}
		return implode("
		UNION ALL
		", $sql);
	}

    public function graficaVariables(){
        $response = new stdClass;
        $filters = $this->params();
        $sWhere = "";

        $variables = [
            "Peso (lb)" => "SELECT semana AS label_x, ROUND(AVG(peso), 2) AS value, 0 AS index_y, '{cliente-finca}' AS name
                        FROM produccion_historica
                        WHERE YEAR = {$filters->year} {$sWhere}
                        GROUP BY semana
                        ORDER BY semana",
            "Manos" => "SELECT semana AS label_x, ROUND(AVG(manos), 2) AS value, 0 AS index_y, '{cliente-finca}' AS name
                        FROM produccion_historica
                        WHERE YEAR = {$filters->year} {$sWhere}
                        GROUP BY semana
                        ORDER BY semana",
            "Calibre" => "SELECT semana AS label_x, ROUND(AVG(calibre), 2) AS value, 0 AS index_y, '{cliente-finca}' AS name
                            FROM produccion_historica
                            WHERE YEAR = {$filters->year} AND calibre > 0 {$sWhere}
                            GROUP BY semana
                            ORDER BY semana",
            "Edad" => "SELECT semana AS label_x, ROUND(AVG(edad), 2) AS value, 0 AS index_y, '{cliente-finca}' AS name
                        FROM produccion_historica
                        WHERE YEAR = {$filters->year} AND edad > 0 {$sWhere}
                        GROUP BY semana
                        ORDER BY semana",
            "CAJAS/HA" => "SELECT semana AS label_x, ROUND(SUM(cantidad / has / semanas), 2) AS 'value', 0 AS index_y, '{cliente-finca}' AS 'name'
				FROM (
					SELECT semana, SUM(cantidad) cantidad, COUNT(DISTINCT semana) semanas, (SELECT SUM(hectareas) FROM fincas) has
					FROM (
						SELECT semana, marca, ROUND(SUM(convertidas_415), 0) AS cantidad
						FROM produccion_cajas
						WHERE YEAR = {$filters->year}
						GROUP BY semana, fecha, marca
						UNION ALL
						SELECT semana, marca, ROUND(SUM(convertidas_415), 0) AS cantidad
						FROM produccion_gavetas
						WHERE YEAR = {$filters->year}
						GROUP BY semana, fecha, marca
					) tbl
					GROUP BY semana
				) AS tbl
				GROUP BY semana
				ORDER BY semana",
			"RAC PROC/HA" => "SELECT semana AS label_x, ROUND(SUM(cantidad / has), 2) AS 'value', 0 AS index_y, '{cliente-finca}' AS 'name'
				FROM (
					SELECT semana, SUM(cantidad) cantidad, (SELECT SUM(hectareas) FROM fincas) has
					FROM (
						SELECT semana, lote, ROUND(COUNT(1), 0) AS cantidad
						FROM produccion_historica
						WHERE YEAR = {$filters->year} AND tipo = 'PROC'
						GROUP BY semana, lote
					) tbl
					GROUP BY semana
				) tbl
				GROUP BY semana
				ORDER BY semana",
			"RAC CORT/HA" => "SELECT semana AS label_x, ROUND(SUM(cantidad / has), 2) AS 'value', 0 AS index_y, '{cliente-finca}' AS 'name'
				FROM (
					SELECT semana, SUM(cantidad) cantidad, (SELECT SUM(hectareas) FROM fincas) has
					FROM (
						SELECT semana, lote, ROUND(COUNT(1), 0) AS cantidad
						FROM produccion_historica
						WHERE YEAR = {$filters->year} AND tipo = 'PROC'
						GROUP BY semana, lote
					) tbl
					WHERE cantidad > 0
					GROUP BY semana
				) tbl
				GROUP BY semana
				ORDER BY semana",
			"RATIO CORTADO" => "SELECT semana as label_x, ROUND(AVG(cantidad), 2) AS 'value', 0 AS index_y, '{cliente-finca}' AS 'name'
				FROM (
					{$this->getSemanasVariable('RATIO CORTADO', $filters->year)}
				) tbl
				WHERE cantidad > 0
				GROUP BY semana
				ORDER BY semana",
			"RATIO PROCESADO" => "SELECT semana as label_x, ROUND(AVG(cantidad), 2) AS 'value', 0 AS index_y, '{cliente-finca}' AS 'name'
				FROM (
					{$this->getSemanasVariable('RATIO PROCESADO', $filters->year)}
				) tbl
				WHERE cantidad > 0
				GROUP BY semana
				ORDER BY semana",
			"MERMA CORTADA" => "SELECT semana as label_x, ROUND(AVG(cantidad), 2) AS 'value', 0 AS index_y, '{cliente-finca}' AS 'name'
				FROM (
					{$this->getSemanasVariable('MERMA CORTADA', $filters->year)}
				) tbl
				WHERE cantidad > 0
				GROUP BY semana
				ORDER BY semana",
			"MERMA PROCESADA" => "SELECT semana as label_x, ROUND(AVG(cantidad), 2) AS 'value', 0 AS index_y, '{cliente-finca}' AS 'name'
				FROM (
					{$this->getSemanasVariable('MERMA PROCESADA', $filters->year)}
				) tbl
				WHERE cantidad > 0
				GROUP BY semana
				ORDER BY semana",
        ];
        
		$data_chart = [];
        // VAR 1
		$sql = $variables[$filters->var1];
		$sql = str_replace("{cliente-finca}", "EL ORO", $sql);
		if($filters->var1 == 'Peso (lb)'){
			$sql = str_replace("AVG(peso)", "AVG(peso * 2.2)", $sql);
		}
		$data_chart = array_merge($data_chart, $this->marcel->queryAll($sql));
		
        $sql = $variables[$filters->var1];
		$sql = str_replace("{cliente-finca}", "LOS RIOS NORTE", $sql);
		if($filters->var1 == 'Peso (lb)'){
			$sql = str_replace("AVG(peso)", "IF(semana = 12, 90, AVG(peso))", $sql);
		}
		$data_chart = array_merge($data_chart, $this->quintana->queryAll($sql));
		
        $sql = $variables[$filters->var1];
		$sql = str_replace("{cliente-finca}", "GUAYAS", $sql);
		$sql = str_replace("produccion_historica", "produccion_racimos", $sql);
		if($filters->var1 == 'Peso (lb)'){
			$sql = str_replace("AVG(peso)", "AVG(peso * 2.2)", $sql);
		}
		$data_chart = array_merge($data_chart, $this->marun->queryAll($sql));
		
        $sql = $variables[$filters->var1];
		$sql = str_replace("{cliente-finca}", "LOS RIOS SUR", $sql);
		$sql = str_replace("produccion_historica", "produccion_racimos", $sql);
		$data_chart = array_merge($data_chart, $this->reiset->queryAll($sql));

		$semanas = [];
		$semanas_values = [];
		foreach ($data_chart as $key => $row)
        {
			if(!in_array($row->label_x, $semanas)) $semanas[] = (int) $row->label_x;
			$semanas_values[$row->label_x]["value"] += (float) $row->value;
			$semanas_values[$row->label_x]["count"] += 1;
		}
		sort($semanas);
		foreach ($semanas as $sem){
			$val = $semanas_values[$sem]["value"] / $semanas_values[$sem]["count"];
			if($val > 0){
				$data_chart[] = [ "label_x" => $sem, "index_y" => 0, "value" => $val, 'name' => 'AGROBAN' ];
			}else{
				$data_chart[] = [ "label_x" => null, "index_y" => 0, "value" => $val, 'name' => 'AGROBAN' ];
			}
		}

		$semanas = [];
        foreach ($data_chart as $key => $row)
        {
            $row = (object) $row;
			$semanas[$key] = (int) $row->label_x;
			
        }
        array_multisort($semanas, SORT_ASC, $data_chart);

        $groups = [
			[
				"name" => $filters->var1,
				"type" => 'line',
				'format' => ''
			]
        ];

        /*$selected = [
            "Edad" => false,
            "Calibre" => false,
            "Peso" => false,
            "Merma Neta" => false,
            "Racimos Procesados" => false,
            "Racimos Cosechados" => true,
            "Cajas" => false,
            'Temp Min.' => true,
            'Horas Luz (400)' => false
        ];*/
        $selected = [];
        $types = [$filters->type1];
        
        $response->data = $this->grafica_z($data_chart, $groups, $selected, $types);

        return $response;
	}
	
	public function porHectareas(){
		$response = new stdClass;
		$response->data = [];
		$filters = $this->params();
		$finca_marun = 1;
		$finca_quintana = 2;
		$proyeccion = 1;
		if($filters->por_hectareas == 'HA/ANIO') $proyeccion = 52;
		$week = $this->db->queryOne("SELECT agroaudit_reiset.getWeek(CURRENT_DATE)");
		$year = $this->db->queryOne("SELECT YEAR(CURRENT_DATE)");

		// RACIMOS COSECHADOS
			$sql = "SELECT
						'RACIMOS CORTADOS' as name,
						(
							SELECT ROUND(AVG(cantidad)/hectareas, 2) * $proyeccion
							FROM (
								SELECT semana, COUNT(1) as cantidad, (SELECT SUM(agroaudit_marcel.getHectareasLote(id, $year, semana)) FROM agroaudit_marcel.lotes) as hectareas
								FROM agroaudit_marcel.produccion_historica 
								WHERE year = $year  AND semana < $week
								GROUP BY semana
							) tbl
						) AS laniado,
						(
							SELECT ROUND(AVG(cantidad)/hectareas, 2) * $proyeccion
							FROM (
								SELECT semana, COUNT(1) as cantidad, (SELECT SUM(agroaudit_marun.getHectareasLote(id, $year, semana)) FROM agroaudit_marun.lotes WHERE idFinca = $finca_marun) as hectareas
								FROM agroaudit_marun.produccion_racimos 
								WHERE year = $year AND id_finca = $finca_marun AND semana < $week
								GROUP BY semana
							) tbl
						) AS marun,
						(
							SELECT ROUND(AVG(cantidad)/hectareas, 2) * $proyeccion
							FROM (
								SELECT semana, COUNT(1) as cantidad, (SELECT SUM(agroaudit_reiset.getHectareasLote(id, $year, semana)) FROM agroaudit_reiset.lotes) as hectareas
								FROM agroaudit_reiset.produccion_racimos_formularios
								WHERE year = $year AND semana < $week
								GROUP BY semana
							) tbl
						) AS reiset,
						(
							SELECT ROUND(AVG(cantidad)/hectareas, 2) * $proyeccion
							FROM (
								SELECT semana, COUNT(1) as cantidad, (SELECT SUM(agroaudit_quintana.getHectareasLote(id, $year, semana)) FROM agroaudit_quintana.lotes WHERE idFinca = $finca_quintana) as hectareas
								FROM agroaudit_quintana.produccion_historica 
								WHERE year = $year AND id_finca = $finca_quintana AND semana < $week
								GROUP BY semana
							) tbl
						) AS quintana";
			$racimos_cortados = $this->db->queryRow($sql);
			$racimos_cortados->agroban = round(
					(((float)$racimos_cortados->laniado) + ((float)$racimos_cortados->marun) + ((float)$racimos_cortados->reiset) + ((float)$racimos_cortados->quintana))
					/ (
						((float)$racimos_cortados->laniado > 0 ? 1 : 0) + 
						((float)$racimos_cortados->marun > 0 ? 1 : 0) + 
						((float)$racimos_cortados->reiset > 0 ? 1 : 0) + 
						((float)$racimos_cortados->quintana > 0 ? 1 : 0)
					)
				, 2);

		// RACIMOS PROCESADOS
			$sql = "SELECT
						'RACIMOS PROCESADOS' as name,
						(
							SELECT ROUND(AVG(cantidad)/hectareas, 2) * $proyeccion
							FROM (
								SELECT semana, COUNT(1) as cantidad, (SELECT SUM(agroaudit_marcel.getHectareasLote(id, $year, semana)) FROM agroaudit_marcel.lotes) as hectareas
								FROM agroaudit_marcel.produccion_historica 
								WHERE year = $year AND tipo = 'PROC' AND semana < $week
								GROUP BY semana
							) tbl
						) AS laniado,
						(
							SELECT ROUND(AVG(cantidad)/hectareas, 2) * $proyeccion
							FROM (
								SELECT semana, COUNT(1) as cantidad, (SELECT SUM(agroaudit_marun.getHectareasLote(id, $year, semana)) FROM agroaudit_marun.lotes WHERE idFinca = $finca_marun) as hectareas
								FROM agroaudit_marun.produccion_racimos 
								WHERE year = $year AND id_finca = $finca_marun AND tipo = 'PROC' AND semana < $week
								GROUP BY semana
							) tbl
						) AS marun,
						(
							SELECT ROUND(AVG(cantidad)/hectareas, 2) * $proyeccion
							FROM (
								SELECT semana, COUNT(1) as cantidad, (SELECT SUM(agroaudit_reiset.getHectareasLote(id, $year, semana)) FROM agroaudit_reiset.lotes) as hectareas
								FROM agroaudit_reiset.produccion_racimos_formularios
								WHERE year = $year AND tipo = 'PROC' AND semana < $week
								GROUP BY semana
							) tbl
						) AS reiset,
						(
							SELECT ROUND(AVG(cantidad)/hectareas, 2) * $proyeccion
							FROM (
								SELECT semana, COUNT(1) as cantidad, (SELECT SUM(agroaudit_quintana.getHectareasLote(id, $year, semana)) FROM agroaudit_quintana.lotes WHERE idFinca = $finca_quintana) as hectareas
								FROM agroaudit_quintana.produccion_historica 
								WHERE year = $year AND id_finca = $finca_quintana AND tipo = 'PROC' AND semana < $week
								GROUP BY semana
							) tbl
						) AS quintana";
			$racimos_procesados = $this->db->queryRow($sql);
			$racimos_procesados->agroban = round(
					(((float)$racimos_procesados->laniado) + ((float)$racimos_procesados->marun) + ((float)$racimos_procesados->reiset) + ((float)$racimos_procesados->quintana))
					/ (
						((float)$racimos_procesados->laniado > 0 ? 1 : 0) + 
						((float)$racimos_procesados->marun > 0 ? 1 : 0) + 
						((float)$racimos_procesados->reiset > 0 ? 1 : 0) + 
						((float)$racimos_procesados->quintana > 0 ? 1 : 0)
					)
				, 2);
		
		// RACIMOS RECUSADOS
			$sql = "SELECT
						'RACIMOS RECUSADOS' as name,
						(
							SELECT ROUND(AVG(cantidad)/hectareas, 2) * $proyeccion
							FROM (
								SELECT semana, COUNT(1) as cantidad, (SELECT SUM(agroaudit_marcel.getHectareasLote(id, $year, semana)) FROM agroaudit_marcel.lotes) as hectareas
								FROM agroaudit_marcel.produccion_historica 
								WHERE year = $year AND (tipo = 'RECU' OR tipo = 'RECUSADO') AND semana < $week
								GROUP BY semana
							) tbl
						) AS laniado,
						(
							SELECT ROUND(AVG(cantidad)/hectareas, 2) * $proyeccion
							FROM (
								SELECT semana, COUNT(1) as cantidad, (SELECT SUM(agroaudit_marun.getHectareasLote(id, $year, semana)) FROM agroaudit_marun.lotes WHERE idFinca = $finca_marun) as hectareas
								FROM agroaudit_marun.produccion_racimos 
								WHERE year = $year AND id_finca = $finca_marun AND (tipo = 'RECU' OR tipo = 'RECUSADO') AND semana < $week
								GROUP BY semana
							) tbl
						) AS marun,
						(
							SELECT ROUND(AVG(cantidad)/hectareas, 2) * $proyeccion
							FROM (
								SELECT semana, COUNT(1) as cantidad, (SELECT SUM(agroaudit_reiset.getHectareasLote(id, $year, semana)) FROM agroaudit_reiset.lotes) as hectareas
								FROM agroaudit_reiset.produccion_racimos_formularios
								WHERE year = $year AND (tipo = 'RECU' OR tipo = 'RECUSADO') AND semana < $week
								GROUP BY semana
							) tbl
						) AS reiset,
						(
							SELECT ROUND(AVG(cantidad)/hectareas, 2) * $proyeccion
							FROM (
								SELECT semana, COUNT(1) as cantidad, (SELECT SUM(agroaudit_quintana.getHectareasLote(id, $year, semana)) FROM agroaudit_quintana.lotes WHERE idFinca = $finca_quintana) as hectareas
								FROM agroaudit_quintana.produccion_historica 
								WHERE year = $year AND id_finca = $finca_quintana AND (tipo = 'RECU' OR tipo = 'RECUSADO') AND semana < $week
								GROUP BY semana
							) tbl
						) AS quintana";
			$racimos_recusados = $this->db->queryRow($sql);
			$racimos_recusados->agroban = round(
					(((float)$racimos_recusados->laniado) + ((float)$racimos_recusados->marun) + ((float)$racimos_recusados->reiset) + ((float)$racimos_recusados->quintana))
					/ (
						((float)$racimos_recusados->laniado > 0 ? 1 : 0) + 
						((float)$racimos_recusados->marun > 0 ? 1 : 0) + 
						((float)$racimos_recusados->reiset > 0 ? 1 : 0) + 
						((float)$racimos_recusados->quintana > 0 ? 1 : 0)
					)
				, 2);

		// CAJAS
			$sql = "SELECT
					'CAJAS CONV' as name,
					(
						SELECT ROUND(AVG(cantidad)/hectareas, 2) * $proyeccion
						FROM (
							SELECT semana, SUM(cantidad) cantidad, 
								(SELECT SUM(agroaudit_marcel.getHectareasLote(id, $year, semana)) FROM agroaudit_marcel.lotes) as hectareas
							FROM (
								SELECT
									semana,
									IF(conv.id IS NOT NULL, 
										ROUND(SUM(cajas.`kg`) / 0.4536 / 40.5, 0),
										COUNT(1)) AS cantidad
								FROM agroaudit_marcel.produccion_cajas `cajas`
								LEFT JOIN agroaudit_marcel.produccion_cajas_convertir conv ON cajas.marca = conv.marca
								WHERE cajas.marca != '' AND year = $year AND semana < $week
								GROUP BY `cajas`.`marca`, cajas.fecha
								UNION ALL
								SELECT
									semana,
									IF(conv.id IS NOT NULL, 
										ROUND(SUM(cajas.`kg`) / 0.4536 / 40.5, 0),
										COUNT(1)) AS cantidad
								FROM agroaudit_marcel.produccion_gavetas `cajas`
								LEFT JOIN agroaudit_marcel.produccion_cajas_convertir conv ON cajas.marca = conv.marca
								WHERE cajas.marca != '' AND year = $year AND semana < $week
								GROUP BY `cajas`.`marca`, cajas.fecha
							) tbl
							GROUP BY semana
						) tbl
					) AS laniado,
					(
						SELECT ROUND(AVG(cantidad)/hectareas, 2) * $proyeccion
						FROM (
							SELECT semana, SUM(convertidas_415) as cantidad, (SELECT SUM(agroaudit_marun.getHectareasLote(id, $year, semana)) FROM agroaudit_marun.lotes WHERE idFinca = $finca_marun) as hectareas
							FROM agroaudit_marun.produccion_cajas
							WHERE year = $year AND id_finca = $finca_marun AND semana < $week
							GROUP BY semana
						) tbl
					) AS marun,
					(
						SELECT ROUND(AVG(cantidad)/hectareas, 2) * $proyeccion
						FROM (
							SELECT semana, SUM(convertidas_415) as cantidad, (SELECT SUM(agroaudit_reiset.getHectareasLote(id, $year, semana)) FROM agroaudit_reiset.lotes) as hectareas
							FROM agroaudit_reiset.produccion_cajas
							WHERE year = $year AND semana < $week
							GROUP BY semana
						) tbl
					) AS reiset,
					(
						SELECT ROUND(AVG(cantidad)/hectareas, 2) * $proyeccion
						FROM (
							SELECT semana, SUM(convertidas_415) as cantidad, (SELECT SUM(agroaudit_quintana.getHectareasLote(id, $year, semana)) FROM agroaudit_quintana.lotes WHERE idFinca = $finca_quintana) as hectareas
							FROM agroaudit_quintana.produccion_cajas 
							WHERE year = $year AND id_finca = $finca_quintana AND semana < $week
							GROUP BY semana
						) tbl
					) AS quintana";
			$cajas = $this->db->queryRow($sql);
			$cajas->agroban = round(
				(((float)$cajas->laniado) + ((float)$cajas->marun) + ((float)$cajas->reiset) + ((float)$cajas->quintana))
				/ (
					((float)$cajas->laniado > 0 ? 1 : 0) + 
					((float)$cajas->marun > 0 ? 1 : 0) + 
					((float)$cajas->reiset > 0 ? 1 : 0) + 
					((float)$cajas->quintana > 0 ? 1 : 0)
				)
			, 2);
				
		// ENFUNDE
			$sql = "SELECT
					'ENFUNDE' as name,
					(
						SELECT ROUND(AVG(cantidad)/hectareas, 2) * $proyeccion
						FROM (
							SELECT semana, SUM(usadas) as cantidad, (SELECT SUM(agroaudit_marcel.getHectareasLote(id, $year, semana)) FROM agroaudit_marcel.lotes WHERE idFinca = $finca_marun) as hectareas
							FROM agroaudit_marcel.produccion_enfunde
							WHERE years = $year AND semana < $week
							GROUP BY semana
						) tbl
					) AS laniado,
					(
						SELECT ROUND(AVG(cantidad)/hectareas, 2) * $proyeccion
						FROM (
							SELECT semana, SUM(usadas) as cantidad, (SELECT SUM(agroaudit_marun.getHectareasLote(id, $year, semana)) FROM agroaudit_marun.lotes WHERE idFinca = $finca_marun) as hectareas
							FROM agroaudit_marun.produccion_enfunde
							WHERE years = $year AND id_finca = $finca_marun AND semana < $week
							GROUP BY semana
						) tbl
					) AS marun,
					(
						SELECT ROUND(AVG(cantidad)/hectareas, 2) * $proyeccion
						FROM (
							SELECT semana, SUM(usadas) as cantidad, (SELECT SUM(agroaudit_reiset.getHectareasLote(id, $year, semana)) FROM agroaudit_reiset.lotes) as hectareas
							FROM agroaudit_reiset.produccion_enfunde
							WHERE years = $year AND semana < $week
							GROUP BY semana
						) tbl
					) AS reiset,
					(
						SELECT ROUND(AVG(cantidad)/hectareas, 2) * $proyeccion
						FROM (
							SELECT semana, SUM(usadas) as cantidad, (SELECT SUM(agroaudit_quintana.getHectareasLote(id, $year, semana)) FROM agroaudit_quintana.lotes WHERE idFinca = $finca_quintana) as hectareas
							FROM agroaudit_quintana.produccion_enfunde 
							WHERE years = $year AND id_finca = $finca_quintana AND semana < $week
							GROUP BY semana
						) tbl
					) AS quintana";
			$enfunde = $this->db->queryRow($sql);
			$enfunde->agroban = round(
				(((float)$enfunde->laniado) + ((float)$enfunde->marun) + ((float)$enfunde->reiset) + ((float)$enfunde->quintana))
				/ (
					((float)$enfunde->laniado > 0 ? 1 : 0) + 
					((float)$enfunde->marun > 0 ? 1 : 0) + 
					((float)$enfunde->reiset > 0 ? 1 : 0) + 
					((float)$enfunde->quintana > 0 ? 1 : 0)
				)
			, 2);

		$response->data = [
			$racimos_cortados,
			$racimos_procesados,
			$racimos_recusados,
			$cajas,
			$enfunde
		];

		return $response;
	}

	public function semanal(){
		$response = new stdClass;
		$filters = $this->params();
		$finca_marun = 1;
		$finca_quintana = 2;
		$proyeccion = 1;

		if($filters->por_hectareas == 'HA/ANIO') $proyeccion = 52;
		$week = $this->db->queryOne("SELECT agroaudit_reiset.getWeek(CURRENT_DATE)");
		$year = $this->db->queryOne("SELECT YEAR(CURRENT_DATE)");
		$sWhere = "";
		$sWhereEnfundada = "";
		$tablaValue = "promedio";
		if($filters->por_semana > 0){
			$sWhere .= " AND semana = $filters->por_semana";
			$sWhereEnfundada = " AND semana_enfundada = $filters->por_semana";
			$tablaValue = "sem_{$filters->por_semana}";
		}

		$response->semanas = [];
		for($i = 1; $i <= $week; $i++){
			$response->semanas[] = $i;
		}

		// PESO RACIMOS
			$sql = "SELECT
						'PESO RACIMO PROM LB' as name,
						(
							SELECT ROUND(AVG(cantidad), 2)
							FROM (
								SELECT semana, AVG(peso/0.4536) as cantidad
								FROM agroaudit_marcel.produccion_historica 
								WHERE year = $year $sWhere
								GROUP BY semana
							) tbl
						) AS laniado,
						(
							SELECT ROUND(AVG(cantidad), 2)
							FROM (
								SELECT semana, AVG(peso/0.4536) as cantidad
								FROM agroaudit_marun.produccion_racimos 
								WHERE year = $year AND id_finca = $finca_marun $sWhere
								GROUP BY semana
							) tbl
						) AS marun,
						(
							SELECT ROUND(AVG(cantidad), 2)
							FROM (
								SELECT semana, AVG(peso) as cantidad
								FROM agroaudit_reiset.produccion_racimos_formularios
								WHERE year = $year $sWhere
								GROUP BY semana
							) tbl
						) AS reiset,
						(
							SELECT ROUND(AVG(cantidad), 2)
							FROM (
								SELECT semana, AVG(peso) as cantidad
								FROM agroaudit_quintana.produccion_historica 
								WHERE year = $year AND id_finca = $finca_quintana $sWhere
								GROUP BY semana
							) tbl
						) AS quintana";
				$peso_racimo_prom = $this->db->queryRow($sql);
				$peso_racimo_prom->agroban = round(
					(((float)$peso_racimo_prom->laniado) + ((float)$peso_racimo_prom->marun) + ((float)$peso_racimo_prom->reiset) + ((float)$peso_racimo_prom->quintana))
					/ (
						((float)$peso_racimo_prom->laniado > 0 ? 1 : 0) + 
						((float)$peso_racimo_prom->marun > 0 ? 1 : 0) + 
						((float)$peso_racimo_prom->reiset > 0 ? 1 : 0) + 
						((float)$peso_racimo_prom->quintana > 0 ? 1 : 0)
					)
				, 2);
		// EDAD PROM
			$sql = "SELECT
						'EDAD PROM' as name,
						(
							SELECT ROUND(AVG(cantidad), 2)
							FROM (
								SELECT semana, AVG(edad) as cantidad
								FROM agroaudit_marcel.produccion_historica 
								WHERE year = $year $sWhere
								GROUP BY semana
							) tbl
						) AS laniado,
						(
							SELECT ROUND(AVG(cantidad), 2)
							FROM (
								SELECT semana, AVG(edad) as cantidad
								FROM agroaudit_marun.produccion_racimos
								WHERE year = $year $sWhere
								GROUP BY semana
							) tbl
						) AS marun,
						(
							SELECT ROUND(AVG(cantidad), 2)
							FROM (
								SELECT semana, AVG(edad) as cantidad
								FROM agroaudit_reiset.produccion_racimos_formularios
								WHERE year = $year $sWhere
								GROUP BY semana
							) tbl
						) AS reiset,
						(
							SELECT ROUND(AVG(cantidad), 2)
							FROM (
								SELECT semana, AVG(edad) as cantidad
								FROM agroaudit_quintana.produccion_historica 
								WHERE year = $year $sWhere
								GROUP BY semana
							) tbl
						) AS quintana";
				$edad_prom = $this->db->queryRow($sql);
				$edad_prom->agroban = round(
					(((float)$edad_prom->laniado) + ((float)$edad_prom->marun) + ((float)$edad_prom->reiset) + ((float)$edad_prom->quintana))
					/ (
						((float)$edad_prom->laniado > 0 ? 1 : 0) + 
						((float)$edad_prom->marun > 0 ? 1 : 0) + 
						((float)$edad_prom->reiset > 0 ? 1 : 0) + 
						((float)$edad_prom->quintana > 0 ? 1 : 0)
					)
				, 2);

		// % TALLO
			$sql = "SELECT
					'% TALLO' as name,
					(
						SELECT ROUND(AVG(cantidad), 2)
						FROM (
							SELECT semana, AVG(tallo/peso_racimos*100) as cantidad
							FROM agroaudit_marcel.merma 
							WHERE year = $year $sWhere
							GROUP BY semana
						) tbl
					) AS laniado,
					(
						SELECT ROUND(AVG(cantidad), 2)
						FROM (
							SELECT semana, AVG(tallo/racimo*100) as cantidad
							FROM agroaudit_marun.merma 
							WHERE year = $year AND id_finca = $finca_marun $sWhere
							GROUP BY semana
						) tbl
					) AS marun,
					(
						SELECT ROUND(AVG(cantidad), 2)
						FROM (
							SELECT semana, AVG(tallo/racimo*100) as cantidad
							FROM agroaudit_reiset.merma
							WHERE year = $year $sWhere
							GROUP BY semana
						) tbl
					) AS reiset,
					(
						SELECT ROUND(AVG(cantidad), 2)
						FROM (
							SELECT semana, AVG(tallo/racimo*100) as cantidad
							FROM agroaudit_quintana.merma 
							WHERE year = $year AND id_finca = $finca_quintana $sWhere
							GROUP BY semana
						) tbl
					) AS quintana";
			$tallo = $this->db->queryRow($sql);
			$tallo->agroban = round(
				(((float)$tallo->laniado) + ((float)$tallo->marun) + ((float)$tallo->reiset) + ((float)$tallo->quintana))
				/ (
					((float)$tallo->laniado > 0 ? 1 : 0) + 
					((float)$tallo->marun > 0 ? 1 : 0) + 
					((float)$tallo->reiset > 0 ? 1 : 0) + 
					((float)$tallo->quintana > 0 ? 1 : 0)
				)
			, 2);
		// % RECUSADOS
			$sql = "SELECT
					'% RECUSADOS' as name,
					(
						SELECT ROUND(AVG(cantidad), 2)
						FROM (
							SELECT semana, SUM(IF(tipo='RECU' OR tipo='RECUSADO',1,0))/COUNT(1)*100 as cantidad
							FROM agroaudit_marcel.produccion_historica 
							WHERE year = $year $sWhere
							GROUP BY semana
						) tbl
					) AS laniado,
					(
						SELECT ROUND(AVG(cantidad), 2)
						FROM (
							SELECT semana, SUM(IF(tipo='RECU' OR tipo='RECUSADO',1,0))/COUNT(1)*100 as cantidad
							FROM agroaudit_marun.produccion_racimos 
							WHERE year = $year AND id_finca = $finca_marun $sWhere
							GROUP BY semana
						) tbl
					) AS marun,
					(
						SELECT ROUND(AVG(cantidad), 2)
						FROM (
							SELECT semana, SUM(IF(tipo='RECU' OR tipo='RECUSADO',1,0))/COUNT(1)*100 as cantidad
							FROM agroaudit_reiset.produccion_racimos_formularios
							WHERE year = $year $sWhere
							GROUP BY semana
						) tbl
					) AS reiset,
					(
						SELECT ROUND(AVG(cantidad), 2)
						FROM (
							SELECT semana, SUM(IF(tipo='RECU' OR tipo='RECUSADO',1,0))/COUNT(1)*100 as cantidad
							FROM agroaudit_quintana.produccion_historica 
							WHERE year = $year AND id_finca = $finca_quintana $sWhere
							GROUP BY semana
						) tbl
					) AS quintana";
			$recusados = $this->db->queryRow($sql);
			$recusados->agroban = round(
				(((float)$recusados->laniado) + ((float)$recusados->marun) + ((float)$recusados->reiset) + ((float)$recusados->quintana))
				/ (
					((float)$recusados->laniado > 0 ? 1 : 0) + 
					((float)$recusados->marun > 0 ? 1 : 0) + 
					((float)$recusados->reiset > 0 ? 1 : 0) + 
					((float)$recusados->quintana > 0 ? 1 : 0)
				)
			, 2);
		// CALIBRE PROM
			$sql = "SELECT
					'CALIBRE PROM' as name,
					(
						SELECT ROUND(AVG(cantidad), 2)
						FROM (
							SELECT semana, AVG(calibre) as cantidad
							FROM agroaudit_marcel.produccion_historica 
							WHERE year = $year AND calibre > 0 $sWhere
							GROUP BY semana
						) tbl
					) AS laniado,
					(
						SELECT ROUND(AVG(cantidad), 2)
						FROM (
							SELECT semana, AVG(calibre_segunda) as cantidad
							FROM agroaudit_marun.produccion_racimos
							WHERE year = $year $sWhere
							GROUP BY semana
						) tbl
					) AS marun,
					(
						SELECT ROUND(AVG(cantidad), 2)
						FROM (
							SELECT semana, AVG(calibre_segunda) as cantidad
							FROM agroaudit_reiset.produccion_racimos_formularios
							WHERE year = $year $sWhere
							GROUP BY semana
						) tbl
					) AS reiset,
					(
						SELECT ROUND(AVG(cantidad), 2)
						FROM (
							SELECT semana, AVG(calibre) as cantidad
							FROM agroaudit_quintana.produccion_historica
							WHERE year = $year $sWhere
							GROUP BY semana
						) tbl
					) AS quintana";
			$calibre = $this->db->queryRow($sql);
			$calibre->agroban = round(
				(((float)$calibre->laniado) + ((float)$calibre->marun) + ((float)$calibre->reiset) + ((float)$calibre->quintana))
				/ (
					((float)$calibre->laniado > 0 ? 1 : 0) + 
					((float)$calibre->marun > 0 ? 1 : 0) + 
					((float)$calibre->reiset > 0 ? 1 : 0) + 
					((float)$calibre->quintana > 0 ? 1 : 0)
				)
			, 2);
		// MANOS PROM
			$sql = "SELECT
				'MANOS PROM' as name,
				(
					SELECT ROUND(AVG(cantidad), 2)
					FROM (
						SELECT semana, AVG(manos) as cantidad
						FROM agroaudit_marcel.produccion_historica 
						WHERE year = $year AND manos > 0 $sWhere
						GROUP BY semana
					) tbl
				) AS laniado,
				(
					SELECT ROUND(AVG(cantidad), 2)
					FROM (
						SELECT semana, AVG(manos) as cantidad
						FROM agroaudit_marun.produccion_racimos
						WHERE year = $year $sWhere
						GROUP BY semana
					) tbl
				) AS marun,
				(
					SELECT ROUND(AVG(cantidad), 2)
					FROM (
						SELECT semana, AVG(manos) as cantidad
						FROM agroaudit_reiset.produccion_racimos_formularios
						WHERE year = $year $sWhere
						GROUP BY semana
					) tbl
				) AS reiset,
				(
					SELECT ROUND(AVG(cantidad), 2)
					FROM (
						SELECT semana, AVG(manos) as cantidad
						FROM agroaudit_quintana.produccion_historica
						WHERE year = $year $sWhere
						GROUP BY semana
					) tbl
				) AS quintana";
			$manos = $this->db->queryRow($sql);
			$manos->agroban = round(
				(((float)$manos->laniado) + ((float)$manos->marun) + ((float)$manos->reiset) + ((float)$manos->quintana))
				/ (
					((float)$manos->laniado > 0 ? 1 : 0) + 
					((float)$manos->marun > 0 ? 1 : 0) + 
					((float)$manos->reiset > 0 ? 1 : 0) + 
					((float)$manos->quintana > 0 ? 1 : 0)
				)
			, 2);
		// MUESTREO
			$sql = "SELECT
					'% MUESTREO' as name,
					(
						SELECT ROUND(AVG(cantidad), 2)
						FROM (
							SELECT semana, SUM(IF(manos > 3, 1, 0))/COUNT(1)*100 as cantidad
							FROM agroaudit_marcel.produccion_historica 
							WHERE year = $year $sWhere
							GROUP BY semana
						) tbl
					) AS laniado,
					(
						SELECT ROUND(AVG(cantidad), 2)
						FROM (
							SELECT semana, SUM(IF(manos > 0, 1, 0))/COUNT(1)*100 as cantidad
							FROM agroaudit_marun.produccion_racimos 
							WHERE year = $year AND id_finca = $finca_marun $sWhere
							GROUP BY semana
						) tbl
					) AS marun,
					(
						SELECT ROUND(AVG(cantidad), 2)
						FROM (
							SELECT semana, SUM(IF(manos > 0, 1, 0))/COUNT(1)*100 as cantidad
							FROM agroaudit_reiset.produccion_racimos_formularios
							WHERE year = $year $sWhere
							GROUP BY semana
						) tbl
					) AS reiset,
					(
						SELECT ROUND(AVG(cantidad), 2)
						FROM (
							SELECT semana, SUM(IF(manos > 3, 1, 0))/COUNT(1)*100 as cantidad
							FROM agroaudit_quintana.produccion_historica 
							WHERE year = $year AND id_finca = $finca_quintana $sWhere
							GROUP BY semana
						) tbl
					) AS quintana";
			$muestreo = $this->db->queryRow($sql);
			$muestreo->agroban = round(
				(((float)$muestreo->laniado) + ((float)$muestreo->marun) + ((float)$muestreo->reiset) + ((float)$muestreo->quintana))
				/ (
					((float)$muestreo->laniado > 0 ? 1 : 0) + 
					((float)$muestreo->marun > 0 ? 1 : 0) + 
					((float)$muestreo->reiset > 0 ? 1 : 0) + 
					((float)$muestreo->quintana > 0 ? 1 : 0)
				)
			, 2);
		// RATIO CORTADO
			$sql = "SELECT
					'RATIO CORTADO' as name,
					(
						SELECT ROUND(AVG(cantidad), 2)
						FROM (
							SELECT semana, SUM(IF(tipo='CAJAS',cantidad,0))/SUM(IF(tipo='RACIMOS',cantidad,0)) cantidad
							FROM (
								SELECT semana, COUNT(1) as cantidad, 'RACIMOS' AS tipo
								FROM agroaudit_marcel.produccion_historica 
								WHERE year = $year $sWhere
								GROUP BY semana
								UNION ALL
								SELECT semana, SUM(cantidad) cantidad, 'CAJAS' AS tipo
								FROM (
									SELECT
										semana,
										IF(conv.id IS NOT NULL, 
											ROUND(SUM(cajas.`kg`) / 0.4536 / 40.5, 0),
											COUNT(1)) AS cantidad
									FROM agroaudit_marcel.produccion_cajas `cajas`
									LEFT JOIN agroaudit_marcel.produccion_cajas_convertir conv ON cajas.marca = conv.marca
									WHERE cajas.marca != '' AND year = $year $sWhere
									GROUP BY `cajas`.`marca`, cajas.fecha
									UNION ALL
									SELECT
										semana,
										IF(conv.id IS NOT NULL, 
											ROUND(SUM(cajas.`kg`) / 0.4536 / 40.5, 0),
											COUNT(1)) AS cantidad
									FROM agroaudit_marcel.produccion_gavetas `cajas`
									LEFT JOIN agroaudit_marcel.produccion_cajas_convertir conv ON cajas.marca = conv.marca
									WHERE cajas.marca != '' AND year = $year $sWhere
									GROUP BY `cajas`.`marca`, cajas.fecha
								) tbl
								GROUP BY semana
							) tbl
							GROUP BY semana
						) tbl
					) AS laniado,
					(
						SELECT ROUND(AVG(cantidad), 2)
						FROM (
							SELECT semana, SUM(IF(tipo='CAJAS',cantidad,0))/SUM(IF(tipo='RACIMOS',cantidad,0)) cantidad
							FROM (
								SELECT semana, COUNT(1) as cantidad, 'RACIMOS' AS tipo
								FROM agroaudit_marun.produccion_racimos 
								WHERE year = $year AND id_finca = $finca_marun $sWhere
								GROUP BY semana
								UNION ALL
								SELECT semana, SUM(convertidas_415) as cantidad, 'CAJAS' AS tipo
								FROM agroaudit_marun.produccion_cajas 
								WHERE year = $year AND id_finca = $finca_marun $sWhere
								GROUP BY semana
							) tbl
							GROUP BY semana
						) tbl
					) AS marun,
					(
						SELECT ROUND(AVG(cantidad), 2)
						FROM (
							SELECT semana, SUM(IF(tipo='CAJAS',cantidad,0))/SUM(IF(tipo='RACIMOS',cantidad,0)) cantidad
							FROM (
								SELECT semana, COUNT(1) as cantidad, 'RACIMOS' AS tipo
								FROM agroaudit_reiset.produccion_racimos_formularios 
								WHERE year = $year $sWhere
								GROUP BY semana
								UNION ALL
								SELECT semana, SUM(convertidas_415) as cantidad, 'CAJAS' AS tipo
								FROM agroaudit_reiset.produccion_cajas 
								WHERE year = $year $sWhere
								GROUP BY semana
							) tbl
							GROUP BY semana
						) tbl
					) AS reiset,
					(
						SELECT ROUND(AVG(cantidad), 2)
						FROM (
							SELECT semana, SUM(IF(tipo='CAJAS',cantidad,0))/SUM(IF(tipo='RACIMOS',cantidad,0)) cantidad
							FROM (
								SELECT semana, COUNT(1) as cantidad, 'RACIMOS' AS tipo
								FROM agroaudit_quintana.produccion_historica 
								WHERE year = $year AND id_finca = $finca_quintana
								GROUP BY semana
								UNION ALL
								SELECT semana, SUM(convertidas_415) as cantidad, 'CAJAS' AS tipo
								FROM agroaudit_quintana.produccion_cajas 
								WHERE year = $year AND id_finca = $finca_quintana
								GROUP BY semana
							) tbl
							GROUP BY semana
						) tbl
					) AS quintana";
			$ratio_cortado = $this->db->queryRow($sql);
			$ratio_cortado->agroban = round(
				(((float)$ratio_cortado->laniado) + ((float)$ratio_cortado->marun) + ((float)$ratio_cortado->reiset) + ((float)$ratio_cortado->quintana))
				/ (
					((float)$ratio_cortado->laniado > 0 ? 1 : 0) + 
					((float)$ratio_cortado->marun > 0 ? 1 : 0) + 
					((float)$ratio_cortado->reiset > 0 ? 1 : 0) + 
					((float)$ratio_cortado->quintana > 0 ? 1 : 0)
				)
			, 2);
		
		// RATIO PROCESADO
			$sql = "SELECT
				'RATIO PROCESADO' as name,
				(
					SELECT ROUND(AVG(cantidad), 2)
					FROM (
						SELECT semana, SUM(IF(tipo='CAJAS',cantidad,0))/SUM(IF(tipo='RACIMOS',cantidad,0)) cantidad
						FROM (
							SELECT semana, COUNT(1) as cantidad, 'RACIMOS' AS tipo
							FROM agroaudit_marcel.produccion_historica 
							WHERE year = $year AND tipo = 'PROC' $sWhere
							GROUP BY semana
							UNION ALL
							SELECT semana, SUM(cantidad) cantidad, 'CAJAS' AS tipo
							FROM (
								SELECT
									semana,
									IF(conv.id IS NOT NULL, 
										ROUND(SUM(cajas.`kg`) / 0.4536 / 40.5, 0),
										COUNT(1)) AS cantidad
								FROM agroaudit_marcel.produccion_cajas `cajas`
								LEFT JOIN agroaudit_marcel.produccion_cajas_convertir conv ON cajas.marca = conv.marca
								WHERE cajas.marca != '' AND year = $year $sWhere
								GROUP BY `cajas`.`marca`, cajas.fecha
								UNION ALL
								SELECT
									semana,
									IF(conv.id IS NOT NULL, 
										ROUND(SUM(cajas.`kg`) / 0.4536 / 40.5, 0),
										COUNT(1)) AS cantidad
								FROM agroaudit_marcel.produccion_gavetas `cajas`
								LEFT JOIN agroaudit_marcel.produccion_cajas_convertir conv ON cajas.marca = conv.marca
								WHERE cajas.marca != '' AND year = $year $sWhere
								GROUP BY `cajas`.`marca`, cajas.fecha
							) tbl
							GROUP BY semana
						) tbl
						GROUP BY semana
					) tbl
				) AS laniado,
				(
					SELECT ROUND(AVG(cantidad), 2)
					FROM (
						SELECT semana, SUM(IF(tipo='CAJAS',cantidad,0))/SUM(IF(tipo='RACIMOS',cantidad,0)) cantidad
						FROM (
							SELECT semana, COUNT(1) as cantidad, 'RACIMOS' AS tipo
							FROM agroaudit_marun.produccion_racimos 
							WHERE year = $year AND id_finca = $finca_marun AND tipo = 'PROC' $sWhere
							GROUP BY semana
							UNION ALL
							SELECT semana, SUM(convertidas_415) as cantidad, 'CAJAS' AS tipo
							FROM agroaudit_marun.produccion_cajas 
							WHERE year = $year AND id_finca = $finca_marun $sWhere
							GROUP BY semana
						) tbl
						GROUP BY semana
					) tbl
				) AS marun,
				(
					SELECT ROUND(AVG(cantidad), 2)
					FROM (
						SELECT semana, SUM(IF(tipo='CAJAS',cantidad,0))/SUM(IF(tipo='RACIMOS',cantidad,0)) cantidad
						FROM (
							SELECT semana, COUNT(1) as cantidad, 'RACIMOS' AS tipo
							FROM agroaudit_reiset.produccion_racimos_formularios 
							WHERE year = $year AND tipo = 'PROC' $sWhere
							GROUP BY semana
							UNION ALL
							SELECT semana, SUM(convertidas_415) as cantidad, 'CAJAS' AS tipo
							FROM agroaudit_reiset.produccion_cajas 
							WHERE year = $year $sWhere
							GROUP BY semana
						) tbl
						GROUP BY semana
					) tbl
				) AS reiset,
				(
					SELECT ROUND(AVG(cantidad), 2)
					FROM (
						SELECT semana, SUM(IF(tipo='CAJAS',cantidad,0))/SUM(IF(tipo='RACIMOS',cantidad,0)) cantidad
						FROM (
							SELECT semana, COUNT(1) as cantidad, 'RACIMOS' AS tipo
							FROM agroaudit_quintana.produccion_historica 
							WHERE year = $year AND id_finca = $finca_quintana AND tipo = 'PROC' $sWhere
							GROUP BY semana
							UNION ALL
							SELECT semana, SUM(convertidas_415) as cantidad, 'CAJAS' AS tipo
							FROM agroaudit_quintana.produccion_cajas 
							WHERE year = $year AND id_finca = $finca_quintana $sWhere
							GROUP BY semana
						) tbl
						GROUP BY semana
					) tbl
				) AS quintana";
			$ratio_procesado = $this->db->queryRow($sql);
			$ratio_procesado->agroban = round(
				(((float)$ratio_procesado->laniado) + ((float)$ratio_procesado->marun) + ((float)$ratio_procesado->reiset) + ((float)$ratio_procesado->quintana))
				/ (
					((float)$ratio_procesado->laniado > 0 ? 1 : 0) + 
					((float)$ratio_procesado->marun > 0 ? 1 : 0) + 
					((float)$ratio_procesado->reiset > 0 ? 1 : 0) + 
					((float)$ratio_procesado->quintana > 0 ? 1 : 0)
				)
			, 2);
		// MERMA CORTADA
			$sql = "SELECT
					'% MERMA CORTADA' as name,
					(
						SELECT $tablaValue FROM agroaudit_marcel.produccion_resumen_tabla WHERE anio = $year AND variable = 'MERMA CORTADA'
					) AS laniado,
					(
						SELECT $tablaValue FROM agroaudit_marun.produccion_resumen_tabla WHERE anio = $year AND variable = 'MERMA CORTADA' AND id_finca = $finca_marun
					) AS marun,
					(
						SELECT $tablaValue FROM agroaudit_reiset.produccion_resumen_tabla WHERE anio = $year AND variable = 'MERMA CORTADA'
					) AS reiset,
					(
						SELECT $tablaValue FROM agroaudit_quintana.produccion_resumen_tabla WHERE anio = $year AND variable = 'MERMA CORTADA' AND id_finca = $finca_quintana
					) AS quintana";
				$merma_cortada = $this->db->queryRow($sql);
				$merma_cortada->agroban = round(
					(((float)$merma_cortada->laniado) + ((float)$merma_cortada->marun) + ((float)$merma_cortada->reiset) + ((float)$merma_cortada->quintana))
					/ (
						((float)$merma_cortada->laniado > 0 ? 1 : 0) + 
						((float)$merma_cortada->marun > 0 ? 1 : 0) + 
						((float)$merma_cortada->reiset > 0 ? 1 : 0) + 
						((float)$merma_cortada->quintana > 0 ? 1 : 0)
					)
				, 2);
			
		// MERMA PROCESADA
			$merma_procesada = (object)[
				"name" => "% MERMA PROCESADA",
				"laniado" => $merma_cortada->laniado - $tallo->laniado,
				"marun" => $merma_cortada->marun - $tallo->marun,
				"reiset" => $merma_cortada->reiset - $tallo->reiset,
				"quintana" => $merma_cortada->quintana - $tallo->quintana,
			];
			$merma_procesada->agroban = round(
				(((float)$merma_procesada->laniado) + ((float)$merma_procesada->marun) + ((float)$merma_procesada->reiset) + ((float)$merma_procesada->quintana))
				/ (
					((float)$merma_procesada->laniado > 0 ? 1 : 0) + 
					((float)$merma_procesada->marun > 0 ? 1 : 0) + 
					((float)$merma_procesada->reiset > 0 ? 1 : 0) + 
					((float)$merma_procesada->quintana > 0 ? 1 : 0)
				)
			, 2);
		// MERMA NETA
			$sql = "SELECT
				'% MERMA NETA' as name,
				(
					SELECT ROUND(AVG(cantidad), 2)
					FROM (
						SELECT 
							(SELECT SUM(cantidad) 
							FROM agroaudit_marcel.merma 
							INNER JOIN agroaudit_marcel.merma_detalle ON merma.id = merma_detalle.id_merma
							WHERE year = $year
								AND porcentaje_merma > 0 
								AND campo = 'Total Peso Merma (lb)'
								$sWhere
							) / SUM(peso_neto) * 100 AS cantidad
						FROM agroaudit_marcel.merma 
						WHERE year = $year AND porcentaje_merma > 0 $sWhere
					) tbl
				) AS laniado,
				(
					SELECT ROUND(AVG(cantidad), 2)
					FROM (
						SELECT 
							(SELECT SUM(cantidad) 
							FROM agroaudit_marun.merma 
							INNER JOIN agroaudit_marun.merma_detalle ON merma.id = merma_detalle.id_merma
							WHERE year = $year
								AND porcentaje_merma > 0 
								AND campo = 'Total peso Merma Neta (Kg)'
								AND id_finca = $finca_marun
								$sWhere
							) / SUM(peso_neto) * 100 AS cantidad
						FROM agroaudit_marun.merma 
						WHERE year = $year AND porcentaje_merma > 0 AND id_finca = $finca_marun $sWhere
					) tbl
				) AS marun,
				(
					SELECT ROUND(AVG(cantidad), 2)
					FROM (
						SELECT 
							(SELECT SUM(cantidad) 
							FROM agroaudit_reiset.merma 
							INNER JOIN agroaudit_reiset.merma_detalle ON merma.id = merma_detalle.id_merma
							WHERE year = $year
								AND porcentaje_merma > 0 
								AND campo = 'Total peso Merma'
								$sWhere
							) / SUM(peso_neto) * 100 AS cantidad
						FROM agroaudit_reiset.merma 
						WHERE year = $year AND porcentaje_merma > 0 $sWhere
					) tbl
				) AS reiset,
				(
					SELECT ROUND(AVG(cantidad), 2)
					FROM (
						SELECT 
							(SELECT SUM(cantidad) 
							FROM agroaudit_quintana.merma 
							INNER JOIN agroaudit_quintana.merma_detalle ON merma.id = merma_detalle.id_merma
							WHERE year = $year
								AND porcentaje_merma > 0 
								AND campo = 'Total Peso Merma Neta'
								AND id_finca = $finca_quintana
								AND tipo_merma = 'NETA'
								$sWhere
							) / SUM(peso_neto) * 100 AS cantidad
						FROM agroaudit_quintana.merma 
						WHERE year = $year AND porcentaje_merma > 0 AND id_finca = $finca_quintana AND tipo_merma = 'NETA' $sWhere
					) tbl
				) AS quintana";
			$merma_neta = $this->db->queryRow($sql);
			$merma_neta->agroban = round(
				(((float)$merma_neta->laniado) + ((float)$merma_neta->marun) + ((float)$merma_neta->reiset) + ((float)$merma_neta->quintana))
				/ (
					((float)$merma_neta->laniado > 0 ? 1 : 0) + 
					((float)$merma_neta->marun > 0 ? 1 : 0) + 
					((float)$merma_neta->reiset > 0 ? 1 : 0) + 
					((float)$merma_neta->quintana > 0 ? 1 : 0)
				)
			, 2);
		// RECOBRO
			$sql = "SELECT
				'RECOBRO' as name,
				(
					SELECT ROUND(AVG(cantidad), 2)
					FROM (
						SELECT (racimos+caidos)/enfunde*100 cantidad
						FROM (
							SELECT semana, SUM(IF(tipo='RACIMOS',cantidad,0)) racimos, SUM(IF(tipo='CAIDOS',cantidad,0)) caidos, SUM(IF(tipo='ENFUNDE',cantidad,0)) enfunde
							FROM (
								SELECT semana, SUM(usadas) cantidad, 'ENFUNDE' AS tipo
								FROM agroaudit_marcel.produccion_enfunde
								WHERE years = $year AND semana < $week-5 $sWhere
								GROUP BY semana
								UNION ALL
								SELECT semana_enfundada, COUNT(1) cantidad, 'RACIMOS' AS tipo
								FROM agroaudit_marcel.produccion_historica
								WHERE anio_enfundado = $year AND semana_enfundada < $week-5 $sWhereEnfundada
								GROUP BY semana_enfundada
								UNION
								SELECT semana_enfundada, SUM(cantidad) cantidad, 'CAIDOS' AS tipo
								FROM agroaudit_marcel.produccion_racimos_caidos
								WHERE anio_enfundado = $year AND semana_enfundada < $week-5 $sWhereEnfundada
								GROUP BY semana_enfundada
							) tbl
							GROUP BY semana
							HAVING racimos > 0 && enfunde > 0
						) tbl
						GROUP BY semana
						HAVING cantidad > 60
					) tbl
				) AS laniado,
				(
					''
				) AS marun,
				(
					SELECT ROUND(AVG(cantidad), 2)
					FROM (
						SELECT (racimos+caidos)/enfunde*100 cantidad
						FROM (
							SELECT semana, SUM(IF(tipo='RACIMOS',cantidad,0)) racimos, SUM(IF(tipo='CAIDOS',cantidad,0)) caidos, SUM(IF(tipo='ENFUNDE',cantidad,0)) enfunde
							FROM (
								SELECT semana, SUM(usadas) cantidad, 'ENFUNDE' AS tipo
								FROM agroaudit_reiset.produccion_enfunde
								WHERE years = $year $sWhere
								GROUP BY semana
								UNION ALL
								SELECT semana_enfundada, COUNT(1) cantidad, 'RACIMOS' AS tipo
								FROM agroaudit_reiset.produccion_racimos_formularios
								WHERE anio_enfundado = $year AND $week-5 $sWhereEnfundada
								GROUP BY semana_enfundada
							) tbl
							GROUP BY semana
							HAVING racimos > 0 && enfunde > 0
						) tbl
						GROUP BY semana
					) tbl
				) AS reiset,
				(
					''
				) AS quintana";
			$recobro = $this->db->queryRow($sql);
			$recobro->agroban = round(
				(((float)$recobro->laniado) + ((float)$recobro->marun) + ((float)$recobro->reiset) + ((float)$recobro->quintana))
				/ (
					((float)$recobro->laniado > 0 ? 1 : 0) + 
					((float)$recobro->marun > 0 ? 1 : 0) + 
					((float)$recobro->reiset > 0 ? 1 : 0) + 
					((float)$recobro->quintana > 0 ? 1 : 0)
				)
			, 2);

		$response->data = [
			$peso_racimo_prom,
			$edad_prom,
			$tallo,
			$recusados,
			$calibre,
			$manos,
			$muestreo,
			$ratio_cortado,
			$ratio_procesado,
			$merma_cortada,
			$merma_procesada,
			$merma_neta,
			$recobro
		];

		return $response;
	}
    
    private function grafica_z($data = [], $group_y = [], $selected = null, $types = []){
		$options = [];
		$options["tooltip"] = [
			"trigger" => 'axis',
			"axisPointer" => [
				"type" => 'cross',
				"crossStyle" => [
					"color" => '#999'
				]
			]
		];
		$options["toolbox"] = [
			"feature" => [
				"dataView" => [
					"show" => true,
					"readOnly" => false
				],
				"magicType" => [
					"show" => true,
					"type" => ['line', 'bar']
				],
				"restore" => [
					"show" => true
				],
				"saveAsImage" => [
					"show" => true
				]
			]
		];
		$options["legend"]["data"] = [];
		$options["legend"]["bottom"] = "0%";
        $options["legend"]["left"] = "center";
        $options["legend"]["selected"] = $selected;
		$options["xAxis"] = [
			[
				"type" => 'category',
				"data" => [],
				"axisPointer" => [
					"type" => 'shadow'
				]
			]
		];
		/*
			[
				type => 'value',
				name => {String},
				min => 0,
				max => 200,
				interval => 5,
				axisLabel => [
					formatter => {value} KG
				]
			]
		*/
		$options["yAxis"] = [];
		/*
			[
				name => {String},
				type => 'line',
				data => [
					{double}, {double}, {double}
				]
			]
		*/
		$options["series"] = [];

		$maxs = [];
		$mins = [];
		$prepare_data = [];
		$_x = [];
		$_names = [];
		$_namess = [];
		foreach($data as $d){
			$d = (object) $d;
			if(!isset($maxs[$d->index_y])) if($d->value > 0)
				$maxs[$d->index_y] = $d->value;
			if($d->value > $maxs[$d->index_y]) if($d->value > 0)
				$maxs[$d->index_y] = $d->value;

			if(!isset($mins[$d->index_y])) if($d->value > 0)
				$mins[$d->index_y] = $d->value;
			if($d->value < $mins[$d->index_y]) if($d->value > 0)
				$mins[$d->index_y] = $d->value;

			if(!in_array($d->label_x, $_x)){
				$_x[] = $d->label_x;
			}
			if(!in_array($d->name, $_namess)){
				$_namess[] = $d->name;
				 
				$n = ["name" => $d->name, "group" => $d->index_y];
				if(isset($d->line)){
					$n["line"] = $d->line;
				}
				$_names[] = $n;
			}
			$prepare_data[$d->label_x][$d->name] = $d->value;
        }

		foreach($group_y as $key => $col){
			$col = (object) $col;
			$options["yAxis"][] = [
				'type' => 'value',
				'name' => $col->name,
				//'max' => ($key == 1) ? $maxs[$key] + ($maxs[$key] - $mins[$key]) * .05 : null,
                //'min' => ($key == 1) ? $mins[$key] - ($maxs[$key] - $mins[$key]) * .05 : null,
                'max' => null,
                'min' => 'dataMin',
				'axisLabel' => [
					'formatter' => "{value} $col->format"
				]
			];
		}

		foreach($_x as $row){
			$options["xAxis"][0]["data"][] = $row;
        }

		foreach($_names as $i => $name){
			$name = (object) $name;

			if(!in_array($name->name, $options["legend"]["data"]))
				$options["legend"]["data"][] = $name->name;

			$serie = [
				"name" => $name->name,
				"type" => isset($types[$i]) ? $types[$i] : 'line',
				"connectNulls" => true,
				"data" => []
			];
			if($name->group > 0)
				$serie["yAxisIndex"] = $name->group;

			if(isset($name->line)){
				$serie["itemStyle"]["normal"]["lineStyle"]["width"] = 5;
			}

			foreach($_x as $row){
				$val = 0;
				if(isset($prepare_data[$row][$name->name]))
					$val = $prepare_data[$row][$name->name];

				if($val > 0)
					$serie["data"][] = $val;
				else
					$serie["data"][] = null;
			}
			$options["series"][] = $serie;
		}

		return $options;
	}
	
}
