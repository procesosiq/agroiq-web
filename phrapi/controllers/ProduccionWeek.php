<?php defined('PHRAPI') or die("Direct access not allowed!");

class ProduccionWeek {
	public $name;
	private $db;
	private $config;

	public function __construct(){
		$this->config = $GLOBALS['config'];
		$this->session = Session::getInstance();
		// D($this->session);
        $this->maindb = DB::getInstance();
        $this->db = DB::getInstance($this->session->agent_user);
	}


	public function index(){
		$postdata = (object)json_decode(file_get_contents("php://input"));

		$filters = (object)[
			"finca" => getValueFrom($postdata , "finca" , "" , FILTER_SANITIZE_STRING),
			"idLote" => getValueFrom($postdata , "idLote" , "" , FILTER_SANITIZE_STRING),
			"type" => getValueFrom($postdata , "type" , "cosechada" , FILTER_SANITIZE_STRING),
			"fecha_inicial" => getValueFrom($postdata , "fecha_inicial" , "" , FILTER_SANITIZE_STRING),
			"fecha_final" => getValueFrom($postdata , "fecha_final" , "" , FILTER_SANITIZE_STRING),
			"year" => getValueFrom($postdata , "year" , "" , FILTER_SANITIZE_STRING),
		];

		$sWhere = "";

		// if($filters->fecha_final != "" && $filters->fecha_inicial != ""){
		// 	$sWhere .= " AND DATE(fecha) BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}'";
		// }

		$sYear = " AND YEAR(fecha) = YEAR(CURRENT_DATE)";
		if($filters->idLote != "" && (int)$filters->idLote > 0){
			$sWhere .= " AND lote = '{$filters->idLote}'";
		}
		if($filters->year != "" && (int)$filters->year > 0){
			$sYear = " AND year = '{$filters->year}' ";
		}

		$sYearCampo = "YEAR(fecha) AS year";
		$sSemana = "WEEK(p.fecha)";
		if($this->session->id_company == 2 || $this->session->id_company == 3){
			$sSemana = "semana";
			$sYearCampo = "year";
		}

		$response = new stdClass;

		if($this->session->id_company == 2 || $this->session->id_company == 3){
			$sSemana = "p.semana";
			$sql = "SELECT $sSemana AS semana , $sYearCampo,
					SUM(total_cosechados) AS total_cosechados , 
					SUM(total_recusados) AS total_recusados,
					(SUM(total_cosechados) - SUM(total_recusados)) AS total_procesada,
					'false' AS expanded,
					((SELECT SUM(caja) AS caja FROM produccion_cajas WHERE year = YEAR(CURRENT_DATE) AND semana = p.semana GROUP BY semana) /
					SUM((total_cosechados))
					) AS ratio_cortado,
					((SELECT SUM(caja) AS caja FROM produccion_cajas WHERE year = YEAR(CURRENT_DATE) AND semana = p.semana GROUP BY semana) /
					SUM((total_cosechados-total_recusados))
					) AS ratio_procesado,
					(SELECT COUNT(*) FROM produccion_muestra WHERE WEEK(fecha) = $sSemana {$sYear}) AS muestrados,
					(SELECT AVG(peso) FROM produccion_peso WHERE semana = $sSemana {$sYear}) AS peso,
					(SELECT AVG(largo_dedos) FROM produccion_muestra WHERE WEEK(fecha) = $sSemana {$sYear}) AS largo_dedos,
					(SELECT AVG(calibracion) FROM produccion_calibracion WHERE semana = $sSemana {$sYear}) AS calibracion,
					(SELECT AVG(manos) FROM produccion_manos WHERE semana = $sSemana {$sYear}) AS manos
					FROM produccion AS p
					WHERE 1 = 1 {$sWhere}
					{$sYear}
					GROUP BY $sSemana";
			// D($sql);
		}else{
			$sql = "SELECT $sSemana AS semana , $sYearCampo,
					SUM(total_cosechados) AS total_cosechados , 
					SUM(total_recusados) AS total_recusados,
					(SUM(total_cosechados) - SUM(total_recusados)) AS total_procesada,
					'false' AS expanded,
					'0' AS ratio_cortado,
					'0' AS ratio_procesado,
					(SELECT COUNT(*) FROM produccion_muestra WHERE WEEK(fecha) = $sSemana {$sYear}) AS muestrados,
					(SELECT AVG(peso) FROM produccion_muestra WHERE WEEK(fecha) = $sSemana {$sYear}) AS peso,
					(SELECT AVG(largo_dedos) FROM produccion_muestra WHERE WEEK(fecha) = $sSemana {$sYear}) AS largo_dedos,
					(SELECT AVG(calibracion) FROM produccion_muestra WHERE WEEK(fecha) = $sSemana {$sYear}) AS calibracion,
					(SELECT AVG(manos) FROM produccion_muestra WHERE WEEK(fecha) = $sSemana {$sYear}) AS manos
					FROM produccion AS p
					WHERE 1 = 1 {$sWhere}
					{$sYear}
					GROUP BY $sSemana";
		}

		$data = $this->db->queryAll($sql);		
		$response->data = [];
		$response->lotes = [];
		/*----------  GRAFICAS  ----------*/
		$racimos = [];
		$manos = [];
		$peso = [];
		$calibre = [];
		$dedo = [];
		$response->racimos = [];
		$response->manos = [];
		$response->calibre = [];
		$response->dedo = [];
		/*----------  GRAFICAS  ----------*/
		$dataMax = [];
		foreach ($data as $key => $value) {
			$value = (object)$value;
			$response->data["totales"]["semanas"][$value->semana]["campo"] = (int)$value->semana;
			$response->data["totales"]["semanas"][$value->semana]["expanded"] = false;
			$response->data["totales"]["semanas"][$value->semana]["lotes"] = $this->getLotes((int)$value->semana);
			$response->data["totales"]["semanas"][$value->semana]["muestrados"] = (float)$value->muestrados;
			$response->data["totales"]["semanas"][$value->semana]["peso"] = (float)$value->peso;
			$response->data["totales"]["semanas"][$value->semana]["manos"] = (float)$value->manos;
			$response->data["totales"]["semanas"][$value->semana]["largo_dedos"] = (float)$value->largo_dedos;
			$response->data["totales"]["semanas"][$value->semana]["calibracion"] = (float)$value->calibracion;
			$response->data["totales"]["semanas"][$value->semana]["total_cosechados"] = (float)$value->total_cosechados;
			$response->data["totales"]["semanas"][$value->semana]["total_recusados"] = (float)$value->total_recusados;
			$response->data["totales"]["semanas"][$value->semana]["total_procesada"] = (float)$value->total_procesada;
			$response->data["totales"]["semanas"][$value->semana]["ratio_procesado"] = (float)$value->ratio_procesado;
			$response->data["totales"]["semanas"][$value->semana]["ratio_cortado"] = (float)$value->ratio_cortado;
			
			$response->data["totales"]["campo"] = "2016";
			$response->data["totales"]["expanded"] = false;
			$response->data["totales"]["muestrados"] += (float)$value->muestrados;
			$response->data["totales"]["peso"] += (float)$value->peso;
			$response->data["totales"]["largo_dedos"] += (float)$value->largo_dedos;
			$response->data["totales"]["calibracion"] += (float)$value->calibracion;
			$response->data["totales"]["manos"] += (float)$value->manos;
			$response->data["totales"]["total_cosechados"] += (float)$value->total_cosechados;
			$response->data["totales"]["total_recusados"] += (float)$value->total_recusados;
			$response->data["totales"]["total_procesada"] += (float)$value->total_procesada;
			$response->data["totales"]["ratio_procesado"] += (float)$value->ratio_procesado;
			$response->data["totales"]["ratio_cortado"] += (float)$value->ratio_cortado;

			if((float)$value->muestrados > 0){
				$dataMax["muestrados"][] = (float)$value->muestrados;
			}
			if((float)$value->peso > 0){
				$dataMax["peso"][] = (float)$value->peso;
			}
			if((float)$value->largo_dedos > 0){
				$dataMax["largo_dedos"][] = (float)$value->largo_dedos;
			}
			if((float)$value->calibracion > 0){
				$dataMax["calibracion"][] = (float)$value->calibracion;
			}
			if((float)$value->total_cosechados > 0){
				$dataMax["total_cosechados"][] = (float)$value->total_cosechados;
			}
			if((float)$value->total_recusados > 0){
				$dataMax["total_recusados"][] = (float)$value->total_recusados;
			}
			if((float)$value->total_procesada > 0){
				$dataMax["total_procesada"][] = (float)$value->total_procesada;
			}
			if((float)$value->ratio_procesado > 0){
				$dataMax["ratio_procesado"][] = (float)$value->ratio_procesado;
			}
			if((float)$value->ratio_cortado > 0){
				$dataMax["ratio_cortado"][] = (float)$value->ratio_cortado;
			}
			if((float)$value->manos > 0){
				$dataMax["manos"][] = (float)$value->manos;
			}

			/*----------  GRAFICAS  ----------*/
			$racimos[] = [
				"label" => $value->semana,
				"position" => 0,
				"legend" => "Cortados",
				"value" => (double)$value->total_cosechados,
			];
			$racimos[] = [
				"label" => $value->semana,
				"position" => 1,
				"legend" => "Recusados",
				"value" => (double)$value->total_recusados,
			];
			$racimos[] = [
				"label" => $value->semana,
				"position" => 2,
				"legend" => "Procesados",
				"value" => (double)$value->total_procesada,
			];

			$peso[$value->semana] = [
				"label" => $value->semana,
				"position" => 0,
				"legend" => "Peso",
				"value" => (double)$value->peso,
			];
			$manos[] = [
				"label" => $value->semana,
				"position" => 0,
				"legend" => "Manos",
				"value" => (double)$value->manos,
			];
			$calibre[] = [
				"label" => $value->semana,
				"position" => 0,
				"legend" => "Calibración",
				"value" => (double)$value->calibracion,
			];
			$dedo[] = [
				"label" => $value->semana,
				"position" => 0,
				"legend" => "Dedos",
				"value" => (double)$value->largo_dedos,
			];
			/*----------  GRAFICAS  ----------*/
		}

		$response->data["max"]["campo"] = "MAX";
		$response->data["max"]["muestrados"] = (double)max($dataMax["muestrados"]);
		$response->data["max"]["peso"] = (double)max($dataMax["peso"]);
		$response->data["max"]["largo_dedos"] = (double)max($dataMax["largo_dedos"]);
		$response->data["max"]["calibracion"] = (double)max($dataMax["calibracion"]);
		$response->data["max"]["manos"] = (double)max($dataMax["manos"]);
		$response->data["max"]["total_cosechados"] = (double)max($dataMax["total_cosechados"]);
		$response->data["max"]["total_recusados"] = (double)max($dataMax["total_recusados"]);
		$response->data["max"]["total_procesada"] = (double)max($dataMax["total_procesada"]);
		$response->data["max"]["ratio_procesado"] = (double)max($dataMax["ratio_procesado"]);
		$response->data["max"]["ratio_cortado"] = (double)max($dataMax["ratio_cortado"]);

		$response->data["min"]["campo"] = "MIN";
		$response->data["min"]["muestrados"] = (double)min($dataMax["muestrados"]);
		$response->data["min"]["peso"] = (double)min($dataMax["peso"]);
		$response->data["min"]["largo_dedos"] = (double)min($dataMax["largo_dedos"]);
		$response->data["min"]["calibracion"] = (double)min($dataMax["calibracion"]);
		$response->data["min"]["manos"] = (double)min($dataMax["manos"]);
		$response->data["min"]["total_cosechados"] = (double)min($dataMax["total_cosechados"]);
		$response->data["min"]["total_recusados"] = (double)min($dataMax["total_recusados"]);
		$response->data["min"]["total_procesada"] = (double)min($dataMax["total_procesada"]);
		$response->data["min"]["ratio_procesado"] = (double)min($dataMax["ratio_procesado"]);
		$response->data["min"]["ratio_cortado"] = (double)min($dataMax["ratio_cortado"]);

		$response->data["avg"]["campo"] = "AVG";
		$response->data["avg"]["muestrados"] = (double)(array_sum($dataMax["muestrados"]) / count($dataMax["muestrados"]));
		$response->data["avg"]["peso"] = (double)(array_sum($dataMax["peso"]) / count($dataMax["peso"]));
		$response->data["totales"]["peso"] = (double)(array_sum($dataMax["peso"]) / count($dataMax["peso"]));
		$response->data["avg"]["largo_dedos"] = (double)(array_sum($dataMax["largo_dedos"]) / count($dataMax["largo_dedos"]));
		$response->data["totales"]["largo_dedos"] = (double)(array_sum($dataMax["largo_dedos"]) / count($dataMax["largo_dedos"]));
		$response->data["avg"]["calibracion"] = (double)(array_sum($dataMax["calibracion"]) / count($dataMax["calibracion"]));
		$response->data["totales"]["calibracion"] = (double)(array_sum($dataMax["calibracion"]) / count($dataMax["calibracion"]));
		$response->data["avg"]["manos"] = (double)(array_sum($dataMax["manos"]) / count($dataMax["manos"]));
		$response->data["totales"]["manos"] = (double)(array_sum($dataMax["manos"]) / count($dataMax["manos"]));
		$response->data["avg"]["total_cosechados"] = (double)(array_sum($dataMax["total_cosechados"]) / count($dataMax["total_cosechados"]));
		$response->data["avg"]["total_recusados"] = (double)(array_sum($dataMax["total_recusados"]) / count($dataMax["total_recusados"]));
		$response->data["avg"]["total_procesada"] = (double)(array_sum($dataMax["total_procesada"]) / count($dataMax["total_procesada"]));
		$response->data["avg"]["ratio_procesado"] = (double)(array_sum($dataMax["ratio_procesado"]) / count($dataMax["ratio_procesado"]));
		$response->data["totales"]["ratio_procesado"] = (double)(array_sum($dataMax["ratio_procesado"]) / count($dataMax["ratio_procesado"]));
		$response->data["avg"]["ratio_cortado"] = (double)(array_sum($dataMax["ratio_cortado"]) / count($dataMax["ratio_cortado"]));
		$response->data["totales"]["ratio_cortado"] = (double)(array_sum($dataMax["ratio_cortado"]) / count($dataMax["ratio_cortado"]));

		/* BEGIN CAMBIO DE MARCEL */
		$response->anios = $this->db->queryAllSpecial("SELECT year as id, year as label FROM produccion GROUP BY year");
		#mario
		if($this->session->id_company == 3){
			$response->anios = $this->db->queryAllSpecial("SELECT YEAR AS id, YEAR AS label FROM produccion WHERE year != '' GROUP BY YEAR ORDER BY YEAR DESC");
		}
		/* END CAMBIO DE MARCEL */

		/*
		* mario / tabla de produccion
		*/
		if($this->session->id_company == 3){
			unset($response->data["totales"]);
			/*
			*	MIN
			*	MAX
			*	AVG
			*/
			$response->data["max"]["campo"] = "MAX";
			$response->data["min"]["campo"] = "MIN";
			$response->data["avg"]["campo"] = "AVG";

			// racimos cortados
			$min = $this->db->queryRow("SELECT MIN(racimos_cortados) AS racimos_cortados_min, MAX(racimos_cortados) as racimos_cortados_max, AVG(racimos_cortados) as racimos_cortados_avg
				FROM(SELECT SUM(racimos_cortados) AS racimos_cortados FROM produccion WHERE YEAR = '{$filters->year}' AND racimos_cortados > 0 GROUP BY YEAR, semana) AS tbl");
			$response->data["min"]["racimos_cortados"] = $min->racimos_cortados_min;
			$response->data["max"]["racimos_cortados"] = $min->racimos_cortados_max;
			$response->data["avg"]["racimos_cortados"] = $min->racimos_cortados_avg;

			// racimos procesados
			$min = $this->db->queryRow("SELECT MIN(racimos_procesados) AS racimos_procesados_min, MAX(racimos_procesados) as racimos_procesados_max, AVG(racimos_procesados) as racimos_procesados_avg
				FROM(SELECT SUM(racimos_procesados) AS racimos_procesados FROM produccion WHERE YEAR = '{$filters->year}' AND racimos_procesados > 0 GROUP BY YEAR, semana) AS tbl");
			$response->data["min"]["racimos_procesados"] = $min->racimos_procesados_min;
			$response->data["max"]["racimos_procesados"] = $min->racimos_procesados_max;
			$response->data["avg"]["racimos_procesados"] = $min->racimos_procesados_avg;

			// peso prom cajas primera
			$min = $this->db->queryRow("SELECT MIN(cajas_primera) AS cajas_primera_min, MAX(cajas_primera) as cajas_primera_max, AVG(cajas_primera) as cajas_primera_avg
				FROM(SELECT AVG(cajas_primera) AS cajas_primera FROM produccion WHERE YEAR = '{$filters->year}' AND cajas_primera > 0 GROUP BY YEAR, semana) AS tbl");
			$response->data["min"]["peso_prom_cajas"] = 19.09;
			$response->data["max"]["peso_prom_cajas"] = 19.09;
			$response->data["avg"]["peso_prom_cajas"] = 19.09;

			// peso prom cajas segunda
			$min = $this->db->queryRow("SELECT MIN(cajas_segunda) AS cajas_segunda_min, MAX(cajas_segunda) as cajas_segunda_max, AVG(cajas_segunda) as cajas_segunda_avg
				FROM(SELECT AVG(cajas_segunda) AS cajas_segunda FROM produccion WHERE YEAR = '{$filters->year}' AND cajas_segunda > 0 GROUP BY YEAR, semana) AS tbl");
			$response->data["min"]["peso_prom_otras_cajas"] = 21.82;
			$response->data["max"]["peso_prom_otras_cajas"] = 21.82;
			$response->data["avg"]["peso_prom_otras_cajas"] = 21.82;

			// calibracion
			$min = $this->db->queryRow("SELECT MIN(calibracion) AS calibracion_min, MAX(calibracion) as calibracion_max, AVG(calibracion) as calibracion_avg
				FROM(SELECT SUM(calibracion) AS calibracion FROM produccion WHERE YEAR = '{$filters->year}' AND calibracion > 0 GROUP BY YEAR, semana) AS tbl");
			$response->data["min"]["calibracion"] = $min->calibracion_min;
			$response->data["max"]["calibracion"] = $min->calibracion_max;
			$response->data["avg"]["calibracion"] = $min->calibracion_avg;

			// peso prom racimos
			$min = $this->db->queryRow("SELECT MIN(peso) AS peso_min, MAX(peso) as peso_max, AVG(peso) as peso_avg
				FROM(SELECT SUM(peso) AS peso FROM produccion WHERE YEAR = '{$filters->year}' AND peso > 0 GROUP BY YEAR, semana) AS tbl");
			$response->data["min"]["peso_prom_racimos"] = $min->peso_min;
			$response->data["max"]["peso_prom_racimos"] = $min->peso_max;
			$response->data["avg"]["peso_prom_racimos"] = $min->peso_avg;

			// ratio cortado
			$min = $this->db->queryRow("SELECT MIN(ratio_cortado) AS ratio_cortado_min, MAX(ratio_cortado) as ratio_cortado_max, AVG(ratio_cortado) as ratio_cortado_avg
				FROM(SELECT ROUND(AVG(IFNULL(cajas_primera, 0)/IFNULL(racimos_cortados, 0)), 2) AS ratio_cortado FROM produccion WHERE YEAR = '{$filters->year}' GROUP BY semana) AS tbl");
			$response->data["min"]["ratio_cortado"] = $min->ratio_cortado_min;
			$response->data["max"]["ratio_cortado"] = $min->ratio_cortado_max;
			$response->data["avg"]["ratio_cortado"] = $min->ratio_cortado_avg;

			// ratio procesado
			$min = $this->db->queryRow("SELECT MIN(ratio_procesado) AS ratio_procesado_min, MAX(ratio_procesado) as ratio_procesado_max, AVG(ratio_procesado) as ratio_procesado_avg
				FROM(SELECT ROUND(AVG(IFNULL(cajas_primera, 0)/IFNULL(racimos_procesados, 0)), 2) AS ratio_procesado FROM produccion WHERE YEAR = '{$filters->year}' GROUP BY semana) AS tbl");
			$response->data["min"]["ratio_procesado"] = $min->ratio_procesado_min;
			$response->data["max"]["ratio_procesado"] = $min->ratio_procesado_max;
			$response->data["avg"]["ratio_procesado"] = $min->ratio_procesado_avg;

			// merma cosechada
			$min = $this->db->queryRow("SELECT MIN(merma_cosechada) AS merma_cosechada_min, MAX(merma_cosechada) as merma_cosechada_max, AVG(merma_cosechada) as merma_cosechada_avg
				FROM(SELECT ROUND(AVG(((peso*racimos_cortados)-(cajas_primera*19.09))/(peso*racimos_cortados)*100),2) AS merma_cosechada FROM produccion WHERE YEAR = '{$filters->year}' GROUP BY semana) AS tbl");
			$response->data["min"]["merma_cosechada"] = $min->merma_cosechada_min;
			$response->data["max"]["merma_cosechada"] = $min->merma_cosechada_max;
			$response->data["avg"]["merma_cosechada"] = $min->merma_cosechada_avg;

			// merma procesada
			$min = $this->db->queryRow("SELECT MIN(merma_procesada) AS merma_procesada_min, MAX(merma_procesada) as merma_procesada_max, AVG(merma_procesada) as merma_procesada_avg
				FROM(SELECT ROUND(AVG(((peso*racimos_procesados)-(cajas_primera*19.09))/(peso*racimos_procesados)*100),2) AS merma_procesada FROM produccion WHERE YEAR = '{$filters->year}' GROUP BY semana) AS tbl");
			$response->data["min"]["merma_procesada"] = $min->merma_procesada_min;
			$response->data["max"]["merma_procesada"] = $min->merma_procesada_max;
			$response->data["avg"]["merma_procesada"] = $min->merma_procesada_avg;

			// merma neta
			$response->data["min"]["merma_neta"] = $min->merma_procesada_min-10;
			$response->data["max"]["merma_neta"] = $min->merma_procesada_max-10;
			$response->data["avg"]["merma_neta"] = $min->merma_procesada_avg-10;

			// merma kg
			$min = $this->db->queryRow("SELECT MIN(merma_kg) AS merma_kg_min, MAX(merma_kg) as merma_kg_max, AVG(merma_kg) as merma_kg_avg
				FROM(SELECT ROUND(acum * (SELECT ROUND(AVG(((peso*racimos_procesados)-(cajas_primera*19.09))/(peso*racimos_procesados)*100),2) AS merma_kg FROM produccion WHERE YEAR = '{$filters->year}' AND semana = main.semana),2) as merma_kg FROM(SELECT SUM(peso) AS acum, semana FROM produccion WHERE YEAR = '{$filters->year}' GROUP BY semana) AS main) AS tbl");
			$response->data["min"]["merma_kg"] = $min->merma_kg_min;
			$response->data["max"]["merma_kg"] = $min->merma_kg_max;
			$response->data["avg"]["merma_kg"] = $min->merma_kg_avg;
			
			// merma cajas
			$min = $this->db->queryRow("SELECT MIN(merma_cajas) AS merma_cajas_min, MAX(merma_cajas) AS merma_cajas_max, AVG(merma_cajas) AS merma_cajas_avg
				FROM(
					SELECT ROUND(acum * ((
						SELECT ROUND(AVG(((peso*racimos_procesados)-(cajas_primera*19.09))/(peso*racimos_procesados)*100),2) AS merma_procesada
						FROM produccion 
						WHERE YEAR = '{$filters->year}' AND semana = main.semana)-10), 2) / 19.09 AS merma_cajas
					FROM(SELECT SUM(peso) AS acum, semana 
						FROM produccion WHERE YEAR = '{$filters->year}' 
						GROUP BY semana) AS main
				) AS tbl");
			$response->data["min"]["merma_cajas"] = $min->merma_cajas_min;
			$response->data["max"]["merma_cajas"] = $min->merma_cajas_max;
			$response->data["avg"]["merma_cajas"] = $min->merma_cajas_avg;

			$response->data["min"]["merma_dolares"] = $min->merma_cajas_min * 6.26;
			$response->data["max"]["merma_dolares"] = $min->merma_cajas_max * 6.26;
			$response->data["avg"]["merma_dolares"] = $min->merma_cajas_avg * 6.26;

			// # manos
			$min = $this->db->queryRow("SELECT MIN(manos) AS manos_min, MAX(manos) as manos_max, AVG(manos) as manos_avg
				FROM(SELECT SUM(manos) AS manos FROM produccion WHERE YEAR = '{$filters->year}' AND manos > 0 GROUP BY YEAR, semana) AS tbl");
			$response->data["min"]["manos"] = $min->manos_min;
			$response->data["max"]["manos"] = $min->manos_max;
			$response->data["avg"]["manos"] = $min->manos_avg;
		}

		if($this->session->id_company == 3)
		foreach($response->anios as $year){
			$response->data[$year]["campo"] = $year;
			
			$racimos_cortados = $this->db->queryAllSpecial("SELECT semana as id, SUM(racimos_cortados) as label FROM produccion WHERE YEAR = '{$year}' AND racimos_cortados > 0 GROUP BY YEAR, semana");
			$racimos_procesados = $this->db->queryAllSpecial("SELECT semana as id, SUM(racimos_procesados) AS label FROM produccion WHERE YEAR = '{$year}' AND racimos_procesados > 0 GROUP BY YEAR, semana");
			$peso_prom_racimos = $this->db->queryAllSpecial("SELECT semana as id, ROUND(AVG(IFNULL(peso, 0)),2) AS label FROM produccion WHERE YEAR = '{$year}' GROUP BY YEAR, semana");
			$ratio_cortado = $this->db->queryAllSpecial("SELECT semana as id, ROUND(AVG(IFNULL(cajas_primera, 0)/IFNULL(racimos_cortados, 0)), 2) AS label FROM produccion WHERE YEAR = '{$year}' GROUP BY YEAR, semana");
			$ratio_procesado = $this->db->queryAllSpecial("SELECT semana as id, ROUND(AVG(IFNULL(cajas_primera, 0)/IFNULL(racimos_procesados, 0)), 2) AS label FROM produccion WHERE YEAR = '{$year}' GROUP BY YEAR, semana");
			$calibracion = $this->db->queryAllSpecial("SELECT semana as id, SUM(calibracion) AS label FROM produccion WHERE YEAR = '{$year}' AND calibracion > 0 GROUP BY YEAR, semana");
			$merma_cosechada = $this->db->queryAllSpecial("SELECT semana as id, ROUND(AVG(((peso*racimos_cortados)-(cajas_primera*19.09))/(peso*racimos_cortados)*100),2) AS label FROM produccion WHERE YEAR = '{$year}' GROUP BY semana");
			$merma_procesada = $this->db->queryAllSpecial("SELECT semana as id, ROUND(AVG(((peso*racimos_procesados)-(cajas_primera*19.09))/(peso*racimos_procesados)*100),2) AS label FROM produccion WHERE YEAR = '{$year}' GROUP BY YEAR, semana");
			$merma_kg = $this->db->queryAllSpecial("SELECT semana as id, ROUND(acum * (SELECT ROUND(AVG(((peso*racimos_procesados)-(cajas_primera*19.09))/(peso*racimos_procesados)*100),2) AS label FROM produccion WHERE YEAR = '{$year}' AND semana = main.semana),2) as label FROM(SELECT SUM(peso) AS acum, semana FROM produccion WHERE YEAR = '{$year}' GROUP BY semana) AS main HAVING id IS NOT NULL");
			$merma_cajas = $this->db->queryAllSpecial("SELECT ROUND(acum * ((
						SELECT ROUND(AVG(((peso*racimos_procesados)-(cajas_primera*19.09))/(peso*racimos_procesados)*100),2) AS merma_procesada
						FROM produccion 
						WHERE YEAR = '{$year}' AND semana = main.semana)-10), 2) / 19.09 AS label, semana as id
					FROM(SELECT SUM(peso) AS acum, semana 
						FROM produccion WHERE YEAR = '{$year}' 
						GROUP BY semana) as main
					HAVING id IS NOT NULL");
			$manos = $this->db->queryAllSpecial("SELECT semana as id, SUM(manos) AS label FROM produccion WHERE YEAR = '{$year}' AND manos > 0 GROUP BY YEAR, semana");

			for($semana = 1; $semana <= 52; $semana++){
				$response->data[$year]["semanas"][$semana]["campo"] = $semana;

				$response->data[$year]["semanas"][$semana]["racimos_cortados"] = $racimos_cortados[$semana];
				$response->data[$year]["racimos_cortados"] += $racimos_cortados[$semana];

				$response->data[$year]["semanas"][$semana]["racimos_procesados"] = $racimos_procesados[$semana];
				$response->data[$year]["racimos_procesados"] += $racimos_procesados[$semana];

				$response->data[$year]["semanas"][$semana]["peso_prom_cajas"] = 19.09;
				$response->data[$year]["peso_prom_cajas"] += 19.09;

				$response->data[$year]["semanas"][$semana]["peso_prom_otras_cajas"] = 21.82;
				$response->data[$year]["peso_prom_otras_cajas"] += 21.82;

				$response->data[$year]["semanas"][$semana]["calibracion"] = $calibracion[$semana];
				$response->data[$year]["calibracion"] += $calibracion[$semana];

				$response->data[$year]["semanas"][$semana]["peso_prom_racimos"] = $peso_prom_racimos[$semana];
				$response->data[$year]["peso_prom_racimos"] += (is_numeric($peso_prom_racimos[$semana])) ? $peso_prom_racimos[$semana] : 0;

				$response->data[$year]["semanas"][$semana]["ratio_cortado"] = $ratio_cortado[$semana];
				$response->data[$year]["ratio_cortado"] += $ratio_cortado[$semana];

				$response->data[$year]["semanas"][$semana]["ratio_procesado"] = $ratio_procesado[$semana];
				$response->data[$year]["ratio_procesado"] += $ratio_procesado[$semana];

				$response->data[$year]["semanas"][$semana]["merma_cosechada"] = $merma_cosechada[$semana];
				$response->data[$year]["merma_cosechada"] += $merma_cosechada[$semana];

				$response->data[$year]["semanas"][$semana]["merma_procesada"] = $merma_procesada[$semana];
				$response->data[$year]["merma_procesada"] += $merma_procesada[$semana];

				$response->data[$year]["semanas"][$semana]["merma_neta"] = $merma_procesada[$semana] - 10;
				$response->data[$year]["merma_neta"] += $merma_procesada[$semana] - 10;

				$response->data[$year]["semanas"][$semana]["merma_kg"] = $merma_kg[$semana];
				$response->data[$year]["merma_kg"] += $merma_kg[$semana];

				$response->data[$year]["semanas"][$semana]["merma_cajas"] = $merma_cajas[$semana];
				$response->data[$year]["merma_cajas"] += $merma_cajas[$semana];

				$response->data[$year]["semanas"][$semana]["merma_dolares"] = $merma_cajas[$semana] * 6.26;
				$response->data[$year]["merma_dolares"] += $merma_cajas[$semana] * 6.26;

				$response->data[$year]["semanas"][$semana]["manos"] = $manos[$semana] * 6.26;
				$response->data[$year]["manos"] += $manos[$semana] * 6.26;
			}

			/*// peso prom cajas primera
			$min = $this->db->queryRow("SELECT MIN(cajas_primera) AS cajas_primera_min, MAX(cajas_primera) as cajas_primera_max, AVG(cajas_primera) as cajas_primera_avg
				FROM(SELECT AVG(cajas_primera) AS cajas_primera FROM produccion WHERE YEAR = '{$filters->year}' AND cajas_primera > 0 GROUP BY YEAR, semana) AS tbl");
			$response->data[$year]["peso_prom_cajas"] = 19.09;

			// peso prom cajas segunda
			$min = $this->db->queryRow("SELECT MIN(cajas_segunda) AS cajas_segunda_min, MAX(cajas_segunda) as cajas_segunda_max, AVG(cajas_segunda) as cajas_segunda_avg
				FROM(SELECT AVG(cajas_segunda) AS cajas_segunda FROM produccion WHERE YEAR = '{$filters->year}' AND cajas_segunda > 0 GROUP BY YEAR, semana) AS tbl");
			$response->data[$year]["peso_prom_otras_cajas"] = 21.82;

			// calibracion
			$min = $this->db->queryRow("SELECT MIN(calibracion) AS calibracion_min, MAX(calibracion) as calibracion_max, AVG(calibracion) as calibracion_avg
				FROM(SELECT SUM(calibracion) AS calibracion FROM produccion WHERE YEAR = '{$filters->year}' AND calibracion > 0 GROUP BY YEAR, semana) AS tbl");
			$response->data[$year]["calibracion"] = $min->calibracion_min;

			// peso prom racimos
			$min = $this->db->queryRow("SELECT MIN(peso) AS peso_min, MAX(peso) as peso_max, AVG(peso) as peso_avg
				FROM(SELECT SUM(peso) AS peso FROM produccion WHERE YEAR = '{$filters->year}' AND peso > 0 GROUP BY YEAR, semana) AS tbl");
			$response->data[$year]["peso_prom_racimos"] = $min->peso_min;

			// ratio cortado
			$min = $this->db->queryRow("SELECT MIN(ratio_cortado) AS ratio_cortado_min, MAX(ratio_cortado) as ratio_cortado_max, AVG(ratio_cortado) as ratio_cortado_avg
				FROM(SELECT ROUND(AVG(IFNULL(cajas_primera, 0)/IFNULL(racimos_cortados, 0)), 2) AS ratio_cortado FROM produccion WHERE YEAR = '{$filters->year}' GROUP BY semana) AS tbl");
			$response->data[$year]["ratio_cortado"] = $min->ratio_cortado_min;

			// ratio procesado
			$min = $this->db->queryRow("SELECT MIN(ratio_procesado) AS ratio_procesado_min, MAX(ratio_procesado) as ratio_procesado_max, AVG(ratio_procesado) as ratio_procesado_avg
				FROM(SELECT ROUND(AVG(IFNULL(cajas_primera, 0)/IFNULL(racimos_procesados, 0)), 2) AS ratio_procesado FROM produccion WHERE YEAR = '{$filters->year}' GROUP BY semana) AS tbl");
			$response->data[$year]["ratio_procesado"] = $min->ratio_procesado_min;

			// merma cosechada
			$min = $this->db->queryRow("SELECT MIN(merma_cosechada) AS merma_cosechada_min, MAX(merma_cosechada) as merma_cosechada_max, AVG(merma_cosechada) as merma_cosechada_avg
				FROM(SELECT ROUND(AVG(((peso*racimos_cortados)-(cajas_primera*19.09))/(peso*racimos_cortados)*100),2) AS merma_cosechada FROM produccion WHERE YEAR = '{$filters->year}' GROUP BY semana) AS tbl");
			$response->data[$year]["merma_cosechada"] = $min->merma_cosechada_min;

			// merma procesada
			$min = $this->db->queryRow("SELECT MIN(merma_procesada) AS merma_procesada_min, MAX(merma_procesada) as merma_procesada_max, AVG(merma_procesada) as merma_procesada_avg, COUNT(merma_procesada) as coun
				FROM(SELECT ROUND(AVG(((peso*racimos_procesados)-(cajas_primera*19.09))/(peso*racimos_procesados)*100),2) AS merma_procesada FROM produccion WHERE YEAR = '{$filters->year}' GROUP BY semana) AS tbl");
			$response->data[$year]["merma_procesada"] = $min->merma_procesada_min;

			// merma neta
			$response->data[$year]["merma_neta"] = $min->merma_procesada_min - (10 * $min->coun);

			// merma kg
			$min = $this->db->queryRow("SELECT MIN(merma_kg) AS merma_kg_min, MAX(merma_kg) as merma_kg_max, AVG(merma_kg) as merma_kg_avg
				FROM(SELECT ROUND(acum * (SELECT ROUND(AVG(((peso*racimos_procesados)-(cajas_primera*19.09))/(peso*racimos_procesados)*100),2) AS merma_kg FROM produccion WHERE YEAR = '{$filters->year}' AND semana = main.semana),2) as merma_kg FROM(SELECT SUM(peso) AS acum, semana FROM produccion WHERE YEAR = '{$filters->year}' GROUP BY semana) AS main) AS tbl");
			$response->data[$year]["merma_kg"] = $min->merma_kg_min;
			
			// merma cajas
			$min = $this->db->queryRow("SELECT MIN(merma_cajas) AS merma_cajas_min, MAX(merma_cajas) AS merma_cajas_max, AVG(merma_cajas) AS merma_cajas_avg
				FROM(
					SELECT ROUND(acum * ((
						SELECT ROUND(AVG(((peso*racimos_procesados)-(cajas_primera*19.09))/(peso*racimos_procesados)*100),2) AS merma_procesada
						FROM produccion 
						WHERE YEAR = '{$filters->year}' AND semana = main.semana)-10), 2) / 19.09 AS merma_cajas
					FROM(SELECT SUM(peso) AS acum, semana 
						FROM produccion WHERE YEAR = '{$filters->year}' 
						GROUP BY semana) AS main
				) AS tbl");
			$response->data[$year]["merma_cajas"] = $min->merma_cajas_min;

			$response->data[$year]["merma_dolares"] = $min->merma_cajas_min * 6.26;

			// # manos
			$min = $this->db->queryRow("SELECT MIN(manos) AS manos_min, MAX(manos) as manos_max, AVG(manos) as manos_avg
				FROM(SELECT SUM(manos) AS manos FROM produccion WHERE YEAR = '{$filters->year}' AND manos > 0 GROUP BY YEAR, semana) AS tbl");
			$response->data[$year]["manos"] = $min->manos_min;*/
		}

		$response->peso = $this->generateDataPeso();
		$response->manos = $this->generateDataManos();
		$response->calibracion = $this->generateDataCalibracion();
		$response->racimos = $this->generateDataCosecha();

		if((int)$response->data["avg"]["peso"] <= 0){
			$response->data["avg"]["peso"] = $response->peso->promedio;
		}

		// $response->manos = $this->chartInit($manos , "vertical" , "Mano");
		// $response->calibre = $this->chartInit($calibre , "vertical" , "Calibración");
		$response->dedo = $this->chartInit($dedo , "vertical" , "Dedos");
		// $response->racimos["cortados"] = $this->chartInit($racimos , "vertical" , "Racimos");
		// $response->racimos["recusados"] = $this->chartInit($racimos["recusados"] , "vertical" , "Recusados");
		// $response->racimos["procesados"] = $this->chartInit($racimos["procesados"] , "vertical" , "Procesados");
		$cosechados_year = (double)$this->db->queryOne("SELECT SUM(cosechados_ha_sem) AS cosechados_ha_sem FROM (
							SELECT semana, SUM(cosechados_sn) AS cosechados_sn ,(SELECT SUM(ha) AS ha FROM produccion_ha
							WHERE year = YEAR(CURRENT_DATE)  AND semana = p.semana
							) AS ha ,
							(
							SUM(cosechados_sn) /
							(SELECT SUM(ha) AS ha FROM produccion_ha
							WHERE year = YEAR(CURRENT_DATE) AND semana = p.semana)
							) AS cosechados_ha_sem
							FROM produccion AS p
							WHERE year = YEAR(CURRENT_DATE)
							GROUP BY semana) AS details");

		$procesados_year = (double)$this->db->queryOne("SELECT SUM(cosechados_ha_sem) AS cosechados_ha_sem FROM (
							SELECT semana, SUM(cosechados_sn) AS cosechados_sn, SUM(recusados_sn) AS recusados_sn,(SELECT SUM(ha) AS ha FROM produccion_ha
							WHERE year = YEAR(CURRENT_DATE)  AND semana = p.semana
							) AS ha ,
							(
							(SUM(cosechados_sn)-SUM(recusados_sn)) /
							(SELECT SUM(ha) AS ha FROM produccion_ha
							WHERE year = YEAR(CURRENT_DATE) AND semana = p.semana)
							) AS cosechados_ha_sem
							FROM produccion AS p
							WHERE year = YEAR(CURRENT_DATE)
							GROUP BY semana) AS details");
		

		$cajas_year = (double)$this->db->queryOne("SELECT SUM(cajas_year) AS cajas FROM (SELECT semana ,SUM(caja) AS caja ,(SELECT SUM(ha) AS ha FROM produccion_ha
						WHERE year = YEAR(CURRENT_DATE)  AND semana = p.semana
							) AS ha,
						(
							SUM(caja) /
							(SELECT SUM(ha) AS ha FROM produccion_ha
							WHERE year = YEAR(CURRENT_DATE)  AND semana = p.semana
							)
						) AS cajas_year
						FROM produccion_cajas AS p
						WHERE year = YEAR(CURRENT_DATE)
						GROUP BY year,semana) AS details");

		$weekofYear = (int)$this->db->queryOne("SELECT MAX(semana) AS semana FROM produccion WHERE year = YEAR(CURRENT_DATE)");

		$response->resumen = [
			"cosechados" => $response->data["totales"]["total_cosechados"],
			"cosechados_year" => $cosechados_year,
			"cosechados_week" => ($cosechados_year / $weekofYear),
			"recusados" => $response->data["totales"]["total_recusados"],
			"procesados" => $response->data["totales"]["total_procesada"],
			"procesados_year" => $procesados_year,
			"procesados_week" => ($procesados_year / $weekofYear),
			"cajas" => (float)$this->db->queryOne("SELECT ROUND(SUM(caja),2) AS cajas FROM produccion_cajas WHERE year = YEAR(CURRENT_DATE)"),
			"cajas_year" => $cajas_year,
			"cajas_week" => ($cajas_year / $weekofYear),
		];

		if($this->session->id_company == 3){
			$response->resumen["cosechados"] = $this->db->queryRow("SELECT SUM(racimos_cortados) AS acum FROM produccion WHERE YEAR = '{$filters->year}'")->acum;
			$response->resumen["recusados"] = $this->db->queryRow("SELECT SUM(racimos_recusados) AS acum FROM produccion WHERE YEAR = '{$filters->year}'")->acum;
			$response->resumen["procesados"] = $this->db->queryRow("SELECT SUM(racimos_procesados) AS acum FROM produccion WHERE YEAR = '{$filters->year}'")->acum;
			
			$response->resumen["cajas"] = $this->db->queryRow("SELECT SUM(cajas_primera ) AS acum FROM produccion WHERE YEAR = '{$filters->year}'")->acum;
			$response->resumen["cajas_dos"] = $this->db->queryRow("SELECT SUM(cajas_segunda) AS acum FROM produccion WHERE YEAR = '{$filters->year}'")->acum;
		}

		$response->lotes = $this->db->queryAllSpecial("SELECT lote AS id , lote AS label FROM produccion GROUP BY lote");

		$response->id_company = $this->session->id_company;
		$response->edades = $this->getPromedioEdadWeek();

		/* TAGS MARIO */
		if($this->session->id_company == 3){
			$response->tags->peso_prom_cajas = 19.09;
			$response->tags->otras_cajas = 21.82;
			$response->tags->calibracion = $this->db->queryRow("SELECT ROUND(AVG(calibracion), 2) AS prom FROM produccion_calibracion WHERE YEAR = '{$filters->year}'")->prom;
			$response->tags->racimo = $this->db->queryRow("SELECT ROUND(AVG(peso), 2) AS prom FROM produccion_peso WHERE YEAR = '{$filters->year}'")->prom;
			$response->tags->ratio_cortado = $this->db->queryRow("SELECT ROUND(AVG(IFNULL(cajas_primera, 0)/IFNULL(racimos_cortados, 0)), 2) AS prom FROM produccion WHERE YEAR = '{$filters->year}'")->prom;
			$response->tags->ratio_cortado_dos = $this->db->queryRow("SELECT ROUND(IFNULL(AVG(IFNULL(cajas_primera+cajas_segunda, 0)/IFNULL(racimos_cortados, 0)),0), 2) AS prom FROM produccion WHERE YEAR = '{$filters->year}'")->prom;
			$response->tags->ratio_procesado = $this->db->queryRow("SELECT ROUND(AVG(IFNULL(cajas_primera, 0)/IFNULL(racimos_procesados, 0)), 2) AS prom FROM produccion WHERE YEAR = '{$filters->year}'")->prom;
			$response->tags->ratio_procesado_dos = $this->db->queryRow("SELECT ROUND(IFNULL(AVG(IFNULL(cajas_primera+cajas_segunda, 0)/IFNULL(racimos_procesados, 0)),0), 2) AS prom FROM produccion WHERE YEAR = '{$filters->year}'")->prom;

			/*
			* peso_caja_primera = cajas_primera * 19.09
			* peso_caja_segunda = cajas_segunda * 19.09
			* peso_racimo_cortado = peso * racimo_cortado
			*
			* merma_cosechada 1era = (peso_racimo_cortado-peso_caja_primera)/peso_racimo_cortado
			* merma_cosechada 2da = (peso_racimo_cortado-(peso_caja_primera+peso_caja_segunda))/peso_racimo_cortado
			*/
			$response->tags->merma_cosechada = $this->db->queryRow("SELECT ROUND(AVG(((peso*racimos_cortados)-(cajas_primera*19.09))/(peso*racimos_cortados)*100),2) AS merma_cortada_1era FROM produccion WHERE YEAR = '{$filters->year}'")->merma_cortada_1era;
			$response->tags->merma_cosechada_dos = $this->db->queryRow("SELECT ROUND(AVG(((peso*racimos_cortados)-( cajas_primera*19.09 + cajas_segunda*19.09 ))/(peso*racimos_cortados)*100),2) AS merma_cortada_1era FROM produccion WHERE YEAR = '{$filters->year}'")->merma_cortada_1era;

			/*
			* peso_racimo_procesado = peso * racimos_procesados 
			*
			* merma_procesada 1era = (peso_racimo_procesado-peso_caja_primera)/peso_racimo_procesado
			* merma_procesada 2da = (peso_racimo_procesado-(peso_caja_primera+peso_caja_segunda))/peso_racimo_procesado
			*/
			$response->tags->merma_procesada = $this->db->queryRow("SELECT ROUND(AVG(((peso*racimos_procesados)-(cajas_primera*19.09))/(peso*racimos_procesados)*100),2) AS merma_procesada FROM produccion WHERE YEAR = '{$filters->year}'")->merma_procesada;
			$response->tags->merma_procesada_dos = $this->db->queryRow("SELECT ROUND(AVG(((peso*racimos_procesados)-(cajas_primera*19.09+cajas_segunda*19.09))/(peso*racimos_procesados)*100),2) AS merma_procesada FROM produccion WHERE YEAR = '{$filters->year}'")->merma_procesada;
			/*
			* merma_neta = merma_procesada - 10
			*
			* merma kg = merma_neta * acum_peso_racimo
			*/
			$response->tags->merma_kg = $this->db->queryRow("SELECT ROUND(acum * ".($response->tags->merma_procesada-10).",2) as acum FROM(SELECT SUM(peso) AS acum FROM produccion WHERE YEAR = '{$filters->year}') AS tbl")->acum;
		}

		//$response->produccion = $this->produccionActual();

		return $response;
	}

	private function produccionHistorica(){

	}

	public function produccionActual(){
		$data = $this->db->queryAll("SELECT *, WEEK(fecha) as semana, CEIL(WEEK(fecha)/4) as periodo FROM produccion WHERE year >= 2017");
		$data_procesada = [];
		foreach($data as $reg){
			if($reg->cosechados_blanco > 0){
				$data_procesada[$reg->semana]["blancas"]["sum_edades"] += $this->calcularEdad("BLANCA", (int) $reg->year, $reg->periodo, (int) $reg->semana);
				$data_procesada[$reg->semana]["blancas"]["prom_edades"][] = $this->calcularEdad("BLANCA", (int) $reg->year, $reg->periodo, (int) $reg->semana);
			}
			if($reg->cosechados_negro > 0){
				$data_procesada[$reg->semana]["negras"]["sum_edades"] += $this->calcularEdad("NEGRA", $reg->year, $reg->periodo, $reg->semana);
				$data_procesada[$reg->semana]["negras"]["prom_edades"][] = $this->calcularEdad("NEGRA", $reg->year, $reg->periodo, $reg->semana);
			}
			if($reg->cosechados_azul > 0){
				$data_procesada[$reg->semana]["azules"]["sum_edades"] += $this->calcularEdad("AZUL", $reg->year, $reg->periodo, $reg->semana);
				$data_procesada[$reg->semana]["azules"]["prom_edades"][] = $this->calcularEdad("AZUL", $reg->year, $reg->periodo, $reg->semana);
			}
		}
		foreach($data_procesada as $semana => $colores){
			foreach($colores as $color => $value){
				$data_sem[$semana][$color] = ($colores[$color]["sum_edades"] / count($colores[$color]["prom_edades"]));
			}
			#$data_sem[$semana]["blancas"] = ($blancas["blancas"]["sum_edades"] / count($blancas["blancas"]["prom_edades"]));
		}
		return $data_sem;
	}

	private function calcularEdad($color, $anio, $periodo, $semana){
		$secuencia = [
			"2016" => [
				10 => [ 37 => "CAFE", 38 => "AMARILLA", 39 => "VERDE", 40 => "AZUL"],
				11 => [ 41 => "BLANCA", 42 => "NEGRA", 43 => "LILA", 44 => "ROJA" ], 
				12 => [ 45 => "CAFE", 46 => "AMARILLA", 47 => "VERDE", 48 => "AZUL" ],
				13 => [ 49 => "BLANCA", 50 => "NEGRA", 51 => "LILA", 52 => "ROJA" ]
			],
			"2017" => [
				1 => [ 1 => "ROJA-CAFE", 2 => "CAFE", 3 => "NEGRA", 4 => "VERDE" ],
				2 => [ 5 => "AZUL", 6 => "BLANCA", 7 => "AMARILLA", 8 => "LILA", ],
				3 => [ 9 => "ROJA", 10 => "CAFE", 11 => "NEGRA", 12 => "VERDE" ],
				4 => [ 13 => "AZUL", 14 => "BLANCA", 15 => "AMARILLA", 16 => "LILA" ],
				5 => [ 17 => "ROJA", 18 => "CAFE", 19 => "NEGRA", 20 => "VERDE" ],
				6 => [ 21 => "AZUL", 22 => "BLANCA", 23 => "AMARILLA", 24 => "LILA" ],
				7 => [ 25 => "ROJA", 26 => "CAFE", 27 => "NEGRA", 28 => "VERDE" ],
				8 => [ 29 => "AZUL", 30 => "BLANCA", 31 => "AMARILLA", 32 => "LILA" ],
				9 => [ 33 => "ROJA", 34 => "CAFE", 35 => "NEGRA", 36 => "VERDE" ],
				10 => [ 37 => "AZUL", 38 => "BLANCA", 39 => "AMARILLA", 40 => "LILA" ],
				11 => [ 41 => "ROJA", 42 => "CAFE", 43 => "NEGRA", 44 => "VERDE" ],
				12 => [ 45 => "AZUL", 46 => "BLANCA", 47 => "AMARILLA", 48 => "LILA" ],
				13 => [ 49 => "ROJA", 50 => "CAFE", 51 => "NEGRA", 52 => "VERDE" ]
			]
		];
		$edad = 0;
		$encontro = false;
		$semana_edad = 0;
		$pr = 0;

		#$this->DD($semana);
		
		if($anio == 2017 && $semana < 18){
			if(in_array($semana, [14,15,16,17]))
				$pr = 13;
			if(in_array($semana, [10,11,12,13]))
				$pr = 12;
			if(in_array($semana, [6,8,9,10,3]))
				$pr = 11;
			if(in_array($semana, [5,7,4,2]))
				$pr = 10;

			foreach($secuencia["2016"][$pr] as $sec_periodo){
				if(!$encontro)
				foreach($sec_periodo as $sem => $col){					
					if($col == $color){
						$encontro = true;
						$semana_edad = $sem;
						break;
					}
				}
			}
			$edad = $semana_edad;
		}else if($semana >= 18){
			foreach($secuencia["2017"][$periodo-4] as $sec_periodo){
				foreach($sec_periodo as $sem => $col){
					if($col == $color){
						$encontro = true;
						$semana_edad = $sem;
					}
				}
			}
			$edad = $semana - $semana_edad;
		}else{
			foreach($secuencia["2017"][$periodo+13-5] as $sec_periodo){
				foreach($sec_periodo as $sem => $col){
					if($col == $color){
						$encontro = true;
						$semana_edad = $sem;
					}
				}
			}
			$edad = $semana + (13 - $semana_edad);
		}

		return $edad;
	}

	private function DD($string){
		echo "<pre>";
		D($string);
		echo "</pre>";
	}

	private function getLotes($semana = 0){
		$response = new stdClass;
		$response->data = [];
		if($semana > 0){
			$sYear = " AND YEAR(fecha) = YEAR(CURRENT_DATE)";
			$sSemana = "WEEK(p.fecha)";
			if($this->session->id_company == 2 || $this->session->id_company == 3){
				$sYear = " AND year = YEAR(CURRENT_DATE)";
				$sSemana = "semana";
			}

			if($this->session->id_company == 2 || $this->session->id_company == 3){
				$sql = "SELECT $sSemana AS semana , lote,
					SUM(total_cosechados) AS total_cosechados , 
					SUM(total_recusados) AS total_recusados,
					(SUM(total_cosechados) - SUM(total_recusados)) AS total_procesada,
					'false' AS expanded,
					((SELECT SUM(caja) AS caja FROM produccion_cajas WHERE year = YEAR(CURRENT_DATE) AND semana = p.semana AND lote = p.lote GROUP BY semana) /
					SUM((total_cosechados))
					) AS ratio_cortado,
					((SELECT SUM(caja) AS caja FROM produccion_cajas WHERE year = YEAR(CURRENT_DATE) AND semana = p.semana AND lote = p.lote GROUP BY semana) /
					SUM((total_cosechados-total_recusados))
					) AS ratio_procesado,
					(SELECT COUNT(*) FROM produccion_muestra WHERE WEEK(fecha) = {$semana}  AND lote = p.lote {$sYear}) AS muestrados,
					(SELECT AVG(largo_dedos) FROM produccion_muestra WHERE WEEK(fecha) = {$semana}  AND lote = p.lote {$sYear}) AS largo_dedos,
					(SELECT AVG(peso) FROM produccion_peso WHERE semana = $sSemana {$sYear} AND lote = p.lote) AS peso,
					(SELECT AVG(largo_dedos) FROM produccion_muestra WHERE WEEK(fecha) = $sSemana {$sYear} AND lote = p.lote) AS largo_dedos,
					(SELECT AVG(calibracion) FROM produccion_calibracion WHERE semana = $sSemana {$sYear} AND lote = p.lote) AS calibracion,
					(SELECT AVG(manos) FROM produccion_manos WHERE semana = $sSemana {$sYear} AND lote = p.lote) AS manos
					FROM produccion AS p
					WHERE 1 = 1 AND $sSemana = {$semana} {$sYear}
					GROUP BY lote";
			}else{
				$sql = "SELECT $sSemana AS semana , lote,
					SUM(total_cosechados) AS total_cosechados , 
					SUM(total_recusados) AS total_recusados,
					(SUM(total_cosechados) - SUM(total_recusados)) AS total_procesada,
					'false' AS expanded,
					((SELECT SUM(caja) AS caja FROM produccion_cajas WHERE year = YEAR(CURRENT_DATE) AND semana = p.semana AND lote = p.lote GROUP BY semana) /
					SUM((total_cosechados-total_recusados))
					) AS ratio_procesado,
					(SELECT COUNT(*) FROM produccion_muestra WHERE WEEK(fecha) = {$semana}  AND lote = p.lote {$sYear}) AS muestrados,
					(SELECT AVG(peso) FROM produccion_muestra WHERE WEEK(fecha) = {$semana} AND lote = p.lote {$sYear}) AS peso,
					(SELECT AVG(largo_dedos) FROM produccion_muestra WHERE WEEK(fecha) = {$semana}  AND lote = p.lote {$sYear}) AS largo_dedos,
					(SELECT AVG(calibracion) FROM produccion_muestra WHERE WEEK(fecha) = {$semana}  AND lote = p.lote {$sYear}) AS calibracion,
					(SELECT AVG(manos) FROM produccion_muestra WHERE WEEK(fecha) = {$semana}  AND lote = p.lote {$sYear}) AS manos
					FROM produccion AS p
					WHERE 1 = 1 AND $sSemana = {$semana} {$sYear}
					GROUP BY lote";
			}
			$data = $this->db->queryAll($sql);
			foreach ($data as $key => $value) {
				$response->data[$value->lote]["campo"] = $value->lote;
				$response->data[$value->lote]["muestrados"] = (double)$value->muestrados;
				$response->data[$value->lote]["peso"] = (double)$value->peso;
				$response->data[$value->lote]["manos"] = (double)$value->manos;
				$response->data[$value->lote]["largo_dedos"] = (double)$value->largo_dedos;
				$response->data[$value->lote]["calibracion"] = (double)$value->calibracion;
				$response->data[$value->lote]["total_cosechados"] = (double)$value->total_cosechados;
				$response->data[$value->lote]["total_recusados"] = (double)$value->total_recusados;
				$response->data[$value->lote]["total_procesada"] = (double)$value->total_procesada;
				$response->data[$value->lote]["total_procesada"] = (double)$value->total_procesada;
				$response->data[$value->lote]["ratio_procesado"] = (double)$value->ratio_procesado;
				$response->data[$value->lote]["ratio_cortado"] = (double)$value->ratio_cortado;
			}
		}

		return $response->data;
	}

	private function getPromedioEdadWeek(){
		$sql = "SELECT WEEK(m.fecha) AS semana,SUM(cosechados_blanco) AS cosechados_blanco , SUM(cosechados_negro) AS cosechados_negro , 
				SUM(cosechados_lila) AS cosechados_lila ,SUM(cosechados_rojo) AS cosechados_rojo , SUM(cosechados_cafe) AS cosechados_cafe , 
				SUM(cosechados_amarillo) AS cosechados_amarillo , SUM(cosechados_verde) AS cosechados_verde , 
				SUM(cosechados_azul) AS cosechados_azul,SUM(cosechados_naranja) AS cosechados_naranja,SUM(cosechados_gris) AS cosechados_gris
				FROM produccion AS m
				GROUP BY WEEK(fecha)";
		$data = $this->db->queryAll($sql);
		$response = new stdClass;
		$response->data = [];
		foreach ($data as $key => $value) {
			$value->cosechados_blanco = (int)$value->cosechados_blanco;
			$value->cosechados_negro = (int)$value->cosechados_negro;
			$value->cosechados_lila = (int)$value->cosechados_lila;
			$value->cosechados_rojo = (int)$value->cosechados_rojo;
			$value->cosechados_cafe = (int)$value->cosechados_cafe;
			$value->cosechados_amarillo = (int)$value->cosechados_amarillo;
			$value->cosechados_verde = (int)$value->cosechados_verde;
			$value->cosechados_azul = (int)$value->cosechados_azul;
			$value->cosechados_naranja = (int)$value->cosechados_naranja;
			$value->cosechados_gris = (int)$value->cosechados_gris;
			if($value->cosechados_blanco > 0){
				$response->data[$value->semana]["blanco"]["edad"] = $this->getEdad("blanca" , $value->semana);
				$response->data[$value->semana]["blanco"]["cosechadas"] = $value->cosechados_blanco;
				$response->total[$value->semana]["sum"] += $value->cosechados_blanco * $response->data[$value->semana]["blanco"]["edad"];
				$response->total[$value->semana]["sum_cosecha"] += $value->cosechados_blanco;
			}
			if($value->cosechados_negro > 0){
				$response->data[$value->semana]["negra"]["edad"] = $this->getEdad("negra" , $value->semana);
				$response->data[$value->semana]["negra"]["cosechadas"] = $value->cosechados_negro;
				$response->total[$value->semana]["sum"] += $value->cosechados_negro * $response->data[$value->semana]["negra"]["edad"];
				$response->total[$value->semana]["sum_cosecha"] += $value->cosechados_negro;
			}
			if($value->cosechados_lila > 0){
				$response->data[$value->semana]["lila"]["edad"] = $this->getEdad("lila" , $value->semana);
				$response->data[$value->semana]["lila"]["cosechadas"] = $value->cosechados_lila;
				$response->total[$value->semana]["sum"] += $value->cosechados_lila * $response->data[$value->semana]["lila"]["edad"];
				$response->total[$value->semana]["sum_cosecha"] += $value->cosechados_lila;
			}
			if($value->cosechados_rojo > 0){
				$response->data[$value->semana]["roja"]["edad"] = $this->getEdad("roja" , $value->semana);
				$response->data[$value->semana]["roja"]["cosechadas"] = $value->cosechados_rojo;
				$response->total[$value->semana]["sum"] += $value->cosechados_rojo * $response->data[$value->semana]["roja"]["edad"];
				$response->total[$value->semana]["sum_cosecha"] += $value->cosechados_rojo;
			}
			if($value->cosechados_cafe > 0){
				$response->data[$value->semana]["cafe"]["edad"] = $this->getEdad("cafe" , $value->semana);
				$response->data[$value->semana]["cafe"]["cosechadas"] = $value->cosechados_cafe;
				$response->total[$value->semana]["sum"] += $value->cosechados_cafe * $response->data[$value->semana]["cafe"]["edad"];
				$response->total[$value->semana]["sum_cosecha"] += $value->cosechados_cafe;
			}
			if($value->cosechados_amarillo > 0){
				$response->data[$value->semana]["amarilla"]["edad"] = $this->getEdad("amarilla" , $value->semana);
				$response->data[$value->semana]["amarilla"]["cosechadas"] = $value->cosechados_amarillo;
				$response->total[$value->semana]["sum"] += $value->cosechados_amarillo * $response->data[$value->semana]["amarilla"]["edad"];
				$response->total[$value->semana]["sum_cosecha"] += $value->cosechados_amarillo;
			}
			if($value->cosechados_verde > 0){
				$response->data[$value->semana]["verde"]["edad"] = $this->getEdad("verde" , $value->semana);
				$response->data[$value->semana]["verde"]["cosechadas"] = $value->cosechados_verde;
				$response->total[$value->semana]["sum"] += $value->cosechados_verde * $response->data[$value->semana]["verde"]["edad"];
				$response->total[$value->semana]["sum_cosecha"] += $value->cosechados_verde;
			}
			if($value->cosechados_azul > 0){
				$response->data[$value->semana]["azul"]["edad"] = $this->getEdad("azul" , $value->semana);
				$response->data[$value->semana]["azul"]["cosechadas"] = $value->cosechados_azul;
				$response->total[$value->semana]["sum"] += $value->cosechados_azul * $response->data[$value->semana]["azul"]["edad"];
				$response->total[$value->semana]["sum_cosecha"] += $value->cosechados_azul;
			}
			if($value->cosechados_naranja > 0){
				$response->data[$value->semana]["naranja"]["edad"] = $this->getEdad("naranja" , $value->semana);
				$response->data[$value->semana]["naranja"]["cosechadas"] = $value->cosechados_naranja;
				$response->total[$value->semana]["sum"] += $value->cosechados_naranja * $response->data[$value->semana]["naranja"]["edad"];
				$response->total[$value->semana]["sum_cosecha"] += $value->cosechados_naranja;
			}
			if($value->cosechados_gris > 0){
				$response->data[$value->semana]["gris"]["edad"] = $this->getEdad("gris" , $value->semana);
				$response->data[$value->semana]["gris"]["cosechadas"] = $value->cosechados_gris;
				$response->total[$value->semana]["sum"] += $value->cosechados_gris * $response->data[$value->semana]["gris"]["edad"];
				$response->total[$value->semana]["sum_cosecha"] += $value->cosechados_gris;
			}
			$response->edad_promedio[$value->semana] = [
				"label" => $value->semana,
				"position" => 0,
				"legend" => "Edad Promedio",
				"value" => (double)($response->total[$value->semana]["sum"] / $response->total[$value->semana]["sum_cosecha"]),
			];

			$response->edad_prom[$value->semana] = (double)($response->total[$value->semana]["sum"] / $response->total[$value->semana]["sum_cosecha"]);
		}

		$response->edad = array_sum($response->edad_prom) / count($response->edad_prom);
		$minMax = [
			"min" => 9,
			"max" => 15,
		];
		$response->edad_promedio = $this->chartInit($response->edad_promedio , "vertical" , "Edad Promedio" , "line" , $minMax);
		return $response;
	}

	private function getPromedioEdad(){
		$sql = "SELECT DATE(fecha) AS fecha,WEEK(m.fecha) AS semana,SUM(cosechados_blanco) AS cosechados_blanco , SUM(cosechados_negro) AS cosechados_negro , 
				SUM(cosechados_lila) AS cosechados_lila ,SUM(cosechados_rojo) AS cosechados_rojo , SUM(cosechados_cafe) AS cosechados_cafe , 
				SUM(cosechados_amarillo) AS cosechados_amarillo , SUM(cosechados_verde) AS cosechados_verde , 
				SUM(cosechados_azul) AS cosechados_azul,SUM(cosechados_naranja) AS cosechados_naranja,SUM(cosechados_gris) AS cosechados_gris
				FROM produccion AS m
				GROUP BY DATE(fecha)";
		$data = $this->db->queryAll($sql);
		$response = new stdClass;
		$response->data = [];
		$edad_promedio = [];
		$sum_edadxcosecha = [];
		foreach ($data as $key => $value) {
			$value->cosechados_blanco = (int)$value->cosechados_blanco;
			$value->cosechados_negro = (int)$value->cosechados_negro;
			$value->cosechados_lila = (int)$value->cosechados_lila;
			$value->cosechados_rojo = (int)$value->cosechados_rojo;
			$value->cosechados_cafe = (int)$value->cosechados_cafe;
			$value->cosechados_amarillo = (int)$value->cosechados_amarillo;
			$value->cosechados_verde = (int)$value->cosechados_verde;
			$value->cosechados_azul = (int)$value->cosechados_azul;
			$value->cosechados_naranja = (int)$value->cosechados_naranja;
			$value->cosechados_gris = (int)$value->cosechados_gris;
			if($value->cosechados_blanco > 0){
				$response->data[$value->fecha]["blanco"]["edad"] = $this->getEdad("blanca" , $value->semana);
				$response->data[$value->fecha]["blanco"]["cosechadas"] = $value->cosechados_blanco;
				$sum_edadxcosecha[$value->fecha]["sum"] += $response->data[$value->fecha]["blanco"]["edad"] * $response->data[$value->fecha]["blanco"]["cosechadas"];
				$sum_edadxcosecha[$value->fecha]["sum_cosecha"] += $response->data[$value->fecha]["blanco"]["cosechadas"];
			}
			if($value->cosechados_negro > 0){
				$response->data[$value->fecha]["negra"]["edad"] = $this->getEdad("negra" , $value->semana);
				$response->data[$value->fecha]["negra"]["cosechadas"] = $value->cosechados_negro;
				$sum_edadxcosecha[$value->fecha]["sum"] += $response->data[$value->fecha]["negra"]["edad"] * $response->data[$value->fecha]["negra"]["cosechadas"];
				$sum_edadxcosecha[$value->fecha]["sum_cosecha"] += $response->data[$value->fecha]["negra"]["cosechadas"];
			}
			if($value->cosechados_lila > 0){
				$response->data[$value->fecha]["lila"]["edad"] = $this->getEdad("lila" , $value->semana);
				$response->data[$value->fecha]["lila"]["cosechadas"] = $value->cosechados_lila;
				$sum_edadxcosecha[$value->fecha]["sum"] += $response->data[$value->fecha]["lila"]["edad"] * $response->data[$value->fecha]["lila"]["cosechadas"];
				$sum_edadxcosecha[$value->fecha]["sum_cosecha"] += $response->data[$value->fecha]["lila"]["cosechadas"];
			}
			if($value->cosechados_rojo > 0){
				$response->data[$value->fecha]["roja"]["edad"] = $this->getEdad("roja" , $value->semana);
				$response->data[$value->fecha]["roja"]["cosechadas"] = $value->cosechados_rojo;
				$sum_edadxcosecha[$value->fecha]["sum"] += $response->data[$value->fecha]["roja"]["edad"] * $response->data[$value->fecha]["roja"]["cosechadas"];
				$sum_edadxcosecha[$value->fecha]["sum_cosecha"] += $response->data[$value->fecha]["roja"]["cosechadas"];
			}
			if($value->cosechados_cafe > 0){
				$response->data[$value->fecha]["cafe"]["edad"] = $this->getEdad("cafe" , $value->semana);
				$response->data[$value->fecha]["cafe"]["cosechadas"] = $value->cosechados_cafe;
				$sum_edadxcosecha[$value->fecha]["sum"] += $response->data[$value->fecha]["cafe"]["edad"] * $response->data[$value->fecha]["cafe"]["cosechadas"];
				$sum_edadxcosecha[$value->fecha]["sum_cosecha"] += $response->data[$value->fecha]["cafe"]["cosechadas"];
			}
			if($value->cosechados_amarillo > 0){
				$response->data[$value->fecha]["amarilla"]["edad"] = $this->getEdad("amarilla" , $value->semana);
				$response->data[$value->fecha]["amarilla"]["cosechadas"] = $value->cosechados_amarillo;
				$sum_edadxcosecha[$value->fecha]["sum"] += $response->data[$value->fecha]["amarilla"]["edad"] * $response->data[$value->fecha]["amarilla"]["cosechadas"];
				$sum_edadxcosecha[$value->fecha]["sum_cosecha"] += $response->data[$value->fecha]["amarilla"]["cosechadas"];
			}
			if($value->cosechados_verde > 0){
				$response->data[$value->fecha]["verde"]["edad"] = $this->getEdad("verde" , $value->semana);
				$response->data[$value->fecha]["verde"]["cosechadas"] = $value->cosechados_verde;
				$sum_edadxcosecha[$value->fecha]["sum"] += $response->data[$value->fecha]["verde"]["edad"] * $response->data[$value->fecha]["verde"]["cosechadas"];
				$sum_edadxcosecha[$value->fecha]["sum_cosecha"] += $response->data[$value->fecha]["verde"]["cosechadas"];
			}
			if($value->cosechados_azul > 0){
				$response->data[$value->fecha]["azul"]["edad"] = $this->getEdad("azul" , $value->semana);
				$response->data[$value->fecha]["azul"]["cosechadas"] = $value->cosechados_azul;
				$sum_edadxcosecha[$value->fecha]["sum"] += $response->data[$value->fecha]["azul"]["edad"] * $response->data[$value->fecha]["azul"]["cosechadas"];
				$sum_edadxcosecha[$value->fecha]["sum_cosecha"] += $response->data[$value->fecha]["azul"]["cosechadas"];
			}
			if($value->cosechados_naranja > 0){
				$response->data[$value->fecha]["naranja"]["edad"] = $this->getEdad("naranja" , $value->semana);
				$response->data[$value->fecha]["naranja"]["cosechadas"] = $value->cosechados_naranja;
				$sum_edadxcosecha[$value->fecha]["sum"] += $response->data[$value->fecha]["naranja"]["edad"] * $response->data[$value->fecha]["naranja"]["cosechadas"];
				$sum_edadxcosecha[$value->fecha]["sum_cosecha"] += $response->data[$value->fecha]["naranja"]["cosechadas"];
			}
			if($value->cosechados_gris > 0){
				$response->data[$value->fecha]["gris"]["edad"] = $this->getEdad("gris" , $value->semana);
				$response->data[$value->fecha]["gris"]["cosechadas"] = $value->cosechados_gris;
				$sum_edadxcosecha[$value->fecha]["sum"] += $response->data[$value->fecha]["gris"]["edad"] * $response->data[$value->fecha]["gris"]["cosechadas"];
				$sum_edadxcosecha[$value->fecha]["sum_cosecha"] += $response->data[$value->fecha]["gris"]["cosechadas"];
			}

			$response->edad_promedio[$value->semana][$value->fecha] = $sum_edadxcosecha[$value->fecha]["sum"] / $sum_edadxcosecha[$value->fecha]["sum_cosecha"];
			$response->data[$value->semana] += $sum_edadxcosecha[$value->fecha]["sum"] / $sum_edadxcosecha[$value->fecha]["sum_cosecha"];
		}

		return $response;
	}

	private function getEdad($color = "" , $semana = 1){
		$edad = 0;
		$sql = "SELECT
				({$semana} - (
				SELECT semana FROM semanas_colores
				WHERE YEAR(CURRENT_DATE) = year AND color = UPPER('{$color}') AND semana < ({$semana} - 8) ORDER BY semana DESC LIMIT 1
				)) + 1 AS edad";
		$edad = (int)$this->db->queryOne($sql);
		return $edad;
	}

	private function generateDataCosecha(){
		$postdata = (object)json_decode(file_get_contents("php://input"));
		$response = new stdClass;
		$filters = (object)[
			"type" => getValueFrom($postdata , "type" , "cosechados" , FILTER_SANITIZE_STRING),
		];
		$sYearCampo = "YEAR(fecha)";
		$sSemana = "WEEK(p.fecha)";
		if($this->session->id_company == 2 || $this->session->id_company == 3){
			$sSemana = "semana";
			$sYearCampo = "year";
		}

		$type = [
			"cosechados" => "SUM(total_cosechados)",
			"recusados" => "SUM(total_recusados)",
			"procesada" => "(SUM(total_cosechados) - SUM(total_recusados))",
		];
		$title = "Cosechas";
		if($filters->type == "cajas"){
			$title = "Cajas";
			$sql_historico = "SELECT year AS legend,semana AS label,SUM(caja) AS value,IF(year > 2013 ,true,false) AS selected
								FROM produccion_cajas AS p
								GROUP BY year,semana";
		}else{
			$title = "Cosechas";
			$sql_historico = "SELECT {$sSemana} AS label,{$sYearCampo} AS legend,IF(year > 2013 ,true,false) AS selected,
				{$type[$filters->type]} AS value 
				FROM produccion AS p
				GROUP BY {$sYearCampo} , {$sSemana}";
		}
		
		$data = $this->db->queryAll($sql_historico);

		$minMax = [
			// "min" => 50,
			// "max" => 100,
		];
		$response->historico = $this->chartInit($data,"vertical",$title,"line",$minMax);
		return $response;
	}

	private function generateDataPeso(){
		$response = new stdClass;
		$sql_promedio = "SELECT AVG(peso) AS peso FROM (
						SELECT semana ,AVG(peso) as peso FROM (
						(SELECT lote,semana , AVG(peso) AS peso FROM produccion_peso
						WHERE year = YEAR(CURRENT_DATE)
						GROUP BY year,lote,semana)) AS p
						GROUP BY semana) AS p1";
		$response->promedio = (double)$this->db->queryOne($sql_promedio);

		$sql_historico = "SELECT year AS legend , semana AS label , ROUND(AVG(peso),2) AS value,IF(year > 2013 ,true,false) AS selected FROM (
							SELECT year , semana ,lote,AVG(peso) AS peso FROM produccion_peso
							GROUP BY year,lote,semana) AS details
							GROUP BY year,semana";
		$data = $this->db->queryAll($sql_historico);
		// $data = [];
		$minMax = [
			"min" => 50,
			"max" => 100,
		];
		$response->historico = $this->chartInit($data,"vertical","Peso","line",$minMax);

		return $response;
	}

	private function generateDataManos(){
		$response = new stdClass;
		$sql_promedio = "SELECT AVG(manos) AS manos FROM (
						SELECT semana ,AVG(manos) as manos FROM (
						(SELECT lote,semana , AVG(manos) AS manos FROM produccion_manos
						WHERE year = YEAR(CURRENT_DATE)
						GROUP BY year,lote,semana)) AS p
						GROUP BY semana) AS p1";
		$response->promedio = (double)$this->db->queryOne($sql_promedio);

		$sql_historico = "SELECT year AS legend , semana AS label , ROUND(AVG(manos),2) AS value,IF(year > 2013 ,true,false) AS selected FROM (
							SELECT year , semana ,lote,AVG(manos) AS manos FROM produccion_manos
							GROUP BY year,lote,semana) AS details
							GROUP BY year,semana";
		$data = $this->db->queryAll($sql_historico);
		// $data = [];
		$minMax = [
			"min" => 5,
			"max" => 10,
		];
		$response->historico = $this->chartInit($data,"vertical","Manos","line",$minMax);

		return $response;
	}

	private function generateDataCalibracion(){
		$response = new stdClass;
		$sql_promedio = "SELECT AVG(calibracion) AS calibracion FROM (
						SELECT semana ,AVG(calibracion) as calibracion FROM (
						(SELECT lote,semana , AVG(calibracion) AS calibracion FROM produccion_calibracion
						WHERE year = YEAR(CURRENT_DATE)
						GROUP BY year,lote,semana)) AS p
						GROUP BY semana) AS p1";
		$response->promedio = (double)$this->db->queryOne($sql_promedio);

		$sql_historico = "SELECT year AS legend , semana AS label , ROUND(AVG(calibracion),2) AS value,IF(year > 2013 ,true,false) AS selected FROM (
							SELECT year , semana ,lote,AVG(calibracion) AS calibracion FROM produccion_calibracion
							GROUP BY year,lote,semana) AS details
							GROUP BY year,semana";
		$data = $this->db->queryAll($sql_historico);
		// $data = [];
		$minMax = [
			"min" => 38,
			"max" => 46,
		];
		$response->historico = $this->chartInit($data,"vertical","Calibración","line",$minMax);

		return $response;
	}

	private function chartInit($data = [] , $mode = "vertical" , $name = "" , $type = "line" , $minMax = []){
		$response = new stdClass;
		$response->chart = [];
		if(count($data) > 0){
			$response->chart["title"]["show"] = true;
			$response->chart["title"]["text"] = $name;
			$response->chart["title"]["subtext"] = $this->session->sloganCompany;
			$response->chart["tooltip"]["trigger"] = "item";
			$response->chart["tooltip"]["axisPointer"]["type"] = "shadow";
			$response->chart["toolbox"]["show"] = true;
			$response->chart["toolbox"]["feature"]["mark"]["show"] = true;
			$response->chart["toolbox"]["feature"]["restore"]["show"] = true;
			$response->chart["toolbox"]["feature"]["magicType"]["show"] = true;
			$response->chart["toolbox"]["feature"]["magicType"]["type"] = ['bar' , 'line'];
			$response->chart["toolbox"]["feature"]["saveAsImage"]["show"] = true;
			$response->chart["legend"]["data"] = [];
			$response->chart["legend"]["left"] = "center";
			$response->chart["legend"]["bottom"] = "1%";
			$response->chart["grid"]["left"] = "3%";
			$response->chart["grid"]["right"] = "4%";
			$response->chart["grid"]["bottom"] = "15%";
			$response->chart["grid"]["containLabel"] = true;
			if($mode == "vertical"){
				$response->chart["xAxis"] = ["type" => "category" , "data" => [""]];
				$response->chart["yAxis"] = ["type" => "value"];
				if(isset($minMax["min"])){
					$response->chart["yAxis"] = ["type" => "value" , "min" => $minMax["min"], "max" => $minMax["max"]];
				}
			}else if($mode == "horizontal"){
				$response->chart["yAxis"] = ["type" => "category" , "data" => [""]];
				$response->chart["xAxis"] = ["type" => "value"];
			}
			$response->chart["series"] = [];
			$count = 0;
			$position = -1;
			$colors = [];
			if($type == "line"){
				$response->chart["xAxis"]["data"] = [];
			}
			foreach ($data as $key => $value) {
				$value = (object)$value;
				$value->legend = ucwords(strtolower($value->legend));
				$value->label = ucwords(strtolower($value->label));
				if(isset($value->position)){
					$position = $value->position;
				}
				if(isset($value->color)){
					$colors = [
						"normal" => ["color" => $value->color],
					];
				}

				if($type == "line"){
					if(!in_array($value->label, $response->chart["xAxis"]["data"])){
						$response->chart["xAxis"]["data"][] = $value->label;
					}
					if(!in_array($value->legend, $response->chart["legend"]["data"])){
						if(!isset($value->position)){
							$position++;
						}
						$response->chart["legend"]["data"][] = $value->legend;
						$response->chart["legend"]["selected"][$value->legend] = ($value->selected == 0) ? false : true;
						$response->chart["series"][$position] = [
							"name" => $value->legend,
							"type" => $type,
							"data" => [],
							"label" => [
								"normal" => ["show" => false , "position" => "top"],
								"emphasis" => ["show" => false , "position" => "top"],
							],
							"itemStyle" => $colors
						];
					}

					$response->chart["series"][$position]["data"][] = ROUND($value->value,2);

				}else{
					$response->chart["legend"]["data"][] = $value->label;
					// if($count == 0){
						$response->chart["series"][$count] = [
							"name" => $value->label,
							"type" => $type,
							"data" => [(int)$value->value],
							"label" => [
								"normal" => ["show" => true , "position" => "top"],
								"emphasis" => ["show" => true , "position" => "top"],
							],
							"itemStyle" => $colors
						];
					// }
				}
				$count++;
			}
		}

		return $response->chart;
	}
	private function pie($data = [] , $radius = ['50%', '70%'] , $name = "CATEGORIAS" , $roseType = "" , $type = "normal" , $legend = true , $position = ['45%', '40%'] , $version = 3){
		$response = new stdClass;
		$response->pie = [];
		if(count($data) > 0){
			$response->pie["title"]["show"] = true;
			$response->pie["title"]["text"] = $name;
			$response->pie["title"]["left"] = "center";
			// $response->pie["title"]["left"] = "right";
			$response->pie["title"]["subtext"] = $this->session->sloganCompany;
			$response->pie["tooltip"]["trigger"] = "item";
			$response->pie["tooltip"]["formatter"] = "{a} <br/>{b}: {c} ({d}%)";
			$response->pie["toolbox"]["show"] = true;
			$response->pie["toolbox"]["feature"]["mark"]["show"] = true;
			$response->pie["toolbox"]["feature"]["restore"]["show"] = true;
			$response->pie["toolbox"]["feature"]["magicType"]["show"] = false;
			$response->pie["toolbox"]["feature"]["magicType"]["type"] = ['pie'];
			$response->pie["toolbox"]["feature"]["saveAsImage"]["show"] = true;
			$response->pie["toolbox"]["feature"]["saveAsImage"]["name"] = $name;
			$response->pie["legend"]["show"] = $legend;
			$response->pie["legend"]["x"] = "center";
			$response->pie["legend"]["y"] = "bottom";
			$response->pie["calculable"] = true;
			$response->pie["legend"]["data"] = [];
			$response->pie["series"]["name"] = $name;
			$response->pie["series"]["type"] = "pie";
			$response->pie["series"]["center"] = $position;
			$response->pie["series"]["radius"] = $radius;
			if($version === 3){
				$response->pie["series"]["selectedMode"] = true;
				$response->pie["series"]["label"]["normal"]["show"] = true;
				$response->pie["series"]["label"]["emphasis"]["show"] = true;
				if($type != "normal"){
					$response->pie["series"]["roseType"] = $roseType;
					$response->pie["series"]["avoidLabelOverlap"] = "pie";
				}
				// $response->pie["series"]["label"]["normal"]["position"] = "center";
				// $response->pie["series"]["label"]["emphasis"]["textStyle"]["fontSize"] = "30";
				// $response->pie["series"]["label"]["emphasis"]["textStyle"]["fontWeight"] = "bold";
				$response->pie["series"]["labelLine"]["normal"]["show"] = true;
			}
			$response->pie["series"]["data"] = [];
			$colors = [];
			foreach ($data as $key => $value) {
				$value = (object)$value;
				$value->label = ucwords(strtolower($value->label));
				$response->pie["legend"]["data"][] = $value->label;
				if($version === 3){
					if(isset($value->color)){
							$colors = [
								"normal" => ["color" => $value->color],
							];
					}
					$response->pie["series"]["data"][] = ["name" => $value->label  , "value" => (float)$value->value , 
							"label" => [
								"normal" => ["show" => true , "position" => "outside" , "formatter" => "{b} \n {c} ({d}%)"],
								"emphasis" => ["show" => true , "position" => "outside"],
							],
							"itemStyle" => $colors
					];
				}else{
					// $response->pie["series"]["data"][] = ["name" => $value->label  , "value" => (float)$value->value];
					$response->pie["series"]["data"][] = ["name" => $value->label  , "value" => (float)$value->value,
						"itemStyle" => [
							"normal" => [ "label" => ["formatter" => "{b} \n {c} \n ({d}%)"] ],
							"emphasis" => [ "label" => ["formatter" => "{b} \n {c} \n ({d}%)"] ],
						]
					];
				}
			}
		}

		return $response->pie;
	}
	private function clear($String){
		$String = str_replace(array('á','à','â','ã','ª','ä'),"a",$String);
	    $String = str_replace(array('Á','À','Â','Ã','Ä'),"A",$String);
	    $String = str_replace(array('Í','Ì','Î','Ï'),"I",$String);
	    $String = str_replace(array('í','ì','î','ï'),"i",$String);
	    $String = str_replace(array('é','è','ê','ë'),"e",$String);
	    $String = str_replace(array('É','È','Ê','Ë'),"E",$String);
	    $String = str_replace(array('ó','ò','ô','õ','ö','º'),"o",$String);
	    $String = str_replace(array('Ó','Ò','Ô','Õ','Ö'),"O",$String);
	    $String = str_replace(array('ú','ù','û','ü'),"u",$String);
	    $String = str_replace(array('Ú','Ù','Û','Ü'),"U",$String);
	    $String = str_replace(array('[','^','´','`','¨','~',']'),"",$String);
	    $String = str_replace("ç","c",$String);
	    $String = str_replace("Ç","C",$String);
	    $String = str_replace("ñ","n",$String);
	    $String = str_replace("Ñ","N",$String);
	    $String = str_replace("Ý","Y",$String);
	    $String = str_replace("ý","y",$String);
	    $String = str_replace(".","",$String);
	    $String = str_replace("&aacute;","a",$String);
	    $String = str_replace("&Aacute;","A",$String);
	    $String = str_replace("&eacute;","e",$String);
	    $String = str_replace("&Eacute;","E",$String);
	    $String = str_replace("&iacute;","i",$String);
	    $String = str_replace("&Iacute;","I",$String);
	    $String = str_replace("&oacute;","o",$String);
	    $String = str_replace("&Oacute;","O",$String);
	    $String = str_replace("&uacute;","u",$String);
	    $String = str_replace("&Uacute;","U",$String);
	    return $String;
	}
	
}
