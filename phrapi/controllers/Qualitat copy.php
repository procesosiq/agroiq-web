<?php defined('PHRAPI') or die("Direct access not allowed!");

class Qualitat {
	public $name;
	private $db;
	private $config;

	public function __construct(){
		$this->config = $GLOBALS['config'];
		$this->session = Session::getInstance();
		// D($this->session);
        $this->maindb = DB::getInstance();
        $this->db = DB::getInstance($this->session->agent_user);
	}

	public function index(){
		$postdata = (object)json_decode(file_get_contents("php://input"));

		$cliente = "";
		$marca   = "";
		if(isset($postdata->cliente) && isset($postdata->marca)){
			$cliente = mb_strtoupper($postdata->cliente);
			$marca   = mb_strtoupper($postdata->marca);
		}

		$sql = "SELECT (fecha) AS fecha , calidad FROM quality WHERE marca = '{$marca}' 
		GROUP BY fecha
		ORDER BY  fecha";
		$response->data = $this->db->queryAll($sql);
		$response->danhos->series = $this->getDanhos($postdata->fecha_inicial, $postdata->fecha_final, $cliente, $marca);
		$response->danhos->seleccion = $this->getDetails("SELECCION", $postdata->fecha_inicial, $postdata->fecha_final, $cliente, $marca);
		$response->danhos->empaque = $this->getDetails("EMPAQUE", $postdata->fecha_inicial, $postdata->fecha_final, $cliente, $marca);
		$response->danhos->otros = $this->getDetails("OTROS", $postdata->fecha_inicial, $postdata->fecha_final);
		$response->id_company = $this->session->id_company;
		$response->umbrals = $this->session->umbrals;
		
		// manuel
		$response->tags = $this->tags($postdata->fecha_inicial, $postdata->fecha_final, $cliente, $marca);
		$response->grafica_principal_calidad = $this->grafica_principal_calidad($postdata->fecha_inicial, $postdata->fecha_final, $cliente, $marca);
		// $response->grafica_principal_de = $this->grafica_principal_de($postdata->fecha_inicial, $postdata->fecha_final, $cliente, $marca);
		$response->grafica_principal_de = [];
		$response->grafica_principal_peso = $this->grafica_principal_peso($postdata->fecha_inicial, $postdata->fecha_final, $cliente, $marca);
		$response->grafica_principal_cluster = [];
		// $response->grafica_principal_cluster = $this->grafica_principal_cluster($postdata->fecha_inicial, $postdata->fecha_final, $cliente, $marca);
		$response->grafica_principal_peso_cluster = [];
		// $response->grafica_principal_peso_cluster = $this->grafica_principal_peso_cluster($postdata->fecha_inicial, $postdata->fecha_final, $cliente, $marca);
		#victor
		$response->grafica_calidad_historico_marcas = [];
		// $response->grafica_calidad_historico_marcas = $this->grafica_calidad_historico_marcas($postdata->fecha_inicial, $postdata->fecha_final, $cliente, $marca);
		
		$response->grafica_danos_total 	   = $this->grafica_danos_total($postdata->fecha_inicial, $postdata->fecha_final, $cliente, $marca);
		$response->grafica_danos_seleccion = $this->grafica_danos_seleccion($postdata->fecha_inicial, $postdata->fecha_final, $cliente, $marca);
		$response->grafica_danos_seleccion_campos = $this->grafica_danos_get_campos('SELECCION');
		$response->grafica_danos_empaque   = $this->grafica_danos_empaque($postdata->fecha_inicial, $postdata->fecha_final, $cliente, $marca);
		$response->grafica_danos_empaque_campos = $this->grafica_danos_get_campos('EMPAQUE', $postdata->fecha_inicial, $postdata->fecha_final, $cliente, $marca);
		$response->grafica_danos_otros     = $this->grafica_danos_otros($postdata->fecha_inicial, $postdata->fecha_final, $cliente, $marca);
		$response->grafica_danos_otros_campos = $this->grafica_danos_get_campos('OTROS', $postdata->fecha_inicial, $postdata->fecha_final, $cliente, $marca);

		$response->tabla_principal_calidad = $this->tabla_principal_calidad($postdata->fecha_inicial, $postdata->fecha_final, $cliente, $marca);
		$response->tabla_principal_danos = $this->tabla_principal_danos($postdata->fecha_inicial, $postdata->fecha_final, $cliente, $marca);
		$response->tabla_principal = $this->tabla_principal($postdata->fecha_inicial, $postdata->fecha_final, $cliente, $marca);

		$response->type = $this->session->type_users;

		/*----------  BARCO  ----------*/
		$barcoWeek = $this->getBarco($postdata);
		$response->vessel = $barcoWeek->data;
		$response->week = $barcoWeek->week;
		/*----------  BARCO  ----------*/

		if(!empty($cliente) && !empty($marca)){
			$response->data_header = $this->data_header($postdata->fecha_inicial, $postdata->fecha_final, $cliente, $marca);
		}

		$response->table_por_cluster = [];
		if(!empty($cliente) && !empty($marca)){
			$response->table_por_cluster = $this->por_cluster($postdata->fecha_inicial, $postdata->fecha_final, $cliente, $marca);
			$response->por_cluster = $response->table_por_cluster->totales;
		}

		return $response;
	}

	private function getBarco($filters = []){
		$response = new stdClass;
		$response->data = "";
		$data = new stdClass;

		if($filters->fecha_final != "" && $filters->fecha_inicial != ""){
			$sql = "SELECT 
				DATEDIFF('{$filters->fecha_final}' , '{$filters->fecha_inicial}') AS dia , 
				WEEK('{$filters->fecha_final}') AS fecha_fin , 
				WEEK('{$filters->fecha_inicial}') AS fecha_inicial,
				(WEEK('{$filters->fecha_final}') -  WEEK('{$filters->fecha_inicial}')) AS dateDiff";
			$data = $this->db->queryRow($sql);
			$data->dia = (int)$data->dia;
			$data->dateDiff = (int)$data->dateDiff;
			$response->week = "";

			if($data->dia == 0){
				$sql = "SELECT barco FROM quality WHERE barco != '' AND DATE(fecha) BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}' GROUP BY barco";
				$response->week = $data->fecha_fin;
			}elseif ($data->dia != 0 && $data->dateDiff == 0) {
				$sql = "SELECT barco FROM quality WHERE barco != '' AND DATE(fecha) BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}' GROUP BY barco";
				$response->week = $data->fecha_fin;
			}else{
				return "";
			}
			$response->data = $this->db->queryOne($sql);
		}
		return $response;
	}

	private function por_cluster($inicio, $fin, $cliente, $marca){
		
		$response = new stdClass;
		$response->data = [];
		$sql = "SELECT id_calidad , fecha , cliente , marca , 
				(calidad_gajos.gajo_count + 1) AS gajo_count , calidad_gajos.gajo as peso,
				(SELECT MAX(calidad_gajos.gajo) FROM calidad_gajos WHERE id_calidad = calidad.id) AS maxPEso,
				(SELECT MIN(calidad_gajos.gajo) FROM calidad_gajos WHERE id_calidad = calidad.id) AS minPEso
				FROM calidad
				INNER JOIN calidad_gajos ON calidad.id = calidad_gajos.id_calidad
				WHERE marca = '{$marca}' AND cliente = '{$cliente}' AND DATE(fecha) BETWEEN '$inicio' AND '$fin'";
		$response->data = $this->db->queryAll($sql);
		$data = [];
		$calidad = [];
		$count = 0;
		$label = [];
		$dataGeneral = [];
		$prom = [];
		$totales = [];
		$tempTotal = [];
		foreach ($response->data as $key => $value) {
			if(!in_array($value->id_calidad, $calidad)){
				$calidad[] = $value->id_calidad;
				$count++;
				$label[] = "Hora ".$count;
				$totales["Max"]["Hora ".$count] = 0;
				$totales["Min"]["Hora ".$count] = 0;
				$totales["Promedio"]["Hora ".$count] = 0;
				$totales["Desviacion Estandar"]["Hora ".$count] = 0;
				$tempTotal["Varianza"]["Hora ".$count] = 0;
				$dataGeneral["Hora ".$count] = [];
			}
			$dataGeneral["Hora ".$count][] = (int)$value->peso;
			$data[$value->gajo_count]["nombre"] = (int)$value->gajo_count;
			$data[$value->gajo_count]["Hora ".$count] = $value->peso;
			$totales["Total"]["Hora ".$count] += (int)$value->peso;
			$prom["Promedio"]["Hora ".$count]["count"]++;
		}

		$Varianza = 0;
		foreach ($label as $key => $value) {
			$key = ($key +1);
			$Varianza = 0;
			$totales["Max"]["Hora ".$key] = max($dataGeneral["Hora ".$key]);
			$totales["Min"]["Hora ".$key] = min($dataGeneral["Hora ".$key]);
			$totales["Promedio"]["Hora ".$key] = round($totales["Total"]["Hora ".$key] / $prom["Promedio"]["Hora ".$key]["count"] , 3);
			foreach ($dataGeneral["Hora ".$key] as $llave => $valor) {
				$Varianza += (($totales["Promedio"]["Hora ".$key] - $valor) * ($totales["Promedio"]["Hora ".$key] - $valor));
			}
			$tempTotal["Varianza"]["Hora ".$key] = ($Varianza / ($prom["Promedio"]["Hora ".$key]["count"] - 1));
			$totales["Desviacion Estandar"]["Hora ".$key] = round(sqrt($tempTotal["Varianza"]["Hora ".$key]), 2);
		}
		$totales["categories"] = $label;
		$response->labels = new stdClass;
		$response->table = new stdClass;
		$response->totales = $totales;
		$response->labels = $label;
		$response->table = $data;
		return $response;
	}

	private function data_header($inicio, $fin, $cliente, $marca){
		$sql = "SELECT DISTINCT(peso) as cantidad
				FROM quality
				WHERE UPPER(cliente) = '$cliente' AND UPPER(marca) = '$marca' AND peso IS NOT NULL AND DATE(fecha) BETWEEN '$inicio' AND '$fin'";
		$rp1 = $this->db->queryRow($sql);
		$sql = "SELECT DISTINCT(cluster) as cantidad
				FROM quality
				WHERE UPPER(cliente) = '$cliente' AND UPPER(marca) = '$marca' AND cluster IS NOT NULL AND DATE(fecha) BETWEEN '$inicio' AND '$fin'";
		$rp2 = $this->db->queryRow($sql);
		$sql = "SELECT * FROM brands WHERE nombre = '$marca'";
		$rp3 = $this->db->queryRow($sql);
		$logo = $rp3->logotipo;
		if(!file_exists($_SERVER['DOCUMENT_ROOT']."/".$logo)){
			$logo = "logos/marcas/no-available.png";
		}
		$sql = "SELECT marca FROM quality WHERE cliente = '$cliente' AND DATE(fecha) BETWEEN '$inicio' AND '$fin' GROUP BY marca";
		$res = $this->db->queryAll($sql);
		$marcas = [];
		foreach ($res as $key => $value) {
			$value = (object)$value;
			$marcas[] = $value;
		}
		return array(
			'peso' => $rp1->cantidad,
			'cluster' => $rp2->cantidad,
			'logo' => $logo,
			'marcas' => $marcas,
		);
	}
	
	private function tabla_principal($inicio, $fin, $cliente, $marca){
		$sql_add = "";
		if(!empty($cliente) && !empty($marca)){
			$sql_add = " AND cliente = '$cliente' AND marca = '$marca'";
		}
		$sql = "SELECT * FROM quality WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' $sql_add";
		// D($sql);
		$res = $this->db->queryAll($sql);
		$data = [];
		$data2 = [];
		foreach ($res as $key => $value) {
			$data2 = [];
			$value = (object)$value;
				
			$sql2 = "SELECT campo, type, (cantidad) AS cantidad FROM quality_detalle_unit WHERE 
			id_quality = $value->id AND type='ROTS/MOLDS' GROUP BY campo";
			// D($sql2);
			$res2 = $this->db->queryAll($sql2);
			foreach ($res2 as $key2 => $value2) {
				$value2 = (object)$value2;
				$data2[] = $value2;
			}
			$value->ROTS = $data2;

			$data2 = [];
			$sql2 = "SELECT campo, type, (cantidad) AS cantidad FROM quality_detalle_unit WHERE 
			id_quality = $value->id AND type='SCARS/BRUISES' GROUP BY campo";
			$res2 = $this->db->queryAll($sql2);
			foreach ($res2 as $key2 => $value2) {
				$value2 = (object)$value2;
				$data2[] = $value2;
			}
			$value->SCARS = $data2;

			$data2 = [];
			$sql2 = "SELECT campo, type, (cantidad) AS cantidad FROM quality_detalle_unit WHERE 
			id_quality = $value->id AND type='LATEX' GROUP BY campo";
			$res2 = $this->db->queryAll($sql2);
			foreach ($res2 as $key2 => $value2) {
				$value2 = (object)$value2;
				$data2[] = $value2;
			}
			$value->LATEX = $data2;

			$data2 = [];
			$sql2 = "SELECT campo, type, (cantidad) AS cantidad FROM quality_detalle_unit WHERE 
			id_quality = $value->id AND type='LENGTH/GRAD' GROUP BY campo";
			$res2 = $this->db->queryAll($sql2);
			foreach ($res2 as $key2 => $value2) {
				$value2 = (object)$value2;
				$data2[] = $value2;
			}
			$value->LENGTH = $data2;

			$data2 = [];
			$sql2 = "SELECT campo, type, (cantidad) AS cantidad FROM quality_detalle_unit WHERE 
			id_quality = $value->id AND type='OTHER MAJOR DEFECTS' GROUP BY campo";
			$res2 = $this->db->queryAll($sql2);
			foreach ($res2 as $key2 => $value2) {
				$value2 = (object)$value2;
				$data2[] = $value2;
			}
			$value->MAJOR = $data2;

			$data2 = [];
			$sql2 = "SELECT campo, type, (cantidad) AS cantidad FROM quality_detalle_unit WHERE 
			id_quality = $value->id AND type='PACKING DEFECTS' GROUP BY campo";
			$res2 = $this->db->queryAll($sql2);
			foreach ($res2 as $key2 => $value2) {
				$value2 = (object)$value2;
				$data2[] = $value2;
			}
			$value->PACKING = $data2;

			$data2 = [];
			$sql2 = "SELECT campo, type, (cantidad) AS cantidad FROM quality_detalle_unit WHERE 
			id_quality = $value->id AND type='OTHER OBSERVATIONS' GROUP BY campo";
			$res2 = $this->db->queryAll($sql2);
			foreach ($res2 as $key2 => $value2) {
				$value2 = (object)$value2;
				$data2[] = $value2;
			}
			$value->OTHER = $data2;

			$data[] = $value;
		}
		return $data;
	}

	private function tags($inicio, $fin, $cliente, $marca){
		$sql_add = "";
		if(!empty($cliente) && !empty($marca)){
			$sql_add = " AND marca = '$cliente'";
			#AND marca = '$marca'
		}

		$sql = "SELECT AVG(dedos) AS calidad FROM quality WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' $sql_add";
		// D($sql);
		$rp1 = $this->db->queryRow($sql);
		$sql = "SELECT MAX(calidad) AS calidad_maxima FROM quality	WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND calidad > 0  $sql_add";
		$rp2 = $this->db->queryRow($sql);
		$sql = "SELECT MIN(calidad) AS calidad_minima FROM quality	WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND calidad > 0 $sql_add";
		$rp3 = $this->db->queryRow($sql);
		$sql = "SELECT '' AS desviacion_estandar FROM quality	WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' $sql_add";
		$rp4 = $this->db->queryRow($sql);
		$sql = "SELECT AVG(peso) AS peso FROM quality WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' $sql_add";
		$rp5 = $this->db->queryRow($sql);
		$sql = "SELECT AVG(cluster) AS cluster FROM quality WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' $sql_add";
		$rp6 = $this->db->queryRow($sql);
		$sql = "SELECT calidad AS calidad_dedos FROM quality WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' $sql_add";
		$rp7 = $this->db->queryRow($sql);
		$sql = "SELECT calidad AS calidad_cluster FROM quality WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' $sql_add";
		$rp8 = $this->db->queryRow($sql);
		$sql = "SELECT AVG(dedos) AS dedos_promedio FROM quality WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND dedos > 0 $sql_add";
		$rp9 = $this->db->queryRow($sql);
		$tags = [];

		$sql_desviacion = "SELECT (SELECT AVG(calidad) AS avg_calidad FROM quality WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND calidad > 0 $sql_add) AS avg_calidad , 
							calidad 
							FROM quality WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND calidad > 0 $sql_add";
		// D($sql_desviacion);
		$dEstandar = $this->db->queryAll($sql_desviacion);
		$Varianza = 0;
		$Count = 0;
		$Desviacion_estandar = 0;
		foreach ($dEstandar as $key => $value) {
			$Count++;
			// D($value->avg_calidad);
			// D($value->calidad_dedos);
			$Varianza += (($value->avg_calidad - $value->calidad) * ($value->avg_calidad - $value->calidad));
		}
		// D($Varianza);
		$Desviacion_estandar = ($Varianza / ($Count-1));


		$tags = array(
			'calidad' => 100,
			'calidad_maxima' => $rp2->calidad_maxima,
			'calidad_dedos' => $rp7->calidad_dedos,
			'calidad_minima' => $rp3->calidad_minima,
			'desviacion_estandar' => $Desviacion_estandar,
			'peso' => $rp5->peso,
			'cluster' => $rp6->cluster,
			'calidad_cluster' => $rp8->calidad_cluster,
			'dedos_promedio' => $rp1->calidad
		);
		return $tags;
	}

	private function grafica_principal_calidad($inicio, $fin, $cliente, $marca){
		$sql_add = "";
		if(!empty($cliente) && !empty($marca)){
			$sql_add = " AND cliente = '$cliente' AND marca = '$marca'";
		}

		$sql = "SELECT DATE(fecha) AS fecha, AVG(calidad) AS calidad, MAX(calidad) AS calidad_maxima, MIN(calidad) AS calidad_minima
				FROM quality
				WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND calidad > 0  AND calidad > 0
				$sql_add
				GROUP BY DATE(fecha)";
		$res = $this->db->queryAll($sql);
		$data = [];
		foreach ($res as $key => $value) {
			$value = (object)$value;
			$data[$value->fecha] = $value;
		}
		return $data;
	}

	private function grafica_principal_de($inicio, $fin){
		$sql_add = "";
		if(!empty($cliente) && !empty($marca)){
			$sql_add = " AND cliente = '$cliente' AND marca = '$marca'";
		}

		$sql_desviacion = "SELECT (SELECT AVG(calidad) AS avg_calidad FROM quality WHERE DATE(fecha) = qu.fecha AND calidad > 0 $sql_add) AS avg_calidad , 
							calidad 
							DATE(fecha) AS fecha
							FROM quality AS qu WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND calidad > 0 $sql_add";
		// D($sql_desviacion);
		$dEstandar = $this->db->queryAll($sql_desviacion);
		$Varianza = 0;
		$Count = 0;
		$Desviacion_estandar = 0;
		foreach ($dEstandar as $key => $value) {
			$Count++;
			// D($value->avg_calidad);
			// D($value->calidad_dedos);
			$Varianza += (($value->avg_calidad - $value->calidad) * ($value->avg_calidad - $value->calidad));
		}
		// D($Varianza);
		$Desviacion_estandar = ($Varianza / ($Count-1));

		foreach ($res as $key => $value) {
			$value = (object)$value;
			$data[$value->fecha] = $value;
		}
		return $data;
	}

	private function grafica_principal_peso($inicio, $fin){
		$sql_add = "";
		if(!empty($cliente) && !empty($marca)){
			$sql_add = " AND cliente = '$cliente' AND marca = '$marca'";
		}

		$sql = "SELECT DATE(fecha) AS fecha, AVG(peso) AS peso
				FROM quality
				WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND calidad > 0 
				$sql_add
				GROUP BY DATE(fecha)";
		$res = $this->db->queryAll($sql);
		$data = [];
		foreach ($res as $key => $value) {
			$value = (object)$value;
			$data[$value->fecha] = $value;
		}
		return $data;
	}
	
	private function grafica_principal_peso_cluster($inicio, $fin){
		$sql_add = "";
		if(!empty($cliente) && !empty($marca)){
			$sql_add = " AND cliente = '$cliente' AND marca = '$marca'";
		}

		$sql = "SELECT DATE(fecha) AS fecha, AVG(peso_cluster) AS peso_cluster
				FROM calidad
				WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND calidad_dedos > 0  AND cantidad_gajos > 0
				$sql_add
				GROUP BY DATE(fecha)";
		$res = $this->db->queryAll($sql);
		$data = [];
		foreach ($res as $key => $value) {
			$value = (object)$value;
			$data[$value->fecha] = $value;
		}
		return $data;
	}

	private function grafica_principal_cluster($inicio, $fin){
		$sql_add = "";
		if(!empty($cliente) && !empty($marca)){
			$sql_add = " AND cliente = '$cliente' AND marca = '$marca'";
		}

		$sql = "SELECT DATE(fecha) AS fecha, AVG(cantidad_gajos) AS cluster
				FROM calidad
				WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND calidad_dedos > 0  AND cantidad_gajos > 0
				$sql_add
				GROUP BY DATE(fecha)";
		$res = $this->db->queryAll($sql);
		$data = [];
		foreach ($res as $key => $value) {
			$value = (object)$value;
			$data[$value->fecha] = $value;
		}
		return $data;
	}

	#victor
	private function grafica_calidad_historico_marcas($inicio, $fin, $cliente, $marca){
		$sql_add = "";
		if(!empty($cliente) && !empty($marca)){
			$sql_add = " AND cliente = '$cliente' AND marca = '$marca'";
		}

		$sql = "SELECT DATE(fecha) AS fecha, AVG(calidad_dedos) AS rewe, MAX(calidad_dedos) AS pinalinda, MIN(calidad_dedos) AS palmar_aldi
				FROM calidad
				WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND calidad_dedos > 0  AND cantidad_gajos > 0
				$sql_add
				GROUP BY DATE(fecha)";
		$res = $this->db->queryAll($sql);
		$data = [];
		foreach ($res as $key => $value) {
			$value = (object)$value;
			$data[$value->fecha] = $value;
		}
		return $data;
	}

	private function grafica_danos_total($inicio, $fin){
		$sql_add = "";
		if(!empty($cliente) && !empty($marca)){
			$sql_add = " AND cliente = '$cliente' AND marca = '$marca'";
		}

		$data = [];
		$sql = "SELECT fecha, AVG(seleccion) AS seleccion FROM calidad WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND seleccion > 0  $sql_add GROUP BY DATE(fecha)";
		$res = $this->db->queryAll($sql);
		foreach ($res as $key => $value) {
			$value = (object)$value;
			$data[$value->fecha]['seleccion'] = (float)$value->seleccion;
			$data[$value->fecha]['empaque'] = 0;
			$data[$value->fecha]['otros'] = 0;
		}

		$sql = "SELECT fecha, AVG(empaque) AS empaque FROM calidad WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND empaque > 0 $sql_add GROUP BY DATE(fecha)";
		$res = $this->db->queryAll($sql);
		foreach ($res as $key => $value) {
			$value = (object)$value;
			$data[$value->fecha]['empaque'] = (float)$value->empaque;
		}

		$sql = "SELECT fecha, AVG(otros) AS otros FROM calidad WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND otros > 0 $sql_add GROUP BY DATE(fecha)";
		$res = $this->db->queryAll($sql);
		foreach ($res as $key => $value) {
			$value = (object)$value;
			$data[$value->fecha]['otros'] = (float)$value->otros;
		}

		return $data;
	}

	// get all campos
	private function grafica_danos_get_campos($type){
		$sql_add = "";
		if(!empty($cliente) && !empty($marca)){
			$sql_add = " AND cliente = '$cliente' AND marca = '$marca'";
		}

		$data = [];
		$type = strtoupper($type);
		$sql = "SELECT campo FROM calidad_detalle WHERE TYPE = '$type'GROUP BY campo";
		$res = $this->db->queryAll($sql);
		foreach ($res as $key => $value) {
			$value = (object)$value;
			$data[] = $value->campo;
		}
		return $data;
	}

	private function grafica_danos_seleccion($inicio, $fin){
		$sql_add = "";
		if(!empty($cliente) && !empty($marca)){
			$sql_add = " AND c.cliente = '$cliente' AND c.marca = '$marca'";
		}

		$data = [];
		$sql = "SELECT c.fecha, cd.campo, AVG(cd.cantidad) AS cantidad
				FROM calidad_detalle cd
				INNER JOIN calidad AS c ON c.id = cd.id_calidad
				WHERE cd.type = 'SELECCION' AND DATE(c.fecha) BETWEEN '$inicio' AND '$fin' AND cantidad > 0
				$sql_add
				GROUP BY cd.campo";
		$res = $this->db->queryAll($sql);
		foreach ($res as $key => $value) {
			$value = (object)$value;
			$data[$value->fecha][$value->campo] = (float)$value->cantidad;
		}
		return $data;
	}

	private function grafica_danos_empaque($inicio, $fin){
		$sql_add = "";
		if(!empty($cliente) && !empty($marca)){
			$sql_add = " AND c.cliente = '$cliente' AND c.marca = '$marca'";
		}

		$data = [];
		$sql = "SELECT c.fecha, cd.campo, AVG(cd.cantidad) AS cantidad
				FROM calidad_detalle cd
				INNER JOIN calidad AS c ON c.id = cd.id_calidad
				WHERE cd.type = 'EMPAQUE' AND DATE(c.fecha) BETWEEN '$inicio' AND '$fin' AND cantidad > 0
				$sql_add
				GROUP BY cd.campo";
		$res = $this->db->queryAll($sql);
		foreach ($res as $key => $value) {
			$value = (object)$value;
			$data[$value->fecha][$value->campo] = (float)$value->cantidad;
		}
		return $data;
	}

	private function grafica_danos_otros($inicio, $fin){
		$sql_add = "";
		if(!empty($cliente) && !empty($marca)){
			$sql_add = " AND c.cliente = '$cliente' AND c.marca = '$marca'";
		}

		$data = [];
		$sql = "SELECT c.fecha, cd.campo, AVG(cd.cantidad) AS cantidad
				FROM calidad_detalle cd
				INNER JOIN calidad AS c ON c.id = cd.id_calidad
				WHERE cd.type = 'OTROS' AND DATE(c.fecha) BETWEEN '$inicio' AND '$fin' AND cantidad > 0
				$sql_add
				GROUP BY cd.campo";
		$res = $this->db->queryAll($sql);
		foreach ($res as $key => $value) {
			$value = (object)$value;
			$data[$value->fecha][$value->campo] = (float)$value->cantidad;
		}
		return $data;
	}


	private function tabla_principal_calidad($inicio, $fin){
		$sql_add = "";
		if(!empty($cliente) && !empty($marca)){
			$sql_add = " AND cliente = '$cliente' AND marca = '$marca'";
		}

		$data = [];
		$sql = "SELECT cliente, marca, AVG(calidad) AS calidad FROM quality WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND calidad > 0 $sql_add GROUP BY DATE(fecha), cliente, marca";
		$res = $this->db->queryAll($sql);
		foreach ($res as $key => $value) {
			$value = (object)$value;
			$data[$value->cliente][$value->marca]['calidad']['total'] += $value->calidad;
			$data[$value->cliente][$value->marca]['calidad']['registros']++;
		}

		$sql = "SELECT cliente, marca, AVG(cluster) AS cluster FROM quality WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND cluster > 0 $sql_add GROUP BY DATE(fecha), cliente, marca";
		$res = $this->db->queryAll($sql);
		foreach ($res as $key => $value) {
			$value = (object)$value;
			$data[$value->cliente][$value->marca]['cluster']['total'] += $value->cluster;
			$data[$value->cliente][$value->marca]['cluster']['registros']++;
		}

		$sql = "SELECT cliente, marca, AVG(peso) AS peso FROM quality WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND peso > 0 $sql_add GROUP BY DATE(fecha), cliente, marca";
		$res = $this->db->queryAll($sql);
		foreach ($res as $key => $value) {
			$value = (object)$value;
			$data[$value->cliente][$value->marca]['peso']['total'] += $value->peso;
			$data[$value->cliente][$value->marca]['peso']['registros']++;
		}

		$data_final = [];
		$c= 0;
		foreach($data as $cliente => $record){
			$data_final[$c]['id'] = $c;
			$data_final[$c]['cliente_marca'] = $cliente;
			$record_keys = array_keys($record);

			$suma_calidad = 0;
			$avg_calidad  = 0;
			$suma_cluster = 0;
			$avg_cluster  = 0;
			$suma_peso    = 0;
			$avg_peso     = 0;
			$d = 0;
			foreach($record_keys as $marca){
				$data_final[$c]['marcas'][$d]['marca'] = $marca;
				$data_final[$c]['marcas'][$d]['calidad'] = $data[$cliente][$marca]['calidad']['total'] / $data[$cliente][$marca]['calidad']['registros'];
				$suma_calidad += $data_final[$c]['marcas'][$d]['calidad'];
				$avg_calidad++;
				$data_final[$c]['marcas'][$d]['cluster'] = $data[$cliente][$marca]['cluster']['total'] / $data[$cliente][$marca]['cluster']['registros'];
				$suma_cluster += $data_final[$c]['marcas'][$d]['cluster'];
				$avg_cluster++;
				$data_final[$c]['marcas'][$d]['peso'] = $data[$cliente][$marca]['peso']['total'] / $data[$cliente][$marca]['peso']['registros'];
				$suma_peso += $data_final[$c]['marcas'][$d]['peso'];
				$avg_peso++;
				$d++;
			}
			$data_final[$c]['calidad'] = $suma_calidad / $avg_calidad;
			$data_final[$c]['cluster'] = $suma_cluster / $avg_cluster;
			$data_final[$c]['peso']    = $suma_peso / $avg_peso;
			$c++;
		}

		return $data_final;
	}

	private function tabla_principal_danos($inicio, $fin){
		$sql_add = "";
		if(!empty($cliente) && !empty($marca)){
			$sql_add = " AND c.cliente = '$cliente' AND c.marca = '$marca'";
		}

		$data = [];
		$sql = "SELECT c.cliente, c.marca, AVG(cd.cantidad) AS cantidad
				FROM quality_detalle_unit cd
				INNER JOIN quality AS c ON c.id = cd.id_quality
				WHERE cd.type = 'ROTS/MOLDS' AND DATE(c.fecha) BETWEEN '$inicio' AND '$fin' AND cantidad > 0
				$sql_add
				GROUP BY c.cliente, c.marca";
		$res = $this->db->queryAll($sql);
		foreach ($res as $key => $value) {
			$value = (object)$value;
			$data[$value->cliente][$value->marca]['ROTS']['total'] += $value->cantidad;
			$data[$value->cliente][$value->marca]['ROTS']['registros']++;
		}

		$sql = "SELECT c.cliente, c.marca, AVG(cd.cantidad) AS cantidad
				FROM quality_detalle_unit cd
				INNER JOIN quality AS c ON c.id = cd.id_quality
				WHERE cd.type = 'SCARS/BRUISES' AND DATE(c.fecha) BETWEEN '$inicio' AND '$fin' AND cantidad > 0
				$sql_add
				GROUP BY c.cliente, c.marca";
		$res = $this->db->queryAll($sql);
		foreach ($res as $key => $value) {
			$value = (object)$value;
			$data[$value->cliente][$value->marca]['SCARS']['total'] += $value->cantidad;
			$data[$value->cliente][$value->marca]['SCARS']['registros']++;
		}

		$sql = "SELECT c.cliente, c.marca, AVG(cd.cantidad) AS cantidad
				FROM quality_detalle_unit cd
				INNER JOIN quality AS c ON c.id = cd.id_quality
				WHERE cd.type = 'LATEX' AND DATE(c.fecha) BETWEEN '$inicio' AND '$fin' AND cantidad > 0
				$sql_add
				GROUP BY c.cliente, c.marca";
		$res = $this->db->queryAll($sql);
		foreach ($res as $key => $value) {
			$value = (object)$value;
			$data[$value->cliente][$value->marca]['LATEX']['total'] += $value->cantidad;
			$data[$value->cliente][$value->marca]['LATEX']['registros']++;
		}

		$sql = "SELECT c.cliente, c.marca, AVG(cd.cantidad) AS cantidad
				FROM quality_detalle_unit cd
				INNER JOIN quality AS c ON c.id = cd.id_quality
				WHERE cd.type = 'LENGTH/GRAD' AND DATE(c.fecha) BETWEEN '$inicio' AND '$fin' AND cantidad > 0
				$sql_add
				GROUP BY c.cliente, c.marca";
		$res = $this->db->queryAll($sql);
		foreach ($res as $key => $value) {
			$value = (object)$value;
			$data[$value->cliente][$value->marca]['LENGTH']['total'] += $value->cantidad;
			$data[$value->cliente][$value->marca]['LENGTH']['registros']++;
		}

		$sql = "SELECT c.cliente, c.marca, AVG(cd.cantidad) AS cantidad
				FROM quality_detalle_unit cd
				INNER JOIN quality AS c ON c.id = cd.id_quality
				WHERE cd.type = 'OTHER MAJOR DEFECTS' AND DATE(c.fecha) BETWEEN '$inicio' AND '$fin' AND cantidad > 0
				$sql_add
				GROUP BY c.cliente, c.marca";
		$res = $this->db->queryAll($sql);
		foreach ($res as $key => $value) {
			$value = (object)$value;
			$data[$value->cliente][$value->marca]['MAJOR']['total'] += $value->cantidad;
			$data[$value->cliente][$value->marca]['MAJOR']['registros']++;
		}

		$data_final = [];
		$c= 0;
		foreach($data as $cliente => $record){
			$data_final[$c]['id'] = $c;
			$data_final[$c]['cliente_marca'] = $cliente;
			$record_keys = array_keys($record);

			$suma_calidad = 0;
			$avg_calidad  = 0;
			$suma_cluster = 0;
			$avg_cluster  = 0;
			$suma_peso    = 0;
			$avg_peso     = 0;
			$suma_length   = 0;
			$avg_length   = 0;
			$suma_major    = 0;
			$avg_major    = 0;
			$d = 0;

			foreach($record_keys as $marca){
				$data_final[$c]['marcas'][$d]['marca'] = $marca;
				/*----------  SCARS/BRUISES  ----------*/
				$data_final[$c]['marcas'][$d]['SCARS'] = $data[$cliente][$marca]['SCARS']['total'] / $data[$cliente][$marca]['SCARS']['registros'];
				$suma_calidad += $data_final[$c]['marcas'][$d]['SCARS'];
				$avg_calidad++;
				/*----------  SCARS/BRUISES  ----------*/
				/*----------  ROTS/MOLDS  ----------*/
				$data_final[$c]['marcas'][$d]['ROTS'] = $data[$cliente][$marca]['ROTS']['total'] / $data[$cliente][$marca]['ROTS']['registros'];
				$suma_cluster += $data_final[$c]['marcas'][$d]['ROTS'];
				$avg_cluster++;
				/*----------  ROTS/MOLDS  ----------*/
				/*---------- LATEX  ----------*/
				$data_final[$c]['marcas'][$d]['LATEX'] = $data[$cliente][$marca]['LATEX']['total'] / $data[$cliente][$marca]['LATEX']['registros'];
				$suma_peso += $data_final[$c]['marcas'][$d]['LATEX'];
				$avg_peso++;
				/*---------- LATEX  ----------*/
				/*---------- LENGTH/GRAD  ----------*/
				$data_final[$c]['marcas'][$d]['LENGTH'] = $data[$cliente][$marca]['LENGTH']['total'] / $data[$cliente][$marca]['LENGTH']['registros'];
				$suma_length += $data_final[$c]['marcas'][$d]['LENGTH'];
				$avg_length++;
				/*---------- LENGTH/GRAD  ----------*/
				/*---------- OTHER MAJOR DEFECTS  ----------*/
				$data_final[$c]['marcas'][$d]['MAJOR'] = $data[$cliente][$marca]['MAJOR']['total'] / $data[$cliente][$marca]['MAJOR']['registros'];
				$major_peso += $data_final[$c]['marcas'][$d]['MAJOR'];
				$avg_major++;
				/*---------- OTHER MAJOR DEFECTS  ----------*/
				$d++;
			}
			$data_final[$c]['SCARS'] = (float)($suma_calidad / $avg_calidad);
			$data_final[$c]['ROTS'] = (float)($suma_cluster / $avg_cluster);
			$data_final[$c]['LATEX']    = (float)($suma_peso / $avg_peso);
			$data_final[$c]['LENGTH']    = (float)($suma_length / $avg_length);
			$data_final[$c]['MAJOR']    = (float)($suma_major / $avg_major);
			$c++;
		}

		return $data_final;
	}



	private function getDanhos($inicio, $fin, $cliente, $marca){
		$sql_add = "";
		if(!empty($cliente) && !empty($marca)){
			$sql_add = " AND cliente = '$cliente' AND marca = '$marca'";
		}

		$data = [];
		$sql = "SELECT 'SELECCION' AS name, 
		CASE WHEN AVG(seleccion) IS NULL or AVG(seleccion) = ''
		THEN 0
		ELSE AVG(seleccion) END AS value FROM calidad WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND seleccion > 0 $sql_add";
		// D($sql);
		$data[] = $this->db->queryRow($sql);
		// foreach ($res as $key => $value) {
		// 	$value = (object)$value;
		// 	$data[0]['name']  = "SELECCION";
		// 	$data[0]['value'] = (float)$value->seleccion;
		// 	$data[1]['name'] = "EMPAQUE";
		// 	$data[1]['value'] = 0;
		// 	$data[2]['name'] = "OTROS";
		// 	$data[2]['value'] = 0;
		// }

		$sql = "SELECT 'EMPAQUE' AS name, CASE WHEN AVG(empaque) IS NULL or AVG(empaque) = ''
		THEN 0
		ELSE AVG(empaque) END AS value FROM calidad WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND empaque > 0 $sql_add";
		$data[] = $this->db->queryRow($sql);
		// foreach ($res as $key => $value) {
		// 	$value = (object)$value;
		// 	$data[1]['value'] = (float)$value->empaque;
		// }

		$sql = "SELECT 'OTROS' AS name,CASE WHEN AVG(otros) IS NULL or AVG(otros) = ''
		THEN 0
		ELSE AVG(otros) END AS value FROM calidad WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND otros > 0 $sql_add";
		$data[] = $this->db->queryRow($sql);
		// foreach ($res as $key => $value) {
		// 	$value = (object)$value;
		// 	$data[2]['value'] = (float)$value->otros;
		// }

		return $data;
		/*
		$sql = "SELECT AVG(seleccion) AS seleccion,
		AVG(empaque) AS empaque,
		AVG(otros) AS otros
		FROM calidad
		WHERE seleccion > 0 AND empaque > 0 AND otros > 0 AND DATE(fecha) BETWEEN '$inicio' AND '$fin'";
		$danhos = $this->db->queryAll($sql);
		$data = [];
		foreach ($danhos as $key => $value) {
			$value = (object)$value;
			$data = [
				[
					"value" => $value->seleccion,
					"name" => "SELECCION"
				],
				[
					"value" => $value->empaque,
					"name" => "EMPAQUE"
				],
				[	
					"value" => $value->otros,
					"name" => "OTROS"
				]
			];
		}
		return $data;
		*/
	}

	private function getDetails($type = "SELECCION", $inicio, $fin){
		$sql_add = "";
		if(!empty($cliente) && !empty($marca)){
			$sql_add = " AND c.cliente = '$cliente' AND c.marca = '$marca'";
		}

		$sql = "SELECT cd.campo AS name, SUM(cd.cantidad) as value FROM calidad_detalle cd INNER JOIN calidad AS c ON c.id = cd.id_calidad WHERE cd.type = '$type' AND DATE(c.fecha) BETWEEN '$inicio' AND '$fin' $sql_add GROUP BY cd.campo";
		// D($sql);
		$response = $this->db->queryAll($sql);
		$response->value = (int)$response->value;
		// $response = [];
		// foreach ($data as $key => $value) {
		// 	$response[] = [
		// 		"value" => $value->cantidad,
		// 		"name" => $value->campo
		// 	];
		// }

		return $response;
	}
}