<?php defined('PHRAPI') or die("Direct access not allowed!");

class Reportes {
	public $name;
	private $db;

	public function __construct(){
        $this->db = DB::getInstance();
		$this->session = Session::getInstance();
	}

	public function index(){
		
	}
	/*=========================================
	=            PANTALLA NUMERO 3            =
	=========================================*/
	
	/*----------  PANTALLA 3 : TABLA POR CAUSAS - GRAFICA CAUSAS  ----------*/
	public function getCausaLabor(){
		$postdata = (object)json_decode(file_get_contents("php://input"));
		$idfinca = getValueFrom($postdata,'idFinca','',FILTER_SANITIZE_PHRAPI_INT);
		$lote = getValueFrom($postdata,'idLote','',FILTER_SANITIZE_PHRAPI_INT);
		$idLabor = getValueFrom($postdata,'idLabor','',FILTER_SANITIZE_PHRAPI_INT);
		$fecha_fin = getValueFrom($postdata,'fecha_final','',FILTER_SANITIZE_STRING);
		$fecha_inicio = getValueFrom($postdata,'fecha_inicial','',FILTER_SANITIZE_STRING);
		$response = new stdClass;
		/**
		
			CONSULA DE CAUSAS:
			- idFinca
			- idLabor
			- idLote
			- Fecha
		
		 */
		
		$sql = "SELECT causa , CausasIndividual , buenas , malas ,muestras,valor,
			CASE
				WHEN valor = 1 THEN  ((buenas / muestras) * 100) 
				ELSE ((((1-(buenas / muestras))*CausasIndividual) / malas)*100)
			END AS porcentaje,
			CASE
				WHEN valor = 1 THEN  NULL
				ELSE (CausasIndividual / malas) * 100
			END AS porcentaje2
			FROM 
			(SELECT laborCausaDesc AS causa ,COUNT( causa) AS CausasIndividual,valor,
			(SELECT COUNT(*) AS total_muestras FROM muestras WHERE idFinca = '{$idfinca}' AND idLote ='{$lote}' AND valor = 1 AND idLabor = '{$idLabor}' AND fecha BETWEEN '{$fecha_inicio}' AND '{$fecha_fin}') AS buenas,
			(SELECT COUNT(*) AS total_muestras FROM muestras WHERE idFinca = '{$idfinca}' AND idLote ='{$lote}' AND valor = 0 AND idLabor = '{$idLabor}' AND fecha BETWEEN '{$fecha_inicio}' AND '{$fecha_fin}')  AS malas,
			(SELECT COUNT(distinct archivoJson_contador , archivoJson) AS total_causas FROM muestras WHERE 
					fecha BETWEEN '{$fecha_inicio}' AND '{$fecha_fin}' AND
					idFinca = '{$idfinca}' AND idLote ='{$lote}' AND idLabor = '{$idLabor}') AS muestras
			FROM muestras 
			WHERE 
				fecha BETWEEN '{$fecha_inicio}' AND '{$fecha_fin}' AND
				idFinca = '{$idfinca}' AND idLote ='{$lote}' AND idLabor = '{$idLabor}'
			GROUP BY idLaborCausa) as t
			ORDER BY valor DESC , causa ASC";
				// D($sql);
		$response->causas = $this->db->queryAll($sql);


		/**
		
			CONSULTA POR MES GRAFICA DE BARRAS:
			- listado por meses
			- Obtener valor por mes por labores
			- Obtener valor por mes por todas las labores
		
		 */
		

		$months = ["Ene" , "Feb"  , "Mar" , "Abr" , "May" , "Jun" , "Jul" ,"Ago", "Sep" , "Oct" , "Nov" , "Dic"];

		$response->principal = [];
		$labor = ""; 
		$labores = "";
		foreach ($months as $key => $value) {
			$labor = $this->getPromMonth(($key+1),$lote , $idLabor  , $idfinca);
			$labores = $this->getPromMonthGeneral(($key+1) ,$lote , $idLabor , $idfinca );
			// D($labor);
			// D($labores);
			$response->principal[] = [ 
				$value , 
				$labor->labor => $labor->promedioLabor,
				strtoupper($labores->labores) => $labores->promedioLote,
			];
		}

		return $response;
	}
	/*----------  PANTALLA 3 : TABLA POR CAUSAS  ----------*/

	/*----------  PANTALLA 3 : PROMEDIO POR MES GENERAL : GRAFICA DE BARRAS  ----------*/
	private function getPromMonthGeneral($month , $lote , $labor , $idfinca){
		$lote = $lote;
		$idfinca = $idfinca;
		$idLabor = $labor;
		$response = new stdClass;
		$response->data = 0;
		$sql = "SELECT
				CASE
					WHEN valor = 1 THEN  AVG(((buenas / muestras) * 100))
					ELSE 0
				END AS promedioLote , 'labores'
				FROM 
				(SELECT labor ,causa ,COUNT( causa) AS CausasIndividual,valor,
				(SELECT COUNT(*) AS total_muestras 
					FROM muestras 
				WHERE idFinca = '{$idfinca}' AND idLote ='{$lote}' AND valor = 1 AND MONTH(fecha) = {$month}) AS buenas,
				(SELECT COUNT(distinct archivoJson_contador , archivoJson) AS total_causas 
					FROM muestras 
				WHERE idFinca = '{$idfinca}' AND idLote ='{$lote}' AND MONTH(fecha) = {$month}) AS muestras
				FROM muestras 
				WHERE idFinca = '{$idfinca}' AND idLote ='{$lote}' AND MONTH(fecha) = {$month}) as e";
				// D($sql);
		$response->data = $this->db->queryRow($sql);

		return $response->data;
	}
	/*----------  PANTALLA 3 : PROMEDIO POR MES GENERAL : GRAFICA DE BARRAS  ----------*/

	/*----------  PANTALLA 3 : PROMEDIO POR MES POR LABOR : GRAFICA DE BARRAS  ----------*/
	private function getPromMonth($month , $lote , $labor , $idfinca){
		$lote = $lote;
		$idfinca = $idfinca;
		$idLabor = $labor;
		$response = new stdClass;
		$response->data = 0;
		$sql = "SELECT IFNULL(promedioLabor, 0) AS promedioLabor , IFNULL(labor , (SELECT nombre FROM labores WHERE id = '{$idLabor}')) AS labor
				FROM
				(SELECT valor , buenas , muestras ,
				CASE
					WHEN valor = 1 THEN  AVG(((buenas / muestras) * 100))
					ELSE NULL
				END AS promedioLabor,
				CASE 
					WHEN labor = '' THEN NULL
					WHEN labor = NULL THEN NULL
					ELSE labor
				END AS labor
				FROM 
				(SELECT labor ,causa ,COUNT( causa) AS CausasIndividual,valor,
				(SELECT COUNT(*) AS total_muestras 
					FROM muestras 
				WHERE idFinca = '{$idfinca}' AND idLote ='{$lote}' AND idLabor = '{$idLabor}' AND valor = 1 AND MONTH(fecha) = {$month}) AS buenas,
				(SELECT COUNT(distinct archivoJson_contador , archivoJson) AS total_causas 
					FROM muestras 
				WHERE idFinca = '{$idfinca}' AND idLote ='{$lote}' AND idLabor = '{$idLabor}' AND MONTH(fecha) = {$month}) AS muestras
				FROM muestras 
				WHERE idFinca = '{$idfinca}' AND idLote ='{$lote}' AND idLabor = '{$idLabor}' AND MONTH(fecha) = {$month}) as t) AS LaboresPromedio";
				// D($sql);
		$response->data = $this->db->queryRow($sql);

		return $response->data;
	}
	/*----------  PANTALLA 3 : PROMEDIO POR MES POR LABOR : GRAFICA DE BARRAS  ----------*/
	
	/*=====  FIN DE LA PANTALLA NUMERO 3  ======*/
	
	/*=========================================
	=            PANTALLA NUMERO 2            =
	=========================================*/
	public function getLoteLabor(){
		$postdata = (object)json_decode(file_get_contents("php://input"));
		$idfinca = getValueFrom($postdata,'idFinca','',FILTER_SANITIZE_PHRAPI_INT);
		$idLote = getValueFrom($postdata,'idLote','',FILTER_SANITIZE_PHRAPI_INT);
		$idLabor = getValueFrom($postdata,'idLabor','',FILTER_SANITIZE_PHRAPI_INT);
		$fecha_fin = getValueFrom($postdata,'fecha_final','',FILTER_SANITIZE_STRING);
		$fecha_inicio = getValueFrom($postdata,'fecha_inicial','',FILTER_SANITIZE_STRING);

		$response = new stdClass;
		$sql = "SELECT idLote, lote , idLabor , labor FROM muestras WHERE fecha BETWEEN '$fecha_inicio' AND '$fecha_fin' AND idLote = '{$idLote}' GROUP BY idLote ,idLabor ORDER BY lote , labor";
		$LaboresExist = $this->db->queryAll($sql);
		$lote = "";
		$promedio = 0;
		foreach ($LaboresExist as $key => $value) {
			$value = (object)$value;
			$promedio = $this->getLaborCausa($value->idLabor , $value->idLote , $fecha_inicio , $fecha_fin ,$idfinca  )->porcentaje;
			$response->causas["lote"]["labores"][$value->labor]["idLote"] = $value->idLote;
			$response->causas["lote"]["labores"][$value->labor]["idLabor"] = $value->idLabor;
			$response->causas["lote"]["labores"][$value->labor]["promedio"] =  ($promedio > 0) ? $promedio : 0;
		}

		$months_des = ["Ene" , "Feb"  , "Mar" , "Abr" , "May" , "Jun" , "Jul" ,"Ago", "Sep" , "Oct" , "Nov" , "Dic"];
		$months = ["Ene"  => [], "Feb"  => [] 
		, "Mar"  => [], "Abr"  => [], "May"  => [], "Jun"  => [], "Jul"  => [],"Ago" => [], "Sep"  => [], 
		"Oct"  => [], "Nov"  => [], "Dic" => []];

		$sql_detalle = "SELECT 
			MONTH(fecha) AS Mes,
			idLote, 
			lote , 
			idLabor , 
			labor,
			IF(MONTH(CURRENT_DATE) = MONTH(fecha) , -1 , MONTH(fecha)) AS MesActual 
			FROM muestras 
			WHERE YEAR(fecha) = YEAR(CURRENT_DATE)  
			AND fecha 
			GROUP BY idLote ,idLabor 
			ORDER BY MONTH(fecha) , lote , labor";
		$LaboresExistMonth = $this->db->queryAll($sql_detalle);
		$response->main = [];
		$Lpromedio = [];
		$response->main = $months;
		$response->lote_name = "";
		foreach ($LaboresExistMonth as $key => $value) {
			$value = (object)$value;
			$Lpromedio = $this->getPromMonthLoteAvg($value->Mes,$value->idLote , $value->idLabor  , $idfinca);
			if((int)$idLote == (int)$value->idLote){
				$response->lote_name = $value->lote;
				$response->main[$months_des[$value->Mes]]["lote"]["name"] = $value->lote;
				$response->main[$months_des[$value->Mes]]["lote"]["avg"]++;
				$response->main[$months_des[$value->Mes]]["lote"]["sum"] += $Lpromedio->porcentaje;
			}
			$response->main[$months_des[$value->Mes]]["lotes"][$value->lote]["avg"]++;
			$response->main[$months_des[$value->Mes]]["lotes"][$value->lote]["sum"] += $Lpromedio->porcentaje;
		}

		return $response;
	}

	private function getPromMonthLoteAvg($month , $lote , $labor , $idfinca){
		$lote = $lote;
		$idfinca = $idfinca;
		$idLabor = $labor;
		$response = new stdClass;
		$response->data = 0;
		$sql = "SELECT causa , CausasIndividual , buenas , malas ,muestras,valor,
				CASE
					WHEN valor = 1 THEN  ((buenas / muestras) * 100) 
					ELSE ((((1-(buenas / muestras))*CausasIndividual) / malas)*100)
				END AS porcentaje,
				CASE
					WHEN valor = 1 THEN  NULL
					ELSE (CausasIndividual / malas) * 100
				END AS porcentaje2
				FROM 
				(SELECT laborCausaDesc AS causa ,COUNT( causa) AS CausasIndividual,valor,
				(SELECT COUNT(*) AS total_muestras FROM muestras WHERE idFinca = '{$idfinca}' AND idLote ='{$lote}' AND valor = 1 AND idLabor = '{$labor}' AND MONTH(fecha) = '{$month}') AS buenas,
				(SELECT COUNT(*) AS total_muestras FROM muestras WHERE idFinca = '{$idfinca}' AND idLote ='{$lote}' AND valor = 0 AND idLabor = '{$labor}' AND MONTH(fecha) = '{$month}')  AS malas,
				(SELECT COUNT(distinct archivoJson_contador , archivoJson) AS total_causas FROM muestras WHERE 
						MONTH(fecha) = '{$month}' AND
						idFinca = '{$idfinca}' AND idLote ='{$lote}' AND idLabor = '{$labor}') AS muestras
				FROM muestras 
				WHERE 
					MONTH(fecha) = '{$month}' AND
					idFinca = '{$idfinca}' AND idLote ='{$lote}' AND idLabor = '{$labor}'
				GROUP BY idLaborCausa) as t
				ORDER BY valor DESC , causa ASC";
				// D($sql);
		$response->data = $this->db->queryRow($sql);

		return $response->data;
	}

	private function getLaborCausa($idLabor , $idLote , $fecha_inicio , $fecha_fin , $idFinca){
		$response = new stdClass;
		$sql = "SELECT causa , CausasIndividual , buenas , malas ,muestras,valor,
				CASE
					WHEN valor = 1 THEN  ((buenas / muestras) * 100) 
					ELSE ((((1-(buenas / muestras))*CausasIndividual) / malas)*100)
				END AS porcentaje,
				CASE
					WHEN valor = 1 THEN  NULL
					ELSE (CausasIndividual / malas) * 100
				END AS porcentaje2
				FROM 
				(SELECT laborCausaDesc AS causa ,COUNT( causa) AS CausasIndividual,valor,
				(SELECT COUNT(*) AS total_muestras FROM muestras WHERE idFinca = '{$idFinca}' AND idLote ='{$idLote}' AND valor = 1 AND idLabor = '{$idLabor}' AND fecha BETWEEN '{$fecha_inicio}' AND '{$fecha_fin}') AS buenas,
				(SELECT COUNT(*) AS total_muestras FROM muestras WHERE idFinca = '{$idFinca}' AND idLote ='{$idLote}' AND valor = 0 AND idLabor = '{$idLabor}' AND fecha BETWEEN '{$fecha_inicio}' AND '{$fecha_fin}')  AS malas,
				(SELECT COUNT(distinct archivoJson_contador , archivoJson) AS total_causas FROM muestras WHERE 
						fecha BETWEEN '{$fecha_inicio}' AND '{$fecha_fin}' AND
						idFinca = '{$idFinca}' AND idLote ='{$idLote}' AND idLabor = '{$idLabor}') AS muestras
				FROM muestras 
				WHERE 
					fecha BETWEEN '{$fecha_inicio}' AND '{$fecha_fin}' AND
					idFinca = '{$idFinca}' AND idLote ='{$idLote}' AND idLabor = '{$idLabor}'
				GROUP BY idLaborCausa) as t
				ORDER BY valor DESC , causa ASC";
		// D($sql);
				// WHERE valor = 1
		$response->data = $this->db->queryRow($sql);

		return $response->data;
	}

	/*----------  PANTALLA 2 : PROMEDIO POR MES GENERAL : GRAFICA DE BARRAS  ----------*/
	private function getPromMonthGeneralLote($month , $lote , $labor , $idfinca){
		$lote = $lote;
		$idfinca = $idfinca;
		$idLabor = $labor;
		$response = new stdClass;
		$response->data = 0;
		$sql = "SELECT
				CASE
					WHEN valor = 1 THEN  (((buenas / muestras) * 100))
					ELSE 0
				END AS promedioLote , 'lotes'
				FROM 
				(SELECT labor ,causa ,COUNT( causa) AS CausasIndividual,valor,
				(SELECT COUNT(*) AS total_muestras 
					FROM muestras 
				WHERE idFinca = '{$idfinca}' AND valor = 1 AND MONTH(fecha) = {$month}) AS buenas,
				(SELECT COUNT(distinct archivoJson_contador , archivoJson) AS total_causas 
					FROM muestras 
				WHERE idFinca = '{$idfinca}' AND MONTH(fecha) = {$month}) AS muestras
				FROM muestras 
				WHERE idFinca = '{$idfinca}' AND MONTH(fecha) = {$month}) as e";
				// D($sql);
		$response->data = $this->db->queryRow($sql);

		return $response->data;
	}
	/*----------  PANTALLA 2 : PROMEDIO POR MES GENERAL : GRAFICA DE BARRAS  ----------*/

	/*----------  PANTALLA 2 : PROMEDIO POR MES POR LABOR : GRAFICA DE BARRAS  ----------*/
	private function getPromMonthLote($month , $lote , $labor , $idfinca){
		$lote = $lote;
		$idfinca = $idfinca;
		$idLabor = $labor;
		$response = new stdClass;
		$response->data = 0;
		$sql = "SELECT IFNULL(promedioLote, 0) AS promedioLote , IFNULL(lote , (SELECT nombre FROM lotes WHERE id = '{$idLabor}')) AS lote
				FROM
				(SELECT valor , buenas , muestras ,
				CASE
					WHEN valor = 1 THEN  AVG(((buenas / muestras) * 100))
					ELSE NULL
				END AS promedioLote,
				CASE 
					WHEN lote = '' THEN NULL
					WHEN lote = NULL THEN NULL
					ELSE lote
				END AS lote
				FROM 
				(SELECT lote ,causa ,COUNT( causa) AS CausasIndividual,valor,
				(SELECT COUNT(*) AS total_muestras 
					FROM muestras 
				WHERE idFinca = '{$idfinca}' AND idLote ='{$lote}' AND valor = 1 AND MONTH(fecha) = {$month}) AS buenas,
				(SELECT COUNT(distinct archivoJson_contador , archivoJson) AS total_causas 
					FROM muestras 
				WHERE idFinca = '{$idfinca}' AND idLote ='{$lote}' AND MONTH(fecha) = {$month}) AS muestras
				FROM muestras 
				WHERE idFinca = '{$idfinca}' AND idLote ='{$lote}' AND MONTH(fecha) = {$month}) as t) AS LaboresPromedio";
				// D($sql);
		$response->data = $this->db->queryRow($sql);

		return $response->data;
	}
	/*=====  FIN DE LA PANTALLA NUMERO 2  ======*/

	/*=========================================
	=            PANTALLA NUMERO 1            =
	=========================================*/
	
	public function getFincaLote(){
		$postdata = (object)json_decode(file_get_contents("php://input"));
		$idfinca = getValueFrom($postdata,'idFinca','',FILTER_SANITIZE_PHRAPI_INT);
		$lote = getValueFrom($postdata,'idLote','',FILTER_SANITIZE_PHRAPI_INT);
		$idLabor = getValueFrom($postdata,'idLabor','',FILTER_SANITIZE_PHRAPI_INT);
		$fecha_fin = getValueFrom($postdata,'fecha_final','',FILTER_SANITIZE_STRING);
		$fecha_inicio = getValueFrom($postdata,'fecha_inicial','',FILTER_SANITIZE_STRING);

		$months = ["Ene" , "Feb" , "Mar" , "Abr" , "May" , "Jun" , "Jul" ,"Ago", "Sep", "Oct" , "Nov", "Dic"];


		$sql = "SELECT 
			MONTH(fecha) AS Mes,
			idLote, 
			lote , 
			idLabor , 
			labor,
			IF(MONTH(CURRENT_DATE) = MONTH(fecha) , -1 , MONTH(fecha)) AS MesActual 
			FROM muestras 
			WHERE YEAR(fecha) = YEAR(CURRENT_DATE)  
			AND fecha BETWEEN '$fecha_inicio' AND '$fecha_fin'
			GROUP BY idLote ,idLabor 
			ORDER BY MONTH(fecha) , lote , labor";
		$LostesExist = $this->db->queryAll($sql);
		$response = new stdClass; 
		$response->labores_mes = $months;
		$response->causas = [];
		$response->data_labores = [];
		$response->data_labores_lotes = [];
		$response->labores = [];
		$lote = "";
		$labor = "";
		$promedio = 0;

		foreach ($LostesExist as $key => $value) {
			$value = (object)$value;
			if($value->MesActual == -1){
				$promedio = $this->getAvgLoteLabor($value->idLabor ,$value->idLote , $fecha_inicio ,$fecha_fin ,$idfinca )->porcentaje;
				$response->causas["lote"][$value->lote]["idLote"] = $value->idLote;
				$response->causas["lote"][$value->lote]["idLabor"] = $value->idLote;
				$response->causas["lote"][$value->lote]["labores"][$value->labor]["idLote"] = $value->idLabor;
				$response->causas["lote"][$value->lote]["labores"][$value->labor]["idLabor"] = $value->idLabor;
				$response->causas["lote"][$value->lote]["labores"][$value->labor]["promedio"] =  ($promedio > 0) ? $promedio : 0;
				if($lote != $value->idLote){
					$lote = $value->idLote;
					$response->data_labores_lotes[$value->lote] = [
						"lote" => $value->lote ,
						"idLote" => $value->idLote ,
						"idfinca" => $idfinca ,
						"promedio" => $this->getAvgLote($value->idLote , $fecha_inicio ,$fecha_fin ,$idfinca )->porcentaje
					];
				}

				$response->data_labores_lotes[$value->lote]["labores"][$value->labor] = $this->getAvgLoteLabor($value->idLabor ,$value->idLote , $fecha_inicio ,$fecha_fin ,$idfinca )->porcentaje;
				if($labor != $value->idLabor){
					$labor = $value->idLabor;
					$response->labores[] = $value->labor;
					$response->data_labores[$value->idLabor]["avg"]++;
					$response->data_labores[$value->idLabor]["labor"] = $value->labor;
					$response->data_labores[$value->idLabor]["idLabor"] = $value->idLabor;
					$response->data_labores[$value->idLabor]["idfinca"] = $idfinca;
					$response->data_labores[$value->idLabor]["promedio"] += $this->getAvgLoteLabor($value->idLabor ,$value->idLote , $fecha_inicio ,$fecha_fin ,$idfinca )->porcentaje;
				}
			}
		}

		$response->principal = [];
		$labor = ""; 
		$labores = "";
		foreach ($months as $key => $value) {
			$labor = $this->getPromMonthLote(($key+1),$lote , $idLabor  , $idfinca);
			$labores = $this->getPromMonthGeneralLote(($key+1) ,$lote , $idLabor , $idfinca );
			// D($labor);
			// D($labores);
			$response->principal[] = [ 
				$value , 
				strtoupper($labor->lote) => $labor->promedioLote,
				strtoupper($labores->lotes) => $labores->promedioLote,
			];
		}

		$months_des = ["Ene" , "Feb"  , "Mar" , "Abr" , "May" , "Jun" , "Jul" ,"Ago", "Sep" , "Oct" , "Nov" , "Dic"];
		$months = ["Ene"  => [], "Feb"  => [] 
		, "Mar"  => [], "Abr"  => [], "May"  => [], "Jun"  => [], "Jul"  => [],"Ago" => [], "Sep"  => [], 
		"Oct"  => [], "Nov"  => [], "Dic" => []];

		$sql_detalle = "SELECT 
			MONTH(fecha) AS Mes,
			idLote, 
			lote , 
			idLabor , 
			labor,
			IF(MONTH(CURRENT_DATE) = MONTH(fecha) , -1 , MONTH(fecha)) AS MesActual 
			FROM muestras 
			WHERE YEAR(fecha) = YEAR(CURRENT_DATE)  
			AND fecha 
			GROUP BY idLote ,idLabor 
			ORDER BY MONTH(fecha) , lote , labor";
		$LaboresExistMonth = $this->db->queryAll($sql_detalle);
		$response->main = [];
		$Lpromedio = [];
		$response->main = $months;
		// $response->lote_name = "";
		foreach ($LaboresExistMonth as $key => $value) {
			$value = (object)$value;
			$Lpromedio = $this->getPromMonthLoteAvg($value->Mes,$value->idLote , $value->idLabor  , $idfinca);
			// if((int)$idLote == (int)$value->idLote){
			// 	$response->lote_name = $value->lote;
			// 	$response->main[$months_des[$value->Mes]]["lote"]["name"] = $value->lote;
			// 	$response->main[$months_des[$value->Mes]]["lote"]["avg"]++;
			// 	$response->main[$months_des[$value->Mes]]["lote"]["sum"] += $Lpromedio->porcentaje;
			// }
			$response->main[$months_des[$value->Mes]]["lotes"][$value->lote]["avg"]++;
			$response->main[$months_des[$value->Mes]]["lotes"][$value->lote]["sum"] += $Lpromedio->porcentaje;
		}


		return $response;
	}

	private function getAvgLote($idLabor , $idLote , $fecha_inicio , $fecha_fin , $idFinca){
		$response = new stdClass;
		$sql = "SELECT causa , CausasIndividual , buenas , malas ,muestras,valor,
				CASE
					WHEN valor = 1 THEN  ((buenas / muestras) * 100) 
					ELSE ((((1-(buenas / muestras))*CausasIndividual) / malas)*100)
				END AS porcentaje,
				CASE
					WHEN valor = 1 THEN  NULL
					ELSE (CausasIndividual / malas) * 100
				END AS porcentaje2
				FROM 
				(SELECT laborCausaDesc AS causa ,COUNT( causa) AS CausasIndividual,valor,
				(SELECT COUNT(*) AS total_muestras FROM muestras WHERE idFinca = '{$idFinca}' AND idLote ='{$idLote}' AND valor = 1 AND idLabor = '{$idLabor}' AND fecha BETWEEN '{$fecha_inicio}' AND '{$fecha_fin}') AS buenas,
				(SELECT COUNT(*) AS total_muestras FROM muestras WHERE idFinca = '{$idFinca}' AND idLote ='{$idLote}' AND valor = 0 AND idLabor = '{$idLabor}' AND fecha BETWEEN '{$fecha_inicio}' AND '{$fecha_fin}')  AS malas,
				(SELECT COUNT(distinct archivoJson_contador , archivoJson) AS total_causas FROM muestras WHERE 
						fecha BETWEEN '{$fecha_inicio}' AND '{$fecha_fin}' AND
						idFinca = '{$idFinca}' AND idLote ='{$idLote}' AND idLabor = '{$idLabor}') AS muestras
				FROM muestras 
				WHERE 
					fecha BETWEEN '{$fecha_inicio}' AND '{$fecha_fin}' AND
					idFinca = '{$idFinca}' AND idLote ='{$idLote}' AND idLabor = '{$idLabor}'
				GROUP BY idLaborCausa) as t
				ORDER BY valor DESC , causa ASC";
		// D($sql);
		$response->data = $this->db->queryRow($sql);

		return $response->data;
	}

	private function getAvgLoteLabor($idLabor , $idLote , $fecha_inicio , $fecha_fin , $idFinca){
		$response = new stdClass;
		$sql = "SELECT causa , CausasIndividual , buenas , malas ,muestras,valor,
				CASE
					WHEN valor = 1 THEN  ((buenas / muestras) * 100) 
					ELSE ((((1-(buenas / muestras))*CausasIndividual) / malas)*100)
				END AS porcentaje,
				CASE
					WHEN valor = 1 THEN  NULL
					ELSE (CausasIndividual / malas) * 100
				END AS porcentaje2
				FROM 
				(SELECT laborCausaDesc AS causa ,COUNT( causa) AS CausasIndividual,valor,
				(SELECT COUNT(*) AS total_muestras FROM muestras WHERE idFinca = '{$idFinca}' AND idLote ='{$idLote}' AND valor = 1 AND idLabor = '{$idLabor}' AND fecha BETWEEN '{$fecha_inicio}' AND '{$fecha_fin}') AS buenas,
				(SELECT COUNT(*) AS total_muestras FROM muestras WHERE idFinca = '{$idFinca}' AND idLote ='{$idLote}' AND valor = 0 AND idLabor = '{$idLabor}' AND fecha BETWEEN '{$fecha_inicio}' AND '{$fecha_fin}')  AS malas,
				(SELECT COUNT(distinct archivoJson_contador , archivoJson) AS total_causas FROM muestras WHERE 
						fecha BETWEEN '{$fecha_inicio}' AND '{$fecha_fin}' AND
						idFinca = '{$idFinca}' AND idLote ='{$idLote}' AND idLabor = '{$idLabor}') AS muestras
				FROM muestras 
				WHERE 
					fecha BETWEEN '{$fecha_inicio}' AND '{$fecha_fin}' AND
					idFinca = '{$idFinca}' AND idLote ='{$idLote}' AND idLabor = '{$idLabor}'
				GROUP BY idLaborCausa) as t
				ORDER BY valor DESC , causa ASC";
		// D($sql);
		$response->data = $this->db->queryRow($sql);

		return $response->data;
	}			
	
	/*=====  FIN DE LA PANTALLA NUMERO 1  ======*/
	
	
}