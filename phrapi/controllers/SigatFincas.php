<?php defined('PHRAPI') or die("Direct access not allowed!");

class SigatFincas {

    public function __construct(){
        $this->config = $GLOBALS['config'];
        $this->session = Session::getInstance();
        $this->db = DB::getInstance($this->session->db);
    }

    public function index(){
        $response = new stdClass;
        $response->table = array();

        $table = $this->db->queryAll("SELECT id, codigo, finca, fumigacion, produccion, neta FROM cat_fincas");
        foreach($table as $row){
            $response->table[] = [$row->id, $row->codigo, $row->finca, $row->fumigacion, $row->produccion, $row->neta];
        }
        return $response;
    }
}