<?php defined('PHRAPI') or die("Direct access not allowed!");

class Tutoriales {
    private $db;
    private $config;
    private $token;

    public function __construct(){
        $this->config = $GLOBALS['config'];
        $this->session = Session::getInstance();
        $this->db = DB::getInstance();
    }

    public function index(){
        $response = new stdClass;
        $response->data = $this->db->queryAll("SELECT title, description, code FROM tutoriales WHERE status = 'Activo'");
        return $response;
    }
}