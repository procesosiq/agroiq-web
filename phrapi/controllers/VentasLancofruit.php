<?php defined('PHRAPI') or die("Direct access not allowed!");

class VentasLancofruit {
    private $db;
    private $config;
    private $token;
    private $Service;

    public function __construct(){
        $this->config = $GLOBALS['config'];
        $this->session = Session::getInstance();
        $this->db = DB::getInstance($this->session->agent_user);        
    }

    private function getFilters(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $filters = (object)[
            "semana" => getValueFrom($postdata , "semana", "", FILTER_SANITIZE_STRING),
        ];
        return $filters;
    }

    public function index(){
        $response = new stdClass;
        $filters = $this->getFilters();

        $response->semanas = $this->db->queryAllSpecial("SELECT semana AS id, semana AS label FROM lancofruit_ventas WHERE total > 0 GROUP BY semana");

        $keys = array_keys($response->semanas);
        $ultima = $response->semanas[$keys[count($keys)-1]];
        if($filters->semana != $ultima){
            $keys = array_keys($response->semanas);
            $response->ultimaSemana = $ultima;
        }
        return $response;
    }

    
    public function ventasDia(){
        $response = new stdClass;

        $filters = $this->getFilters();
        if($filters->year > 0){
            $sWhere .= " AND anio = {$filters->year}";
        }
        if($filters->semana > 0){
            $sWhere .= " AND semana = {$filters->semana}";
        }

        /*TABLE*/
        $sql = "SELECT CASE DAYOFWEEK(fecha)
                            WHEN 1 THEN 'Dom'
                            WHEN 2 THEN 'Lun'
                            WHEN 3 THEN 'Mar'
                            WHEN 4 THEN 'Mie'
                            WHEN 5 THEN 'Jue'
                            WHEN 6 THEN 'Vie'
                            WHEN 7 THEN 'Sab'
                        END AS dia, DAY(fecha) AS num_dia,
                        pto_emi,
                        SUM(total) AS total,
                        fecha
                FROM lancofruit_ventas 
                WHERE total > 0 $sWhere
                GROUP BY DAY(fecha)";
        $response->data = $this->db->queryAll($sql);
        
        foreach($response->data as $row){            
            $row->rutas = $this->db->queryAllSpecial("SELECT id_ruta AS id, SUM(total) AS label FROM lancofruit_ventas WHERE fecha = '{$row->fecha}' GROUP BY id_ruta");
        }

        $response->rutas = $this->db->queryAll("SELECT id_ruta AS id, id_ruta AS label FROM lancofruit_ventas WHERE fecha = '{$row->fecha}' AND total > 0 GROUP BY id_ruta");
        
        foreach($response->rutas as $ruta){
            $response->totales->min[$ruta->label] = $this->db->queryRow("SELECT MIN(total) AS total FROM(SELECT SUM(total) AS total FROM lancofruit_ventas WHERE id_ruta = '{$ruta->label}' AND semana = '{$filters->semana}' AND anio = YEAR(CURRENT_DATE) AND total > 0 GROUP BY DAY(fecha)) AS tbl")->total;
            $response->totales->max[$ruta->label] = $this->db->queryRow("SELECT MAX(total) AS total FROM(SELECT SUM(total) AS total FROM lancofruit_ventas WHERE id_ruta = '{$ruta->label}' AND semana = '{$filters->semana}' AND anio = YEAR(CURRENT_DATE) AND total > 0 GROUP BY DAY(fecha)) AS tbl")->total;
            $response->totales->prom[$ruta->label] = $this->db->queryRow("SELECT AVG(total) AS total FROM(SELECT SUM(total) AS total FROM lancofruit_ventas WHERE id_ruta = '{$ruta->label}' AND semana = '{$filters->semana}' AND anio = YEAR(CURRENT_DATE) AND total > 0 GROUP BY DAY(fecha)) AS tbl")->total;
            $response->totales->total[$ruta->label] = $this->db->queryRow("SELECT SUM(total) AS total FROM(SELECT SUM(total) AS total FROM lancofruit_ventas WHERE id_ruta = '{$ruta->label}' AND semana = '{$filters->semana}' AND anio = YEAR(CURRENT_DATE) AND total > 0 GROUP BY DAY(fecha)) AS tbl")->total;
        }
        /*TABLE*/

        /*LINEAR CHART*/
        $data_chart = [];
        $rutas = [];
        foreach($response->data as $row){
            foreach($row->rutas as $ruta => $value){
                if(!in_array($ruta, $rutas)) $rutas[] = $ruta;
                $data_chart[] = [
                    "value" => $value, 
                    "legend" => "{$ruta}", 
                    "selected" => 1, 
                    "label" => $row->dia, 
                    "position" => array_search($ruta, $rutas)
                ];
            }
        }
        $response->line_chart = $this->chartInit($data_chart, "vertical" , "" , "line");
        /*LINEAR CHART*/

        /*PIE CHART*/
        $rutas = [];
        $data_chart = [];
        foreach($response->data as $row){
            foreach($row->rutas as $ruta => $value){
                $rutas[$ruta] += $value;
            }
        }
        foreach($rutas as $key => $value){
            $data_chart[] = ["value" => $value, "label" => "$key"];
        }

        $response->pie_chart = $this->pie($data_chart, "50%", "");
        /*PIE CHART*/

        return $response;
    }

    public function ventasSemana(){
        $response = new stdClass;

        $filters = $this->getFilters();
        if($filters->year > 0){
            $sWhere .= " AND anio = {$filters->year}";
        }

        /*TABLE*/
        $sql = "SELECT 
                        semana, 
                        SUM(total) AS total
                FROM lancofruit_ventas 
                WHERE total > 0 $sWhere
                GROUP BY semana";
        $response->data = $this->db->queryAll($sql);
        foreach($response->data as $row){            
            $sql = "SELECT IF(pto_emi = '002', 'RUTA 1', 'RUTA 2') AS id, SUM(total) AS label FROM lancofruit_ventas WHERE semana = '{$row->semana}' $sWhere GROUP BY pto_emi";
            $row->rutas = $this->db->queryAllSpecial($sql);
        }
        $sql = "SELECT IF(pto_emi = '002', 'RUTA 1', 'RUTA 2') AS id, pto_emi AS label FROM lancofruit_ventas WHERE semana = '{$row->semana}' AND total > 0 GROUP BY pto_emi";
        $response->rutas = $this->db->queryAll($sql);
        
        foreach($response->rutas as $ruta){
            $response->totales->min[$ruta->label] = $this->db->queryRow("SELECT MIN(total) AS total FROM(SELECT SUM(total) AS total FROM lancofruit_ventas WHERE pto_emi = '{$ruta->label}' AND total > 0 $sWhere GROUP BY semana) AS tbl")->total;
            $response->totales->max[$ruta->label] = $this->db->queryRow("SELECT MAX(total) AS total FROM(SELECT SUM(total) AS total FROM lancofruit_ventas WHERE pto_emi = '{$ruta->label}' AND total > 0 $sWhere GROUP BY semana) AS tbl")->total;
            $response->totales->prom[$ruta->label] = $this->db->queryRow("SELECT AVG(total) AS total FROM(SELECT SUM(total) AS total FROM lancofruit_ventas WHERE pto_emi = '{$ruta->label}' AND total > 0 $sWhere GROUP BY semana) AS tbl")->total;
            $response->totales->total[$ruta->label] = $this->db->queryRow("SELECT SUM(total) AS total FROM(SELECT SUM(total) AS total FROM lancofruit_ventas WHERE pto_emi = '{$ruta->label}' AND total > 0 $sWhere GROUP BY semana) AS tbl")->total;
        }
        /*TABLE*/

        /*LINEAR CHART*/
        $data_chart = [];
        $rutas = [];
        foreach($response->data as $row){
            foreach($row->rutas as $ruta => $value){
                if(!in_array($ruta, $rutas))
                    $rutas[] = $ruta;
                $data_chart[] = [
                    "value" => $value, 
                    "legend" => "{$ruta}", 
                    "selected" => 1, 
                    "label" => $row->semana, 
                    "position" => array_search($ruta, $rutas)
                ];
            }
        }
        $response->line_chart = $this->chartInit($data_chart, "vertical" , "" , "line");
        /*LINEAR CHART*/

        /*PIE CHART*/
        $rutas = [];
        $data_chart = [];
        foreach($response->data as $row){
            foreach($row->rutas as $ruta => $value){
                $rutas[$ruta] += $value;
            }
        }
        foreach($rutas as $key => $value){
            $data_chart[] = ["value" => $value, "label" => "$key"];
        }

        $response->pie_chart = $this->pie($data_chart, "50%", "");
        /*PIE CHART*/

        return $response;
    }

    public function ventasMes(){
        $response = new stdClass;
        
        $filters = $this->getFilters();
        if($filters->year > 0){
            $sWhere .= " AND anio = {$filters->year}";
        }

        /*TABLE*/
        $sql = "SELECT mes, 
                        SUM(total) AS total,
                        CASE mes 
                            WHEN 1 THEN 'ENE'
                            WHEN 2 THEN 'FEB'
                            WHEN 3 THEN 'MAR'
                            WHEN 4 THEN 'ABR'
                            WHEN 5 THEN 'MAY'
                            WHEN 6 THEN 'JUN'
                            WHEN 7 THEN 'JUL'
                            WHEN 8 THEN 'AGO'
                            WHEN 9 THEN 'SEP'
                            WHEN 10 THEN 'OCT'
                            WHEN 11 THEN 'NOV'
                            WHEN 12 THEN 'DIC'
                        END AS name
                FROM lancofruit_ventas 
                WHERE total > 0 $sWhere
                GROUP BY mes";
        $response->data = $this->db->queryAll($sql);
        foreach($response->data as $row){            
            $row->rutas = $this->db->queryAllSpecial("SELECT id_ruta AS id, SUM(total) AS label FROM lancofruit_ventas WHERE semana = '{$row->semana}' AND anio = YEAR(CURRENT_DATE) GROUP BY id_ruta");
        }
        
        $response->rutas = $this->db->queryAll("SELECT id_ruta AS id, id_ruta AS label FROM lancofruit_ventas WHERE semana = '{$row->semana}' AND total > 0 GROUP BY id_ruta");
        
        foreach($response->rutas as $ruta){
            $response->totales->min[$ruta->label] = $this->db->queryRow("SELECT MIN(total) AS total FROM(SELECT SUM(total) AS total FROM lancofruit_ventas WHERE id_ruta = '{$ruta->label}' AND anio = YEAR(CURRENT_DATE) AND total > 0 GROUP BY semana) AS tbl")->total;
            $response->totales->max[$ruta->label] = $this->db->queryRow("SELECT MAX(total) AS total FROM(SELECT SUM(total) AS total FROM lancofruit_ventas WHERE id_ruta = '{$ruta->label}' AND anio = YEAR(CURRENT_DATE) AND total > 0 GROUP BY semana) AS tbl")->total;
            $response->totales->prom[$ruta->label] = $this->db->queryRow("SELECT AVG(total) AS total FROM(SELECT SUM(total) AS total FROM lancofruit_ventas WHERE id_ruta = '{$ruta->label}' AND anio = YEAR(CURRENT_DATE) AND total > 0 GROUP BY semana) AS tbl")->total;
            $response->totales->total[$ruta->label] = $this->db->queryRow("SELECT SUM(total) AS total FROM(SELECT SUM(total) AS total FROM lancofruit_ventas WHERE id_ruta = '{$ruta->label}' AND anio = YEAR(CURRENT_DATE) AND total > 0 GROUP BY semana) AS tbl")->total;
        }
        /*TABLE*/

        /*LINEAR CHART*/
        $data_chart = [];
        $rutas = [];
        foreach($response->data as $row){
            foreach($row->rutas as $ruta => $value){
                if(!in_array($ruta, $rutas))
                    $rutas[] = $ruta;
                $data_chart[] = [
                    "value" => $value, 
                    "legend" => "{$ruta}", 
                    "selected" => 1, 
                    "label" => $row->name, 
                    "position" => array_search($ruta, $rutas)
                ];
            }
        }
        $response->line_chart = $this->chartInit($data_chart, "vertical" , "" , "line");
        /*LINEAR CHART*/

        /*PIE CHART*/
        $rutas = [];
        $data_chart = [];
        foreach($response->data as $row){
            foreach($row->rutas as $ruta => $value){
                $rutas[$ruta] += $value;
            }
        }
        foreach($rutas as $key => $value){
            $data_chart[] = ["value" => $value, "label" => "$key"];
        }

        $response->pie_chart = $this->pie($data_chart, "50%", "");
        /*PIE CHART*/

        return $response;
    }

    public function ventasAnio(){
        $response = new stdClass;
        
        $filters = $this->getFilters();
        if($filters->year > 0){
            $sWhere .= " AND anio = {$filters->year}";
        }

        /*TABLE*/
        $sql = "SELECT anio, SUM(total) AS total
                FROM lancofruit_ventas 
                WHERE total > 0 $sWhere
                GROUP BY anio";
        $response->data = $this->db->queryAll($sql);
        foreach($response->data as $row){            
            $row->rutas = $this->db->queryAllSpecial("SELECT id_ruta AS id, SUM(total) AS label FROM lancofruit_ventas WHERE anio = '{$row->anio}' AND anio = YEAR(CURRENT_DATE) GROUP BY id_ruta");
        }
        $response->rutas = $this->db->queryAll("SELECT id_ruta AS id, id_ruta AS label FROM lancofruit_ventas WHERE anio = '{$row->anio}' AND total > 0 GROUP BY id_ruta");
        foreach($response->rutas as $ruta){
            $response->totales->min[$ruta->label] = $this->db->queryRow("SELECT MIN(total) AS total FROM(SELECT SUM(total) AS total FROM lancofruit_ventas WHERE id_ruta = '{$ruta->label}' AND anio = YEAR(CURRENT_DATE) AND total > 0 GROUP BY anio) AS tbl")->total;
            $response->totales->max[$ruta->label] = $this->db->queryRow("SELECT MAX(total) AS total FROM(SELECT SUM(total) AS total FROM lancofruit_ventas WHERE id_ruta = '{$ruta->label}' AND anio = YEAR(CURRENT_DATE) AND total > 0 GROUP BY anio) AS tbl")->total;
            $response->totales->prom[$ruta->label] = $this->db->queryRow("SELECT AVG(total) AS total FROM(SELECT SUM(total) AS total FROM lancofruit_ventas WHERE id_ruta = '{$ruta->label}' AND anio = YEAR(CURRENT_DATE) AND total > 0 GROUP BY anio) AS tbl")->total;
            $response->totales->total[$ruta->label] = $this->db->queryRow("SELECT SUM(total) AS total FROM(SELECT SUM(total) AS total FROM lancofruit_ventas WHERE id_ruta = '{$ruta->label}' AND anio = YEAR(CURRENT_DATE) AND total > 0 GROUP BY anio) AS tbl")->total;
        }
        /*TABLE*/

        /*LINEAR CHART*/
        $data_chart = [];
        $rutas = [];
        foreach($response->data as $row){
            foreach($row->rutas as $ruta => $value){
                if(!in_array($ruta, $rutas)) $rutas[] = $ruta;
                $data_chart[] = [
                    "value" => $value, 
                    "legend" => "{$ruta}", 
                    "selected" => 1, 
                    "label" => $row->anio, 
                    "position" => array_search($ruta, $rutas)
                ];
            }
        }
        $response->line_chart = $this->chartInit($data_chart, "vertical" , "" , "line");
        /*LINEAR CHART*/

        /*PIE CHART*/
        $rutas = [];
        $data_chart = [];
        foreach($response->data as $row){
            foreach($row->rutas as $ruta => $value){
                $rutas[$ruta] += $value;
            }
        }
        foreach($rutas as $key => $value){
            $data_chart[] = ["value" => $value, "label" => "$key"];
        }

        $response->pie_chart = $this->pie($data_chart, "50%", "");
        /*PIE CHART*/

        return $response;
    }
    
    private function chartInit($data = [] , $mode = "vertical" , $name = "" , $type = "line" , $minMax = []){
		$response = new stdClass;
		$response->chart = [];
		function object_to_array($obj) {
		    if(is_object($obj)) $obj = (array) $obj;
		    if(is_array($obj)) {
		        $new = array();
		        foreach($obj as $key => $val) {
		            $new[$key] = object_to_array($val);
		        }
		    }
		    else $new = $obj;
		    return $new;       
		}

		if(count($data) > 0){
			$response->chart["title"]["show"] = true;
			$response->chart["title"]["text"] = $name;
			$response->chart["title"]["subtext"] = $this->session->sloganCompany;
			$response->chart["tooltip"]["trigger"] = "item";
			$response->chart["tooltip"]["axisPointer"]["type"] = "shadow";
			$response->chart["toolbox"]["show"] = true;
			$response->chart["toolbox"]["feature"]["mark"]["show"] = true;
			$response->chart["toolbox"]["feature"]["restore"]["show"] = true;
			$response->chart["toolbox"]["feature"]["magicType"]["show"] = true;
			$response->chart["toolbox"]["feature"]["magicType"]["type"] = ['bar' , 'line'];
			$response->chart["toolbox"]["feature"]["saveAsImage"]["show"] = true;
			$response->chart["legend"]["data"] = [];
			$response->chart["legend"]["left"] = "center";
			$response->chart["legend"]["bottom"] = "0%";
			$response->chart["grid"]["left"] = "3%";
			$response->chart["grid"]["right"] = "4%";
			$response->chart["grid"]["bottom"] = "20%";
			$response->chart["grid"]["containLabel"] = true;
			$response->chart["plotOptions"]["series"]["connectNulls"] = true;
			if($mode == "vertical"){
				$response->chart["xAxis"] = ["type" => "category" , "data" => [""]];
				$response->chart["yAxis"] = ["type" => "value"];
				if(isset($minMax["min"])){
					$response->chart["yAxis"] = ["type" => "value" , "min" => $minMax["min"], "max" => $minMax["max"]];
				}
			}else if($mode == "horizontal"){
				$response->chart["yAxis"] = ["type" => "value" , "data" => [""]];
				$response->chart["xAxis"] = ["type" => "value" , "boundaryGap" => true , "axisLine" => ["onZero" => true]];
			}
			$response->chart["series"] = [];
			$count = 0;
			$positionxAxis = -1;
			$position = -1;
			$colors = [];
			if($type == "line"){
                $response->chart["yAxis"]["min"] = 'dataMin';
				$response->chart["xAxis"]["data"] = [];
			}
			foreach ($data as $key => $value) {
				$value = (object)$value;
				$value->legend = ucwords(strtolower($value->legend));
				$value->label = ucwords(strtolower($value->label));
				if(isset($value->position)){
					$position = $value->position;
				}
				if(isset($value->positionxAxis)){
					$positionxAxis = $value->positionxAxis;
				}
				if(isset($value->color)){
					$colors = [
						"normal" => ["color" => $value->color],
					];
				}

				if($type == "line"){
					if(!in_array($value->label, $response->chart["xAxis"]["data"])){
						if(is_numeric($value->label)){
							$value->label = (double)$value->label;
						}
						if(!isset($value->positionxAxis)){
							$response->chart["xAxis"]["data"][] = $value->label;
						}else{
							$value->label = array($value->label);
							if(count($response->chart["xAxis"]["data"]) <= 0){
								while (count($response->chart["xAxis"]["data"]) <= $value->positionxAxis) {
									$response->chart["xAxis"]["data"][] = NULL;
								}
							}
							array_splice($response->chart["xAxis"]["data"] , $value->positionxAxis , 0 , $value->label);
							$response->chart["xAxis"]["data"] = array_filter($response->chart["xAxis"]["data"]);
						}
					}
					if(!in_array($value->legend, $response->chart["legend"]["data"])){
						if(!isset($value->position)){
							$position++;
						}
						$response->chart["legend"]["data"][] = $value->legend;
						$response->chart["legend"]["selected"][$value->legend] = ($value->selected == 0) ? false : true;
						$response->chart["series"][$position] = [
							"name" => $value->legend,
							"type" => $type,
							"connectNulls" => true,
							"data" => [],
							"label" => [
								"normal" => ["show" => false , "position" => "top"],
								"emphasis" => ["show" => false , "position" => "top"],
							],
							"itemStyle" => $colors
						];
                        if(isset($value->z)){
                            $response->chart["series"][$position]["z"] = $value->z;
                        }
					}
                    
					if(is_numeric($value->value)){
						$response->chart["series"][$position]["data"][] = ROUND($value->value,2);
					}else{
						$response->chart["series"][$position]["data"][] = $value->value;
					}

				}else{
					$response->chart["legend"]["data"][] = $value->label;
					// if($count == 0){
						$response->chart["series"][$count] = [
							"name" => $value->label,
							"type" => $type,
							"data" => [(int)$value->value],
							"label" => [
								"normal" => ["show" => true , "position" => "top"],
								"emphasis" => ["show" => true , "position" => "top"],
							],
							"itemStyle" => $colors
						];
					// }
				}
				$count++;
			}
		}

		return $response->chart;
	}

    private function pie($data = [] , $radius = ['50%', '70%'] , $name = "CATEGORIAS" , $roseType = "" , $type = "normal" , $legend = true , $position = ['45%', '40%'] , $version = 3){
		$response = new stdClass;
		$response->pie = [];
		if(count($data) > 0){
			$response->pie["title"]["show"] = true;
			$response->pie["title"]["text"] = $name;
			$response->pie["title"]["left"] = "center";
			// $response->pie["title"]["left"] = "right";
			$response->pie["title"]["subtext"] = $this->session->sloganCompany;
			$response->pie["tooltip"]["trigger"] = "item";
			$response->pie["tooltip"]["formatter"] = "{a} <br/>{b}: {c} ({d}%)";
			$response->pie["toolbox"]["show"] = true;
			$response->pie["toolbox"]["feature"]["mark"]["show"] = true;
			$response->pie["toolbox"]["feature"]["restore"]["show"] = true;
			$response->pie["toolbox"]["feature"]["magicType"]["show"] = false;
			$response->pie["toolbox"]["feature"]["magicType"]["type"] = ['pie'];
			$response->pie["toolbox"]["feature"]["saveAsImage"]["show"] = true;
			$response->pie["toolbox"]["feature"]["saveAsImage"]["name"] = $name;
			$response->pie["legend"]["show"] = $legend;
			$response->pie["legend"]["x"] = "center";
			$response->pie["legend"]["y"] = "bottom";
			$response->pie["calculable"] = true;
			$response->pie["legend"]["data"] = [];
			$response->pie["series"]["name"] = $name;
			$response->pie["series"]["type"] = "pie";
			$response->pie["series"]["center"] = $position;
			$response->pie["series"]["radius"] = $radius;
			if($version === 3){
				$response->pie["series"]["selectedMode"] = true;
				$response->pie["series"]["label"]["normal"]["show"] = true;
				$response->pie["series"]["label"]["emphasis"]["show"] = true;
				if($type != "normal"){
					$response->pie["series"]["roseType"] = $roseType;
					$response->pie["series"]["avoidLabelOverlap"] = "pie";
				}
				// $response->pie["series"]["label"]["normal"]["position"] = "center";
				// $response->pie["series"]["label"]["emphasis"]["textStyle"]["fontSize"] = "30";
				// $response->pie["series"]["label"]["emphasis"]["textStyle"]["fontWeight"] = "bold";
				$response->pie["series"]["labelLine"]["normal"]["show"] = true;
			}
			$response->pie["series"]["data"] = [];
			$colors = [];
			foreach ($data as $key => $value) {
				$value = (object)$value;
				$value->label = ucwords(strtolower($value->label));
				$response->pie["legend"]["data"][] = $value->label;
				if($version === 3){
					if(isset($value->color)){
							$colors = [
								"normal" => ["color" => $value->color],
							];
					}
					$response->pie["series"]["data"][] = ["name" => $value->label  , "value" => (float)$value->value , 
							"label" => [
								"normal" => ["show" => true , "position" => "outside" , "formatter" => "{b} \n {c} ({d}%)"],
								"emphasis" => ["show" => true , "position" => "outside"],
							],
							"itemStyle" => $colors
					];
				}else{
					// $response->pie["series"]["data"][] = ["name" => $value->label  , "value" => (float)$value->value];
					$response->pie["series"]["data"][] = ["name" => $value->label  , "value" => (float)$value->value,
						"itemStyle" => [
							"normal" => [ "label" => ["formatter" => "{b} \n {c} \n ({d}%)"] ],
							"emphasis" => [ "label" => ["formatter" => "{b} \n {c} \n ({d}%)"] ],
						]
					];
				}
			}
		}

		return $response->pie;
	}
}