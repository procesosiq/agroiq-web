<?php defined('PHRAPI') or die("Direct access not allowed!");

class AddData {
	public $name;
	private $db;
	private $config;

	public function __construct(){
		$this->config = $GLOBALS['config'];
		$this->session = Session::getInstance();
		// D($this->session);
        $this->maindb = DB::getInstance();
        $this->db = DB::getInstance($this->session->agent_user);
	}

    public function index(){
        $fallas_total = "
            (
                SCARING_SR + 
                POINT_SCAR + 
                BRUISE_BR + 
                FRICTION_F + 
                LATEX_STAI + 
                RESIDUE_RE + 
                NECK_INJUR + 
                DIRT + 
                POOR_OR_TR + 
                CHILLING_I + 
                OVERG_UN + 
                CROWN_ROT + 
                CROWN_MOD + 
                WHITEPOINT + 
                MOLD
            )";

        $sql = "SELECT 
            FEC_INS AS fecha , NOM_NAV AS barco , marca AS marca , id , 
            N_CLUSTER AS cluster , GROSSWEIGH AS peso , CAL_MIN AS calibre_minimo,
            CAL_MAX AS calibre_maximo,N_FINGER AS dedos,
            {$fallas_total}
            AS fallas_total,
            N_FINGER - {$fallas_total} AS dedos_buenos,
            ((N_FINGER - {$fallas_total}) / N_FINGER) * 100 AS calidad
            FROM calidad_destino
            WHERE
            ((({$fallas_total}) / N_FINGER) * 100) > -1";
        // $data = $this->db->queryAll($sql);
        // $id_quality = 0;
        // foreach ($data as $key => $value) {
        //     $sql = "INSERT INTO quality SET
        //             fecha = '{$value->fecha}',
        //             barco = '{$value->barco}',
        //             marca = '{$value->marca}',
        //             cliente = '{$value->marca}',
        //             muestra = '{$value->id}',
        //             cluster = '{$value->cluster}',
        //             peso = '{$value->peso}',
        //             calibre_minimo = '{$value->calibre_minimo}',
        //             calibre_maximo = '{$value->calibre_maximo}',
        //             n = '{$value->id}',
        //             dedos = '{$value->dedos}',
        //             fallas_total = '{$value->fallas_total}',
        //             dedos_buenos = '{$value->dedos_buenos}',
        //             calidad = '{$value->calidad}'";
        //     $this->db->query($sql);
        //     $id_quality = (double)$this->db->getLastID();

        //     if($id_quality > 0){
        //         $sql = "SELECT 
        //             id AS id_muestra,
        //             SCARING_SR AS sr , POINT_SCAR AS psr , BRUISE_BR AS br, 
        //             FRICTION_F AS fr , LATEX_STAI AS ls , RESIDUE_RE AS re, 
        //             NECK_INJUR AS ni, DIRT AS id , POOR_OR_TR AS rt , CHILLING_I AS ch , 
        //             OVERG_UN AS og , CROWN_ROT AS cr , CROWN_MOD AS cm , 
        //             WHITEPOINT AS wp  , MOLD AS m
        //             FROM calidad_destino
        //             WHERE
        //             id = {$value->id}";
        //         $fila = $this->db->queryRow($sql);
                
        //         // 30/06/2017 - TAG: LENGTH/GRAD
        //         $sql = "INSERT INTO quality_detalle_unit SET
        //                 id_quality = {$id_quality},
        //                 type = 'LENGTH/GRAD',
        //                 campo = 'og',
        //                 cantidad = '{$fila->og}'";
        //         $this->db->query($sql);
        //         // 30/06/2017 - TAG: LENGTH/GRAD
                
        //         // 30/06/2017 - TAG: SCARS/BRUISES
        //         $sql = "INSERT INTO quality_detalle_unit SET
        //                 id_quality = {$id_quality},
        //                 type = 'SCARS/BRUISES',
        //                 campo = 'sr',
        //                 cantidad = '{$fila->sr}'";
        //         $this->db->query($sql);
        //         $sql = "INSERT INTO quality_detalle_unit SET
        //                 id_quality = {$id_quality},
        //                 type = 'SCARS/BRUISES',
        //                 campo = 'psr',
        //                 cantidad = '{$fila->psr}'";
        //         $this->db->query($sql);
        //         $sql = "INSERT INTO quality_detalle_unit SET
        //                 id_quality = {$id_quality},
        //                 type = 'SCARS/BRUISES',
        //                 campo = 'br',
        //                 cantidad = '{$fila->br}'";
        //         $this->db->query($sql);
        //         // 30/06/2017 - TAG: SCARS/BRUISES

        //         // 30/06/2017 - TAG: OTHER MAJOR DEFECTS
        //         $sql = "INSERT INTO quality_detalle_unit SET
        //                 id_quality = {$id_quality},
        //                 type = 'OTHER MAJOR DEFECTS',
        //                 campo = 'fr',
        //                 cantidad = '{$fila->fr}'";
        //         $this->db->query($sql);
        //         $sql = "INSERT INTO quality_detalle_unit SET
        //                 id_quality = {$id_quality},
        //                 type = 'OTHER MAJOR DEFECTS',
        //                 campo = 'id',
        //                 cantidad = '{$fila->id}'";
        //         $this->db->query($sql);
        //         $sql = "INSERT INTO quality_detalle_unit SET
        //                 id_quality = {$id_quality},
        //                 type = 'OTHER MAJOR DEFECTS',
        //                 campo = 'rt',
        //                 cantidad = '{$fila->rt}'";
        //         $this->db->query($sql);
        //         // 30/06/2017 - TAG: OTHER MAJOR DEFECTS

        //         // 30/06/2017 - TAG: LATEX
        //         $sql = "INSERT INTO quality_detalle_unit SET
        //                 id_quality = {$id_quality},
        //                 type = 'LATEX',
        //                 campo = 'ls',
        //                 cantidad = '{$fila->ls}'";
        //         $this->db->query($sql);
        //         // 30/06/2017 - TAG: LATEX

        //         // 30/06/2017 - TAG: OTHER OBSERVATIONS
        //         $sql = "INSERT INTO quality_detalle_unit SET
        //                 id_quality = {$id_quality},
        //                 type = 'OTHER OBSERVATIONS',
        //                 campo = 're',
        //                 cantidad = '{$fila->re}'";
        //         $this->db->query($sql);
        //         $sql = "INSERT INTO quality_detalle_unit SET
        //                 id_quality = {$id_quality},
        //                 type = 'OTHER OBSERVATIONS',
        //                 campo = 'wp',
        //                 cantidad = '{$fila->wp}'";
        //         $this->db->query($sql);
        //         $sql = "INSERT INTO quality_detalle_unit SET
        //                 id_quality = {$id_quality},
        //                 type = 'OTHER OBSERVATIONS',
        //                 campo = 'm',
        //                 cantidad = '{$fila->m}'";
        //         $this->db->query($sql);
        //         // 30/06/2017 - TAG: OTHER OBSERVATIONS

        //          // 30/06/2017 - TAG: ROTS/MOLDS
        //         $sql = "INSERT INTO quality_detalle_unit SET
        //                 id_quality = {$id_quality},
        //                 type = 'ROTS/MOLDS',
        //                 campo = 'ni',
        //                 cantidad = '{$fila->ni}'";
        //         $this->db->query($sql);
        //         $sql = "INSERT INTO quality_detalle_unit SET
        //                 id_quality = {$id_quality},
        //                 type = 'ROTS/MOLDS',
        //                 campo = 'cr',
        //                 cantidad = '{$fila->cr}'";
        //         $this->db->query($sql);
        //         $sql = "INSERT INTO quality_detalle_unit SET
        //                 id_quality = {$id_quality},
        //                 type = 'ROTS/MOLDS',
        //                 campo = 'cm',
        //                 cantidad = '{$fila->cm}'";
        //         $this->db->query($sql);
        //         // 30/06/2017 - TAG: ROTS/MOLDS
        //     }
        // }
    }
}