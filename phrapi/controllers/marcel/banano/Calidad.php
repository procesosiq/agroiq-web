<?php defined('PHRAPI') or die("Direct access not allowed!");

class Calidad {
	public $name;
	private $db;
	private $config;

	public function __construct(){
		$this->config = $GLOBALS['config'];
		$this->session = Session::getInstance();
        $this->maindb = DB::getInstance();
        $this->db = DB::getInstance($this->session->agent_user);
    }
    
    public function calidadLast(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        if(isset($postdata->agent)){
            $this->db = DB::getInstance('marcel');
        }

		$cliente = "";
		$marca   = "";
		if(isset($postdata->cliente) && isset($postdata->marca)){
			$cliente = mb_strtoupper($postdata->cliente);
			$marca   = mb_strtoupper($postdata->marca);
        }

        if(!empty($cliente) && !empty($marca)){
			$sql_add = " AND cliente = '{$cliente}' AND marca = '{$marca}'";
		}
		if(!empty($contenedor)){
			$sql_add .= " AND contenedor = '{$contenedor}' ";
        }
        
        $response = new stdClass;
		$response->days = $this->db->queryAllOne("SELECT fecha FROM calidad WHERE 1=1 {$sql_add} GROUP BY fecha");
		if(!empty($marca)){
			if($marca == 'TIA' || $marca == 'MI COMISARIATO'){
				$response->verde = $this->db->queryAllOne("SELECT fecha FROM calidad WHERE tipo_muestreo = 'Proceso en Verde' {$sql_add} GROUP BY fecha");
				$response->maduracion = $this->db->queryAllOne("SELECT fecha FROM calidad WHERE tipo_muestreo = 'Camara de Maduración' {$sql_add} GROUP BY fecha");
            }
		}

		if(!empty($marca)){
            if($marca == 'TIA' || $marca == 'MI COMISARIATO'){
                $sql_add .= " AND tipo_muestreo = '{$postdata->proceso}'";
            }
		}
		$response->fecha = $this->db->queryOne("SELECT MAX(fecha) FROM calidad WHERE 1=1 {$sql_add}");

        return $response;
    }

	public function index(){
		$postdata = (object)json_decode(file_get_contents("php://input"));
		$response = new stdClass;
        if(isset($postdata->agent)){
            $this->db = DB::getInstance('marcel');
        }

		$cliente = "";
		$marca   = "";
		if(isset($postdata->cliente) && isset($postdata->marca)){
			$cliente = mb_strtoupper($postdata->cliente);
			$marca   = mb_strtoupper($postdata->marca);
        }
        if(!empty($marca)){
            if(in_array($marca, ["MI COMISARIATO", "TIA"])){
                $sWhere = " AND tipo_muestreo = '{$postdata->proceso}'";
            }
        }

		$response->contenedores = $this->db->queryAll("SELECT contenedor AS id, contenedor as label FROM calidad WHERE contenedor != '' GROUP BY contenedor");

		$sql = "SELECT (fecha) AS fecha , calidad 
                FROM calidad 
                WHERE marca = '{$marca}' $sWhere
		        GROUP BY fecha
		        ORDER BY  fecha";
		$response->data = $this->db->queryAll($sql);
		$response->danhos = new stdClass;
		$response->danhos->series = $this->getDanhos($postdata->fecha_inicial, $postdata->fecha_final, $cliente, $marca, $postdata->contenedor, $postdata->proceso);
		$response->danhos->seleccion = $this->getDetails("SELECCION", $postdata->fecha_inicial, $postdata->fecha_final, $cliente, $marca, $postdata->contenedor, $postdata->proceso);
		$response->danhos->empaque = $this->getDetails("EMPAQUE", $postdata->fecha_inicial, $postdata->fecha_final, $cliente, $marca, $postdata->contenedor, $postdata->proceso);
		$response->danhos->otros = $this->getDetails("OTROS", $postdata->fecha_inicial, $postdata->fecha_final, $cliente, $marca, $postdata->contenedor, $postdata->proceso);
		$response->id_company = $this->session->id_company;
		$response->umbrals = $this->session->umbrals;
		
		// manuel
		$response->tags = $this->tags($postdata->fecha_inicial, $postdata->fecha_final, $cliente, $marca, $postdata->contenedor, $postdata->proceso);
		$response->grafica_principal_calidad = $this->grafica_principal_calidad($postdata->fecha_inicial, $postdata->fecha_final, $cliente, $marca, $postdata->contenedor, $postdata->proceso);
		$response->grafica_principal_de = $this->grafica_principal_de($postdata->fecha_inicial, $postdata->fecha_final, $cliente, $marca, $postdata->contenedor, $postdata->proceso);
		$response->grafica_principal_peso = $this->grafica_principal_peso($postdata->fecha_inicial, $postdata->fecha_final, $cliente, $marca, $postdata->contenedor, $postdata->proceso);
		$response->grafica_principal_cluster = $this->grafica_principal_cluster($postdata->fecha_inicial, $postdata->fecha_final, $cliente, $marca, $postdata->contenedor, $postdata->proceso);
		$response->grafica_principal_peso_cluster = $this->grafica_principal_peso_cluster($postdata->fecha_inicial, $postdata->fecha_final, $cliente, $marca, $postdata->contenedor, $postdata->proceso);
		#victor
		$response->grafica_calidad_historico_marcas = $this->grafica_calidad_historico_marcas($postdata->fecha_inicial, $postdata->fecha_final, $cliente, $marca, $postdata->contenedor);
		
		$response->grafica_danos_total 	   = $this->grafica_danos_total($postdata->fecha_inicial, $postdata->fecha_final, $cliente, $marca, $postdata->contenedor);
		$response->grafica_danos_seleccion = $this->grafica_danos_seleccion($postdata->fecha_inicial, $postdata->fecha_final, $cliente, $marca, $postdata->contenedor);
		$response->grafica_danos_seleccion_campos = $this->grafica_danos_get_campos('SELECCION', $postdata->contenedor);
		$response->grafica_danos_empaque   = $this->grafica_danos_empaque($postdata->fecha_inicial, $postdata->fecha_final, $cliente, $marca, $postdata->contenedor);
		$response->grafica_danos_empaque_campos = $this->grafica_danos_get_campos('EMPAQUE', $postdata->fecha_inicial, $postdata->fecha_final, $cliente, $marca, $postdata->contenedor);
		$response->grafica_danos_otros     = $this->grafica_danos_otros($postdata->fecha_inicial, $postdata->fecha_final, $cliente, $marca, $postdata->contenedor);
		$response->grafica_danos_otros_campos = $this->grafica_danos_get_campos('OTROS', $postdata->fecha_inicial, $postdata->fecha_final, $cliente, $marca, $postdata->contenedor);

		$response->tabla_principal_calidad = $this->tabla_principal_calidad($postdata->fecha_inicial, $postdata->fecha_final, $cliente, $marca, $postdata->contenedor, $postdata->proceso);
		$response->tabla_principal_danos = $this->tabla_principal_danos($postdata->fecha_inicial, $postdata->fecha_final, $cliente, $marca, $postdata->contenedor, $postdata->proceso);
		$response->tabla_principal = $this->tabla_principal($postdata->fecha_inicial, $postdata->fecha_final, $cliente, $marca, $postdata->contenedor, $postdata->proceso);

		$response->type = $this->session->type_users;

		/*----------  BARCO  ----------*/
		$response->barco = $this->getBarco($postdata);
		/*----------  BARCO  ----------*/
		

		if(!empty($cliente) && !empty($marca)){
			$response->data_header = $this->data_header($postdata->fecha_inicial, $postdata->fecha_final, $cliente, $marca, $postdata->contenedor);
		}

		$response->table_por_cluster = [];
		if(!empty($cliente) && !empty($marca)){
			$response->table_por_cluster = $this->por_cluster($postdata->fecha_inicial, $postdata->fecha_final, $cliente, $marca, $postdata->contenedor);
			$response->por_cluster = $response->table_por_cluster->totales;
        }
        
        #$response->graficas_barras = $this->graficasBarras();

		return $response;
    }
    
    public function graficasBarras(){
        $response = new stdClass;
		$postdata = (object)json_decode(file_get_contents("php://input"));

        $postdata->unidad = 'lb';
        $id_cliente = $this->db->queryOne("SELECT id FROM clients WHERE nombre = '{$postdata->cliente}'");
        $nombre_marca = $this->db->queryOne("SELECT nombre_cajas FROM brands WHERE id_client = '$id_cliente' AND nombre = '{$postdata->marca}'");

		// HISTORICO BD
		$response->historico = $this->db->queryAll("SELECT *, lb AS peso FROM produccion_cajas INNER JOIN produccion_tipo_caja ON nombre = marca  WHERE fecha BETWEEN '{$postdata->fecha_inicial}' AND '{$postdata->fecha_final}' AND marca = '{$nombre_marca}'");

		// GRAFICAS
        $response->marcas = $this->db->queryAll("SELECT marca , sec, minimo, maximo
                                                FROM (
                                                    SELECT marca, 1 AS sec, minimo_{$postdata->unidad} AS minimo, maximo_{$postdata->unidad} AS maximo 
                                                    FROM produccion_cajas 
                                                    INNER JOIN produccion_tipo_caja ON nombre = marca 
                                                    WHERE fecha BETWEEN '{$postdata->fecha_inicial}' AND '{$postdata->fecha_final}' AND marca = '{$nombre_marca}'
                                                    GROUP BY marca
                                                    UNION ALL
                                                    SELECT marca, 2 AS sec, minimo_{$postdata->unidad} AS minimo, maximo_{$postdata->unidad} AS maximo 
                                                    FROM produccion_gavetas 
                                                    INNER JOIN produccion_tipo_caja ON nombre = marca 
                                                    WHERE fecha BETWEEN '{$postdata->fecha_inicial}' AND '{$postdata->fecha_final}' AND marca = '{$nombre_marca}'
                                                    GROUP BY marca
                                                ) AS tbl
                                                GROUP BY marca");

        $response->graficas = [];
        $response->pasteles = [];
        foreach($response->marcas as $marca){
            # BARRAS
            $response->graficas[$marca->marca] = (object)[
                "legends" => [],
                "series" => [
                    "Cantidad" => [
                        "name" => "Cantidad",
                        "connectNulls" => true,
                        "type" => "bar",
                        "itemStyle" => [
                            "normal" => [
                                "barBorderRadius" => 0,
                                "barBorderWidth" => 6
                            ]
                        ],
                        "label" => [
                            "normal" => [
                                "show" => true,
                                "position" => "inside"
                            ]
                        ],
                        "data" => [],
                    ]
                ]
            ];
            $pesos = $this->db->queryAllSpecial("SELECT peso AS id, cantidad AS label
                                                FROM(
                                                    SELECT {$postdata->unidad} AS peso, COUNT(1) AS cantidad FROM produccion_cajas WHERE fecha BETWEEN '{$postdata->fecha_inicial}' AND '{$postdata->fecha_final}' AND marca = '{$marca->marca}' AND marca != 'PINALINDA BAG' GROUP BY {$postdata->unidad}
                                                    UNION ALL
                                                    SELECT grupo AS peso, SUM(cantidad) AS cantidad
                                                    FROM (
                                                        SELECT {$postdata->unidad} AS peso, COUNT(1) AS cantidad, 
                                                            IF(FLOOR({$postdata->unidad})+0.5 <= {$postdata->unidad}, 
                                                                CONCAT(
                                                                    FLOOR({$postdata->unidad})+0.5,
                                                                    '-',
                                                                    FLOOR({$postdata->unidad})+1
                                                                ), 
                                                                CONCAT(
                                                                    FLOOR({$postdata->unidad}),
                                                                    '-',
                                                                    FLOOR({$postdata->unidad})+0.5
                                                                )
                                                            ) AS grupo
                                                        FROM produccion_cajas
                                                        WHERE fecha BETWEEN '{$postdata->fecha_inicial}' AND '{$postdata->fecha_final}' AND marca = '{$marca->marca}' AND marca = 'PINALINDA BAG'
                                                        GROUP BY {$postdata->unidad}
                                                    ) AS tbl
                                                    GROUP BY grupo
                                                    UNION ALL
                                                    SELECT grupo AS peso, SUM(cantidad) AS cantidad
                                                    FROM (
                                                        SELECT {$postdata->unidad} AS peso, COUNT(1) AS cantidad, 
                                                            IF(FLOOR({$postdata->unidad})+0.5 <= {$postdata->unidad}, 
                                                                CONCAT(
                                                                    FLOOR({$postdata->unidad})+0.5,
                                                                    '-',
                                                                    -- CAST(FLOOR(peso)+1 AS DECIMAL(5,1))
                                                                    FLOOR({$postdata->unidad})+1
                                                                ), 
                                                                CONCAT(
                                                                    -- CAST(FLOOR(peso) AS DECIMAL(5,1)),
                                                                    FLOOR({$postdata->unidad}),
                                                                    '-',
                                                                    FLOOR({$postdata->unidad})+0.5
                                                                )
                                                            ) AS grupo
                                                        FROM produccion_gavetas
                                                        WHERE fecha BETWEEN '{$postdata->fecha_inicial}' AND '{$postdata->fecha_final}' AND marca = '{$marca->marca}'
                                                        GROUP BY {$postdata->unidad}
                                                    ) AS tbl
                                                    GROUP BY grupo
                                                ) AS tbl");
            
            foreach($pesos as $peso => $cantidad){
                $response->graficas[$marca->marca]->legends[] = $peso;
                $response->graficas[$marca->marca]->series["Cantidad"]["umbral"] = ["max" => $marca->maximo, "min" => $marca->minimo];
                $response->graficas[$marca->marca]->series["Cantidad"]["data"][] = $cantidad;
            }
            
            # PASTELES
            $response->pasteles[$marca->marca] = [];
            $cajas1840 = $this->db->queryAllSpecial("SELECT nombre AS id, nombre AS label FROM produccion_tipo_caja WHERE maximo IS NOT NULL AND minimo IS NOT NULL");
            $table = $this->db->queryOne("SELECT name_table FROM produccion_tipo_caja WHERE nombre = '{$marca->marca}'");
            #$fieldPeso = $table == 'produccion_cajas' ? 'caja' : 'peso';

            if(in_array($marca->marca, $cajas1840)){
                $sql = "SELECT value, label
                        FROM (
                            SELECT '{$marca->minimo}-{$marca->maximo}' AS label, COUNT(1) AS value
                            FROM {$table}
                            WHERE marca = '{$marca->marca}' AND fecha BETWEEN '{$postdata->fecha_inicial}' AND '{$postdata->fecha_final}' AND {$postdata->unidad} >= {$marca->minimo} AND {$postdata->unidad} <= {$marca->maximo}
                            UNION ALL
                            SELECT '> {$marca->maximo}' AS label, COUNT(1) AS value
                            FROM {$table}
                            WHERE marca = '{$marca->marca}' AND fecha BETWEEN '{$postdata->fecha_inicial}' AND '{$postdata->fecha_final}' AND {$postdata->unidad} > {$marca->maximo}
                            UNION ALL
                            SELECT '< {$marca->minimo}' AS label, COUNT(1) AS value
                            FROM {$table}
                            WHERE marca = '{$marca->marca}' AND fecha BETWEEN '{$postdata->fecha_inicial}' AND '{$postdata->fecha_final}' AND {$postdata->unidad} < {$marca->minimo}
                        ) AS tbl
                        WHERE value > 0";
                $response->pasteles[$marca->marca] = $this->db->queryAll($sql);
            }else{
                $peso_prom = $this->db->queryOne("SELECT ROUND(AVG(peso), 2) FROM (
                    SELECT AVG(caja) AS peso FROM produccion_cajas WHERE marca = '{$marca->marca}' AND fecha BETWEEN '{$postdata->fecha_inicial}' AND '{$postdata->fecha_final}'
                    UNION ALL
                    SELECT AVG(peso) AS peso FROM produccion_gavetas WHERE marca = '{$marca->marca}' AND fecha BETWEEN '{$postdata->fecha_inicial}' AND '{$postdata->fecha_final}'
                ) AS tbl");
                $sql = "SELECT value, label
                        FROM (
                            SELECT '{$peso_prom}' AS label, COUNT(1) AS value
                            FROM $table
                            WHERE marca = '{$marca->marca}' AND fecha BETWEEN '{$postdata->fecha_inicial}' AND '{$postdata->fecha_final}' AND {$postdata->unidad} = {$peso_prom}
                            UNION ALL
                            SELECT '> {$peso_prom}' AS label, COUNT(1) AS value
                            FROM $table
                            WHERE marca = '{$marca->marca}' AND fecha BETWEEN '{$postdata->fecha_inicial}' AND '{$postdata->fecha_final}' AND {$postdata->unidad} > {$peso_prom}
                            UNION ALL
                            SELECT '< {$peso_prom}' AS label, COUNT(1) AS value
                            FROM $table
                            WHERE marca = '{$marca->marca}' AND fecha BETWEEN '{$postdata->fecha_inicial}' AND '{$postdata->fecha_final}' AND {$postdata->unidad} < {$peso_prom}
                        ) AS tbl";
                $response->pasteles[$marca->marca] = $this->db->queryAll($sql);
            }
        }
        return $response;
    }

	public function saveBarco(){
		$postdata = (object)json_decode(file_get_contents("php://input"));
		$filters = (object)[
			"fecha_inicial" => getValueFrom($postdata , "fecha_inicio",'', FILTER_SANITIZE_STRING),
			"fecha_final" => getValueFrom($postdata , "fecha_final",'', FILTER_SANITIZE_STRING),
			"barco" => getValueFrom($postdata , "barco",'', FILTER_SANITIZE_STRING),
		];

		$response = new stdClass;

		$response->data = [];
		$data = new stdClass;

		if($filters->fecha_final != "" && $filters->fecha_inicial != "" &&  $filters->barco != ""){
			$sql = "SELECT 
				DATEDIFF('{$filters->fecha_final}' , '{$filters->fecha_inicial}') AS dia , 
				getWeek('{$filters->fecha_final}') AS fecha_fin , 
				getWeek('{$filters->fecha_inicial}') AS fecha_inicial,
				(getWeek('{$filters->fecha_final}') -  getWeek('{$filters->fecha_inicial}')) AS dateDiff";
			// D($sql);
			$data = $this->db->queryRow($sql);
			// D($data);
			$data->dia = (int)$data->dia;
			$data->dateDiff = (int)$data->dateDiff;
			if($data->dia == 0){
				$sql = "UPDATE calidad SET barco = '{$filters->barco}' WHERE fecha = '{$filters->fecha_inicial}'";
			}elseif ($data->dia != 0 && $data->dateDiff == 0) {
				$sql = "UPDATE calidad SET barco = '{$filters->barco}' WHERE DATE(fecha) BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}'";
			}else{
				return "";
			}

			$this->db->query($sql);
		}

		return $this->getBarco($filters);
	}

	private function getBarco($filters = []){
		$response = new stdClass;
		$response->data = "";
		$data = new stdClass;

		if($filters->fecha_final != "" && $filters->fecha_inicial != ""){
			$sql = "SELECT 
				DATEDIFF('{$filters->fecha_final}' , '{$filters->fecha_inicial}') AS dia , 
				getWeek('{$filters->fecha_final}') AS fecha_fin , 
				getWeek('{$filters->fecha_inicial}') AS fecha_inicial,
				(getWeek('{$filters->fecha_final}') -  getWeek('{$filters->fecha_inicial}')) AS dateDiff";
			$data = $this->db->queryRow($sql);
			$data->dia = (int)$data->dia;
			$data->dateDiff = (int)$data->dateDiff;
			if($data->dia == 0){
				$sql = "SELECT barco FROM calidad WHERE barco != '' AND DATE(fecha) BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}' GROUP BY barco";
			}elseif ($data->dia != 0 && $data->dateDiff == 0) {
				$sql = "SELECT barco FROM calidad WHERE barco != '' AND DATE(fecha) BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}' GROUP BY barco";
			}else{
				return "";
			}
			$response->data = $this->db->queryOne($sql);
		}
		return $response->data;
	}

	private function por_cluster($inicio, $fin, $cliente, $marca, $contenedor = ""){

		$sWhere = "";
		if(!empty($contenedor)){
			$sWhere = " AND contenedor = '{$contenedor}' ";
		}
		
		$response = new stdClass;
		$response->data = [];
		$sql = "SELECT id_calidad , fecha , cliente , marca , 
				(calidad_gajos.gajo_count + 1) AS gajo_count , (calidad_gajos.gajo / 1000) as peso,
				(SELECT MAX(calidad_gajos.gajo) FROM calidad_gajos WHERE id_calidad = calidad.id) AS maxPEso,
				(SELECT MIN(calidad_gajos.gajo) FROM calidad_gajos WHERE id_calidad = calidad.id) AS minPEso
				FROM calidad
				INNER JOIN calidad_gajos ON calidad.id = calidad_gajos.id_calidad
				WHERE marca = '{$marca}' AND cliente = '{$cliente}' AND DATE(fecha) BETWEEN '$inicio' AND '$fin' $sWhere";
		$response->data = $this->db->queryAll($sql);
		$data = [];
		$calidad = [];
		$count = 0;
		$label = [];
		$dataGeneral = [];
		$prom = [];
		$totales = [];
		$tempTotal = [];
		foreach ($response->data as $key => $value) {
			if(!in_array($value->id_calidad, $calidad)){
				$calidad[] = $value->id_calidad;
				$count++;
				$label[] = "Hora ".$count;
				$totales["Max"]["Hora ".$count] = 0;
				$totales["Min"]["Hora ".$count] = 0;
				$totales["Promedio"]["Hora ".$count] = 0;
				$totales["Desviacion Estandar"]["Hora ".$count] = 0;
				$tempTotal["Varianza"]["Hora ".$count] = 0;
				$dataGeneral["Hora ".$count] = [];
			}
			$dataGeneral["Hora ".$count][] = round((float)$value->peso,2);
			$data[$value->gajo_count]["nombre"] = (int)$value->gajo_count;
			$data[$value->gajo_count]["Hora ".$count] = round($value->peso,2);
			$totales["Total"]["Hora ".$count] += round((float)$value->peso,2);
			$prom["Promedio"]["Hora ".$count]["count"]++;
		}

		$Varianza = 0;
		foreach ($label as $key => $value) {
			$key = ($key +1);
			$Varianza = 0;
			$totales["Max"]["Hora ".$key] = max($dataGeneral["Hora ".$key]);
			$totales["Min"]["Hora ".$key] = min($dataGeneral["Hora ".$key]);
			$totales["Promedio"]["Hora ".$key] = round($totales["Total"]["Hora ".$key] / $prom["Promedio"]["Hora ".$key]["count"] , 3);
			foreach ($dataGeneral["Hora ".$key] as $llave => $valor) {
				$Varianza += (($totales["Promedio"]["Hora ".$key] - $valor) * ($totales["Promedio"]["Hora ".$key] - $valor));
			}
			$tempTotal["Varianza"]["Hora ".$key] = (($Varianza / ($prom["Promedio"]["Hora ".$key]["count"] - 1)));
			$totales["Desviacion Estandar"]["Hora ".$key] = round(sqrt($tempTotal["Varianza"]["Hora ".$key]), 2);
		}
		$totales["categories"] = $label;
		$response->labels = new stdClass;
		$response->table = new stdClass;
		$response->totales = $totales;
		$response->labels = $label;
		$response->table = $data;
		return $response;
	}

	private function data_header($inicio, $fin, $cliente, $marca, $contenedor = ""){
		$sWhere = "";
		if(!empty($contenedor)){
			$sWhere = " AND contenedor = '{$contenedor}' ";
		}

		$sql = "SELECT DISTINCT(peso_referencial) as cantidad
				FROM calidad
				WHERE UPPER(cliente) = '$cliente' AND UPPER(marca) = '$marca' AND peso_referencial IS NOT NULL AND DATE(fecha) BETWEEN '$inicio' AND '$fin' $sWhere";
		// D($sql);
		$rp1 = $this->db->queryRow($sql);
		$sql = "SELECT DISTINCT(gajos_referencial) as cantidad
				FROM calidad
				WHERE UPPER(cliente) = '$cliente' AND UPPER(marca) = '$marca' AND gajos_referencial IS NOT NULL AND DATE(fecha) BETWEEN '$inicio' AND '$fin' $sWhere";
		$rp2 = $this->db->queryRow($sql);
		$sql = "SELECT * FROM brands WHERE nombre = '$marca'";
		$rp3 = $this->db->queryRow($sql);
		$logo = $rp3->logotipo;
		if(!file_exists($_SERVER['DOCUMENT_ROOT']."/".$logo)){
			$logo = "logos/marcas/no-available.png";
		}
		$sql = "SELECT marca FROM calidad WHERE cliente = '$cliente' AND DATE(fecha) BETWEEN '$inicio' AND '$fin' $sWhere GROUP BY marca";
		$res = $this->db->queryAll($sql);
		$marcas = [];
		foreach ($res as $key => $value) {
			$value = (object)$value;
			$marcas[] = $value;
		}
		return array(
			'peso' => round(($rp1->cantidad / 2.2),2),
			'cluster' => $rp2->cantidad,
			'logo' => $logo,
			'marcas' => $marcas,
		);
	}
	
	private function tabla_principal($inicio, $fin, $cliente, $marca, $contenedor = "", $proceso){
		$sql_add = "";
		if(!empty($cliente) && !empty($marca)){
			$sql_add = " AND cliente = '{$cliente}' AND marca = '{$marca}'";
		}
		if(!empty($contenedor)){
			$sql_add .= " AND contenedor = '{$contenedor}' ";
        }
        if(!empty($marca)){
            if($marca == 'TIA' || $marca == 'MI COMISARIATO'){
                $sql_add .= " AND tipo_muestreo = '{$proceso}'";
            }
        }

		$sql = "SELECT * , 
                    ROUND(IF(peso > 0, 
                                IF(fecha > '2017-07-12', peso, (peso / 2.2)),  
                                0
                            ), 
                        2) AS peso 
                FROM calidad 
                WHERE DATE(fecha) BETWEEN '{$inicio}' AND '{$fin}' {$sql_add}";
		$res = $this->db->queryAll($sql);
		$data = [];
		$data2 = [];
		foreach ($res as $key => $value) {
			$data2 = [];
			$value = (object)$value;
				
			$sql2 = "SELECT campo, type, (cantidad) AS cantidad FROM calidad_detalle_unit WHERE 
			id_calidad = $value->id AND type='SELECCION' GROUP BY campo";
			$res2 = $this->db->queryAll($sql2);
			foreach ($res2 as $key2 => $value2) {
				$value2 = (object)$value2;
				$data2[] = $value2;
			}
			$value->seleccion = $data2;
			$data2 = [];
			$sql2 = "SELECT campo, type, (cantidad) AS cantidad FROM calidad_detalle_unit WHERE 
			id_calidad = $value->id AND type='EMPAQUE' GROUP BY campo";
			$res2 = $this->db->queryAll($sql2);
			foreach ($res2 as $key2 => $value2) {
				$value2 = (object)$value2;
				$data2[] = $value2;
			}
			$value->empaque = $data2;
			$data2 = [];
			$sql2 = "SELECT campo, type, (cantidad) AS cantidad FROM calidad_detalle_unit WHERE 
			id_calidad = $value->id AND type='OTROS' GROUP BY campo";
			$res2 = $this->db->queryAll($sql2);
			foreach ($res2 as $key2 => $value2) {
				$value2 = (object)$value2;
				$data2[] = $value2;
			}
			$value->otros = $data2;
			$data[] = $value;
		}
		return $data;
	}

	private function tags($inicio, $fin, $cliente, $marca, $contenedor = "", $proceso){
		$sql_add = "";
		if(!empty($cliente) && !empty($marca)){
			$sql_add .= " AND cliente = '$cliente' AND marca = '$marca'";
		}
		if(!empty($contenedor)){
			$sql_add .= " AND contenedor = '{$contenedor}' ";
        }
        if(!empty($marca)){
            if($marca == 'TIA' || $marca == 'MI COMISARIATO'){
                $sql_add .= " AND tipo_muestreo = '{$proceso}'";
            }
        }

		$peso = "AVG(ROUND((peso / 2.2) , 2))";
		if($this->session->id_company == 2){
			$peso = "IF(fecha > '2017-07-12' ,ROUND(AVG(peso),2) , AVG(ROUND((peso / 2.2) , 2)))";
		}


		$sql = "SELECT AVG(calidad) AS calidad FROM calidad 	WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND calidad > 0 $sql_add";
		$rp1 = $this->db->queryRow($sql);
		$sql = "SELECT MAX(calidad_dedos) AS calidad_maxima FROM calidad 	WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND calidad_dedos > 0  $sql_add";
		$rp2 = $this->db->queryRow($sql);
		$sql = "SELECT MIN(calidad_dedos) AS calidad_minima FROM calidad 	WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND calidad_dedos > 0 $sql_add";
		$rp3 = $this->db->queryRow($sql);

		$sql = "SELECT MAX(calidad_cluster) AS calidad_maxima FROM calidad 	WHERE fecha BETWEEN '$inicio' AND '$fin' AND calidad_cluster > 0  $sql_add";
		$calidad_max_cluster = $this->db->queryOne($sql);
		$sql = "SELECT MIN(calidad_cluster) AS calidad_minima FROM calidad 	WHERE fecha BETWEEN '$inicio' AND '$fin' AND calidad_cluster > 0 $sql_add";
		$calidad_min_cluster = $this->db->queryOne($sql);

		$sql = "SELECT AVG(desviacion_estandar) AS desviacion_estandar FROM calidad 	WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND desviacion_estandar > 0 $sql_add";
		$rp4 = $this->db->queryRow($sql);
		$sql = "SELECT $peso AS peso FROM calidad WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND peso > 0 $sql_add";
		$rp5 = $this->db->queryRow($sql);
		if($this->session->id_company == 2)
			$sql = "SELECT AVG(cantidad_gajos) AS cluster FROM calidad 	WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND cantidad_gajos > 0 $sql_add";
		else
			$sql = "SELECT AVG(total_gajos) AS cluster FROM calidad 	WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND cantidad_gajos > 0 $sql_add";
		$rp6 = $this->db->queryRow($sql);
		$sql = "SELECT AVG(calidad_dedos) AS calidad_dedos FROM calidad WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND calidad_dedos > 0 $sql_add";
		$rp7 = $this->db->queryRow($sql);
		$sql = "SELECT AVG(calidad_cluster) AS calidad_cluster FROM calidad WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND calidad_cluster > 0 $sql_add";
		$rp8 = $this->db->queryRow($sql);
		if($this->session->id_company == 6)
			$sql = "SELECT AVG(cantidad_dedos_caja) AS dedos_promedio FROM calidad WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND cantidad_dedos_caja > 0 $sql_add";
		else
			$sql = "SELECT AVG(cantidad_dedos) AS dedos_promedio FROM calidad WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND cantidad_dedos > 0 $sql_add";
		$rp9 = $this->db->queryRow($sql);
		$tags = [];

		$sql_desviacion = "SELECT (SELECT AVG(calidad_dedos) AS avg_calidad FROM calidad WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND calidad_dedos > 0 $sql_add) AS avg_calidad , 
							calidad_dedos 
							FROM calidad WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND calidad_dedos > 0 $sql_add";
		// D($sql_desviacion);
		$dEstandar = $this->db->queryAll($sql_desviacion);
		$Varianza = 0;
		$Count = 0;
		$Desviacion_estandar = 0;
		foreach ($dEstandar as $key => $value) {
			$Count++;
			// D($value->avg_calidad);
			// D($value->calidad_dedos);
			$Varianza += (($value->avg_calidad - $value->calidad_dedos) * ($value->avg_calidad - $value->calidad_dedos));
		}
		// D($Varianza);
		$Desviacion_estandar = ($Varianza / ($Count-1));


		$tags = array(
			'calidad' => 100,
			'calidad_maxima_dedos' => $rp2->calidad_maxima,
			'calidad_dedos' => $rp7->calidad_dedos,
			'calidad_minima_dedos' => $rp3->calidad_minima,
			'calidad_minima_cluster' => $calidad_min_cluster,
			'calidad_maxima_cluster' => $calidad_max_cluster,
			'desviacion_estandar' => $Desviacion_estandar,
			'peso' => $rp5->peso,
			'cluster' => $rp6->cluster,
			'calidad_cluster' => $rp8->calidad_cluster,
			'dedos_promedio' => $rp9->dedos_promedio
		);
		return $tags;
	}

	private function grafica_principal_calidad($inicio, $fin, $cliente, $marca, $contenedor = "", $proceso){
		$sql_add = "";
		if(!empty($cliente) && !empty($marca)){
			$sql_add .= " AND cliente = '$cliente' AND marca = '$marca'";
		}
		if(!empty($contenedor)){
			$sql_add .= " AND contenedor = '{$contenedor}' ";
        }
        if(!empty($marca)){
            if(in_array($marca, ["MI COMISARIATO", "TIA"])){
                $sql_add .= " AND tipo_muestreo = '{$proceso}'";
            }
		}
		if((int)$this->session->id_company != 6){
			$sql_add .= " AND calidad_dedos > 0  AND cantidad_gajos > 0 ";
		}

		$sql = "SELECT semana, ROUND(AVG(calidad_dedos), 2) AS calidad, MAX(calidad_dedos) AS calidad_maxima, MIN(calidad_dedos) AS calidad_minima
				FROM calidad
				WHERE semana >= 0 AND YEAR(fecha) = YEAR('{$inicio}') AND fecha != '2017-03-07' $sql_add
				GROUP BY semana";
				
        $res = $this->db->queryAll($sql);
        
        $response = (object)[
            "legends" => [],
            "series" => [
                "Calidad" => [
                    "name" => "Calidad Dedos",
                    "connectNulls" => true,
                    "type" => "line",
                    "itemStyle" => [
                        "normal" => [
                            "barBorderRadius" => 0,
                            "barBorderWidth" => 6
                        ]
                    ],
                    "data" => [],
                ]
            ]
        ];
		foreach ($res as $key => $value) {
			//$value = (object)$value;
            //$data[$value->semana] = $value;

            $response->legends[] = $value->semana;
            $response->series["Calidad"]["data"][] = $value->calidad;
		}
		return $response;
	}

	private function grafica_principal_de($inicio, $fin, $cliente , $marca, $contenedor = "", $proceso){
		$sql_add = "";
		if(!empty($cliente) && !empty($marca)){
			$sql_add .= " AND cliente = '$cliente' AND marca = '$marca'";
		}
		if(!empty($contenedor)){
			$sql_add .= " AND contenedor = '{$contenedor}' ";
        }
        if(!empty($marca)){
            if(in_array($marca, ["MI COMISARIATO", "TIA"])){
                $sql_add .= " AND tipo_muestreo = '{$proceso}'";
            }
        }

		if($this->session->id_company != 6)
			$sql_add .= " AND cantidad_gajos > 0";

		$sql = "SELECT semana, ROUND(STD(calidad_dedos), 2) AS desviacion_estandar
				FROM calidad 
				WHERE calidad_dedos > 0 AND YEAR(fecha) = YEAR('{$inicio}') $sql_add
				GROUP BY semana
				ORDER BY semana DESC";
		$res = $this->db->queryAll($sql);
        
        $response = (object)[
            "legends" => [],
            "series" => [
                "Desviacion" => [
                    "name" => "Desviación",
                    "connectNulls" => true,
                    "type" => "line",
                    "itemStyle" => [
                        "normal" => [
                            "barBorderRadius" => 0,
                            "barBorderWidth" => 6
                        ]
                    ],
                    "data" => [],
                ]
            ]
        ];
		foreach ($res as $key => $value) {
			//$value = (object)$value;
            //$data[$value->semana] = $value;

            $response->legends[] = $value->semana;
            $response->series["Desviacion"]["data"][] = $value->desviacion_estandar;
		}
		return $response;
	}

	private function grafica_principal_peso($inicio, $fin, $cliente , $marca, $contenedor = "", $proceso){
		$sql_add = "";
		if(!empty($cliente) && !empty($marca)){
			$sql_add .= " AND cliente = '$cliente' AND marca = '$marca'";
		}
		if(!empty($contenedor)){
			$sql_add .= " AND contenedor = '{$contenedor}' ";
        }
        if(!empty($marca)){
            if(in_array($marca, ["MI COMISARIATO", "TIA"])){
                $sql_add .= " AND tipo_muestreo = '{$proceso}'";
            }
        }
		if($this->session->id_company != 6)
			$sql_add .= " AND cantidad_gajos > 0";

		
		$peso = "ROUND(AVG(peso / 2.2) , 2))";
		if($this->session->id_company == 2){
			$peso = "IF(fecha > '2017-07-12' ,ROUND(AVG(peso),2) , ROUND(AVG((peso / 2.2)), 2))";
		}

		$sql = "SELECT semana, {$peso} AS peso
				FROM calidad
				WHERE calidad_dedos > 0 AND YEAR(fecha) = YEAR('{$inicio}') $sql_add
				GROUP BY semana";
		$res = $this->db->queryAll($sql);
        //$data = [];
        
        $response = (object)[
            "legends" => [],
            "series" => [
                "Peso" => [
                    "name" => "Peso",
                    "connectNulls" => true,
                    "type" => "line",
                    "itemStyle" => [
                        "normal" => [
                            "barBorderRadius" => 0,
                            "barBorderWidth" => 6
                        ]
                    ],
                    "data" => [],
                ]
            ]
        ];
		foreach ($res as $key => $value) {
			//$value = (object)$value;
            //$data[$value->semana] = $value;
            
            $response->legends[] = $value->semana;
            $response->series["Peso"]["data"][] = $value->peso;
		}
		return $response;
	}
	
	private function grafica_principal_peso_cluster($inicio, $fin, $cliente , $marca, $contenedor = "", $proceso){
		$sql_add = "";
		if(!empty($cliente) && !empty($marca)){
			$sql_add = " AND cliente = '$cliente' AND marca = '$marca'";
		}
		if(!empty($contenedor)){
			$sql_add .= " AND contenedor = '{$contenedor}' ";
        }
        if(!empty($marca)){
            if(in_array($marca, ["MI COMISARIATO", "TIA"])){
                $sql_add .= " AND tipo_muestreo = '{$proceso}'";
            }
        }
		if($this->session->id_company != 6)
			$sql_add .= " AND cantidad_gajos > 0";

		$sql = "SELECT semana, ROUND(AVG(peso_cluster), 2) AS peso_cluster
				FROM calidad
				WHERE calidad_dedos > 0 AND YEAR(fecha) = YEAR('{$inicio}') $sql_add
				GROUP BY semana";
		$res = $this->db->queryAll($sql);
        
        $response = (object)[
            "legends" => [],
            "series" => [
                "Peso" => [
                    "name" => "Peso Clúster",
                    "connectNulls" => true,
                    "type" => "line",
                    "itemStyle" => [
                        "normal" => [
                            "barBorderRadius" => 0,
                            "barBorderWidth" => 6
                        ]
                    ],
                    "data" => [],
                ]
            ]
        ];
		foreach ($res as $key => $value) {
			//$value = (object)$value;
            //$data[$value->semana] = $value;
            
            $response->legends[] = $value->semana;
            $response->series["Peso"]["data"][] = $value->peso_cluster;
		}
		return $response;
	}

	private function grafica_principal_cluster($inicio, $fin, $cliente , $marca, $contenedor = "", $proceso){
		$sql_add = "";
		if(!empty($cliente) && !empty($marca)){
			$sql_add = " AND cliente = '$cliente' AND marca = '$marca'";
		}
		if(!empty($contenedor)){
			$sql_add .= " AND contenedor = '{$contenedor}' ";
        }
        if(!empty($marca)){
            if(in_array($marca, ["MI COMISARIATO", "TIA"])){
                $sql_add .= " AND tipo_muestreo = '{$proceso}'";
            }
        }
		if($this->session->id_company != 6)
			$sql_add .= " AND cantidad_gajos > 0";

		$sql = "SELECT semana, ROUND(AVG(cantidad_gajos), 2) AS cluster
				FROM calidad
				WHERE calidad_dedos > 0 AND YEAR(fecha) = YEAR('{$inicio}') $sql_add
				GROUP BY semana";
		$res = $this->db->queryAll($sql);
        
        $response = (object)[
            "legends" => [],
            "series" => [
                "Peso" => [
                    "name" => "Clúster",
                    "connectNulls" => true,
                    "type" => "line",
                    "itemStyle" => [
                        "normal" => [
                            "barBorderRadius" => 0,
                            "barBorderWidth" => 6
                        ]
                    ],
                    "data" => [],
                ]
            ]
        ];
		foreach ($res as $key => $value) {
			//$value = (object)$value;
            //$data[$value->semana] = $value;
            $response->legends[] = $value->semana;
            $response->series["Peso"]["data"][] = (float) $value->cluster;
		}
		return $response;
	}

	#victor
	private function grafica_calidad_historico_marcas($inicio, $fin, $cliente, $marca, $contenedor = ""){
		$sql_add = "";
		if(!empty($cliente) && !empty($marca)){
			$sql_add = " AND cliente = '$cliente' AND marca = '$marca'";
		}
		if(!empty($contenedor)){
			$sql_add .= " AND contenedor = '{$contenedor}' ";
		}

		$sCompany = " AND calidad_dedos > 0  AND cantidad_gajos > 0";
		if($this->session->id_company == 6){
			$sCompany = " AND calidad_dedos > 0 ";
		}

		$sql = "SELECT fecha AS fecha, AVG(calidad_dedos) AS rewe, MAX(calidad_dedos) AS pinalinda, MIN(calidad_dedos) AS palmar_aldi
				FROM calidad
				WHERE fecha BETWEEN '$inicio' AND '$fin' {$sCompany}
				$sql_add
				GROUP BY fecha";
		
		$res = $this->db->queryAll($sql);
		$data = [];
		foreach ($res as $key => $value) {
			$value = (object)$value;
			$data[$value->fecha] = $value;
		}
		return $data;
	}

	private function grafica_danos_total($inicio, $fin, $cliente = "", $marca = ""){
		$sql_add = "";
		if(!empty($cliente)){
			$sql_add .= " AND cliente = '$cliente'";
        }
        if(!empty($marca)){
            $sql_add .= " AND marca = '$marca'";
        }
		$seleccion = "";
		$empaque = "";
		$otros = "";
		if((int)$this->session->id_company != 6){
			$seleccion = "AND seleccion > 0";
			$empaque = "AND empaque > 0";
			$otros = "AND otros > 0";
        }
        
        $response = (object)[
            "legends" => [],
            "series" => []
        ];

		$data = [];
        $sql = "SELECT semanas.semana, seleccion
                FROM (
                    SELECT semana
                    FROM calidad
                    WHERE YEAR(fecha) = YEAR('{$inicio}') $sql_add
                    GROUP BY semana
                    ORDER BY semana
                ) AS semanas
                LEFT JOIN (
                    SELECT semana, ROUND(AVG(seleccion), 2) AS seleccion 
                    FROM calidad
                    WHERE YEAR(fecha) = YEAR('{$inicio}') $seleccion $sql_add
                    GROUP BY semana
				) AS calidad ON semanas.semana = calidad.semana";
			
		$res = $this->db->queryAll($sql);
		foreach ($res as $key => $value) {
            if(!isset($response->series["SELECCION"])){
                $response->series["SELECCION"] = [
                    "name" => "Selección",
                    "connectNulls" => true,
                    "type" => "line",
                    "itemStyle" => [
                        "normal" => [
                            "barBorderRadius" => 0,
                            "barBorderWidth" => 6
                        ]
                    ],
                    "data" => [],
                ];
            }
            if(!in_array($value->semana, $response->legends)) $response->legends[] = $value->semana;
            $response->series['SELECCION']["data"][] = ($value->seleccion > 0) ? (float) $value->seleccion : null;
		}

        $sql = "SELECT semanas.semana, empaque
                FROM (
                    SELECT semana
                    FROM calidad
                    WHERE YEAR(fecha) = YEAR('{$inicio}') $sql_add
                    GROUP BY semana
                    ORDER BY semana
                ) AS semanas
                LEFT JOIN (
                    SELECT semana, ROUND(AVG(empaque), 2) AS empaque
                    FROM calidad 
                    WHERE YEAR(fecha) = YEAR('{$inicio}') $empaque $sql_add 
                    GROUP BY semana
                ) AS calidad ON semanas.semana = calidad.semana";
		$res = $this->db->queryAll($sql);
		foreach ($res as $key => $value) {
			if(!isset($response->series["EMPAQUE"])){
                $response->series["EMPAQUE"] = [
                    "name" => "Empaque",
                    "connectNulls" => true,
                    "type" => "line",
                    "itemStyle" => [
                        "normal" => [
                            "barBorderRadius" => 0,
                            "barBorderWidth" => 6
                        ]
                    ],
                    "data" => [],
                ];
            }
            $response->series['EMPAQUE']["data"][] = ($value->empaque > 0) ? (float) $value->empaque : null;
		}

        $sql = "SELECT semanas.semana, otros
                FROM (
                    SELECT semana
                    FROM calidad
                    WHERE YEAR(fecha) = YEAR('{$inicio}') $sql_add
                    GROUP BY semana
                    ORDER BY semana
                ) AS semanas
                LEFT JOIN (
                    SELECT semana, ROUND(AVG(otros), 2) AS otros
                    FROM calidad 
                    WHERE YEAR(fecha) = YEAR('{$inicio}') $otros $sql_add 
                    GROUP BY semana
                ) AS calidad ON semanas.semana = calidad.semana";
		$res = $this->db->queryAll($sql);
		foreach ($res as $key => $value) {
			if(!isset($response->series["OTROS"])){
                $response->series["OTROS"] = [
                    "name" => "Otros",
                    "connectNulls" => true,
                    "type" => "line",
                    "itemStyle" => [
                        "normal" => [
                            "barBorderRadius" => 0,
                            "barBorderWidth" => 6
                        ]
                    ],
                    "data" => [],
                ];
            }
            $response->series['OTROS']["data"][] = ($value->otros > 0) ? (float) $value->otros : null;
		}

		return $response;
	}

	// get all campos
	private function grafica_danos_get_campos($type){
		$sql_add = "";
		if(!empty($cliente) && !empty($marca)){
			$sql_add = " AND cliente = '$cliente' AND marca = '$marca'";
		}

		$data = [];
		$type = strtoupper($type);
		$sql = "SELECT campo FROM calidad_detalle WHERE TYPE = '$type'GROUP BY campo";
		$res = $this->db->queryAll($sql);
		foreach ($res as $key => $value) {
			$value = (object)$value;
			$data[] = $value->campo;
		}
		return $data;
	}

	private function grafica_danos_seleccion($inicio, $fin , $cliente, $marca, $contenedor = ""){
		$sql_add = "";
		if(!empty($cliente) && !empty($marca)){
			$sql_add = " AND c.cliente = '$cliente' AND c.marca = '$marca'";
		}
		if(!empty($contenedor)){
			$sql_add .= " AND contenedor = '{$contenedor}' ";
		}

		$sql = "SELECT semanas.semana, labels.label, cantidad
                FROM (
                    SELECT semana
                    FROM calidad_detalle cd
                    INNER JOIN calidad AS c ON c.id = cd.id_calidad
                    WHERE cd.type = 'SELECCION' AND YEAR(fecha) = YEAR('{$inicio}') AND cantidad > 0 $sql_add
                    GROUP BY semana
                ) AS semanas
                JOIN (
                    SELECT cd.campo AS label
                    FROM calidad_detalle cd
                    INNER JOIN calidad AS c ON c.id = cd.id_calidad
                    WHERE cd.type = 'SELECCION' AND YEAR(fecha) = YEAR('{$inicio}') AND cantidad > 0 $sql_add
                    GROUP BY cd.campo
                ) AS labels
                LEFT JOIN (
                    SELECT c.semana, cd.campo AS label, ROUND(AVG(cd.cantidad), 2) AS cantidad
                    FROM calidad_detalle cd
                    INNER JOIN calidad AS c ON c.id = cd.id_calidad
                    WHERE cd.type = 'SELECCION' AND YEAR(fecha) = YEAR('{$inicio}') AND cantidad > 0 $sql_add
                    GROUP BY c.semana , cd.campo   
                ) AS calidad ON semanas.semana = calidad.semana AND labels.label = calidad.label";
        $res = $this->db->queryAll($sql);
        
        $response = (object)[
            "legends" => [],
            "series" => []
        ];
		foreach ($res as $key => $value) {
            if(!isset($response->series[$value->label])){
                $response->series[$value->label] = [
                    "name" => $value->label,
                    "connectNulls" => true,
                    "type" => "line",
                    "itemStyle" => [
                        "normal" => [
                            "barBorderRadius" => 0,
                            "barBorderWidth" => 6
                        ]
                    ],
                    "data" => [],
                ];
            }
            if(!in_array($value->semana, $response->legends)) $response->legends[] = $value->semana;
            $response->series[$value->label]["data"][] = ($value->cantidad > 0) ? (float) $value->cantidad : null;
		}
		return $response;
	}

	private function grafica_danos_empaque($inicio, $fin , $cliente, $marca, $contenedor = ""){
		$sql_add = "";
		if(!empty($cliente) && !empty($marca)){
			$sql_add = " AND c.cliente = '$cliente' AND c.marca = '$marca'";
		}
		/*if(!empty($contenedor)){
			$sql_add .= " AND contenedor = '{$contenedor}' ";
		}*/

		$sql = "SELECT semanas.semana, labels.label, cantidad
                FROM (
                    SELECT semana
                    FROM calidad_detalle cd
                    INNER JOIN calidad AS c ON c.id = cd.id_calidad
                    WHERE cd.type = 'EMPAQUE' AND YEAR(fecha) = YEAR('{$inicio}') AND cantidad > 0 $sql_add
                    GROUP BY c.semana
                ) AS semanas
                JOIN (
                    SELECT campo AS label
                    FROM calidad_detalle cd
                    INNER JOIN calidad AS c ON c.id = cd.id_calidad
                    WHERE cd.type = 'EMPAQUE' AND YEAR(fecha) = YEAR('{$inicio}') AND cantidad > 0 $sql_add
                    GROUP BY campo
                ) AS labels
                LEFT JOIN (
                    SELECT c.semana, cd.campo AS label, ROUND(AVG(cd.cantidad), 2) AS cantidad
                    FROM calidad_detalle cd
                    INNER JOIN calidad AS c ON c.id = cd.id_calidad
                    WHERE cd.type = 'EMPAQUE' AND YEAR(fecha) = YEAR('{$inicio}') AND cantidad > 0 $sql_add
                    GROUP BY c.semana , cd.campo
                ) AS calidad ON semanas.semana = calidad.semana AND labels.label = calidad.label";
        $res = $this->db->queryAll($sql);
        
        $response = (object)[
            "legends" => [],
            "series" => []
        ];
		foreach ($res as $key => $value) {
            if(!isset($response->series[$value->label])){
                $response->series[$value->label] = [
                    "name" => $value->label,
                    "connectNulls" => true,
                    "type" => "line",
                    "itemStyle" => [
                        "normal" => [
                            "barBorderRadius" => 0,
                            "barBorderWidth" => 6
                        ]
                    ],
                    "data" => [],
                ];
            }
            if(!in_array($value->semana, $response->legends)) $response->legends[] = $value->semana;
            $response->series[$value->label]["data"][] = ($value->cantidad > 0) ? (float) $value->cantidad : null;
		}
		return $response;
	}

	private function grafica_danos_otros($inicio, $fin , $cliente, $marca, $contenedor = ""){
		$sql_add = "";
		if(!empty($cliente) && !empty($marca)){
			$sql_add = " AND c.cliente = '$cliente' AND c.marca = '$marca'";
		}
		/*if(!empty($contenedor)){
			$sql_add .= " AND contenedor = '{$contenedor}' ";
		}*/

		$sql = "SELECT semanas.semana, labels.label, cantidad
                FROM (
                    SELECT c.semana
                    FROM calidad_detalle cd
                    INNER JOIN calidad AS c ON c.id = cd.id_calidad
                    WHERE cd.type = 'OTROS' AND YEAR(fecha) = YEAR('{$inicio}') AND cantidad > 0 $sql_add
                    GROUP BY c.semana
                ) AS semanas
                JOIN (
                    SELECT campo AS label
                    FROM calidad_detalle cd
                    INNER JOIN calidad AS c ON c.id = cd.id_calidad
                    WHERE cd.type = 'OTROS' AND YEAR(fecha) = YEAR('{$inicio}') AND cantidad > 0 $sql_add
                    GROUP BY campo
                ) AS labels
                LEFT JOIN (
                    SELECT c.semana, cd.campo AS label, ROUND(AVG(cd.cantidad), 2) AS cantidad
                    FROM calidad_detalle cd
                    INNER JOIN calidad AS c ON c.id = cd.id_calidad
                    WHERE cd.type = 'OTROS' AND YEAR(fecha) = YEAR('{$inicio}') AND cantidad > 0 $sql_add
                    GROUP BY c.semana , cd.campo
                ) AS calidad ON semanas.semana = calidad.semana AND labels.label = calidad.label";
        $res = $this->db->queryAll($sql);
        
        $response = (object)[
            "legends" => [],
            "series" => []
        ];
		foreach ($res as $key => $value) {
			if(!isset($response->series[$value->label])){
                $response->series[$value->label] = [
                    "name" => $value->label,
                    "connectNulls" => true,
                    "type" => "line",
                    "itemStyle" => [
                        "normal" => [
                            "barBorderRadius" => 0,
                            "barBorderWidth" => 6
                        ]
                    ],
                    "data" => [],
                ];
            }
            if(!in_array($value->semana, $response->legends)) $response->legends[] = $value->semana;
            $response->series[$value->label]["data"][] = ($value->cantidad > 0) ? (float) $value->cantidad : null;
		}
		return $response;
	}

	private function tabla_principal_calidad($inicio, $fin, $cliente, $marca, $contenedor = "", $proceso){
		$sql_add = "";
		if(!empty($cliente) && !empty($marca)){
			$sql_add = " AND cliente = '$cliente' AND marca = '$marca'";
		}
		if(!empty($contenedor)){
			$sql_add .= " AND contenedor = '{$contenedor}' ";
        }
        if(!empty($marca)){
            if($marca == 'TIA' || $marca == 'MI COMISARIATO'){
                $sql_add .= " AND tipo_muestreo = '{$proceso}'";
            }
        }

		$data = [];
		$sql = "SELECT cliente, marca, AVG(calidad_dedos) AS calidad FROM calidad WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND calidad_dedos > 0 $sql_add GROUP BY DATE(fecha), cliente, marca";
		$res = $this->db->queryAll($sql);
		foreach ($res as $key => $value) {
			$value = (object)$value;
			$data[$value->cliente][$value->marca]['calidad']['total'] += $value->calidad;
			$data[$value->cliente][$value->marca]['calidad']['registros']++;
		}

		$sql = "SELECT cliente, marca, AVG(cantidad_gajos) AS cluster FROM calidad WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND cantidad_gajos > 0 $sql_add GROUP BY DATE(fecha), cliente, marca";
		$res = $this->db->queryAll($sql);
		foreach ($res as $key => $value) {
			$value = (object)$value;
			$data[$value->cliente][$value->marca]['cluster']['total'] += $value->cluster;
			$data[$value->cliente][$value->marca]['cluster']['registros']++;
		}

		$sql = "SELECT cliente, marca, IF(fecha > '2017-07-12', AVG(peso), AVG(peso / 2.2 )) AS peso FROM calidad WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND peso > 0 $sql_add GROUP BY DATE(fecha), cliente, marca";
		$res = $this->db->queryAll($sql);
		foreach ($res as $key => $value) {
			$value = (object)$value;
			$data[$value->cliente][$value->marca]['peso']['total'] += $value->peso;
			$data[$value->cliente][$value->marca]['peso']['registros']++;
		}

		$data_final = [];
		$c= 0;
		foreach($data as $cliente => $record){
			$data_final[$c]['id'] = $c;
			$data_final[$c]['cliente_marca'] = $cliente;
			$record_keys = array_keys($record);

			$suma_calidad = 0;
			$avg_calidad  = 0;
			$suma_cluster = 0;
			$avg_cluster  = 0;
			$suma_peso    = 0;
			$avg_peso     = 0;
			$d = 0;
			foreach($record_keys as $marca){
				$data_final[$c]['marcas'][$d]['marca'] = $marca;
				$data_final[$c]['marcas'][$d]['calidad'] = $data[$cliente][$marca]['calidad']['total'] / $data[$cliente][$marca]['calidad']['registros'];
				$suma_calidad += $data_final[$c]['marcas'][$d]['calidad'];
				$avg_calidad++;
				$data_final[$c]['marcas'][$d]['cluster'] = $data[$cliente][$marca]['cluster']['total'] / $data[$cliente][$marca]['cluster']['registros'];
				$suma_cluster += $data_final[$c]['marcas'][$d]['cluster'];
				$avg_cluster++;
				$data_final[$c]['marcas'][$d]['peso'] = $data[$cliente][$marca]['peso']['total'] / $data[$cliente][$marca]['peso']['registros'];
				$suma_peso += $data_final[$c]['marcas'][$d]['peso'];
				$avg_peso++;
				$d++;
			}
			$data_final[$c]['calidad'] = $suma_calidad / $avg_calidad;
			$data_final[$c]['cluster'] = $suma_cluster / $avg_cluster;
			$data_final[$c]['peso']    = $suma_peso / $avg_peso;
			$c++;
		}

		return $data_final;
	}

	private function tabla_principal_danos($inicio, $fin, $cliente = "", $marca, $contenedor, $proceso){
		$sql_add = "";
		if(!empty($cliente) && !empty($marca)){
			$sql_add = " AND c.cliente = '$cliente' AND c.marca = '$marca'";
		}
		if(!empty($contenedor)){
			$sql_add .= " AND contenedor = '{$contenedor}' ";
        }
        if(!empty($marca)){
            if($marca == 'TIA' || $marca == 'MI COMISARIATO'){
                $sql_add .= " AND tipo_muestreo = '{$proceso}'";
            }
        }

		$data = [];
		$sql = "SELECT c.cliente, c.marca, AVG(cd.cantidad) AS cantidad
				FROM calidad_detalle cd
				INNER JOIN calidad AS c ON c.id = cd.id_calidad
				WHERE cd.type = 'SELECCION' AND DATE(c.fecha) BETWEEN '$inicio' AND '$fin' AND cantidad > 0
				$sql_add
				GROUP BY c.cliente, c.marca";
		$res = $this->db->queryAll($sql);
		foreach ($res as $key => $value) {
			$value = (object)$value;
			$data[$value->cliente][$value->marca]['seleccion']['total'] += $value->cantidad;
			$data[$value->cliente][$value->marca]['seleccion']['registros']++;
		}

		$sql = "SELECT c.cliente, c.marca, AVG(cd.cantidad) AS cantidad
				FROM calidad_detalle cd
				INNER JOIN calidad AS c ON c.id = cd.id_calidad
				WHERE cd.type = 'EMPAQUE' AND DATE(c.fecha) BETWEEN '$inicio' AND '$fin' AND cantidad > 0
				$sql_add
				GROUP BY c.cliente, c.marca";
		$res = $this->db->queryAll($sql);
		foreach ($res as $key => $value) {
			$value = (object)$value;
			$data[$value->cliente][$value->marca]['empaque']['total'] += $value->cantidad;
			$data[$value->cliente][$value->marca]['empaque']['registros']++;
		}

		$sql = "SELECT c.cliente, c.marca, AVG(cd.cantidad) AS cantidad
				FROM calidad_detalle cd
				INNER JOIN calidad AS c ON c.id = cd.id_calidad
				WHERE cd.type = 'OTROS' AND DATE(c.fecha) BETWEEN '$inicio' AND '$fin' AND cantidad > 0
				$sql_add
				GROUP BY c.cliente, c.marca";
		$res = $this->db->queryAll($sql);
		foreach ($res as $key => $value) {
			$value = (object)$value;
			$data[$value->cliente][$value->marca]['otros']['total'] += $value->cantidad;
			$data[$value->cliente][$value->marca]['otros']['registros']++;
		}

		$data_final = [];
		$c= 0;
		foreach($data as $cliente => $record){
			$data_final[$c]['id'] = $c;
			$data_final[$c]['cliente_marca'] = $cliente;
			$record_keys = array_keys($record);

			$suma_calidad = 0;
			$avg_calidad  = 0;
			$suma_cluster = 0;
			$avg_cluster  = 0;
			$suma_peso    = 0;
			$avg_peso     = 0;
			$d = 0;

			foreach($record_keys as $marca){
				$data_final[$c]['marcas'][$d]['marca'] = $marca;
				$data_final[$c]['marcas'][$d]['seleccion'] = $data[$cliente][$marca]['seleccion']['total'] / $data[$cliente][$marca]['seleccion']['registros'];
				$suma_calidad += $data_final[$c]['marcas'][$d]['seleccion'];
				$avg_calidad++;
				$data_final[$c]['marcas'][$d]['empaque'] = $data[$cliente][$marca]['empaque']['total'] / $data[$cliente][$marca]['empaque']['registros'];
				$suma_cluster += $data_final[$c]['marcas'][$d]['empaque'];
				$avg_cluster++;
				$data_final[$c]['marcas'][$d]['otros'] = $data[$cliente][$marca]['otros']['total'] / $data[$cliente][$marca]['otros']['registros'];
				$suma_peso += $data_final[$c]['marcas'][$d]['otros'];
				$avg_peso++;
				$d++;
			}
			$data_final[$c]['seleccion'] = (float)($suma_calidad / $avg_calidad);
			$data_final[$c]['empaque'] = (float)($suma_cluster / $avg_cluster);
			$data_final[$c]['otros']    = (float)($suma_peso / $avg_peso);
			$c++;
		}

		return $data_final;
	}

	private function getDanhos($inicio, $fin, $cliente, $marca, $contenedor = "", $proceso){
		$sql_add = "";
		if(!empty($cliente) && !empty($marca)){
			$sql_add .= " AND cliente = '$cliente' AND marca = '$marca'";
		}
		if(!empty($contenedor)){
			$sql_add .= " AND contenedor = '{$contenedor}' ";
        }
        if(!empty($marca)){
            if($marca == 'TIA' || $marca == 'MI COMISARIATO'){
                $sql_add .= " AND tipo_muestreo = '{$proceso}'";
            }
        }

		$sql = "SELECT 'SELECCIÓN' AS name, 
		            CASE WHEN AVG(seleccion) IS NULL or AVG(seleccion) = ''
		                THEN 0 ELSE AVG(seleccion) 
                    END AS value 
		        FROM calidad 
                WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND seleccion > 0 $sql_add";
		$row = $this->db->queryRow($sql);
        if($row->value > 0)
		    $data[] = $row;

		$sql = "SELECT 'EMPAQUE' AS name, 
                    CASE 
                        WHEN AVG(empaque) IS NULL OR AVG(empaque) = '' THEN 0 ELSE AVG(empaque) 
                    END AS value
                FROM calidad
                WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND empaque > 0 $sql_add";
        $row = $this->db->queryRow($sql);
        if($row->value > 0)
		    $data[] = $row;

		$sql = "SELECT 'OTROS' AS name,
                    CASE WHEN AVG(otros) IS NULL OR AVG(otros) = '' THEN 0
		                ELSE AVG(otros) 
                    END AS value 
                FROM calidad 
                WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND otros > 0 $sql_add";
		$row = $this->db->queryRow($sql);
        if($row->value > 0)
		    $data[] = $row;

		return $data;
		/*
		$sql = "SELECT AVG(seleccion) AS seleccion,
		AVG(empaque) AS empaque,
		AVG(otros) AS otros
		FROM calidad
		WHERE seleccion > 0 AND empaque > 0 AND otros > 0 AND DATE(fecha) BETWEEN '$inicio' AND '$fin'";
		$danhos = $this->db->queryAll($sql);
		$data = [];
		foreach ($danhos as $key => $value) {
			$value = (object)$value;
			$data = [
				[
					"value" => $value->seleccion,
					"name" => "SELECCION"
				],
				[
					"value" => $value->empaque,
					"name" => "EMPAQUE"
				],
				[	
					"value" => $value->otros,
					"name" => "OTROS"
				]
			];
		}
		return $data;
		*/
	}

	private function getDetails($type = "SELECCION", $inicio, $fin,$cliente, $marca, $contenedor = "", $proceso){
		$sql_add = "";
		if(!empty($cliente) && !empty($marca)){
			$sql_add .= " AND c.cliente = '$cliente' AND c.marca = '$marca'";
		}
		if(!empty($contenedor)){
			$sql_add .= " AND contenedor = '{$contenedor}' ";
        }
        if(!empty($marca)){
            if($marca == 'TIA' || $marca == 'MI COMISARIATO'){
                $sql_add .= " AND tipo_muestreo = '{$proceso}'";
            }
        }

		$sql = "SELECT cd.campo AS name, SUM(cd.cantidad) as value 
                FROM calidad_detalle cd 
                INNER JOIN calidad AS c ON c.id = cd.id_calidad 
		        WHERE cd.type = '$type' AND DATE(c.fecha) BETWEEN '$inicio' AND '$fin' AND cantidad > 0 $sql_add 
				GROUP BY cd.campo";
				
		$response = $this->db->queryAll($sql);
		$response->value = (int)$response->value;

		return $response;
    }

    public function getLastWeek(){
        $response = new stdClass;
        $response->semana = $this->db->queryOne("SELECT getWeek(MAX(fecha)) FROM calidad");
        $response->semanas_disponibles = $this->db->queryAll("SELECT getWeek(fecha) AS semana FROM calidad WHERE getWeek(fecha) > 0 AND YEAR(fecha) = YEAR(CURRENT_DATE) GROUP BY getWeek(fecha)");
        return $response;
    }
    
    public function fotos(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));

        if($postdata->cliente != ""){
            $sWhere .= " AND cliente = '{$postdata->cliente}'";
        }
        if($postdata->marca != ""){
            $sWhere .= " AND marca = '{$postdata->marca}'";
        }
        if($postdata->fecha_inicial != "" && $postdata->fecha_final != ""){
            $sWhere .= " AND fecha BETWEEN '{$postdata->fecha_inicial}' AND '{$postdata->fecha_final}'";
        }
        if($postdata->desde > 0){
            $sWhere .= " AND semana >= $postdata->desde";
        }
        if($postdata->hasta > 0){
            $sWhere .= " AND semana <= $postdata->hasta";
		}
        if(!empty($postdata->marca)){
            if(in_array($postdata->marca, ["MI COMISARIATO", "TIA"])){
                $sWhere .= " AND tipo_muestreo = '{$postdata->proceso}'";
            }
        }

		$sql = "SELECT calidad.`marca`, fecha, semana, calidad.`calidad_cluster`, images AS url
				FROM calidad
				INNER JOIN calidad_images ON id_calidad = calidad.id
				WHERE semana > 0 $sWhere
				ORDER BY semana DESC";
        $data = $this->db->queryAll($sql);

        $response->marcas = [];
        foreach($data as $row){
            if(!in_array($row->marca, $response->marcas)) $response->marcas[] = $row->marca;
            $response->data[$row->semana]["semana"] = $row->semana;
            $response->data[$row->semana]["data"][] = $row;
        }
        return $response;
    }

    public function getComentariosImagenes(){
        echo "iniciamos";
        $data = $this->db->queryAll("SELECT id, archivoJson AS json FROM calidad LIMIT 0, 1000");
        foreach($data as $row){
            $json = json_decode(file_get_contents("http://app.procesos-iq.com/{$row->json}"), true);
            $observaciones = $json["pages"][1]["answers"][4]["values"];
            if(count($observaciones) > 0){
                D($observaciones);
            }
        }
    }
}