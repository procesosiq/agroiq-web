<?php defined('PHRAPI') or die("Direct access not allowed!");

class Enfunde extends ReactGrafica {
	public $name;
	private $db;
	private $config;

	public function __construct(){
        $this->config = $GLOBALS['config'];
        $this->session = Session::getInstance();
        $this->db = DB::getInstance($this->session->agent_user);
    }
    
    public function params(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $data = (object)[
            "id_finca" => getValueFrom($postdata, 'id_finca', 1),
            "week" => getValueFrom($postdata, 'week', ''),
            "year" => getValueFrom($postdata, 'year', date('y'))
        ];
        return $data;
    }

    public function last(){
        $response = new stdClass;
        $params = $this->params();

        $response->weeks = $this->db->queryAll("
            SELECT tbl.anio, tbl.semana, class
            FROM (
                SELECT anio_enfundado AS anio, semana_enfundada AS semana
                FROM produccion_historica
                WHERE anio_enfundado > 0 AND semana_enfundada > 0
                GROUP BY anio_enfundado, semana_enfundada
                UNION ALL
                SELECT years, semana
                FROM produccion_enfunde
                WHERE years > 0 AND semana > 0
                GROUP BY years, semana
            ) tbl
            LEFT JOIN semanas_colores sc ON sc.year = anio AND sc.semana = tbl.semana
            LEFT JOIN produccion_colores pc ON pc.color = sc.color
            GROUP BY tbl.anio, tbl.semana
            ORDER BY tbl.anio DESC, tbl.semana DESC");

        $response->last_year = $response->weeks[0]->anio;
        $response->last_week = $response->weeks[0]->semana;

        
        return $response;
    }

	public function index(){
        $response = new stdClass;
        $params = $this->params();

        $sql = "SELECT id, nombre as label
                FROM (
                    SELECT fincas.id, fincas.nombre
                    FROM produccion_enfunde
                    INNER JOIN fincas ON id_finca = fincas.id
                    WHERE years = {$params->year} AND semana = {$params->week}
                    GROUP BY id_finca
                    UNION ALL
                    SELECT fincas.id, fincas.nombre
                    FROM produccion_historica
                    INNER JOIN fincas ON id_finca = fincas.id
                    WHERE anio_enfundado = {$params->year} AND semana_enfundada = {$params->week}
                    GROUP BY id_finca
                ) tbl
                GROUP BY id";
        $response->fincas = $this->db->queryAllSpecial($sql);
        $response->lotes = $this->db->queryAllOne("SELECT lotes.nombre FROM lotes WHERE idFinca = {$params->id_finca}");
        $response->color = $this->db->queryOne("SELECT color FROM semanas_colores WHERE year = {$params->year} AND semana = {$params->week}");
        $response->color = $this->db->queryRow("SELECT * FROM produccion_colores WHERE color = '{$response->color}'");

        $sql = "SELECT lote,
                    IFNULL((SELECT SUM(usadas) FROM produccion_enfunde WHERE years = {$params->year} AND semana = {$params->week} AND id_finca = {$params->id_finca} AND lote = tbl.lote), 0) AS racimos_enfunde
                FROM (
                    SELECT lote
                    FROM produccion_enfunde
                    WHERE years = {$params->year} AND semana = {$params->week} AND id_finca = {$params->id_finca}
                    GROUP BY lote
                    UNION ALL
                    SELECT lote
                    FROM produccion_historica
                    WHERE anio_enfundado = {$params->year} AND semana_enfundada = {$params->week} AND id_finca = {$params->id_finca}
                    GROUP BY lote
                ) tbl
                GROUP BY lote
                ORDER BY lote+0";
        $response->data = $this->db->queryAll($sql);
        $response->edades = $this->db->queryAll("SELECT cinta, IFNULL(edad, 'S/C') AS edad FROM produccion_historica WHERE anio_enfundado = {$params->year} AND semana_enfundada = {$params->week} AND id_finca = {$params->id_finca} GROUP BY edad ORDER BY edad");

        foreach($response->data as $row){
            foreach($response->edades as $e){
                $row->{"edad_{$e->edad}"} = (int) $this->db->queryOne("SELECT COUNT(1) FROM produccion_historica WHERE anio_enfundado = {$params->year} AND semana_enfundada = {$params->week} AND id_finca = {$params->id_finca} AND lote = '{$row->lote}' AND edad = '{$e->edad}'");
                $row->cosechados += $row->{"edad_{$e->edad}"};
            }
            $row->enfunde = round(($row->racimos_enfunde > 0 ? ($row->cosechados/$row->racimos_enfunde*100) : 0), 2);
            $row->caidos = (int) $this->db->queryOne("SELECT SUM(cantidad) FROM produccion_racimos_caidos WHERE anio_enfundado = {$params->year} AND semana_enfundada = {$params->week} AND id_finca = {$params->id_finca} AND lote = '{$row->lote}'");
            $row->porc_caidos = round((($row->caidos > 0 && $row->racimos_enfunde > 0) ? ($row->caidos/$row->racimos_enfunde*100) : 0), 2);
        }

        return $response;
    }
}
