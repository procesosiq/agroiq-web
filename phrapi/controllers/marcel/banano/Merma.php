<?php defined('PHRAPI') or die("Direct access not allowed!");

class Merma {
	public $name;
	private $db;
	private $config;
	private $companies;
	private $string;

	public function __construct(){
        $this->config = $GLOBALS['config'];
		$this->session = Session::getInstance();
		$this->db = DB::getInstance($this->session->agent_user);
        $this->string = "Total Peso";
        $this->search = "Total Daños";
        $this->cajas = "18.86";
        $this->usd = "6.16";
        $this->factor = 1;
	}
	
	public function last(){
		$response = new stdClass;
		$response->fecha = $this->db->queryOne("SELECT MAX(date_fecha) FROM merma");
		return $response;
	}

    public function historicoSemanal(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));
		$palanca = "";
		if(isset($postdata->palanca)){
			$palanca = $postdata->palanca;
		}
		if(isset($postdata->idFinca)){
			$id_finca = $postdata->idFinca;
		}
		if(isset($postdata->year)){
			$year = $postdata->year;
		}else{
			$year = 2016;
        }
        
        $historico = $this->grafica_historico($palanca , $id_finca , $year);

        $response->historico = $historico->data;
        $response->historico_avg = $historico->avg;
		$response->historico_legends = $historico->legend;
        return $response;
    }

    public function tendenciaSemanal(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));
		$palanca = "";
		if(isset($postdata->palanca)){
			$palanca = $postdata->palanca;
		}
		if(isset($postdata->idFinca)){
			$id_finca = $postdata->idFinca;
		}
		if(isset($postdata->yearTendencia)){
			$year = $postdata->yearTendencia;
		}else{
			$year = date('Y');
        }

        $tendencia = $this->tendencia($palanca , $id_finca , $postdata);
        $response->tendencia = $tendencia->series;
		$response->tendencia_legends = $tendencia->legend;
        return $response;
    }

    public function index(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
		$palanca = "";
		if(isset($postdata->palanca)){
			$palanca = $postdata->palanca;
		}

		if(isset($postdata->idFinca)){
			$id_finca = $postdata->idFinca;
		}

		if(isset($postdata->idFincaDia)){
			$id_fincaDia = $postdata->idFincaDia;
		}

		if(isset($postdata->statusLbKg) && $postdata->statusLbKg == 1){
			$this->MermaTypePeso = "Kg";
		}else{
			$this->MermaTypePeso = "Lb";
		}

		if(isset($postdata->year)){
			$year = $postdata->year;
		}else{
			$year = 2016;
		}

		$response->id_company = (int)$this->session->id_company;
		$response->tags = $this->tags($postdata->fecha_inicial, $postdata->fecha_final , $palanca , $id_finca , $postdata);
		$response->tabla_lote_merma = $this->tabla_lote_merma($postdata->fecha_inicial, $postdata->fecha_final , $palanca , $id_finca , $postdata);
		// Cargamos la data una sola ves 
		$danos = $this->tabla_danos_merma($postdata->fecha_inicial, $postdata->fecha_final , $palanca , $id_finca , $postdata);
		// Cargamos la data del historico una sola ves 
		$historico = $this->grafica_historico($palanca , $id_finca , $year);
		// Cargamos la data de tendencia una sola ves 
		$tendencia = $this->tendencia($palanca , $id_finca , $postdata);
		// Cargamos la data Grafica Dia
		$dia = $this->grafica_dia($palanca , $id_finca , $id_fincaDia , $postdata);
		// Asignamos la data 'Tabla'
		$response->tabla_danos_merma = $danos->data;
		// Asignamos la data 'Tabla' Daños
		$response->tabla_danos_merma_danhos_merma = $danos->danhos_merma;
		// Asignamos la data 'Grafica de daños'
		$response->danos = $danos->grafica;
		// Asignamos la data 'Grafica del detalle de daños'
		$response->danos_detalle = $danos->grafica_detalle;
		// Asignamos la data 'Grafica del historico'
		$response->historico = $historico->data;
		// Asignamos la data 'Grafica del dia'
		$response->dia = $dia->data;
		// Agregamos el titulo de la 'Grafica del dia'
		$response->dia_title = $dia->title;
		// Agregamos el promedio del historico
		$response->historico_avg = $historico->avg;
		$response->historico_legends = $historico->legend;

		$response->tendencia = $tendencia->series;
		$response->tendencia_legends = $tendencia->legend;

		$response->palanca = ["" => "Todos"] + $this->getPalanca($postdata);
        $response->factor = (double)$this->factor;
        $response->years = $this->db->queryAllOne("SELECT YEAR(fecha) FROM merma GROUP BY YEAR(fecha)");

		return $response;
	}
	
	// % TALLO
	public function tablaVariables(){
		$postdata = (object)json_decode(file_get_contents("php://input"));
		$response = new stdClass;

		$array_str = explode("-", $postdata->fecha_inicial);
		$year=$array_str[0];
		// D($array_str);
		// E($year);

		$sql = "SELECT *
				FROM(
					SELECT
						semanas.semana,
						lotes.lote,
						tbl.cantidad
					FROM (
						SELECT semana
						FROM merma
						WHERE YEAR = {$year} AND porcentaje_merma > 0 AND bloque != '0'
						GROUP BY semana
					) semanas
					JOIN (
						SELECT bloque AS lote
						FROM merma
						WHERE YEAR = {$year} AND porcentaje_merma > 0 AND bloque != '0'
						GROUP BY lote
					) lotes
					LEFT JOIN (
						SELECT bloque AS lote, semana, ROUND(AVG(tallo/peso_racimos*100), 2) cantidad
						FROM merma 
						WHERE YEAR = {$year} AND porcentaje_merma > 0 AND bloque != '0'
						GROUP BY semana, bloque
					) tbl ON tbl.semana = semanas.semana AND tbl.lote = lotes.lote
					UNION ALL
					SELECT semana, 'PUBENZA' AS lote, ROUND(AVG(cantidad), 2) AS cantidad
					FROM (
						SELECT
							semanas.semana,
							lotes.lote,
							tbl.cantidad
						FROM (
							SELECT semana
							FROM merma
							WHERE YEAR = {$year} AND porcentaje_merma > 0 AND bloque != '0'
							GROUP BY semana
						) semanas
						JOIN (
							SELECT bloque AS lote
							FROM merma
							WHERE YEAR = {$year} AND porcentaje_merma > 0 AND bloque != '0'
							GROUP BY lote
						) lotes
						LEFT JOIN (
							SELECT bloque AS lote, semana, ROUND(AVG(tallo/peso_racimos*100), 2) cantidad
							FROM merma 
							WHERE YEAR = {$year} AND porcentaje_merma > 0 AND bloque != '0'
							GROUP BY semana, bloque
						) tbl ON tbl.semana = semanas.semana AND tbl.lote = lotes.lote
						ORDER BY lotes.lote, semanas.semana
					) tbl
					GROUP BY semana
				) tbl
				ORDER BY lote, semana";

		$response->data = $this->db->queryAll($sql);
		
		$data_chart = new stdClass;
		$data_table = [];
		$data_chart->data = [];
		$data_chart->legend = [];
		$umbral = ["count" => 0, "sum" => 0];
		foreach($response->data as $row){
			if(!isset($data_chart->data[$row->lote])){
				$data_chart->data[$row->lote] = [
					"connectNulls" => true,
					"data" => [],
					"name" => $row->lote,
					"type" => 'line',
					'itemStyle' => [
						"normal" => [
							"barBorderRadius" => "0",
							"barBorderWidth" => "6",
							"label" => [
								"show" => false,
								"position" => "insideTop"
							]
						]
					]
				];
			}

			if($row->lote == 'PUBENZA'){
				$umbral["count"] += 1;
				$umbral["sum"] += $row->cantidad;
			}
			$data_chart->data[$row->lote]["data"][] = $row->cantidad;
			if(!in_array($row->semana, $data_chart->legend)){
				$data_chart->legend[] = $row->semana;
			}

			if($row->lote != 'PUBENZA'){
				if(!in_array($row->lote, $data_table_lotes)){
					$data_table_lotes[] = $row->lote;
					$data_table[] = ["lote" => $row->lote];
				}
				$indexLote = array_search($row->lote, $data_table_lotes);
				$data_table[$indexLote]["sem_{$row->semana}"] = $row->cantidad;

				if($row->cantidad > 0){
					$data_table[$indexLote]["sum"] += $row->cantidad;
					$data_table[$indexLote]["count"] += 1;
					$data_table[$indexLote]["avg"] = round($data_table[$indexLote]["sum"] / $data_table[$indexLote]["count"], 2);
					if(!isset($data_table[$indexLote]["max"]) || $data_table[$indexLote]["max"] < $row->cantidad) $data_table[$indexLote]["max"] = $row->cantidad;
					if(!isset($data_table[$indexLote]["min"]) || $data_table[$indexLote]["min"] > $row->cantidad) $data_table[$indexLote]["min"] = $row->cantidad;
				}
			}
		}

		foreach($response->data as $row){
			if($row->lote == 'PUBENZA'){
				if(!in_array($row->lote, $data_table_lotes)){
					$data_table_lotes[] = $row->lote;
					$data_table[] = ["lote" => $row->lote];
				}
				$indexLote = array_search($row->lote, $data_table_lotes);
				$data_table[$indexLote]["sem_{$row->semana}"] = $row->cantidad;

				if($row->cantidad > 0){
					$data_table[$indexLote]["sum"] += $row->cantidad;
					$data_table[$indexLote]["count"] += 1;
					$data_table[$indexLote]["avg"] = round($data_table[$indexLote]["sum"] / $data_table[$indexLote]["count"], 2);
					if(!isset($data_table[$indexLote]["max"]) || $data_table[$indexLote]["max"] < $row->cantidad) $data_table[$indexLote]["max"] = $row->cantidad;
					if(!isset($data_table[$indexLote]["min"]) || $data_table[$indexLote]["min"] > $row->cantidad) $data_table[$indexLote]["min"] = $row->cantidad;
				}
			}
		}
		$response->umbral = round($umbral["sum"]/$umbral["count"], 2);
		$response->chart = $data_chart;
		$response->datatable = $data_table;
		return $response;
	}

    private function tags($inicio, $fin , $palanca , $id_finca , $filters = []){
		$sWhere = "";
		if($palanca != ""){
			$sWhere = " AND palanca = '{$palanca}'";
		}
		if($id_finca != ""){
			$sWhere .= " AND id_finca = {$id_finca}";
		}

		$sql = "SELECT IF(date_fecha > DATE('2016-09-13'), AVG(porcentaje_merma) , AVG((porcentaje_merma))) AS merma  FROM merma WHERE date_fecha BETWEEN '$inicio' AND '$fin' AND porcentaje_merma > 0 $sWhere";
		$rp1 = $this->db->queryRow($sql);

		$peso_neto = "peso_neto";
		$cajas = "(SUM(total_peso_merma) / $this->cajas) AS cajas";
		
		/*----------  CAMBIO DE PESO A LIBRA MARCEL  ----------*/
		if($this->MermaTypePeso == "Kg"){
			$cantidad = "(cantidad / 2.2)";
			$this->cajas = "18.86";
		}else{
			$cantidad = "(cantidad)";
			$this->cajas = "41.50";
		}
		/*----------  CAMBIO DE PESO A LIBRA MARCEL  ----------*/
		

		$total_merma = 0;
		$TagsCaja = (double)$this->db->queryOne("SELECT {$cajas} FROM merma WHERE date_fecha BETWEEN '$inicio' AND '$fin' {$sWhere}");

        $sql = "SELECT LOWER(type) AS id,ROUND(AVG(Total),2) AS label 
                FROM (
                    SELECT fecha  , bloque,merma_detalle.id_merma , type ,campo , ({$cantidad}) AS cantidad , peso_neto , (({$cantidad}) / peso_neto) * 100 as Total , porcentaje_merma 
                    FROM merma
                    INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
                    WHERE date_fecha BETWEEN '{$inicio}' AND '{$fin}' AND campo like '%Total Peso%' {$sWhere}
                    AND type NOT IN ('RESULTADOS','FIRMA ELECTRONICA','OTROS' ,'RECUSADOS')
                ) as detalle
                GROUP BY type";

        $data = $this->db->queryAllSpecial($sql);
        if(is_array($data)){
            $total_merma = array_sum($data);
            $data = (object)$data;
        }

        foreach ($data as $key => $value) {
            $data->{$key} = round((($data->{$key} * 100) / $total_merma), 2);
        }

        $tags = [];
        $tags = array(
            'merma' 		=> $total_merma,
            'enfunde' 		=> $data->enfunde,
            'campo' 		=> $data->campo,
            'cosecha' 		=> $data->cosecha,
            'animales' 		=> $data->animales,
            'hongos' 		=> $data->hongos,
            'empacadora' 	=> $data->empacadora,
            'fisiologicos' 	=> $data->fisiologicos,
            'cajas' 	  	=> $TagsCaja,
            'usd' 	  	  	=> ($TagsCaja * $this->usd) ,
        );
		

		return $tags;
	}

    private function getPalanca($filters = []){
		
		$inicio = $filters->fecha_inicial;
		$fin = $filters->fecha_final;
		$sWhere = "";
		if(isset($filters->idFinca) && $filters->idFinca != ""){
			// $sWhere .= " AND id_finca = {$filters->idFinca}";
			if($filters->idFinca == "9999999" && $this->session->id_company == 7){
				$sWhere .= " AND id_finca IN (2,3)";
			}else{
				$sWhere .= " AND id_finca = {$filters->idFinca}";
			}
		}
		$typeMerma = "";
		if($this->session->id_company == 6){
			$typeMerma = " AND tipo_merma = '{$filters->idMerma}'";
		}

		$response = new stdClass;
		$sql = "SELECT palanca AS id , palanca AS label  FROM merma
				WHERE date_fecha BETWEEN '$inicio' AND '$fin' AND palanca != '' $sWhere {$typeMerma}
				GROUP BY TRIM(palanca)";
		// D($sql);
		$response->data = [];
		$response->data = $this->db->queryAllSpecial($sql);
		return $response->data;
    }

    private function getDanhos($inicio ,$fin ,$bloque , $palanca = "", $id_finca = "" , $filters = []){
		$sWhere = "";
		if($palanca != ""){
			$sWhere = " AND palanca = '{$palanca}'";
		}
		if($id_finca != ""){
			$sWhere .= " AND id_finca = {$id_finca}";
		}

		$sql = "SELECT type AS id ,AVG(cantidad) AS label , SUM(cantidad) AS label2 
                FROM (
				    SELECT type, cantidad
                    FROM merma
				    INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
				    WHERE bloque = '{$bloque}' AND date_fecha BETWEEN '$inicio' AND '$fin' AND campo like '%{$this->search}%'
                        AND type NOT IN ('RESULTADOS','FIRMA ELECTRONICA','OTROS' ,'RECUSADOS') {$sWhere}
                ) as detalle
				GROUP BY type";
		$res = $this->db->queryAll($sql);
		$response = new stdClass;
		$response->danhos = [];
		$response->danhos_suma = [];
		foreach ($res as $key => $value) {
			$response->danhos[$value->id] = (double)$value->label;
			$response->danhos_suma[$value->id] = (double)$value->label2;
		}

		return $response;
    }
    
    private function getDanhosMerma($inicio ,$fin  , $palanca = "", $id_finca = "" , $filters = []){
		$sWhere = "";
		if($palanca != ""){
			$sWhere = " AND palanca = '{$palanca}'";
		}
		if($id_finca != ""){
			if($id_finca == "9999999" && $this->session->id_company == 7){
				$sWhere .= " AND id_finca IN (2,3)";
			}else{
				$sWhere .= " AND id_finca = {$id_finca}";
			}
		}

		$typeMerma = "";
		if($this->session->id_company == 6){
			$typeMerma = " AND tipo_merma = '{$filters->idMerma}'";
		}

		$sWhereConsulta = "AND campo like '%".$this->search."%'";
		if($this->session->id_company == 4 || $this->session->id_company == 9){
			$sWhereConsulta = "AND flag = 1";
		}

		if($this->session->id_company == 7){
			$sWhereConsulta = "AND campo like '%".$this->search."%' AND campo NOT LIKE '%Total Peso%'";
		}

		if($this->session->id_company == 3){
			$sWhereConsulta = "AND campo like '%Total Peso%'";
		}

		$sql = "SELECT type AS id ,AVG(cantidad) AS label FROM (
				SELECT merma_detalle.id,merma.id AS id_m , fecha  ,merma_detalle.id_merma , type ,campo , cantidad , peso_neto , (cantidad / peso_neto) * 100 as Total , porcentaje_merma  FROM merma
				INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
				WHERE date_fecha BETWEEN '$inicio' AND '$fin' {$sWhereConsulta} {$typeMerma}
				AND type NOT IN ('RESULTADOS','FIRMA ELECTRONICA','OTROS' ,'RECUSADOS') $sWhere ) as detalle
				GROUP BY type";
		//D($sql);
		$res = $this->db->queryAllSpecial($sql);

		return $res;
    }

    private function getDanhosMermaDetalle($inicio ,$fin , $type  , $palanca = "", $id_finca = "" , $filters = []){
		$sWhere = "";
		if($palanca != ""){
			$sWhere = " AND palanca = '{$palanca}'";
		}
		if($id_finca != ""){
			if($id_finca == "9999999" && $this->session->id_company == 7){
				$sWhere .= " AND id_finca IN (2,3)";
			}else{
				$sWhere .= " AND id_finca = {$id_finca}";
			}
		}

		$typeMerma = "";
		if($this->session->id_company == 6){
			$typeMerma = " AND tipo_merma = '{$filters->idMerma}'";
		}

		$condition = "AND flag = 2";
		if($this->session->id_company == 3){
			if($type == "LOTERO AEREO"){
				$condition = "AND flag = 3";
			}elseif($type == "COSECHA"){
				$condition = "AND (flag = 1 OR flag = 2)";
			}
		}

		$sql = "SELECT campo AS id ,AVG(cantidad) AS label , SUM(cantidad) AS label2 FROM (
				SELECT fecha  , bloque,merma_detalle.id_merma , type ,campo , cantidad , peso_neto , (cantidad / peso_neto) * 100 as Total , porcentaje_merma  FROM merma
				INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
				WHERE date_fecha BETWEEN '$inicio' AND '$fin' AND type = '{$type}' {$condition} {$typeMerma}
				AND type NOT IN ('RESULTADOS','FIRMA ELECTRONICA','OTROS' ,'RECUSADOS') $sWhere) as detalle
				GROUP BY type ,campo";
		#D($sql);
		$res = $this->db->queryAll($sql);
		$response = new stdClass;
		$response->danhos = [];
		$response->danhos_suma = [];
		foreach ($res as $key => $value) {
			$response->danhos[$value->id] = (double)$value->label;
			$response->danhos_suma[$value->id] = (double)$value->label2;
		}

		return $response;
	}
    
    private function tabla_danos_merma_detalle($inicio, $fin , $type , $palanca = "", $id_finca = "" , $filters = []){
		$sWhere = "";
		if($palanca != ""){
			$sWhere = " AND palanca = '{$palanca}'";
		}
		if($id_finca != ""){
			if($id_finca == "9999999" && $this->session->id_company == 7){
				$sWhere .= " AND id_finca IN (2,3)";
			}else{
				$sWhere .= " AND id_finca = {$id_finca}";
			}
		}

		$peso_total = "SUM(total_peso_merma) AS peso_total";
		$cajas = "(SUM(total_peso_merma) / $this->cajas) AS cajas";
		$usdCampo =  "((SUM(total_peso_merma) / $this->cajas) * $this->usd) AS usd";
		$racimos_lote = "racimos_procesados";
		if($this->session->id_company == 2){
			/*----------  CAMBIO DE PESO A LIBRA MARCEL  ----------*/
			$racimo = "racimo";
			if($this->MermaTypePeso == "Kg"){
				$cantidad = "(cantidad / 2.2)";
				$racimo = "(racimo / 2.2)";
				$this->cajas = "18.86";
			}else{
				$cantidad = "(cantidad)";
				$racimo = "racimo";
				$this->cajas = "41.50";
			}
			/*----------  CAMBIO DE PESO A LIBRA MARCEL  ----------*/
		}elseif($this->session->id_company == 7){
			$cantidad = "(cantidad)";
			$racimo = "(racimo)";
			$peso_total = "SUM(total_peso_merma - tallo) AS peso_total";
			$cajas = "(SUM(total_peso_merma - tallo) / $this->cajas) AS cajas";
			$usdCampo =  "((SUM(total_peso_merma - tallo) / $this->cajas) * $this->usd) AS usd";
			$racimos_lote = "racimos_procesados";
		}elseif($this->session->id_company == 4){
			if($this->MermaTypePeso == "Kg"){
				$cantidad = "(cantidad / 2.2)";
				$racimo = "(racimo / 2.2)";
			}else{
				$racimo = "(racimo)";
				$cantidad = "(cantidad)";
			}
		}elseif($this->session->id_company == 3){
			if($this->MermaTypePeso == "Kg"){
				$cantidad = "(cantidad / 2.2)";
				$racimo = "(racimo / 2.2)";
			}else{
				$racimo = "(racimo)";
				$cantidad = "(cantidad)";
			}
		}else{
				$racimo = "racimo";
				$cantidad = "(cantidad)";
		}


		$typeMerma = "";
		if($this->session->id_company == 6){
			$typeMerma = " AND tipo_merma = '{$filters->idMerma}'";
		}

		$condition = "AND flag = 1";
		if($this->session->id_company == 3){
			if($type == "LOTERO AEREO"){
				$condition = "AND flag = 3";
			}
		}

		$response = new stdClass;
		$subQuery = "(SELECT SUM(racimo) FROM merma WHERE date_fecha BETWEEN '$inicio' AND '$fin')";
		$sql = "SELECT campo AS type, campo , AVG(cantidad) AS danhos_peso, (((SUM(cantidad)) / ({$subQuery})) * 100) AS cantidad  , $cajas , 
					$peso_total , SUM($racimos_lote) AS racimos_lote , AVG($racimos_lote) AS racimos_lote_avg , $usdCampo
				FROM (
					SELECT fecha  , bloque,merma_detalle.id_merma , type ,REPLACE(campo,'Peso ' , '') AS campo , {$cantidad} AS cantidad , {$racimo} AS racimo,
						total_peso_merma , tallo , $racimos_lote
					FROM merma
					INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
					WHERE date_fecha BETWEEN '$inicio' AND '$fin' AND type = '{$type}' {$condition} {$typeMerma}
						AND type NOT IN ('RESULTADOS','FIRMA ELECTRONICA','OTROS' ,'RECUSADOS') $sWhere
					HAVING cantidad > 0
				) as detalle
				GROUP BY type,campo";
		$response->data = $this->db->queryAll($sql);
		
		$danhos = $this->getDanhosMermaDetalle($inicio, $fin , $type , $palanca , $id_finca , $filters);
		$response->danhos_merma = $danhos->danhos;
		$response->danhos_merma_sum = $danhos->danhos_suma;
		$response->grafica = $this->pie($response->data , '50%' ,"Detalle de Daños"  , "area" , "normal" , false , ['50%', '50%'] , 2);
		return $response;
	}

    private function tabla_lote_merma($inicio, $fin , $palanca , $id_finca , $filters = []){
		$sWhere = "";
		if($palanca != ""){
			$sWhere = " AND palanca = '{$palanca}'";
		}
		if($id_finca != ""){
			$sWhere .= " AND id_finca = {$id_finca}";
		}

		$data = [];
		$peso_total = "SUM(total_peso_merma) AS peso_total";
		$cajas = "(SUM(total_peso_merma) / $this->cajas) AS cajas";
		$peso = "cantidad";
        
        /*----------  CAMBIO DE PESO A LIBRA MARCEL  ----------*/
        if($this->MermaTypePeso == "Kg"){
            $total_peso_merma = "AVG(total_peso_merma / 2.2)";
			$this->cajas = "18.86";
			$peso = "cantidad/2.2";
        }else{
            $total_peso_merma = "AVG(total_peso_merma)";
			$this->cajas = "41.50";
			$peso = "cantidad";
        }

        $cajas = "(SUM(total_peso_merma) / $this->cajas) AS cajas";
        $racimos_lote = "racimos_procesados";
        $sql = "SELECT id , bloque AS lote, 
					{$total_peso_merma} AS total_peso_merma, 
					AVG(total_defectos) AS total_defectos, 
					SUM(total_defectos) AS total_defectos_sum, 
					AVG(porcentaje_merma) AS merma,
					SUM($racimos_lote) AS racimos_lote, 
					AVG($racimos_lote) AS racimos_lote_avg , 
					SUM(peso_neto) as peso_neto,
					(SELECT SUM(cantidad)
                            FROM merma 
                            INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
                            WHERE bloque = main.bloque 
                                AND date_fecha BETWEEN '$inicio' AND '$fin'
                                AND campo = 'Total Peso Merma (lb)' $sWhere {$typeMerma}) AS peso_total,
					{$cajas}
                FROM merma main
                WHERE date_fecha BETWEEN '$inicio' AND '$fin' AND porcentaje_merma > 0 $sWhere 
                GROUP BY bloque";
        /*----------  CAMBIO DE PESO A LIBRA MARCEL  ----------*/
        
        $res = $this->db->queryAll($sql);
		$danhos = [];
		foreach ($res as $key => $value) {
			$value = (object)$value;
			$value->merma = $value->peso_total / $value->peso_neto * 100;
			#$value->peso_total = $this->db->queryOne("SELECT SUM(cantidad) FROM merma INNER JOIN merma_detalle d ON id_merma = merma.id AND flag = 1 WHERE date_fecha BETWEEN '$inicio' AND '$fin' AND porcentaje_merma > 0 $sWhere AND bloque = '{$value->lote}'");
			$value->usd = (double)($value->cajas * $this->usd);
			if(is_numeric($value->lote)){
				$value->lote = (int)$value->lote;
			}else{
				$value->lote = $value->lote;
			}
			$danhos = $this->getDanhos($inicio, $fin , $value->lote , $palanca , $id_finca , $filters);
			$value->expanded = false;
			$value->detalle = $this->tabla_lote_merma_detalle($inicio, $fin , $value->lote , $palanca , $id_finca , $filters);
			$value->detalle_danhos = $danhos->danhos;
            $value->detalle_danhos_sum = $danhos->danhos_suma;
            
            if($this->session->id_company == 2){
                $value->total_peso_merma = 0;
                foreach($value->detalle as $row){
                    $value->total_peso_merma += $row->danhos_peso;
                    $value->dedos_prom_marcel += $row->dedos_prom_marcel;
                }
            }
		}
		return $res;
	}
    
    private function tabla_lote_merma_detalle($inicio ,$fin ,$bloque , $palanca = "", $id_finca = "", $filters = []){
		$sWhere = "";
		if($palanca != ""){
			$sWhere = " AND palanca = '{$palanca}'";
		}
		if($id_finca != ""){
			$sWhere .= " AND id_finca = {$id_finca}";
		}
		$data = [];
		$peso_neto = "peso_neto";
		$racimos_procesados = "racimos_procesados";
		$racimos_lote = "cantidad";
		$peso_total = "(cantidad)";
		$cajas = "((cantidad) / $this->cajas)";
        
        if($this->MermaTypePeso == "Kg"){
            $cantidad = "(cantidad / 2.2)";
            $cantidad_racimos = "(cantidad / 2.2)";
        }else{
            $cantidad = "(cantidad)";
            $cantidad_racimos = "(cantidad)";
        }
        /*----------  CAMBIO DE PESO A LIBRA MARCEL  ----------*/
        $racimos_lote = "racimos_procesados";

		$typeMerma = "";

		$sql = "SELECT 
					type, 
					bloque, 
					SUM(cantidad) AS danhos_peso, 
					SUM(total) AS cantidad, 
					AVG(porcentaje_merma), 
					racimos_lote, 
					AVG(racimos_lote) AS racimos_lote_avg, 
					SUM(total_peso) AS peso_total, 
					SUM(cajas) AS cajas
				FROM (
					SELECT 
						fecha, 
						bloque, 
						merma_detalle.id_merma, 
						SUM({$racimos_lote}) AS racimos_lote, 
						SUM($cajas) AS cajas, 
						{$racimos_procesados}, 
						type,
						campo, 
						SUM({$cantidad_racimos}) AS cantidad, 
						{$peso_neto}, 
						SUM(({$cantidad} / {$peso_neto}) * 100) as total, 
						porcentaje_merma,
						SUM({$peso_total}) AS total_peso
					FROM merma
					INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
					WHERE bloque = '{$bloque}' AND date_fecha BETWEEN '$inicio' AND '$fin' {$typeMerma}
						AND type NOT IN ('RESULTADOS','FIRMA ELECTRONICA','OTROS' ,'RECUSADOS', 'PESO RACIMOS') $sWhere 
						AND flag = 1
					GROUP BY type, campo
					HAVING cantidad > 0) as detalle
				GROUP BY type";

		$res = $this->db->queryAll($sql);
		foreach ($res as $key => $value) {
			#calcular porcentaje de merma marun y marlon
            $peso_neto = "peso_neto";

            $sql = "SELECT ROUND(AVG((porcentaje_peso_merma/{$peso_neto}*100)),2) AS porcentaje_merma
                    FROM (
                        SELECT bloque, merma.id, TYPE, porcentaje_merma, {$peso_neto}, SUM(cantidad) AS porcentaje_peso_merma
                        FROM merma
                        INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
                        WHERE bloque = '{$bloque}' AND date_fecha BETWEEN '$inicio' AND '$fin' AND flag = 1 AND type = '{$value->type}' {$sWhere}
                        AND TYPE NOT IN ('RESULTADOS','FIRMA ELECTRONICA','OTROS' ,'RECUSADOS', 'PESO RACIMOS') 
                        GROUP BY merma.id, TYPE
                    ) AS tbl
                    GROUP BY TYPE";
			$value->cantidad = $this->db->queryRow($sql)->porcentaje_merma;

			$value->expanded = false;
			$value->usd = (double)($value->cajas * $this->usd);
            $value->details = $this->tabla_lote_merma_detalle_details($inicio, $fin , $value->type, $palanca, $id_finca , $filters , $bloque, $value->danhos_peso, $value->racimos_lote, $value->cantidad, $cajas);
            foreach($value->details as $row){
                $value->dedos_prom_marcel += $row->dedos_prom_marcel;
            }
		}
		return $res;
	}

	private function tabla_lote_merma_detalle_details($inicio, $fin , $type , $palanca = "", $id_finca = "" , $filters = [] , $bloque, $_danhos_peso, 
		$racimos_procesados, $_cantidad){
		$sWhere = "";
		if($palanca != ""){
			$sWhere = " AND palanca = '{$palanca}'";
		}
		if($id_finca != ""){
			$sWhere .= " AND id_finca = {$id_finca}";
		}
		$cajas = "(SUM(cantidad) / $this->cajas) AS cajas";
		$peso_total = "SUM(cantidad) AS total_peso";

		$racimo = "racimo";
        if($this->MermaTypePeso == "Kg"){
            $cantidad = "(cantidad / 2.2)";
            $racimo = "(racimo / 2.2)";
        }else{
            $cantidad = "(cantidad)";
            $racimo = "racimo";
        }

		$condition = "AND flag = 1";
		$response = new stdClass;
		$sql = "SELECT campo AS type, campo,
                    AVG(cantidad) AS danhos_peso, 
                    AVG(total) AS cantidad,
                    SUM(cajas) AS cajas,
                    (SUM(cajas)*{$this->usd}) AS usd,
                    total_peso,
                    ROUND(AVG((cantidad / racimo * 100)),2) AS porcentaje_merma
				FROM (
					SELECT type, REPLACE(campo, 'Peso ', '') AS campo,
                            $peso_total, 
                            $cajas, 
                            SUM({$cantidad}) AS cantidad , 
                            SUM(((cantidad) / peso_neto) * 100) AS total,
                            racimo
					FROM merma
					INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma AND type = '{$type}' AND type NOT IN ('RESULTADOS','FIRMA ELECTRONICA','OTROS' ,'RECUSADOS') {$condition}
					WHERE date_fecha BETWEEN '$inicio' AND '$fin' AND bloque = '{$bloque}' $sWhere
					GROUP BY campo
					HAVING cantidad > 0
                ) as detalle
				GROUP BY campo";
		$response->data = $this->db->queryAll($sql);

		#calcular porcentaje de merma marun y marlon
        $peso_neto = "racimo";
        foreach($response->data as $value){
            /*$sql = "SELECT ROUND(AVG((porcentaje_peso_merma/{$peso_neto}*100)),2) AS porcentaje_merma
                    FROM (
                        SELECT bloque, merma.id, TYPE, porcentaje_merma, {$peso_neto}, SUM(cantidad) AS porcentaje_peso_merma, campo
                        FROM merma
                        INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
                        WHERE bloque = '{$bloque}' AND date_fecha BETWEEN '$inicio' AND '$fin' AND type = '{$type}' {$condition} {$typeMerma}
                        AND type NOT IN ('RESULTADOS','FIRMA ELECTRONICA','OTROS' ,'RECUSADOS') AND REPLACE(campo,'Peso ' , '') = '{$value->campo}' $sWhere
                        GROUP BY merma.id, TYPE, campo
                    ) AS tbl
                    GROUP BY TYPE, campo";
            $value->cantidad = $this->db->queryRow($sql)->porcentaje_merma;*/
            $value->dedos_prom_marcel = $this->db->queryOne("SELECT SUM(cantidad) / SUM((SELECT COUNT(1) FROM merma_detalle WHERE id_merma = m.id_merma AND cantidad > 0 AND flag = 3)) AS dedos_prom
                    FROM merma
                    INNER JOIN merma_detalle m ON merma.id = id_merma
                    WHERE fecha BETWEEN '$inicio' AND '$fin' AND type = '{$type}' AND campo = '{$value->campo}' AND flag = 2 AND cantidad > 0");
        }
		return $response->data;
    }
    
    private function tabla_danos_merma($inicio, $fin , $palanca , $id_finca = "" , $filters = []){
		$sWhere = "";
		if($palanca != ""){
			$sWhere = " AND palanca = '{$palanca}'";
		}
		if($id_finca != ""){
			if($id_finca == "9999999" && $this->session->id_company == 7){
				$sWhere .= " AND id_finca IN (2,3)";
			}else{
				$sWhere .= " AND id_finca = {$id_finca}";
			}
		}
		$response = new stdClass;
		$racimo = "racimo";
		$peso_total = "SUM(total_peso_merma) AS peso_total";
		$cajas = "(SUM(total_peso_merma) / $this->cajas) AS cajas";
		$racimos_lote = "racimos_procesados";
		if($this->session->id_company == 2){
			/*----------  CAMBIO DE PESO A LIBRA MARCEL  ----------*/
			if($this->MermaTypePeso == "Kg"){
				$cantidad = "(cantidad / 2.2)";
				$racimo = "(racimo / 2.2)";
				$this->cajas = "18.86";
			}else{
				$cantidad = "(cantidad)";
				$racimo = "(racimo)";
				$this->cajas = "41.5";
			}
			/*----------  CAMBIO DE PESO A LIBRA MARCEL  ----------*/
			$cajas = "(SUM(total_peso_merma) / $this->cajas) AS cajas";
		}elseif($this->session->id_company == 7){
			$cantidad = "(cantidad)";
			$racimo = "(racimo)";
			$peso_total = "SUM(total_peso_merma - tallo) AS peso_total";
			$cajas = "(SUM(total_peso_merma - tallo) / $this->cajas) AS cajas";
			$racimos_lote = "racimos_procesados";
		}elseif($this->session->id_company == 4){
			$peso_total = "SUM(peso_neto) AS peso_total";

			if($this->MermaTypePeso == "Kg"){
				$cantidad = "(cantidad / 2.2)";
				$racimo = "(racimo / 2.2)";
			}else{
				$cantidad = "(cantidad)";
				$racimo = "(racimo)";
			}
		}elseif($this->session->id_company == 3){
			$peso_total = "SUM(peso_neto) AS peso_total";

			if($this->MermaTypePeso == "Kg"){
				$cantidad = "(cantidad / 2.2)";
				$racimo = "(racimo / 2.2)";
			}else{
				$racimo = "(racimo)";
				$cantidad = "(cantidad)";
			}
		}else{
				$cantidad = "(cantidad)";
				$racimo = "(racimo)";
		}

		$typeMerma = "";
		if($this->session->id_company == 6){
			$typeMerma = " AND tipo_merma = '{$filters->idMerma}'";

			$peso_total = "SUM(peso_neto) AS peso_total";

			if($this->MermaTypePeso == "Kg"){
				$cantidad = "(cantidad / 2.2)";
				$racimo = "(racimo / 2.2)";
			}else{
				$cantidad = "(cantidad)";
				$racimo = "(racimo)";
			}
		}

		// $sql ="SELECT type , bloque ,AVG(cantidad) AS danhos_peso, ROUND(AVG(Total),2) AS cantidad , AVG(porcentaje_merma) FROM (
		// 		SELECT fecha  , bloque,merma_detalle.id_merma , type ,campo , {$cantidad} AS cantidad , {$peso_neto} , ({$cantidad} / {$peso_neto}) * 100 as Total , porcentaje_merma  FROM merma
		// 		INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
		// 		WHERE date_fecha BETWEEN '$inicio' AND '$fin' AND campo like '%Total Peso%' {$typeMerma}
		// 		AND type NOT IN ('RESULTADOS','FIRMA ELECTRONICA','OTROS' ,'RECUSADOS') $sWhere ) as detalle
		// 		GROUP BY type";
		$subQuery = "(SELECT SUM(racimo) FROM merma WHERE date_fecha BETWEEN '$inicio' AND '$fin')";
		$sql ="SELECT type ,AVG(cantidad) AS danhos_peso ,AVG(total_defectos) AS total_defectos,  SUM(total_defectos) AS total_defectos_sum , $peso_total,
			SUM($racimos_lote) AS racimos_lote , AVG($racimos_lote) AS racimos_lote_avg,
			ROUND((ROUND(SUM(cantidad),2) / ROUND({$subQuery},2)) * 100,2) AS cantidad  , {$cajas}
			FROM (
			SELECT fecha  , bloque,merma_detalle.id_merma , type ,campo , {$cantidad} AS cantidad , peso_neto, total_defectos , total_peso_merma , tallo , {$racimos_lote}
			FROM merma
			INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
			WHERE date_fecha BETWEEN '$inicio' AND '$fin' AND campo like '%Total Peso%' {$typeMerma}
			AND type NOT IN ('RESULTADOS','FIRMA ELECTRONICA','OTROS' ,'RECUSADOS') $sWhere 
			HAVING cantidad > 0) as detalle
			GROUP BY type";

		$response->data = $this->db->queryAll($sql);
		$response->grafica = $this->pie($response->data , '50%' ,"Daños"  , "area" , "normal" , false , ['50%', '50%'] , 2);
		$response->grafica_detalle = [];
		$detalle = "";
		foreach ($response->data as $key => $value) {
			$value = (object)$value;
			$value->usd = (double)($value->cajas * $this->usd);
			$detalle = $this->tabla_danos_merma_detalle($inicio, $fin , $value->type, $palanca, $id_finca , $filters);
			$value->detalle = $detalle->data;
			$value->detalle_danhos = $detalle->danhos_merma;
			$value->detalle_danhos_sum = $detalle->danhos_merma_sum;
			$response->grafica_detalle[$value->type] = $detalle->grafica;
		}
		$response->danhos_merma = $this->getDanhosMerma($inicio, $fin , $palanca , $id_finca , $filters);
		return $response;
	}

    public function weeksHistories(){
        $response = new stdClass;
		$postdata = (object)json_decode(file_get_contents("php://input"));
		// F($postdata);

		$filters = (object)[
			"type" => ["ENFUNDE" , "CAMPO" , "COSECHA" , "ANIMALES" , "HONGOS" , "EMPACADORA" , "FISIOLOGICOS"],
			"mode" => getValueFrom($postdata , "mode" , "Peso" , FILTER_SANITIZE_STRING),
			"idFinca" => getValueFrom($postdata , "idFinca" , 1 , FILTER_SANITIZE_PHRAPI_INT),
			"year" => getValueFrom($postdata , "year" , date('Y') , FILTER_SANITIZE_PHRAPI_INT),
			"palanca" => getValueFrom($postdata , "palanca" , "" , FILTER_SANITIZE_STRING),
		];

		if($this->MermaTypePeso == "Kg"){
			$cantidad = "(cantidad / 2.2)";
		}else{
			$cantidad = "(cantidad)";
		}

		$peso_neto = "peso_neto";
        if($filters->mode == "Total Peso"){

			$modeCampo = "SUM(cantidad)";

		}elseif($filters->mode == "Prom peso-viaje"){

			$modeCampo = "SUM(cantidad) AS cantidad";

		}elseif($filters->mode == "Prom peso-racimo"){

			$modeCampo = "SUM(cantidad / racimos_procesados) AS cantidad";

		}elseif($filters->mode == "Merma"){

			$modeCampo = "ROUND(SUM(Total),2)";

		}elseif($filters->mode == "Total Danos"){

			$filters->type = ["ENFUNDE" , "COSECHA"];
			$modeCampo = "SUM(cantidad) AS cantidad";
			$this->string = "Total Daños";

		}elseif($filters->mode == "Prom dano-viaje"){

			$filters->type = ["ENFUNDE" , "COSECHA"];
			$modeCampo = "SUM(cantidad) AS cantidad";
			$this->string = "Total Daños";

		}elseif($filters->mode == "Prom dano-racimo"){

			$filters->type = ["ENFUNDE" , "COSECHA"];
			$modeCampo = "SUM(cantidad / racimos_procesados) AS cantidad";
			$this->string = "Total Daños";
		}

		$sql_config = "SELECT umbral , bono , descuento FROM bonificacion_conts WHERE tipo = '{$filters->type}' AND mode = '{$filters->mode}'";
		$_config = $this->db->queryRow($sql_config);
		$umbral = (double)$_config->umbral;
		$valor_descuento = (double)$_config->descuento;
		$bono = (double)$_config->bono;

        $sql = "SELECT semanas.semana AS label , bloque AS legend ,
					IF(bloque != 127 ,true,false) AS selected,
					0 AS cantidad 
				FROM (
					SELECT semana
					FROM merma 
					WHERE semana > 0 
						AND year = {$filters->year} 
					GROUP BY semana
					ORDER BY semana
				) AS semanas, merma AS m
				WHERE year = {$filters->year} {$sFinca}
					AND bloque != 127 AND bloque != 0
                GROUP BY bloque, semanas.semana";
        $variable = $this->db->queryAll($sql);

        // 24/06/2017 - TAG: NUEVA FORMULA
        $response->historico_tabla = [];
        $valor = 0;
        $sumCategories = [];
        $countCategories = [];
        // 24/06/2017 - TAG: NUEVA FORMULA
		$weeks = (array)$this->db->queryAllSpecial("SELECT getWeek(fecha) AS id , getWeek(fecha) AS label FROM merma WHERE YEAR(fecha) = {$filters->year} GROUP BY getWeek(fecha)");
		foreach ($filters->type as $llave => $category) {
	        foreach ($variable as $key => $value) {
				$response->historico_tabla[$category]["expanded"] = false;
				$response->historico_tabla[$category]["category"] = $category;
				$sql ="SELECT $modeCampo FROM (
							SELECT fecha, bloque,merma_detalle.id_merma , type ,campo , ({$cantidad}) AS cantidad , {$peso_neto} , (({$cantidad}) / {$peso_neto}) * 100 as Total , porcentaje_merma, racimos_procesados
							FROM merma
							INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
							WHERE semana = {$value->label} 
                                AND year = {$filters->year} 
                                AND type = '{$category}' 
                                AND campo like '%$this->string%' 
                                AND bloque = '{$value->legend}'
							    AND bloque != 127 
							    AND bloque != 0 
							    AND type NOT IN ('RESULTADOS','FIRMA ELECTRONICA','OTROS' ,'RECUSADOS') {$sFinca}
                        ) as detalle";

				$valor = (double)$this->db->queryOne($sql);

				// D($valor);
				$response->historico_tabla[$category]["lotes"][$value->legend]["lote"] = $value->legend;
				$response->historico_tabla[$category]["lotes"][$value->legend]["expanded"] = false;
				$response->historico_tabla[$category]["lotes"][$value->legend]["cantidad"][$value->label] = $valor;
				if($this->session->id_company == 2){
					if($filters->type == "COSECHA"){
						$response->historico_tabla[$category]["lotes"][$value->legend]["umbral"][$value->label] = (double)$umbral;
					}else{
						$response->historico_tabla[$category]["lotes"][$value->legend]["umbral"][$value->label] = $this->db->queryRow("SELECT IFNULL(umbral, 0) as umbral FROM bonificacion_config WHERE lote = '{$value->legend}' AND semana <= '{$value->label}' AND umbral > 0 ORDER BY semana DESC LIMIT 1")->umbral;
					}
				}
				// if($this->session->id_company == 7){
				//     $response->historico_tabla[$value->legend][$valor]["umbral"][$value->label] = (double)$umbral;
				// }
				if($valor > 0){
					$count[$value->legend]++;
					$sum[$value->legend] += $valor;

					// 26/06/2017 - TAG: HISTORICO SEMANAL
					$countCategories[$category][$value->label]++;
					$sumCategories[$category][$value->label] += $valor;
					// 26/06/2017 - TAG: HISTORICO SEMANAL

					$response->historico_tabla[$category]["lotes"][$value->legend]["total"] = ROUND(($sum[$value->legend] / $count[$value->legend]) , 2);
					if($this->session->id_company == 2){
						if($filters->type == "COSECHA"){
							$response->historico_tabla[$category]["lotes"][$value->legend]["umbral"][$value->label] = (double)$umbral;
						}else{
							$response->historico_tabla[$category]["lotes"][$value->legend]["umbral_total"] = $this->db->queryRow("SELECT IFNULL(umbral, 0) as umbral FROM bonificacion_config WHERE lote = '{$value->legend}' AND umbral > 0 ORDER BY semana DESC LIMIT 1")->umbral;
						}
					}
					if($this->session->id_company == 7){
						$response->historico_tabla[$category]["lotes"][$value->legend]["umbral_total"] = (double)$umbral;
					}
					// $response->historico_tabla[$value->legend]["total"] = ROUND(($sum[$value->legend] / $count[$value->legend]) , 2);
				}
				$response->totales_historico[$category]["cantidad"][$value->label]  += $valor;
				// $response->historico_tabla[$category]["totales"][$value->label]  = round(($sum[$value->legend] / $count[$value->legend]) , 2);
				$response->historico_tabla[$category]["totales"][$value->label]  += $valor;
				// D($sumCategories[$category][$value->label]);
				// D($countCategories[$category][$value->label]);
				if($filters->mode == "Total Peso" || $filters->mode == "Total Danos"){
					$response->historico_tabla[$category]["totales"][$value->label]  = $sumCategories[$category][$value->label];
				}else{
					$response->historico_tabla[$category]["totales"][$value->label]  = $sumCategories[$category][$value->label] / $countCategories[$category][$value->label];
				}
				if($this->session->id_company == 2){
					$response->totales_historico[$category]["umbral"][$value->label] = $this->db->queryRow("SELECT IFNULL(umbral, 0) as umbral FROM bonificacion_config WHERE lote = '{$value->legend}' AND semana <= '{$value->label}' AND umbral > 0 ORDER BY semana DESC LIMIT 1")->umbral;
					// $response->historico_tabla[$category]["totales"][$value->label] = $response->totales_historico[$category]["umbral"][$value->label];
				}
				if($this->session->id_company == 7){
					$response->totales_historico[$category]["umbral"][$value->label] = (double)$umbral;
				}
				$response->totales_historico[$category]["total"]  += $valor;
				// $response->historico_tabla[$category]["totales"]  += $valor;
			}
        }
		// 24/06/2017 - TAG: NUEVA FORMULA
		foreach ($filters->type as $llave => $category) {
			foreach ($response->historico_tabla[$category]["lotes"] as $key => $value) {
				$response->historico_tabla[$category]["lotes"][$key]["details"] = $this->getDefectos($key , 0 , $category , $filters->idFinca  , $filters->mode , "weeks" , $filters ,$response->historico_tabla[$category]["lotes"] , 'weeks');
			}
		}
		// 24/06/2017 - TAG: NUEVA FORMULA



		$semanas_historico = (array)$this->db->queryAllSpecial("SELECT getWeek(fecha) AS id , getWeek(fecha) AS label FROM merma WHERE YEAR(fecha) = {$filters->year} GROUP BY getWeek(fecha)");
		
		foreach ($semanas_historico as $key => $value) {
			$response->semanas_historico[] = (double)$value;
		}

        return $response;
    }

	private function getDefectos($lote , $semana , $type = "ENFUNDE" , $idFinca = 1 , $mode = "peso" , $modePrint = "days" , $filters , $promedios = [] , $origin = 'date'){
		$response = new stdClass;
		$sWhere = "AND bloque = '{$lote}'";
		$cantidad  = "SUM(cantidad)";
		$sFincas = " AND id_finca = '{$idFinca}'";
		$peso_neto = "peso_neto";
		$groupBy = "GROUP BY DAY(fecha),campo";
		$orderBy = "ORDER BY DAY(fecha),campo";
		$campo = "DATE(fecha)";
		$modeWhere = " getWeek(fecha) = {$semana}  AND";
		$conditions = "";
		$sum = [];
		$count = [];

		if($modePrint == "weeks"){	
			$campo = "getWeek(fecha)";
			$modeWhere = "";
			$groupBy = "GROUP BY getWeek(fecha),campo";
			$orderBy = "ORDER BY getWeek(fecha),campo";
		}

		if($this->session->id_company == 7){
			$peso_neto = "(racimo - tallo)";
			$cantidad = "SUM((cantidad / racimos_procesados))";
			if($type == "ENFUNDE"){
				$type = "LOTERO AEREO";
			}
			if($idFinca == 9999999){
				$sFincas = " AND id_finca IN(1,2)";
			}
		}

		// if($type == "COSECHA"){
		// 	$sWhere = "AND palanca = '$lote'";
		// 	if($lote == "S/PALANCA"){
		// 		$sWhere = "AND palanca IS NULL";
		// 	}
		// }

		$conditions = "";
		$tipoConsulta = "AND campo LIKE '%Peso%'";
		if($mode == "Total Peso"){
			$cantidad = "SUM(cantidad)";
			/*----------  CAMBIO DE PESO A LIBRA MARCEL  ----------*/
			if($this->MermaTypePeso == "Kg"){
				$cantidad = "SUM(cantidad / 2.2)";
			}else{
				$cantidad = "SUM(cantidad)";
			}
			/*----------  CAMBIO DE PESO A LIBRA MARCEL  ----------*/
			$modeCampo = "SUM(cantidad)";
		}
		if($mode == "Prom peso-viaje"){
			$cantidad = "SUM(cantidad)";
			/*----------  CAMBIO DE PESO A LIBRA MARCEL  ----------*/
			if($this->MermaTypePeso == "Kg"){
				$cantidad = "SUM(cantidad / 2.2)";
			}else{
				$cantidad = "SUM(cantidad)";
			}
			/*----------  CAMBIO DE PESO A LIBRA MARCEL  ----------*/
			$modeCampo = "SUM(cantidad)";
		}
		if($mode == "Prom peso-racimo"){
			$cantidad = "SUM(cantidad)";
			/*----------  CAMBIO DE PESO A LIBRA MARCEL  ----------*/
			if($this->MermaTypePeso == "Kg"){
				$cantidad = "SUM(cantidad / 2.2)";
			}else{
				$cantidad = "SUM(cantidad)";
			}
			/*----------  CAMBIO DE PESO A LIBRA MARCEL  ----------*/
			$modeCampo = "SUM(cantidad / racimos_procesados)";
		}
		if($mode == "Merma"){
			$cantidad = "SUM(cantidad)";
			$modeCampo = "SUM(Total)";
			if($this->session->id_company == 2){
				/*----------  CAMBIO DE PESO A LIBRA MARCEL  ----------*/
				if($this->MermaTypePeso == "Kg"){
					$cantidad = "SUM(cantidad / 2.2)";
				}else{
					$cantidad = "SUM(cantidad)";
				}
				/*----------  CAMBIO DE PESO A LIBRA MARCEL  ----------*/
			}elseif($this->session->id_company == 7){
				$peso_neto = "(racimo - tallo)";
				// $conditions = " AND campo LIKE '%(# Daños)%' AND campo NOT LIKE '%Total Merma%'";
			}else{
				$cantidad = "SUM(cantidad)";
			}
		}
		if($mode == "Total Danos"){
			$tipoConsulta = "AND campo NOT LIKE '%Peso%'";
			if($this->MermaTypePeso == "Kg"){
				$cantidad = "SUM(cantidad / 2.2)";
			}else{
				$cantidad = "SUM(cantidad)";
			}

			$modeCampo = "SUM(cantidad)";
		}
		if($filters->mode == "Prom dano-viaje"){
			$tipoConsulta = "AND campo NOT LIKE '%Peso%'";
			if($this->MermaTypePeso == "Kg"){
				$cantidad = "SUM(cantidad / 2.2)";
			}else{
				$cantidad = "SUM(cantidad)";
			}
			$modeCampo = "SUM(cantidad)";
		}
		if($filters->mode == "Prom dano-racimo"){
			$tipoConsulta = "AND campo NOT LIKE '%Peso%'";
			if($this->MermaTypePeso == "Kg"){
				$cantidad = "SUM(cantidad / 2.2)";
			}else{
				$cantidad = "SUM(cantidad)";
			}
			$modeCampo = "SUM(cantidad / racimos_procesados)";
		}

		$modes = ["Prom dano-racimo" ,"Prom dano-viaje" , "Prom peso-racimo" , "Merma" , "Prom peso-viaje"];
		// $sql = "SELECT 
		// 	DATE(fecha) AS fecha,
		// 	campo,
		// 	SUM(cantidad) AS cantidad
		// 	FROM merma AS m
		// 	INNER JOIN merma_detalle AS md ON m.id = md.id_merma
		// 	WHERE getWeek(fecha) = {$semana} {$sWhere}
		// 	AND type = '{$type}' AND bloque != 127 AND bloque != 0
		// 	AND campo NOT LIKE '%Peso%' 
		// 	AND campo NOT LIKE '%Total Peso%' 
		// 	AND campo NOT LIKE '%Total Daños%'
		// 	AND campo != 'Observaciones'
		// 	{$sFincas}
		// 	AND type NOT IN ('RESULTADOS','FIRMA ELECTRONICA','OTROS','RECUSADOS') 		
		// 	GROUP BY DAY(fecha),campo
		// 	HAVING cantidad > 0
		// 	ORDER BY fecha , campo";
		$sql = "SELECT 
			{$campo} AS fecha,
			campo,
			{$modeCampo} AS cantidad,
			SUM(sumCantidad) AS sumCantidad
			FROM (
			SELECT fecha  , palanca , id_finca , 
			bloque,merma_detalle.id_merma , type , cantidad AS sumCantidad,campo , ROUND({$cantidad},2) AS cantidad , $peso_neto AS peso_neto , ({$cantidad} / {$peso_neto}) * 100 as Total ,
			porcentaje_merma , racimos_procesados  FROM merma
			INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
			WHERE {$modeWhere} type = '{$type}'  {$sWhere} 
			{$tipoConsulta} 
			AND campo NOT LIKE '%Total Peso%' 
			AND campo NOT LIKE '%Total Daños%'
			AND campo != 'Observaciones' AND YEAR(fecha) = {$filters->year}
			{$sFincas} AND bloque != 127 AND bloque != 0 AND cantidad > 0
			AND type NOT IN ('RESULTADOS','FIRMA ELECTRONICA','OTROS' ,'RECUSADOS')
			{$conditions}
			GROUP BY id_merma,campo) AS detalle
			{$groupBy}
			{$orderBy}";
			// if($type == "COSECHA"){
				// D($sql);
			// }
		$data = $this->db->queryAll($sql);
		$totales = [];

		$sql_totales ="SELECT 
				fecha AS id , SUM(sumCantidad) AS label  
				FROM (
					$sql
				)AS finally
				GROUP BY fecha";
		// D($sql_totales);
		$totales = $this->db->queryAllSpecial($sql_totales);
		$response->data = [];
		$subTotal = 0;
		$Total = 0;
		// D($promedios);
		foreach ($data as $key => $value) {
			$value->campo = trim($value->campo);
			$response->data[$value->campo]["defecto"] = $value->campo;
			$response->data[$value->campo]["valor"][$value->fecha] = (double)$value->cantidad;
			if(in_array( $filters->mode , $modes)){
				// 23/06/2017 - TAG: NUEVA FORMULA
				$value->sumCantidad = (double)$value->sumCantidad;
				$totales[$value->fecha] = (double)$totales[$value->fecha];
				$subTotal = (double)((($value->sumCantidad / $totales[$value->fecha]) * 100) / 100);
				if($origin == 'date'){
					$promedio = (double)$promedios[$lote]["cantidad"][$value->fecha];
				}else{
					$promedio = (double)$promedios[$lote]["cantidad"][$value->fecha];
				}
				// D($promedios[$lote]);
				// D($promedio);
				$Total = (double)($promedio * $subTotal);

				$value->cantidad = $Total;
				$response->data[$value->campo]["valor"][$value->fecha] = (double)$value->cantidad;
				// 23/06/2017 - TAG: NUEVA FORMULA
				$count[$value->campo]++;
				$sum[$value->campo] += (double)$value->cantidad;
				// D(ROUND(($sum[$value->fecha] / $count[$value->fecha]) , 2));
				$response->data[$value->campo]["total"] = ROUND(($sum[$value->campo] / $count[$value->campo]) , 2);
			}else{
				$response->data[$value->campo]["total"] += (double)$value->cantidad;
			}
			
		}
		return $response->data;
    }

    private function grafica_historico($palanca = "", $id_finca = "" , $year = "YEAR(CURRENT_DATE)" ){
		$sWhere = "";
		$sWhere_PromFincas = "";
		if($palanca != ""){
			$sWhere = " AND palanca = '{$palanca}'";
		}
		if($id_finca != ""){
			if($id_finca == "9999999" && $this->session->id_company == 7){
				$sWhere .= " AND id_finca IN (2,3)";
			}else{
				$sWhere .= " AND id_finca = {$id_finca}";
			}
		}

		$sWhere .= " AND YEAR(fecha) = {$year}";
		$Umbral = 2;

		if($this->session->id_company == 2){
			/*----------  CAMBIO DE PESO A LIBRA MARCEL  ----------*/
			if($this->MermaTypePeso == "Kg"){
				$total_peso_merma = "(total_peso_merma / 2.2)";
			}else{
				$total_peso_merma = "(total_peso_merma)";
			}
			/*----------  CAMBIO DE PESO A LIBRA MARCEL  ----------*/
		}else{
				$total_peso_merma = "(total_peso_merma)";
		}

		$sql = "SELECT TRIM(finca) AS finca ,id_finca, getWeek(fecha) AS fecha, IF(date_fecha > DATE('2016-09-13'), AVG(porcentaje_merma) , AVG((porcentaje_merma))) AS porcentaje_merma, AVG({$total_peso_merma}) AS total_peso_merma
				FROM merma
				WHERE porcentaje_merma > 0 $sWhere
				GROUP BY id_finca , getWeek(fecha)
				ORDER BY id_finca , getWeek(fecha)";

		$response = new stdClass;
		$res = $this->db->queryAll($sql);
		$response->data = [];
		$response->legend = [];
		$response->avg = 0;
		$count = 0;
		$sum = 0;
		$series = new stdClass;
		$finca = "";
		$flag_count = 0;
		$markLine = new stdClass;
		foreach ($res as $key => $value) {
			$value = (object)$value;
			$value->porcentaje_merma = (float)$value->porcentaje_merma;
			$sum += $value->porcentaje_merma;
			$response->legend[(int)$value->fecha] = (int)$value->fecha;
			if($finca != $value->id_finca){
				$series = new stdClass;
				$finca = $value->id_finca;
				$series->name = $value->finca;
				$series->type = "line";
				$series->data = [];

				if($this->session->id_company == 7)
					$series->data[$value->fecha] = round($value->porcentaje_merma ,2);
				else
					$series->data[] = round($value->porcentaje_merma ,2);

				$series->itemStyle = (object)[
					"normal" => [
						// "color" => '#007537',
						// "barBorderColor" => '#007537',
						"barBorderWidth" => 6,
						"barBorderRadius" => 0,
						"label" => [ "show" => false , "position" => "insideTop"]
					]
				];
				if($flag_count == 0){
					$series->markLine->data = [[ "name" => "Umbral", "label" => 'Umbral', "value" => 2, "xAxis" => -1, "yAxis" => 2 ]];
					$flag_count++;
				}
				
				$response->data[$value->id_finca] = $series;
			}else{
				if($finca == $value->id_finca){
					if($this->session->id_company == 7)
						$response->data[$value->id_finca]->data[$value->fecha] = round($value->porcentaje_merma ,2);
					else
						$response->data[$value->id_finca]->data[] = round($value->porcentaje_merma ,2);
				}
			}
			$count++;
		}

		$response->avg = ($sum / $count);

		return $response; 
    }

    private function grafica_dia($palanca = "", $id_finca = ""  , $id_fincaDia = "" , $filters = []){
		$sWhere = "";
		$fincaDia = 0;
		if($palanca != ""){
			$sWhere = " AND palanca = '{$palanca}'";
		}
		if($filters->fecha_inicial != '' && $filters->fecha_final != ''){
            $sWhere .= " AND date_fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}'";
        }
		if($id_finca != ""){
			if($id_finca == "9999999" && $this->session->id_company == 7){
				$sWhere .= " AND id_finca IN (2,3)";
			}else{
				$sWhere .= " AND id_finca = {$id_finca}";
			}
		}
		if($id_fincaDia != ""){
			$fincaDia = $id_fincaDia;
		}

		if($this->session->id_company == 2){
			/*----------  CAMBIO DE PESO A LIBRA MARCEL  ----------*/
			if($this->MermaTypePeso == "Kg"){
				$total_peso_merma = "(total_peso_merma / 2.2)";
			}else{
				$total_peso_merma = "(total_peso_merma)";
			}
			/*----------  CAMBIO DE PESO A LIBRA MARCEL  ----------*/
		}else{
				$total_peso_merma = "(total_peso_merma)";
		}


		$typeMerma = "";
		if($this->session->id_company == 6){
			$typeMerma = " AND tipo_merma = '{$filters->idMerma}'";
		}

		$response = new stdClass;
		$sql = "SELECT id , id_finca,TIME_FORMAT(TIME(fecha) , '%H:%i') AS time ,date_fecha as fecha ,IF(date_fecha > DATE('2016-09-13'), AVG(porcentaje_merma) , AVG((porcentaje_merma))) AS porcentaje_merma, AVG({$total_peso_merma}) AS total_peso_merma,
				CONCAT_WS(' ','Última Actualización : ' , date_fecha) as title FROM merma
				WHERE 1 = 1 $sWhere {$typeMerma}
				GROUP BY id
				ORDER BY fecha desc";
		// D($sql);
		$res = $this->db->queryAll($sql);
		$count = 1;
		$fecha = "";
		$flag = 0;
		$response->data = new stdClass;
		$response->data->data = [];
		$response->data->label = [];
		$response->data->series = [];
		$response->title = "";
		$label = "";
		foreach ($res as $key => $value) {
			$value = (object)$value;
			if($value->fecha != $fecha){
				$fecha = $value->fecha;
				$flag++;
				if($flag == 2){
					break;
				}
			}

			if($fincaDia > 0){
				if($value->id_finca == $fincaDia){
					$response->title = $value->title;
					$value->porcentaje_merma = (float)$value->porcentaje_merma;
					if($value->time == '00:00'){
						$label = "Hora ".$count;
					}else{
						$label = $value->time;
					}
					$response->data->label[$value->fecha." ".$label] = $label;
					$response->data->series[$value->fecha." ".$label] = $value->porcentaje_merma;
					$response->data->data[$label] = $value;
					$count++;
				}
			}else{
				$response->title = $value->title;
				$value->porcentaje_merma = (float)$value->porcentaje_merma;
				if($value->time == '00:00'){
					$label = "Hora ".$count;
				}else{
					$label = $value->time;
				}
				$response->data->label[$value->fecha." ".$label] = $label;
				$response->data->series[$value->fecha." ".$label] = $value->porcentaje_merma;
				$response->data->data[$label] = $value;
				$count++;
			}
		}
		return $response; 
	}

    private function tendencia($palanca , $id_finca , $filters = []){
		$sWhere = "";
		if($palanca != ""){
			$sWhere = " AND palanca = '{$palanca}'";
		}
		if($id_finca != ""){
			if($id_finca == "9999999" && $this->session->id_company == 7){
				$sWhere .= " AND id_finca IN (2,3)";
			}else{
				$sWhere .= " AND id_finca = {$id_finca}";
			}
		}
		if(isset($filters->yearTendencia) && $filters->yearTendencia != ""){
			$sWhere .= " AND year = '{$filters->yearTendencia}' ";
		}else{
			$sWhere .= " AND year = YEAR(CURRENT_DATE)";
		}

        $Umbral = 1;
		
		/*----------  CAMBIO DE PESO A LIBRA MARCEL  ----------*/
		if($this->MermaTypePeso == "Kg"){
			$cantidad = "(cantidad / 2.2)";
			$total_peso_merma = "(total_peso_merma / 2.2)";
		}else{
			$cantidad = "(cantidad)";
			$total_peso_merma = "(total_peso_merma)";
		}
		/*----------  CAMBIO DE PESO A LIBRA MARCEL  ----------*/
		$Umbral = 50;

		$sql = "SELECT semana , type AS labor,ROUND(({$cantidad} / {$total_peso_merma}) * 100,2) AS cantidad 
				FROM (
					SELECT fecha, semana, bloque, merma_detalle.id_merma , type ,campo , cantidad , peso_neto , porcentaje_merma , total_peso_merma 
					FROM merma
					INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
					WHERE campo like '%Total Peso%' {$sWhere}
						AND type NOT IN ('RESULTADOS','FIRMA ELECTRONICA','OTROS' ,'RECUSADOS')  
				) as detalle
				GROUP BY type , semana
				ORDER BY type , semana";
		$data = [];

		$data = $this->db->queryAll($sql);
		$response = new stdClass;
		$series = new stdClass;
		$flag_count = 0;
		$labor = "";

		$response->series = [];
		$cantidad = 0;
		foreach ($data as $key => $value) {
			$response->legend[(int)$value->semana] = (int)$value->semana;
			if($this->session->id_company == 7){
				$cantidad = round(($value->cantidad / $total_peso_merma[$value->semana])*100 , 2);
			}elseif($this->session->id_company == 3){
				$cantidad = round(($value->cantidad / $total_peso_merma[$value->semana])*100 , 2);
			}else{
				$cantidad = (float)$value->cantidad;

			}
			if($labor != $value->labor){
				$labor = $value->labor;
				$series = new stdClass;
				$series->name = $value->labor;
				$series->type = "line";
				$series->data = [];
				$series->data[] = $cantidad;
				$series->itemStyle = (object)[
					"normal" => [
						"barBorderWidth" => 6,
						"barBorderRadius" => 0,
						"label" => [ "show" => false , "position" => "insideTop"]
					]
				];
				if($flag_count == 0){
					$series->markLine = new stdClass;
					$series->markLine->data = [];
					$markLine->name = "Umbral";
					$markLine->value = $Umbral;
					$markLine->xAxis = -1;
					$markLine->yAxis = $Umbral;
					$series->markLine->data[0][] = $markLine;
					
					$markLine = new stdClass;
					$markLine->xAxis = 52;
					$markLine->yAxis = $Umbral;

					$series->markLine->data[0][] = $markLine;
					$flag_count++;
				}	

				$response->series[$value->labor] = $series;
			}else{
				$response->series[$value->labor]->data[] = $cantidad;
			}
		}

		return $response;
	}
    
    private function pie($data = [] , $radius = ['50%', '70%'] , $name = "DAÑOS" , $roseType = "" , $type = "normal" , $legend = true , $position = ['60%', '60%'] , $version = 3){
		$response = new stdClass;
		$response->pie = [];
		if(count($data) > 0){
			$response->pie["title"]["show"] = true;
			$response->pie["title"]["text"] = $name;
			$response->pie["title"]["left"] = "center";
			// $response->pie["title"]["left"] = "right";
			$response->pie["title"]["subtext"] = "Merma";
			$response->pie["tooltip"]["trigger"] = "item";
			$response->pie["tooltip"]["formatter"] = "{a} <br/>{b}: {c} ({d}%)";
			$response->pie["toolbox"]["show"] = true;
			$response->pie["toolbox"]["feature"]["mark"]["show"] = true;
			$response->pie["toolbox"]["feature"]["restore"]["show"] = true;
			$response->pie["toolbox"]["feature"]["saveAsImage"]["show"] = true;
			$response->pie["toolbox"]["feature"]["saveAsImage"]["name"] = $name;
			$response->pie["legend"]["show"] = $legend;
			$response->pie["legend"]["orient"] = "vertical";
			$response->pie["legend"]["x"] = "left";
			$response->pie["calculable"] = true;
			$response->pie["legend"]["data"] = [];
			$response->pie["series"]["name"] = $name;
			$response->pie["series"]["type"] = "pie";
			$response->pie["series"]["center"] = $position;
			$response->pie["series"]["radius"] = $radius;
			if($version === 3){
				$response->pie["series"]["selectedMode"] = true;
				$response->pie["series"]["label"]["normal"]["show"] = true;
				$response->pie["series"]["label"]["emphasis"]["show"] = true;
				if($type != "normal"){
					$response->pie["series"]["roseType"] = $roseType;
					$response->pie["series"]["avoidLabelOverlap"] = "pie";
				}
				// $response->pie["series"]["label"]["normal"]["position"] = "center";
				// $response->pie["series"]["label"]["emphasis"]["textStyle"]["fontSize"] = "30";
				// $response->pie["series"]["label"]["emphasis"]["textStyle"]["fontWeight"] = "bold";
				$response->pie["series"]["labelLine"]["normal"]["show"] = true;
			}
			$response->pie["series"]["data"] = [];

			foreach ($data as $key => $value) {
				$value = (object)$value;
				$response->pie["legend"]["data"][] = $value->type;
				if($version === 3){
					$response->pie["series"]["data"][] = ["name" => $value->type  , "value" => (float)$value->cantidad , 
							"label" => [
								"normal" => ["show" => true , "position" => "outside" , "formatter" => "{b} \n {c} ({d}%)"],
								"emphasis" => ["show" => true , "position" => "outside"],
							]
					];
				}else{
					// $response->pie["series"]["data"][] = ["name" => $value->type  , "value" => (float)round($value->cantidad,2)];
					$response->pie["series"]["data"][] = ["name" => $value->type  , "value" => (float)round($value->cantidad,2),
						"itemStyle" => [
							"normal" => [ "label" => ["formatter" => "{b} \n {c} ({d}%)"] ],
						]
					];
				}
			}
		}

		return $response->pie;
	}
}