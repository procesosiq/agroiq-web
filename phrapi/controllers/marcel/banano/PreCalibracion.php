<?php defined('PHRAPI') or die("Direct access not allowed!");

class PreCalibracion {
	public $name;
	private $db;
	private $config;

	public function __construct(){
        $this->config = $GLOBALS['config'];
		$this->session = Session::getInstance();
        $this->db = DB::getInstance($this->session->agent_user);
    }

    public function params(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $data = (object)[
            "week" => getValueFrom($postdata, 'week', ''),
            "year" => getValueFrom($postdata, 'year', date('y')),
            "id_finca" => (int) getValueFrom($postdata, 'id_finca', 0)
        ];
        return $data;
    }

    public function last(){
        $response = new stdClass;
        $params = $this->params();

        $response->weeks = $this->db->queryAll("
            SELECT tbl.anio, tbl.semana, class
            FROM (
                SELECT year AS anio, semana
                FROM pre_calibracion
                WHERE year > 0 AND semana > 0
                GROUP BY year, semana
                UNION ALL
                SELECT year, semana
                FROM produccion_historica
                WHERE year > 0 AND semana > 0
                GROUP BY year, semana
            ) tbl
            LEFT JOIN semanas_colores sc ON sc.year = anio AND sc.semana = tbl.semana
            LEFT JOIN produccion_colores pc ON pc.color = sc.color
            GROUP BY tbl.anio, tbl.semana
            ORDER BY tbl.anio DESC, tbl.semana DESC");

        $response->weeks_enfunde = $this->db->queryAll("
            SELECT tbl.anio, tbl.semana, class,
                IFNULL((SELECT COUNT(1) FROM produccion_historica WHERE anio_enfundado = tbl.anio AND semana_enfundada = tbl.semana), 0) cosechados,
                IFNULL((SELECT SUM(cantidad) FROM produccion_racimos_caidos WHERE anio_enfundado = tbl.anio AND semana_enfundada = tbl.semana), 0) caidos,
                IFNULL((SELECT SUM(usadas) FROM produccion_enfunde WHERE years = tbl.anio AND semana = tbl.semana), 0) enfundados
            FROM (
                SELECT anio_enfundado AS anio, semana_enfundada AS semana
                FROM produccion_historica
                WHERE anio_enfundado > 0 AND semana_enfundada > 0
                GROUP BY anio_enfundado, semana_enfundada
                UNION ALL
                SELECT years, semana
                FROM produccion_enfunde
                WHERE years > 0 AND semana > 0
                GROUP BY years, semana
            ) tbl
            LEFT JOIN semanas_colores sc ON sc.year = anio AND sc.semana = tbl.semana
            LEFT JOIN produccion_colores pc ON pc.color = sc.color
            WHERE NOT EXISTS (SELECT * FROM produccion_enfunde_barrido WHERE anio_enfundado = tbl.anio AND semana_enfundada = tbl.semana)
            GROUP BY tbl.anio, tbl.semana
            ORDER BY tbl.anio DESC, tbl.semana DESC");
        foreach($response->weeks_enfunde as $row){
            $row->cosechados = (int) $row->cosechados;
            $row->caidos = (int) $row->caidos;
            $row->enfundados = (int) $row->enfundados;
        }

        $response->last_year = $response->weeks[0]->anio;
        $response->last_week = $response->weeks[0]->semana;

        $sql = "SELECT *
                FROM (
                    SELECT fincas.id, fincas.nombre
                    FROM produccion_historica
                    INNER JOIN fincas ON id_finca = fincas.id
                    GROUP BY anio_enfundado, semana_enfundada
                    UNION ALL
                    SELECT fincas.id, fincas.nombre
                    FROM produccion_enfunde
                    INNER JOIN fincas ON id_finca = fincas.id
                    GROUP BY id_finca
                ) tbl
                GROUP BY id";
        $response->fincas = $this->db->queryAll($sql);
        return $response;
    }

    public function barrer(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $sql = "INSERT INTO produccion_enfunde_barrido SET anio_enfundado = {$postdata->anio}, semana_enfundada = {$postdata->semana}";
        $this->db->query($sql);
    }

    // ENFUNDE VS PRECALIBRACION
    public function index(){
        $response = new stdClass;
        $params = $this->params();

        $response->color = $this->db->queryOne("SELECT color FROM semanas_colores WHERE year = {$params->year} AND semana = {$params->week}");
        $response->color = $this->db->queryRow("SELECT * FROM produccion_colores WHERE color = '{$response->color}'");

        $sql = "SELECT lote,
                    IFNULL((SELECT SUM(usadas) FROM produccion_enfunde WHERE years = {$params->year} AND semana = {$params->week} AND id_finca = {$params->id_finca} AND lote = tbl.lote), 0) AS racimos_enfunde
                FROM (
                    SELECT lote
                    FROM produccion_enfunde
                    WHERE years = {$params->year} AND semana = {$params->week} AND id_finca = {$params->id_finca}
                    GROUP BY lote
                    UNION ALL
                    SELECT lote
                    FROM pre_calibracion
                    WHERE anio_enfundado = {$params->year} AND semana_enfundada = {$params->week} AND id_finca = {$params->id_finca}
                    GROUP BY lote
                ) tbl
                GROUP BY lote
                ORDER BY lote+0";
        $response->data = $this->db->queryAll($sql);
        $response->edades = $this->db->queryAll("SELECT cinta, IFNULL(edad, 'S/C') AS edad FROM pre_calibracion WHERE anio_enfundado = {$params->year} AND semana_enfundada = {$params->week} AND id_finca = {$params->id_finca} GROUP BY edad ORDER BY edad");

        foreach($response->data as $row){
            foreach($response->edades as $e){
                // CANTIDAD DE RACIMOS POR EDAD
                $sql = "SELECT sum(cantidad) 
                        FROM pre_calibracion 
                        WHERE anio_enfundado = {$params->year} 
                            AND semana_enfundada = {$params->week} 
                            AND id_finca = {$params->id_finca} 
                            AND cinta = '{$e->cinta}' 
                            AND lote = '{$row->lote}' 
                            AND edad = '{$e->edad}'";
                $row->{"edad_{$e->edad}"} = (int) $this->db->queryOne($sql);
                $row->cosechados += $row->{"edad_{$e->edad}"};
            }
            $row->enfunde = round(($row->racimos_enfunde > 0 ? ($row->cosechados/$row->racimos_enfunde*100) : 0), 2);
            $row->caidos = (int) $this->db->queryOne("SELECT SUM(cantidad) FROM produccion_racimos_caidos WHERE anio_enfundado = {$params->year} AND semana_enfundada = {$params->week} AND id_finca = {$params->id_finca} AND lote = '{$row->lote}'");
            $row->porc_caidos = round((($row->caidos > 0 && $row->racimos_enfunde > 0) ? ($row->caidos/$row->racimos_enfunde*100) : 0), 2);
        }

        return $response;
    }

    public function precalibrado(){
        $response = new stdClass;
        $filters = $this->params();

        $sql = "SELECT cinta, h.edad, class, semana_enfundada, anio_enfundado
                FROM pre_calibracion h
                LEFT JOIN produccion_colores ON color = cinta
                WHERE year = $filters->year AND semana = $filters->week AND id_finca = {$filters->id_finca} AND h.edad > 0
                GROUP BY h.edad
                ORDER BY h.edad";
        $response->edades = $this->db->queryAll($sql);

        $sql = "SELECT lote
                FROM pre_calibracion
                WHERE year = $filters->year AND semana = $filters->week AND id_finca = {$filters->id_finca}
                GROUP BY lote
                ORDER BY 0+lote";
        $response->data = $this->db->queryAll($sql);

        foreach($response->data as $row){
            foreach($response->edades as $e){
                $rac_precalibrados = (int) $this->db->queryOne("SELECT SUM(cantidad) FROM pre_calibracion WHERE year = $filters->year AND semana = $filters->week AND id_finca = {$filters->id_finca} AND lote = '{$row->lote}' AND edad = {$e->edad}");
                $row->{"sem_{$e->edad}"} = $rac_precalibrados;
                $row->total += $row->{"sem_{$e->edad}"};
            }
        }

        return $response;
    }

    public function porPrecalibrar(){
        $response = new stdClass;
        $filters = $this->params();

        $sql = "SELECT *
                FROM (
                    SELECT cinta, h.edad, class, semana_enfundada, anio_enfundado
                    FROM produccion_historica h
                    LEFT JOIN produccion_colores ON color = cinta
                    WHERE year = $filters->year AND semana = $filters->week AND id_finca = {$filters->id_finca} AND h.edad > 0
                    GROUP BY h.edad
                    UNION ALL
                    SELECT cinta, h.edad, class, semana_enfundada, anio_enfundado
                    FROM pre_calibracion h
                    LEFT JOIN produccion_colores ON color = cinta
                    WHERE year = $filters->year AND semana = $filters->week AND id_finca = {$filters->id_finca} AND h.edad > 0
                    GROUP BY h.edad
                ) tbl
                WHERE NOT EXISTS (SELECT * FROM produccion_enfunde_barrido WHERE anio_enfundado = tbl.anio_enfundado AND semana_enfundada = tbl.semana_enfundada)
                GROUP BY edad
                ORDER BY edad";
        $response->edades = $this->db->queryAll($sql);

        $sql = "SELECT *
                FROM (
                    SELECT lote, 0 total
                    FROM produccion_historica
                    WHERE year = $filters->year AND semana = $filters->week AND id_finca = {$filters->id_finca}
                    GROUP BY lote
                    UNION ALL
                    SELECT lote, 0 total
                    FROM pre_calibracion
                    WHERE year = $filters->year AND semana = $filters->week AND id_finca = {$filters->id_finca}
                    GROUP BY lote
                ) tbl
                GROUP BY lote
                ORDER BY 0+lote";
        $response->data = $this->db->queryAll($sql);

        foreach($response->data as $row){
            foreach($response->edades as $e){
                $sql = "SELECT SUM(usadas) 
                        FROM produccion_enfunde 
                        WHERE years = {$e->anio_enfundado} 
                            AND semana = {$e->semana_enfundada} 
                            AND id_finca = {$filters->id_finca} 
                            AND lote = '{$row->lote}'";
                $rac_enfundados = (int) $this->db->queryOne($sql);

                $sql = "SELECT SUM(cantidad) 
                        FROM pre_calibracion 
                        WHERE anio_enfundado = {$e->anio_enfundado}
                            AND semana_enfundada = {$e->semana_enfundada}
                            AND ((year = {$filters->year} AND semana <= {$filters->week}) OR year < {$filters->year})
                            AND id_finca = {$filters->id_finca} 
                            AND lote = '{$row->lote}'";
                $rac_precalibrados = (int) $this->db->queryOne($sql);

                $sql = "SELECT COUNT(1) 
                        FROM produccion_racimos_caidos 
                        WHERE anio_enfundado = {$e->anio_enfundado} 
                            AND semana_enfundada = {$e->semana_enfundada} 
                            AND id_finca = {$filters->id_finca} 
                            AND lote = '{$row->lote}'";
                $rac_caidos = (int) $this->db->queryOne($sql);

                $row->{"sem_{$e->edad}"} = $rac_enfundados - $rac_precalibrados - $rac_caidos;
                $row->{"sem_{$e->edad}_enfundados"} = $rac_enfundados;
                $row->{"sem_{$e->edad}_precalibrados"} = $rac_precalibrados;
                $row->{"sem_{$e->edad}_caidos"} = $rac_caidos;
                
                $row->total += $row->{"sem_{$e->edad}"};
                $row->{"total_enfundados"} += $rac_enfundados;
                $row->{"total_precalibrados"} += $rac_precalibrados;
                $row->{"total_caidos"} += $rac_caidos;
            }
        }

        return $response;
    }

    public function porCosechar(){
        $response = new stdClass;
        $filters = $this->params();

        $sql = "SELECT *
                FROM (
                    SELECT cinta, h.edad, class, semana_enfundada, anio_enfundado
                    FROM produccion_historica h
                    LEFT JOIN produccion_colores ON color = cinta
                    WHERE year = $filters->year AND semana = $filters->week AND id_finca = {$filters->id_finca} AND h.edad > 0
                    GROUP BY h.edad
                    UNION ALL
                    SELECT cinta, h.edad, class, semana_enfundada, anio_enfundado
                    FROM pre_calibracion h
                    LEFT JOIN produccion_colores ON color = cinta
                    WHERE year = $filters->year AND semana = $filters->week AND id_finca = {$filters->id_finca} AND h.edad > 0
                    GROUP BY h.edad
                ) tbl
                GROUP BY edad
                ORDER BY edad";
        $response->edades = $this->db->queryAll($sql);

        $sql = "SELECT *
                FROM (
                    SELECT lote, 0 total
                    FROM produccion_historica
                    WHERE year = $filters->year AND semana = $filters->week AND id_finca = {$filters->id_finca}
                    GROUP BY lote
                    UNION ALL
                    SELECT lote, 0 total
                    FROM pre_calibracion
                    WHERE year = $filters->year AND semana = $filters->week AND id_finca = {$filters->id_finca}
                    GROUP BY lote
                ) tbl
                GROUP BY lote
                ORDER BY 0+lote";
        $response->data = $this->db->queryAll($sql);

        foreach($response->data as $row){
            foreach($response->edades as $e){
                $sql = "SELECT SUM(cantidad) 
                        FROM pre_calibracion 
                        WHERE anio_enfundado = {$e->anio_enfundado} 
                            AND semana_enfundada = {$e->semana_enfundada} 
                            AND ((year = {$filters->year} AND semana <= {$filters->week}) OR year < {$filters->year})
                            AND id_finca = {$filters->id_finca} 
                            AND lote = '{$row->lote}'";
                $rac_precalibrados = (int) $this->db->queryOne($sql);

                $sql = "SELECT COUNT(1) 
                        FROM produccion_historica 
                        WHERE anio_enfundado = {$e->anio_enfundado} 
                            AND semana_enfundada = {$e->semana_enfundada} 
                            AND ((year = {$filters->year} AND semana <= {$filters->week}) OR year < {$filters->year})
                            AND id_finca = {$filters->id_finca} 
                            AND lote = '{$row->lote}'";
                $rac_cosechados = (int) $this->db->queryOne($sql);

                $sql = "SELECT COUNT(1) 
                        FROM produccion_racimos_caidos 
                        WHERE anio_enfundado = {$e->anio_enfundado} 
                            AND semana_enfundada = {$e->semana_enfundada} 
                            AND id_finca = {$filters->id_finca} 
                            AND lote = '{$row->lote}'";
                $rac_caidos = (int) $this->db->queryOne($sql);

                $row->{"sem_{$e->edad}"} = $rac_precalibrados - $rac_cosechados - $rac_caidos;
                $row->{"sem_{$e->edad}_cosechados"} = $rac_cosechados;
                $row->{"sem_{$e->edad}_precalibrados"} = $rac_precalibrados;
                $row->{"sem_{$e->edad}_caidos"} = $rac_caidos;

                $row->total += $row->{"sem_{$e->edad}"};
                $row->{"total_cosechados"} += $rac_cosechados;
                $row->{"total_precalibrados"} += $rac_precalibrados;
                $row->{"total_caidos"} += $rac_caidos;
            }
        }

        return $response;
    }

    public function enCampo(){
        
    }

    public function database(){
        $response = new stdClass;
        $params = $this->params();

        $sql = "SELECT lotes.nombre
                FROM lotes
                INNER JOIN fincas ON lotes.idFinca = fincas.id
                WHERE lotes.status = 'Activo'";
        $response->lotes = $this->db->queryAllOne($sql);

        $sql = "SELECT pre.*, REPLACE(json, 'json/marcel/completados/preCalibracion/', '') 'json', colores.class
                FROM pre_calibracion pre
                INNER JOIN produccion_colores colores ON colores.color = pre.cinta
                WHERE pre.semana = {$params->week} 
                    AND pre.year = {$params->year} 
                    AND pre.id_finca = {$params->id_finca}
                ORDER BY pre.lote+0";
        $response->data = $this->db->queryAll($sql);

        return $response;
    }

    public function guardarEditar(){
        $response = new stdClass;
        $response->status = 400;
        $postdata = (object)json_decode(file_get_contents("php://input"));
        
        if(isset($postdata->id) && $postdata->id > 0 && isset($postdata->edad) && $postdata->edad > 0){
            $response->status = 200;

            $sql = "UPDATE pre_calibracion
                    SET
                        calibrador = '{$postdata->calibrador}',
                        lote = '{$postdata->lote}',
                        id_finca = (SELECT id FROM fincas WHERE nombre = '{$postdata->lote}'),
                        cantidad = {$postdata->cantidad},
                        edad = {$postdata->edad},
                        cinta = getCintaFromEdad({$postdata->edad}, semana, year),
                        semana_enfundada = getSemanaEnfundada({$postdata->edad}, semana, year),
                        anio_enfundado = IF(getSemanaEnfundada({$postdata->edad}, semana, year) > semana, year-1, year)
                    WHERE id = {$postdata->id}";
            $this->db->query($sql);
        }

        return $response;
    }

    public function borrar(){
        $response = new stdClass;
        $response->status = 400;
        $postdata = (object)json_decode(file_get_contents("php://input"));
        
        if(isset($postdata->id) && $postdata->id > 0 && isset($postdata->edad) && $postdata->edad > 0){
            $response->status = 200;

            $sql = "DELETE FROM pre_calibracion
                    WHERE id = {$postdata->id}";
            $this->db->query($sql);
        }

        return $response;
    }
}
