<?php defined('PHRAPI') or die("Direct access not allowed!");

class Produccion {
	public $name;
	private $db;
	private $config;

	public function __construct(){
		$this->config = $GLOBALS['config'];
		$this->session = Session::getInstance();
        $this->maindb = DB::getInstance();
        $this->db = DB::getInstance($this->session->agent_user);
	}

	public function lastDay(){
		$response = new stdClass;
		$response->last = $this->db->queryRow("SELECT DATE(MAX(fecha)) AS fecha FROM produccion_historica");
		$response->fincas = $this->db->queryAllSpecial("SELECT id_finca AS id, finca AS label FROM produccion GROUP BY id_finca");
		return $response;
	}

	public function historico(){
		$postdata = (object)json_decode(file_get_contents("php://input"));

		$filters = (object)[
			"finca" => getValueFrom($postdata , "finca" , "" , FILTER_SANITIZE_STRING),
			"fecha_inicial" => getValueFrom($postdata , "fecha_inicial" , "" , FILTER_SANITIZE_STRING),
			"fecha_final" => getValueFrom($postdata , "fecha_final" , "" , FILTER_SANITIZE_STRING),
		];

		$response = new stdClass;
		$response->data = $this->db->queryAll("SELECT historica.id, fecha, lote, causa, peso, manos, calibre, cinta, dedos, colores.class, historica.edad, cuadrilla, hora, tipo
			FROM produccion_historica historica
			LEFT JOIN produccion_colores colores ON colores.color = cinta
			WHERE fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}'");
		return $response;
	}

	public function index(){
		$postdata = (object)json_decode(file_get_contents("php://input"));

		$filters = (object)[
			"finca" => getValueFrom($postdata , "finca" , "" , FILTER_SANITIZE_STRING),
			"fecha_inicial" => getValueFrom($postdata , "fecha_inicial" , "" , FILTER_SANITIZE_STRING),
			"fecha_final" => getValueFrom($postdata , "fecha_final" , "" , FILTER_SANITIZE_STRING),
		];

		$sWhere = "";

		if($filters->fecha_final != "" && $filters->fecha_inicial != ""){
			$sWhere .= " AND fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}'";
		}

		$response = new stdClass;
		$response->totales = (object)[
			"total_cosechados" => 0,
			"total_recusados" => 0,
			"total_procesada" => 0,
			"muestreados" => 0,
			"porcen" => 100,
			"peso" => 0,
			"manos" => 0,
			"calibracion" => 0,
			"dedos" => 0
		];

		$sql = "SELECT lote, 
                    IFNULL((SELECT SUM(IF(blz > form, blz, form)) FROM produccion_racimos_cuadrado WHERE fecha = '{$filters->fecha_inicial}' AND lote = produccion.lote AND status = 'RECU'), 
                        (SELECT COUNT(1) FROM produccion_historica WHERE tipo = 'RECUSADO' AND lote = produccion.lote  {$sWhere})) AS total_recusados,
                    IFNULL((SELECT SUM(IF(blz > form, blz, form)) FROM produccion_racimos_cuadrado WHERE fecha = '{$filters->fecha_inicial}' AND lote = produccion.lote AND status = 'PROC'), 
                        (SELECT COUNT(1) FROM produccion_historica WHERE tipo = 'PROC' AND lote = produccion.lote  {$sWhere})) AS total_procesados,
                    ROUND((SELECT AVG(edad) FROM produccion_historica WHERE lote = produccion.lote {$sWhere}), 2) AS edad,
                    (SELECT COUNT(1) FROM produccion_historica WHERE tipo = 'PROC' AND lote = produccion.lote AND calibre > 0 {$sWhere}) AS muestreados,
                    IFNULL((SELECT IF(SUM(blz) = 0, (SELECT AVG(peso) FROM produccion_historica WHERE tipo = 'PROC' {$sWhere}), NULL) FROM produccion_racimos_cuadrado WHERE fecha = '{$filters->fecha_inicial}' AND lote = produccion.lote),
					    (SELECT AVG(peso) FROM produccion_historica WHERE tipo = 'PROC' AND lote = produccion.lote  {$sWhere})) AS peso,
					(SELECT AVG(calibre) FROM produccion_historica WHERE tipo = 'PROC' AND lote = produccion.lote AND cinta != 'n/a' AND calibre IS NOT NULL AND calibre > 0 {$sWhere}) AS calibracion,
					(SELECT AVG(manos) FROM produccion_historica WHERE tipo = 'PROC' AND lote = produccion.lote AND cinta != 'n/a' AND manos IS NOT NULL AND manos > 0 {$sWhere}) AS manos,
					(SELECT AVG(dedos) FROM produccion_historica WHERE tipo = 'PROC' AND lote = produccion.lote AND cinta != 'n/a' AND dedos IS NOT NULL AND dedos > 0 {$sWhere}) AS dedos
				FROM(
                    SELECT *
                    FROM(
                        SELECT lote
                        FROM produccion_historica
                        WHERE 1=1 {$sWhere}
                        GROUP BY lote
                        UNION ALL
                        SELECT lote
                        FROM racimos_cosechados_by_color
                        WHERE 1=1 {$sWhere}
                        GROUP BY lote
                    ) AS tbl
                    GROUP BY lote
                ) AS produccion";

		$response->data = $this->db->queryAll($sql);

		foreach ($response->data as $key => $value) {
			$value->expanded = false;
			$value->total_cosechados = $value->total_procesados + $value->total_recusados;

			$value->cables = $this->db->queryAll("SELECT cinta AS cable, edad, class,
                                IFNULL((SELECT SUM(IF(blz > form, blz, form)) FROM produccion_racimos_cuadrado WHERE lote = '{$value->lote}' AND fecha = '{$filters->fecha_inicial}' AND cinta = produccion.cinta AND status = 'RECU'), 
                                    (SELECT COUNT(1) FROM produccion_historica WHERE tipo = 'RECUSADO' AND lote = '{$value->lote}' AND cinta = produccion.cinta {$sWhere})   ) AS total_recusados, 
                                IFNULL((SELECT SUM(IF(blz > form, blz, form)) FROM produccion_racimos_cuadrado WHERE lote = '{$value->lote}' AND fecha = '{$filters->fecha_inicial}' AND cinta = produccion.cinta AND status = 'PROC'), 
                                    (SELECT COUNT(1) FROM produccion_historica WHERE tipo = 'PROC' AND lote = '{$value->lote}' AND cinta = produccion.cinta {$sWhere})) AS total_procesados, 
                                IFNULL((SELECT IF(SUM(blz) = 0, (SELECT AVG(peso) FROM produccion_historica WHERE tipo = 'PROC' {$sWhere}), NULL) FROM produccion_racimos_cuadrado WHERE fecha = '{$filters->fecha_inicial}' AND lote = '{$value->lote}' AND cinta = produccion.cinta),
								    (SELECT AVG(peso) FROM produccion_historica WHERE tipo = 'PROC' AND lote = '{$value->lote}' AND cinta = produccion.cinta {$sWhere})) AS peso,
								(SELECT AVG(calibre) FROM produccion_historica WHERE tipo = 'PROC' AND lote = '{$value->lote}' AND cinta = produccion.cinta AND cinta != 'n/a' AND calibre IS NOT NULL AND calibre > 0 {$sWhere}) AS calibracion,
								(SELECT AVG(manos) FROM produccion_historica WHERE tipo = 'PROC' AND lote = '{$value->lote}' AND cinta = produccion.cinta AND cinta != 'n/a' AND manos IS NOT NULL AND manos > 0 {$sWhere}) AS manos,
								(SELECT AVG(dedos) FROM produccion_historica WHERE tipo = 'PROC' AND lote = '{$value->lote}' AND cinta = produccion.cinta AND cinta != 'n/a' AND dedos IS NOT NULL AND dedos > 0 {$sWhere}) AS dedos
							FROM(
                                SELECT *
                                FROM (
                                    SELECT cinta , produccion.edad, class
                                    FROM produccion_historica produccion
                                    LEFT JOIN produccion_colores colores ON cinta = color
                                    WHERE 1 = 1 AND lote = '{$value->lote}'  {$sWhere}
                                    GROUP BY cinta
                                    UNION ALL
                                    SELECT b.color AS cinta, b.edad, class
                                    FROM racimos_cosechados_by_color b
                                    LEFT JOIN produccion_colores c ON b.color = c.color
                                    WHERE 1 = 1 AND lote = '{$value->lote}' {$sWhere}
                                ) AS tbl
                                GROUP BY cinta
                            ) AS produccion");
			foreach ($value->cables as $llave => $valor) {
				$valor->expanded = false;
				$valor->total_cosechados = $valor->total_procesados + $valor->total_recusados;

				$valor->cuadrillas = $this->db->queryAll("SELECT cuadrilla,
                        IFNULL((SELECT SUM(IF(blz > form, blz, form))  FROM produccion_racimos_cuadrado WHERE lote = '{$value->lote}' AND fecha = '{$filters->fecha_inicial}' AND cinta = '{$value->cable}' AND palanca = produccion.cuadrilla AND status = 'RECU'), 
                            (SELECT COUNT(1) FROM produccion_historica WHERE tipo = 'RECUSADO' AND lote = '{$value->lote}' AND cinta = '{$valor->cable}' AND cuadrilla = produccion.cuadrilla {$sWhere})) AS total_recusados, 
                        IFNULL((SELECT SUM(IF(blz > form, blz, form))  FROM produccion_racimos_cuadrado WHERE lote = '{$value->lote}' AND fecha = '{$filters->fecha_inicial}' AND cinta = '{$value->cable}' AND palanca = produccion.cuadrilla AND status = 'PROC'), 
                            (SELECT COUNT(1) FROM produccion_historica WHERE tipo = 'PROC' AND lote = '{$value->lote}' AND cinta = '{$valor->cable}' AND cuadrilla = produccion.cuadrilla {$sWhere})) AS procesados, 
                        (SELECT COUNT(1) FROM produccion_historica WHERE tipo = 'PROC' AND lote = '{$value->lote}' AND cinta = '{$valor->cable}' AND calibre > 0 AND cuadrilla = produccion.cuadrilla {$sWhere}) AS muestreados, 
                        (SELECT AVG(peso) FROM produccion_historica WHERE tipo = 'PROC' AND lote = '{$value->lote}' AND cinta = '{$valor->cable}' AND cuadrilla = produccion.cuadrilla {$sWhere}) AS peso,
                        (SELECT AVG(calibre) FROM produccion_historica WHERE tipo = 'PROC' AND lote = '{$value->lote}' AND cinta = '{$valor->cable}' AND cuadrilla = produccion.cuadrilla AND cinta != 'n/a' AND calibre IS NOT NULL AND calibre > 0 {$sWhere}) AS calibracion,
                        (SELECT AVG(manos) FROM produccion_historica WHERE tipo = 'PROC' AND lote = '{$value->lote}' AND cinta = '{$valor->cable}' AND cuadrilla = produccion.cuadrilla AND cinta != 'n/a' AND manos IS NOT NULL AND manos > 0 {$sWhere}) AS manos,
                        (SELECT AVG(dedos) FROM produccion_historica WHERE tipo = 'PROC' AND lote = '{$value->lote}' AND cinta = '{$valor->cable}' AND cuadrilla = produccion.cuadrilla AND cinta != 'n/a' AND dedos IS NOT NULL AND dedos > 0 {$sWhere}) AS dedos
                    FROM(
                        SELECT cuadrilla
                        FROM produccion_historica historica
                        LEFT JOIN produccion_colores colores ON cinta = color
                        WHERE 1 = 1 {$sWhere} AND lote = '{$value->lote}' AND cinta = '{$valor->cable}'
                        GROUP BY cuadrilla
                    ) AS produccion");
				foreach($valor->cuadrillas as $row){
					$row->total_cosechados = $row->procesados + $row->total_recusados;
				}
			}
		}
        
        /* LOAD TAGS */
        $response->totales->total_procesada = $this->db->queryOne("SELECT SUM(IFNULL((SELECT SUM(IF(blz > form, blz, form)) FROM produccion_racimos_cuadrado WHERE fecha = '{$filters->fecha_inicial}' AND lote = tbl.lote AND cinta = tbl.cinta AND status = 'PROC'), cant)) AS cant
            FROM(
                SELECT lote, cinta, COUNT(1) AS cant 
                FROM produccion_historica
                WHERE fecha = '{$filters->fecha_inicial}' AND tipo = 'PROC'
                GROUP BY lote, cinta
                UNION ALL
                SELECT lote, h.color AS cinta, 0 AS cant
                FROM racimos_cosechados_by_color h
                INNER JOIN produccion_colores c ON c.color = h.color
                WHERE fecha = '{$filters->fecha_inicial}' AND (SELECT COUNT(1) FROM produccion_historica WHERE lote = h.lote AND cinta = h.color AND fecha = '{$filters->fecha_inicial}') = 0
                GROUP BY lote, h.edad
            ) AS tbl");
        $response->totales->total_cosechados = $this->db->queryOne("SELECT SUM(IFNULL((SELECT SUM(IF(blz > form, blz, form)) FROM produccion_racimos_cuadrado WHERE fecha = '{$filters->fecha_inicial}' AND lote = tbl.lote AND cinta = tbl.cinta), cant)) AS cant
            FROM(
                SELECT lote, cinta, COUNT(1) AS cant 
                FROM produccion_historica 
                WHERE fecha = '{$filters->fecha_inicial}'
                GROUP BY lote, cinta
                UNION ALL
                SELECT lote, h.color AS cinta, 0 AS cant
                FROM racimos_cosechados_by_color h
                INNER JOIN produccion_colores c ON c.color = h.color
                WHERE fecha = '{$filters->fecha_inicial}' AND (SELECT COUNT(1) FROM produccion_historica WHERE lote = h.lote AND cinta = h.color AND fecha = '{$filters->fecha_inicial}') = 0
                GROUP BY lote, h.edad
            ) AS tbl");
        $response->totales->muestreados = $response->totales->total_procesada + $response->totales->total_cosechados;

        $response->id_company = $this->session->id_company;
        $response->totales->recusados = $this->db->queryOne("SELECT SUM(IFNULL((SELECT SUM(IF(blz > form, blz, form)) FROM produccion_racimos_cuadrado WHERE fecha = '{$filters->fecha_inicial}' AND lote = tbl.lote AND cinta = tbl.cinta AND status = 'RECU'), cant)) AS cant
            FROM(
                SELECT lote, cinta, COUNT(1) AS cant 
                FROM produccion_historica 
                WHERE fecha = '{$filters->fecha_inicial}' AND tipo = 'RECUSADO'
                GROUP BY lote, cinta
            ) AS tbl");
		$response->totales->peso = $this->db->queryRow("SELECT AVG(peso) as suma FROM produccion_historica WHERE tipo = 'PROC' {$sWhere}")->suma;
		$response->totales->manos = $this->db->queryRow("SELECT AVG(manos) as suma FROM produccion_historica WHERE tipo = 'PROC' AND manos IS NOT NULL AND manos > 0 AND cinta != 'n/a' {$sWhere}")->suma;
		$response->totales->calibracion = $this->db->queryRow("SELECT AVG(calibre) as suma FROM produccion_historica WHERE tipo = 'PROC' AND calibre IS NOT NULL AND calibre > 0 AND cinta != 'n/a' {$sWhere}")->suma;
		$response->totales->dedos = $this->db->queryRow("SELECT AVG(dedos) as suma FROM produccion_historica WHERE tipo = 'PROC' AND dedos IS NOT NULL AND dedos > 0 AND cinta != 'n/a' {$sWhere}")->suma;
        $response->totales->porcen = ($response->totales->muestreados / $response->totales->total_cosechados) * 100;
        $response->totales->edad = $this->db->queryRow("SELECT ROUND(AVG(edad), 2) AS edad FROM produccion_historica WHERE tipo = 'PROC' {$sWhere}")->edad;

        $suma_peso_proc = $this->db->queryOne("SELECT SUM(IF(blz > form, blz, form))-SUM(blz) FROM produccion_racimos_cuadrado WHERE fecha = '{$filters->fecha_inicial}' AND status = 'PROC'");
        $suma_peso_proc *= $response->totales->peso;
        $response->tags["kg_proc"] = $this->db->queryOne("SELECT SUM(peso)+($suma_peso_proc) FROM produccion_historica WHERE fecha = '{$filters->fecha_inicial}' AND tipo = 'PROC'");

        $suma_peso_proc = $this->db->queryOne("SELECT SUM(IF(blz > form, blz, form))-SUM(blz) FROM produccion_racimos_cuadrado WHERE fecha = '{$filters->fecha_inicial}' AND status = 'RECU'");
        $suma_peso_proc *= $response->totales->peso;
        $response->tags["kg_recu"] = $this->db->queryOne("SELECT SUM(peso)+($suma_peso_proc) FROM produccion_historica WHERE fecha = '{$filters->fecha_inicial}' AND tipo = 'RECUSADO'");

        $times = $this->db->queryAll("SELECT fecha, hora FROM produccion_historica WHERE 1=1 $sWhere ORDER BY hora");
        $p = $times[0];
        $u = $times[count($times)-1];
        $response->tags["ultima_fecha"] = $u->fecha;
        $response->tags["ultima_hora"] = $u->hora;
        $response->tags["primera_fecha"] = $p->fecha;
        $response->tags["primera_hora"] = $p->hora;
        $response->tags["diferencia"] = $this->db->queryRow("SELECT TIMEDIFF('{$u->hora}', '{$p->hora}') AS dif")->dif;
		return $response;
	}

	public function racimosPalanca(){
		$postdata = (object)json_decode(file_get_contents("php://input"));

		$filters = (object)[
			"finca" => getValueFrom($postdata , "finca" , "" , FILTER_SANITIZE_STRING),
			"fecha_inicial" => getValueFrom($postdata , "fecha_inicial" , "" , FILTER_SANITIZE_STRING),
			"fecha_final" => getValueFrom($postdata , "fecha_final" , "" , FILTER_SANITIZE_STRING),
		];
		if($filters->fecha_final != "" && $filters->fecha_inicial != ""){
			$sWhere .= " AND fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}'";
		}

		$sql = "SELECT cuadrilla,
					(SELECT COUNT(1) FROM produccion_historica WHERE tipo = 'RECUSADO' AND cuadrilla = produccion.cuadrilla  {$sWhere}) AS recusados , 
					(SELECT COUNT(1) FROM produccion_historica WHERE tipo = 'PROC' AND cuadrilla = produccion.cuadrilla  {$sWhere}) AS procesados, 
                    (SELECT COUNT(1) FROM produccion_historica WHERE tipo = 'PROC' AND cuadrilla = produccion.cuadrilla AND calibre > 0 {$sWhere}) AS muestreados, 
                    ROUND((SELECT AVG(edad) FROM produccion_historica WHERE tipo = 'PROC' AND cuadrilla = produccion.cuadrilla AND edad > 0 {$sWhere}), 2) AS edad,
					(SELECT AVG(peso) FROM produccion_historica WHERE tipo = 'PROC' AND cuadrilla = produccion.cuadrilla  {$sWhere}) AS peso,
					(SELECT AVG(calibre) FROM produccion_historica WHERE tipo = 'PROC' AND cuadrilla = produccion.cuadrilla AND cinta != 'n/a' AND calibre IS NOT NULL AND calibre > 0 {$sWhere}) AS calibracion,
					(SELECT AVG(manos) FROM produccion_historica WHERE tipo = 'PROC' AND cuadrilla = produccion.cuadrilla AND cinta != 'n/a' AND manos IS NOT NULL AND manos > 0 {$sWhere}) AS manos,
					(SELECT AVG(dedos) FROM produccion_historica WHERE tipo = 'PROC' AND cuadrilla = produccion.cuadrilla AND cinta != 'n/a' AND dedos IS NOT NULL AND dedos > 0 {$sWhere}) AS dedos
				FROM(
				SELECT cuadrilla
				FROM produccion_historica produccion
				WHERE 1 = 1  {$sWhere}
				GROUP BY cuadrilla) AS produccion";
		$response->palancas = $this->db->queryAll($sql);
		foreach($response->palancas as $val){
			$val->cosechados = $val->procesados + $val->recusados;

			$val->lotes = $this->db->queryAll("SELECT lote,
					(SELECT COUNT(1) FROM produccion_historica WHERE tipo = 'RECUSADO' AND cuadrilla = '{$val->cuadrilla}' AND lote = produccion.lote {$sWhere}) AS recusados , 
					(SELECT COUNT(1) FROM produccion_historica WHERE tipo = 'PROC' AND cuadrilla = '{$val->cuadrilla}' AND lote = produccion.lote {$sWhere}) AS procesados, 
                    (SELECT COUNT(1) FROM produccion_historica WHERE tipo = 'PROC' AND cuadrilla = '{$val->cuadrilla}' AND lote = produccion.lote AND calibre > 0 {$sWhere}) AS muestreados, 
                    ROUND((SELECT AVG(edad) FROM produccion_historica WHERE tipo = 'PROC' AND cuadrilla = '{$val->cuadrilla}' AND lote = produccion.lote AND edad > 0 {$sWhere}), 2) AS edad, 
					(SELECT AVG(peso) FROM produccion_historica WHERE tipo = 'PROC' AND cuadrilla = '{$val->cuadrilla}'AND lote = produccion.lote  {$sWhere}) AS peso,
					(SELECT AVG(calibre) FROM produccion_historica WHERE tipo = 'PROC' AND cuadrilla = '{$val->cuadrilla}' AND cinta != 'n/a' AND lote = produccion.lote AND calibre IS NOT NULL AND calibre > 0 {$sWhere}) AS calibracion,
					(SELECT AVG(manos) FROM produccion_historica WHERE tipo = 'PROC' AND cuadrilla = '{$val->cuadrilla}' AND cinta != 'n/a' AND lote = produccion.lote AND manos IS NOT NULL AND manos > 0 {$sWhere}) AS manos,
					(SELECT AVG(dedos) FROM produccion_historica WHERE tipo = 'PROC' AND cuadrilla = '{$val->cuadrilla}' AND cinta != 'n/a' AND lote = produccion.lote AND dedos IS NOT NULL AND dedos > 0 {$sWhere}) AS dedos
				FROM(
				SELECT lote
				FROM produccion_historica produccion
				WHERE cuadrilla = '{$val->cuadrilla}' {$sWhere}
				GROUP BY lote) AS produccion");
			foreach($val->lotes as $value){
				$value->cosechados = $value->procesados + $value->recusados;
				
				$value->cables = $this->db->queryAll("SELECT cinta AS cable, produccion.edad, class,
						(SELECT COUNT(1) FROM produccion_historica WHERE tipo = 'RECUSADO' AND cuadrilla = '{$val->cuadrilla}' AND lote = '{$value->lote}' AND cinta = produccion.cinta {$sWhere}) AS recusados , 
						(SELECT COUNT(1) FROM produccion_historica WHERE tipo = 'PROC' AND cuadrilla = '{$val->cuadrilla}' AND lote = '{$value->lote}' AND cinta = produccion.cinta {$sWhere}) AS procesados, 
                        (SELECT COUNT(1) FROM produccion_historica WHERE tipo = 'PROC' AND cuadrilla = '{$val->cuadrilla}' AND lote = '{$value->lote}' AND cinta = produccion.cinta AND calibre > 0 {$sWhere}) AS muestreados, 
                        (SELECT AVG(edad) FROM produccion_historica WHERE tipo = 'PROC' AND cuadrilla = '{$val->cuadrilla}' AND lote = '{$value->lote}' AND cinta = produccion.cinta AND edad > 0 {$sWhere}) AS edad,
						(SELECT AVG(peso) FROM produccion_historica WHERE tipo = 'PROC' AND cuadrilla = '{$val->cuadrilla}' AND lote = '{$value->lote}' AND cinta = produccion.cinta  {$sWhere}) AS peso,
						(SELECT AVG(calibre) FROM produccion_historica WHERE tipo = 'PROC' AND cuadrilla = '{$val->cuadrilla}' AND cinta != 'n/a' AND lote = '{$value->lote}' AND cinta = produccion.cinta AND calibre IS NOT NULL AND calibre > 0 {$sWhere}) AS calibracion,
						(SELECT AVG(manos) FROM produccion_historica WHERE tipo = 'PROC' AND cuadrilla = '{$val->cuadrilla}' AND cinta != 'n/a' AND lote = '{$value->lote}' AND cinta = produccion.cinta AND manos IS NOT NULL AND manos > 0 {$sWhere}) AS manos,
						(SELECT AVG(dedos) FROM produccion_historica WHERE tipo = 'PROC' AND cuadrilla = '{$val->cuadrilla}' AND cinta != 'n/a' AND lote = '{$value->lote}' AND cinta = produccion.cinta AND dedos IS NOT NULL AND dedos > 0 {$sWhere}) AS dedos
					FROM(
					SELECT cinta , produccion.edad, class
					FROM produccion_historica produccion
					LEFT JOIN produccion_colores colores ON cinta = color
					WHERE cuadrilla = '{$val->cuadrilla}' AND lote = '{$value->lote}' {$sWhere}
					GROUP BY cinta) AS produccion");
				foreach($value->cables as $row){
					$row->cosechados = $row->procesados + $row->recusados;
				}
			}
		}
		return $response;
	}

	public function racimosEdad(){
		$postdata = (object)json_decode(file_get_contents("php://input"));

		$filters = (object)[
			"finca" => getValueFrom($postdata , "finca" , "" , FILTER_SANITIZE_STRING),
			"fecha_inicial" => getValueFrom($postdata , "fecha_inicial" , "" , FILTER_SANITIZE_STRING),
			"fecha_final" => getValueFrom($postdata , "fecha_final" , "" , FILTER_SANITIZE_STRING),
		];

		if($filters->fecha_final != "" && $filters->fecha_inicial != ""){
			$sWhere .= " AND fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}'";
		}

		$sql = "SELECT cinta AS cable, edad AS edad, class,
                    IFNULL((SELECT SUM(IF(blz > form, blz, form)) FROM produccion_racimos_cuadrado WHERE fecha = '{$filters->fecha_inicial}' AND cinta = produccion.cinta AND status = 'RECU'),
					    (SELECT COUNT(1) FROM produccion_historica WHERE tipo = 'RECUSADO' AND cinta = produccion.cinta  {$sWhere})) AS recusados, 
                    IFNULL((SELECT SUM(IF(blz > form, blz, form)) FROM produccion_racimos_cuadrado WHERE fecha = '{$filters->fecha_inicial}' AND cinta = produccion.cinta AND status = 'PROC'),
    					(SELECT COUNT(1) FROM produccion_historica WHERE tipo = 'PROC' AND cinta = produccion.cinta  {$sWhere})) AS procesados, 
                    (SELECT COUNT(1) FROM produccion_historica WHERE tipo = 'PROC' AND cinta = produccion.cinta AND calibre > 0 {$sWhere}) AS muestreados, 
					IFNULL((SELECT IF(SUM(blz) = 0, (SELECT AVG(peso) FROM produccion_historica WHERE tipo = 'PROC' {$sWhere}), NULL) FROM produccion_racimos_cuadrado WHERE fecha = '{$filters->fecha_inicial}' AND cinta = produccion.cinta),
                        (SELECT AVG(peso) FROM produccion_historica WHERE tipo = 'PROC' AND cinta = produccion.cinta  {$sWhere})) AS peso,
					(SELECT AVG(calibre) FROM produccion_historica WHERE tipo = 'PROC' AND cinta = produccion.cinta AND cinta != 'n/a' AND calibre IS NOT NULL AND calibre > 0 {$sWhere}) AS calibracion,
					(SELECT AVG(manos) FROM produccion_historica WHERE tipo = 'PROC' AND cinta = produccion.cinta AND cinta != 'n/a' AND manos IS NOT NULL AND manos > 0 {$sWhere}) AS manos,
					(SELECT AVG(dedos) FROM produccion_historica WHERE tipo = 'PROC' AND cinta = produccion.cinta AND cinta != 'n/a' AND dedos IS NOT NULL AND dedos > 0 {$sWhere}) AS dedos
				FROM(
                    SELECT *
                    FROM (
                        SELECT cinta , produccion.edad, class
                        FROM produccion_historica produccion
                        LEFT JOIN produccion_colores colores ON cinta = color
                        WHERE 1 = 1  {$sWhere}
                        GROUP BY cinta
                        UNION ALL
                        SELECT b.color AS cinta, b.edad, class
                        FROM racimos_cosechados_by_color b
                        LEFT JOIN produccion_colores c ON b.color = c.color
                        WHERE 1 = 1 {$sWhere}
                    ) AS tbl
                    GROUP BY cinta
                ) AS produccion";
		$response->data = $this->db->queryAll($sql);
		foreach($response->data as $i => $val){
			$val->expanded = false;
            $val->cosechados = $val->procesados + $val->recusados;
			$val->lotes = $this->db->queryAll("SELECT lote, 
                    IFNULL((SELECT SUM(IF(blz > form, blz, form)) FROM produccion_racimos_cuadrado WHERE fecha = '{$filters->fecha_inicial}' AND cinta = '{$val->cable}' AND lote = produccion.lote AND status = 'RECU'),
					    (SELECT COUNT(1) FROM produccion_historica WHERE tipo = 'RECUSADO' AND cinta = '{$val->cable}' AND lote = produccion.lote {$sWhere})) AS recusados,
                    IFNULL((SELECT SUM(IF(blz > form, blz, form)) FROM produccion_racimos_cuadrado WHERE fecha = '{$filters->fecha_inicial}' AND cinta = '{$val->cable}' AND lote = produccion.lote AND status = 'PROC'),
					    (SELECT COUNT(1) FROM produccion_historica WHERE tipo = 'PROC' AND cinta = '{$val->cable}' AND lote = produccion.lote {$sWhere})) AS procesados, 
                    (SELECT COUNT(1) FROM produccion_historica WHERE tipo = 'PROC' AND cinta = '{$val->cable}' AND lote = produccion.lote AND calibre > 0 {$sWhere}) AS muestreados, 
                    IFNULL((SELECT IF(SUM(blz) = 0, (SELECT AVG(peso) FROM produccion_historica WHERE tipo = 'PROC' {$sWhere}), NULL) FROM produccion_racimos_cuadrado WHERE fecha = '{$filters->fecha_inicial}' AND cinta = '{$val->cable}' AND lote = produccion.lote),
					    (SELECT AVG(peso) FROM produccion_historica WHERE tipo = 'PROC' AND cinta = '{$val->cable}' AND lote = produccion.lote {$sWhere})) AS peso,
					(SELECT AVG(calibre) FROM produccion_historica WHERE tipo = 'PROC' AND cinta = '{$val->cable}' AND cinta != 'n/a' AND lote = produccion.lote AND calibre IS NOT NULL AND calibre > 0 {$sWhere}) AS calibracion,
					(SELECT AVG(manos) FROM produccion_historica WHERE tipo = 'PROC' AND cinta = '{$val->cable}' AND cinta != 'n/a' AND lote = produccion.lote AND manos IS NOT NULL AND manos > 0 {$sWhere}) AS manos,
					(SELECT AVG(dedos) FROM produccion_historica WHERE tipo = 'PROC' AND cinta = '{$val->cable}' AND cinta != 'n/a' AND lote = produccion.lote AND dedos IS NOT NULL AND dedos > 0 {$sWhere}) AS dedos
				FROM(
                    SELECT *
                    FROM(
                        SELECT lote
                        FROM produccion_historica
                        WHERE 1=1 AND cinta = '{$val->cable}' {$sWhere}
                        GROUP BY lote
                        UNION ALL
                        SELECT lote
                        FROM racimos_cosechados_by_color
                        WHERE 1=1 AND color = '{$val->cable}' {$sWhere}
                        GROUP BY lote
                    ) AS tbl
                    GROUP BY lote
                ) AS produccion");
			foreach($val->lotes as $key => $value){
				$value->expanded = false;
				$value->cosechados = $value->procesados + $value->recusados;

				$value->cuadrillas = $this->db->queryAll("SELECT cuadrilla, 
						(SELECT COUNT(1) FROM produccion_historica WHERE tipo = 'RECUSADO' AND cinta = '{$val->cable}' AND lote = '{$value->lote}' AND cuadrilla = produccion.cuadrilla {$sWhere}) AS recusados,
						(SELECT COUNT(1) FROM produccion_historica WHERE tipo = 'PROC' AND cinta = '{$val->cable}' AND lote = '{$value->lote}' AND cuadrilla = produccion.cuadrilla {$sWhere}) AS procesados, 
                        (SELECT COUNT(1) FROM produccion_historica WHERE tipo = 'PROC' AND cinta = '{$val->cable}' AND lote = '{$value->lote}' AND cuadrilla = produccion.cuadrilla AND calibre > 0 {$sWhere}) AS muestreados, 
						(SELECT AVG(peso) FROM produccion_historica WHERE tipo = 'PROC' AND cinta = '{$val->cable}' AND lote = '{$value->lote}' AND cuadrilla = produccion.cuadrilla {$sWhere}) AS peso,
						(SELECT AVG(calibre) FROM produccion_historica WHERE tipo = 'PROC' AND cinta = '{$val->cable}' AND cinta != 'n/a' AND lote = '{$value->lote}' AND cuadrilla = produccion.cuadrilla AND calibre IS NOT NULL AND calibre > 0 {$sWhere}) AS calibracion,
						(SELECT AVG(manos) FROM produccion_historica WHERE tipo = 'PROC' AND cinta = '{$val->cable}' AND cinta != 'n/a' AND lote = '{$value->lote}' AND cuadrilla = produccion.cuadrilla AND manos IS NOT NULL AND manos > 0 {$sWhere}) AS manos,
						(SELECT AVG(dedos) FROM produccion_historica WHERE tipo = 'PROC' AND cinta = '{$val->cable}' AND cinta != 'n/a' AND lote = '{$value->lote}' AND cuadrilla = produccion.cuadrilla AND dedos IS NOT NULL AND dedos > 0 {$sWhere}) AS dedos
					FROM(
					SELECT cuadrilla
					FROM produccion_historica
					WHERE 1=1 AND cinta = '{$val->cable}' AND lote = '{$value->lote}' {$sWhere}
					GROUP BY cuadrilla) AS produccion");
				foreach($value->cuadrillas as $row){
					$row->cosechados = $row->procesados + $row->recusados;
				}
			}
		}
		return $response;
	}

	public function promediosLotes(){
		$postdata = (object)json_decode(file_get_contents("php://input"));

		$filters = (object)[
			"finca" => getValueFrom($postdata , "finca" , "" , FILTER_SANITIZE_STRING),
			"fecha_inicial" => getValueFrom($postdata , "fecha_inicial" , "" , FILTER_SANITIZE_STRING),
			"fecha_final" => getValueFrom($postdata , "fecha_final" , "" , FILTER_SANITIZE_STRING),
		];

		if($filters->fecha_final != "" && $filters->fecha_inicial != ""){
			$sWhere .= " AND fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}'";
		}

		$response = new stdClass;
		$response->data = $this->db->queryAll("SELECT lote, 
				ROUND((SELECT AVG(edad) FROM produccion_historica WHERE tipo = 'PROC' AND lote = produccion.lote {$sWhere} AND edad > 0), 2) AS edad, 
                #TAG - cuadrar
                IFNULL((SELECT SUM(IF(blz > form, blz, form))
                        FROM produccion_racimos_cuadrado
                        WHERE fecha = '{$filters->fecha_inicial}' AND lote = produccion.lote), (SELECT COUNT(1) FROM produccion_historica WHERE tipo = 'PROC' AND lote = produccion.lote {$sWhere})) AS racimos, 
				ROUND((SELECT AVG(peso) FROM produccion_historica WHERE tipo = 'PROC' AND lote = produccion.lote {$sWhere} AND peso > 0), 2) AS peso, 
				ROUND((SELECT AVG(calibre) FROM produccion_historica WHERE tipo = 'PROC' AND lote = produccion.lote {$sWhere} AND calibre > 0 AND cinta != 'n/a'), 2) AS calibre, 
				ROUND((SELECT AVG(manos) FROM produccion_historica WHERE tipo = 'PROC' AND lote = produccion.lote {$sWhere} AND manos > 0 AND cinta != 'n/a'), 2) AS manos, 
				ROUND((SELECT AVG(dedos) FROM produccion_historica WHERE tipo = 'PROC' AND lote = produccion.lote {$sWhere} AND dedos > 0 AND cinta != 'n/a'), 2) AS dedos
			FROM(
				SELECT lote
				FROM produccion_historica h
				WHERE tipo = 'PROC' {$sWhere}
				GROUP BY lote) AS produccion");
		return $response;
    }
    
    public function racimosFolmularios(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $filters = (object)[
            "finca" => getValueFrom($postdata , "finca" , "" , FILTER_SANITIZE_STRING),
            "fecha_inicial" => getValueFrom($postdata , "fecha_inicial" , "" , FILTER_SANITIZE_STRING),
            "fecha_final" => getValueFrom($postdata , "fecha_final" , "" , FILTER_SANITIZE_STRING),
        ];

        if($filters->fecha_final != "" && $filters->fecha_inicial != ""){
            $sWhere .= " AND DATE(fecha) BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}'";
        }

        $response = new stdClass;
        
        /* BEGIN TABLA POR LOTE */
        $response->racimos_formularios = $this->db->queryAll("SELECT lote, 
                                                    SUM(racimos_cosechados) AS cosechados, 
                                                    (SELECT COUNT(1) 
                                                        FROM racimos_cosechados
                                                        INNER JOIN racimos_cosechados_detalle ON racimos_cosechados.id = racimos_cosechados_detalle.`id_racimos_cosechados`
                                                        WHERE lote = tbl.lote {$sWhere}) AS recusados
                                        FROM racimos_cosechados tbl
                                        WHERE 1=1 $sWhere AND lote != ''
                                        GROUP BY lote");
        
        $suma_edades = 0;
        $cantidad = 0;
        $nombres_cintas = ["roja" => "ROJA", "lila" => "LILA", "negra" => "NEGRO", "verde" => "VERDE", "azul" => "AZUL", "amarilla" => "AMARILLO", "blanca" => "BLANCO", "cafe" => "CAFE"];
        foreach($response->racimos_formularios as $row){
            $colores = $this->db->queryRow("SELECT SUM(roja) AS roja, SUM(lila) AS lila, SUM(negra) AS negra, SUM(verde) AS verde, SUM(azul) AS azul, SUM(amarilla) AS amarilla, SUM(blanca) AS blanca, SUM(cafe) AS cafe
                                            FROM racimos_cosechados 
                                            WHERE lote = '{$row->lote}' {$sWhere}");
            $colores = (array) $colores;
            $in = [];
            $d_suma_edades = 0;
            $d_cantidad = 0;
            foreach($colores as $col => $val){
                if($val > 0){
                    $cinta = $nombres_cintas[$col];
                    $in[] = $cinta;
                    $detalle = $this->db->queryRow("SELECT '{$cinta}' AS cinta, SUM($col) AS cosechados,
                                                                (SELECT COUNT(1) 
                                                                        FROM racimos_cosechados
                                                                        INNER JOIN racimos_cosechados_detalle ON racimos_cosechados.id = racimos_cosechados_detalle.`id_racimos_cosechados`
                                                                        WHERE lote = tbl.lote AND UPPER(TRIM(color_cinta)) = '{$col}' {$sWhere}) AS recusados,
                                                                IFNULL((SELECT edad FROM produccion_historica WHERE cinta = '{$cinta}' AND fecha = tbl.fecha LIMIT 1), getEdadCinta(WEEK(fecha), '{$cinta}', YEAR(fecha))) AS edad
                                                    FROM racimos_cosechados tbl
                                                    WHERE lote = '{$row->lote}' {$sWhere}");
                    $class = $this->db->queryOne("SELECT class FROM produccion_colores WHERE color = '{$cinta}'");
                    $detalle->class = $class;
                    $detalle->procesados = $detalle->cosechados - $detalle->recusados;

                    $suma_edades += ($detalle->edad * $detalle->procesados);
                    $cantidad += $detalle->procesados;

                    $d_suma_edades += ($detalle->edad * $detalle->procesados);
                    $d_cantidad += $detalle->procesados;

                    $detalle->detalle = $this->db->queryAll("SELECT palanca, SUM($col) AS cosechados,
                                                                            (SELECT COUNT(1) 
                                                                                FROM racimos_cosechados
                                                                                INNER JOIN racimos_cosechados_detalle ON racimos_cosechados.id = racimos_cosechados_detalle.`id_racimos_cosechados`
                                                                                WHERE lote = tbl.lote AND color_cinta = '{$col}' AND palanca = tbl.palanca {$sWhere}) AS recusados
                                                                FROM racimos_cosechados tbl
                                                                WHERE lote = '{$row->lote}' {$sWhere}
                                                                GROUP BY palanca
                                                                HAVING cosechados > 0");
                    foreach($detalle->detalle as $p){
                        $p->procesados = $p->cosechados - $p->recusados;
                    }

                    $row->detalle[] = $detalle;
                }
            }
            $in = "'".implode("','", $in)."'";

            $row->edad = $d_suma_edades / $d_cantidad;
            $row->procesados = round($row->cosechados - $row->recusados, 2);
        }
        $response->prom_edad = $suma_edades / $cantidad;
        /* END TABLE POR LOTE */

        /* BEGIN TABLA POR PALANCA */
        $response->table_palanca = $this->db->queryAll("SELECT palanca as cuadrilla, 
                    SUM(roja + lila + negra + verde + azul + blanca + amarilla + cafe) AS proc,
                    SUM((SELECT COUNT(1) FROM racimos_cosechados_detalle WHERE id_racimos_cosechados = racimos_cosechados.id)) AS recu
            FROM racimos_cosechados
            WHERE 1=1 {$sWhere}
            GROUP BY palanca");
        foreach($response->table_palanca as $row){
            $row->proc = $row->proc - $row->recu;
            $row->cose = $row->proc + $row->recu;
        }
        /* END TABLA POR PALANCA */

        /* BEGIN TABLA POR EDAD */
        $edades = $this->db->queryAll("SELECT YEAR(fecha) AS year, WEEK(fecha) AS sem, SUM(roja) AS roja, SUM(lila) AS lila, SUM(negra) AS negra, SUM(verde) AS verde, SUM(azul) AS azul, SUM(amarilla) AS amarilla, SUM(blanca) AS blanca, SUM(cafe) AS cafe
            FROM racimos_cosechados his
            WHERE 1=1 {$sWhere}
            GROUP BY WEEK(fecha)");
        foreach($edades as $row){
            foreach($row as $col => $val){
                if($col != 'sem' && $col != 'year')
                if($val > 0){
                    $edad = $this->db->queryRow("SELECT getEdadCinta($row->sem, '{$nombres_cintas[$col]}', $row->year) AS edad,
                                                        (SELECT class FROM produccion_colores WHERE '$nombres_cintas[$col]' = color) AS class");
                    $item = [
                        "col" => $col,
                        "cinta" => $nombres_cintas[$col],
                        "edad" => $edad->edad,
                        "class" => $edad->class,
                        "proc" => $val,
                        "recu" => $this->db->queryOne("SELECT COUNT(1) FROM racimos_cosechados INNER JOIN racimos_cosechados_detalle ON racimos_cosechados.id = id_racimos_cosechados WHERE color_cinta = '{$nombres_cintas[$col]}' {$sWhere}")
                    ];
                    $item["proc"] -= $item["recu"];
                    $item["cose"] = $item["proc"] + $item["recu"];
                    $response->table_edades[] = $item;
                }
            }
        }
        /* END TABLA POR EDAD */
        return $response;
    }

    public function eliminar(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        if(count($postdata->ids) > 0){
            foreach($postdata->ids as $reg){
                $this->db->query("DELETE FROM produccion_historica WHERE id = $reg->id");
            }
            return true;
        }
        return false;
    }
    
    public function editar(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        if(isset($postdata->id)){
            if($postdata->peso > 0 && $postdata->lote != "" && $postdata->cuadrilla != "" && $postdata->edad > 0 && $postdata->tipo != ""){
                $this->db->query("INSERT INTO update_produccion_historica(id, fecha, hora, id_finca, finca, idv, racimo, num_racimo, grupo_racimo, peso, lote, cinta, edad, cuadrilla, manos, calibre, dedos, tipo, nivel, causa, timestamp, semana, id_balanza)
                                    SELECT id, fecha, hora, id_finca, finca, idv, racimo, num_racimo, grupo_racimo, peso, lote, cinta, edad, cuadrilla, manos, calibre, dedos, tipo, nivel, causa, timestamp, semana, id_balanza
                                    FROM produccion_historica
                                    WHERE id = {$postdata->id}");
                $this->db->query("UPDATE produccion_historica SET
                                        peso = $postdata->peso,
                                        edad = $postdata->edad,
                                        cinta = getCintaFromEdad($postdata->edad, semana, YEAR(fecha)),
                                        cuadrilla = '{$postdata->cuadrilla}',
                                        lote = '{$postdata->lote}',
                                        tipo = '{$postdata->tipo}',
                                        causa = '{$postdata->causa}',
                                        manos = '{$postdata->manos}',
                                        calibre = '{$postdata->calibre}',
                                        dedos = '{$postdata->dedos}'
                                    WHERE id = {$postdata->id}");
            }
            return $this->db->queryRow("SELECT h.id, fecha, lote, causa, peso, manos, calibre, cinta, dedos, h.edad, cuadrilla, hora, tipo, class
                                        FROM produccion_historica h
                                        INNER JOIN produccion_colores c ON h.cinta = c.color
                                        WHERE h.id = {$postdata->id}");
        }
        return false;
    }

    public function cuadreRacimos(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $filters = (object)[
            "finca" => getValueFrom($postdata , "finca" , "" , FILTER_SANITIZE_STRING),
            "fecha_inicial" => getValueFrom($postdata , "fecha_inicial" , "" , FILTER_SANITIZE_STRING),
            "fecha_final" => getValueFrom($postdata , "fecha_final" , "" , FILTER_SANITIZE_STRING),
        ];
        $peso_prom = $this->db->queryOne("SELECT ROUND(AVG(peso), 2) FROM produccion_historica WHERE fecha = '{$filters->fecha_inicial}' AND tipo = 'PROC'");

        $response->cuadrado = $this->db->queryOne("SELECT COUNT(1) FROM produccion_racimos_cuadrado WHERE fecha = '{$filters->fecha_inicial}'");
        $sql = "SELECT lote, edad, cinta, class, blz,
                    (SELECT SUM(cantidad) FROM racimos_cosechados_by_color WHERE lote = tbl.lote AND color = cinta AND fecha = '{$filters->fecha_inicial}') -
                        (SELECT COUNT(1) FROM racimos_cosechados r INNER JOIN racimos_cosechados_detalle ON r.id = id_racimos_cosechados WHERE DATE(fecha) = '{$filters->fecha_inicial}' AND lote = tbl.lote AND color_cinta = tbl.cinta) AS form,
                    '{$peso_prom}' AS peso_prom
                FROM(
                    SELECT lote, edad, cinta, class, SUM(blz) AS blz
                    FROM (
                        SELECT lote, h.edad, cinta, c.class, COUNT(1) AS blz
                        FROM produccion_historica h
                        INNER JOIN produccion_colores c ON c.color = cinta
                        WHERE fecha = '{$filters->fecha_inicial}' AND tipo = 'PROC'
                        GROUP BY lote, h.edad
                        UNION ALL
                        SELECT lote, h.edad, h.color AS cinta, c.class, 0 AS blz
                        FROM racimos_cosechados_by_color h
                        INNER JOIN produccion_colores c ON c.color = h.color
                        WHERE fecha = '{$filters->fecha_inicial}'
                        GROUP BY lote, h.edad
                    ) AS tbl
                    GROUP BY lote, edad
                ) AS tbl";
                
        $response->data = $this->db->queryAll($sql);

        $sql = "SELECT lote, edad, cinta, class, blz,
                    (SELECT COUNT(1) FROM racimos_cosechados r INNER JOIN racimos_cosechados_detalle ON r.id = id_racimos_cosechados WHERE lote = tbl.lote AND UPPER(color_cinta) = cinta AND DATE(fecha) = '{$filters->fecha_inicial}' AND status = 'RECU') AS form,
                    '{$peso_prom}' AS peso_prom
                FROM(
                    SELECT lote, h.edad, cinta, c.class, COUNT(1) AS blz
                    FROM produccion_historica h
                    INNER JOIN produccion_colores c ON c.color = cinta
                    WHERE fecha = '{$filters->fecha_inicial}' AND tipo = 'RECUSADO'
                    GROUP BY lote, h.edad
                ) AS tbl";
        $response->data_recu = $this->db->queryAll($sql);

        $response->faltante_recusados = [];
        foreach($response->data_recu as $row){
            if($row->form > $row->blz){
                $sql = "SELECT *, 
                            (SELECT COUNT(1) FROM produccion_historica WHERE causa = tbl.causa AND tipo = 'recusado' AND fecha = '{$filters->fecha_inicial}' AND lote = tbl.lote AND cinta = color_cinta) AS cantidad_blz,
                            '{$peso_prom}' AS peso_prom
                        FROM(
                            SELECT lote, d.`color_cinta`, causa, COUNT(1) AS cantidad
                            FROM racimos_cosechados r
                            INNER JOIN racimos_cosechados_detalle d ON r.id = d.id_racimos_cosechados
                            INNER JOIN produccion_colores c ON c.color = d.`color_cinta`
                            WHERE DATE(fecha) = '{$filters->fecha_inicial}' AND lote = '$row->lote'
                            GROUP BY lote, d.`color_cinta`, causa
                        ) AS tbl
                        HAVING cantidad > cantidad_blz";
                $response->faltante_recusados = array_merge($response->faltante_recusados, $this->db->queryAll($sql));
            }
        }
    
        return $response;
    }

    public function cuadrarRacimos(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $filters = (object)[
            "finca" => getValueFrom($postdata , "finca" , "" , FILTER_SANITIZE_STRING),
            "fecha_inicial" => getValueFrom($postdata , "fecha_inicial" , "" , FILTER_SANITIZE_STRING),
            "fecha_final" => getValueFrom($postdata , "fecha_final" , "" , FILTER_SANITIZE_STRING),
        ];

        $response->status = 400;
        $dia_procesado = $this->db->queryOne("SELECT COUNT(1) FROM produccion_racimos_cuadrado WHERE fecha = '{$filters->fecha_inicial}'");
        if($dia_procesado == 0){
            $response->status = 200;

            $data = $this->cuadreRacimos();
            $procesados = $data->data;
            $recusados = $data->data_recu;
            foreach($procesados as $row){
                $this->db->query("INSERT INTO produccion_racimos_cuadrado SET fecha = '{$filters->fecha_inicial}', lote = '{$row->lote}', cinta = '{$row->cinta}', blz = '$row->blz', form = '$row->form'");
            }
            foreach($recusados as $row){
                $this->db->query("INSERT INTO produccion_racimos_cuadrado SET fecha = '{$filters->fecha_inicial}', lote = '{$row->lote}', cinta = '{$row->cinta}', blz = '$row->blz', form = '$row->form', status = 'RECU'");
            }
        }

        return $response;
    }
}
