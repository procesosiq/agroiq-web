<?php defined('PHRAPI') or die("Direct access not allowed!");

class ProduccionCajas {
    public $name;
    private $db;
    private $config;

    public function __construct(){
        $this->config = $GLOBALS['config'];
        $this->session = Session::getInstance();
        $this->db = DB::getInstance($this->session->agent_user);
        $this->configCajas = $this->db->queryRow("SELECT umbral_excedente_cajas AS umbral_excedente FROM produccion_configuracion");
    }

    public function last(){
        $response = new stdClass;
        $response->days = $this->db->queryAllOne("SELECT fecha
            FROM (
                SELECT fecha FROM produccion_cajas WHERE fecha != '0000-00-00' GROUP BY fecha
                UNION ALL
                SELECT fecha FROM produccion_gavetas WHERE fecha != '0000-00-00' GROUP BY fecha
            ) tbl
            GROUP BY fecha
        ");
        $response->umbralExcedente = $this->configCajas->umbral_excedente;
        $response->tiposCajas = $this->db->queryAllOne("SELECT marca FROM (
                SELECT marca
                FROM produccion_cajas
                GROUP BY marca
                UNION ALL
                SELECT marca
                FROM produccion_gavetas
                GROUP BY marca
            ) AS tbl
            WHERE marca != 'n/a' AND marca != ''");
        $response->last = $this->db->queryRow("SELECT MAX(fecha) as fecha 
            FROM (
                SELECT MAX(fecha) as fecha 
                FROM produccion_gavetas 
                UNION ALL 
                SELECT MAX(fecha) 
                FROM produccion_cajas) AS tbl");
        return $response;
    }

    public function cuadreCajas(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $sql = "SELECT *, (SELECT SUM(valor) FROM produccion_cajas_real WHERE marca = tbl.marca AND fecha BETWEEN '{$postdata->fecha_inicial}' AND '{$postdata->fecha_final}') AS suma_real
                FROM(
                SELECT 'CAJA' AS tipo, produccion_cajas.marca, COUNT(1) AS balanza
                FROM produccion_cajas 
                WHERE fecha BETWEEN '{$postdata->fecha_inicial}' AND '{$postdata->fecha_final}'
                GROUP BY produccion_cajas.marca) AS tbl";
        $response->data = $this->db->queryAll($sql);
        $sql = "SELECT *, (SELECT SUM(valor) FROM produccion_cajas_real WHERE marca = tbl.marca AND fecha BETWEEN '{$postdata->fecha_inicial}' AND '{$postdata->fecha_final}') AS suma_real
                FROM(
                SELECT 'GAVETA' AS tipo, marca, COUNT(1) AS balanza
                FROM produccion_gavetas
                WHERE fecha BETWEEN '{$postdata->fecha_inicial}' AND '{$postdata->fecha_final}'
                GROUP BY marca
                UNION ALL
                SELECT 'GAVETA' AS tipo, marca, 0 AS balanza
                FROM produccion_cajas_real
                WHERE fecha BETWEEN '{$postdata->fecha_inicial}' AND '{$postdata->fecha_final}'
                    AND marca NOT IN(SELECT marca FROM produccion_gavetas WHERE fecha BETWEEN '{$postdata->fecha_inicial}' AND '{$postdata->fecha_final}' GROUP BY marca)
                    AND marca IN ('MI COMISARIATO', 'TIA')
                GROUP BY marca
                ) AS tbl";
        $data_gavetas = $this->db->queryAll($sql);
        $response->data = array_merge($response->data, $data_gavetas);

        foreach($response->data as $row){
            $row->detalle = $this->db->queryAll("SELECT guia, SUM(valor) AS valor FROM produccion_cajas_real WHERE fecha BETWEEN '{$postdata->fecha_inicial}' AND '{$postdata->fecha_final}' AND marca = '{$row->marca}' GROUP BY guia");
            if($row->balanza > 0 && $row->suma_real > 0)
                $row->porcentaje = $row->balanza / $row->suma_real * 100;
        }

        $response->guias = $this->db->queryAll("SELECT SUM(valor) AS cajas, guia, fecha, codigo_productor, codigo_magap, sello_seguridad FROM produccion_cajas_real WHERE fecha BETWEEN '{$postdata->fecha_inicial}' AND '{$postdata->fecha_final}' GROUP BY guia");
        foreach($response->guias as $row){
            $row->detalle = $this->db->queryAll("SELECT marca, SUM(valor) AS valor FROM produccion_cajas_real WHERE fecha BETWEEN '{$postdata->fecha_inicial}' AND '{$postdata->fecha_final}' AND guia = '{$row->guia}' GROUP BY marca");
        }
        return $response;
    }

    public function guardarCuadrar(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));

        if(isset($postdata->fecha) && $postdata->fecha != ""){
            $marcasRegistradas =  $this->db->queryAllOne("SELECT marca FROM produccion_cajas_real WHERE fecha = '{$postdata->fecha}' AND guia = '{$postdata->guia}'");
            $postdata->marca = (array) $postdata->marca;
            $marcasNuevas = array_keys($postdata->marca);

            foreach($postdata->marca as $marca => $value){
                if($marca != '' && $value > 0){
                    $exits = in_array($marca, $marcasRegistradas);
                    if($exits){
                        $this->db->query("UPDATE produccion_cajas_real SET valor = '{$value}' WHERE marca = '{$marca}' AND fecha = '{$postdata->fecha}' AND guia = '{$postdata->guia}'");
                        $response->message = "Se actualizo con éxito";
                    }else{
                        $this->db->query("INSERT INTO produccion_cajas_real SET valor = '{$value}', marca = '{$marca}', fecha = '{$postdata->fecha}', guia = '{$postdata->guia}', codigo_productor = '{$postdata->productor}', codigo_magap = '{$postdata->magap}', sello_seguridad = '{$postdata->sello_seguridad}'");
                        $response->message = "Se inserto con éxito";
                    }
                }
            }

            foreach($marcasRegistradas as $marca) {
                if(!in_array($marca, $marcasNuevas) || (in_array($marca, $marcasNuevas) && $postdata->marca[$marca] == 0)){
                    $this->db->query("DELETE FROM produccion_cajas_real WHERE fecha = '{$postdata->fecha}' AND guia = '{$postdata->guia}' AND marca = '{$marca}'");
                }
            }
        }
        return $response;
    }

    public function procesar(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));

        $marcas = $this->db->queryAll("SELECT marca, SUM(valor) AS cantidad FROM produccion_cajas_real WHERE fecha = '{$postdata->fecha}' GROUP BY marca");
        foreach($marcas as $mr){
            $blz = $this->db->queryRow("SELECT SUM(cantidad) AS cantidad, lb_prom, kg_prom
                                        FROM(
                                            SELECT COUNT(1) AS cantidad, AVG(lb) as lb_prom, AVG(kg) AS kg_prom
                                            FROM produccion_cajas 
                                            WHERE fecha = '{$postdata->fecha}' AND marca = '{$mr->marca}'
                                            UNION ALL
                                            SELECT COUNT(1) AS cantidad, AVG(lb) as lb_prom, AVG(kg) AS kg_prom
                                            FROM produccion_gavetas
                                            WHERE fecha = '{$postdata->fecha}' AND marca = '{$mr->marca}'
                                        ) AS tbl");

            if($blz->cantidad < $mr->cantidad){
                $this->db->query("UPDATE produccion_cajas_real SET status = 'PROCESADO' WHERE fecha = '{$postdata->fecha}' AND marca = '{$mr->marca}'");
            
                //INSERTAR DIFERENCIA
                $diff = $mr->cantidad - $blz->cantidad;
                $table = 'produccion_cajas';
                if(in_array($mr->marca, ['TIA', 'MI COMISARIATO'])) $table = 'produccion_gavetas';

                for($x = 0; $x < $diff; $x++){
                    if(!$blz->lb_prom > 0){
                        $last_fecha = $this->db->queryOne("SELECT MAX(fecha) FROM {$table} WHERE fecha < '{$postdata->fecha}'");
                        $blz->lb_prom = $this->db->queryOne("SELECT AVG(lb) FROM {$table} WHERE fecha = '{$last_fecha}'");
                        $blz->kg_prom = $this->db->queryOne("SELECT AVG(kg) FROM {$table} WHERE fecha = '{$last_fecha}'");
                    }
                    $this->db->query("INSERT INTO {$table} SET
                                        fecha = '{$postdata->fecha}',
                                        semana = getWeek('{$postdata->fecha}'),
                                        year = getYear('{$postdata->fecha}'),
                                        marca = '{$mr->marca}',
                                        lb = {$blz->lb_prom},
                                        kg = {$blz->kg_prom},
                                        origin = 'cuadre'");
                }
            }
        }
        return $response;
    }

    public function eliminar(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        if(count($postdata->ids) > 0){
            $gavetas = $this->db->queryAllOne("SELECT marca FROM produccion_gavetas WHERE marca != '' AND marca != 'n/a' GROUP BY marca");
            foreach($postdata->ids as $reg){
                if(in_array($reg->marca, $gavetas)){
                    #D("DELETE FROM produccion_gavetas WHERE id = $reg->id");
                    $this->db->query("DELETE FROM produccion_gavetas WHERE id = $reg->id");
                }else
                    $this->db->query("DELETE FROM produccion_cajas WHERE id = $reg->id");
            }
            return true;
        }
        return false;
    }

    public function filters(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));
        if(isset($postdata->year) && $postdata->year != ""){
            $sYear = " AND year = '{$postdata->year}'";
        }

        $response->years = $this->db->queryAllSpecial("SELECT year as id, year as label FROM(
            SELECT year FROM produccion_cajas GROUP BY year 
            UNION ALL
            SELECT year FROM produccion_gavetas GROUP BY year
            ) AS tbl
            WHERE year > 0
            GROUP BY year");
        
        $sql = "SELECT semana as id, semana as label FROM(
            SELECT semana FROM produccion_cajas  WHERE 1=1 $sYear GROUP BY semana 
            UNION ALL
            SELECT semana FROM produccion_gavetas WHERE 1=1 $sYear GROUP BY semana
            ) AS tbl
            GROUP BY semana";
        $response->semanas = $this->db->queryAll($sql);
        return $response;
    }

    public function registros(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $filters = (object)[
            'actualPage' => (int) getValueFrom($postdata, 'actualPage', 0),
            'pagination' => (int) getValueFrom($postdata, 'pagination', 10),
            'fecha_inicial' => getValueFrom($postdata, 'fecha_inicial', ''),
            'fecha_final' => getValueFrom($postdata, 'fecha_final', '')
        ];

        $response = new stdClass;
        $response->marcas = $this->db->queryAllOne("SELECT marca FROM produccion_cajas WHERE marca != '' AND marca != 'n/a' GROUP BY marca UNION ALL SELECT marca FROM produccion_gavetas WHERE marca != '' AND marca != 'n/a' GROUP BY marca");
        $response->data = $this->db->queryAll("SELECT * FROM (
            SELECT c.id, fecha, marca, {$postdata->unidad} AS peso, 'GAVETA' AS tipo, hora, maximo_{$postdata->unidad} AS maximo, minimo_{$postdata->unidad} AS minimo
            FROM produccion_gavetas c
            LEFT JOIN produccion_tipo_caja ON nombre = marca
            WHERE fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}'
            UNION ALL
            SELECT c.id, fecha, marca, {$postdata->unidad} AS peso, 'CAJA' AS tipo, hora, maximo_{$postdata->unidad} AS maximo, minimo_{$postdata->unidad} AS minimo
            FROM produccion_cajas c
            LEFT JOIN produccion_tipo_caja ON nombre = marca
            WHERE fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}') AS tbl
            ORDER BY fecha DESC, hora DESC");
        $response->numPages = $this->db->queryRow("SELECT CEIL(COUNT(1)/10) as num
            FROM (
            SELECT id
            FROM produccion_gavetas
            WHERE fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}'
            UNION ALL
            SELECT id
            FROM produccion_cajas
            WHERE fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}') AS tbl")->num;
        
        return $response;   
    }

    public function guardarCaja(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;
        $response->status = 400;

        if($postdata->id > 0){
            $new_table = $this->db->queryOne("SELECT COUNT(1) FROM produccion_gavetas WHERE marca = '{$postdata->marca}' AND marca != '' AND marca != 'n/a'") == 0 ? 'produccion_cajas' : 'produccion_gavetas';
            if($postdata->tipo == 'GAVETA' && $new_table == 'produccion_cajas'){
                // PASO DE SER GAVETA A CAJA
                $this->db->query("INSERT INTO produccion_cajas (id_balanza_cajas, id_finca, finca, semana, year, caja, tipo_caja, marca, fecha, hora)
                                    SELECT id_balanza_cajas, id_finca, finca, semana, year, peso, '{$postdata->marca}', '{$postdata->marca}', fecha, hora
                                    FROM produccion_gavetas
                                    WHERE id = $postdata->id");
                $response->registro->tipo = 'CAJA';
                $response->registro->id = $this->db->getLastID();
                $postdata->id = $response->registro->id;
                $this->db->query("DELETE FROM produccion_gavetas WHERE id = $postdata->id");
            }else if($postdata->tipo == 'CAJA' && $new_table == 'produccion_gavetas'){
                // PASO DE SER CAJA A GAVETA
                $this->db->query("INSERT INTO produccion_gavetas (id_balanza_cajas, id_finca, finca, semana, year, peso, marca, fecha, hora)
                                    SELECT id_balanza_cajas, id_finca, finca, semana, year, caja, '{$postdata->marca}', fecha, hora
                                    FROM produccion_cajas
                                    WHERE id = $postdata->id");
                $response->registro->tipo = 'GAVETA';
                $response->registro->id = $this->db->getLastID();
                $postdata->id = $response->registro->id;
                $this->db->query("DELETE FROM produccion_cajas WHERE id = $postdata->id");
            }

            $response->status = 200;
            if($postdata->unidad == "lb")
                $sql_add = ", lb = {$postdata->peso}, kg = ROUND({$postdata->peso} * 0.4536, 2)";
            else if($postdata->unidad == "kg")
                $sql_add = ", kg = {$postdata->peso}, lb = ROUND({$postdata->peso} / 0.4536, 2)";
            if($new_table == 'produccion_cajas')
                $sql_add .= ", tipo_caja = '{$postdata->marca}'";

            $this->db->query("UPDATE {$new_table} SET marca = '{$postdata->marca}' {$sql_add} WHERE id = {$postdata->id}");
        }else{
            $response->message = "Hubo un problema refresque la página y vuelva a intentar";
        }

        return $response;
    }

    public function guardarUmbral(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;
        $response->status = 200;

        if($postdata->umbral > 0){
            $this->db->query("UPDATE produccion_configuracion SET umbral_excedente_cajas = {$postdata->umbral}");
        }else{
            $response->message = "El umbral debe ser mayor a 0";
        }

        return $response;
    }

    public function resumenMarca(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $filters = (object)[
            'actualPage' => (int) getValueFrom($postdata, 'actualPage', 0),
            'pagination' => (int) getValueFrom($postdata, 'pagination', 10),
            'fecha_inicial' => getValueFrom($postdata, 'fecha_inicial', ''),
            'fecha_final' => getValueFrom($postdata, 'fecha_final', ''),
            'semana' => (int) getValueFrom($postdata, 'semana', 0),
        ];
        /*if($filters->semana > 0){
            $sWhere = " AND semana = {$filters->semana}";
        }*/

        $response = new stdClass;
        $sql = "SELECT marca, SUM(cantidad) AS cantidad, SUM(total_kg) AS total_kg, 
                    IF((SELECT COUNT(1) FROM produccion_cajas_convertir WHERE marca = tbl.marca) > 0,
                        ROUND(SUM(total_kg) / IF('{$postdata->unidad}' = 'lb' , 40.5, 18.37), 0), 
                        SUM(cantidad)
                    ) AS 'conv',  tipo, promedio, maximo, minimo, desviacion
                FROM(
                    SELECT
                        `cajas`.`marca` AS `marca`,
                        IF((`cajas_real`.`id` IS NOT NULL),`cajas_real`.`valor`, COUNT(1)) AS `cantidad`,
                        ROUND(SUM(cajas.`{$postdata->unidad}`) + IF(cajas_real.id IS NOT NULL, (cajas_real.valor - COUNT(1)) * AVG(cajas.{$postdata->unidad}), 0), 2) AS total_kg,
                        IF(conv.id IS NOT NULL, 
                            ROUND(((SUM(cajas.`kg`) + IF(cajas_real.id IS NOT NULL, (cajas_real.valor - COUNT(1)) * AVG(cajas.kg), 0)) / 0.4536) / 40.5, 0),
                            COUNT(1)) AS conv,
                        'CAJA' AS tipo,
                        ROUND(AVG(cajas.`{$postdata->unidad}`), 2) AS promedio,
                        MAX(cajas.`{$postdata->unidad}`) AS maximo,
                        MIN(cajas.`{$postdata->unidad}`) AS minimo,
                        ROUND(STD(cajas.{$postdata->unidad}), 2) AS desviacion
                    FROM (`produccion_cajas` `cajas`
                    LEFT JOIN `produccion_cajas_real` `cajas_real`
                        ON (((`cajas_real`.`fecha` = `cajas`.`fecha`)
                            AND (CONVERT(`cajas_real`.`marca` USING utf8) = `cajas`.`marca`))
                            AND status = 'PROCESADO'))
                    LEFT JOIN produccion_cajas_convertir conv ON cajas.marca = conv.marca
                    WHERE cajas.fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}' AND (`cajas`.`marca` <> '') $sWhere
                    GROUP BY `cajas`.`marca`, cajas_real.id
                    UNION ALL
                    SELECT
                        `cajas`.`marca` AS `marca`,
                        IF((`cajas_real`.`id` IS NOT NULL),`cajas_real`.`valor`, COUNT(1)) AS `cantidad`,
                        ROUND(SUM(cajas.`{$postdata->unidad}`) + IF(cajas_real.id IS NOT NULL, (cajas_real.valor - COUNT(1)) * AVG(cajas.{$postdata->unidad}), 0), 2) AS total_kg,
                        IF(conv.id IS NOT NULL, 
                            ROUND(((SUM(cajas.`kg`) + IF(cajas_real.id IS NOT NULL, (cajas_real.valor - COUNT(1)) * AVG(cajas.kg), 0)) / 0.4536) / 40.5, 0),
                            COUNT(1)) AS conv,
                        'GAVETA' AS tipo,
                        ROUND(AVG(cajas.`{$postdata->unidad}`), 2) AS promedio,
                        MAX(cajas.`{$postdata->unidad}`) AS maximo,
                        MIN(cajas.`{$postdata->unidad}`) AS minimo,
                        ROUND(STD(cajas.{$postdata->unidad}), 2) AS desviacion
                    FROM (`produccion_gavetas` `cajas`
                    LEFT JOIN `produccion_cajas_real` `cajas_real`
                        ON (((`cajas_real`.`fecha` = `cajas`.`fecha`)
                            AND (CONVERT(`cajas_real`.`marca` USING utf8) = `cajas`.`marca`))
                            AND status = 'PROCESADO'))
                    LEFT JOIN produccion_cajas_convertir conv ON cajas.marca = conv.marca
                    WHERE cajas.fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}' AND (`cajas`.`marca` <> '') $sWhere
                    GROUP BY `cajas`.`marca`, cajas_real.id
                    UNION ALL
                    (
                        SELECT tbl.marca, cantidad, (cantidad * promedio) AS total_kg,
                            IF(conver.id IS NOT NULL, 
                                    ROUND(((cantidad * promedio) / 0.4536) / 40.5, 0),
                                    COUNT(1)) AS conv,
                            tipo, promedio, promedio AS maximo, promedio AS minimo, promedio AS desviacion
                        FROM (
                            SELECT
                                `cajas_real`.`marca` AS `marca`,
                                cajas_real.`valor` AS `cantidad`,
                                (SELECT AVG({$postdata->unidad}) FROM produccion_gavetas WHERE cajas_real.fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}' AND marca != '') AS promedio,
                                'GAVETA' AS tipo
                            FROM produccion_cajas_real cajas_real
                            WHERE cajas_real.fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}' AND marca != ''  AND STATUS = 'PROCESADO'
                                AND marca NOT IN (SELECT marca FROM produccion_gavetas WHERE fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}')
                                AND marca IN ('TIA', 'MI COMISARIATO')
                        ) AS tbl
                        LEFT JOIN produccion_cajas_convertir conver ON tbl.marca = conver.marca
                        HAVING cantidad > 0
                    )
                ) AS tbl
                GROUP BY marca";
        $response->data = $this->db->queryAll($sql);
        
        
        /* LOAD TAGS */
        $response->tags = $this->tags($filters);
        $response->tags["cajas40"] = $this->sumOfValue($response->data, 'conv');

        return $response;
    }

    private function sumOfValue($data, $prop){
        $sum = 0;
        foreach($data as $row)
            if(is_object($row))
                $sum += (double) $row->{$prop};
            else
                $sum += (double) $row[$prop];
        return $sum;
    }

    public function guardarRegistro(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;
        $response->registro = $postdata;
        $response->status = 400;

        $gavetas = array_keys($this->db->queryAllSpecial("SELECT UPPER(marca) AS id, marca AS label FROM produccion_gavetas GROUP BY UPPER(marca)"));
        $cajas = array_keys($this->db->queryAllSpecial("SELECT UPPER(marca) AS id, marca AS label FROM produccion_cajas WHERE marca != '' AND marca != 'n/a' GROUP BY UPPER(marca)"));

        $marca = strtoupper($postdata->marca);
        $postdata->marca = $marca;
        if(in_array($marca, $gavetas) || in_array($marca, $cajas)){
            $response->status = 200;
            $nuevo_tipo = in_array($marca, $cajas) ? "CAJA" : "GAVETA";

            if($postdata->tipo == $nuevo_tipo){
                $this->db->query("UPDATE produccion_cajas SET marca = '{$postdata->marca}', tipo_caja = '{$postdata->marca}' WHERE id = $postdata->id");
            }else{
                if($nuevo_tipo == 'CAJA'){
                    $this->db->query("INSERT INTO produccion_cajas (id_balanza_cajas, id_finca, finca, semana, year, caja, tipo_caja, marca, fecha, hora)
                                        SELECT id_balanza_cajas, id_finca, finca, semana, year, peso, '{$postdata->marca}', '{$postdata->marca}', fecha, hora
                                        FROM produccion_gavetas
                                        WHERE id = $postdata->id");
                    $response->registro->tipo = 'CAJA';
                    $response->registro->id = $this->db->getLastID();
                    $this->db->query("DELETE FROM produccion_gavetas WHERE id = $postdata->id");
                }else{
                    $this->db->query("INSERT INTO produccion_gavetas (id_balanza_cajas, id_finca, finca, semana, year, peso, marca, fecha, hora)
                                        SELECT id_balanza_cajas, id_finca, finca, semana, year, caja, '{$postdata->marca}', fecha, hora
                                        FROM produccion_cajas
                                        WHERE id = $postdata->id");
                    $response->registro->tipo = 'GAVETA';
                    $response->registro->id = $this->db->getLastID();
                    $this->db->query("DELETE FROM produccion_cajas WHERE id = $postdata->id");
                }
            }
        }else{
            $response->message = "No es una marca valida";
        }
        return $response;
    }

    private function getSemanasVariable($var, $year){
        $round = 0;
        if($var == 'CONV/HA') $round = 2;
		$sql = [];
		for($i = 1; $i <= 53; $i++){
			$sql[] = "SELECT $i AS semana, ROUND(AVG(sem_{$i}), $round) cantidad FROM produccion_resumen_tabla WHERE sem_{$i} > 0 AND variable = '$var' AND anio = $year";
		}
		return implode("
		UNION ALL
		", $sql);
	}

    public function historicoCajasSemanal(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));

        $namesVar = [
            'CONV' => 'CAJAS 40.5',
            'CONV/HA' => 'CONV/HA'
        ];

        $data_chart = $this->db->queryAll("SELECT semana AS label_x, sum(cantidad) as value, '{$postdata->var}', 0 index_y
            FROM(
                {$this->getSemanasVariable("{$namesVar[$postdata->var]}", $postdata->year)}
            ) AS tbl
            WHERE cantidad > 0
            GROUP BY label_x
            ORDER BY label_x");

        $groups = [
            [
                "name" => $postdata->var,
                "type" => 'line',
                'format' => ''
            ]
        ];
        $response->chart = $this->grafica_z($data_chart, $groups);
        return $response;
    }

    public function getMarcas(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));
        if(isset($postdata->year) && $postdata->year != ""){
            $sYear = " AND year = '{$postdata->year}'";
        }

        $response->data = $this->db->queryAllSpecial("SELECT marca as id, marca as label FROM(
            SELECT marca
            FROM produccion_cajas
            WHERE 1=1 $sYear
            GROUP BY marca
        ) AS tbl");
        return $response;
    }

    public function getGuias(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));
        
        if($postdata->guia != "" && $postdata->fecha != ""){
            $response->marcas = $this->db->queryAll("SELECT marca, valor FROM produccion_cajas_real WHERE fecha = '{$postdata->fecha}' AND guia = '{$postdata->guia}'");
        }
        return $response;
    }

    public function tags($filters){
        $response = [];
        /*if($filters->semana > 0){
            $sWhere = " AND semana = {$filters->semana}";
        }*/
        $rows = $this->db->queryAll("SELECT hora, fecha
                                    FROM(
                                        SELECT hora, fecha
                                        FROM produccion_cajas 
                                        WHERE fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}' AND marca != '' $sWhere
                                        UNION ALL
                                        SELECT hora, fecha
                                        FROM produccion_gavetas 
                                        WHERE fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}' AND marca != '' $sWhere
                                    ) AS tbl
                                    ORDER BY hora");
        if(count($rows)>0){
            $p = $rows[0];
            $u = $rows[count($rows)-1];
            $response["primera_caja"] = $p->hora;
            $response["ultima_caja"] = $u->hora;
            $response["fecha_primera"] = $p->fecha;
            $response["fecha_ultima"] = $u->fecha;
            $response["diferencia"] = $this->db->queryRow("SELECT TIMEDIFF('{$u->hora}}',  '{$p->hora}') AS dif")->dif;
        }else{
            $response["primera_caja"] = "";
            $response["ultima_caja"] = "";
            $response["fecha_primera"] = "";
            $response["fecha_ultima"] = "";
        }
        return $response;
    }

    public function graficasBarras(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));

        $response->marcas = $this->db->queryAll("SELECT marca , sec, minimo, maximo
                                                FROM (
                                                    SELECT marca, 1 AS sec, minimo_{$postdata->unidad} AS minimo, maximo_{$postdata->unidad} AS maximo FROM produccion_cajas INNER JOIN produccion_tipo_caja ON nombre = marca WHERE fecha = '{$postdata->fecha_inicial}' GROUP BY marca
                                                    UNION ALL
                                                    SELECT marca, 2 AS sec, minimo_{$postdata->unidad} AS minimo, maximo_{$postdata->unidad} AS maximo FROM produccion_gavetas INNER JOIN produccion_tipo_caja ON nombre = marca WHERE fecha = '{$postdata->fecha_inicial}' GROUP BY marca
                                                ) AS tbl
                                                GROUP BY marca");

        $response->graficas = [];
        $response->pasteles = [];
        foreach($response->marcas as $marca){
            # BARRAS
            $response->graficas[$marca->marca] = (object)[
                "legends" => [],
                "series" => [
                    "Cantidad" => [
                        "name" => "Cantidad",
                        "connectNulls" => true,
                        "type" => "bar",
                        "itemStyle" => [
                            "normal" => [
                                "barBorderRadius" => 0,
                                "barBorderWidth" => 6
                            ]
                        ],
                        "label" => [
                            "normal" => [
                                "show" => true,
                                "position" => "inside"
                            ]
                        ],
                        "data" => [],
                    ]
                ]
            ];
            $pesos = $this->db->queryAllSpecial("SELECT peso AS id, cantidad AS label
                                                FROM(
                                                    SELECT {$postdata->unidad} AS peso, COUNT(1) AS cantidad FROM produccion_cajas WHERE fecha = '{$postdata->fecha_inicial}' AND marca = '{$marca->marca}' AND marca NOT IN ('PINALINDA BAG', 'SH PRATT') GROUP BY {$postdata->unidad}
                                                    UNION ALL
                                                    SELECT grupo AS peso, SUM(cantidad) AS cantidad
                                                    FROM (
                                                        SELECT {$postdata->unidad} AS peso, COUNT(1) AS cantidad, 
                                                            IF(FLOOR({$postdata->unidad})+0.5 <= {$postdata->unidad}, 
                                                                CONCAT(
                                                                    FLOOR({$postdata->unidad})+0.5,
                                                                    '-',
                                                                    FLOOR({$postdata->unidad})+1
                                                                ), 
                                                                CONCAT(
                                                                    FLOOR({$postdata->unidad}),
                                                                    '-',
                                                                    FLOOR({$postdata->unidad})+0.5
                                                                )
                                                            ) AS grupo
                                                        FROM produccion_cajas
                                                        WHERE fecha = '{$postdata->fecha_inicial}' AND marca = '{$marca->marca}' AND marca IN ('PINALINDA BAG', 'SH PRATT')
                                                        GROUP BY {$postdata->unidad}
                                                    ) AS tbl
                                                    GROUP BY grupo
                                                    UNION ALL
                                                    SELECT grupo AS peso, SUM(cantidad) AS cantidad
                                                    FROM (
                                                        SELECT {$postdata->unidad} AS peso, COUNT(1) AS cantidad, 
                                                            IF(FLOOR({$postdata->unidad})+0.5 <= {$postdata->unidad}, 
                                                                CONCAT(
                                                                    FLOOR({$postdata->unidad})+0.5,
                                                                    '-',
                                                                    -- CAST(FLOOR(peso)+1 AS DECIMAL(5,1))
                                                                    FLOOR({$postdata->unidad})+1
                                                                ), 
                                                                CONCAT(
                                                                    -- CAST(FLOOR(peso) AS DECIMAL(5,1)),
                                                                    FLOOR({$postdata->unidad}),
                                                                    '-',
                                                                    FLOOR({$postdata->unidad})+0.5
                                                                )
                                                            ) AS grupo
                                                        FROM produccion_gavetas
                                                        WHERE fecha = '{$postdata->fecha_inicial}' AND marca = '{$marca->marca}'
                                                        GROUP BY {$postdata->unidad}
                                                    ) AS tbl
                                                    GROUP BY grupo
                                                ) AS tbl");
            
            foreach($pesos as $peso => $cantidad){
                $response->graficas[$marca->marca]->legends[] = $peso;
                $response->graficas[$marca->marca]->series["Cantidad"]["umbral"] = ["max" => $marca->maximo, "min" => $marca->minimo];
                $response->graficas[$marca->marca]->series["Cantidad"]["data"][] = $cantidad;
            }
            
            # PASTELES
            $response->pasteles[$marca->marca] = [];
            $cajas1840 = $this->db->queryAllSpecial("SELECT nombre AS id, nombre AS label FROM produccion_tipo_caja WHERE maximo IS NOT NULL AND minimo IS NOT NULL");
            $table = $this->db->queryOne("SELECT name_table FROM produccion_tipo_caja WHERE nombre = '{$marca->marca}'");
            #$fieldPeso = $table == 'produccion_cajas' ? 'caja' : 'peso';

            if(in_array($marca->marca, $cajas1840)){
                $sql = "SELECT value, label
                        FROM (
                            SELECT '{$marca->minimo}-{$marca->maximo}' AS label, COUNT(1) AS value
                            FROM {$table}
                            WHERE marca = '{$marca->marca}' AND fecha = '{$postdata->fecha_inicial}' AND {$postdata->unidad} >= {$marca->minimo} AND {$postdata->unidad} <= {$marca->maximo}
                            UNION ALL
                            SELECT '> {$marca->maximo}' AS label, COUNT(1) AS value
                            FROM {$table}
                            WHERE marca = '{$marca->marca}' AND fecha = '{$postdata->fecha_inicial}' AND {$postdata->unidad} > {$marca->maximo}
                            UNION ALL
                            SELECT '< {$marca->minimo}' AS label, COUNT(1) AS value
                            FROM {$table}
                            WHERE marca = '{$marca->marca}' AND fecha = '{$postdata->fecha_inicial}' AND {$postdata->unidad} < {$marca->minimo}
                        ) AS tbl
                        WHERE value > 0";
                $response->pasteles[$marca->marca] = $this->db->queryAll($sql);
            }else{
                $peso_prom = $this->db->queryOne("SELECT ROUND(AVG(peso), 2) FROM (
                    SELECT AVG(caja) AS peso FROM produccion_cajas WHERE marca = '{$marca->marca}' AND fecha = '{$postdata->fecha_inicial}'
                    UNION ALL
                    SELECT AVG(peso) AS peso FROM produccion_gavetas WHERE marca = '{$marca->marca}' AND fecha = '{$postdata->fecha_inicial}'
                ) AS tbl");
                $sql = "SELECT value, label
                        FROM (
                            SELECT '{$peso_prom}' AS label, COUNT(1) AS value
                            FROM $table
                            WHERE marca = '{$marca->marca}' AND fecha = '{$postdata->fecha_inicial}' AND {$postdata->unidad} = {$peso_prom}
                            UNION ALL
                            SELECT '> {$peso_prom}' AS label, COUNT(1) AS value
                            FROM $table
                            WHERE marca = '{$marca->marca}' AND fecha = '{$postdata->fecha_inicial}' AND {$postdata->unidad} > {$peso_prom}
                            UNION ALL
                            SELECT '< {$peso_prom}' AS label, COUNT(1) AS value
                            FROM $table
                            WHERE marca = '{$marca->marca}' AND fecha = '{$postdata->fecha_inicial}' AND {$postdata->unidad} < {$peso_prom}
                        ) AS tbl";
                $response->pasteles[$marca->marca] = $this->db->queryAll($sql);
            }
        }
        return $response;
    }

    public function tablasDiferecias(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));

        $requerimiento = $postdata->requerimiento;
        $cajas1840 = $this->db->queryAllSpecial("SELECT nombre AS id, nombre AS label FROM produccion_tipo_caja WHERE requerimiento_{$postdata->unidad} IS NOT NULL");

        foreach($cajas1840 as $marca){
            $sql = "SELECT marca, ROUND(SUM(IF({$postdata->unidad} > {$requerimiento}, {$postdata->unidad} - {$requerimiento}, 0)), 2) AS kg_diff
                    FROM produccion_cajas
                    WHERE marca = '{$marca}' AND fecha = '{$postdata->fecha_inicial}'";
            $row = $this->db->queryRow($sql);
            $row->cajas = round($row->kg_diff / $requerimiento, 2);
            $row->dolares = round($row->cajas * 6.2, 2);
            if($row->kg_diff > 0) 
                $response->tablas[] = $row;
        }

        return $response;
    }

    public function historicoExcedente(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));

        $requerimiento = $postdata->requerimiento;
        $response->data = [];
        $response->umbrales = $this->db->queryRow("SELECT ROUND(AVG(kg_diff), 2) AS exce, ROUND(AVG(kg_diff) / 18.4, 2) AS cajas, ROUND(AVG(kg_diff) / 18.4 / 6.2, 2) AS dolares
            FROM (
                SELECT ROUND(SUM(IF({$postdata->unidad} > 18.4, {$postdata->unidad} - 18.4, 0)), 2) AS kg_diff FROM produccion_cajas WHERE YEAR = {$postdata->year} AND marca IN('COBANA 101', 'COBANA MADERA', 'COBANA REWE', 'COBANA VERDE', 'AGROBAN', 'PINALINDA') GROUP BY marca, semana
            ) AS tbl");
        $marcas = $this->db->queryAllSpecial("SELECT marca AS id, marca AS label FROM produccion_cajas WHERE YEAR = {$postdata->year} AND marca IN (SELECT nombre FROM produccion_tipo_caja WHERE requerimiento IS NOT NULL) AND marca != '' GROUP BY marca
                                                UNION ALL
                                                SELECT marca AS id, marca AS label FROM produccion_gavetas WHERE YEAR = {$postdata->year} AND marca IN (SELECT nombre FROM produccion_tipo_caja WHERE requerimiento IS NOT NULL) AND marca != '' GROUP BY marca");
        $semanas = $this->db->queryAllSpecial("SELECT semana AS id, semana AS label FROM(
                SELECT semana
                FROM produccion_cajas
                INNER JOIN produccion_tipo_caja ON nombre = marca
                WHERE YEAR = {$postdata->year} AND {$postdata->unidad} > $requerimiento AND requerimiento IS NOT NULL
                GROUP BY semana
                UNION ALL
                SELECT semana
                FROM produccion_gavetas
                INNER JOIN produccion_tipo_caja ON nombre = marca
                WHERE YEAR = {$postdata->year} AND {$postdata->unidad} > $requerimiento AND requerimiento IS NOT NULL
                GROUP BY semana
            ) AS tbl
            GROUP BY semana");
        $response->semanas = $semanas;

        $max_exce = []; $max_cajas = []; $max_dolares = [];
        $total = new stdClass;
        foreach($marcas as $marca){
            $row = new stdClass;
            $row->marca = $marca;

            /*if(!isset($postdata->requerimiento) && $postdata->requerimiento > 0){
                $requerimiento = $this->db->queryOne("SELECT IFNULL(requerimiento, 
                                                                (SELECT AVG(peso)
                                                                FROM (
                                                                    SELECT AVG(caja) as peso
                                                                    FROM produccion_cajas
                                                                    WHERE YEAR = {$postdata->year} AND marca = '{$marca}'
                                                                    GROUP BY semana
                                                                    UNION ALL
                                                                    SELECT AVG(peso) as peso
                                                                    FROM produccion_gavetas
                                                                    WHERE YEAR = {$postdata->year} AND marca = '{$marca}'
                                                                    GROUP BY semana
                                                                ) AS tbl))
                                                        FROM produccion_tipo_caja 
                                                        WHERE nombre = '{$marca}'");
                $row->requerimiento = $requerimiento;
            }else{
                $requerimiento = $postdata->requerimiento;*/
            //}
            $row->requerimietno = $requerimiento;
            $tipo = $this->db->queryOne("SELECT tipo FROM produccion_tipo_caja WHERE nombre = '{$marca}'");
            foreach($semanas as $semana){
                if($tipo == 'CAJA')
                    $val = $this->db->queryOne("SELECT ROUND(SUM(IF({$postdata->unidad} > {$requerimiento}, {$postdata->unidad} - {$requerimiento}, 0)), 2) FROM produccion_cajas WHERE YEAR = {$postdata->year} AND marca = '{$marca}' AND semana = {$semana}");
                else
                    $val = $this->db->queryOne("SELECT ROUND(SUM(IF({$postdata->unidad} > {$requerimiento}, {$postdata->unidad} - {$requerimiento}, 0)), 2) FROM produccion_gavetas WHERE YEAR = {$postdata->year} AND marca = '{$marca}' AND semana = {$semana}");
                $row->{"sem_exce_{$semana}"} = $val;
                $total->{"sem_exce_{$semana}"} += $val;

                $row->{"sem_cajas_{$semana}"} = round($val / $requerimiento, 2);
                if($val > 0 && !$row->{"sem_cajas_{$semana}"} > 0) $row->{"sem_cajas_{$semana}"} = 0.01;
                $total->{"sem_cajas_{$semana}"} += round($val / $requerimiento, 2);

                $row->{"sem_dolares_{$semana}"} = round($row->{"sem_cajas_{$semana}"} * 6.2, 2);
                $total->{"sem_dolares_{$semana}"} += round($row->{"sem_cajas_{$semana}"} * 6.2, 2);

                if($val > 0) if(!isset($row->{"min_exce"}) || ($row->{"min_exce"} > $val)) $row->{"min_exce"} = $val;
                if($val > 0) if(!isset($row->{"max_exce"}) || ($row->{"max_exce"} < $val)) $row->{"max_exce"} = $val;
                $row->{"sum_exce"} += $val;
                $total->{"sum_exce"} += $val;

                if($val > 0) if((!isset($row->{"min_cajas"}) && $val > 0) || ($row->{"min_cajas"} > $row->{"sem_cajas_{$semana}"})) $row->{"min_cajas"} = $row->{"sem_cajas_{$semana}"};
                if($val > 0) if((!isset($row->{"max_cajas"}) && $val > 0) || ($row->{"max_cajas"} < $row->{"sem_cajas_{$semana}"})) $row->{"max_cajas"} = $row->{"sem_cajas_{$semana}"};
                $row->{"sum_cajas"} += $row->{"sem_cajas_{$semana}"};
                $total->{"sum_cajas"} += $row->{"sem_cajas_{$semana}"};

                if($val > 0) if(!isset($row->{"min_dolares"}) || ($row->{"min_dolares"} > $row->{"sem_dolares_{$semana}"})) $row->{"min_dolares"} = $row->{"sem_dolares_{$semana}"};
                if($val > 0) if(!isset($row->{"max_dolares"}) || ($row->{"max_dolares"} < $row->{"sem_dolares_{$semana}"})) $row->{"max_dolares"} = $row->{"sem_dolares_{$semana}"};
                $row->{"sum_dolares"} += $row->{"sem_dolares_{$semana}"};
                $total->{"sum_dolares"} += $row->{"sem_dolares_{$semana}"};

                if($val > 0) $row->{"count"} += 1;
            }
            $row->{"avg_exce"} = $row->{"count"} > 0 ? round($row->{"sum_exce"} / $row->{"count"}, 2) : 0;
            $row->{"avg_cajas"} = $row->{"count"} > 0 ? round($row->{"sum_cajas"} / $row->{"count"}, 2) : 0;
            $row->{"avg_dolares"} = $row->{"count"} > 0 ? round($row->{"sum_dolares"} / $row->{"count"}, 2) : 0;

            $response->data[] = $row;
        }
        $total->marca = 'TOTAL';
        foreach($total as $key => $value){
            if(strpos($key, "sem_exce") !== false){
                if($value > 0) if(!isset($total->{"max_exce"}) || $total->{"max_exce"} < $value) $total->{"max_exce"} = $value;
                if($value > 0) if(!isset($total->{"min_exce"}) || $total->{"min_exce"} > $value) $total->{"min_exce"} = $value;
            }
            if(strpos($key, "sem_cajas") !== false){
                if($value > 0) if(!isset($total->{"max_cajas"}) || $total->{"max_cajas"} < $value) $total->{"max_cajas"} = $value;
                if($value > 0) if(!isset($total->{"min_cajas"}) || $total->{"min_cajas"} > $value) $total->{"min_cajas"} = $value;
            }
            if(strpos($key, "sem_dolares") !== false){
                if($value > 0) if(!isset($total->{"max_dolares"}) || $total->{"max_dolares"} < $value) $total->{"max_dolares"} = $value;
                if($value > 0) if(!isset($total->{"min_dolares"}) || $total->{"min_dolares"} > $value) $total->{"min_dolares"} = $value;
            }
        }
        $total->{"avg_exce"} = round($total->{"sum_exce"} / count($semanas), 2);
        $total->{"avg_cajas"} = round($total->{"sum_cajas"} / count($semanas), 2);
        $total->{"avg_dolares"} = round($total->{"sum_dolares"} / count($semanas), 2);
        $response->data[] = $total;
        return $response;
    }

    private function grafica_z($data = [], $group_y = []){
        $options = [];
        $options["tooltip"] = [
            "trigger" => 'axis',
            "axisPointer" => [
                "type" => 'cross',
                "crossStyle" => [
                    "color" => '#999'
                ]
            ]
        ];
        $options["toolbox"] = [
            "feature" => [
                "dataView" => [
                    "show" => true,
                    "readOnly" => false
                ],
                "magicType" => [
                    "show" => true,
                    "type" => ['line', 'bar']
                ],
                "restore" => [
                    "show" => true
                ],
                "saveAsImage" => [
                    "show" => true
                ]
            ]
        ];
        $options["legend"]["data"] = [];
        $options["legend"]["bottom"] = "0%";
        $options["legend"]["left"] = "center";
        $options["xAxis"] = [
            [
                "type" => 'category',
                "data" => [],
                "axisPointer" => [
                    "type" => 'shadow'
                ]
            ]
        ];
        /*
            [
                type => 'value',
                name => {String},
                min => 0,
                max => 200,
                interval => 5,
                axisLabel => [
                    formatter => {value} KG
                ]
            ]
        */
        $options["yAxis"] = [];
        /*
            [
                name => {String},
                type => 'line',
                data => [
                    {double}, {double}, {double}
                ]
            ]
        */
        $options["series"] = [];

        $maxs = [];
        $mins = [];
        $prepare_data = [];
        $_x = [];
        $_names = [];
        $_namess = [];
        foreach($data as $d){
            $d = (object) $d;
            if(!isset($maxs[$d->index_y])) if($d->value > 0)
                $maxs[$d->index_y] = $d->value;
            if($d->value > $maxs[$d->index_y]) if($d->value > 0)
                $maxs[$d->index_y] = $d->value;

            if(!isset($mins[$d->index_y])) if($d->value > 0)
                $mins[$d->index_y] = $d->value;
            if($d->value < $mins[$d->index_y]) if($d->value > 0)
                $mins[$d->index_y] = $d->value;

            if(!in_array($d->label_x, $_x)){
                $_x[] = $d->label_x;
            }
            if(!in_array($d->name, $_namess)){
                $_namess[] = $d->name;
                 
                $n = ["name" => $d->name, "group" => $d->index_y];
                if(isset($d->line)){
                    $n["line"] = $d->line;
                }
                $_names[] = $n;
            }
            $prepare_data[$d->label_x][$d->name] = $d->value;
        }

        foreach($group_y as $key => $col){
            $col = (object) $col;
            $options["yAxis"][] = [
                'type' => 'value',
                'name' => $col->name,
                'min' => 'dataMin',
                'axisLabel' => [
                    'formatter' => "{value} $col->format"
                ]
            ];
        }

        foreach($_x as $row){
            $options["xAxis"][0]["data"][] = $row;
        }

        foreach($_names as $name){
            $name = (object) $name;

            if(!in_array($name->name, $options["legend"]["data"]))
                $options["legend"]["data"][] = $name->name;

            $serie = [
                "name" => $name->name,
                "type" => 'line',
                "connectNulls" => true,
                "data" => []
            ];
            if($name->group > 0)
                $serie["yAxisIndex"] = $name->group;

            if(isset($name->line)){
                $serie["itemStyle"]["normal"]["lineStyle"]["width"] = 5;
            }

            foreach($_x as $row){
                $val = 0;
                if(isset($prepare_data[$row][$name->name]))
                    $val = $prepare_data[$row][$name->name];

                if($val > 0)
                    $serie["data"][] = $val;
                else
                    $serie["data"][] = null;
            }
            $options["series"][] = $serie;
        }

        return $options;
    }
}

