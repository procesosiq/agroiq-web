<?php defined('PHRAPI') or die("Direct access not allowed!");

class ProduccionDemo {
	public $name;
	private $db;
	private $config;

	public function __construct(){
        $this->config = $GLOBALS['config'];
		$this->session = Session::getInstance();
        $this->db = DB::getInstance($this->session->agent_user);
	}

	public function lastDay(){
        $response = new stdClass;
		$response->last = $this->db->queryRow("SELECT DATE(MAX(fecha)) AS fecha FROM produccion_historica");
		$response->fincas = $this->db->queryAllSpecial("SELECT id_finca AS id, finca AS label FROM produccion_historica GROUP BY id_finca");
		return $response;
	}

	public function historico(){
		$postdata = (object)json_decode(file_get_contents("php://input"));

		$filters = (object)[
			"finca" => getValueFrom($postdata , "finca" , "" , FILTER_SANITIZE_STRING),
			"fecha_inicial" => getValueFrom($postdata , "fecha_inicial" , "" , FILTER_SANITIZE_STRING),
            "fecha_final" => getValueFrom($postdata , "fecha_final" , "" , FILTER_SANITIZE_STRING),
		];

		$response = new stdClass;
		$response->data = $this->db->queryAll("SELECT historica.id, fecha, lote, causa, peso, manos, calibre, cinta, dedos, colores.class, IFNULL(historica.edad, 'N/A') AS edad, cuadrilla, hora, tipo
			FROM produccion_historica historica
			LEFT JOIN produccion_colores colores ON colores.color = cinta
			WHERE fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}'");
		return $response;
	}

	public function index(){
		$postdata = (object)json_decode(file_get_contents("php://input"));

		$filters = (object)[
			"finca" => getValueFrom($postdata , "finca" , "" , FILTER_SANITIZE_STRING),
			"fecha_inicial" => getValueFrom($postdata , "fecha_inicial" , "" , FILTER_SANITIZE_STRING),
			"fecha_final" => getValueFrom($postdata , "fecha_final" , "" , FILTER_SANITIZE_STRING),
		];

		$sWhere = "";

		if($filters->fecha_final != "" && $filters->fecha_inicial != ""){
			$sWhere .= " AND fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}'";
		}

		$response = new stdClass;
		$response->totales = (object)[
			"total_cosechados" => 0,
			"total_recusados" => 0,
			"total_procesada" => 0,
			"muestreados" => 0,
			"porcen" => 100,
			"peso" => 0,
			"manos" => 0,
			"calibracion" => 0,
			"dedos" => 0
		];

		$sql = "SELECT lote, 
                    IFNULL((SELECT SUM(IF(blz > form, blz, form)) FROM produccion_racimos_cuadrado WHERE fecha = '{$filters->fecha_inicial}' AND lote = produccion.lote AND status = 'RECU'), 
                        (SELECT COUNT(1) FROM produccion_historica WHERE tipo = 'RECUSADO' AND lote = produccion.lote  {$sWhere})) AS total_recusados,
                    IFNULL((SELECT SUM(IF(blz > form, blz, form)) FROM produccion_racimos_cuadrado WHERE fecha = '{$filters->fecha_inicial}' AND lote = produccion.lote AND status = 'PROC'), 
                        (SELECT COUNT(1) FROM produccion_historica WHERE tipo = 'PROC' AND lote = produccion.lote  {$sWhere})) AS total_procesados,
                    ROUND((SELECT AVG(edad) FROM produccion_historica WHERE lote = produccion.lote {$sWhere}), 2) AS edad,
                    (SELECT COUNT(1) FROM produccion_historica WHERE tipo = 'PROC' AND lote = produccion.lote AND calibre > 0 {$sWhere}) AS muestreados,
                    IFNULL((SELECT IF(SUM(blz) = 0, (SELECT AVG(peso) FROM produccion_historica WHERE tipo = 'PROC' {$sWhere}), NULL) FROM produccion_racimos_cuadrado WHERE fecha = '{$filters->fecha_inicial}' AND lote = produccion.lote),
					    (SELECT AVG(peso) FROM produccion_historica WHERE tipo = 'PROC' AND lote = produccion.lote  {$sWhere})) AS peso,
					(SELECT AVG(calibre) FROM produccion_historica WHERE tipo = 'PROC' AND lote = produccion.lote AND cinta != 'n/a' AND calibre IS NOT NULL AND calibre > 0 {$sWhere}) AS calibracion,
					(SELECT AVG(manos) FROM produccion_historica WHERE tipo = 'PROC' AND lote = produccion.lote AND cinta != 'n/a' AND manos IS NOT NULL AND manos > 0 {$sWhere}) AS manos,
					(SELECT AVG(dedos) FROM produccion_historica WHERE tipo = 'PROC' AND lote = produccion.lote AND cinta != 'n/a' AND dedos IS NOT NULL AND dedos > 0 {$sWhere}) AS dedos
				FROM(
                    SELECT *
                    FROM(
                        SELECT lote
                        FROM produccion_historica
                        WHERE 1=1 {$sWhere}
                        GROUP BY lote
                        UNION ALL
                        SELECT lote
                        FROM racimos_cosechados_by_color
                        WHERE 1=1 {$sWhere}
                        GROUP BY lote
                    ) AS tbl
                    GROUP BY lote
                ) AS produccion";

		$response->data = $this->db->queryAll($sql);

		foreach ($response->data as $key => $value) {
			$value->expanded = false;
			$value->total_cosechados = $value->total_procesados + $value->total_recusados;

			$value->cables = $this->db->queryAll("SELECT cinta AS cable, edad, class,
                                IFNULL((SELECT SUM(IF(blz > form, blz, form)) FROM produccion_racimos_cuadrado WHERE lote = '{$value->lote}' AND fecha = '{$filters->fecha_inicial}' AND cinta = produccion.cinta AND status = 'RECU'), 
                                    (SELECT COUNT(1) FROM produccion_historica WHERE tipo = 'RECUSADO' AND lote = '{$value->lote}' AND cinta = produccion.cinta {$sWhere})   ) AS total_recusados, 
                                IFNULL((SELECT SUM(IF(blz > form, blz, form)) FROM produccion_racimos_cuadrado WHERE lote = '{$value->lote}' AND fecha = '{$filters->fecha_inicial}' AND cinta = produccion.cinta AND status = 'PROC'), 
                                    (SELECT COUNT(1) FROM produccion_historica WHERE tipo = 'PROC' AND lote = '{$value->lote}' AND cinta = produccion.cinta {$sWhere})) AS total_procesados, 
                                IFNULL((SELECT IF(SUM(blz) = 0, (SELECT AVG(peso) FROM produccion_historica WHERE tipo = 'PROC' {$sWhere}), NULL) FROM produccion_racimos_cuadrado WHERE fecha = '{$filters->fecha_inicial}' AND lote = '{$value->lote}' AND cinta = produccion.cinta),
								    (SELECT AVG(peso) FROM produccion_historica WHERE tipo = 'PROC' AND lote = '{$value->lote}' AND cinta = produccion.cinta {$sWhere})) AS peso,
								(SELECT AVG(calibre) FROM produccion_historica WHERE tipo = 'PROC' AND lote = '{$value->lote}' AND cinta = produccion.cinta AND cinta != 'n/a' AND calibre IS NOT NULL AND calibre > 0 {$sWhere}) AS calibracion,
								(SELECT AVG(manos) FROM produccion_historica WHERE tipo = 'PROC' AND lote = '{$value->lote}' AND cinta = produccion.cinta AND cinta != 'n/a' AND manos IS NOT NULL AND manos > 0 {$sWhere}) AS manos,
								(SELECT AVG(dedos) FROM produccion_historica WHERE tipo = 'PROC' AND lote = '{$value->lote}' AND cinta = produccion.cinta AND cinta != 'n/a' AND dedos IS NOT NULL AND dedos > 0 {$sWhere}) AS dedos
							FROM(
                                SELECT *
                                FROM (
                                    SELECT cinta , produccion.edad, class
                                    FROM produccion_historica produccion
                                    LEFT JOIN produccion_colores colores ON cinta = color
                                    WHERE 1 = 1 AND lote = '{$value->lote}'  {$sWhere}
                                    GROUP BY cinta
                                    UNION ALL
                                    SELECT b.color AS cinta, b.edad, class
                                    FROM racimos_cosechados_by_color b
                                    LEFT JOIN produccion_colores c ON b.color = c.color
                                    WHERE 1 = 1 AND lote = '{$value->lote}' {$sWhere}
                                ) AS tbl
                                GROUP BY cinta
                            ) AS produccion");
			foreach ($value->cables as $llave => $valor) {
				$valor->expanded = false;
				$valor->total_cosechados = $valor->total_procesados + $valor->total_recusados;

				$valor->cuadrillas = $this->db->queryAll("SELECT cuadrilla,
                        IFNULL((SELECT SUM(IF(blz > form, blz, form))  FROM produccion_racimos_cuadrado WHERE lote = '{$value->lote}' AND fecha = '{$filters->fecha_inicial}' AND cinta = '{$value->cable}' AND palanca = produccion.cuadrilla AND status = 'RECU'), 
                            (SELECT COUNT(1) FROM produccion_historica WHERE tipo = 'RECUSADO' AND lote = '{$value->lote}' AND cinta = '{$valor->cable}' AND cuadrilla = produccion.cuadrilla {$sWhere})) AS total_recusados, 
                        IFNULL((SELECT SUM(IF(blz > form, blz, form))  FROM produccion_racimos_cuadrado WHERE lote = '{$value->lote}' AND fecha = '{$filters->fecha_inicial}' AND cinta = '{$value->cable}' AND palanca = produccion.cuadrilla AND status = 'PROC'), 
                            (SELECT COUNT(1) FROM produccion_historica WHERE tipo = 'PROC' AND lote = '{$value->lote}' AND cinta = '{$valor->cable}' AND cuadrilla = produccion.cuadrilla {$sWhere})) AS procesados, 
                        (SELECT COUNT(1) FROM produccion_historica WHERE tipo = 'PROC' AND lote = '{$value->lote}' AND cinta = '{$valor->cable}' AND calibre > 0 AND cuadrilla = produccion.cuadrilla {$sWhere}) AS muestreados, 
                        (SELECT AVG(peso) FROM produccion_historica WHERE tipo = 'PROC' AND lote = '{$value->lote}' AND cinta = '{$valor->cable}' AND cuadrilla = produccion.cuadrilla {$sWhere}) AS peso,
                        (SELECT AVG(calibre) FROM produccion_historica WHERE tipo = 'PROC' AND lote = '{$value->lote}' AND cinta = '{$valor->cable}' AND cuadrilla = produccion.cuadrilla AND cinta != 'n/a' AND calibre IS NOT NULL AND calibre > 0 {$sWhere}) AS calibracion,
                        (SELECT AVG(manos) FROM produccion_historica WHERE tipo = 'PROC' AND lote = '{$value->lote}' AND cinta = '{$valor->cable}' AND cuadrilla = produccion.cuadrilla AND cinta != 'n/a' AND manos IS NOT NULL AND manos > 0 {$sWhere}) AS manos,
                        (SELECT AVG(dedos) FROM produccion_historica WHERE tipo = 'PROC' AND lote = '{$value->lote}' AND cinta = '{$valor->cable}' AND cuadrilla = produccion.cuadrilla AND cinta != 'n/a' AND dedos IS NOT NULL AND dedos > 0 {$sWhere}) AS dedos
                    FROM(
                        SELECT cuadrilla
                        FROM produccion_historica historica
                        LEFT JOIN produccion_colores colores ON cinta = color
                        WHERE 1 = 1 {$sWhere} AND lote = '{$value->lote}' AND cinta = '{$valor->cable}'
                        GROUP BY cuadrilla
                    ) AS produccion");
				foreach($valor->cuadrillas as $row){
					$row->total_cosechados = $row->procesados + $row->total_recusados;
				}
			}
		}
        
        /* LOAD TAGS */
        /** JAVI*/
        /*$sql_javi = "SELECT SUM(cant) AS blz 
        FROM(
            SELECT lote, cinta, COUNT(1) AS cant 
            FROM produccion_historica
            WHERE fecha = '{$filters->fecha_inicial}' AND tipo = 'PROC'
            GROUP BY lote, cinta
            UNION ALL
            SELECT lote, h.color AS cinta, 0 AS cant
            FROM racimos_cosechados_by_color h
            INNER JOIN produccion_colores c ON c.color = h.color
            WHERE fecha = '{$filters->fecha_inicial}' AND (SELECT COUNT(1) FROM produccion_historica WHERE lote = h.lote AND cinta = h.color AND fecha = '{$filters->fecha_inicial}') = 0
            GROUP BY lote, h.edad
        ) AS tbl";*/
        // D($sql_javi);
        $sql_original = "SELECT SUM(IFNULL((SELECT SUM(IF(blz > form, blz, form)) FROM produccion_racimos_cuadrado WHERE fecha = '{$filters->fecha_inicial}' AND lote = tbl.lote AND cinta = tbl.cinta AND status = 'PROC'), cant)) AS cant
        FROM(
            SELECT lote, cinta, COUNT(1) AS cant 
            FROM produccion_historica
            WHERE fecha = '{$filters->fecha_inicial}' AND tipo = 'PROC'
            GROUP BY lote, cinta
            UNION ALL
            SELECT lote, h.color AS cinta, 0 AS cant
            FROM racimos_cosechados_by_color h
            INNER JOIN produccion_colores c ON c.color = h.color
            WHERE fecha = '{$filters->fecha_inicial}' AND (SELECT COUNT(1) FROM produccion_historica WHERE lote = h.lote AND cinta = h.color AND fecha = '{$filters->fecha_inicial}') = 0
            GROUP BY lote, h.edad
        ) AS tbl";

        $response->totales->total_procesada = $this->db->queryOne($sql_original);

        /*$sql_javi = "SELECT   SUM(cant) AS blz        
        FROM(
            SELECT lote, cinta, COUNT(1) AS cant 
            FROM produccion_historica 
            WHERE fecha = '{$filters->fecha_inicial}'
            GROUP BY lote, cinta
            UNION ALL
            SELECT lote, h.color AS cinta, 0 AS cant
            FROM racimos_cosechados_by_color h
            INNER JOIN produccion_colores c ON c.color = h.color
            WHERE fecha = '{$filters->fecha_inicial}' AND (SELECT COUNT(1) FROM produccion_historica WHERE lote = h.lote AND cinta = h.color AND fecha = '{$filters->fecha_inicial}') = 0
            GROUP BY lote, h.edad
        ) AS tbl";*/

        $sql_original = "SELECT SUM(IFNULL((SELECT SUM(IF(blz > form, blz, form)) FROM produccion_racimos_cuadrado WHERE fecha = '{$filters->fecha_inicial}' AND lote = tbl.lote AND cinta = tbl.cinta), cant)) AS cant
        FROM(
            SELECT lote, cinta, COUNT(1) AS cant 
            FROM produccion_historica 
            WHERE fecha = '{$filters->fecha_inicial}'
            GROUP BY lote, cinta
            UNION ALL
            SELECT lote, h.color AS cinta, 0 AS cant
            FROM racimos_cosechados_by_color h
            INNER JOIN produccion_colores c ON c.color = h.color
            WHERE fecha = '{$filters->fecha_inicial}' AND (SELECT COUNT(1) FROM produccion_historica WHERE lote = h.lote AND cinta = h.color AND fecha = '{$filters->fecha_inicial}') = 0
            GROUP BY lote, h.edad
        ) AS tbl";

        $response->totales->total_cosechados = $this->db->queryOne($sql_original);
        /** JAVI*/
        $response->totales->muestreados = $response->totales->total_procesada + $response->totales->total_cosechados;

        $response->id_company = $this->session->id_company;

        /** JAVI*/
        /*$sql_javi = "SELECT SUM(cant) AS blz   
        FROM(
            SELECT lote, cinta, COUNT(1) AS cant 
            FROM produccion_historica 
            WHERE fecha = '{$filters->fecha_inicial}' AND tipo = 'RECUSADO'
            GROUP BY lote, cinta
        ) AS tbl";*/

        $sql_original = "SELECT SUM(IFNULL((SELECT SUM(IF(blz > form, blz, form)) FROM produccion_racimos_cuadrado WHERE fecha = '{$filters->fecha_inicial}' AND lote = tbl.lote AND cinta = tbl.cinta AND status = 'RECU'), cant)) AS cant
        FROM (
            SELECT *
            FROM(
                SELECT lote, cinta, COUNT(1) AS cant 
                FROM produccion_historica 
                WHERE fecha = '{$filters->fecha_inicial}' AND tipo = 'RECUSADO'
                GROUP BY lote, cinta
                UNION ALL
                SELECT lote, color_cinta AS cinta, 0 AS cant
                FROM racimos_cosechados r
                INNER JOIN racimos_cosechados_detalle d ON id_racimos_cosechados = r.id
                WHERE DATE(fecha) = '{$filters->fecha_inicial}'
                GROUP BY lote, color_cinta
            ) AS tbl
            GROUP BY lote, cinta
        ) AS tbl";

        $response->totales->recusados = $this->db->queryOne($sql_original);
        /** JAVI*/
		$response->totales->peso = $this->db->queryRow("SELECT AVG(peso) as suma FROM produccion_historica WHERE tipo = 'PROC' {$sWhere}")->suma;
		$response->totales->manos = $this->db->queryRow("SELECT AVG(manos) as suma FROM produccion_historica WHERE tipo = 'PROC' AND manos IS NOT NULL AND manos > 0 AND cinta != 'n/a' {$sWhere}")->suma;
		$response->totales->calibracion = $this->db->queryRow("SELECT AVG(calibre) as suma FROM produccion_historica WHERE tipo = 'PROC' AND calibre IS NOT NULL AND calibre > 0 AND cinta != 'n/a' {$sWhere}")->suma;
		$response->totales->dedos = $this->db->queryRow("SELECT AVG(dedos) as suma FROM produccion_historica WHERE tipo = 'PROC' AND dedos IS NOT NULL AND dedos > 0 AND cinta != 'n/a' {$sWhere}")->suma;
        $response->totales->porcen = ($response->totales->muestreados / $response->totales->total_cosechados) * 100;
        $response->totales->edad = $this->db->queryRow("SELECT ROUND(AVG(edad), 2) AS edad FROM produccion_historica WHERE tipo = 'PROC' {$sWhere}")->edad;

        $suma_peso_proc = $this->db->queryOne("SELECT SUM(IF(blz > form, blz, form))-SUM(blz) FROM produccion_racimos_cuadrado WHERE fecha = '{$filters->fecha_inicial}' AND status = 'PROC'");
        $suma_peso_proc *= $response->totales->peso;
        $response->tags["kg_proc"] = $this->db->queryOne("SELECT SUM(peso)+($suma_peso_proc) FROM produccion_historica WHERE fecha = '{$filters->fecha_inicial}' AND tipo = 'PROC'");

        $suma_peso_proc = $this->db->queryOne("SELECT SUM(IF(blz > form, blz, form))-SUM(blz) FROM produccion_racimos_cuadrado WHERE fecha = '{$filters->fecha_inicial}' AND status = 'RECU'");
        $suma_peso_proc *= $response->totales->peso;
        $response->tags["kg_recu"] = $this->db->queryOne("SELECT SUM(peso)+($suma_peso_proc) FROM produccion_historica WHERE fecha = '{$filters->fecha_inicial}' AND tipo = 'RECUSADO'");

        $times = $this->db->queryAll("SELECT fecha, hora FROM produccion_historica WHERE 1=1 $sWhere ORDER BY hora");
        $p = $times[0];
        $u = $times[count($times)-1];
        $response->tags["ultima_fecha"] = $u->fecha;
        $response->tags["ultima_hora"] = $u->hora;
        $response->tags["primera_fecha"] = $p->fecha;
        $response->tags["primera_hora"] = $p->hora;
        $response->tags["diferencia"] = $this->db->queryRow("SELECT TIMEDIFF('{$u->hora}', '{$p->hora}') AS dif")->dif;
		return $response;
	}

	public function racimosPalanca(){
		$postdata = (object)json_decode(file_get_contents("php://input"));

		$filters = (object)[
			"finca" => getValueFrom($postdata , "finca" , "" , FILTER_SANITIZE_STRING),
			"fecha_inicial" => getValueFrom($postdata , "fecha_inicial" , "" , FILTER_SANITIZE_STRING),
			"fecha_final" => getValueFrom($postdata , "fecha_final" , "" , FILTER_SANITIZE_STRING),
		];
		if($filters->fecha_final != "" && $filters->fecha_inicial != ""){
			$sWhere .= " AND fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}'";
		}

		$sql = "SELECT cuadrilla,
					(SELECT COUNT(1) FROM produccion_historica WHERE tipo = 'RECUSADO' AND cuadrilla = produccion.cuadrilla  {$sWhere}) AS recusados , 
					(SELECT COUNT(1) FROM produccion_historica WHERE tipo = 'PROC' AND cuadrilla = produccion.cuadrilla  {$sWhere}) AS procesados, 
                    (SELECT COUNT(1) FROM produccion_historica WHERE tipo = 'PROC' AND cuadrilla = produccion.cuadrilla AND calibre > 0 {$sWhere}) AS muestreados, 
                    ROUND((SELECT AVG(edad) FROM produccion_historica WHERE tipo = 'PROC' AND cuadrilla = produccion.cuadrilla AND edad > 0 {$sWhere}), 2) AS edad,
					(SELECT AVG(peso) FROM produccion_historica WHERE tipo = 'PROC' AND cuadrilla = produccion.cuadrilla  {$sWhere}) AS peso,
					(SELECT AVG(calibre) FROM produccion_historica WHERE tipo = 'PROC' AND cuadrilla = produccion.cuadrilla AND cinta != 'n/a' AND calibre IS NOT NULL AND calibre > 0 {$sWhere}) AS calibracion,
					(SELECT AVG(manos) FROM produccion_historica WHERE tipo = 'PROC' AND cuadrilla = produccion.cuadrilla AND cinta != 'n/a' AND manos IS NOT NULL AND manos > 0 {$sWhere}) AS manos,
					(SELECT AVG(dedos) FROM produccion_historica WHERE tipo = 'PROC' AND cuadrilla = produccion.cuadrilla AND cinta != 'n/a' AND dedos IS NOT NULL AND dedos > 0 {$sWhere}) AS dedos
				FROM(
				SELECT cuadrilla
				FROM produccion_historica produccion
				WHERE 1 = 1  {$sWhere}
				GROUP BY cuadrilla) AS produccion";
		$response->palancas = $this->db->queryAll($sql);
		foreach($response->palancas as $val){
			$val->cosechados = $val->procesados + $val->recusados;

			$val->lotes = $this->db->queryAll("SELECT lote,
					(SELECT COUNT(1) FROM produccion_historica WHERE tipo = 'RECUSADO' AND cuadrilla = '{$val->cuadrilla}' AND lote = produccion.lote {$sWhere}) AS recusados , 
					(SELECT COUNT(1) FROM produccion_historica WHERE tipo = 'PROC' AND cuadrilla = '{$val->cuadrilla}' AND lote = produccion.lote {$sWhere}) AS procesados, 
                    (SELECT COUNT(1) FROM produccion_historica WHERE tipo = 'PROC' AND cuadrilla = '{$val->cuadrilla}' AND lote = produccion.lote AND calibre > 0 {$sWhere}) AS muestreados, 
                    ROUND((SELECT AVG(edad) FROM produccion_historica WHERE tipo = 'PROC' AND cuadrilla = '{$val->cuadrilla}' AND lote = produccion.lote AND edad > 0 {$sWhere}), 2) AS edad, 
					(SELECT AVG(peso) FROM produccion_historica WHERE tipo = 'PROC' AND cuadrilla = '{$val->cuadrilla}'AND lote = produccion.lote  {$sWhere}) AS peso,
					(SELECT AVG(calibre) FROM produccion_historica WHERE tipo = 'PROC' AND cuadrilla = '{$val->cuadrilla}' AND cinta != 'n/a' AND lote = produccion.lote AND calibre IS NOT NULL AND calibre > 0 {$sWhere}) AS calibracion,
					(SELECT AVG(manos) FROM produccion_historica WHERE tipo = 'PROC' AND cuadrilla = '{$val->cuadrilla}' AND cinta != 'n/a' AND lote = produccion.lote AND manos IS NOT NULL AND manos > 0 {$sWhere}) AS manos,
					(SELECT AVG(dedos) FROM produccion_historica WHERE tipo = 'PROC' AND cuadrilla = '{$val->cuadrilla}' AND cinta != 'n/a' AND lote = produccion.lote AND dedos IS NOT NULL AND dedos > 0 {$sWhere}) AS dedos
				FROM(
				SELECT lote
				FROM produccion_historica produccion
				WHERE cuadrilla = '{$val->cuadrilla}' {$sWhere}
				GROUP BY lote) AS produccion");
			foreach($val->lotes as $value){
				$value->cosechados = $value->procesados + $value->recusados;
				
				$value->cables = $this->db->queryAll("SELECT cinta AS cable, produccion.edad, class,
						(SELECT COUNT(1) FROM produccion_historica WHERE tipo = 'RECUSADO' AND cuadrilla = '{$val->cuadrilla}' AND lote = '{$value->lote}' AND cinta = produccion.cinta {$sWhere}) AS recusados , 
						(SELECT COUNT(1) FROM produccion_historica WHERE tipo = 'PROC' AND cuadrilla = '{$val->cuadrilla}' AND lote = '{$value->lote}' AND cinta = produccion.cinta {$sWhere}) AS procesados, 
                        (SELECT COUNT(1) FROM produccion_historica WHERE tipo = 'PROC' AND cuadrilla = '{$val->cuadrilla}' AND lote = '{$value->lote}' AND cinta = produccion.cinta AND calibre > 0 {$sWhere}) AS muestreados, 
                        (SELECT AVG(edad) FROM produccion_historica WHERE tipo = 'PROC' AND cuadrilla = '{$val->cuadrilla}' AND lote = '{$value->lote}' AND cinta = produccion.cinta AND edad > 0 {$sWhere}) AS edad,
						(SELECT AVG(peso) FROM produccion_historica WHERE tipo = 'PROC' AND cuadrilla = '{$val->cuadrilla}' AND lote = '{$value->lote}' AND cinta = produccion.cinta  {$sWhere}) AS peso,
						(SELECT AVG(calibre) FROM produccion_historica WHERE tipo = 'PROC' AND cuadrilla = '{$val->cuadrilla}' AND cinta != 'n/a' AND lote = '{$value->lote}' AND cinta = produccion.cinta AND calibre IS NOT NULL AND calibre > 0 {$sWhere}) AS calibracion,
						(SELECT AVG(manos) FROM produccion_historica WHERE tipo = 'PROC' AND cuadrilla = '{$val->cuadrilla}' AND cinta != 'n/a' AND lote = '{$value->lote}' AND cinta = produccion.cinta AND manos IS NOT NULL AND manos > 0 {$sWhere}) AS manos,
						(SELECT AVG(dedos) FROM produccion_historica WHERE tipo = 'PROC' AND cuadrilla = '{$val->cuadrilla}' AND cinta != 'n/a' AND lote = '{$value->lote}' AND cinta = produccion.cinta AND dedos IS NOT NULL AND dedos > 0 {$sWhere}) AS dedos
					FROM(
					SELECT cinta , produccion.edad, class
					FROM produccion_historica produccion
					LEFT JOIN produccion_colores colores ON cinta = color
					WHERE cuadrilla = '{$val->cuadrilla}' AND lote = '{$value->lote}' {$sWhere}
					GROUP BY cinta) AS produccion");
				foreach($value->cables as $row){
					$row->cosechados = $row->procesados + $row->recusados;
				}
			}
		}
		return $response;
	}

	public function racimosEdad(){
        $postdata = (object)json_decode(file_get_contents("php://input"));

		$filters = (object)[
			"finca" => getValueFrom($postdata , "finca" , "" , FILTER_SANITIZE_STRING),
			"fecha_inicial" => getValueFrom($postdata , "fecha_inicial" , "" , FILTER_SANITIZE_STRING),
			"fecha_final" => getValueFrom($postdata , "fecha_final" , "" , FILTER_SANITIZE_STRING),
		];

		if($filters->fecha_final != "" && $filters->fecha_inicial != ""){
			$sWhere .= " AND fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}'";
		}

		$sql = "SELECT cinta AS cable, edad AS edad, class,
                    IFNULL((SELECT SUM(IF(blz > form, blz, form)) FROM produccion_racimos_cuadrado WHERE fecha = '{$filters->fecha_inicial}' AND cinta = produccion.cinta AND status = 'RECU'),
					    (SELECT COUNT(1) FROM produccion_historica WHERE tipo = 'RECUSADO' AND cinta = produccion.cinta  {$sWhere})) AS recusados, 
                    IFNULL((SELECT SUM(IF(blz > form, blz, form)) FROM produccion_racimos_cuadrado WHERE fecha = '{$filters->fecha_inicial}' AND cinta = produccion.cinta AND status = 'PROC'),
    					(SELECT COUNT(1) FROM produccion_historica WHERE tipo = 'PROC' AND cinta = produccion.cinta  {$sWhere})) AS procesados, 
                    (SELECT COUNT(1) FROM produccion_historica WHERE tipo = 'PROC' AND cinta = produccion.cinta AND calibre > 0 {$sWhere}) AS muestreados, 
					IFNULL((SELECT IF(SUM(blz) = 0, (SELECT AVG(peso) FROM produccion_historica WHERE tipo = 'PROC' {$sWhere}), NULL) FROM produccion_racimos_cuadrado WHERE fecha = '{$filters->fecha_inicial}' AND cinta = produccion.cinta),
                        (SELECT AVG(peso) FROM produccion_historica WHERE tipo = 'PROC' AND cinta = produccion.cinta  {$sWhere})) AS peso,
					(SELECT AVG(calibre) FROM produccion_historica WHERE tipo = 'PROC' AND cinta = produccion.cinta AND cinta != 'n/a' AND calibre IS NOT NULL AND calibre > 0 {$sWhere}) AS calibracion,
					(SELECT AVG(manos) FROM produccion_historica WHERE tipo = 'PROC' AND cinta = produccion.cinta AND cinta != 'n/a' AND manos IS NOT NULL AND manos > 0 {$sWhere}) AS manos,
					(SELECT AVG(dedos) FROM produccion_historica WHERE tipo = 'PROC' AND cinta = produccion.cinta AND cinta != 'n/a' AND dedos IS NOT NULL AND dedos > 0 {$sWhere}) AS dedos
				FROM(
                    SELECT *
                    FROM (
                        SELECT cinta , produccion.edad, class
                        FROM produccion_historica produccion
                        LEFT JOIN produccion_colores colores ON cinta = color
                        WHERE 1 = 1  {$sWhere}
                        GROUP BY cinta
                        UNION ALL
                        SELECT b.color AS cinta, b.edad, class
                        FROM racimos_cosechados_by_color b
                        LEFT JOIN produccion_colores c ON b.color = c.color
                        WHERE 1 = 1 {$sWhere}
                    ) AS tbl
                    GROUP BY cinta
                ) AS produccion";
		$response->data = $this->db->queryAll($sql);
		foreach($response->data as $i => $val){
			$val->expanded = false;
            $val->cosechados = $val->procesados + $val->recusados;
			$val->lotes = $this->db->queryAll("SELECT lote, 
                    IFNULL((SELECT SUM(IF(blz > form, blz, form)) FROM produccion_racimos_cuadrado WHERE fecha = '{$filters->fecha_inicial}' AND cinta = '{$val->cable}' AND lote = produccion.lote AND status = 'RECU'),
					    (SELECT COUNT(1) FROM produccion_historica WHERE tipo = 'RECUSADO' AND cinta = '{$val->cable}' AND lote = produccion.lote {$sWhere})) AS recusados,
                    IFNULL((SELECT SUM(IF(blz > form, blz, form)) FROM produccion_racimos_cuadrado WHERE fecha = '{$filters->fecha_inicial}' AND cinta = '{$val->cable}' AND lote = produccion.lote AND status = 'PROC'),
					    (SELECT COUNT(1) FROM produccion_historica WHERE tipo = 'PROC' AND cinta = '{$val->cable}' AND lote = produccion.lote {$sWhere})) AS procesados, 
                    (SELECT COUNT(1) FROM produccion_historica WHERE tipo = 'PROC' AND cinta = '{$val->cable}' AND lote = produccion.lote AND calibre > 0 {$sWhere}) AS muestreados, 
                    IFNULL((SELECT IF(SUM(blz) = 0, (SELECT AVG(peso) FROM produccion_historica WHERE tipo = 'PROC' {$sWhere}), NULL) FROM produccion_racimos_cuadrado WHERE fecha = '{$filters->fecha_inicial}' AND cinta = '{$val->cable}' AND lote = produccion.lote),
					    (SELECT AVG(peso) FROM produccion_historica WHERE tipo = 'PROC' AND cinta = '{$val->cable}' AND lote = produccion.lote {$sWhere})) AS peso,
					(SELECT AVG(calibre) FROM produccion_historica WHERE tipo = 'PROC' AND cinta = '{$val->cable}' AND cinta != 'n/a' AND lote = produccion.lote AND calibre IS NOT NULL AND calibre > 0 {$sWhere}) AS calibracion,
					(SELECT AVG(manos) FROM produccion_historica WHERE tipo = 'PROC' AND cinta = '{$val->cable}' AND cinta != 'n/a' AND lote = produccion.lote AND manos IS NOT NULL AND manos > 0 {$sWhere}) AS manos,
					(SELECT AVG(dedos) FROM produccion_historica WHERE tipo = 'PROC' AND cinta = '{$val->cable}' AND cinta != 'n/a' AND lote = produccion.lote AND dedos IS NOT NULL AND dedos > 0 {$sWhere}) AS dedos
				FROM(
                    SELECT *
                    FROM(
                        SELECT lote
                        FROM produccion_historica
                        WHERE 1=1 AND cinta = '{$val->cable}' {$sWhere}
                        GROUP BY lote
                        UNION ALL
                        SELECT lote
                        FROM racimos_cosechados_by_color
                        WHERE 1=1 AND color = '{$val->cable}' {$sWhere}
                        GROUP BY lote
                    ) AS tbl
                    GROUP BY lote
                ) AS produccion");
			foreach($val->lotes as $key => $value){
				$value->expanded = false;
				$value->cosechados = $value->procesados + $value->recusados;

				$value->cuadrillas = $this->db->queryAll("SELECT cuadrilla, 
						(SELECT COUNT(1) FROM produccion_historica WHERE tipo = 'RECUSADO' AND cinta = '{$val->cable}' AND lote = '{$value->lote}' AND cuadrilla = produccion.cuadrilla {$sWhere}) AS recusados,
						(SELECT COUNT(1) FROM produccion_historica WHERE tipo = 'PROC' AND cinta = '{$val->cable}' AND lote = '{$value->lote}' AND cuadrilla = produccion.cuadrilla {$sWhere}) AS procesados, 
                        (SELECT COUNT(1) FROM produccion_historica WHERE tipo = 'PROC' AND cinta = '{$val->cable}' AND lote = '{$value->lote}' AND cuadrilla = produccion.cuadrilla AND calibre > 0 {$sWhere}) AS muestreados, 
						(SELECT AVG(peso) FROM produccion_historica WHERE tipo = 'PROC' AND cinta = '{$val->cable}' AND lote = '{$value->lote}' AND cuadrilla = produccion.cuadrilla {$sWhere}) AS peso,
						(SELECT AVG(calibre) FROM produccion_historica WHERE tipo = 'PROC' AND cinta = '{$val->cable}' AND cinta != 'n/a' AND lote = '{$value->lote}' AND cuadrilla = produccion.cuadrilla AND calibre IS NOT NULL AND calibre > 0 {$sWhere}) AS calibracion,
						(SELECT AVG(manos) FROM produccion_historica WHERE tipo = 'PROC' AND cinta = '{$val->cable}' AND cinta != 'n/a' AND lote = '{$value->lote}' AND cuadrilla = produccion.cuadrilla AND manos IS NOT NULL AND manos > 0 {$sWhere}) AS manos,
						(SELECT AVG(dedos) FROM produccion_historica WHERE tipo = 'PROC' AND cinta = '{$val->cable}' AND cinta != 'n/a' AND lote = '{$value->lote}' AND cuadrilla = produccion.cuadrilla AND dedos IS NOT NULL AND dedos > 0 {$sWhere}) AS dedos
					FROM(
					SELECT cuadrilla
					FROM produccion_historica
					WHERE 1=1 AND cinta = '{$val->cable}' AND lote = '{$value->lote}' {$sWhere}
					GROUP BY cuadrilla) AS produccion");
				foreach($value->cuadrillas as $row){
					$row->cosechados = $row->procesados + $row->recusados;
				}
			}
		}
		return $response;
	}

	public function promediosLotes(){
		$postdata = (object)json_decode(file_get_contents("php://input"));

		$filters = (object)[
			"finca" => getValueFrom($postdata , "finca" , "" , FILTER_SANITIZE_STRING),
			"fecha_inicial" => getValueFrom($postdata , "fecha_inicial" , "" , FILTER_SANITIZE_STRING),
			"fecha_final" => getValueFrom($postdata , "fecha_final" , "" , FILTER_SANITIZE_STRING),
		];

		if($filters->fecha_final != "" && $filters->fecha_inicial != ""){
			$sWhere .= " AND fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}'";
		}

		$response = new stdClass;
		$response->data = $this->db->queryAll("SELECT lote, 
				ROUND((SELECT AVG(edad) FROM produccion_historica WHERE tipo = 'PROC' AND lote = produccion.lote {$sWhere} AND edad > 0), 2) AS edad, 
                #TAG - cuadrar
                IFNULL((SELECT SUM(IF(blz > form, blz, form))
                        FROM produccion_racimos_cuadrado
                        WHERE fecha = '{$filters->fecha_inicial}' AND lote = produccion.lote), (SELECT COUNT(1) FROM produccion_historica WHERE tipo = 'PROC' AND lote = produccion.lote {$sWhere})) AS racimos, 
				ROUND((SELECT AVG(peso) FROM produccion_historica WHERE tipo = 'PROC' AND lote = produccion.lote {$sWhere} AND peso > 0), 2) AS peso, 
				ROUND((SELECT AVG(calibre) FROM produccion_historica WHERE tipo = 'PROC' AND lote = produccion.lote {$sWhere} AND calibre > 0 AND cinta != 'n/a'), 2) AS calibre, 
				ROUND((SELECT AVG(manos) FROM produccion_historica WHERE tipo = 'PROC' AND lote = produccion.lote {$sWhere} AND manos > 0 AND cinta != 'n/a'), 2) AS manos, 
				ROUND((SELECT AVG(dedos) FROM produccion_historica WHERE tipo = 'PROC' AND lote = produccion.lote {$sWhere} AND dedos > 0 AND cinta != 'n/a'), 2) AS dedos
			FROM(
				SELECT lote
				FROM produccion_historica h
				WHERE tipo = 'PROC' {$sWhere}
				GROUP BY lote) AS produccion");
		return $response;
    }
    
    public function racimosFolmularios(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $filters = (object)[
            "finca" => getValueFrom($postdata , "finca" , "" , FILTER_SANITIZE_STRING),
            "fecha_inicial" => getValueFrom($postdata , "fecha_inicial" , "" , FILTER_SANITIZE_STRING),
            "fecha_final" => getValueFrom($postdata , "fecha_final" , "" , FILTER_SANITIZE_STRING),
        ];

        if($filters->fecha_final != "" && $filters->fecha_inicial != ""){
            $sWhere .= " AND DATE(fecha) BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}'";
        }

        $response = new stdClass;
        
        /* BEGIN TABLA POR LOTE */
        $response->racimos_formularios = $this->db->queryAll("SELECT lote, 
                                                    SUM(racimos_cosechados) AS cosechados, 
                                                    (SELECT COUNT(1) 
                                                        FROM racimos_cosechados
                                                        INNER JOIN racimos_cosechados_detalle ON racimos_cosechados.id = racimos_cosechados_detalle.`id_racimos_cosechados`
                                                        WHERE lote = tbl.lote {$sWhere}) AS recusados
                                        FROM racimos_cosechados tbl
                                        WHERE 1=1 $sWhere AND lote != ''
                                        GROUP BY lote");
        
        $suma_edades = 0;
        $cantidad = 0;
        $nombres_cintas = ["roja" => "ROJA", "lila" => "LILA", "negra" => "NEGRO", "verde" => "VERDE", "azul" => "AZUL", "amarilla" => "AMARILLO", "blanca" => "BLANCO", "cafe" => "CAFE"];
        foreach($response->racimos_formularios as $row){
            $colores = $this->db->queryRow("SELECT SUM(roja) AS roja, SUM(lila) AS lila, SUM(negra) AS negra, SUM(verde) AS verde, SUM(azul) AS azul, SUM(amarilla) AS amarilla, SUM(blanca) AS blanca, SUM(cafe) AS cafe
                                            FROM racimos_cosechados 
                                            WHERE lote = '{$row->lote}' {$sWhere}");
            $colores = (array) $colores;
            $in = [];
            $d_suma_edades = 0;
            $d_cantidad = 0;
            foreach($colores as $col => $val){
                if($val > 0){
                    $cinta = $nombres_cintas[$col];
                    $in[] = $cinta;
                    $detalle = $this->db->queryRow("SELECT '{$cinta}' AS cinta, SUM($col) AS cosechados,
                                                                (SELECT COUNT(1) 
                                                                        FROM racimos_cosechados
                                                                        INNER JOIN racimos_cosechados_detalle ON racimos_cosechados.id = racimos_cosechados_detalle.`id_racimos_cosechados`
                                                                        WHERE lote = tbl.lote AND UPPER(TRIM(color_cinta)) = '{$col}' {$sWhere}) AS recusados,
                                                                IFNULL((SELECT edad FROM produccion_historica WHERE cinta = '{$cinta}' AND fecha = tbl.fecha LIMIT 1), getEdadCinta(getWeek(fecha), '{$cinta}', getYear(fecha))) AS edad
                                                    FROM racimos_cosechados tbl
                                                    WHERE lote = '{$row->lote}' {$sWhere}");
                    $class = $this->db->queryOne("SELECT class FROM produccion_colores WHERE color = '{$cinta}'");
                    $detalle->class = $class;
                    $detalle->procesados = $detalle->cosechados - $detalle->recusados;

                    $suma_edades += ($detalle->edad * $detalle->procesados);
                    $cantidad += $detalle->procesados;

                    $d_suma_edades += ($detalle->edad * $detalle->procesados);
                    $d_cantidad += $detalle->procesados;

                    $detalle->detalle = $this->db->queryAll("SELECT palanca, SUM($col) AS cosechados,
                                                                            (SELECT COUNT(1) 
                                                                                FROM racimos_cosechados
                                                                                INNER JOIN racimos_cosechados_detalle ON racimos_cosechados.id = racimos_cosechados_detalle.`id_racimos_cosechados`
                                                                                WHERE lote = tbl.lote AND color_cinta = '{$col}' AND palanca = tbl.palanca {$sWhere}) AS recusados
                                                                FROM racimos_cosechados tbl
                                                                WHERE lote = '{$row->lote}' {$sWhere}
                                                                GROUP BY palanca
                                                                HAVING cosechados > 0");
                    foreach($detalle->detalle as $p){
                        $p->procesados = $p->cosechados - $p->recusados;
                    }

                    $row->detalle[] = $detalle;
                }
            }
            $in = "'".implode("','", $in)."'";

            $row->edad = $d_suma_edades / $d_cantidad;
            $row->procesados = round($row->cosechados - $row->recusados, 2);
        }
        $response->prom_edad = $suma_edades / $cantidad;
        /* END TABLE POR LOTE */

        /* BEGIN TABLA POR PALANCA */
        $response->table_palanca = $this->db->queryAll("SELECT palanca as cuadrilla, 
                    SUM(roja + lila + negra + verde + azul + blanca + amarilla + cafe) AS proc,
                    SUM((SELECT COUNT(1) FROM racimos_cosechados_detalle WHERE id_racimos_cosechados = racimos_cosechados.id)) AS recu
            FROM racimos_cosechados
            WHERE 1=1 {$sWhere}
            GROUP BY palanca");
        foreach($response->table_palanca as $row){
            $row->proc = $row->proc - $row->recu;
            $row->cose = $row->proc + $row->recu;
        }
        /* END TABLA POR PALANCA */

        /* BEGIN TABLA POR EDAD */
        $edades = $this->db->queryAll("SELECT getYear(fecha) AS year, getWeek(fecha) AS sem, SUM(roja) AS roja, SUM(lila) AS lila, SUM(negra) AS negra, SUM(verde) AS verde, SUM(azul) AS azul, SUM(amarilla) AS amarilla, SUM(blanca) AS blanca, SUM(cafe) AS cafe
            FROM racimos_cosechados his
            WHERE 1=1 {$sWhere}
            GROUP BY getWeek(fecha)");
        foreach($edades as $row){
            foreach($row as $col => $val){
                if($col != 'sem' && $col != 'year')
                if($val > 0){
                    $edad = $this->db->queryRow("SELECT getEdadCinta($row->sem, '{$nombres_cintas[$col]}', $row->year) AS edad,
                                                        (SELECT class FROM produccion_colores WHERE '$nombres_cintas[$col]' = color) AS class");
                    $item = [
                        "col" => $col,
                        "cinta" => $nombres_cintas[$col],
                        "edad" => $edad->edad,
                        "class" => $edad->class,
                        "proc" => $val,
                        "recu" => $this->db->queryOne("SELECT COUNT(1) FROM racimos_cosechados INNER JOIN racimos_cosechados_detalle ON racimos_cosechados.id = id_racimos_cosechados WHERE color_cinta = '{$nombres_cintas[$col]}' {$sWhere}")
                    ];
                    $item["proc"] -= $item["recu"];
                    $item["cose"] = $item["proc"] + $item["recu"];
                    $response->table_edades[] = $item;
                }
            }
        }
        /* END TABLA POR EDAD */
        return $response;
    }

    public function eliminar(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        if(count($postdata->ids) > 0){
            foreach($postdata->ids as $reg){
                $this->db->query("DELETE FROM produccion_historica WHERE id = $reg->id");
            }
            return true;
        }
        return false;
    }
    
    public function editar(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        if(isset($postdata->id)){
            if($postdata->peso > 0 && $postdata->lote != "" && $postdata->cuadrilla != "" && $postdata->edad > 0 && $postdata->tipo != ""){
                $this->db->query("INSERT INTO update_produccion_historica(id, fecha, hora, id_finca, finca, idv, racimo, num_racimo, grupo_racimo, peso, lote, cinta, edad, cuadrilla, manos, calibre, dedos, tipo, nivel, causa, timestamp, semana, id_balanza)
                                    SELECT id, fecha, hora, id_finca, finca, idv, racimo, num_racimo, grupo_racimo, peso, lote, cinta, edad, cuadrilla, manos, calibre, dedos, tipo, nivel, causa, timestamp, semana, id_balanza
                                    FROM produccion_historica
                                    WHERE id = {$postdata->id}");
                $this->db->query("UPDATE produccion_historica SET
                                        peso = $postdata->peso,
                                        edad = $postdata->edad,
                                        cinta = getCintaFromEdad($postdata->edad, getWeek(fecha), getYear(fecha)),
                                        cuadrilla = '{$postdata->cuadrilla}',
                                        lote = '{$postdata->lote}',
                                        tipo = '{$postdata->tipo}',
                                        causa = '{$postdata->causa}',
                                        manos = '{$postdata->manos}',
                                        calibre = '{$postdata->calibre}',
                                        dedos = '{$postdata->dedos}'
                                    WHERE id = {$postdata->id}");
            }
            return $this->db->queryRow("SELECT h.id, fecha, lote, causa, peso, manos, calibre, cinta, dedos, h.edad, cuadrilla, hora, tipo, class
                                        FROM produccion_historica h
                                        INNER JOIN produccion_colores c ON h.cinta = c.color
                                        WHERE h.id = {$postdata->id}");
        }
        return false;
    }

    public function cuadreRacimos(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $filters = (object)[
            "finca" => getValueFrom($postdata , "finca" , "" , FILTER_SANITIZE_STRING),
            "fecha_inicial" => getValueFrom($postdata , "fecha_inicial" , "" , FILTER_SANITIZE_STRING),
            "fecha_final" => getValueFrom($postdata , "fecha_final" , "" , FILTER_SANITIZE_STRING),
        ];
        $filters->semana = $this->db->queryOne("SELECT getWeek('{$filters->fecha_inicial}')");
        #$peso_prom = $this->db->queryOne("SELECT ROUND(AVG(peso), 2) FROM produccion_historica WHERE semana = WEEK('{$filters->fecha_inicial}') AND tipo = 'PROC'");

        $response->cuadrado = $this->db->queryOne("SELECT COUNT(1) FROM produccion_racimos_cuadrado WHERE fecha = '{$filters->fecha_inicial}'");
        $sql = "SELECT lote, edad, cinta, class, blz,
                    (SELECT SUM(cantidad) FROM racimos_cosechados_by_color WHERE lote = tbl.lote AND color = cinta AND fecha = '{$filters->fecha_inicial}') -
                        (SELECT COUNT(1) FROM racimos_cosechados r INNER JOIN racimos_cosechados_detalle ON r.id = id_racimos_cosechados WHERE DATE(fecha) = '{$filters->fecha_inicial}' AND lote = tbl.lote AND color_cinta = tbl.cinta) AS form
                FROM(
                    SELECT lote, edad, cinta, class, SUM(blz) AS blz
                    FROM (
                        SELECT lote, h.edad, cinta, c.class, COUNT(1) AS blz
                        FROM produccion_historica h
                        LEFT JOIN produccion_colores c ON c.color = cinta
                        WHERE fecha = '{$filters->fecha_inicial}' AND tipo = 'PROC'
                        GROUP BY lote, h.edad
                        UNION ALL
                        SELECT lote, h.edad, h.color AS cinta, c.class, 0 AS blz
                        FROM racimos_cosechados_by_color h
                        LEFT JOIN produccion_colores c ON c.color = h.color
                        WHERE fecha = '{$filters->fecha_inicial}'
                        GROUP BY lote, h.edad
                    ) AS tbl
                    GROUP BY lote, edad
                ) AS tbl";
        $response->data = $this->db->queryAll($sql);

        # CALCULAR PESO PROM RACIMO
        $pesos_prom = [];
        foreach($response->data as $row){
            $row->peso_prom = $this->getPesoPromCintaMax($row->lote, $row->cinta, $row->edad, $filters->semana);
            $peso_prom[$row->lote][$row->cinta] = $row->peso_prom;
        }

        $sql = "SELECT lote, edad, cinta, class, blz,
                    (SELECT COUNT(1) FROM racimos_cosechados r INNER JOIN racimos_cosechados_detalle ON r.id = id_racimos_cosechados WHERE lote = tbl.lote AND UPPER(color_cinta) = cinta AND DATE(fecha) = '{$filters->fecha_inicial}' AND status = 'RECU') AS form
                FROM(
                    SELECT lote, h.edad, cinta, c.class, COUNT(1) AS blz
                    FROM produccion_historica h
                    LEFT JOIN produccion_colores c ON c.color = cinta
                    WHERE fecha = '{$filters->fecha_inicial}' AND tipo = 'RECUSADO'
                    GROUP BY lote, h.edad
                    UNION ALL
                    SELECT lote, h.edad, h.color AS cinta, c.class, 0 AS blz
                    FROM racimos_cosechados_by_color h
                    LEFT JOIN produccion_colores c ON c.color = h.color
                    WHERE fecha = '{$filters->fecha_inicial}'
                    GROUP BY lote, h.edad
                ) AS tbl
                GROUP BY lote, edad
                HAVING form > 0 OR blz > 0";
        $response->data_recu = $this->db->queryAll($sql);
        foreach($response->data_recu as $row){
            if(!isset($pesos_prom[$row->lote][$row->cinta])){
                $peso_prom[$row->lote][$row->cinta] = $this->getPesoPromCintaMax($row->lote, $row->cinta, $row->edad, $filters->semana, true);
            }
            $row->peso_prom = $peso_prom[$row->lote][$row->cinta];
        }

        $response->faltante_recusados = [];
        foreach($response->data_recu as $row){
            if($row->form > $row->blz){
                $sql = "SELECT *, 
                            getEdadCinta($filters->semana, color_cinta, getYear('{$filters->fecha_inicial}')) AS edad,
                            (SELECT COUNT(1) FROM produccion_historica WHERE causa = tbl.causa AND tipo = 'recusado' AND fecha = '{$filters->fecha_inicial}' AND lote = tbl.lote AND cinta = color_cinta) AS cantidad_blz
                        FROM(
                            SELECT lote, d.`color_cinta`, causa, COUNT(1) AS cantidad
                            FROM racimos_cosechados r
                            INNER JOIN racimos_cosechados_detalle d ON r.id = d.id_racimos_cosechados
                            INNER JOIN produccion_colores c ON c.color = d.`color_cinta`
                            WHERE DATE(fecha) = '{$filters->fecha_inicial}' AND lote = '$row->lote'
                            GROUP BY lote, d.`color_cinta`, causa
                        ) AS tbl
                        HAVING cantidad > cantidad_blz";
                $response->faltante_recusados = array_merge($response->faltante_recusados, $this->db->queryAll($sql));
                foreach($response->faltante_recusados as $row){
                    if(!isset($pesos_prom[$row->lote][$row->cinta])){
                        $peso_prom[$row->lote][$row->color_cinta] = $this->getPesoPromCintaMax($row->lote, $row->color_cinta, $row->edad, $filters->semana, true);
                    }
                    $row->peso_prom = $peso_prom[$row->lote][$row->color_cinta];
                }
            }
        }
    
        return $response;
    }

    private function getPesoPromCintaMax($lote, $cinta, $edad, $semana, $recusado = false){
        $tipo = $recusado ? 'RECUSADO' : 'PROC';
        $sql_max_semana_cinta = "SELECT MAX(semana)
                                FROM produccion_historica
                                WHERE cinta = '{$cinta}' AND lote = '{$lote}' AND edad = '{$edad}' AND semana <= $semana AND tipo = '{$tipo}'";
        $sql_peso_prom = "SELECT ROUND(AVG(peso), 2)
                            FROM produccion_historica
                            WHERE semana = ({$sql_max_semana_cinta})
                                AND lote = '{$lote}'
                                AND cinta = '{$cinta}'
                                AND tipo = '{$tipo}'";
        return $this->db->queryOne($sql_peso_prom);
    }

    public function cuadrarRacimos(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $filters = (object)[
            "finca" => getValueFrom($postdata , "finca" , "" , FILTER_SANITIZE_STRING),
            "fecha_inicial" => getValueFrom($postdata , "fecha_inicial" , "" , FILTER_SANITIZE_STRING),
            "fecha_final" => getValueFrom($postdata , "fecha_final" , "" , FILTER_SANITIZE_STRING),
        ];

        $response->status = 400;
        $dia_procesado = $this->db->queryOne("SELECT COUNT(1) FROM produccion_racimos_cuadrado WHERE fecha = '{$filters->fecha_inicial}'");
        if($dia_procesado == 0){
            $response->status = 200;

            $data = $this->cuadreRacimos();
            $procesados = $data->data;
            $recusados = $data->data_recu;
            foreach($procesados as $row){
                $this->db->query("INSERT INTO produccion_racimos_cuadrado SET fecha = '{$filters->fecha_inicial}', lote = '{$row->lote}', cinta = '{$row->cinta}', blz = '$row->blz', form = '$row->form'");
            }
            foreach($recusados as $row){
                $this->db->query("INSERT INTO produccion_racimos_cuadrado SET fecha = '{$filters->fecha_inicial}', lote = '{$row->lote}', cinta = '{$row->cinta}', blz = '$row->blz', form = '$row->form', status = 'RECU'");
            }
        }

        return $response;
    }

    public function analizisRecusados(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));

        if($postdata->var_recusado == 'cant'){
            $sql = "SELECT
                    causa AS dano,
                    SUM(cantidad) AS 'sum',
                    MIN(cantidad) AS 'min',
                    MAX(cantidad) AS 'max',
                    ROUND(AVG(cantidad), 2) AS 'prom',
                    SUM(IF(semana = 0, cantidad, 0)) AS 'sem_0',
                    SUM(IF(semana = 1, cantidad, 0)) AS 'sem_1',
                    SUM(IF(semana = 2, cantidad, 0)) AS 'sem_2',
                    SUM(IF(semana = 3, cantidad, 0)) AS 'sem_3',
                    SUM(IF(semana = 4, cantidad, 0)) AS 'sem_4',
                    SUM(IF(semana = 5, cantidad, 0)) AS 'sem_5',
                    SUM(IF(semana = 6, cantidad, 0)) AS 'sem_6',
                    SUM(IF(semana = 7, cantidad, 0)) AS 'sem_7',
                    SUM(IF(semana = 8, cantidad, 0)) AS 'sem_8',
                    SUM(IF(semana = 9, cantidad, 0)) AS 'sem_9',
                    SUM(IF(semana = 10, cantidad, 0)) AS 'sem_10',
                    SUM(IF(semana = 11, cantidad, 0)) AS 'sem_11',
                    SUM(IF(semana = 12, cantidad, 0)) AS 'sem_12',
                    SUM(IF(semana = 13, cantidad, 0)) AS 'sem_13',
                    SUM(IF(semana = 14, cantidad, 0)) AS 'sem_14',
                    SUM(IF(semana = 15, cantidad, 0)) AS 'sem_15',
                    SUM(IF(semana = 16, cantidad, 0)) AS 'sem_16',
                    SUM(IF(semana = 17, cantidad, 0)) AS 'sem_17',
                    SUM(IF(semana = 18, cantidad, 0)) AS 'sem_18',
                    SUM(IF(semana = 19, cantidad, 0)) AS 'sem_19',
                    SUM(IF(semana = 20, cantidad, 0)) AS 'sem_20',
                    SUM(IF(semana = 21, cantidad, 0)) AS 'sem_21',
                    SUM(IF(semana = 22, cantidad, 0)) AS 'sem_22',
                    SUM(IF(semana = 23, cantidad, 0)) AS 'sem_23',
                    SUM(IF(semana = 24, cantidad, 0)) AS 'sem_24',
                    SUM(IF(semana = 25, cantidad, 0)) AS 'sem_25',
                    SUM(IF(semana = 26, cantidad, 0)) AS 'sem_26',
                    SUM(IF(semana = 27, cantidad, 0)) AS 'sem_27',
                    SUM(IF(semana = 28, cantidad, 0)) AS 'sem_28',
                    SUM(IF(semana = 29, cantidad, 0)) AS 'sem_29',
                    SUM(IF(semana = 30, cantidad, 0)) AS 'sem_30',
                    SUM(IF(semana = 31, cantidad, 0)) AS 'sem_31',
                    SUM(IF(semana = 32, cantidad, 0)) AS 'sem_32',
                    SUM(IF(semana = 33, cantidad, 0)) AS 'sem_33',
                    SUM(IF(semana = 34, cantidad, 0)) AS 'sem_34',
                    SUM(IF(semana = 35, cantidad, 0)) AS 'sem_35',
                    SUM(IF(semana = 36, cantidad, 0)) AS 'sem_36',
                    SUM(IF(semana = 37, cantidad, 0)) AS 'sem_37',
                    SUM(IF(semana = 38, cantidad, 0)) AS 'sem_38',
                    SUM(IF(semana = 39, cantidad, 0)) AS 'sem_39',
                    SUM(IF(semana = 40, cantidad, 0)) AS 'sem_40',
                    SUM(IF(semana = 41, cantidad, 0)) AS 'sem_41',
                    SUM(IF(semana = 42, cantidad, 0)) AS 'sem_42',
                    SUM(IF(semana = 43, cantidad, 0)) AS 'sem_43',
                    SUM(IF(semana = 44, cantidad, 0)) AS 'sem_44',
                    SUM(IF(semana = 45, cantidad, 0)) AS 'sem_45',
                    SUM(IF(semana = 46, cantidad, 0)) AS 'sem_46',
                    SUM(IF(semana = 47, cantidad, 0)) AS 'sem_47',
                    SUM(IF(semana = 48, cantidad, 0)) AS 'sem_48',
                    SUM(IF(semana = 49, cantidad, 0)) AS 'sem_49',
                    SUM(IF(semana = 50, cantidad, 0)) AS 'sem_50',
                    SUM(IF(semana = 51, cantidad, 0)) AS 'sem_51',
                    SUM(IF(semana = 52, cantidad, 0)) AS 'sem_52',
                    SUM(IF(semana = 53, cantidad, 0)) AS 'sem_53'
                FROM (
                    SELECT semana, causa, COUNT(1) AS cantidad
                    FROM produccion_historica
                    WHERE tipo = 'RECUSADO' AND year = getYear('{$postdata->fecha_inicial}')
                    GROUP BY semana
                ) AS tbl
                GROUP BY causa
                UNION ALL
                SELECT
                    'TOTAL' AS dano,
                    SUM(cantidad) AS 'sum',
                    MIN(cantidad) AS 'min',
                    MAX(cantidad) AS 'max',
                    ROUND(AVG(cantidad), 2) AS 'prom',
                    SUM(IF(semana = 0, cantidad, 0)) AS 'sem_0',
                    SUM(IF(semana = 1, cantidad, 0)) AS 'sem_1',
                    SUM(IF(semana = 2, cantidad, 0)) AS 'sem_2',
                    SUM(IF(semana = 3, cantidad, 0)) AS 'sem_3',
                    SUM(IF(semana = 4, cantidad, 0)) AS 'sem_4',
                    SUM(IF(semana = 5, cantidad, 0)) AS 'sem_5',
                    SUM(IF(semana = 6, cantidad, 0)) AS 'sem_6',
                    SUM(IF(semana = 7, cantidad, 0)) AS 'sem_7',
                    SUM(IF(semana = 8, cantidad, 0)) AS 'sem_8',
                    SUM(IF(semana = 9, cantidad, 0)) AS 'sem_9',
                    SUM(IF(semana = 10, cantidad, 0)) AS 'sem_10',
                    SUM(IF(semana = 11, cantidad, 0)) AS 'sem_11',
                    SUM(IF(semana = 12, cantidad, 0)) AS 'sem_12',
                    SUM(IF(semana = 13, cantidad, 0)) AS 'sem_13',
                    SUM(IF(semana = 14, cantidad, 0)) AS 'sem_14',
                    SUM(IF(semana = 15, cantidad, 0)) AS 'sem_15',
                    SUM(IF(semana = 16, cantidad, 0)) AS 'sem_16',
                    SUM(IF(semana = 17, cantidad, 0)) AS 'sem_17',
                    SUM(IF(semana = 18, cantidad, 0)) AS 'sem_18',
                    SUM(IF(semana = 19, cantidad, 0)) AS 'sem_19',
                    SUM(IF(semana = 20, cantidad, 0)) AS 'sem_20',
                    SUM(IF(semana = 21, cantidad, 0)) AS 'sem_21',
                    SUM(IF(semana = 22, cantidad, 0)) AS 'sem_22',
                    SUM(IF(semana = 23, cantidad, 0)) AS 'sem_23',
                    SUM(IF(semana = 24, cantidad, 0)) AS 'sem_24',
                    SUM(IF(semana = 25, cantidad, 0)) AS 'sem_25',
                    SUM(IF(semana = 26, cantidad, 0)) AS 'sem_26',
                    SUM(IF(semana = 27, cantidad, 0)) AS 'sem_27',
                    SUM(IF(semana = 28, cantidad, 0)) AS 'sem_28',
                    SUM(IF(semana = 29, cantidad, 0)) AS 'sem_29',
                    SUM(IF(semana = 30, cantidad, 0)) AS 'sem_30',
                    SUM(IF(semana = 31, cantidad, 0)) AS 'sem_31',
                    SUM(IF(semana = 32, cantidad, 0)) AS 'sem_32',
                    SUM(IF(semana = 33, cantidad, 0)) AS 'sem_33',
                    SUM(IF(semana = 34, cantidad, 0)) AS 'sem_34',
                    SUM(IF(semana = 35, cantidad, 0)) AS 'sem_35',
                    SUM(IF(semana = 36, cantidad, 0)) AS 'sem_36',
                    SUM(IF(semana = 37, cantidad, 0)) AS 'sem_37',
                    SUM(IF(semana = 38, cantidad, 0)) AS 'sem_38',
                    SUM(IF(semana = 39, cantidad, 0)) AS 'sem_39',
                    SUM(IF(semana = 40, cantidad, 0)) AS 'sem_40',
                    SUM(IF(semana = 41, cantidad, 0)) AS 'sem_41',
                    SUM(IF(semana = 42, cantidad, 0)) AS 'sem_42',
                    SUM(IF(semana = 43, cantidad, 0)) AS 'sem_43',
                    SUM(IF(semana = 44, cantidad, 0)) AS 'sem_44',
                    SUM(IF(semana = 45, cantidad, 0)) AS 'sem_45',
                    SUM(IF(semana = 46, cantidad, 0)) AS 'sem_46',
                    SUM(IF(semana = 47, cantidad, 0)) AS 'sem_47',
                    SUM(IF(semana = 48, cantidad, 0)) AS 'sem_48',
                    SUM(IF(semana = 49, cantidad, 0)) AS 'sem_49',
                    SUM(IF(semana = 50, cantidad, 0)) AS 'sem_50',
                    SUM(IF(semana = 51, cantidad, 0)) AS 'sem_51',
                    SUM(IF(semana = 52, cantidad, 0)) AS 'sem_52',
                    SUM(IF(semana = 53, cantidad, 0)) AS 'sem_53'
                FROM (
                    SELECT semana, causa, COUNT(1) AS cantidad
                    FROM produccion_historica
                    WHERE tipo = 'RECUSADO' AND year = getYear('{$postdata->fecha_inicial}')
                    GROUP BY semana
                ) AS tbl";
            $response->data = $this->db->queryAll($sql);
        }else{
            $sql = "SELECT
                    causa AS dano,
                    SUM(cantidad) AS 'sum',
                    MIN(cantidad) AS 'min',
                    MAX(cantidad) AS 'max',
                    ROUND(AVG(cantidad), 2) AS 'prom',
                    SUM(IF(semana = 0, cantidad, 0)) AS 'sem_0',
                    SUM(IF(semana = 1, cantidad, 0)) AS 'sem_1',
                    SUM(IF(semana = 2, cantidad, 0)) AS 'sem_2',
                    SUM(IF(semana = 3, cantidad, 0)) AS 'sem_3',
                    SUM(IF(semana = 4, cantidad, 0)) AS 'sem_4',
                    SUM(IF(semana = 5, cantidad, 0)) AS 'sem_5',
                    SUM(IF(semana = 6, cantidad, 0)) AS 'sem_6',
                    SUM(IF(semana = 7, cantidad, 0)) AS 'sem_7',
                    SUM(IF(semana = 8, cantidad, 0)) AS 'sem_8',
                    SUM(IF(semana = 9, cantidad, 0)) AS 'sem_9',
                    SUM(IF(semana = 10, cantidad, 0)) AS 'sem_10',
                    SUM(IF(semana = 11, cantidad, 0)) AS 'sem_11',
                    SUM(IF(semana = 12, cantidad, 0)) AS 'sem_12',
                    SUM(IF(semana = 13, cantidad, 0)) AS 'sem_13',
                    SUM(IF(semana = 14, cantidad, 0)) AS 'sem_14',
                    SUM(IF(semana = 15, cantidad, 0)) AS 'sem_15',
                    SUM(IF(semana = 16, cantidad, 0)) AS 'sem_16',
                    SUM(IF(semana = 17, cantidad, 0)) AS 'sem_17',
                    SUM(IF(semana = 18, cantidad, 0)) AS 'sem_18',
                    SUM(IF(semana = 19, cantidad, 0)) AS 'sem_19',
                    SUM(IF(semana = 20, cantidad, 0)) AS 'sem_20',
                    SUM(IF(semana = 21, cantidad, 0)) AS 'sem_21',
                    SUM(IF(semana = 22, cantidad, 0)) AS 'sem_22',
                    SUM(IF(semana = 23, cantidad, 0)) AS 'sem_23',
                    SUM(IF(semana = 24, cantidad, 0)) AS 'sem_24',
                    SUM(IF(semana = 25, cantidad, 0)) AS 'sem_25',
                    SUM(IF(semana = 26, cantidad, 0)) AS 'sem_26',
                    SUM(IF(semana = 27, cantidad, 0)) AS 'sem_27',
                    SUM(IF(semana = 28, cantidad, 0)) AS 'sem_28',
                    SUM(IF(semana = 29, cantidad, 0)) AS 'sem_29',
                    SUM(IF(semana = 30, cantidad, 0)) AS 'sem_30',
                    SUM(IF(semana = 31, cantidad, 0)) AS 'sem_31',
                    SUM(IF(semana = 32, cantidad, 0)) AS 'sem_32',
                    SUM(IF(semana = 33, cantidad, 0)) AS 'sem_33',
                    SUM(IF(semana = 34, cantidad, 0)) AS 'sem_34',
                    SUM(IF(semana = 35, cantidad, 0)) AS 'sem_35',
                    SUM(IF(semana = 36, cantidad, 0)) AS 'sem_36',
                    SUM(IF(semana = 37, cantidad, 0)) AS 'sem_37',
                    SUM(IF(semana = 38, cantidad, 0)) AS 'sem_38',
                    SUM(IF(semana = 39, cantidad, 0)) AS 'sem_39',
                    SUM(IF(semana = 40, cantidad, 0)) AS 'sem_40',
                    SUM(IF(semana = 41, cantidad, 0)) AS 'sem_41',
                    SUM(IF(semana = 42, cantidad, 0)) AS 'sem_42',
                    SUM(IF(semana = 43, cantidad, 0)) AS 'sem_43',
                    SUM(IF(semana = 44, cantidad, 0)) AS 'sem_44',
                    SUM(IF(semana = 45, cantidad, 0)) AS 'sem_45',
                    SUM(IF(semana = 46, cantidad, 0)) AS 'sem_46',
                    SUM(IF(semana = 47, cantidad, 0)) AS 'sem_47',
                    SUM(IF(semana = 48, cantidad, 0)) AS 'sem_48',
                    SUM(IF(semana = 49, cantidad, 0)) AS 'sem_49',
                    SUM(IF(semana = 50, cantidad, 0)) AS 'sem_50',
                    SUM(IF(semana = 51, cantidad, 0)) AS 'sem_51',
                    SUM(IF(semana = 52, cantidad, 0)) AS 'sem_52',
                    SUM(IF(semana = 53, cantidad, 0)) AS 'sem_53'
                FROM (
                    SELECT semana, causa, ROUND(COUNT(1)/(SELECT COUNT(1) FROM produccion_historica WHERE semana = main.semana AND YEAR = getYear('{$postdata->fecha_inicial}'))*100, 2) AS cantidad
                    FROM produccion_historica main
                    WHERE tipo = 'RECUSADO' AND YEAR = getYear('{$postdata->fecha_inicial}')
                    GROUP BY semana
                ) AS tbl
                GROUP BY causa";
            $totales = $this->db->queryRow("SELECT tbl.*, 'TOTAL' AS dano, SUM(SUM) AS 'sum', SUM(MIN) AS 'min', SUM(MAX) AS 'max', SUM(prom) AS 'prom'
                FROM (
                    SELECT
                        'TOTAL' AS dano,
                        '' AS 'sum',
                        '' AS 'min',
                        '' AS 'max',
                        '' AS 'prom',
                        SUM(IF(semana = 0, cantidad, 0))/(SELECT COUNT(1) FROM produccion_historica WHERE YEAR = getYear('{$postdata->fecha_inicial}') AND semana = 0)*100 AS 'sem_0',
                        SUM(IF(semana = 1, cantidad, 0))/(SELECT COUNT(1) FROM produccion_historica WHERE YEAR = getYear('{$postdata->fecha_inicial}') AND semana = 1)*100 AS 'sem_1',
                        SUM(IF(semana = 2, cantidad, 0))/(SELECT COUNT(1) FROM produccion_historica WHERE YEAR = getYear('{$postdata->fecha_inicial}') AND semana = 2)*100 AS 'sem_2',
                        SUM(IF(semana = 3, cantidad, 0))/(SELECT COUNT(1) FROM produccion_historica WHERE YEAR = getYear('{$postdata->fecha_inicial}') AND semana = 3)*100 AS 'sem_3',
                        SUM(IF(semana = 4, cantidad, 0))/(SELECT COUNT(1) FROM produccion_historica WHERE YEAR = getYear('{$postdata->fecha_inicial}') AND semana = 4)*100 AS 'sem_4',
                        SUM(IF(semana = 5, cantidad, 0))/(SELECT COUNT(1) FROM produccion_historica WHERE YEAR = getYear('{$postdata->fecha_inicial}') AND semana = 5)*100 AS 'sem_5',
                        SUM(IF(semana = 6, cantidad, 0))/(SELECT COUNT(1) FROM produccion_historica WHERE YEAR = getYear('{$postdata->fecha_inicial}') AND semana = 6)*100 AS 'sem_6',
                        SUM(IF(semana = 7, cantidad, 0))/(SELECT COUNT(1) FROM produccion_historica WHERE YEAR = getYear('{$postdata->fecha_inicial}') AND semana = 7)*100 AS 'sem_7',
                        SUM(IF(semana = 8, cantidad, 0))/(SELECT COUNT(1) FROM produccion_historica WHERE YEAR = getYear('{$postdata->fecha_inicial}') AND semana = 8)*100 AS 'sem_8',
                        SUM(IF(semana = 9, cantidad, 0))/(SELECT COUNT(1) FROM produccion_historica WHERE YEAR = getYear('{$postdata->fecha_inicial}') AND semana = 9)*100 AS 'sem_9',
                        SUM(IF(semana = 10, cantidad, 0))/(SELECT COUNT(1) FROM produccion_historica WHERE YEAR = getYear('{$postdata->fecha_inicial}') AND semana = 10)*100 AS 'sem_10',
                        SUM(IF(semana = 11, cantidad, 0))/(SELECT COUNT(1) FROM produccion_historica WHERE YEAR = getYear('{$postdata->fecha_inicial}') AND semana = 11)*100 AS 'sem_11',
                        SUM(IF(semana = 12, cantidad, 0))/(SELECT COUNT(1) FROM produccion_historica WHERE YEAR = getYear('{$postdata->fecha_inicial}') AND semana = 12)*100 AS 'sem_12',
                        SUM(IF(semana = 13, cantidad, 0))/(SELECT COUNT(1) FROM produccion_historica WHERE YEAR = getYear('{$postdata->fecha_inicial}') AND semana = 13)*100 AS 'sem_13',
                        SUM(IF(semana = 14, cantidad, 0))/(SELECT COUNT(1) FROM produccion_historica WHERE YEAR = getYear('{$postdata->fecha_inicial}') AND semana = 14)*100 AS 'sem_14',
                        SUM(IF(semana = 15, cantidad, 0))/(SELECT COUNT(1) FROM produccion_historica WHERE YEAR = getYear('{$postdata->fecha_inicial}') AND semana = 15)*100 AS 'sem_15',
                        SUM(IF(semana = 16, cantidad, 0))/(SELECT COUNT(1) FROM produccion_historica WHERE YEAR = getYear('{$postdata->fecha_inicial}') AND semana = 16)*100 AS 'sem_16',
                        SUM(IF(semana = 17, cantidad, 0))/(SELECT COUNT(1) FROM produccion_historica WHERE YEAR = getYear('{$postdata->fecha_inicial}') AND semana = 17)*100 AS 'sem_17',
                        SUM(IF(semana = 18, cantidad, 0))/(SELECT COUNT(1) FROM produccion_historica WHERE YEAR = getYear('{$postdata->fecha_inicial}') AND semana = 18)*100 AS 'sem_18',
                        SUM(IF(semana = 19, cantidad, 0))/(SELECT COUNT(1) FROM produccion_historica WHERE YEAR = getYear('{$postdata->fecha_inicial}') AND semana = 19)*100 AS 'sem_19',
                        SUM(IF(semana = 20, cantidad, 0))/(SELECT COUNT(1) FROM produccion_historica WHERE YEAR = getYear('{$postdata->fecha_inicial}') AND semana = 20)*100 AS 'sem_20',
                        SUM(IF(semana = 21, cantidad, 0))/(SELECT COUNT(1) FROM produccion_historica WHERE YEAR = getYear('{$postdata->fecha_inicial}') AND semana = 21)*100 AS 'sem_21',
                        SUM(IF(semana = 22, cantidad, 0))/(SELECT COUNT(1) FROM produccion_historica WHERE YEAR = getYear('{$postdata->fecha_inicial}') AND semana = 22)*100 AS 'sem_22',
                        SUM(IF(semana = 23, cantidad, 0))/(SELECT COUNT(1) FROM produccion_historica WHERE YEAR = getYear('{$postdata->fecha_inicial}') AND semana = 23)*100 AS 'sem_23',
                        SUM(IF(semana = 24, cantidad, 0))/(SELECT COUNT(1) FROM produccion_historica WHERE YEAR = getYear('{$postdata->fecha_inicial}') AND semana = 24)*100 AS 'sem_24',
                        SUM(IF(semana = 25, cantidad, 0))/(SELECT COUNT(1) FROM produccion_historica WHERE YEAR = getYear('{$postdata->fecha_inicial}') AND semana = 25)*100 AS 'sem_25',
                        SUM(IF(semana = 26, cantidad, 0))/(SELECT COUNT(1) FROM produccion_historica WHERE YEAR = getYear('{$postdata->fecha_inicial}') AND semana = 26)*100 AS 'sem_26',
                        SUM(IF(semana = 27, cantidad, 0))/(SELECT COUNT(1) FROM produccion_historica WHERE YEAR = getYear('{$postdata->fecha_inicial}') AND semana = 27)*100 AS 'sem_27',
                        SUM(IF(semana = 28, cantidad, 0))/(SELECT COUNT(1) FROM produccion_historica WHERE YEAR = getYear('{$postdata->fecha_inicial}') AND semana = 28)*100 AS 'sem_28',
                        SUM(IF(semana = 29, cantidad, 0))/(SELECT COUNT(1) FROM produccion_historica WHERE YEAR = getYear('{$postdata->fecha_inicial}') AND semana = 29)*100 AS 'sem_29',
                        SUM(IF(semana = 30, cantidad, 0))/(SELECT COUNT(1) FROM produccion_historica WHERE YEAR = getYear('{$postdata->fecha_inicial}') AND semana = 30)*100 AS 'sem_30',
                        SUM(IF(semana = 31, cantidad, 0))/(SELECT COUNT(1) FROM produccion_historica WHERE YEAR = getYear('{$postdata->fecha_inicial}') AND semana = 31)*100 AS 'sem_31',
                        SUM(IF(semana = 32, cantidad, 0))/(SELECT COUNT(1) FROM produccion_historica WHERE YEAR = getYear('{$postdata->fecha_inicial}') AND semana = 32)*100 AS 'sem_32',
                        SUM(IF(semana = 33, cantidad, 0))/(SELECT COUNT(1) FROM produccion_historica WHERE YEAR = getYear('{$postdata->fecha_inicial}') AND semana = 33)*100 AS 'sem_33',
                        SUM(IF(semana = 34, cantidad, 0))/(SELECT COUNT(1) FROM produccion_historica WHERE YEAR = getYear('{$postdata->fecha_inicial}') AND semana = 34)*100 AS 'sem_34',
                        SUM(IF(semana = 35, cantidad, 0))/(SELECT COUNT(1) FROM produccion_historica WHERE YEAR = getYear('{$postdata->fecha_inicial}') AND semana = 35)*100 AS 'sem_35',
                        SUM(IF(semana = 36, cantidad, 0))/(SELECT COUNT(1) FROM produccion_historica WHERE YEAR = getYear('{$postdata->fecha_inicial}') AND semana = 36)*100 AS 'sem_36',
                        SUM(IF(semana = 37, cantidad, 0))/(SELECT COUNT(1) FROM produccion_historica WHERE YEAR = getYear('{$postdata->fecha_inicial}') AND semana = 37)*100 AS 'sem_37',
                        SUM(IF(semana = 38, cantidad, 0))/(SELECT COUNT(1) FROM produccion_historica WHERE YEAR = getYear('{$postdata->fecha_inicial}') AND semana = 38)*100 AS 'sem_38',
                        SUM(IF(semana = 39, cantidad, 0))/(SELECT COUNT(1) FROM produccion_historica WHERE YEAR = getYear('{$postdata->fecha_inicial}') AND semana = 39)*100 AS 'sem_39',
                        SUM(IF(semana = 40, cantidad, 0))/(SELECT COUNT(1) FROM produccion_historica WHERE YEAR = getYear('{$postdata->fecha_inicial}') AND semana = 40)*100 AS 'sem_40',
                        SUM(IF(semana = 41, cantidad, 0))/(SELECT COUNT(1) FROM produccion_historica WHERE YEAR = getYear('{$postdata->fecha_inicial}') AND semana = 41)*100 AS 'sem_41',
                        SUM(IF(semana = 42, cantidad, 0))/(SELECT COUNT(1) FROM produccion_historica WHERE YEAR = getYear('{$postdata->fecha_inicial}') AND semana = 42)*100 AS 'sem_42',
                        SUM(IF(semana = 43, cantidad, 0))/(SELECT COUNT(1) FROM produccion_historica WHERE YEAR = getYear('{$postdata->fecha_inicial}') AND semana = 43)*100 AS 'sem_43',
                        SUM(IF(semana = 44, cantidad, 0))/(SELECT COUNT(1) FROM produccion_historica WHERE YEAR = getYear('{$postdata->fecha_inicial}') AND semana = 44)*100 AS 'sem_44',
                        SUM(IF(semana = 45, cantidad, 0))/(SELECT COUNT(1) FROM produccion_historica WHERE YEAR = getYear('{$postdata->fecha_inicial}') AND semana = 45)*100 AS 'sem_45',
                        SUM(IF(semana = 46, cantidad, 0))/(SELECT COUNT(1) FROM produccion_historica WHERE YEAR = getYear('{$postdata->fecha_inicial}') AND semana = 46)*100 AS 'sem_46',
                        SUM(IF(semana = 47, cantidad, 0))/(SELECT COUNT(1) FROM produccion_historica WHERE YEAR = getYear('{$postdata->fecha_inicial}') AND semana = 47)*100 AS 'sem_47',
                        SUM(IF(semana = 48, cantidad, 0))/(SELECT COUNT(1) FROM produccion_historica WHERE YEAR = getYear('{$postdata->fecha_inicial}') AND semana = 48)*100 AS 'sem_48',
                        SUM(IF(semana = 49, cantidad, 0))/(SELECT COUNT(1) FROM produccion_historica WHERE YEAR = getYear('{$postdata->fecha_inicial}') AND semana = 49)*100 AS 'sem_49',
                        SUM(IF(semana = 50, cantidad, 0))/(SELECT COUNT(1) FROM produccion_historica WHERE YEAR = getYear('{$postdata->fecha_inicial}') AND semana = 50)*100 AS 'sem_50',
                        SUM(IF(semana = 51, cantidad, 0))/(SELECT COUNT(1) FROM produccion_historica WHERE YEAR = getYear('{$postdata->fecha_inicial}') AND semana = 51)*100 AS 'sem_51',
                        SUM(IF(semana = 52, cantidad, 0))/(SELECT COUNT(1) FROM produccion_historica WHERE YEAR = getYear('{$postdata->fecha_inicial}') AND semana = 52)*100 AS 'sem_52',
                        SUM(IF(semana = 53, cantidad, 0))/(SELECT COUNT(1) FROM produccion_historica WHERE YEAR = getYear('{$postdata->fecha_inicial}') AND semana = 53)*100 AS 'sem_53'
                    FROM (
                        SELECT semana, causa, COUNT(1) AS cantidad
                        FROM produccion_historica
                        WHERE tipo = 'RECUSADO' AND YEAR = getYear('{$postdata->fecha_inicial}')
                        GROUP BY causa, semana
                    ) AS tbl
                ) AS tbl
                GROUP BY dano");
            $row_totales_gropales = $this->db->queryRow("SELECT SUM(recusados)/SUM(cortados)*100 AS 'sum', MIN(cantidad) AS 'min', MAX(cantidad) AS 'max', AVG(cantidad) AS 'prom'
                FROM (
                    SELECT semana, SUM(recusados) AS recusados, SUM(cortados) AS cortados, IF(recusados > 0, recusados/cortados*100, NULL) AS cantidad
                    FROM (
                        SELECT semana, COUNT(1) AS cortados, SUM(IF(tipo = 'RECUSADO', 1, 0)) AS recusados
                        FROM produccion_historica
                        WHERE YEAR = getYear('{$postdata->fecha_inicial}')
                        GROUP BY semana
                    ) AS tbl
                    GROUP BY semana
                ) AS tbl");

            $totales->prom = $row_totales_gropales->prom;
            $totales->sum = $row_totales_gropales->sum;
            $totales->min = $row_totales_gropales->min;
            $totales->max = $row_totales_gropales->max;
            $response->data = $this->db->queryAll($sql);
            $response->data[] = $totales;
        }
        $response->semanas = $this->db->queryAllOne("SELECT semana FROM produccion_historica WHERE tipo = 'RECUSADO' AND YEAR = getYear('{$postdata->fecha_inicial}') GROUP BY semana");
        return $response;
    }

    public function importar(){
        move_uploaded_file($_FILES["upload"]["tmp_name"], PHRAPI_PATH."utilities/marcel/new/racimos.xlsx");
        $data = $this->readExcelRacimos(PHRAPI_PATH."utilities/marcel/new/racimos.xlsx");
        unlink(PHRAPI_PATH."/utilities/marcel/new/racimos.xlsx");
        return $data;
    }

    private function readExcelRacimos($file){
        include PHRAPI_PATH.'libs/utilities/simplexlsx.class.php';
        include PHRAPI_PATH.'libs/utilities/Encoding.php';
        $xlsx = new SimpleXLSX($file);
	    $rowConfig = new stdClass;
	    $count = 0;
	    $head = [];
        $libros = $xlsx->sheetNames();

        foreach ($libros as $x => $name) {
            $rows = $xlsx->rows($x);
            $fecha = "";
            foreach($rows as $key => $fila){
                if($key == 2){
                    $fecha = $fila[0];
                    $fecha = str_replace("Fecha: ", "", $fecha);
                    if(is_numeric($fecha)){
                        $UNIX_DATE = ($fecha - 25569) * 86400;
                        $fecha = gmdate("Y-m-d", $UNIX_DATE);
                    }else{
                        $fecha = explode("-", $fecha);
                        $fecha = "{$fecha[2]}-{$fecha[1]}-{$fecha[0]}";
                    }
                }
                if($key > 4){
                    if($fila[1] > 0){
                        $cinta = $fila[3];
                        if($cinta == "NEGRA") $cinta = "NEGRO";
                        if($cinta == "AMARI") $cinta = "AMARILLO";
                        if($cinta == "BLANC") $cinta = "BLANCO";

                        $manos = (int) $fila[4];
                        $calibre = (int) $fila[5];
                        $dedos = (int) $fila[6];
                        $isRecusado = $fila[8] != "" || $fila[9] != "";
                        $sql = "INSERT INTO produccion_historica SET 
                                    id_finca = 1, 
                                    finca = 'NUEVA PUBENZA', 
                                    peso = ROUND({$fila[1]} * 0.4536, 2), 
                                    lote = '{$fila[2]}', 
                                    cinta = '{$cinta}', 
                                    manos = IF({$manos} > 0, {$manos}, NULL),
                                    dedos = IF({$dedos} > 0, {$dedos}, NULL),
                                    calibre = IF({$calibre} > 0, {$calibre}, NULL),
                                    edad = getEdadCinta(getWeek('{$fecha}'), '{$cinta}', getYear('{$fecha}')), 
                                    fecha = '{$fecha}', 
                                    semana = getWeek('{$fecha}'), 
                                    year = getYear('{$fecha}')";
                        if($isRecusado){
                            $sql .= ", tipo = 'RECUSADO', causa = '{$fila[9]}'";
                        }else{
                            $sql .= ", tipo = 'PROC'";
                        }
                        D($sql);
                        $this->db->query($sql);
                    }
                }
            }
        }
    }
}
