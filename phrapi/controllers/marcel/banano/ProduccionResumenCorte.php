<?php defined('PHRAPI') or die("Direct access not allowed!");

class ProduccionResumenCorte {
	public $name;
	private $db;
	private $config;

	public function __construct(){
		$this->config = $GLOBALS['config'];
		$this->session = Session::getInstance();
        $this->db = DB::getInstance($this->session->agent_user);
    }
    
    private function params(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $filters = (object)[
            'fecha' => getValueFrom($postdata, 'fecha_inicial', FILTER_SANITIZE_STRING),
            'id_finca' => (int) getValueFrom($postdata, 'id_finca', FILTER_SANITIZE_STRING)
        ];
        return $filters;
    }
    
    public function resumenProceso(){
        $response = new stdClass;
        $filters = $this->params();

        $sql = "SELECT *
                FROM (
                    SELECT fincas.id, fincas.nombre
                    FROM produccion_historica
                    INNER JOIN fincas ON id_finca = fincas.id
                    WHERE fecha = '{$filters->fecha}'
                    GROUP BY id_finca
                    UNION ALL
                    SELECT fincas.id, fincas.nombre
                    FROM produccion_cajas
                    INNER JOIN fincas ON id_finca = fincas.id
                    WHERE fecha = '{$filters->fecha}'
                    GROUP BY id_finca
                ) tbl
                GROUP BY id";
        $response->fincas = $this->db->queryAll($sql);
        
        $response->data = [
            [
                "name" => "RACIMOS CORTADOS",
                "value" => $this->getRacimosCortadosInicioFin($filters->fecha, $filters->fecha, $filters->id_finca)
            ],
            [
                "name" => "RACIMOS PROCESADOS",
                "value" => $this->getRacimosProcesadosInicioFin($filters->fecha, $filters->fecha, $filters->id_finca)
            ],
            [
                "name" => "RACIMOS RECUSADOS",
                "value" => 0
            ],
            [
                "name" => "PESO CAJAS (KG)",
                "value" => $this->getPesoCajasKG($filters->fecha, $filters->fecha, $filters->id_finca)
            ],
            [
                "name" => "PESO RACIMOS (KG)",
                "value" => $this->getPesoRacimosKG($filters->fecha, $filters->fecha, $filters->id_finca)
            ],
            [
                "name" => "% TALLO",
                "value" => $this->getTallo($filters->fecha, $filters->fecha, $filters->id_finca)
            ],
            [
                "name" => "PESO RAC PROM (KG)",
                "value" => $this->getPesoPromRacimosKg($filters->fecha, $filters->fecha, $filters->id_finca)
            ],
            [
                "name" => "CALIB 2DA PROM",
                "value" => $this->getCalibreProm($filters->fecha, $filters->fecha, $filters->id_finca)
            ],
            [
                "name" => "CALIB ULT PROM",
                "value" => $this->getCalibreUltProm($filters->fecha, $filters->fecha, $filters->id_finca)
            ],
            [
                "name" => "MANOS PROM",
                "value" => $this->getManosProm($filters->fecha, $filters->fecha, $filters->id_finca)
            ],
            [
                "name" => "L DEDO PROM",
                "value" => $this->getLargoDedosProm($filters->fecha, $filters->fecha, $filters->id_finca)
            ],
            [
                "name" => "RATIO CORT",
                "value" => $this->getCajasRatioCortadoInicioFin($filters->fecha, $filters->fecha, $filters->id_finca)
            ],
            [
                "name" => "RATIO PROC",
                "value" => $this->getCajasRatioProcesadoInicioFin($filters->fecha, $filters->fecha, $filters->id_finca)
            ],
            [
                "name" => "MERMA CORT",
                "value" => $this->getMermaCortadaInicioFin($filters->fecha, $filters->fecha, $filters->id_finca)
            ],
            [
                "name" => "MERMA PROC",
                "value" => $this->getMermaProcesadaInicioFin($filters->fecha, $filters->fecha, $filters->id_finca)
            ]
        ];

        // RACIMOS RECUSADOS
        $response->data[2]["value"] = $response->data[0]["value"] - $response->data[1]["value"];

        return $response;
    }

    /* GET VALUES */
    private function getRacimosProcesadosInicioFin($inicio, $fin, $id_finca){
        $racimos_cortados = "SELECT COUNT(1) FROM produccion_historica WHERE fecha BETWEEN '{$inicio}' AND '{$fin}' AND id_finca = {$id_finca}";
        return $this->db->queryOne($racimos_cortados);
    }
    private function getCajasConvertidasRealesInicioFin($inicio, $fin, $id_finca){
        $sql = "SELECT SUM(cajas_reales)
                FROM (
                    SELECT marca, ROUND(SUM(IF(blz > guia, blz, guia) * IF(convertir = 0, 1, peso_prom / 0.4536 / 40.5)), 0) AS cajas_reales
                    FROM (
                        SELECT tbl.marca, tbl.fecha, blz, SUM(IFNULL(valor,0)) AS guia, peso_prom, IF(con.id IS NULL, 0, 1) AS convertir
                        FROM (
                            SELECT cajas.marca, fecha, COUNT(1) AS blz, AVG(caja) AS peso_prom
                            FROM produccion_cajas cajas
                            WHERE fecha BETWEEN '{$inicio}' AND '{$fin}'
                            GROUP BY cajas.marca, fecha
                        ) AS tbl
                        LEFT JOIN produccion_cajas_real guia ON tbl.fecha = guia.fecha AND tbl.marca = guia.marca AND STATUS = 'PROCESADO'
                        LEFT JOIN produccion_cajas_convertir con ON con.marca = tbl.marca
                        GROUP BY tbl.fecha, tbl.marca
                    ) AS tbl
                    GROUP BY marca
                    UNION ALL
                    SELECT marca, ROUND(SUM(IF(blz > guia, blz, guia) * IF(convertir = 0, 1, peso_prom / 0.4536 / 40.5)), 0) AS cajas_reales
                    FROM (
                        SELECT tbl.marca, tbl.fecha, blz, SUM(IFNULL(valor,0)) AS guia, peso_prom, IF(con.id IS NULL, 0, 1) AS convertir
                        FROM (
                            SELECT cajas.marca, fecha, COUNT(1) AS blz, AVG(peso) AS peso_prom
                            FROM produccion_gavetas cajas
                            WHERE fecha BETWEEN '{$inicio}' AND '{$fin}'
                            GROUP BY cajas.marca, fecha
                        ) AS tbl
                        LEFT JOIN produccion_cajas_real guia ON tbl.fecha = guia.fecha AND tbl.marca = guia.marca AND STATUS = 'PROCESADO'
                        LEFT JOIN produccion_cajas_convertir con ON con.marca = tbl.marca
                        GROUP BY tbl.fecha, tbl.marca
                    ) AS tbl
                    GROUP BY marca
                ) AS tbl";
        return $this->db->queryOne($sql);
    }
    private function getCajasRatioCortadoInicioFin($inicio, $fin, $id_finca){
        $response = new stdClass;

        $exist_ratio_preprocesado = "SELECT IF(MAX(fecha) <= $fin, 1, 0)+IF(MIN(fecha) >= $inicio, 1, 0) FROM produccion_cajas_ratios_preprocesados";
        $exist_ratio_preprocesado = $this->db->queryOne($exist_ratio_preprocesado) == 2;

        if($exist_ratio_preprocesado){
            $sql = "SELECT ROUND(AVG(ratio_cortado), 2) AS ratio
                    FROM produccion_cajas_ratios_preprocesados
                    WHERE getYear(fecha) = {$year} AND fecha BETWEEN '{$inicio}' AND '{$fin}'";
            $val = $this->db->queryOne($sql);
        }else{
            $racimos = $this->getRacimosCortadosInicioFin($inicio, $fin, $id_finca);
            $cajas_conv = $this->getCajasConvertidasRealesInicioFin($inicio, $fin, $id_finca);
            $val = $racimos > 0 ? round(($cajas_conv / $racimos), 2) : 0;
        }
        return $val;
    }
    private function getCajasRatioProcesadoInicioFin($inicio, $fin, $id_finca){
        $response = new stdClass;

        $exist_ratio_preprocesado = "SELECT IF(MAX(fecha) <= $fin, 1, 0)+IF(MIN(fecha) >= $inicio, 1, 0) FROM produccion_cajas_ratios_preprocesados";
        $exist_ratio_preprocesado = $this->db->queryOne($exist_ratio_preprocesado) == 2;

        if($exist_ratio_preprocesado){
            $sql = "SELECT ROUND(AVG(ratio_cortado), 2) AS ratio
                    FROM produccion_cajas_ratios_preprocesados
                    WHERE getYear(fecha) = {$year} AND fecha BETWEEN '{$inicio}' AND '{$fin}'";
            $val = $this->db->queryOne($sql);
        }else{
            $racimos = $this->getRacimosProcesadosInicioFin($inicio, $fin, $id_finca);
            $cajas_conv = $this->getCajasConvertidasRealesInicioFin($inicio, $fin, $id_finca);
            $val = $racimos > 0 ? round(($cajas_conv / $racimos), 2) : 0;
        }
        return $val;
    }
    private function getRacimosCortadosInicioFin($inicio, $fin, $id_finca){
        $racimos_cortados = "SELECT COUNT(1) FROM produccion_historica WHERE fecha BETWEEN '{$inicio}' AND '{$fin}' AND id_finca = {$id_finca}";
        return $this->db->queryOne($racimos_cortados);
    }
    private function getPesoCajasKG($inicio, $fin, $id_finca){
        $sql = "SELECT SUM(peso)
                FROM (
                    SELECT SUM(kg) AS peso
                    FROM produccion_cajas
                    WHERE fecha BETWEEN '{$inicio}' AND '{$fin}'
                    UNION ALL
                    SELECT SUM(kg) AS peso
                    FROM produccion_gavetas
                    WHERE fecha BETWEEN '{$inicio}' AND '{$fin}'
                ) AS tbl";
        return (float) $this->db->queryOne($sql);
    }
    private function getPesoRacimosKG($inicio, $fin, $id_finca){
        $sql = "SELECT SUM(peso)
                FROM (
                    SELECT SUM(peso) AS peso
                    FROM produccion_historica
                    WHERE fecha BETWEEN '{$inicio}' AND '{$fin}' AND id_finca = {$id_finca}
                ) AS tbl";
        return (float) $this->db->queryOne($sql);
    }
    private function getPesoRacimosProcesoKG($inicio, $fin, $id_finca){
        $sql = "SELECT SUM(peso)
                FROM (
                    SELECT SUM(peso) AS peso
                    FROM produccion_historica
                    WHERE fecha BETWEEN '{$inicio}' AND '{$fin}' AND tipo = 'PROC' AND id_finca = {$id_finca}
                ) AS tbl";
        return (float) $this->db->queryOne($sql);
    }
    private function getPesoPromRacimosKg($inicio, $fin, $id_finca){
        $sql = "SELECT AVG(peso) AS peso
                FROM produccion_historica
                WHERE fecha BETWEEN '{$inicio}' AND '{$fin}' AND id_finca = {$id_finca}";
        return (float) $this->db->queryOne($sql);
    }
    private function getCalibreProm($inicio, $fin, $id_finca){
        $sql = "SELECT AVG(calibre)
                FROM produccion_historica
                WHERE fecha BETWEEN '{$inicio}' AND '{$fin}' AND calibre > 0 AND id_finca = {$id_finca}";
        return (float) $this->db->queryOne($sql);
    }
    private function getCalibreUltProm($inicio, $fin, $id_finca){
        $sql = "SELECT AVG(calibre_ultima)
                FROM produccion_historica
                WHERE fecha BETWEEN '{$inicio}' AND '{$fin}' AND calibre_ultima > 0 AND id_finca = {$id_finca}";
        return (float) $this->db->queryOne($sql);
    }
    private function getManosProm($inicio, $fin, $id_finca){
        $sql = "SELECT AVG(manos)
                FROM produccion_historica
                WHERE fecha BETWEEN '{$inicio}' AND '{$fin}' AND manos > 0 AND id_finca = {$id_finca}";
        return (float) $this->db->queryOne($sql);
    }
    private function getLargoDedosProm($inicio, $fin, $id_finca){
        $sql = "SELECT AVG(dedos)
                FROM produccion_historica
                WHERE fecha BETWEEN '{$inicio}' AND '{$fin}' AND dedos > 0 AND id_finca = {$id_finca}";
        return (float) $this->db->queryOne($sql);
    }
    private function getTallo($inicio, $fin, $id_finca){
        $sql = "SELECT ROUND(SUM(tallo) / SUM(racimo) * 100, 2)
                FROM merma main
                WHERE date_fecha BETWEEN '{$inicio}' AND '{$fin}' AND id_finca = {$id_finca}";
        return (float) $this->db->queryOne($sql);
    }
    private function getMermaCortadaInicioFin($inicio, $fin, $id_finca){
        $peso_cajas = $this->getPesoCajasKG($inicio, $fin, $id_finca);
        if($peso_cajas == 0) return '';
        $peso_racimos = $this->getPesoRacimosKG($inicio, $fin, $id_finca);
        $val = round(($peso_racimos - $peso_cajas) / $peso_racimos * 100, 2);
        return $val;
    }
    private function getMermaProcesadaInicioFin($inicio, $fin, $id_finca){
        $peso_cajas = $this->getPesoCajasKG($inicio, $fin, $id_finca);
        if($peso_cajas == 0) return '';
        $peso_racimos = $this->getPesoRacimosProcesoKG($inicio, $fin, $id_finca);
        $val = round(($peso_racimos - $peso_cajas) / $peso_racimos * 100, 2);
        return $val;
    }
    /* GET VALUES */
}
