<?php defined('PHRAPI') or die("Direct access not allowed!");

class LaboresAgricolas {
    public $name;
    private $db;
    private $config;

    public function __construct(){
        $this->config = $GLOBALS['config'];
        $this->session = Session::getInstance();
        $this->db = DB::getInstance($this->session->agent_user);

    }

    public function tendencia(){
        $response = new stdClass;

        $sql_hacienda_week = "SELECT *
            FROM (
                SELECT value, fincas.`nombre` AS serie, periodos.periodo AS legend
                FROM (
                    SELECT periodo
                    FROM muestras
                    WHERE YEAR(fecha) = YEAR(CURRENT_DATE)
                    GROUP BY periodo
                ) AS periodos
                JOIN (
                    SELECT id, nombre
                    FROM fincas
                ) AS fincas
                LEFT JOIN (
                    SELECT idFinca, periodo, ROUND(AVG(IF(valor = 1, 100, 0)), 2) AS value
                    FROM muestras
                    WHERE YEAR(fecha) = YEAR(CURRENT_DATE) 
                    GROUP BY idFinca, periodo
                ) AS valores ON periodos.periodo = valores.periodo AND idFinca = fincas.`id`
            ) AS valores
            ORDER BY legend";
        $data_por_periodo = $this->db->queryAll($sql_hacienda_week);
        $response->week = (object)[
            "legends" => [],
            "series" => [
            ]
        ];
        foreach($data_por_periodo as $row){
            if(!in_array($row->legend, $response->week->legends)){
                $response->week->legends[] = $row->legend;
            }
            if(!in_array($row->serie, array_keys($response->week->series))){
                $response->week->series[$row->serie] = [
                    "name" => $row->serie,
                    "connectNulls" => true,
                    "type" => "line",
                    "itemStyle" => [
                        "normal" => [
                            "barBorderRadius" => 0,
                            "barBorderWidth" => 6
                        ]
                    ],
                    "data" => []
                ];
            }
            $response->week->series[$row->serie]["data"][] = $row->value;
        }

        return $response;
    }

    public function tablaHistorica(){
        $response = new stdClass;

        $response->data = [];
        $response->umbrals = $this->session->umbrals;

        $fincas = $this->db->queryAll("SELECT idFinca, fincas.nombre AS finca FROM muestras INNER JOIN fincas ON idFinca = fincas.id WHERE anio = YEAR(CURRENT_DATE) GROUP BY idFinca");
        $periodos = $this->db->queryAll("SELECT periodo FROM muestras WHERE anio = YEAR(CURRENT_DATE) AND periodo > 0 GROUP BY periodo");

        foreach($fincas as $fila1){
            $idFinca = $fila1->idFinca;

            foreach($periodos as $s){
                $periodo = $s->periodo;
                $fila1->{"PERIODO {$periodo}"} = $this->db->queryOne("SELECT round(SUM(IF(valor = 1, 1, 0))/COUNT(1) * 100, 2)
                    FROM muestras 
                    WHERE anio = YEAR(CURRENT_DATE)
                        AND periodo = {$periodo}
                        AND idFinca = {$idFinca}");
            }

            #segundo nivel
            $lotes = $this->db->queryAll("SELECT idLote, lotes.nombre AS lote FROM muestras INNER JOIN lotes ON idLote = lotes.id WHERE anio = YEAR(CURRENT_DATE) AND muestras.idFinca = {$idFinca} GROUP BY idLote");
            foreach($lotes as $fila2){
                $idLote = $fila2->idLote;

                foreach($periodos as $s2){
                    $periodo2 = $s2->periodo;
                    $fila2->{"PERIODO {$periodo2}"} = $this->db->queryOne("SELECT round(SUM(IF(valor = 1, 1, 0)) / COUNT(1) * 100, 2)
                        FROM muestras 
                        WHERE anio = YEAR(CURRENT_DATE) 
                            AND periodo = {$periodo2} 
                            AND idFinca = {$idFinca} 
                            AND idLote = {$idLote}");
                }

                #tercer nivel
                $labores = $this->db->queryAll("
                    SELECT idLabor, l.nombre AS labor
                    FROM muestras
                    INNER JOIN labores l ON l.id = idLabor AND status = 1
                    WHERE anio = YEAR(CURRENT_DATE) AND idLote = {$idLote} AND idFinca = {$idFinca}
                    GROUP BY idLabor
                    ORDER BY l.nombre
                ");
                foreach($labores as $fila3){
                    $idLabor = $fila3->idLabor;

                    foreach($periodos as $s3){
                        $periodo3 = $s3->periodo;
                        $fila3->{"PERIODO {$periodo3}"} = $this->db->queryOne("SELECT round(SUM(IF(valor = 1, 1, 0))/COUNT(1) * 100, 2)
                            FROM muestras
                            WHERE anio = YEAR(CURRENT_DATE) 
                                AND periodo = {$periodo3} 
                                AND idLote = {$idLote} 
                                AND idLabor = {$idLabor} 
                                AND idFinca = {$idFinca}
                        ");
                    }

                    #cuarto nivel
                    $causas = $this->db->queryAll("SELECT laborCausaDesc AS causa
                        FROM muestras 
                        WHERE anio = YEAR(CURRENT_DATE) 
                            AND idFinca = {$idFinca} 
                            AND idLote = {$idLote} 
                            AND idLabor = {$idLabor}
                        GROUP BY laborCausaDesc");
                    foreach($causas as $fila4){
                        foreach($periodos as $s4){
                            $periodo4 = $s4->periodo;
                            $fila4->{"PERIODO {$periodo4}"} = $this->db->queryOne("SELECT round(SUM(IF(valor = 1, 1, 0))/COUNT(1) * 100, 2)
                                FROM muestras
                                WHERE anio = YEAR(CURRENT_DATE) 
                                    AND periodo = {$periodo4}
                                    AND idFinca = {$idFinca}
                                    AND idLote = {$idLote}
                                    AND idLabor = {$idLabor} 
                                    AND laborCausaDesc = '{$fila4->causa}'");
                        }
                    }
                    $fila3->detalle = $causas;
                }
                $fila2->detalle = $labores;
            }
            $fila1->detalle = $lotes;
        }
        $response->periodos = $periodos;
        $response->data = $fincas;
        return $response;
    }

    public function graficaLabores(){
        $response = new stdClass;

        $response->main = $this->db->queryAll("SELECT idLabor, labores.nombre AS name, ROUND(AVG(IF(valor = 1, 100, 0)), 2) AS value 
            FROM muestras 
            INNER JOIN labores ON idLabor = labores.id 
            WHERE anio = YEAR(CURRENT_DATE) 
            GROUP BY idLabor
            HAVING value > 0");
        foreach($response->main as $row){
            $causas = $this->db->queryAll("SELECT causa AS name, SUM(num_danos) AS value 
                FROM muestras_anual_resumen_causas 
                WHERE anio = YEAR(CURRENT_DATE) 
                    AND idLabor = {$row->idLabor} 
                    AND num_danos > 0 
                GROUP BY causa");
            foreach($causas as $causa){
                $response->labores[$row->name] = $causas;
            }
        }
        return $response;
    }

    public function getFotos(){
        $response = new stdClass;
        $response->fotos = $this->db->queryAll("SELECT periodo FROM muestras anual INNER JOIN muestras_imagenes ON idMuestra = anual.id INNER JOIN labores ON idLabor = labores.id WHERE anio = YEAR(CURRENT_DATE) GROUP BY periodo");
        foreach($response->fotos as $periodo){
            $periodo->fincas = $this->db->queryAll("SELECT idFinca, fincas.nombre AS finca FROM muestras anual INNER JOIN muestras_imagenes ON idMuestra = anual.id INNER JOIN fincas ON idFinca = fincas.id WHERE anio = YEAR(CURRENT_DATE) AND periodo = $periodo->periodo GROUP BY idFinca");
            foreach($periodo->fincas as $finca){
                $finca->lotes = $this->db->queryAll("SELECT idLote, lotes.nombre AS lote 
                    FROM muestras anual 
                    INNER JOIN muestras_imagenes ON idMuestra = anual.id 
                    INNER JOIN lotes ON idLote = lotes.id 
                    WHERE anual.idFinca = $finca->idFinca 
                        AND anio = YEAR(CURRENT_DATE)
                        AND periodo = $periodo->periodo
                    GROUP BY idLote");
                foreach($finca->lotes as $lote){
                    $lote->labores = $this->db->queryAll("SELECT idLabor, labores.nombre AS labor 
                        FROM muestras anual 
                        INNER JOIN muestras_imagenes ON idMuestra = anual.id 
                        INNER JOIN labores ON idLabor = labores.id 
                        WHERE idFinca = $finca->idFinca 
                            AND idLote = $lote->idLote 
                            AND periodo = $periodo->periodo
                            AND anio = YEAR(CURRENT_DATE) 
                        GROUP BY idLabor");
                    foreach($lote->labores as $labor){
                        $labor->imagenes = $this->db->queryAll("SELECT imagen AS url, anual.fecha, IF(anual.valor = 1, 100, 0) AS promedio 
                            FROM muestras anual 
                            INNER JOIN muestras_imagenes ON idMuestra = anual.id 
                            INNER JOIN labores ON idLabor = labores.id 
                            WHERE idFinca = $finca->idFinca 
                                AND idLote = $lote->idLote 
                                AND idLabor = $labor->idLabor 
                                AND periodo = $periodo->periodo 
                                AND anio = YEAR(CURRENT_DATE)");
                    }
                }
            }
        }
        return $response;
    }
}