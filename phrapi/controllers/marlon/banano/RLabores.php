<?php defined('PHRAPI') or die("Direct access not allowed!");
include PHRAPI_PATH.'libs/agroaudit/Graphics.php';
class RLabores {
    public $name;
    private $db;
    private $config;
    private $token;
    private $Graphics;

    public function __construct(){
        $this->config = $GLOBALS['config'];
        $this->session = Session::getInstance();
        // D($this->session);
        $this->token = '289150995ed28e8860f9161d3cc9f259';
        // $this->Service = new ServicesNode('http://procesos-iq.com:3000/' , $this->token);
        $this->Graphics = new Graphics();
        // F($this->Graphics);
        $this->maindb = DB::getInstance();
        $this->db = DB::getInstance($this->session->agent_user);

    }

    public function zonas(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $idZona = getValueFrom($postdata,'idZona','',FILTER_SANITIZE_PHRAPI_INT);
        $idfinca = getValueFrom($postdata,'idFinca','',FILTER_SANITIZE_PHRAPI_INT);
        $idLote = getValueFrom($postdata,'idLote','',FILTER_SANITIZE_PHRAPI_INT);
        $idZonaLabor = getValueFrom($postdata,'idZonaLabor','',FILTER_SANITIZE_PHRAPI_INT);
        $fecha_fin = getValueFrom($postdata,'fecha_final','',FILTER_SANITIZE_STRING);
        $fecha_inicio = getValueFrom($postdata,'fecha_inicial','',FILTER_SANITIZE_STRING);
        $response = new stdClass;

        $sql_tittle = "SELECT
                        (SELECT nombre FROM labores WHERE id = '{$idZonaLabor}') AS labor";
        $tittle = $this->db->queryRow($sql_tittle);
        $response->tittle = "LABOR ".$tittle->labor;

        $sql = "SELECT causa , causa AS label , CausasIndividual , buenas , malas ,muestras,valor,
            CASE
                WHEN valor > 0 THEN  ((buenas / muestras) * 100) 
                ELSE ((((1-(buenas / muestras))*CausasIndividual) / malas)*100)
            END AS porcentaje,
            CASE
                WHEN valor > 0 THEN  ((buenas / muestras) * 100) 
                ELSE ((((1-(buenas / muestras))*CausasIndividual) / malas)*100)
            END AS value,
            CASE
                WHEN valor > 0 THEN  NULL
                ELSE (CausasIndividual / malas) * 100
            END AS porcentaje2
            FROM 
            (SELECT laborCausaDesc AS causa ,COUNT( causa) AS CausasIndividual,valor,
            (SELECT COUNT(*) AS total_muestras FROM muestras WHERE valor > 0 AND idLabor = '{$idZonaLabor}' AND fecha BETWEEN '{$fecha_inicio}' AND '{$fecha_fin}') AS buenas,
            (SELECT COUNT(*) AS total_muestras FROM muestras WHERE valor = 0 AND idLabor = '{$idZonaLabor}' AND fecha BETWEEN '{$fecha_inicio}' AND '{$fecha_fin}')  AS malas,
            (SELECT COUNT(distinct archivoJson_contador , archivoJson) AS total_causas FROM muestras WHERE 
                    fecha BETWEEN '{$fecha_inicio}' AND '{$fecha_fin}' AND
                    idLabor = '{$idZonaLabor}') AS muestras
            FROM muestras 
            WHERE 
                fecha BETWEEN '{$fecha_inicio}' AND '{$fecha_fin}' AND
                idLabor = '{$idZonaLabor}'
            GROUP BY idLaborCausa) as t
            ORDER BY valor DESC , causa ASC";
        $response->causas = $this->db->queryAll($sql);


        $sql_labor = "SELECT idLabor , idZona , zona AS nombre,zona AS label, TRUNCATE(AVG(promedio) , 2) AS promedio, TRUNCATE(AVG(promedio) , 2) AS value,tipo_labor, labor FROM (SELECT       
                    CASE 
                        WHEN mes = 0 THEN 'Ene'
                        WHEN mes = 1 THEN 'Feb'
                        WHEN mes = 2 THEN 'Mar'
                        WHEN mes = 3 THEN 'Abr'
                        WHEN mes = 4 THEN 'May'
                        WHEN mes = 5 THEN 'Jun'
                        WHEN mes = 6 THEN 'Jul'
                        WHEN mes = 7 THEN 'Ago'
                        WHEN mes = 8 THEN 'Sep'
                        WHEN mes = 9 THEN 'Oct'
                        WHEN mes = 10 THEN 'Nov'
                        WHEN mes = 11 THEN 'Dic'
                    END
                    AS mes,
                    (SELECT nombre from zonas where id = idZona) AS zona,idZona ,
        (SELECT nombre from fincas where id = idFinca) AS finca,idFinca ,
        (SELECT nombre from lotes where id = idlote) AS lote,idLote ,
        (SELECT nombre from labores where id = idLabor) AS labor,idLabor ,
        (SELECT nombre from tipo_labor where id = idTipoLabor) AS tipo_labor,idTipoLabor ,
        AVG(promedio) AS promedio FROM muestras_anual
         WHERE  fecha BETWEEN '{$fecha_inicio}' AND '{$fecha_fin}' AND idLabor = '{$idZonaLabor}'
        GROUP BY idZona , idFinca , idLote ,idTipoLabor,idLabor , mes) AS labor
        GROUP BY idZona
        ORDER BY tipo_labor";
        $response->zonas = $this->db->queryAll($sql_labor);

        $response->labor_zona->mes = [];
        $response->labor_zona->semana = [];
        $sql_labor_promedio = "SELECT mes,idLabor , idZona , zona AS nombre,zona AS label, TRUNCATE(AVG(promedio) , 2) AS promedio, TRUNCATE(AVG(promedio) , 2) AS value,tipo_labor, labor FROM (SELECT       
                    CASE 
                        WHEN mes = 0 THEN 'Ene'
                        WHEN mes = 1 THEN 'Feb'
                        WHEN mes = 2 THEN 'Mar'
                        WHEN mes = 3 THEN 'Abr'
                        WHEN mes = 4 THEN 'May'
                        WHEN mes = 5 THEN 'Jun'
                        WHEN mes = 6 THEN 'Jul'
                        WHEN mes = 7 THEN 'Ago'
                        WHEN mes = 8 THEN 'Sep'
                        WHEN mes = 9 THEN 'Oct'
                        WHEN mes = 10 THEN 'Nov'
                        WHEN mes = 11 THEN 'Dic'
                    END
                    AS mes,
                    (SELECT nombre from zonas where id = idZona) AS zona,idZona ,
        (SELECT nombre from fincas where id = idFinca) AS finca,idFinca ,
        (SELECT nombre from lotes where id = idlote) AS lote,idLote ,
        (SELECT nombre from labores where id = idLabor) AS labor,idLabor ,
        (SELECT nombre from tipo_labor where id = idTipoLabor) AS tipo_labor,idTipoLabor ,
        AVG(promedio) AS promedio FROM muestras_anual
         WHERE idLabor = '{$idZonaLabor}' AND YEAR(fecha) = YEAR(CURRENT_DATE)
        GROUP BY idZona , idFinca , idLote ,idTipoLabor,idLabor , mes) AS labor
        GROUP BY mes
        ORDER BY mes ,tipo_labor";
        $mes_labor = $this->db->queryAll($sql_labor_promedio);

        $sql_labor_promedio = "SELECT semana,idLabor , idZona , zona AS nombre,zona AS label, TRUNCATE(AVG(promedio) , 2) AS promedio, TRUNCATE(AVG(promedio) , 2) AS value,tipo_labor, labor FROM (SELECT       
                    CASE 
                        WHEN mes = 0 THEN 'Ene'
                        WHEN mes = 1 THEN 'Feb'
                        WHEN mes = 2 THEN 'Mar'
                        WHEN mes = 3 THEN 'Abr'
                        WHEN mes = 4 THEN 'May'
                        WHEN mes = 5 THEN 'Jun'
                        WHEN mes = 6 THEN 'Jul'
                        WHEN mes = 7 THEN 'Ago'
                        WHEN mes = 8 THEN 'Sep'
                        WHEN mes = 9 THEN 'Oct'
                        WHEN mes = 10 THEN 'Nov'
                        WHEN mes = 11 THEN 'Dic'
                    END
                    AS mes,semana,
                    (SELECT nombre from zonas where id = idZona) AS zona,idZona ,
        (SELECT nombre from fincas where id = idFinca) AS finca,idFinca ,
        (SELECT nombre from lotes where id = idlote) AS lote,idLote ,
        (SELECT nombre from labores where id = idLabor) AS labor,idLabor ,
        (SELECT nombre from tipo_labor where id = idTipoLabor) AS tipo_labor,idTipoLabor ,
        AVG(promedio) AS promedio FROM muestras_anual
         WHERE idLabor = '{$idZonaLabor}' AND YEAR(fecha) = YEAR(CURRENT_DATE)
        GROUP BY idZona , idFinca , idLote ,idTipoLabor,idLabor , mes) AS labor
        GROUP BY semana
        ORDER BY semana ,tipo_labor";
        $semana_labor = $this->db->queryAll($sql_labor_promedio);

        $months_des = ["Ene" , "Feb"  , "Mar" , "Abr" , "May" , "Jun" , "Jul" ,"Ago", "Sep" , "Oct" , "Nov" , "Dic"];
        $months = ["Ene"  => [], "Feb"  => [] 
        , "Mar"  => [], "Abr"  => [], "May"  => [], "Jun"  => [], "Jul"  => [],"Ago" => [], "Sep"  => [], 
        "Oct"  => [], "Nov"  => [], "Dic" => []];

         $months_labores = ["Ene"  => "-", "Feb"  => "-" 
        , "Mar"  => "-", "Abr"  => "-", "May"  => "-", "Jun"  => "-", "Jul"  => "-","Ago" => "-", "Sep"  => "-", 
        "Oct"  => "-", "Nov"  => "-", "Dic" => "-"];

        $week = [];
        $weeks = [];
        $weeks_labores = [];
        for ($i=1; $i <= 52; $i++) { 
            $week[$i] = [];
            $weeks[] = $i;
            $weeks_labores[$i] = "-";
        }


        $sql_hacienda_month = "SELECT CASE 
                WHEN mes = 0 THEN 'Ene'
                WHEN mes = 1 THEN 'Feb'
                WHEN mes = 2 THEN 'Mar'
                WHEN mes = 3 THEN 'Abr'
                WHEN mes = 4 THEN 'May'
                WHEN mes = 5 THEN 'Jun'
                WHEN mes = 6 THEN 'Jul'
                WHEN mes = 7 THEN 'Ago'
                WHEN mes = 8 THEN 'Sep'
                WHEN mes = 9 THEN 'Oct'
                WHEN mes = 10 THEN 'Nov'
                WHEN mes = 11 THEN 'Dic'
            END
         AS id, AVG(promedio) AS label FROM muestras_anual WHERE YEAR(fecha) = YEAR(CURRENT_DATE) GROUP BY mes";
        $sql_hacienda_week = "SELECT semana AS id, AVG(promedio) AS label FROM muestras_anual WHERE YEAR(fecha) = YEAR(CURRENT_DATE)  GROUP BY semana";
        $sql_zones_month = "SELECT CASE 
                WHEN mes = 0 THEN 'Ene'
                WHEN mes = 1 THEN 'Feb'
                WHEN mes = 2 THEN 'Mar'
                WHEN mes = 3 THEN 'Abr'
                WHEN mes = 4 THEN 'May'
                WHEN mes = 5 THEN 'Jun'
                WHEN mes = 6 THEN 'Jul'
                WHEN mes = 7 THEN 'Ago'
                WHEN mes = 8 THEN 'Sep'
                WHEN mes = 9 THEN 'Oct'
                WHEN mes = 10 THEN 'Nov'
                WHEN mes = 11 THEN 'Dic'
            END
         AS id,mes,(SELECT nombre from zonas where id = idZona) AS zona,idZona , idLabor FROM muestras_anual
                  WHERE idLabor = '{$idZonaLabor}' AND YEAR(fecha) = YEAR(CURRENT_DATE)
            GROUP BY idZona , mes,idLabor";
        $sql_zones_week = "SELECT semana AS label,(SELECT nombre from zonas where id = idZona) AS zona,idZona,idLabor  FROM muestras_anual 
        WHERE idLabor = '{$idZonaLabor}' AND YEAR(fecha) = YEAR(CURRENT_DATE)
        GROUP BY idZona , semana , idLabor";


        $response->week = $this->db->queryAllSpecial($sql_hacienda_week);
        $response->main = $this->db->queryAllSpecial($sql_hacienda_month);
        $response->categories = new stdClass;
        $response->categories->months = $months_des;
        $response->categories->weeks = $weeks;
        $response->zonas_main = new stdClass;
        $response->zonas_main->zonas = [0 => $this->session->alias] + 
            $this->db->queryAllSpecial("SELECT (SELECT nombre from zonas where id = idZona) AS id  , (SELECT nombre from zonas where id = idZona) AS label FROM muestras_anual WHERE YEAR(fecha) = YEAR(CURRENT_DATE) GROUP BY idZona") + 
            $this->db->queryAllSpecial("SELECT nombre AS id , nombre AS label FROM labores WHERE id = '{$idZonaLabor}'");
        $months_zones = $this->db->queryAll($sql_zones_month);
        $weeks_zones = $this->db->queryAll($sql_zones_week);
        $response->zonas_main->auditoria = $this->db->queryAll($sql_zones_month);
        $response->labor_name = $this->db->queryOne("SELECT nombre FROM labores WHERE id = '{$idZonaLabor}'");
        $response->auditoria = [];

        foreach ($months_zones as $key => $value) {
            $response->zonas_main->months[$value->zona][$value->id] = $this->getPromZonaMonth($value->idZona , $value->idLabor , $value->mes)->promedio;
        }

        foreach ($weeks_zones as $key => $value) {
            $response->zonas_main->weeks[$value->zona][$value->label] = $this->getPromZonaWeek($value->idZona  , $value->idLabor, $value->label)->promedio;
        }

        foreach ($mes_labor as $key => $value) {
            $response->zonas_main->mes[$response->labor_name][$value->mes] = $value->promedio;
        }

        foreach ($semana_labor as $key => $value) {
            $response->zonas_main->semana[$response->labor_name][$value->semana] = $value->promedio;
        }

        $response->id_company = $this->session->id_company;
        $response->umbrals = $this->session->umbrals;


        return $response;
    }

    public function fincas(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $idZona = getValueFrom($postdata,'idZona','',FILTER_SANITIZE_PHRAPI_INT);
        $idfinca = getValueFrom($postdata,'idFinca','',FILTER_SANITIZE_PHRAPI_INT);
        $idLote = getValueFrom($postdata,'idLote','',FILTER_SANITIZE_PHRAPI_INT);
        $idZonaLabor = getValueFrom($postdata,'idZonaLabor','',FILTER_SANITIZE_PHRAPI_INT);
        $idFincaLabor = getValueFrom($postdata,'idFincaLabor','',FILTER_SANITIZE_PHRAPI_INT);
        $fecha_fin = getValueFrom($postdata,'fecha_final','',FILTER_SANITIZE_STRING);
        $fecha_inicio = getValueFrom($postdata,'fecha_inicial','',FILTER_SANITIZE_STRING);
        $response = new stdClass;

        $sql_tittle = "SELECT
                        (SELECT nombre FROM zonas WHERE id = '{$idFincaLabor}') AS zona,
                        (SELECT nombre FROM labores WHERE id = '{$idZonaLabor}') AS labor";
        $tittle = $this->db->queryRow($sql_tittle);

        $response->tittle = "ZONA ".$tittle->zona.". LABOR ".$tittle->labor;

        if($this->session->id_company == 4){
            $response->tittle = "Orodelti . LABOR ".$tittle->labor;
            $sql = "SELECT causa ,AVG(porcentaje) AS porcentaje , AVG(porcentaje2) AS porcentaje2 FROM (SELECT campo AS causa ,  
                    CASE
                        WHEN (SELECT valor FROM labores_causas WHERE causa LIKE CONCAT('%',muestras_detalles.campo,'%') AND idLabor = muestras_detalles.idLabor ) > 0 THEN valor
                        ELSE NULL
                    END AS porcentaje,
                    CASE
                        WHEN (SELECT valor FROM labores_causas WHERE causa LIKE CONCAT('%',muestras_detalles.campo,'%') AND idLabor = muestras_detalles.idLabor ) > 0 THEN NULL
                        ELSE valor
                    END AS porcentaje2
                    FROM muestras_detalles
                    WHERE valor > 0 AND fecha BETWEEN '{$fecha_inicio}' AND '{$fecha_fin}' AND
                    idZona = '{$idFincaLabor}' AND idLabor = '{$idZonaLabor}') AS details
                    WHERE porcentaje > 0 OR porcentaje2 > 0
                    GROUP BY causa";
        }else{
            $sql = "SELECT causa , CausasIndividual , buenas , malas ,muestras,valor,
                CASE
                    WHEN valor > 0 THEN  ((buenas / muestras) * 100) 
                    ELSE ((((1-(buenas / muestras))*CausasIndividual) / malas)*100)
                END AS porcentaje,
                CASE
                    WHEN valor > 0 THEN  NULL
                    ELSE (CausasIndividual / malas) * 100
                END AS porcentaje2
                FROM 
                (SELECT laborCausaDesc AS causa ,COUNT( causa) AS CausasIndividual,valor,
                (SELECT COUNT(*) AS total_muestras FROM muestras WHERE idZona = '{$idFincaLabor}' AND valor > 0 AND idLabor = '{$idZonaLabor}' AND fecha BETWEEN '{$fecha_inicio}' AND '{$fecha_fin}') AS buenas,
                (SELECT COUNT(*) AS total_muestras FROM muestras WHERE idZona = '{$idFincaLabor}' AND valor = 0 AND idLabor = '{$idZonaLabor}' AND fecha BETWEEN '{$fecha_inicio}' AND '{$fecha_fin}')  AS malas,
                (SELECT COUNT(distinct archivoJson_contador , archivoJson) AS total_causas FROM muestras WHERE 
                        fecha BETWEEN '{$fecha_inicio}' AND '{$fecha_fin}' AND
                        idZona = '{$idFincaLabor}' AND idLabor = '{$idZonaLabor}') AS muestras
                FROM muestras 
                WHERE 
                    fecha BETWEEN '{$fecha_inicio}' AND '{$fecha_fin}' AND
                    idZona = '{$idFincaLabor}' AND idLabor = '{$idZonaLabor}'
                GROUP BY idLaborCausa) as t
                ORDER BY valor DESC , causa ASC";
        }
        $response->causas = $this->db->queryAll($sql);


        $sql_labor = "SELECT idLabor , idFinca , idZona , finca AS nombre,zona AS label, TRUNCATE(AVG(promedio) , 2) AS promedio, TRUNCATE(AVG(promedio) , 2) AS value,tipo_labor, labor FROM (SELECT       
                    CASE 
                        WHEN mes = 0 THEN 'Ene'
                        WHEN mes = 1 THEN 'Feb'
                        WHEN mes = 2 THEN 'Mar'
                        WHEN mes = 3 THEN 'Abr'
                        WHEN mes = 4 THEN 'May'
                        WHEN mes = 5 THEN 'Jun'
                        WHEN mes = 6 THEN 'Jul'
                        WHEN mes = 7 THEN 'Ago'
                        WHEN mes = 8 THEN 'Sep'
                        WHEN mes = 9 THEN 'Oct'
                        WHEN mes = 10 THEN 'Nov'
                        WHEN mes = 11 THEN 'Dic'
                    END
                    AS mes,
                    (SELECT nombre from zonas where id = idZona) AS zona,idZona ,
        (SELECT nombre from fincas where id = idFinca) AS finca,idFinca ,
        (SELECT nombre from lotes where id = idlote) AS lote,idLote ,
        (SELECT nombre from labores where id = idLabor) AS labor,idLabor ,
        (SELECT nombre from tipo_labor where id = idTipoLabor) AS tipo_labor,idTipoLabor ,
        AVG(promedio) AS promedio FROM muestras_anual
         WHERE  fecha BETWEEN '{$fecha_inicio}' AND '{$fecha_fin}' AND idLabor = '{$idZonaLabor}' AND idZona = '{$idFincaLabor}'
        GROUP BY idZona , idFinca , idLote ,idTipoLabor,idLabor , mes) AS labor
        GROUP BY idFinca
        ORDER BY tipo_labor";
        $response->fincas = $this->db->queryAll($sql_labor);

        $response->labor_zona->mes = [];
        $response->labor_zona->semana = [];
        $sql_labor_promedio = "SELECT mes,idLabor , idZona , zona AS nombre,zona AS label, TRUNCATE(AVG(promedio) , 2) AS promedio, TRUNCATE(AVG(promedio) , 2) AS value,tipo_labor, labor FROM (SELECT       
                    CASE 
                        WHEN mes = 0 THEN 'Ene'
                        WHEN mes = 1 THEN 'Feb'
                        WHEN mes = 2 THEN 'Mar'
                        WHEN mes = 3 THEN 'Abr'
                        WHEN mes = 4 THEN 'May'
                        WHEN mes = 5 THEN 'Jun'
                        WHEN mes = 6 THEN 'Jul'
                        WHEN mes = 7 THEN 'Ago'
                        WHEN mes = 8 THEN 'Sep'
                        WHEN mes = 9 THEN 'Oct'
                        WHEN mes = 10 THEN 'Nov'
                        WHEN mes = 11 THEN 'Dic'
                    END
                    AS mes,
                    (SELECT nombre from zonas where id = idZona) AS zona,idZona ,
        (SELECT nombre from fincas where id = idFinca) AS finca,idFinca ,
        (SELECT nombre from lotes where id = idlote) AS lote,idLote ,
        (SELECT nombre from labores where id = idLabor) AS labor,idLabor ,
        (SELECT nombre from tipo_labor where id = idTipoLabor) AS tipo_labor,idTipoLabor ,
        AVG(promedio) AS promedio FROM muestras_anual
         WHERE idLabor = '{$idZonaLabor}' AND idZona = '{$idFincaLabor}' AND YEAR(fecha) = YEAR(CURRENT_DATE)
        GROUP BY idZona , idFinca , idLote ,idTipoLabor,idLabor , mes) AS labor
        GROUP BY mes
        ORDER BY mes ,tipo_labor";
        $mes_labor = $this->db->queryAll($sql_labor_promedio);

        $sql_labor_promedio = "SELECT semana,idLabor , idZona , zona AS nombre,zona AS label, TRUNCATE(AVG(promedio) , 2) AS promedio, TRUNCATE(AVG(promedio) , 2) AS value,tipo_labor, labor FROM 
        (SELECT
                    CASE 
                        WHEN mes = 0 THEN 'Ene'
                        WHEN mes = 1 THEN 'Feb'
                        WHEN mes = 2 THEN 'Mar'
                        WHEN mes = 3 THEN 'Abr'
                        WHEN mes = 4 THEN 'May'
                        WHEN mes = 5 THEN 'Jun'
                        WHEN mes = 6 THEN 'Jul'
                        WHEN mes = 7 THEN 'Ago'
                        WHEN mes = 8 THEN 'Sep'
                        WHEN mes = 9 THEN 'Oct'
                        WHEN mes = 10 THEN 'Nov'
                        WHEN mes = 11 THEN 'Dic'
                    END
                    AS mes,semana,
                    (SELECT nombre from zonas where id = idZona) AS zona,idZona ,
        (SELECT nombre from fincas where id = idFinca) AS finca,idFinca ,
        (SELECT nombre from lotes where id = idlote) AS lote,idLote ,
        (SELECT nombre from labores where id = idLabor) AS labor,idLabor ,
        (SELECT nombre from tipo_labor where id = idTipoLabor) AS tipo_labor,idTipoLabor ,
        AVG(promedio) AS promedio FROM muestras_anual
         WHERE idLabor = '{$idZonaLabor}' AND idZona = '{$idFincaLabor}' AND YEAR(fecha) = YEAR(CURRENT_DATE)
        GROUP BY idZona , idFinca , idLote ,idTipoLabor,idLabor , mes) AS labor
        GROUP BY semana
        ORDER BY semana ,tipo_labor";
        $semana_labor = $this->db->queryAll($sql_labor_promedio);

        $months_des = ["Ene" , "Feb"  , "Mar" , "Abr" , "May" , "Jun" , "Jul" ,"Ago", "Sep" , "Oct" , "Nov" , "Dic"];
        $months = ["Ene"  => [], "Feb"  => [] 
        , "Mar"  => [], "Abr"  => [], "May"  => [], "Jun"  => [], "Jul"  => [],"Ago" => [], "Sep"  => [], 
        "Oct"  => [], "Nov"  => [], "Dic" => []];

         $months_labores = ["Ene"  => "-", "Feb"  => "-" 
        , "Mar"  => "-", "Abr"  => "-", "May"  => "-", "Jun"  => "-", "Jul"  => "-","Ago" => "-", "Sep"  => "-", 
        "Oct"  => "-", "Nov"  => "-", "Dic" => "-"];

        $week = [];
        $weeks = [];
        $weeks_labores = [];
        for ($i=1; $i <= 52; $i++) { 
            $week[$i] = [];
            $weeks[] = $i;
            $weeks_labores[$i] = "-";
        }


        $sql_hacienda_month = "SELECT CASE 
                WHEN mes = 0 THEN 'Ene'
                WHEN mes = 1 THEN 'Feb'
                WHEN mes = 2 THEN 'Mar'
                WHEN mes = 3 THEN 'Abr'
                WHEN mes = 4 THEN 'May'
                WHEN mes = 5 THEN 'Jun'
                WHEN mes = 6 THEN 'Jul'
                WHEN mes = 7 THEN 'Ago'
                WHEN mes = 8 THEN 'Sep'
                WHEN mes = 9 THEN 'Oct'
                WHEN mes = 10 THEN 'Nov'
                WHEN mes = 11 THEN 'Dic'
            END
         AS id, AVG(promedio) AS label FROM muestras_anual WHERE YEAR(fecha) = YEAR(CURRENT_DATE) GROUP BY mes";
        $sql_hacienda_week = "SELECT semana AS id, AVG(promedio) AS label FROM muestras_anual WHERE YEAR(fecha) = YEAR(CURRENT_DATE) GROUP BY semana";
        $sql_zones_month = "SELECT CASE 
                WHEN mes = 0 THEN 'Ene'
                WHEN mes = 1 THEN 'Feb'
                WHEN mes = 2 THEN 'Mar'
                WHEN mes = 3 THEN 'Abr'
                WHEN mes = 4 THEN 'May'
                WHEN mes = 5 THEN 'Jun'
                WHEN mes = 6 THEN 'Jul'
                WHEN mes = 7 THEN 'Ago'
                WHEN mes = 8 THEN 'Sep'
                WHEN mes = 9 THEN 'Oct'
                WHEN mes = 10 THEN 'Nov'
                WHEN mes = 11 THEN 'Dic'
            END
         AS id,mes,(SELECT nombre from fincas where id = idFinca) AS finca,idZona ,idFinca, idLabor FROM muestras_anual
                  WHERE idLabor = '{$idZonaLabor}' AND idZona = '{$idFincaLabor}' AND YEAR(fecha) = YEAR(CURRENT_DATE)
            GROUP BY idFinca , mes,idLabor";
        $sql_zones_week = "SELECT semana AS label,(SELECT nombre from fincas where id = idFinca) AS zona,idZona,idFinca,idLabor  FROM muestras_anual 
        WHERE idLabor = '{$idZonaLabor}' AND idZona = '{$idFincaLabor}' AND YEAR(fecha) = YEAR(CURRENT_DATE)
        GROUP BY idFinca , semana , idLabor";


        $response->week = $this->db->queryAllSpecial($sql_hacienda_week);
        $response->main = $this->db->queryAllSpecial($sql_hacienda_month);
        $response->categories = new stdClass;
        $response->categories->months = $months_des;
        $response->categories->weeks = $weeks;
        $response->zonas_main = new stdClass;
        $response->zonas_main->zonas = [0 => $this->session->alias] + 
            $this->db->queryAllSpecial("SELECT (SELECT nombre from fincas where id = idFinca) AS id  , (SELECT nombre from fincas where id = idFinca) AS label FROM muestras_anual WHERE YEAR(fecha) = YEAR(CURRENT_DATE) GROUP BY idFinca") + 
            $this->db->queryAllSpecial("SELECT nombre AS id , nombre AS label FROM labores WHERE id = '{$idZonaLabor}'");
        $months_zones = $this->db->queryAll($sql_zones_month);
        $weeks_zones = $this->db->queryAll($sql_zones_week);
        $response->zonas_main->auditoria = $this->db->queryAll($sql_zones_month);
        $response->labor_name = $this->db->queryOne("SELECT nombre FROM labores WHERE id = '{$idZonaLabor}'");
        $response->auditoria = [];

        foreach ($months_zones as $key => $value) {
            $response->zonas_main->months[$value->finca][$value->id] = $this->getPromFincaMonth($value->idZona , $value->idLabor , $value->mes , $value->idFinca)->promedio;
        }

        foreach ($weeks_zones as $key => $value) {
            $response->zonas_main->weeks[$value->finca][$value->label] = $this->getPromFincaWeek($value->idZona  , $value->idLabor, $value->label , $value->idFinca)->promedio;
        }

        foreach ($mes_labor as $key => $value) {
            $response->zonas_main->mes[$response->labor_name][$value->mes] = $value->promedio;
        }

        foreach ($semana_labor as $key => $value) {
            $response->zonas_main->semana[$response->labor_name][$value->semana] = $value->promedio;
        }

        $response->id_company = $this->session->id_company;
        $response->umbrals = $this->session->umbrals;

        return $response;
    }

    public function lotes(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $idZona = getValueFrom($postdata,'idZona','',FILTER_SANITIZE_PHRAPI_INT);
        $idFinca = getValueFrom($postdata,'idFinca','',FILTER_SANITIZE_PHRAPI_INT);
        $idLote = getValueFrom($postdata,'idLote','',FILTER_SANITIZE_PHRAPI_INT);
        $idZonaLabor = getValueFrom($postdata,'idZonaLabor','',FILTER_SANITIZE_PHRAPI_INT);
        $idFincaLabor = getValueFrom($postdata,'idFincaLabor','',FILTER_SANITIZE_PHRAPI_INT);
        $idLoteLabor = getValueFrom($postdata,'idLoteLabor','',FILTER_SANITIZE_PHRAPI_INT);
        $fecha_fin = getValueFrom($postdata,'fecha_final','',FILTER_SANITIZE_STRING);
        $fecha_inicio = getValueFrom($postdata,'fecha_inicial','',FILTER_SANITIZE_STRING);
        $response = new stdClass;

        $sql_tittle = "SELECT
            (SELECT nombre FROM zonas WHERE id = '{$idFincaLabor}') AS zona,
            (SELECT nombre FROM fincas WHERE id = '{$idLoteLabor}') AS finca,
            (SELECT nombre FROM labores WHERE id = '{$idZonaLabor}') AS labor";
        $tittle = $this->db->queryRow($sql_tittle);
        $response->tittle = "ZONA ".$tittle->zona.". FINCA ".$tittle->finca.". LABOR ".$tittle->labor;

        if($this->session->id_company == 4){
            $response->tittle = "Orodelti . FINCA ".$tittle->finca.". LABOR ".$tittle->labor;
            $sql = "SELECT causa ,AVG(porcentaje) AS porcentaje , AVG(porcentaje2) AS porcentaje2 FROM (SELECT campo AS causa ,  
                    CASE
                        WHEN (SELECT valor FROM labores_causas WHERE causa LIKE CONCAT('%',muestras_detalles.campo,'%') AND idLabor = muestras_detalles.idLabor ) > 0 THEN valor
                        ELSE NULL
                    END AS porcentaje,
                    CASE
                        WHEN (SELECT valor FROM labores_causas WHERE causa LIKE CONCAT('%',muestras_detalles.campo,'%') AND idLabor = muestras_detalles.idLabor ) > 0 THEN NULL
                        ELSE valor
                    END AS porcentaje2
                    FROM muestras_detalles
                    WHERE valor > 0 AND fecha BETWEEN '{$fecha_inicio}' AND '{$fecha_fin}' AND
                    idFinca = '{$idLoteLabor}' AND idLabor = '{$idZonaLabor}') AS details
                    WHERE porcentaje > 0 OR porcentaje2 > 0
                    GROUP BY causa";
        }else{
            $sql = "SELECT causa , CausasIndividual , buenas , malas ,muestras,valor,
                CASE
                    WHEN valor > 0 THEN  ((buenas / muestras) * 100) 
                    ELSE ((((1-(buenas / muestras))*CausasIndividual) / malas)*100)
                END AS porcentaje,
                CASE
                    WHEN valor > 0 THEN  NULL
                    ELSE (CausasIndividual / malas) * 100
                END AS porcentaje2
                FROM 
                (SELECT laborCausaDesc AS causa ,COUNT( causa) AS CausasIndividual,valor,
                (SELECT COUNT(*) AS total_muestras FROM muestras WHERE idZona = '{$idFincaLabor}' AND idFinca = '{$idLoteLabor}' AND valor > 0 AND idLabor = '{$idZonaLabor}' AND fecha BETWEEN '{$fecha_inicio}' AND '{$fecha_fin}') AS buenas,
                (SELECT COUNT(*) AS total_muestras FROM muestras WHERE idZona = '{$idFincaLabor}' AND idFinca = '{$idLoteLabor}' AND valor = 0 AND idLabor = '{$idZonaLabor}' AND fecha BETWEEN '{$fecha_inicio}' AND '{$fecha_fin}')  AS malas,
                (SELECT COUNT(distinct archivoJson_contador , archivoJson) AS total_causas FROM muestras WHERE 
                        fecha BETWEEN '{$fecha_inicio}' AND '{$fecha_fin}' AND
                        idZona = '{$idFincaLabor}' AND idFinca = '{$idLoteLabor}' AND idLabor = '{$idZonaLabor}') AS muestras
                FROM muestras 
                WHERE 
                    fecha BETWEEN '{$fecha_inicio}' AND '{$fecha_fin}' AND
                    idZona = '{$idFincaLabor}' AND idFinca = '{$idLoteLabor}' AND idLabor = '{$idZonaLabor}'
                GROUP BY idLaborCausa) as t
                ORDER BY valor DESC , causa ASC";
        }
        $response->causas = $this->db->queryAll($sql);


        
        $sql_labor = "SELECT idZona , idFinca ,idLabor , idLote , lote AS nombre,zona AS label, TRUNCATE(AVG(promedio) , 2) AS promedio, TRUNCATE(AVG(promedio) , 2) AS value,tipo_labor, labor FROM (SELECT       
                    CASE 
                        WHEN mes = 0 THEN 'Ene'
                        WHEN mes = 1 THEN 'Feb'
                        WHEN mes = 2 THEN 'Mar'
                        WHEN mes = 3 THEN 'Abr'
                        WHEN mes = 4 THEN 'May'
                        WHEN mes = 5 THEN 'Jun'
                        WHEN mes = 6 THEN 'Jul'
                        WHEN mes = 7 THEN 'Ago'
                        WHEN mes = 8 THEN 'Sep'
                        WHEN mes = 9 THEN 'Oct'
                        WHEN mes = 10 THEN 'Nov'
                        WHEN mes = 11 THEN 'Dic'
                    END
                    AS mes,
                    (SELECT nombre from zonas where id = idZona) AS zona,idZona ,
        (SELECT nombre from fincas where id = idFinca) AS finca,idFinca ,
        (SELECT nombre from lotes where id = idlote) AS lote,idLote ,
        (SELECT nombre from labores where id = idLabor) AS labor,idLabor ,
        (SELECT nombre from tipo_labor where id = idTipoLabor) AS tipo_labor,idTipoLabor ,
        AVG(promedio) AS promedio FROM muestras_anual
         WHERE  fecha BETWEEN '{$fecha_inicio}' AND '{$fecha_fin}' AND idLabor = '{$idZonaLabor}' AND idZona = '{$idFincaLabor}' AND idFinca = '{$idLoteLabor}'
        GROUP BY idZona , idFinca , idLote ,idTipoLabor,idLabor , mes) AS labor
        GROUP BY idLote
        ORDER BY tipo_labor";
        // D($sql_labor);
        $response->lotes = $this->db->queryAll($sql_labor);

        $response->labor_zona->mes = [];
        $response->labor_zona->semana = [];
        $sql_labor_promedio = "SELECT mes,idLabor , idZona , zona AS nombre,zona AS label, TRUNCATE(AVG(promedio) , 2) AS promedio, TRUNCATE(AVG(promedio) , 2) AS value,tipo_labor, labor FROM (SELECT       
                    CASE 
                        WHEN mes = 0 THEN 'Ene'
                        WHEN mes = 1 THEN 'Feb'
                        WHEN mes = 2 THEN 'Mar'
                        WHEN mes = 3 THEN 'Abr'
                        WHEN mes = 4 THEN 'May'
                        WHEN mes = 5 THEN 'Jun'
                        WHEN mes = 6 THEN 'Jul'
                        WHEN mes = 7 THEN 'Ago'
                        WHEN mes = 8 THEN 'Sep'
                        WHEN mes = 9 THEN 'Oct'
                        WHEN mes = 10 THEN 'Nov'
                        WHEN mes = 11 THEN 'Dic'
                    END
                    AS mes,
                    (SELECT nombre from zonas where id = idZona) AS zona,idZona ,
        (SELECT nombre from fincas where id = idFinca) AS finca,idFinca ,
        (SELECT nombre from lotes where id = idlote) AS lote,idLote ,
        (SELECT nombre from labores where id = idLabor) AS labor,idLabor ,
        (SELECT nombre from tipo_labor where id = idTipoLabor) AS tipo_labor,idTipoLabor ,
        AVG(promedio) AS promedio FROM muestras_anual
         WHERE idLabor = '{$idZonaLabor}' AND idZona = '{$idFincaLabor}' AND idFinca = '{$idLoteLabor}' AND YEAR(fecha) = YEAR(CURRENT_DATE)
        GROUP BY idZona , idFinca , idLote ,idTipoLabor,idLabor , mes) AS labor
        GROUP BY mes
        ORDER BY mes ,tipo_labor";
        $mes_labor = $this->db->queryAll($sql_labor_promedio);

        $sql_labor_promedio = "SELECT semana,idLabor , idZona , zona AS nombre,zona AS label, TRUNCATE(AVG(promedio) , 2) AS promedio, TRUNCATE(AVG(promedio) , 2) AS value,tipo_labor, labor FROM 
        (SELECT
                    CASE 
                        WHEN mes = 0 THEN 'Ene'
                        WHEN mes = 1 THEN 'Feb'
                        WHEN mes = 2 THEN 'Mar'
                        WHEN mes = 3 THEN 'Abr'
                        WHEN mes = 4 THEN 'May'
                        WHEN mes = 5 THEN 'Jun'
                        WHEN mes = 6 THEN 'Jul'
                        WHEN mes = 7 THEN 'Ago'
                        WHEN mes = 8 THEN 'Sep'
                        WHEN mes = 9 THEN 'Oct'
                        WHEN mes = 10 THEN 'Nov'
                        WHEN mes = 11 THEN 'Dic'
                    END
                    AS mes,semana,
                    (SELECT nombre from zonas where id = idZona) AS zona,idZona ,
        (SELECT nombre from fincas where id = idFinca) AS finca,idFinca ,
        (SELECT nombre from lotes where id = idlote) AS lote,idLote ,
        (SELECT nombre from labores where id = idLabor) AS labor,idLabor ,
        (SELECT nombre from tipo_labor where id = idTipoLabor) AS tipo_labor,idTipoLabor ,
        AVG(promedio) AS promedio FROM muestras_anual
         WHERE idLabor = '{$idZonaLabor}' AND idZona = '{$idFincaLabor}' AND idFinca = '{$idLoteLabor}' AND YEAR(fecha) = YEAR(CURRENT_DATE) 
        GROUP BY idZona , idFinca , idLote ,idTipoLabor,idLabor , mes) AS labor
        GROUP BY semana
        ORDER BY semana ,tipo_labor";
        $semana_labor = $this->db->queryAll($sql_labor_promedio);

        $months_des = ["Ene" , "Feb"  , "Mar" , "Abr" , "May" , "Jun" , "Jul" ,"Ago", "Sep" , "Oct" , "Nov" , "Dic"];
        $months = ["Ene"  => [], "Feb"  => [] 
        , "Mar"  => [], "Abr"  => [], "May"  => [], "Jun"  => [], "Jul"  => [],"Ago" => [], "Sep"  => [], 
        "Oct"  => [], "Nov"  => [], "Dic" => []];

         $months_labores = ["Ene"  => "-", "Feb"  => "-" 
        , "Mar"  => "-", "Abr"  => "-", "May"  => "-", "Jun"  => "-", "Jul"  => "-","Ago" => "-", "Sep"  => "-", 
        "Oct"  => "-", "Nov"  => "-", "Dic" => "-"];

        $week = [];
        $weeks = [];
        $weeks_labores = [];
        for ($i=1; $i <= 52; $i++) { 
            $week[$i] = [];
            $weeks[] = $i;
            $weeks_labores[$i] = "-";
        }


        $sql_hacienda_month = "SELECT *
            FROM (
                SELECT CASE 
                        WHEN mes = 0 THEN 'Ene'
                        WHEN mes = 1 THEN 'Feb'
                        WHEN mes = 2 THEN 'Mar'
                        WHEN mes = 3 THEN 'Abr'
                        WHEN mes = 4 THEN 'May'
                        WHEN mes = 5 THEN 'Jun'
                        WHEN mes = 6 THEN 'Jul'
                        WHEN mes = 7 THEN 'Ago'
                        WHEN mes = 8 THEN 'Sep'
                        WHEN mes = 9 THEN 'Oct'
                        WHEN mes = 10 THEN 'Nov'
                        WHEN mes = 11 THEN 'Dic'
                    END AS legend, ROUND(AVG(promedio), 2) AS value , 'PROMEDIO GENERAL' AS serie, mes
                FROM muestras_anual 
                WHERE YEAR(fecha) = YEAR(CURRENT_DATE) 
                GROUP BY mes
                UNION ALL
                SELECT *
                FROM (
                    SELECT *
                    FROM (
                        SELECT CASE meses.mes
                            WHEN 0 THEN 'Ene'
                            WHEN 1 THEN 'Feb'
                            WHEN 2 THEN 'Mar'
                            WHEN 3 THEN 'Abr'
                            WHEN 4 THEN 'May'
                            WHEN 5 THEN 'Jun'
                            WHEN 6 THEN 'Jul'
                            WHEN 7 THEN 'Ago'
                            WHEN 8 THEN 'Sep'
                            WHEN 9 THEN 'Oct'
                            WHEN 10 THEN 'Nov'
                            WHEN 11 THEN 'Dic'
                        END AS legend, value, labores.`nombre` AS serie, meses.mes
                        FROM (
                            SELECT mes
                            FROM muestras_anual
                            WHERE YEAR(fecha) = YEAR(CURRENT_DATE)
                            GROUP BY mes
                        ) AS meses
                        JOIN (
                            SELECT id, nombre
                            FROM labores
                            WHERE id = {$idLabor}
                        ) AS labores
                        LEFT JOIN (
                            SELECT idLabor, mes, ROUND(AVG(promedio), 2) AS value
                            FROM muestras_anual
                            WHERE YEAR(fecha) = YEAR(CURRENT_DATE) AND idLabor = {$idLabor}
                            GROUP BY mes
                        ) AS valores ON meses.mes = valores.mes AND idLabor = labores.`id`
                    )
                ) AS valores
        ) AS valores";
        $sql_hacienda_week = "SELECT semana AS id, AVG(promedio) AS label FROM muestras_anual WHERE YEAR(fecha) = YEAR(CURRENT_DATE) GROUP BY semana";
        $sql_zones_month = "SELECT CASE 
                WHEN mes = 0 THEN 'Ene'
                WHEN mes = 1 THEN 'Feb'
                WHEN mes = 2 THEN 'Mar'
                WHEN mes = 3 THEN 'Abr'
                WHEN mes = 4 THEN 'May'
                WHEN mes = 5 THEN 'Jun'
                WHEN mes = 6 THEN 'Jul'
                WHEN mes = 7 THEN 'Ago'
                WHEN mes = 8 THEN 'Sep'
                WHEN mes = 9 THEN 'Oct'
                WHEN mes = 10 THEN 'Nov'
                WHEN mes = 11 THEN 'Dic'
            END
         AS id,mes,(SELECT nombre from fincas where id = idFinca) AS finca,idZona ,idFinca, idLabor FROM muestras_anual
                  WHERE idLabor = '{$idZonaLabor}' AND idZona = '{$idFincaLabor}' AND idFinca = '{$idLoteLabor}' AND YEAR(fecha) = YEAR(CURRENT_DATE)
            GROUP BY idFinca , mes,idLabor";
        $sql_zones_week = "SELECT semana AS label,(SELECT nombre from fincas where id = idFinca) AS zona,idZona,idFinca,idLabor  FROM muestras_anual 
        WHERE idLabor = '{$idZonaLabor}' AND idZona = '{$idFincaLabor}' AND idFinca = '{$idLoteLabor}' AND YEAR(fecha) = YEAR(CURRENT_DATE)
        GROUP BY idFinca , semana , idLabor";


        $response->week = $this->db->queryAllSpecial($sql_hacienda_week);
        $response->main = $this->db->queryAllSpecial($sql_hacienda_month);
        $response->categories = new stdClass;
        $response->categories->months = $months_des;
        $response->categories->weeks = $weeks;
        $response->zonas_main = new stdClass;
        $response->zonas_main->zonas = [0 => $this->session->alias] + 
            $this->db->queryAllSpecial("SELECT nombre AS id , nombre AS label FROM labores WHERE id = '{$idZonaLabor}'");
        $months_zones = $this->db->queryAll($sql_zones_month);
        $weeks_zones = $this->db->queryAll($sql_zones_week);
        $response->zonas_main->auditoria = $this->db->queryAll($sql_zones_month);
        $response->labor_name = $this->db->queryOne("SELECT nombre FROM labores WHERE id = '{$idZonaLabor}'");
        $response->auditoria = [];

        foreach ($months_zones as $key => $value) {
            $response->zonas_main->months[$value->finca][$value->id] = $this->getPromFincaMonth($value->idZona , $value->idLabor , $value->mes , $value->idFinca)->promedio;
        }

        foreach ($weeks_zones as $key => $value) {
            $response->zonas_main->weeks[$value->finca][$value->label] = $this->getPromFincaWeek($value->idZona  , $value->idLabor, $value->label , $value->idFinca)->promedio;
        }

        foreach ($mes_labor as $key => $value) {
            $response->zonas_main->mes[$response->labor_name][$value->mes] = $value->promedio;
        }

        foreach ($semana_labor as $key => $value) {
            $response->zonas_main->semana[$response->labor_name][$value->semana] = $value->promedio;
        }

        $response->id_company = $this->session->id_company;
        $response->umbrals = $this->session->umbrals;

        return $response;
    }

    private function getPromZonaMonth($idZona ,$idLabor, $month){
        $response->promedio = 0;
        $sql = "SELECT idLabor , idZona , zona AS nombre,zona AS label, TRUNCATE(AVG(promedio) , 2) AS promedio, TRUNCATE(AVG(promedio) , 2) AS value,tipo_labor, labor FROM (SELECT       
                    CASE 
                        WHEN mes = 0 THEN 'Ene'
                        WHEN mes = 1 THEN 'Feb'
                        WHEN mes = 2 THEN 'Mar'
                        WHEN mes = 3 THEN 'Abr'
                        WHEN mes = 4 THEN 'May'
                        WHEN mes = 5 THEN 'Jun'
                        WHEN mes = 6 THEN 'Jul'
                        WHEN mes = 7 THEN 'Ago'
                        WHEN mes = 8 THEN 'Sep'
                        WHEN mes = 9 THEN 'Oct'
                        WHEN mes = 10 THEN 'Nov'
                        WHEN mes = 11 THEN 'Dic'
                    END
                    AS mes,
                    (SELECT nombre from zonas where id = idZona) AS zona,idZona ,
        (SELECT nombre from fincas where id = idFinca) AS finca,idFinca ,
        (SELECT nombre from lotes where id = idlote) AS lote,idLote ,
        (SELECT nombre from labores where id = idLabor) AS labor,idLabor ,
        (SELECT nombre from tipo_labor where id = idTipoLabor) AS tipo_labor,idTipoLabor ,
        AVG(promedio) AS promedio FROM muestras_anual
         WHERE  mes = {$month} AND idZona = '{$idZona}' AND idLabor = '{$idLabor}' AND YEAR(fecha) = YEAR(CURRENT_DATE)
        GROUP BY idZona , idFinca , idLote ,idTipoLabor,idLabor , mes) AS labor
        GROUP BY idZona
        ORDER BY tipo_labor";
        $response->promedio = $this->db->queryRow($sql);

        return $response->promedio;
    }
    
    private function getPromZonaWeek($idZona ,$idLabor, $week){
        $response->promedio = 0;
        $sql = "SELECT zona , AVG(promedio) AS promedio FROM(SELECT zona ,finca , AVG(prom) AS promedio FROM(SELECT zona ,finca , lote , AVG(promedio) AS prom FROM
        (SELECT       
                    CASE 
                        WHEN mes = 0 THEN 'Ene'
                        WHEN mes = 1 THEN 'Feb'
                        WHEN mes = 2 THEN 'Mar'
                        WHEN mes = 3 THEN 'Abr'
                        WHEN mes = 4 THEN 'May'
                        WHEN mes = 5 THEN 'Jun'
                        WHEN mes = 6 THEN 'Jul'
                        WHEN mes = 7 THEN 'Ago'
                        WHEN mes = 8 THEN 'Sep'
                        WHEN mes = 9 THEN 'Oct'
                        WHEN mes = 10 THEN 'Nov'
                        WHEN mes = 11 THEN 'Dic'
                    END
                    AS mes,
                    (SELECT nombre from zonas where id = idZona) AS zona,
        (SELECT nombre from fincas where id = idFinca) AS finca,
        (SELECT nombre from lotes where id = idlote) AS lote,
        (SELECT nombre from labores where id = idLabor) AS labor,
        AVG(promedio) AS promedio FROM muestras_anual
         WHERE idZona = '{$idZona}' AND semana = '{$week}' AND idLabor = '{$idLabor}' AND YEAR(fecha) = YEAR(CURRENT_DATE)
        GROUP BY idZona , idFinca , idLote , idLabor , mes) AS lote
        GROUP BY finca , lote) AS finca
        GROUP BY finca) AS zona
        GROUP BY zona";
        $response->promedio = $this->db->queryRow($sql);

        return $response->promedio;
    }

    private function getPromFincaMonth($idZona ,$idLabor, $month , $idFinca){
        $response->promedio = 0;
        $sql = "SELECT idLabor , idZona , zona AS nombre,zona AS label, TRUNCATE(AVG(promedio) , 2) AS promedio, TRUNCATE(AVG(promedio) , 2) AS value,tipo_labor, labor FROM (SELECT       
                    CASE 
                        WHEN mes = 0 THEN 'Ene'
                        WHEN mes = 1 THEN 'Feb'
                        WHEN mes = 2 THEN 'Mar'
                        WHEN mes = 3 THEN 'Abr'
                        WHEN mes = 4 THEN 'May'
                        WHEN mes = 5 THEN 'Jun'
                        WHEN mes = 6 THEN 'Jul'
                        WHEN mes = 7 THEN 'Ago'
                        WHEN mes = 8 THEN 'Sep'
                        WHEN mes = 9 THEN 'Oct'
                        WHEN mes = 10 THEN 'Nov'
                        WHEN mes = 11 THEN 'Dic'
                    END
                    AS mes,
                    (SELECT nombre from zonas where id = idZona) AS zona,idZona ,
        (SELECT nombre from fincas where id = idFinca) AS finca,idFinca ,
        (SELECT nombre from lotes where id = idlote) AS lote,idLote ,
        (SELECT nombre from labores where id = idLabor) AS labor,idLabor ,
        (SELECT nombre from tipo_labor where id = idTipoLabor) AS tipo_labor,idTipoLabor ,
        AVG(promedio) AS promedio FROM muestras_anual
         WHERE  mes = {$month} AND idZona = '{$idZona}' AND idLabor = '{$idLabor}' AND idFinca = '{$idFinca}' AND YEAR(fecha) = YEAR(CURRENT_DATE)
        GROUP BY idZona , idFinca , idLote ,idTipoLabor,idLabor , mes) AS labor
        GROUP BY idZona
        ORDER BY tipo_labor";
        $response->promedio = $this->db->queryRow($sql);

        return $response->promedio;
    }
    
    private function getPromFincaWeek($idZona ,$idLabor, $week , $idFinca){
        $response->promedio = 0;
        $sql = "SELECT zona , AVG(promedio) AS promedio FROM(SELECT zona ,finca , AVG(prom) AS promedio FROM(SELECT zona ,finca , lote , AVG(promedio) AS prom FROM
        (SELECT       
                    CASE 
                        WHEN mes = 0 THEN 'Ene'
                        WHEN mes = 1 THEN 'Feb'
                        WHEN mes = 2 THEN 'Mar'
                        WHEN mes = 3 THEN 'Abr'
                        WHEN mes = 4 THEN 'May'
                        WHEN mes = 5 THEN 'Jun'
                        WHEN mes = 6 THEN 'Jul'
                        WHEN mes = 7 THEN 'Ago'
                        WHEN mes = 8 THEN 'Sep'
                        WHEN mes = 9 THEN 'Oct'
                        WHEN mes = 10 THEN 'Nov'
                        WHEN mes = 11 THEN 'Dic'
                    END
                    AS mes,
                    (SELECT nombre from zonas where id = idZona) AS zona,
        (SELECT nombre from fincas where id = idFinca) AS finca,
        (SELECT nombre from lotes where id = idlote) AS lote,
        (SELECT nombre from labores where id = idLabor) AS labor,
        AVG(promedio) AS promedio FROM muestras_anual
         WHERE idZona = '{$idZona}' AND semana = '{$week}' AND idLabor = '{$idLabor}' AND idFinca = '{$idFinca}' AND YEAR(fecha) = YEAR(CURRENT_DATE)
        GROUP BY idZona , idFinca , idLote , idLabor , mes) AS lote
        GROUP BY finca , lote) AS finca
        GROUP BY finca) AS zona
        GROUP BY zona";
        $response->promedio = $this->db->queryRow($sql);

        return $response->promedio;
    }

}