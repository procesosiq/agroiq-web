<?php defined('PHRAPI') or die("Direct access not allowed!");

class Bonificacion {
	public $name;
	private $db;
	private $config;
	private $companies;
	private $string;

	public function __construct(){
		$this->config = $GLOBALS['config'];
		$this->session = Session::getInstance();
		// D($this->session);
        $this->maindb = DB::getInstance();
        $this->db = DB::getInstance($this->session->agent_user);
		$this->companies = [2,7];
		$this->string = "Total Peso";
        
	}

	public function index(){
		$postdata = (object)json_decode(file_get_contents("php://input"));

		$filters = (object)[
			"idFinca" => getValueFrom($postdata , "idFinca" , 1 , FILTER_SANITIZE_PHRAPI_INT),
			"mode" => getValueFrom($postdata , "mode" , 1 , FILTER_SANITIZE_STRING),
			"semana" => getValueFrom($postdata , "semana" , 48 , FILTER_SANITIZE_PHRAPI_INT),
			"year" => getValueFrom($postdata , "year" , 2017 , FILTER_SANITIZE_PHRAPI_INT),
			"type" => getValueFrom($postdata , "type" , "ENFUNDE" , FILTER_SANITIZE_STRING),
			"lote" => getValueFrom($postdata , "lote" , "" , FILTER_SANITIZE_STRING),
			"palanca" => getValueFrom($postdata , "palanca" , "" , FILTER_SANITIZE_STRING),
		];

		$response = new stdClass;
		$response->fincas = [];
		$response->showFincas = 0;
		$semana = $filters->semana;
		$sGroup = "DAY(fecha)";
		$sCampo = "IFNULL(UPPER(TRIM(palanca)),'S/NOMBRE')";

		// 09/05/2017 - TAG: CONST CONFIG
		$umbral = 10;
		$valor_descuento = 1.8;
		$bono = 0;
		$sql_config = "SELECT umbral , bono , descuento FROM bonificacion_conts WHERE tipo = '{$filters->type}' AND mode = '{$filters->mode}'";
		$_config = $this->db->queryRow($sql_config);
		$umbral = (double)$_config->umbral;
		$valor_descuento = (double)$_config->descuento;
		$bono = (double)$_config->bono;
		// 09/05/2017 - TAG: CONST CONFIG

		$cantidad = "SUM(cantidad)";
		$sWhere = " AND id_finca = '{$filters->idFinca}'";
		$sWhere_week = "";

		if($filters->idFinca == 9999999){
			$sWhere = " AND id_finca IN(1,2)";
		} 
		$response->showFincas = 1;
		$response->fincas = $this->getFincas($response->semanas);

		if($filters->type == "LOTERO AEREO" || $filters->type == "DESHOJE"){
			$sCampo = "bloque";
			if($filters->lote != ""){
				$sWhere .= " AND bloque = '{$filters->lote}'";
			}
		}
		if($filters->type == "COSECHA"){
			if($filters->palanca != ""){
				$sWhere .= " AND REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE(  
					REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE(  
					REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE(  
					UPPER(TRIM(palanca)),
					'ù','u'),'ú','u'),'û','u'),'ü','u'),'ý','y'),'ë','e'),'à','a'),'á','a'),'â','a'),'ã','a'), 
					'ä','a'),'å','a'),'æ','a'),'ç','c'),'è','e'),'é','e'),'ê','e'),'ë','e'),'ì','i'),'í','i'), 
					'î','i'),'ï','i'),'ð','o'),'ñ','n'),'ò','o'),'ó','o'),'Ó','O'),'õ','o'),'ö','o'),'ø','o') = '{$filters->palanca}'";
			}
		}

		$cantidad = "(cantidad)";
		$peso_neto = "(total_peso_merma - tallo)";
		if($filters->mode == 2){
			$modeCampo = "((SUM(cantidad) / SUM(racimos_procesados)) * 5)";
		}elseif($filters->mode == 1){
			$modeCampo = "ROUND(AVG(Total),2)";
		}elseif($filters->mode == 0){
			$modeCampo = "AVG(cantidad / racimos_procesados)";
		}

		$aditional_select = "(SELECT IFNULL(umbral, 0) FROM bonificacion_conts WHERE UPPER(tipo) = '{$filters->type}' AND mode = {$filters->mode}) AS umbral,";

				$sql = "SELECT 
				DATE(fecha) AS fecha ,{$sCampo} AS lote ,
				CASE  
					WHEN DAYNAME(fecha) = 'Monday' THEN 'Lun'
					WHEN DAYNAME(fecha) = 'Tuesday' THEN 'Mar'
					WHEN DAYNAME(fecha) = 'Wednesday' THEN 'Mie'
					WHEN DAYNAME(fecha) = 'Thursday' THEN 'Jue'
					WHEN DAYNAME(fecha) = 'Friday' THEN 'Vie'
					WHEN DAYNAME(fecha) = 'Saturday' THEN 'Sab'
					WHEN DAYNAME(fecha) = 'Sunday' THEN 'Dom'
				END AS day_week,
				CASE 
	                WHEN MONTH(fecha) = 1 THEN 'Ene'
	                WHEN MONTH(fecha) = 2 THEN 'Feb'
	                WHEN MONTH(fecha) = 3 THEN 'Mar'
	                WHEN MONTH(fecha) = 4 THEN 'Abr'
	                WHEN MONTH(fecha) = 5 THEN 'May'
	                WHEN MONTH(fecha) = 6 THEN 'Jun'
	                WHEN MONTH(fecha) = 7 THEN 'Jul'
	                WHEN MONTH(fecha) = 8 THEN 'Ago'
	                WHEN MONTH(fecha) = 9 THEN 'Sep'
	                WHEN MONTH(fecha) = 10 THEN 'Oct'
	                WHEN MONTH(fecha) = 11 THEN 'Nov'
	                WHEN MONTH(fecha) = 12 THEN 'Dic'
	            END AS mes ,DAY(fecha) AS dia,
				@idLote := (SELECT id FROM lotes WHERE nombre = m.bloque AND idFinca = id_finca) AS idLote,
				@idLabor := (SELECT id FROM labores WHERE nombre = m.type) AS idLabor,
				{$aditional_select}
				IFNULL((SELECT nombre FROM personal WHERE id = (SELECT idPersonal FROM lotelabor_personal WHERE idLote = @idLote AND idLabor = @idLabor)),'POR CONFIRMAR') AS enfundador,
				{$modeCampo} AS cantidad ,
				@valor := IF(({$cantidad} - 10) > 0 , (1 * ({$cantidad} - 10)),0 ) AS valor
			FROM (
				SELECT fecha  , palanca , id_finca , 
				bloque,merma_detalle.id_merma , type ,campo , {$cantidad} AS cantidad , {$peso_neto} AS peso_neto , ({$cantidad} / {$peso_neto}) * 100 as Total ,
				porcentaje_merma,racimos_procesados  FROM merma
				INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
				WHERE  WEEK(fecha) = {$semana} AND YEAR(fecha) = {$filters->year}  AND type = '{$filters->type}'  AND campo like '%{$this->string}%'  {$sWhere} AND bloque != 127 AND bloque != 0 
				AND type NOT IN ('RESULTADOS','FIRMA ELECTRONICA','OTROS' ,'RECUSADOS')
				GROUP BY id_merma  
			) as m
			GROUP BY {$sGroup},{$sCampo}
			ORDER BY fecha , {$sCampo}";
			// D($sql);
		$data = $this->db->queryAll($sql);
		$response->data = [];
		$response->dias = [];
		$response->titulos = [];
		$response->totales = [];
		$exedente = 0;
		$count = [];
		$position = 0;
		$avg = [];
		$promedio = [];
		foreach ($data as $key => $value) {
			$response->dias[$value->fecha] = $value->fecha;
			$response->titulos[$value->fecha] = "{$value->day_week}, {$value->dia} {$value->mes}";
			/*----------  DATA  ----------*/
			if($filters->type == "COSECHA"){
				if(!in_array($value->lote, $count)){
					$count[] = $value->lote;
					$position++;
					$response->data[$value->lote]["lote"] = (double)$position;
				}
				$response->data[$value->lote]["enfundador"] = $value->lote;
			}else{
				$response->data[$value->lote]["lote"] = $value->lote;
				$response->data[$value->lote]["enfundador"] = $value->enfundador;
			}
			$response->data[$value->lote]["expanded"] = false;
			$response->data[$value->lote]["umbral"] = $value->umbral;
			$response->data[$value->lote]["hectareas"] = $value->hectareas;
			$response->data[$value->lote]["cantidad"][$value->fecha] = (double)$value->cantidad;

			$value->cantidad = (double)$value->cantidad;

			$response->data[$value->lote]["defectos"] = $this->getDefectos($value->lote , $semana , $filters->type , $filters->idFinca , $filters->mode ,'days' ,$filters);


			// $promedio[$value->lote] = $response->data[$value->lote]["total"];

			if($value->cantidad > 0){
				// $avg[$value->lote]["count"]++;
				// $avg[$value->lote]["total"] += $value->cantidad;
				// $promedio[$value->lote] = (double)($avg[$value->lote]["total"] / $avg[$value->lote]["count"]);
				$avg[$value->fecha]["count"]++;
				$avg[$value->fecha]["total"] += $value->cantidad;
				
				$promedio[$value->fecha] = (double)($avg[$value->fecha]["total"] / $avg[$value->fecha]["count"]);

				if($filters->mode == 2 && $this->session->id_company == 2){
					$response->totales["total"]["cantidad"][$value->fecha] += (double)$value->cantidad;
					/* TAG - 15/05/2017 promedio por dia */
					$count_dia[$value->fecha][] = $value->cantidad;
				}else{
					$response->totales["total"]["cantidad"][$value->fecha] = $promedio[$value->fecha];
				}
			}

			if($filters->mode == 2){

				$aditional_select = "(SELECT IFNULL(umbral, 0) FROM bonificacion_conts WHERE UPPER(tipo) = '{$filters->type}' AND mode = {$filters->mode}) AS umbral,";

				if($filters->type == "COSECHA"){
					$sWhere_week = " AND palanca = '{$value->lote}'";
				}else{
					$sWhere_week = " AND bloque = '{$value->lote}'";
				}
				
				// $response->data[$value->lote]["total"] += (double)$value->cantidad;
				/* TAG - 15/05/2017  */
				$sql_week = "SELECT 
							DATE(fecha) AS fecha ,{$sCampo} AS lote ,
							CASE  
								WHEN DAYNAME(fecha) = 'Monday' THEN 'Lun'
								WHEN DAYNAME(fecha) = 'Tuesday' THEN 'Mar'
								WHEN DAYNAME(fecha) = 'Wednesday' THEN 'Mie'
								WHEN DAYNAME(fecha) = 'Thursday' THEN 'Jue'
								WHEN DAYNAME(fecha) = 'Friday' THEN 'Vie'
								WHEN DAYNAME(fecha) = 'Saturday' THEN 'Sab'
								WHEN DAYNAME(fecha) = 'Sunday' THEN 'Dom'
							END AS day_week,
							CASE 
								WHEN MONTH(fecha) = 1 THEN 'Ene'
								WHEN MONTH(fecha) = 2 THEN 'Feb'
								WHEN MONTH(fecha) = 3 THEN 'Mar'
								WHEN MONTH(fecha) = 4 THEN 'Abr'
								WHEN MONTH(fecha) = 5 THEN 'May'
								WHEN MONTH(fecha) = 6 THEN 'Jun'
								WHEN MONTH(fecha) = 7 THEN 'Jul'
								WHEN MONTH(fecha) = 8 THEN 'Ago'
								WHEN MONTH(fecha) = 9 THEN 'Sep'
								WHEN MONTH(fecha) = 10 THEN 'Oct'
								WHEN MONTH(fecha) = 11 THEN 'Nov'
								WHEN MONTH(fecha) = 12 THEN 'Dic'
							END AS mes ,DAY(fecha) AS dia,
							@idLote := (SELECT id FROM lotes WHERE nombre = m.bloque AND idFinca = id_finca) AS idLote,
							@idLabor := (SELECT id FROM labores WHERE nombre = m.type) AS idLabor,
							{$aditional_select}
							IFNULL((SELECT nombre FROM personal WHERE id = (SELECT idPersonal FROM lotelabor_personal WHERE idLote = @idLote AND idLabor = @idLabor)),'POR CONFIRMAR') AS enfundador,
							{$modeCampo} AS cantidad ,
							@valor := IF(({$cantidad} - 10) > 0 , (1 * ({$cantidad} - 10)),0 ) AS valor
						FROM (
							SELECT fecha  , palanca , id_finca , 
							bloque,merma_detalle.id_merma , type ,campo , {$cantidad} AS cantidad , {$peso_neto} AS peso_neto , ({$cantidad} / {$peso_neto}) * 100 as Total ,
							porcentaje_merma,racimos_procesados  FROM merma
							INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
							WHERE  WEEK(fecha) = {$semana} AND YEAR(fecha) = {$filters->year}  AND type = '{$filters->type}'  AND campo like '%{$this->string}%'  {$sWhere} AND bloque != 127 AND bloque != 0 
							AND type NOT IN ('RESULTADOS','FIRMA ELECTRONICA','OTROS' ,'RECUSADOS') {$sWhere_week}
							GROUP BY id_merma  
						) as m";
						// D($sql_week);
				$response->data[$value->lote]["total"] = $this->db->queryRow($sql_week)->cantidad;
			}else{
				// D("Hola");
				$response->data[$value->lote]["total"] = $this->getAvgLote($filters , $value->lote);
			}

			$exedente = (double)($response->data[$value->lote]["total"] - $value->umbral);

			$response->data[$value->lote]["exedente"] = 0;
			#(lote.total > (lote.umbral || 0)) ? (lote.total || 0) - (lote.umbral || 0) : 0
			if($exedente > 0){
				$response->data[$value->lote]["exedente"] = (double)($response->data[$value->lote]["total"] - $value->umbral);
			}
			$response->data[$value->lote]["usd"] = 0;
			if($response->data[$value->lote]["exedente"] > 0){
				$response->data[$value->lote]["usd"] = "-" . (double)$response->data[$value->lote]["exedente"] * $valor_descuento;
			}else{
				$response->data[$value->lote]["usd"] = (double)$bono;
			}
			/*----------  DATA  ----------*/

			/*----------  TOTALES  ----------*/
			$response->totales["total"]["exedente"] += $response->data[$value->lote]["exedente"];
			$response->totales["total"]["usd"] += $response->data[$value->lote]["usd"];
			/*----------  TOTALES  ----------*/
		}

		/* TAG - 15/05/2017 promedio por dia */
		/*foreach($response->totales["total"]["cantidad"] as $key => $value){
			$value /= count($count_dia[$key]);
		}*/
		foreach($count_dia as $key => $value){
			$response->totales["total"]["cantidad"][$key] = array_sum($value) / count($value);
		}

		if($filters->mode == 2){ #sum de los dedos
			$response->totales["total"]["defectos"] = array_sum($response->totales["total"]["cantidad"]);
		}else{
			$cantidad = count($response->totales["total"]["cantidad"]);
			$suma = array_sum($response->totales["total"]["cantidad"]);
			$response->totales["total"]["defectos"] = (double)($suma / $cantidad);
		}

		$response->colspan = 7 + count($response->dias);
		$response->personal = $this->db->queryAllSpecial("SELECT merma.id as id, REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE(  
			REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE(  
			REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE(  
			UPPER(TRIM(palanca)),
			'ù','u'),'ú','u'),'û','u'),'ü','u'),'ý','y'),'ë','e'),'à','a'),'á','a'),'â','a'),'ã','a'), 
			'ä','a'),'å','a'),'æ','a'),'ç','c'),'è','e'),'é','e'),'ê','e'),'ë','e'),'ì','i'),'í','i'), 
			'î','i'),'ï','i'),'ð','o'),'ñ','n'),'ò','o'),'ó','o'),'Ó','O'),'õ','o'),'ö','o'),'ø','o') AS label
			FROM merma
			WHERE UPPER(TRIM(palanca)) != '' AND palanca IS NOT NULL
			GROUP BY UPPER(TRIM(palanca))
			ORDER BY label");
		$response->semanas = $this->db->queryAllSpecial("SELECT WEEK(fecha) AS id , WEEK(fecha) AS label FROM merma WHERE WEEK(fecha) > 0 AND YEAR(fecha) = {$filters->year} GROUP BY WEEK(fecha)");

		return $response;

	}


	private function getAvgLote($filters = [] , $bloque){
		if($filters->type == "COSECHA"){
			$sWhere = "AND palanca = '{$bloque}'";
		}else{
			$sWhere = " AND bloque = '{$bloque}'";
		}

		if($filters->mode == 2){
			$modeCampo = "AVG(cantidad / racimos_procesados) * 5";
			if($this->session->id_company == 7){
				$modeCampo = "((SUM(cantidad) / SUM(racimos_procesados)) * 5)";
				$this->string = "Total Peso";
			}
			if($this->session->id_company == 2){
				$this->string = "Total Daños";
				$modeCampo = "SUM(cantidad)";
			}
		}elseif($filters->mode == 1){
			$modeCampo = "ROUND(AVG(Total),2)";
		}elseif($filters->mode == 0){
			$modeCampo = "AVG(cantidad / racimos_procesados)";
		}


		$sql = "SELECT {$modeCampo} AS promedioLote
				FROM 
				(
					SELECT 
						((cantidad / (racimo - tallo)) * 100) as Total, cantidad ,racimos_procesados
					FROM merma
					INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
					WHERE  WEEK(fecha) = $filters->semana 
					AND YEAR(fecha) = {$filters->year} 
					AND type = '{$filters->type}' 
					AND bloque != 127 
					AND bloque != 0 
					AND campo like '%{$this->string}%'
					{$sWhere}
					AND id_finca = '{$filters->idFinca}'
					AND type NOT IN ('RESULTADOS','FIRMA ELECTRONICA','OTROS' ,'RECUSADOS')  
				) as detalle";
		D($sql);
		$response = 0;
		$response = (double)$this->db->queryOne($sql);
		return $response;
	}

	private function getFincas($filters = []){
		$response = new stdClass;
		$sql = "SELECT id_finca AS id , finca AS label  FROM merma
				WHERE finca != ''
				GROUP BY id_finca";
		$response->data = [];
		if($this->session->id_company == 7){
			$response->data = ["9999999" => "San José"] + $this->db->queryAllSpecial($sql);
		}else{
			$response->data = $this->db->queryAllSpecial($sql);
		}
		return $response->data;
	}

	public function getWeekYear(){
		$response = new stdClass;
		$postdata = (object)json_decode(file_get_contents("php://input"));
		// F($postdata);

		$filters = (object)[
			"type" => getValueFrom($postdata , "type" , "ENFUNDE" , FILTER_SANITIZE_STRING),
			"mode" => getValueFrom($postdata , "mode" , 1 , FILTER_SANITIZE_STRING),
			"idFinca" => getValueFrom($postdata , "idFinca" , 1 , FILTER_SANITIZE_PHRAPI_INT),
			"year" => getValueFrom($postdata , "year" , 2017 , FILTER_SANITIZE_PHRAPI_INT),
			"lote" => getValueFrom($postdata , "lote" , "" , FILTER_SANITIZE_STRING),
			"palanca" => getValueFrom($postdata , "palanca" , "" , FILTER_SANITIZE_STRING),
		];


		// 09/05/2017 - TAG: CONST CONFIG
		$umbral = 10;
		$valor_descuento = 1.8;
		$bono = 0;
		if($this->session->id_company == 2){
			$sql_config = "SELECT umbral , bono , descuento FROM bonificacion_conts WHERE tipo = '{$filters->type}' AND mode = '{$filters->mode}'";
			$_config = $this->db->queryRow($sql_config);
			$umbral = (double)$_config->umbral;
			$valor_descuento = (double)$_config->descuento;
			$bono = (double)$_config->bono;
		}
		if($this->session->id_company == 7){
			$sql_config = "SELECT umbral , bono , descuento FROM bonificacion_conts WHERE tipo = '{$filters->type}' AND mode = '{$filters->mode}'";
			$_config = $this->db->queryRow($sql_config);
			$umbral = (double)$_config->umbral;
			$valor_descuento = (double)$_config->descuento;
			$bono = (double)$_config->bono;
		}
		// 09/05/2017 - TAG: CONST CONFIG


		$sFinca = " AND id_finca = '{$filters->idFinca}'";
		if($this->session->id_company == 7){
			if($filters->type == "ENFUNDE"){
				$filters->type = "LOTERO AEREO";
			}
			if($filters->idFinca == 9999999){
				$sFinca = " AND id_finca IN(1,2)";
			}
		}

		if($filters->lote != "" && ($filters->type == "ENFUNDE" || $filters->type == "LOTERO AEREO" || $filters->type == "DESHOJE")){
			$sFinca .= " AND bloque = '{$filters->lote}'";
		}
		if($filters->type == "COSECHA"){
			if($filters->palanca != ""){
				$sFinca .= " AND REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE(  
					REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE(  
					REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE(  
					UPPER(TRIM(palanca)),
					'ù','u'),'ú','u'),'û','u'),'ü','u'),'ý','y'),'ë','e'),'à','a'),'á','a'),'â','a'),'ã','a'), 
					'ä','a'),'å','a'),'æ','a'),'ç','c'),'è','e'),'é','e'),'ê','e'),'ë','e'),'ì','i'),'í','i'), 
					'î','i'),'ï','i'),'ð','o'),'ñ','n'),'ò','o'),'ó','o'),'Ó','O'),'õ','o'),'ö','o'),'ø','o') = '{$filters->palanca}'";
			}
		}

		$peso_neto = "peso_neto";
		$cantidad = "(cantidad)";
		if($this->session->id_company == 2){
			/*----------  CAMBIO DE PESO A LIBRA MARCEL  ----------*/
			if($this->MermaTypePeso == "Kg"){
				$cantidad = "(cantidad / 2.2)";
			}else{
				$cantidad = "(cantidad)";
			}
			/*----------  CAMBIO DE PESO A LIBRA MARCEL  ----------*/
		}elseif($this->session->id_company == 7){
			$peso_neto = "(racimo - tallo)";
			
		}else{
			$cantidad = "(cantidad)";
		}

		if($filters->type == "ENFUNDE" || $filters->type == "LOTERO AEREO" || $filters->type == "DESHOJE"){
			/*
			SELECT semana AS label , 'UMBRAL' AS legend ,
					true AS selected,
					{$umbral} AS cantidad
					FROM (SELECT WEEK(fecha) AS semana FROM merma WHERE WEEK(fecha) > 0 AND YEAR(fecha) = {$filters->year} GROUP BY WEEK(fecha) ORDER BY WEEK(fecha)) AS semanas , merma AS m
					WHERE YEAR(fecha) = YEAR(CURRENT_DATE)
					AND bloque != 127 AND bloque != 0
					GROUP BY semana
					UNION ALL
			*/
			$sql = "SELECT semanas.semana AS label , bloque AS legend ,
					IF(bloque != 127 ,true,false) AS selected,
					0 AS cantidad 
					FROM (SELECT WEEK(fecha) AS semana FROM merma WHERE WEEK(fecha) > 0 AND YEAR(fecha) = {$filters->year} GROUP BY WEEK(fecha) ORDER BY WEEK(fecha)) AS semanas , merma AS m
					WHERE YEAR(fecha) = YEAR(CURRENT_DATE) {$sFinca}
					AND bloque != 127 AND bloque != 0
					GROUP BY bloque , semanas.semana";
		}
		if($filters->type == "COSECHA"){
			/* 
			SELECT semana AS label , 'UMBRAL' AS legend ,
					true AS selected,
					{$umbral} AS cantidad
					FROM (SELECT WEEK(fecha) AS semana FROM merma WHERE WEEK(fecha) > 0 AND YEAR(fecha) = {$filters->year} GROUP BY WEEK(fecha) ORDER BY WEEK(fecha)) AS semanas , merma AS m
					WHERE YEAR(fecha) = YEAR(CURRENT_DATE)
					AND bloque != 127 AND bloque != 0
					GROUP BY semana
					UNION ALL
			*/
			$sql = "SELECT semanas.semana AS label , IFNULL(UPPER(TRIM(palanca)),'S/NOMBRE') AS legend ,
					IF(bloque != 127 ,true,false) AS selected,
					0 AS cantidad 
					FROM (SELECT WEEK(fecha) AS semana FROM merma WHERE WEEK(fecha) > 0 AND YEAR(fecha) = {$filters->year} GROUP BY WEEK(fecha) ORDER BY WEEK(fecha)) AS semanas , merma AS m
					WHERE YEAR(fecha) = YEAR(CURRENT_DATE) {$sFinca}
					AND bloque != 127 AND TRIM(palanca) != '' AND bloque != 0
					GROUP BY TRIM(palanca) , semanas.semana";
		}
		$variable = $this->db->queryAll($sql);
		$data = [];
		$valor = 0;
		$count = [];
		$sum = [];
		$response->historico_tabla = [];
		$response->totales_historico = [];

		if($filters->mode == 0){
			$modeCampo = "SUM(cantidad / racimos_procesados)";
			if($this->session->id_company == 7){
				$this->string = "Total Peso";
			}
			if($this->session->id_company == 2){
				$this->string = "Total Peso";
			}
		}elseif($filters->mode == 1){
			$modeCampo = "ROUND(SUM(Total),2)";
		}elseif($filters->mode == 2){
			$modeCampo = "SUM(cantidad / racimos_procesados) * 5";
			if($this->session->id_company == 7){
				$this->string = "Total Peso";
				$modeCampo = "((SUM(cantidad) / SUM(racimos_procesados)) * 5)";
			}
			if($this->session->id_company == 2){
				$this->string = "Total Daños";
				/* TAG - 15/05/2017 - promedio por dia */
				#$modeCampo = "SUM(cantidad) AS cantidad";
				$modeCampo = "AVG(cantidad) AS cantidad";
			}
		}
		foreach ($variable as $key => $value) {
			$valor = (double)$value->cantidad;
			if($value->legend != 'UMBRAL') {
				if($filters->type == "COSECHA"){
					$sWhere = "AND palanca = '{$value->legend}'";
					if($value->legend == 'S/NOMBRE'){
						$sWhere = "AND palanca IS NULL";
					}	
					$sql ="SELECT $modeCampo FROM (
						SELECT fecha  , bloque,merma_detalle.id_merma , type ,campo , ({$cantidad}) AS cantidad , {$peso_neto} , (({$cantidad}) / {$peso_neto}) * 100 as Total , porcentaje_merma ,
						racimos_procesados 
						FROM merma
						INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
						WHERE  WEEK(fecha) = {$value->label} AND YEAR(fecha) = {$filters->year} AND type = 'COSECHA'  AND campo like '%$this->string%' {$sWhere} {$sFinca}
						AND bloque != 127 
						AND bloque != 0 
						AND type NOT IN ('RESULTADOS','FIRMA ELECTRONICA','OTROS' ,'RECUSADOS')  ) as detalle";
						// AND cantidad > 0
					// D($sql);
					$valor = (double)$this->db->queryOne($sql);
				}
				if($filters->type == "ENFUNDE" || $filters->type == "LOTERO AEREO" || $filters->type == "DESHOJE"){
					$sql ="SELECT $modeCampo FROM (
						SELECT fecha  , bloque,merma_detalle.id_merma , type ,campo , ({$cantidad}) AS cantidad , {$peso_neto} , (({$cantidad}) / {$peso_neto}) * 100 as Total , porcentaje_merma ,
						racimos_procesados
						FROM merma
						INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
						WHERE  WEEK(fecha) = {$value->label} AND YEAR(fecha) = {$filters->year} AND type = '{$filters->type}'  AND campo like '%$this->string%' AND bloque = '{$value->legend}'  {$sFinca}
						AND bloque != 127 
						AND bloque != 0 
						AND type NOT IN ('RESULTADOS','FIRMA ELECTRONICA','OTROS' ,'RECUSADOS')  ) as detalle";
						// AND cantidad > 0
					// D($sql);
					$valor = (double)$this->db->queryOne($sql);
				}
			}

			$data[] = [
				"label" => (double)$value->label,
				"legend" => $value->legend,
				"selected" => $value->selected,
				"value" => $valor,
			];

			if($value->legend != "UMBRAL"){
				if(!array_key_exists($value->legend, $response->historico_tabla)){
					$response->historico_tabla[$value->legend]["details"] = $this->getDefectos($value->legend , $value->label , $filters->type , $filters->idFinca  , $filters->mode , "weeks" , $filters);
				}
				$response->historico_tabla[$value->legend]["lote"] = $value->legend;
				$response->historico_tabla[$value->legend]["expanded"] = false;
				$response->historico_tabla[$value->legend]["cantidad"][$value->label] = $valor;
				if($this->session->id_company == 2){
					$response->historico_tabla[$value->legend]["umbral"][$value->label] = $this->db->queryRow("SELECT IFNULL(umbral, 0) as umbral FROM bonificacion_config WHERE lote = '{$value->legend}' AND semana <= '{$value->label}' AND umbral > 0 ORDER BY semana DESC LIMIT 1")->umbral;
				}
				if($this->session->id_company == 7){
					$response->historico_tabla[$value->legend]["umbral"][$value->label] = (double)$umbral;
				}
				if($valor > 0){
					$count[$value->legend]++;
					$sum[$value->legend] += $valor;
					$response->historico_tabla[$value->legend]["total"] = ROUND(($sum[$value->legend] / $count[$value->legend]) , 2);
					if($this->session->id_company == 2){
						$response->historico_tabla[$value->legend]["umbral_total"] = $this->db->queryRow("SELECT IFNULL(umbral, 0) as umbral FROM bonificacion_config WHERE lote = '{$value->legend}' AND umbral > 0 ORDER BY semana DESC LIMIT 1")->umbral;
					}
					if($this->session->id_company == 7){
						$response->historico_tabla[$value->legend]["umbral_total"] = (double)$umbral;
					}
					// $response->historico_tabla[$value->legend]["total"] = ROUND(($sum[$value->legend] / $count[$value->legend]) , 2);
				}
				$response->totales_historico["cantidad"][$value->label]  += $valor;
				if($this->session->id_company == 2){
					$response->totales_historico["umbral"][$value->label] = $this->db->queryRow("SELECT IFNULL(umbral, 0) as umbral FROM bonificacion_config WHERE lote = '{$value->legend}' AND semana <= '{$value->label}' AND umbral > 0 ORDER BY semana DESC LIMIT 1")->umbral;
				}
				if($this->session->id_company == 7){
					$response->totales_historico["umbral"][$value->label] = (double)$umbral;
				}
				$response->totales_historico["total"]  += $valor;
			}
		}
		$minMax = [
			// "min" => 50,
			// "max" => 100,
		];
		// D($data);
		$response->historico = $this->chartInit($data,"vertical",$filters->type,"line",$minMax);
		$semanas_historico = (array)$this->db->queryAllSpecial("SELECT WEEK(fecha) AS id , WEEK(fecha) AS label FROM merma WHERE YEAR(fecha) = {$filters->year} GROUP BY WEEK(fecha)");
		
		foreach ($semanas_historico as $key => $value) {
			$response->semanas_historico[] = (double)$value;
		}
		// D($response->semanas_historico);
		return $response;
	}

	public function saveUmbrals(){
		$response = new stdClass;
		$postdata = (object)json_decode(file_get_contents("php://input"));
		
		$data = $postdata->umbrals;
		$hect = $postdata->hectareas;

		foreach($data as $lote => $umbral){
			if((float)$umbral > 0){
				$hectarea = isset($hect->{$lote}) ? $hect->{$lote} : 0;
				$umbral = (float) $umbral;

				$row = $this->db->queryRow("SELECT * FROM bonificacion_config WHERE lote = '{$lote}' AND anio = YEAR(CURRENT_DATE) AND semana = '{$postdata->semana}'");
				if(isset($row->umbral)){
					$this->db->query("UPDATE bonificacion_config SET umbral = '{$umbral}', hectareas = '{$hectarea}' WHERE lote = '{$lote}' AND anio = YEAR(CURRENT_DATE) AND semana = '{$postdata->semana}'");
				}else{
					$this->db->query("INSERT INTO bonificacion_config SET umbral = '{$umbral}', hectareas = '{$hectarea}', lote = '{$lote}', semana = '{$postdata->semana}', anio = YEAR(CURRENT_DATE)");
				}
			}
		}
		return true;
	}

	private function getDefectos($lote , $semana , $type = "ENFUNDE" , $idFinca = 1 , $mode = "peso" , $modePrint = "days" , $filters){
		$response = new stdClass;
		$sWhere = "AND bloque = '{$lote}'";
		$cantidad  = "SUM(cantidad)";
		$sFincas = " AND id_finca = '{$idFinca}'";
		$peso_neto = "peso_neto";
		$groupBy = "GROUP BY DAY(fecha),campo";
		$orderBy = "ORDER BY DAY(fecha),campo";
		$campo = "DATE(fecha)";
		$modeWhere = " WEEK(fecha) = {$semana}  AND";
		$conditions = "";
		$sum = [];
		$count = [];

		if($modePrint == "weeks"){	
			$campo = "WEEK(fecha)";
			$modeWhere = "";
			$groupBy = "GROUP BY WEEK(fecha),campo";
			$orderBy = "ORDER BY WEEK(fecha),campo";
		}

		if($this->session->id_company == 7){
			$peso_neto = "(racimo - tallo)";
			$cantidad = "SUM((cantidad / racimos_procesados))";
			if($type == "ENFUNDE"){
				$type = "LOTERO AEREO";
			}
			if($idFinca == 9999999){
				$sFincas = " AND id_finca IN(1,2)";
			}
		}

		if($type == "COSECHA"){
			$sWhere = "AND palanca = '$lote'";
			if($lote == "S/PALANCA"){
				$sWhere = "AND palanca IS NULL";
			}
		}

		$conditions = "";
		$tipoConsulta = "AND campo LIKE '%Peso%'";
		if($mode == 0){
			$cantidad = "SUM(cantidad)";
			$modeCampo = "SUM(cantidad / racimos_procesados)";
			if($this->session->id_company == 2){
				/*----------  CAMBIO DE PESO A LIBRA MARCEL  ----------*/
				if($this->MermaTypePeso == "Kg"){
					$cantidad = "SUM(cantidad / 2.2)";
				}else{
					$cantidad = "SUM(cantidad)";
				}
				/*----------  CAMBIO DE PESO A LIBRA MARCEL  ----------*/
			}elseif($this->session->id_company == 7){
				$peso_neto = "(racimo - tallo)";
				// $conditions = " AND campo LIKE '%(# Daños)%' AND campo NOT LIKE '%Total Merma%'";
			}else{
				$cantidad = "SUM(cantidad)";
			}
		}
		if($mode == 1){
			$cantidad = "SUM(cantidad)";
			$modeCampo = "SUM(Total)";
			if($this->session->id_company == 2){
				/*----------  CAMBIO DE PESO A LIBRA MARCEL  ----------*/
				if($this->MermaTypePeso == "Kg"){
					$cantidad = "SUM(cantidad / 2.2)";
				}else{
					$cantidad = "SUM(cantidad)";
				}
				/*----------  CAMBIO DE PESO A LIBRA MARCEL  ----------*/
			}elseif($this->session->id_company == 7){
				$peso_neto = "(racimo - tallo)";
				// $conditions = " AND campo LIKE '%(# Daños)%' AND campo NOT LIKE '%Total Merma%'";
			}else{
				$cantidad = "SUM(cantidad)";
			}
		}
		if($mode == 2){
			$modeCampo = "SUM(cantidad / racimos_procesados) * 5";
			$tipoConsulta = "AND campo NOT LIKE '%Peso%'";
			// $modeCampo = "SUM(cantidad)";
			$cantidad = "SUM(cantidad)";
			if($this->session->id_company == 2){
				/*----------  CAMBIO DE PESO A LIBRA MARCEL  ----------*/
				if($this->MermaTypePeso == "Kg"){
					$cantidad = "SUM(cantidad / 2.2)";
					$modeCampo = "SUM((cantidad / 2.2) / racimos_procesados) * 5";
				}else{
					$cantidad = "SUM(cantidad)";
				}
				$modeCampo = "SUM(cantidad)";
				/*----------  CAMBIO DE PESO A LIBRA MARCEL  ----------*/
			}elseif($this->session->id_company == 7){
				$peso_neto = "(racimo - tallo)";
				// $conditions = " AND campo like '%$this->string%'";
			}else{
				$cantidad = "SUM(cantidad)";
			}
		}

		// $sql = "SELECT 
		// 	DATE(fecha) AS fecha,
		// 	campo,
		// 	SUM(cantidad) AS cantidad
		// 	FROM merma AS m
		// 	INNER JOIN merma_detalle AS md ON m.id = md.id_merma
		// 	WHERE WEEK(fecha) = {$semana} {$sWhere}
		// 	AND type = '{$type}' AND bloque != 127 AND bloque != 0
		// 	AND campo NOT LIKE '%Peso%' 
		// 	AND campo NOT LIKE '%Total Peso%' 
		// 	AND campo NOT LIKE '%Total Daños%'
		// 	AND campo != 'Observaciones'
		// 	{$sFincas}
		// 	AND type NOT IN ('RESULTADOS','FIRMA ELECTRONICA','OTROS','RECUSADOS') 		
		// 	GROUP BY DAY(fecha),campo
		// 	HAVING cantidad > 0
		// 	ORDER BY fecha , campo";
		$sql_tmp = "SELECT 
			{$campo} AS fecha,
			campo,
			{$modeCampo} AS cantidad
			FROM (
			SELECT fecha  , palanca , id_finca , 
			bloque,merma_detalle.id_merma , type ,campo , ROUND({$cantidad},2) AS cantidad , $peso_neto AS peso_neto , ({$cantidad} / {$peso_neto}) * 100 as Total ,
			porcentaje_merma , racimos_procesados  FROM merma
			INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
			WHERE {$modeWhere} type = '{$type}'  {$sWhere} 
			{$tipoConsulta} 
			AND campo NOT LIKE '%Total Peso%' 
			AND campo NOT LIKE '%Total Daños%'
			AND campo != 'Observaciones' AND YEAR(fecha) = {$filters->year}
			{$sFincas} AND bloque != 127 AND bloque != 0 AND cantidad > 0
			AND type NOT IN ('RESULTADOS','FIRMA ELECTRONICA','OTROS' ,'RECUSADOS')
			{$conditions}
			GROUP BY id_merma,campo) AS detalle
			{$groupBy}
			{$orderBy}";
		if($mode == 2){
			$sql = "SELECT fecha  , campo ,ROUND(AVG(cantidad),2)  AS cantidad FROM ( 
				SELECT 
				{$campo} AS fecha,
				campo,
				{$modeCampo} AS cantidad
				FROM (
				SELECT fecha  , palanca , id_finca , 
				bloque,merma_detalle.id_merma , type ,campo , ROUND({$cantidad},2) AS cantidad , $peso_neto AS peso_neto , ({$cantidad} / {$peso_neto}) * 100 as Total ,
				porcentaje_merma , racimos_procesados  FROM merma
				INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
				WHERE {$modeWhere} type = '{$type}'  {$sWhere} 
				{$tipoConsulta} 
				AND campo NOT LIKE '%Total Peso%' 
				AND campo NOT LIKE '%Total Daños%'
				AND campo != 'Observaciones' AND YEAR(fecha) = {$filters->year}
				{$sFincas} AND bloque != 127 AND bloque != 0
				AND type NOT IN ('RESULTADOS','FIRMA ELECTRONICA','OTROS' ,'RECUSADOS')
				{$conditions}
				GROUP BY id_merma,campo) AS detalle
				GROUP BY id_merma,campo
			 ) AS dedos_promedio
			GROUP BY DAY(fecha) , campo 
			HAVING cantidad > 0";
		}else{
			$sql = $sql_tmp;
		}
		// D($sql);
		$data = $this->db->queryAll($sql);
		$response->data = [];
		foreach ($data as $key => $value) {
			$value->campo = trim($value->campo);
			$response->data[$value->campo]["defecto"] = $value->campo;
			$response->data[$value->campo]["valor"][$value->fecha] = (double)$value->cantidad;
			if($mode == 2 && $this->session->id_company == 2){
				$count[$value->campo]++;
				$sum[$value->campo] += (double)$value->cantidad;
				// D(ROUND(($sum[$value->fecha] / $count[$value->fecha]) , 2));
				$response->data[$value->campo]["total"] = ROUND(($sum[$value->campo] / $count[$value->campo]) , 2);
			}else{
				$response->data[$value->campo]["total"] += (double)$value->cantidad;
			}
		}
		return $response->data;
	}

	private function chartInit($data = [] , $mode = "vertical" , $name = "" , $type = "line" , $minMax = []){
		$response = new stdClass;
		$response->chart = [];
		if(count($data) > 0){
			$response->chart["title"]["show"] = true;
			$response->chart["title"]["text"] = $name;
			$response->chart["title"]["subtext"] = $this->session->sloganCompany;
			$response->chart["tooltip"]["trigger"] = "item";
			$response->chart["tooltip"]["axisPointer"]["type"] = "shadow";
			$response->chart["toolbox"]["show"] = true;
			$response->chart["toolbox"]["feature"]["mark"]["show"] = true;
			$response->chart["toolbox"]["feature"]["restore"]["show"] = true;
			$response->chart["toolbox"]["feature"]["magicType"]["show"] = true;
			$response->chart["toolbox"]["feature"]["magicType"]["type"] = ['bar' , 'line'];
			$response->chart["toolbox"]["feature"]["saveAsImage"]["show"] = true;
			$response->chart["legend"]["data"] = [];
			$response->chart["legend"]["left"] = "center";
			$response->chart["legend"]["bottom"] = "1%";
			$response->chart["grid"]["left"] = "3%";
			$response->chart["grid"]["right"] = "4%";
			$response->chart["grid"]["bottom"] = "15%";
			$response->chart["grid"]["containLabel"] = true;
			if($mode == "vertical"){
				$response->chart["xAxis"] = ["type" => "category" , "data" => [""]];
				$response->chart["yAxis"] = ["type" => "value"];
				if(isset($minMax["min"])){
					$response->chart["yAxis"] = ["type" => "value" , "min" => $minMax["min"], "max" => $minMax["max"]];
				}
			}else if($mode == "horizontal"){
				$response->chart["yAxis"] = ["type" => "category" , "data" => [""]];
				$response->chart["xAxis"] = ["type" => "value"];
			}
			$response->chart["series"] = [];
			$count = 0;
			$position = -1;
			$colors = [];
			if($type == "line"){
				$response->chart["xAxis"]["data"] = [];
			}
			foreach ($data as $key => $value) {
				$value = (object)$value;
				$value->legend = ucwords(strtolower($value->legend));
				$value->label = ucwords(strtolower($value->label));
				if(isset($value->position)){
					$position = $value->position;
				}
				if(isset($value->color)){
					$colors = [
						"normal" => ["color" => $value->color],
					];
				}

				if($type == "line"){
					if(!in_array($value->label, $response->chart["xAxis"]["data"])){
						if(is_numeric($value->label)){
							$value->label = (double)$value->label;
						}
						$response->chart["xAxis"]["data"][] = $value->label;
					}
					if(!in_array($value->legend, $response->chart["legend"]["data"])){
						if(!isset($value->position)){
							$position++;
						}
						$response->chart["legend"]["data"][] = $value->legend;
						$response->chart["legend"]["selected"][$value->legend] = ($value->selected == 0) ? false : true;
						$response->chart["series"][$position] = [
							"name" => $value->legend,
							"type" => $type,
							"data" => [],
							"label" => [
								"normal" => ["show" => false , "position" => "top"],
								"emphasis" => ["show" => false , "position" => "top"],
							],
							"itemStyle" => $colors
						];
					}

					$response->chart["series"][$position]["data"][] = ROUND($value->value,2);

				}else{
					$response->chart["legend"]["data"][] = $value->label;
					// if($count == 0){
						$response->chart["series"][$count] = [
							"name" => $value->label,
							"type" => $type,
							"data" => [(int)$value->value],
							"label" => [
								"normal" => ["show" => true , "position" => "top"],
								"emphasis" => ["show" => true , "position" => "top"],
							],
							"itemStyle" => $colors
						];
					// }
				}
				$count++;
			}
		}
		return $response->chart;
	}
	private function pie($data = [] , $radius = ['50%', '70%'] , $name = "CATEGORIAS" , $roseType = "" , $type = "normal" , $legend = true , $position = ['45%', '40%'] , $version = 3){
		$response = new stdClass;
		$response->pie = [];
		if(count($data) > 0){
			$response->pie["title"]["show"] = true;
			$response->pie["title"]["text"] = $name;
			$response->pie["title"]["left"] = "center";
			// $response->pie["title"]["left"] = "right";
			$response->pie["title"]["subtext"] = $this->session->sloganCompany;
			$response->pie["tooltip"]["trigger"] = "item";
			$response->pie["tooltip"]["formatter"] = "{a} <br/>{b}: {c} ({d}%)";
			$response->pie["toolbox"]["show"] = true;
			$response->pie["toolbox"]["feature"]["mark"]["show"] = true;
			$response->pie["toolbox"]["feature"]["restore"]["show"] = true;
			$response->pie["toolbox"]["feature"]["magicType"]["show"] = false;
			$response->pie["toolbox"]["feature"]["magicType"]["type"] = ['pie'];
			$response->pie["toolbox"]["feature"]["saveAsImage"]["show"] = true;
			$response->pie["toolbox"]["feature"]["saveAsImage"]["name"] = $name;
			$response->pie["legend"]["show"] = $legend;
			$response->pie["legend"]["x"] = "center";
			$response->pie["legend"]["y"] = "bottom";
			$response->pie["calculable"] = true;
			$response->pie["legend"]["data"] = [];
			$response->pie["series"]["name"] = $name;
			$response->pie["series"]["type"] = "pie";
			$response->pie["series"]["center"] = $position;
			$response->pie["series"]["radius"] = $radius;
			if($version === 3){
				$response->pie["series"]["selectedMode"] = true;
				$response->pie["series"]["label"]["normal"]["show"] = true;
				$response->pie["series"]["label"]["emphasis"]["show"] = true;
				if($type != "normal"){
					$response->pie["series"]["roseType"] = $roseType;
					$response->pie["series"]["avoidLabelOverlap"] = "pie";
				}
				// $response->pie["series"]["label"]["normal"]["position"] = "center";
				// $response->pie["series"]["label"]["emphasis"]["textStyle"]["fontSize"] = "30";
				// $response->pie["series"]["label"]["emphasis"]["textStyle"]["fontWeight"] = "bold";
				$response->pie["series"]["labelLine"]["normal"]["show"] = true;
			}
			$response->pie["series"]["data"] = [];
			$colors = [];
			foreach ($data as $key => $value) {
				$value = (object)$value;
				$value->label = ucwords(strtolower($value->label));
				$response->pie["legend"]["data"][] = $value->label;
				if($version === 3){
					if(isset($value->color)){
							$colors = [
								"normal" => ["color" => $value->color],
							];
					}
					$response->pie["series"]["data"][] = ["name" => $value->label  , "value" => (float)$value->value , 
							"label" => [
								"normal" => ["show" => true , "position" => "outside" , "formatter" => "{b} \n {c} ({d}%)"],
								"emphasis" => ["show" => true , "position" => "outside"],
							],
							"itemStyle" => $colors
					];
				}else{
					// $response->pie["series"]["data"][] = ["name" => $value->label  , "value" => (float)$value->value];
					$response->pie["series"]["data"][] = ["name" => $value->label  , "value" => (float)$value->value,
						"itemStyle" => [
							"normal" => [ "label" => ["formatter" => "{b} \n {c} \n ({d}%)"] ],
							"emphasis" => [ "label" => ["formatter" => "{b} \n {c} \n ({d}%)"] ],
						]
					];
				}
			}
		}

		return $response->pie;
	}

	// 09/05/2017 - TAG: CONFIGURACION
	public function configuracion(){
		$response = new stdClass;
		$postdata = (object)json_decode(file_get_contents("php://input"));
		$data = (object)[
			"tipo" => getValueFrom($postdata , "tipo" , "enfunde" , FILTER_SANITIZE_STRING),
			"mode" => getValueFrom($postdata , "mode" , 2 , FILTER_SANITIZE_PHRAPI_FLOAT),
		];

		if($data->mode > 0 && $data->tipo != ""){
			$sql = "SELECT umbral , bono , descuento FROM bonificacion_conts WHERE tipo = '{$data->tipo}' AND mode = '{$data->mode}'";
			$response->data = $this->db->queryRow($sql);
			$response->data->umbral = (double)$response->data->umbral;
			$response->data->bono = (double)$response->data->bono;
			$response->data->descuento = (double)$response->data->descuento;
		}

		return $response;
	}

	public function save(){
		$response = new stdClass;
		$postdata = (object)json_decode(file_get_contents("php://input"));
		$data = (object)[
			"tipo" => getValueFrom($postdata , "tipo" , "enfunde" , FILTER_SANITIZE_STRING),
			"mode" => getValueFrom($postdata , "mode" , 2 , FILTER_SANITIZE_PHRAPI_FLOAT),
			"umbral" => getValueFrom($postdata , "umbral" , 0 , FILTER_SANITIZE_PHRAPI_FLOAT),
			"bono" => getValueFrom($postdata , "bono" , 0 , FILTER_SANITIZE_PHRAPI_FLOAT),
			"descuento" => getValueFrom($postdata , "descuento" , 0 , FILTER_SANITIZE_PHRAPI_FLOAT),
		];

		$response->success = ["msj" => "Error al guardar" , "success" => 500];

		if($data->mode > 0 && $data->tipo != ""){
			$sql = "UPDATE bonificacion_conts SET bono = {$data->bono},umbral = {$data->umbral},descuento = {$data->descuento} WHERE tipo = '{$data->tipo}' AND mode = '{$data->mode}'";
			$this->db->query($sql);
			$response->success = ["msj" => "Guardado con Exito" , "success" => 200];
		}

		return $response;
	}

	// 09/05/2017 - TAG: CONFIGURACION

	private function clear($String){
		$String = str_replace(array('á','à','â','ã','ª','ä'),"a",$String);
	    $String = str_replace(array('Á','À','Â','Ã','Ä'),"A",$String);
	    $String = str_replace(array('Í','Ì','Î','Ï'),"I",$String);
	    $String = str_replace(array('í','ì','î','ï'),"i",$String);
	    $String = str_replace(array('é','è','ê','ë'),"e",$String);
	    $String = str_replace(array('É','È','Ê','Ë'),"E",$String);
	    $String = str_replace(array('ó','ò','ô','õ','ö','º'),"o",$String);
	    $String = str_replace(array('Ó','Ò','Ô','Õ','Ö'),"O",$String);
	    $String = str_replace(array('ú','ù','û','ü'),"u",$String);
	    $String = str_replace(array('Ú','Ù','Û','Ü'),"U",$String);
	    $String = str_replace(array('[','^','´','`','¨','~',']'),"",$String);
	    $String = str_replace("ç","c",$String);
	    $String = str_replace("Ç","C",$String);
	    $String = str_replace("ñ","n",$String);
	    $String = str_replace("Ñ","N",$String);
	    $String = str_replace("Ý","Y",$String);
	    $String = str_replace("ý","y",$String);
	    $String = str_replace(".","",$String);
	    $String = str_replace("&aacute;","a",$String);
	    $String = str_replace("&Aacute;","A",$String);
	    $String = str_replace("&eacute;","e",$String);
	    $String = str_replace("&Eacute;","E",$String);
	    $String = str_replace("&iacute;","i",$String);
	    $String = str_replace("&Iacute;","I",$String);
	    $String = str_replace("&oacute;","o",$String);
	    $String = str_replace("&Oacute;","O",$String);
	    $String = str_replace("&uacute;","u",$String);
	    $String = str_replace("&Uacute;","U",$String);
	    return $String;
	}
	
}
