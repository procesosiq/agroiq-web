<?php defined('PHRAPI') or die("Direct access not allowed!");

class Enfunde extends ReactGrafica {
	public $name;
	private $db;
	private $config;

	public function __construct(){
        $this->config = $GLOBALS['config'];
        $this->session = Session::getInstance();
        $this->db = DB::getInstance($this->session->agent_user);
    }
    
    public function params(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $data = (object)[
            "week" => getValueFrom($postdata, 'week', ''),
            "year" => getValueFrom($postdata, 'year', date('y')),
            "id_finca" => (int) getValueFrom($postdata, 'id_finca', 1)
        ];
        return $data;
    }

    public function last(){
        $response = new stdClass;
        $params = $this->params();

        $response->weeks = $this->db->queryAll("
            SELECT tbl.anio, tbl.semana, class
            FROM (
                SELECT anio_enfundado AS anio, semana_enfundada AS semana
                FROM racimo_web
                WHERE anio_enfundado > 0 AND semana_enfundada > 0
                GROUP BY anio_enfundado, semana_enfundada
                UNION ALL
                SELECT years, semana
                FROM produccion_enfunde
                WHERE years > 0 AND semana > 0
                GROUP BY years, semana
            ) tbl
            LEFT JOIN semanas_colores sc ON sc.year = anio AND sc.semana = tbl.semana
            LEFT JOIN produccion_colores pc ON pc.color = sc.color
            GROUP BY tbl.anio, tbl.semana
            ORDER BY tbl.anio DESC, tbl.semana DESC");

        $response->last_year = $response->weeks[0]->anio;
        $response->last_week = $response->weeks[0]->semana;
        $response->fincas = $this->db->queryAllSpecial("SELECT id, nombre as label FROM fincas WHERE status_produccion = 'Activo' AND status_enfunde = 'Activo'");
        return $response;
    }

	public function index(){
        $response = new stdClass;
        $params = $this->params();

        $response->color = $this->db->queryOne("SELECT color FROM semanas_colores WHERE year = {$params->year} AND semana = {$params->week}");
        $response->color = $this->db->queryRow("SELECT * FROM produccion_colores WHERE color = '{$response->color}'");
        $response->fincas = $this->db->queryAllSpecial("SELECT fincas.id, fincas.nombre label FROM produccion_enfunde INNER JOIN fincas ON id_finca = fincas.id WHERE semana = {$params->week} AND years = {$params->year} GROUP BY id_finca");
        $response->lotes = $this->db->queryAllOne("SELECT lotes.nombre FROM lotes WHERE idFinca = {$params->id_finca}");

        $sql = "SELECT 
                    lote,
                    lotes.id as id_lote,
                    IFNULL((SELECT SUM(usadas) FROM produccion_enfunde WHERE years = {$params->year} AND semana = {$params->week} AND id_finca = {$params->id_finca} AND lote = tbl.lote), 0) AS racimos_enfunde
                FROM (
                    SELECT lote
                    FROM produccion_enfunde
                    WHERE years = {$params->year} AND semana = {$params->week} AND id_finca = {$params->id_finca}
                    GROUP BY lote
                    UNION ALL
                    SELECT lotes.nombre as lote
                    FROM racimo_web
                    INNER JOIN lotes ON id_lote = lotes.id
                    WHERE anio_enfundado = {$params->year} AND semana_enfundada = {$params->week} AND id_finca = {$params->id_finca}
                    GROUP BY id_lote
                ) tbl
                INNER JOIN lotes ON nombre = lote AND idFinca = {$params->id_finca}
                GROUP BY lote, lotes.id
                ORDER BY lote+0";
        $response->data = $this->db->queryAll($sql);
        $response->edades = $this->db->queryAll("
            SELECT cinta.color as cinta, IFNULL(edad, 'S/C') AS edad 
            FROM racimo_web 
            INNER JOIN cinta ON id_cinta = cinta.id
            WHERE 
                anio_enfundado = {$params->year} 
                AND semana_enfundada = {$params->week}
                AND id_finca = {$params->id_finca}
            GROUP BY edad 
            ORDER BY edad");

        foreach($response->data as $row){
            if(!isset($row->cosechados)){
                $row->cosechados = 0;
            }
            foreach($response->edades as $e){
                $row->{"edad_{$e->edad}"} = (int) $this->db->queryOne("SELECT COUNT(1) FROM racimo_web WHERE anio_enfundado = {$params->year} AND semana_enfundada = {$params->week} AND id_lote = '{$row->id_lote}' AND edad = '{$e->edad}' AND id_finca = {$params->id_finca}");
                $row->cosechados += $row->{"edad_{$e->edad}"};
            }
            $row->enfunde = round(($row->racimos_enfunde > 0 ? ($row->cosechados/$row->racimos_enfunde*100) : 0), 2);
            $row->caidos = (int) $this->db->queryOne("SELECT SUM(cantidad) FROM produccion_racimos_caidos WHERE anio_enfundado = {$params->year} AND semana_enfundada = {$params->week} AND lote = '{$row->lote}' AND id_finca = {$params->id_finca}");
            $row->porc_caidos = round((($row->caidos > 0 && $row->racimos_enfunde > 0) ? ($row->caidos/$row->racimos_enfunde*100) : 0), 2);
        }

        return $response;
    }
}
