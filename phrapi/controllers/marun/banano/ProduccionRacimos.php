<?php defined('PHRAPI') or die("Direct access not allowed!");

class ProduccionRacimos {
	public $name;
	private $db;
	private $config;

	public function __construct(){
		$this->session = Session::getInstance();
        $this->db = DB::getInstance($this->session->agent_user);
        $this->piq = DB::getInstance();
        $this->unidad = $this->session->unidad_racimos;

        if(!isset($this->session->const_unidad_racimos)){
            $this->session->const_unidad_racimos = $this->piq->queryOne("SELECT balanza_racimos_unidad FROM companies WHERE id = {$this->session->id_company}");
        }
    }

    public function saveImg(){
        $postdata = (object)json_decode(file_get_contents("php://input"));

        $data = base64_decode(explode(';base64,', $postdata->data)[1]);
        $path = PHRAPI_PATH."../pdf/produccion-dia/{$postdata->finca}_{$postdata->fecha}";
        $file_path = "{$path}/{$postdata->name}.jpg";

        if(!file_exists($path))
            mkdir($path);
            
        if(file_exists($file_path))
            unlink($file_path);

        file_put_contents($file_path, $data);
    }

    public function convertir($unidad_from, $unidad_to, $valor){
        if($unidad_from == $unidad_to){
            return $valor;
        }

        if($unidad_from == 'kg' && $unidad_to == 'lb'){
            $formula = $this->piq->queryOne("SELECT convertir_kg_lb FROM companies_variables WHERE id_company = {$this->session->id_company}");
            $formula = str_replace('{kg}', $valor, $formula);
            return '('.$formula.')';
        }

        if($unidad_from == 'lb' && $unidad_to == 'kg'){
            $formula = $this->piq->queryOne("SELECT convertir_lb_kg FROM companies_variables WHERE id_company = {$this->session->id_company}");
            $formula = str_replace('{lb}', $valor, $formula);
            return '('.$formula.')';
        }

        return null;
    }
    
    public function changeUnidad(){
        $response = new stdClass;
        $response->status = 200;
        $this->session->unidad_racimos = $this->session->unidad_racimos == 'lb' ? 'kg' : 'lb';
        return $response;
    }

    public function getEdadCinta(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        return $this->db->queryOne("SELECT getEdadCinta(getWeek('{$postdata->fecha}'), '{$postdata->cinta}', YEAR('{$postdata->fecha}'))");
    }

    public function getPesoPromLote(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $lote = $this->db->queryRow("SELECT * FROM lotes WHERE id = {$postdata->id_lote}");
        if($lote->nombre == 'S/L'){
            $last_fecha = $this->db->queryOne("SELECT MAX(fecha) FROM racimo_web WHERE fecha <= '{$postdata->fecha}'");
            return $this->db->queryOne("SELECT AVG(peso_{$this->session->unidad_racimos}) FROM racimo_web WHERE fecha = '{$last_fecha}'");
        }else{
            $last_fecha = $this->db->queryOne("SELECT MAX(fecha) FROM racimo_web WHERE id_lote = {$postdata->id_lote} AND fecha <= '{$postdata->fecha}'");
            return $this->db->queryOne("SELECT AVG(peso_{$this->session->unidad_racimos}) FROM racimo_web WHERE fecha = '{$last_fecha}' AND id_lote = {$postdata->id_lote}");
        }
    }

	public function lastDay(){
        $response = new stdClass;
        $response->unidad = ucwords($this->unidad);

        #0.123
        $response->pdf_route = $this->piq->queryOne("SELECT racimos_form_url_pdf FROM companies WHERE id = {$this->session->id_company}");
        #0.124
        $variables = $this->piq->queryRow("SELECT * FROM companies_variables WHERE id_company = {$this->session->id_company}");
        $response->enabled = [
            "formularios" => (int) $variables->racimos_formularios,
            "dedos" => (int) $variables->racimos_dedos,
            "calibre_ultima" => (int) $variables->racimos_calibre_ultima
        ];
        #5.5s
        $response->days = $this->db->queryAllOne("SELECT fecha FROM racimo_web GROUP BY fecha");
        #1.125s
        $response->last = $this->db->queryRow("SELECT MAX(fecha) AS fecha FROM racimo_web");
        #0.126s
        $response->fincas = $this->db->queryAllSpecial("SELECT id_finca AS id, fincas.nombre AS label FROM racimo_web INNER JOIN fincas ON id_finca = fincas.id WHERE fecha = '{$response->last->fecha}' GROUP BY id_finca");
        #0.123
        $response->causas = $this->db->queryAllOne("SELECT nombre FROM produccion_racimos_causas WHERE status = 'Activo'");
        #0.125
        $response->fincasAvailable = $this->db->queryAll("SELECT * FROM fincas WHERE status_produccion = 'Activo' AND status = 1");
        foreach($response->fincasAvailable as $f){
            #0.127 * (fincas)
            $f->lotes = $this->db->queryAll("SELECT * FROM lotes WHERE idFinca = $f->id");
        }

		return $response;
    }

    public function tags(){
        $response = new stdClass;
        $response->tags = [];
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $filters = (object)[
			"finca" => getValueFrom($postdata , "finca" , getValueFrom($_GET, 'finca', '', FILTER_SANITIZE_STRING), FILTER_SANITIZE_STRING),
			"fecha_inicial" => getValueFrom($postdata , "fecha_inicial" , getValueFrom($_GET, 'fecha_inicial', '', FILTER_SANITIZE_STRING), FILTER_SANITIZE_STRING),
            "fecha_final" => getValueFrom($postdata , "fecha_final" , getValueFrom($_GET, 'fecha_final', '', FILTER_SANITIZE_STRING), FILTER_SANITIZE_STRING),
        ];

        $fincas = $this->db->queryAllSpecial("SELECT id_finca AS id, fincas.nombre AS label FROM racimo_web INNER JOIN fincas ON id_finca = fincas.id WHERE fecha = '{$filters->fecha_inicial}' GROUP BY id_finca");
        if(!isset($fincas[$filters->finca]) && $filters->finca != ''){
            $filters->finca = array_keys($fincas)[0];
        }

        $sWhere = " AND fecha = '{$filters->fecha_inicial}'";
        $sWhereM = " AND date_fecha = '{$filters->fecha_inicial}'";
        if($filters->finca != ""){
            $sWhere .= " AND id_finca = $filters->finca";
            $sWhereM .= " AND id_finca = $filters->finca";
        }
        
        $response->tags["procesados"] = $this->db->queryOne("SELECT COUNT(1) FROM racimo_web WHERE tipo = 'PROC' $sWhere");
        $response->tags["recusados"] = $this->db->queryOne("SELECT COUNT(1) FROM racimo_web WHERE tipo = 'RECU' $sWhere");
        $response->tags["cosechados"] = $response->tags["procesados"] + $response->tags["recusados"];
        $response->tags["kg_proc"] = $this->db->queryOne("SELECT SUM(peso_{$this->unidad}) FROM racimo_web WHERE tipo = 'PROC' $sWhere");
        $response->tags["kg_recu"] = $this->db->queryOne("SELECT SUM(peso_{$this->unidad}) FROM racimo_web WHERE tipo = 'RECU' $sWhere");
        $response->tags["kg_prom"] = $this->db->queryOne("SELECT ROUND(AVG(peso_{$this->unidad}), 2) FROM racimo_web WHERE 1=1 $sWhere");

        $unidad_tallo = $this->session->const_unidad_racimos;
        $tallo = 'tallo';
        if($unidad_tallo != $this->unidad){
            $tallo = $this->convertir($unidad_tallo, $this->unidad, 'tallo');
        }
        $response->tags["kg_prom_tallo"] = (float) $this->db->queryOne("SELECT ROUND(AVG({$tallo}/racimos_procesados), 2) FROM merma WHERE 1=1 $sWhereM");
        $sql = "SELECT 
                    ROUND(AVG(peso_{$this->unidad}-{$response->tags['kg_prom_tallo']}), 2)
                FROM racimo_web 
                WHERE 1=1 $sWhere";
        $response->tags["kg_prom_neto"] = $this->db->queryOne($sql);

        $response->tags["calibre_segunda"] = $this->db->queryOne("SELECT ROUND(AVG(calibre_segunda), 2) FROM racimo_web WHERE 1=1 $sWhere");
        $response->tags["calibre_ultima"] = $this->db->queryOne("SELECT ROUND(AVG(calibre_ultima), 2) FROM racimo_web WHERE 1=1 $sWhere");
        $response->tags["manos"] = $this->db->queryOne("SELECT ROUND(AVG(manos), 2) FROM racimo_web WHERE 1=1 $sWhere");
        $response->tags["edad"] = $this->db->queryOne("SELECT ROUND(AVG(edad), 2) FROM racimo_web WHERE 1=1 $sWhere");
        $response->tags["muestreo"] = $this->db->queryOne("SELECT ROUND(COUNT(1)/{$response->tags["cosechados"]}*100, 2) FROM racimo_web WHERE manos IS NOT NULL AND manos > 0 $sWhere");
        $response->tags["muestreo_calibre"] = $this->db->queryOne("SELECT ROUND(COUNT(1)/{$response->tags["cosechados"]}*100, 2) FROM racimo_web WHERE calibre_segunda IS NOT NULL AND calibre_segunda > 0 $sWhere");

        $times = $this->db->queryAll("SELECT fecha, hora FROM racimo_web WHERE 1=1 AND hora != '' $sWhere ORDER BY hora");
        $p = $times[0];
        $u = $times[count($times)-1];
        $response->tags["ultima_fecha"] = $u->fecha;
        $response->tags["ultima_hora"] = $u->hora;
        $response->tags["primera_fecha"] = $p->fecha;
        $response->tags["primera_hora"] = $p->hora;
        $response->tags["diferencia"] = $this->db->queryRow("SELECT TIMEDIFF('{$u->hora}', '{$p->hora}') AS dif")->dif;

        return $response;
    }
    
    public function resumen(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $filters = (object)[
			"finca" => getValueFrom($postdata , "finca" , getValueFrom($_GET, 'finca', '', FILTER_SANITIZE_STRING), FILTER_SANITIZE_STRING),
			"fecha_inicial" => getValueFrom($postdata , "fecha_inicial" , getValueFrom($_GET, 'fecha_inicial', '', FILTER_SANITIZE_STRING), FILTER_SANITIZE_STRING),
            "fecha_final" => getValueFrom($postdata , "fecha_final" , getValueFrom($_GET, 'fecha_final', '', FILTER_SANITIZE_STRING), FILTER_SANITIZE_STRING),
        ];

        $fincas = $this->db->queryAllSpecial("SELECT id_finca AS id, fincas.nombre AS label FROM racimo_web INNER JOIN fincas ON id_finca = fincas.id WHERE fecha = '{$filters->fecha_inicial}' GROUP BY id_finca");
        if(!isset($fincas[$filters->finca]) && $filters->finca != ''){
            $filters->finca = array_keys($fincas)[0];
        }

        $sWhere = " AND fecha = '{$filters->fecha_inicial}'";
        $sWhereM = "";
        if($filters->finca != ""){
            $sWhere .= " AND id_finca = $filters->finca";
        }


        $sql = "SELECT IFNULL(racimos.edad, 'S/C') edad, cinta.color cinta, css_class class
                FROM racimo_web racimos
                LEFT JOIN cinta ON id_cinta = cinta.id
                WHERE 1=1 {$sWhere}
                GROUP BY edad
                ORDER BY edad";
        $edades = $this->db->queryAll($sql);

        $sql = "SELECT 
                    id_lote,
                    lotes.nombre lote, 
                    SUM(IF(tipo = 'PROC', 1, 0)) AS procesados, 
                    SUM(IF(tipo = 'RECU', 1, 0)) AS recusados, 
                    ROUND(AVG(IF(tipo = 'PROC', calibre_segunda, NULL)), 2) AS calibre_segunda,
                    ROUND(AVG(IF(tipo = 'PROC', calibre_ultima, NULL)), 2) AS calibre_ultima,
                    ROUND(AVG(IF(tipo = 'PROC', manos , NULL)), 2) AS manos,
                    ROUND(AVG(IF(tipo = 'PROC', dedos, NULL)), 2) AS dedos,
                    ROUND(AVG(peso_{$this->unidad}), 2) AS peso
                FROM racimo_web
                INNER JOIN lotes ON id_lote = lotes.id
                WHERE 1=1 $sWhere
                GROUP BY lotes.id
                ORDER BY lotes.nombre+0";
        $data = $this->db->queryAll($sql);
        foreach($data as $row){
            foreach($edades as $edad){
                if($edad->edad > 0){
                    $row->{"edad_{$edad->edad}"} = (int) $this->db->queryOne("SELECT COUNT(1) FROM racimo_web WHERE id_lote = {$row->id_lote} AND edad = {$edad->edad} $sWhere");
                }else{
                    $row->{"edad_S/C"} = (int) $this->db->queryOne("SELECT COUNT(1) FROM racimo_web WHERE id_lote = {$row->id_lote} AND (edad IS NULL OR edad = 0) $sWhere");
                }
            }

            $unidad_tallo = $this->session->const_unidad_racimos;
            $tallo = 'tallo';
            if($unidad_tallo != $this->unidad){
                $tallo = $this->convertir($unidad_tallo, $this->unidad, 'tallo');
            }
            $row->peso = (float) $row->peso;
            $row->peso_tallo = (float) $this->db->queryOne("SELECT ROUND(AVG({$tallo}/racimos_procesados), 2) FROM merma WHERE bloque = '{$row->lote}' $sWhereM");
            $row->peso_neto = $row->peso - $row->peso_tallo;
        }

        $response->edades = $edades;
        $response->data = $data;
        $response->totales = [
            "peso_prom" => $this->db->queryOne("SELECT AVG(peso_{$this->unidad}) FROM racimo_web WHERE 1=1 $sWhere"),
            "calibre_segunda" => $this->db->queryOne("SELECT AVG(calibre_segunda) FROM racimo_web WHERE 1=1 $sWhere"),
            "calibre_ultima" => $this->db->queryOne("SELECT AVG(calibre_ultima) FROM racimo_web WHERE 1=1 $sWhere"),
            "manos_prom" => $this->db->queryOne("SELECT AVG(manos) FROM racimo_web WHERE 1=1 $sWhere"),
            "dedos_prom" => $this->db->queryOne("SELECT AVG(dedos) FROM racimo_web WHERE 1=1 $sWhere"),
        ];
        return $response;
    }

	public function historico(){
		$postdata = (object)json_decode(file_get_contents("php://input"));

		$filters = (object)[
			"finca" => getValueFrom($postdata , "finca" , "" , FILTER_SANITIZE_STRING),
			"fecha_inicial" => getValueFrom($postdata , "fecha_inicial" , "" , FILTER_SANITIZE_STRING),
            "fecha_final" => getValueFrom($postdata , "fecha_final" , "" , FILTER_SANITIZE_STRING),
        ];

        $response = new stdClass;
        $fincas = $this->db->queryAllSpecial("SELECT id_finca AS id, fincas.nombre AS label FROM racimo_web INNER JOIN fincas ON id_finca = fincas.id WHERE fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}' GROUP BY id_finca");
        
        $sWhere = "";
        if(!isset($fincas[$filters->finca]) && $filters->finca != ''){
            $filters->finca = array_keys($fincas)[0];
        }
        if($filters->finca != ""){
            $sWhere .= " AND id_finca = $filters->finca";
        }
        
		$response->data = $this->db->queryAll("SELECT 
                historica.id, 
                fecha, 
                lotes.nombre lote, 
                causa, 
                peso_{$this->unidad} peso, 
                IF(tipo = 'RECU', 0 , manos) AS manos, 
                cinta.color as cinta, 
                IF(tipo = 'RECU', 0, dedos) AS dedos, 
                cinta.css_class as class, 
                historica.edad, 
                palanca cuadrilla, 
                hora, 
                tipo, 
                calibre_segunda, 
                calibre_ultima, 
                historica.edad _edad, 
                lotes.nombre _lote, 
                palanca _cuadrilla, 
                viaje, 
                racimo num_racimo
			FROM racimo_web historica
            INNER JOIN lotes ON lotes.id = id_lote
			LEFT JOIN cinta ON id_cinta = cinta.id
            WHERE fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}' $sWhere
            ORDER BY viaje, racimo");

		return $response;
    }
    
    public function defectos(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
		$filters = (object)[
			"finca" => getValueFrom($postdata , "finca" , getValueFrom($_GET, 'finca', '', FILTER_SANITIZE_STRING), FILTER_SANITIZE_STRING),
			"fecha_inicial" => getValueFrom($postdata , "fecha_inicial" , getValueFrom($_GET, 'fecha_inicial', '', FILTER_SANITIZE_STRING), FILTER_SANITIZE_STRING),
            "fecha_final" => getValueFrom($postdata , "fecha_final" , getValueFrom($_GET, 'fecha_final', '', FILTER_SANITIZE_STRING), FILTER_SANITIZE_STRING),
        ];

        $response = new stdClass;
        $fincas = $this->db->queryAllSpecial("SELECT id_finca AS id, fincas.nombre AS label FROM racimo_web INNER JOIN fincas ON id_finca = fincas.id WHERE fecha = '{$filters->fecha_inicial}' GROUP BY id_finca");

        $sWhere = "";
        if(!isset($fincas[$filters->finca]) && $filters->finca != ''){
            $filters->finca = array_keys($fincas)[0];
        }
        if($filters->finca != ""){
            $sWhere .= " AND id_finca = $filters->finca";
        }

        $sql = "SELECT causa, COUNT(1) AS cantidad
                FROM racimo_web
                WHERE tipo = 'RECU' AND fecha = '{$filters->fecha_inicial}' {$sWhere}
                GROUP BY causa";
        $data = $this->db->queryAll($sql);

        $total_recusados = 0;
        foreach($data as $row){
            $total_recusados += $row->cantidad;
        }
        foreach($data as $row){
            $row->porcentaje = round($row->cantidad / $total_recusados * 100, 2);
        }
        
        $response->data = $data;
        return $response;
    }

    public function eliminar(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        if(count($postdata->ids) > 0){
            foreach($postdata->ids as $reg){
                $this->db->query("DELETE FROM racimo_web WHERE id = $reg->id");
            }
            return true;
        }
        return false;
    }
    
    public function editar(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        if(isset($postdata->id)){
            if($postdata->edad > 0 && $postdata->cuadrilla != '' && $postdata->lote != ''){
                $sql = "UPDATE racimo_web
                        SET
                            palanca = '{$postdata->cuadrilla}',
                            id_lote = (SELECT id FROM lotes WHERE nombre = '{$postdata->lote}' AND idFinca = id_finca),
                            tipo = '{$postdata->tipo}',
                            causa = IF('{$postdata->tipo}' = 'PROC', '', '{$postdata->causa}'),
                            edad = {$postdata->edad},
                            id_cinta = (SELECT id FROM cinta WHERE color = getCintaFromEdad(edad, semana, anio)),
                            calibre_segunda = '{$postdata->calibre_segunda}',
                            calibre_ultima = '{$postdata->calibre_ultima}',
                            dedos = '{$postdata->dedos}',
                            manos = '{$postdata->manos}',
                            updated_at = CURRENT_TIMESTAMP
                        WHERE id = {$postdata->id}";
                $this->db->query($sql);
                return $this->db->queryRow("SELECT r.*, cinta.css_class as class FROM racimo_web r LEFT JOIN cinta ON cinta.id = id_cinta WHERE r.id = {$postdata->id}");
            }
        }
        return false;
    }

    public function analizisRecusados(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));

        if($postdata->var_recusado == 'cant'){
            $sql = "SELECT
                    causa AS dano,
                    SUM(cantidad) AS 'sum',
                    MIN(cantidad) AS 'min',
                    MAX(cantidad) AS 'max',
                    ROUND(AVG(cantidad), 2) AS 'prom',
                    SUM(IF(semana = 0, cantidad, 0)) AS 'sem_0',
                    SUM(IF(semana = 1, cantidad, 0)) AS 'sem_1',
                    SUM(IF(semana = 2, cantidad, 0)) AS 'sem_2',
                    SUM(IF(semana = 3, cantidad, 0)) AS 'sem_3',
                    SUM(IF(semana = 4, cantidad, 0)) AS 'sem_4',
                    SUM(IF(semana = 5, cantidad, 0)) AS 'sem_5',
                    SUM(IF(semana = 6, cantidad, 0)) AS 'sem_6',
                    SUM(IF(semana = 7, cantidad, 0)) AS 'sem_7',
                    SUM(IF(semana = 8, cantidad, 0)) AS 'sem_8',
                    SUM(IF(semana = 9, cantidad, 0)) AS 'sem_9',
                    SUM(IF(semana = 10, cantidad, 0)) AS 'sem_10',
                    SUM(IF(semana = 11, cantidad, 0)) AS 'sem_11',
                    SUM(IF(semana = 12, cantidad, 0)) AS 'sem_12',
                    SUM(IF(semana = 13, cantidad, 0)) AS 'sem_13',
                    SUM(IF(semana = 14, cantidad, 0)) AS 'sem_14',
                    SUM(IF(semana = 15, cantidad, 0)) AS 'sem_15',
                    SUM(IF(semana = 16, cantidad, 0)) AS 'sem_16',
                    SUM(IF(semana = 17, cantidad, 0)) AS 'sem_17',
                    SUM(IF(semana = 18, cantidad, 0)) AS 'sem_18',
                    SUM(IF(semana = 19, cantidad, 0)) AS 'sem_19',
                    SUM(IF(semana = 20, cantidad, 0)) AS 'sem_20',
                    SUM(IF(semana = 21, cantidad, 0)) AS 'sem_21',
                    SUM(IF(semana = 22, cantidad, 0)) AS 'sem_22',
                    SUM(IF(semana = 23, cantidad, 0)) AS 'sem_23',
                    SUM(IF(semana = 24, cantidad, 0)) AS 'sem_24',
                    SUM(IF(semana = 25, cantidad, 0)) AS 'sem_25',
                    SUM(IF(semana = 26, cantidad, 0)) AS 'sem_26',
                    SUM(IF(semana = 27, cantidad, 0)) AS 'sem_27',
                    SUM(IF(semana = 28, cantidad, 0)) AS 'sem_28',
                    SUM(IF(semana = 29, cantidad, 0)) AS 'sem_29',
                    SUM(IF(semana = 30, cantidad, 0)) AS 'sem_30',
                    SUM(IF(semana = 31, cantidad, 0)) AS 'sem_31',
                    SUM(IF(semana = 32, cantidad, 0)) AS 'sem_32',
                    SUM(IF(semana = 33, cantidad, 0)) AS 'sem_33',
                    SUM(IF(semana = 34, cantidad, 0)) AS 'sem_34',
                    SUM(IF(semana = 35, cantidad, 0)) AS 'sem_35',
                    SUM(IF(semana = 36, cantidad, 0)) AS 'sem_36',
                    SUM(IF(semana = 37, cantidad, 0)) AS 'sem_37',
                    SUM(IF(semana = 38, cantidad, 0)) AS 'sem_38',
                    SUM(IF(semana = 39, cantidad, 0)) AS 'sem_39',
                    SUM(IF(semana = 40, cantidad, 0)) AS 'sem_40',
                    SUM(IF(semana = 41, cantidad, 0)) AS 'sem_41',
                    SUM(IF(semana = 42, cantidad, 0)) AS 'sem_42',
                    SUM(IF(semana = 43, cantidad, 0)) AS 'sem_43',
                    SUM(IF(semana = 44, cantidad, 0)) AS 'sem_44',
                    SUM(IF(semana = 45, cantidad, 0)) AS 'sem_45',
                    SUM(IF(semana = 46, cantidad, 0)) AS 'sem_46',
                    SUM(IF(semana = 47, cantidad, 0)) AS 'sem_47',
                    SUM(IF(semana = 48, cantidad, 0)) AS 'sem_48',
                    SUM(IF(semana = 49, cantidad, 0)) AS 'sem_49',
                    SUM(IF(semana = 50, cantidad, 0)) AS 'sem_50',
                    SUM(IF(semana = 51, cantidad, 0)) AS 'sem_51',
                    SUM(IF(semana = 52, cantidad, 0)) AS 'sem_52',
                    SUM(IF(semana = 53, cantidad, 0)) AS 'sem_53'
                FROM (
                    SELECT semana, causa, COUNT(1) AS cantidad
                    FROM racimo_web
                    WHERE tipo = 'RECU' AND anio = YEAR('{$postdata->fecha_inicial}') AND semana > 0 AND id_finca = {$postdata->finca}
                    GROUP BY semana
                ) AS tbl
                GROUP BY causa
                UNION ALL
                SELECT
                    'TOTAL' AS dano,
                    SUM(cantidad) AS 'sum',
                    MIN(cantidad) AS 'min',
                    MAX(cantidad) AS 'max',
                    ROUND(AVG(cantidad), 2) AS 'prom',
                    SUM(IF(semana = 0, cantidad, 0)) AS 'sem_0',
                    SUM(IF(semana = 1, cantidad, 0)) AS 'sem_1',
                    SUM(IF(semana = 2, cantidad, 0)) AS 'sem_2',
                    SUM(IF(semana = 3, cantidad, 0)) AS 'sem_3',
                    SUM(IF(semana = 4, cantidad, 0)) AS 'sem_4',
                    SUM(IF(semana = 5, cantidad, 0)) AS 'sem_5',
                    SUM(IF(semana = 6, cantidad, 0)) AS 'sem_6',
                    SUM(IF(semana = 7, cantidad, 0)) AS 'sem_7',
                    SUM(IF(semana = 8, cantidad, 0)) AS 'sem_8',
                    SUM(IF(semana = 9, cantidad, 0)) AS 'sem_9',
                    SUM(IF(semana = 10, cantidad, 0)) AS 'sem_10',
                    SUM(IF(semana = 11, cantidad, 0)) AS 'sem_11',
                    SUM(IF(semana = 12, cantidad, 0)) AS 'sem_12',
                    SUM(IF(semana = 13, cantidad, 0)) AS 'sem_13',
                    SUM(IF(semana = 14, cantidad, 0)) AS 'sem_14',
                    SUM(IF(semana = 15, cantidad, 0)) AS 'sem_15',
                    SUM(IF(semana = 16, cantidad, 0)) AS 'sem_16',
                    SUM(IF(semana = 17, cantidad, 0)) AS 'sem_17',
                    SUM(IF(semana = 18, cantidad, 0)) AS 'sem_18',
                    SUM(IF(semana = 19, cantidad, 0)) AS 'sem_19',
                    SUM(IF(semana = 20, cantidad, 0)) AS 'sem_20',
                    SUM(IF(semana = 21, cantidad, 0)) AS 'sem_21',
                    SUM(IF(semana = 22, cantidad, 0)) AS 'sem_22',
                    SUM(IF(semana = 23, cantidad, 0)) AS 'sem_23',
                    SUM(IF(semana = 24, cantidad, 0)) AS 'sem_24',
                    SUM(IF(semana = 25, cantidad, 0)) AS 'sem_25',
                    SUM(IF(semana = 26, cantidad, 0)) AS 'sem_26',
                    SUM(IF(semana = 27, cantidad, 0)) AS 'sem_27',
                    SUM(IF(semana = 28, cantidad, 0)) AS 'sem_28',
                    SUM(IF(semana = 29, cantidad, 0)) AS 'sem_29',
                    SUM(IF(semana = 30, cantidad, 0)) AS 'sem_30',
                    SUM(IF(semana = 31, cantidad, 0)) AS 'sem_31',
                    SUM(IF(semana = 32, cantidad, 0)) AS 'sem_32',
                    SUM(IF(semana = 33, cantidad, 0)) AS 'sem_33',
                    SUM(IF(semana = 34, cantidad, 0)) AS 'sem_34',
                    SUM(IF(semana = 35, cantidad, 0)) AS 'sem_35',
                    SUM(IF(semana = 36, cantidad, 0)) AS 'sem_36',
                    SUM(IF(semana = 37, cantidad, 0)) AS 'sem_37',
                    SUM(IF(semana = 38, cantidad, 0)) AS 'sem_38',
                    SUM(IF(semana = 39, cantidad, 0)) AS 'sem_39',
                    SUM(IF(semana = 40, cantidad, 0)) AS 'sem_40',
                    SUM(IF(semana = 41, cantidad, 0)) AS 'sem_41',
                    SUM(IF(semana = 42, cantidad, 0)) AS 'sem_42',
                    SUM(IF(semana = 43, cantidad, 0)) AS 'sem_43',
                    SUM(IF(semana = 44, cantidad, 0)) AS 'sem_44',
                    SUM(IF(semana = 45, cantidad, 0)) AS 'sem_45',
                    SUM(IF(semana = 46, cantidad, 0)) AS 'sem_46',
                    SUM(IF(semana = 47, cantidad, 0)) AS 'sem_47',
                    SUM(IF(semana = 48, cantidad, 0)) AS 'sem_48',
                    SUM(IF(semana = 49, cantidad, 0)) AS 'sem_49',
                    SUM(IF(semana = 50, cantidad, 0)) AS 'sem_50',
                    SUM(IF(semana = 51, cantidad, 0)) AS 'sem_51',
                    SUM(IF(semana = 52, cantidad, 0)) AS 'sem_52',
                    SUM(IF(semana = 53, cantidad, 0)) AS 'sem_53'
                FROM (
                    SELECT semana, causa, COUNT(1) AS cantidad
                    FROM racimo_web
                    WHERE tipo = 'RECU' AND anio = YEAR('{$postdata->fecha_inicial}') AND semana > 0 AND id_finca = {$postdata->finca}
                    GROUP BY semana
                ) AS tbl";
            $response->data = $this->db->queryAll($sql);
        }else{
            $sql = "SELECT
                    causa AS dano,
                    SUM(cantidad) AS 'sum',
                    MIN(cantidad) AS 'min',
                    MAX(cantidad) AS 'max',
                    ROUND(AVG(cantidad), 2) AS 'prom',
                    SUM(IF(semana = 0, cantidad, 0)) AS 'sem_0',
                    SUM(IF(semana = 1, cantidad, 0)) AS 'sem_1',
                    SUM(IF(semana = 2, cantidad, 0)) AS 'sem_2',
                    SUM(IF(semana = 3, cantidad, 0)) AS 'sem_3',
                    SUM(IF(semana = 4, cantidad, 0)) AS 'sem_4',
                    SUM(IF(semana = 5, cantidad, 0)) AS 'sem_5',
                    SUM(IF(semana = 6, cantidad, 0)) AS 'sem_6',
                    SUM(IF(semana = 7, cantidad, 0)) AS 'sem_7',
                    SUM(IF(semana = 8, cantidad, 0)) AS 'sem_8',
                    SUM(IF(semana = 9, cantidad, 0)) AS 'sem_9',
                    SUM(IF(semana = 10, cantidad, 0)) AS 'sem_10',
                    SUM(IF(semana = 11, cantidad, 0)) AS 'sem_11',
                    SUM(IF(semana = 12, cantidad, 0)) AS 'sem_12',
                    SUM(IF(semana = 13, cantidad, 0)) AS 'sem_13',
                    SUM(IF(semana = 14, cantidad, 0)) AS 'sem_14',
                    SUM(IF(semana = 15, cantidad, 0)) AS 'sem_15',
                    SUM(IF(semana = 16, cantidad, 0)) AS 'sem_16',
                    SUM(IF(semana = 17, cantidad, 0)) AS 'sem_17',
                    SUM(IF(semana = 18, cantidad, 0)) AS 'sem_18',
                    SUM(IF(semana = 19, cantidad, 0)) AS 'sem_19',
                    SUM(IF(semana = 20, cantidad, 0)) AS 'sem_20',
                    SUM(IF(semana = 21, cantidad, 0)) AS 'sem_21',
                    SUM(IF(semana = 22, cantidad, 0)) AS 'sem_22',
                    SUM(IF(semana = 23, cantidad, 0)) AS 'sem_23',
                    SUM(IF(semana = 24, cantidad, 0)) AS 'sem_24',
                    SUM(IF(semana = 25, cantidad, 0)) AS 'sem_25',
                    SUM(IF(semana = 26, cantidad, 0)) AS 'sem_26',
                    SUM(IF(semana = 27, cantidad, 0)) AS 'sem_27',
                    SUM(IF(semana = 28, cantidad, 0)) AS 'sem_28',
                    SUM(IF(semana = 29, cantidad, 0)) AS 'sem_29',
                    SUM(IF(semana = 30, cantidad, 0)) AS 'sem_30',
                    SUM(IF(semana = 31, cantidad, 0)) AS 'sem_31',
                    SUM(IF(semana = 32, cantidad, 0)) AS 'sem_32',
                    SUM(IF(semana = 33, cantidad, 0)) AS 'sem_33',
                    SUM(IF(semana = 34, cantidad, 0)) AS 'sem_34',
                    SUM(IF(semana = 35, cantidad, 0)) AS 'sem_35',
                    SUM(IF(semana = 36, cantidad, 0)) AS 'sem_36',
                    SUM(IF(semana = 37, cantidad, 0)) AS 'sem_37',
                    SUM(IF(semana = 38, cantidad, 0)) AS 'sem_38',
                    SUM(IF(semana = 39, cantidad, 0)) AS 'sem_39',
                    SUM(IF(semana = 40, cantidad, 0)) AS 'sem_40',
                    SUM(IF(semana = 41, cantidad, 0)) AS 'sem_41',
                    SUM(IF(semana = 42, cantidad, 0)) AS 'sem_42',
                    SUM(IF(semana = 43, cantidad, 0)) AS 'sem_43',
                    SUM(IF(semana = 44, cantidad, 0)) AS 'sem_44',
                    SUM(IF(semana = 45, cantidad, 0)) AS 'sem_45',
                    SUM(IF(semana = 46, cantidad, 0)) AS 'sem_46',
                    SUM(IF(semana = 47, cantidad, 0)) AS 'sem_47',
                    SUM(IF(semana = 48, cantidad, 0)) AS 'sem_48',
                    SUM(IF(semana = 49, cantidad, 0)) AS 'sem_49',
                    SUM(IF(semana = 50, cantidad, 0)) AS 'sem_50',
                    SUM(IF(semana = 51, cantidad, 0)) AS 'sem_51',
                    SUM(IF(semana = 52, cantidad, 0)) AS 'sem_52',
                    SUM(IF(semana = 53, cantidad, 0)) AS 'sem_53'
                FROM (
                    SELECT semana, causa, ROUND(COUNT(1)/(SELECT COUNT(1) FROM racimo_web WHERE semana = main.semana AND anio = YEAR('{$postdata->fecha_inicial}') AND semana > 0 AND id_finca = {$postdata->finca})*100, 2) AS cantidad
                    FROM racimo_web main
                    WHERE tipo = 'RECU' AND anio = YEAR('{$postdata->fecha_inicial}') AND semana > 0 AND id_finca = {$postdata->finca}
                    GROUP BY semana
                ) AS tbl
                GROUP BY causa";
            $totales = $this->db->queryRow("SELECT tbl.*, 'TOTAL' AS dano, SUM(SUM) AS 'sum', SUM(MIN) AS 'min', SUM(MAX) AS 'max', SUM(prom) AS 'prom'
                FROM (
                    SELECT
                        'TOTAL' AS dano,
                        '' AS 'sum',
                        '' AS 'min',
                        '' AS 'max',
                        '' AS 'prom',
                        SUM(IF(semana = 0, cantidad, 0))/(SELECT COUNT(1) FROM racimo_web WHERE anio = YEAR('{$postdata->fecha_inicial}') AND semana = 1 AND id_finca = {$postdata->finca})*100 AS 'sem_0',
                        SUM(IF(semana = 1, cantidad, 0))/(SELECT COUNT(1) FROM racimo_web WHERE anio = YEAR('{$postdata->fecha_inicial}') AND semana = 1 AND id_finca = {$postdata->finca})*100 AS 'sem_1',
                        SUM(IF(semana = 2, cantidad, 0))/(SELECT COUNT(1) FROM racimo_web WHERE anio = YEAR('{$postdata->fecha_inicial}') AND semana = 2 AND id_finca = {$postdata->finca})*100 AS 'sem_2',
                        SUM(IF(semana = 3, cantidad, 0))/(SELECT COUNT(1) FROM racimo_web WHERE anio = YEAR('{$postdata->fecha_inicial}') AND semana = 3 AND id_finca = {$postdata->finca})*100 AS 'sem_3',
                        SUM(IF(semana = 4, cantidad, 0))/(SELECT COUNT(1) FROM racimo_web WHERE anio = YEAR('{$postdata->fecha_inicial}') AND semana = 4 AND id_finca = {$postdata->finca})*100 AS 'sem_4',
                        SUM(IF(semana = 5, cantidad, 0))/(SELECT COUNT(1) FROM racimo_web WHERE anio = YEAR('{$postdata->fecha_inicial}') AND semana = 5 AND id_finca = {$postdata->finca})*100 AS 'sem_5',
                        SUM(IF(semana = 6, cantidad, 0))/(SELECT COUNT(1) FROM racimo_web WHERE anio = YEAR('{$postdata->fecha_inicial}') AND semana = 6 AND id_finca = {$postdata->finca})*100 AS 'sem_6',
                        SUM(IF(semana = 7, cantidad, 0))/(SELECT COUNT(1) FROM racimo_web WHERE anio = YEAR('{$postdata->fecha_inicial}') AND semana = 7 AND id_finca = {$postdata->finca})*100 AS 'sem_7',
                        SUM(IF(semana = 8, cantidad, 0))/(SELECT COUNT(1) FROM racimo_web WHERE anio = YEAR('{$postdata->fecha_inicial}') AND semana = 8 AND id_finca = {$postdata->finca})*100 AS 'sem_8',
                        SUM(IF(semana = 9, cantidad, 0))/(SELECT COUNT(1) FROM racimo_web WHERE anio = YEAR('{$postdata->fecha_inicial}') AND semana = 9 AND id_finca = {$postdata->finca})*100 AS 'sem_9',
                        SUM(IF(semana = 10, cantidad, 0))/(SELECT COUNT(1) FROM racimo_web WHERE anio = YEAR('{$postdata->fecha_inicial}') AND semana = 10 AND id_finca = {$postdata->finca})*100 AS 'sem_10',
                        SUM(IF(semana = 11, cantidad, 0))/(SELECT COUNT(1) FROM racimo_web WHERE anio = YEAR('{$postdata->fecha_inicial}') AND semana = 11 AND id_finca = {$postdata->finca})*100 AS 'sem_11',
                        SUM(IF(semana = 12, cantidad, 0))/(SELECT COUNT(1) FROM racimo_web WHERE anio = YEAR('{$postdata->fecha_inicial}') AND semana = 12 AND id_finca = {$postdata->finca})*100 AS 'sem_12',
                        SUM(IF(semana = 13, cantidad, 0))/(SELECT COUNT(1) FROM racimo_web WHERE anio = YEAR('{$postdata->fecha_inicial}') AND semana = 13 AND id_finca = {$postdata->finca})*100 AS 'sem_13',
                        SUM(IF(semana = 14, cantidad, 0))/(SELECT COUNT(1) FROM racimo_web WHERE anio = YEAR('{$postdata->fecha_inicial}') AND semana = 14 AND id_finca = {$postdata->finca})*100 AS 'sem_14',
                        SUM(IF(semana = 15, cantidad, 0))/(SELECT COUNT(1) FROM racimo_web WHERE anio = YEAR('{$postdata->fecha_inicial}') AND semana = 15 AND id_finca = {$postdata->finca})*100 AS 'sem_15',
                        SUM(IF(semana = 16, cantidad, 0))/(SELECT COUNT(1) FROM racimo_web WHERE anio = YEAR('{$postdata->fecha_inicial}') AND semana = 16 AND id_finca = {$postdata->finca})*100 AS 'sem_16',
                        SUM(IF(semana = 17, cantidad, 0))/(SELECT COUNT(1) FROM racimo_web WHERE anio = YEAR('{$postdata->fecha_inicial}') AND semana = 17 AND id_finca = {$postdata->finca})*100 AS 'sem_17',
                        SUM(IF(semana = 18, cantidad, 0))/(SELECT COUNT(1) FROM racimo_web WHERE anio = YEAR('{$postdata->fecha_inicial}') AND semana = 18 AND id_finca = {$postdata->finca})*100 AS 'sem_18',
                        SUM(IF(semana = 19, cantidad, 0))/(SELECT COUNT(1) FROM racimo_web WHERE anio = YEAR('{$postdata->fecha_inicial}') AND semana = 19 AND id_finca = {$postdata->finca})*100 AS 'sem_19',
                        SUM(IF(semana = 20, cantidad, 0))/(SELECT COUNT(1) FROM racimo_web WHERE anio = YEAR('{$postdata->fecha_inicial}') AND semana = 20 AND id_finca = {$postdata->finca})*100 AS 'sem_20',
                        SUM(IF(semana = 21, cantidad, 0))/(SELECT COUNT(1) FROM racimo_web WHERE anio = YEAR('{$postdata->fecha_inicial}') AND semana = 21 AND id_finca = {$postdata->finca})*100 AS 'sem_21',
                        SUM(IF(semana = 22, cantidad, 0))/(SELECT COUNT(1) FROM racimo_web WHERE anio = YEAR('{$postdata->fecha_inicial}') AND semana = 22 AND id_finca = {$postdata->finca})*100 AS 'sem_22',
                        SUM(IF(semana = 23, cantidad, 0))/(SELECT COUNT(1) FROM racimo_web WHERE anio = YEAR('{$postdata->fecha_inicial}') AND semana = 23 AND id_finca = {$postdata->finca})*100 AS 'sem_23',
                        SUM(IF(semana = 24, cantidad, 0))/(SELECT COUNT(1) FROM racimo_web WHERE anio = YEAR('{$postdata->fecha_inicial}') AND semana = 24 AND id_finca = {$postdata->finca})*100 AS 'sem_24',
                        SUM(IF(semana = 25, cantidad, 0))/(SELECT COUNT(1) FROM racimo_web WHERE anio = YEAR('{$postdata->fecha_inicial}') AND semana = 25 AND id_finca = {$postdata->finca})*100 AS 'sem_25',
                        SUM(IF(semana = 26, cantidad, 0))/(SELECT COUNT(1) FROM racimo_web WHERE anio = YEAR('{$postdata->fecha_inicial}') AND semana = 26 AND id_finca = {$postdata->finca})*100 AS 'sem_26',
                        SUM(IF(semana = 27, cantidad, 0))/(SELECT COUNT(1) FROM racimo_web WHERE anio = YEAR('{$postdata->fecha_inicial}') AND semana = 27 AND id_finca = {$postdata->finca})*100 AS 'sem_27',
                        SUM(IF(semana = 28, cantidad, 0))/(SELECT COUNT(1) FROM racimo_web WHERE anio = YEAR('{$postdata->fecha_inicial}') AND semana = 28 AND id_finca = {$postdata->finca})*100 AS 'sem_28',
                        SUM(IF(semana = 29, cantidad, 0))/(SELECT COUNT(1) FROM racimo_web WHERE anio = YEAR('{$postdata->fecha_inicial}') AND semana = 29 AND id_finca = {$postdata->finca})*100 AS 'sem_29',
                        SUM(IF(semana = 30, cantidad, 0))/(SELECT COUNT(1) FROM racimo_web WHERE anio = YEAR('{$postdata->fecha_inicial}') AND semana = 30 AND id_finca = {$postdata->finca})*100 AS 'sem_30',
                        SUM(IF(semana = 31, cantidad, 0))/(SELECT COUNT(1) FROM racimo_web WHERE anio = YEAR('{$postdata->fecha_inicial}') AND semana = 31 AND id_finca = {$postdata->finca})*100 AS 'sem_31',
                        SUM(IF(semana = 32, cantidad, 0))/(SELECT COUNT(1) FROM racimo_web WHERE anio = YEAR('{$postdata->fecha_inicial}') AND semana = 32 AND id_finca = {$postdata->finca})*100 AS 'sem_32',
                        SUM(IF(semana = 33, cantidad, 0))/(SELECT COUNT(1) FROM racimo_web WHERE anio = YEAR('{$postdata->fecha_inicial}') AND semana = 33 AND id_finca = {$postdata->finca})*100 AS 'sem_33',
                        SUM(IF(semana = 34, cantidad, 0))/(SELECT COUNT(1) FROM racimo_web WHERE anio = YEAR('{$postdata->fecha_inicial}') AND semana = 34 AND id_finca = {$postdata->finca})*100 AS 'sem_34',
                        SUM(IF(semana = 35, cantidad, 0))/(SELECT COUNT(1) FROM racimo_web WHERE anio = YEAR('{$postdata->fecha_inicial}') AND semana = 35 AND id_finca = {$postdata->finca})*100 AS 'sem_35',
                        SUM(IF(semana = 36, cantidad, 0))/(SELECT COUNT(1) FROM racimo_web WHERE anio = YEAR('{$postdata->fecha_inicial}') AND semana = 36 AND id_finca = {$postdata->finca})*100 AS 'sem_36',
                        SUM(IF(semana = 37, cantidad, 0))/(SELECT COUNT(1) FROM racimo_web WHERE anio = YEAR('{$postdata->fecha_inicial}') AND semana = 37 AND id_finca = {$postdata->finca})*100 AS 'sem_37',
                        SUM(IF(semana = 38, cantidad, 0))/(SELECT COUNT(1) FROM racimo_web WHERE anio = YEAR('{$postdata->fecha_inicial}') AND semana = 38 AND id_finca = {$postdata->finca})*100 AS 'sem_38',
                        SUM(IF(semana = 39, cantidad, 0))/(SELECT COUNT(1) FROM racimo_web WHERE anio = YEAR('{$postdata->fecha_inicial}') AND semana = 39 AND id_finca = {$postdata->finca})*100 AS 'sem_39',
                        SUM(IF(semana = 40, cantidad, 0))/(SELECT COUNT(1) FROM racimo_web WHERE anio = YEAR('{$postdata->fecha_inicial}') AND semana = 40 AND id_finca = {$postdata->finca})*100 AS 'sem_40',
                        SUM(IF(semana = 41, cantidad, 0))/(SELECT COUNT(1) FROM racimo_web WHERE anio = YEAR('{$postdata->fecha_inicial}') AND semana = 41 AND id_finca = {$postdata->finca})*100 AS 'sem_41',
                        SUM(IF(semana = 42, cantidad, 0))/(SELECT COUNT(1) FROM racimo_web WHERE anio = YEAR('{$postdata->fecha_inicial}') AND semana = 42 AND id_finca = {$postdata->finca})*100 AS 'sem_42',
                        SUM(IF(semana = 43, cantidad, 0))/(SELECT COUNT(1) FROM racimo_web WHERE anio = YEAR('{$postdata->fecha_inicial}') AND semana = 43 AND id_finca = {$postdata->finca})*100 AS 'sem_43',
                        SUM(IF(semana = 44, cantidad, 0))/(SELECT COUNT(1) FROM racimo_web WHERE anio = YEAR('{$postdata->fecha_inicial}') AND semana = 44 AND id_finca = {$postdata->finca})*100 AS 'sem_44',
                        SUM(IF(semana = 45, cantidad, 0))/(SELECT COUNT(1) FROM racimo_web WHERE anio = YEAR('{$postdata->fecha_inicial}') AND semana = 45 AND id_finca = {$postdata->finca})*100 AS 'sem_45',
                        SUM(IF(semana = 46, cantidad, 0))/(SELECT COUNT(1) FROM racimo_web WHERE anio = YEAR('{$postdata->fecha_inicial}') AND semana = 46 AND id_finca = {$postdata->finca})*100 AS 'sem_46',
                        SUM(IF(semana = 47, cantidad, 0))/(SELECT COUNT(1) FROM racimo_web WHERE anio = YEAR('{$postdata->fecha_inicial}') AND semana = 47 AND id_finca = {$postdata->finca})*100 AS 'sem_47',
                        SUM(IF(semana = 48, cantidad, 0))/(SELECT COUNT(1) FROM racimo_web WHERE anio = YEAR('{$postdata->fecha_inicial}') AND semana = 48 AND id_finca = {$postdata->finca})*100 AS 'sem_48',
                        SUM(IF(semana = 49, cantidad, 0))/(SELECT COUNT(1) FROM racimo_web WHERE anio = YEAR('{$postdata->fecha_inicial}') AND semana = 49 AND id_finca = {$postdata->finca})*100 AS 'sem_49',
                        SUM(IF(semana = 50, cantidad, 0))/(SELECT COUNT(1) FROM racimo_web WHERE anio = YEAR('{$postdata->fecha_inicial}') AND semana = 50 AND id_finca = {$postdata->finca})*100 AS 'sem_50',
                        SUM(IF(semana = 51, cantidad, 0))/(SELECT COUNT(1) FROM racimo_web WHERE anio = YEAR('{$postdata->fecha_inicial}') AND semana = 51 AND id_finca = {$postdata->finca})*100 AS 'sem_51',
                        SUM(IF(semana = 52, cantidad, 0))/(SELECT COUNT(1) FROM racimo_web WHERE anio = YEAR('{$postdata->fecha_inicial}') AND semana = 52 AND id_finca = {$postdata->finca})*100 AS 'sem_52',
                        SUM(IF(semana = 53, cantidad, 0))/(SELECT COUNT(1) FROM racimo_web WHERE anio = YEAR('{$postdata->fecha_inicial}') AND semana = 53 AND id_finca = {$postdata->finca})*100 AS 'sem_53'
                    FROM (
                        SELECT semana, causa, COUNT(1) AS cantidad
                        FROM racimo_web
                        WHERE tipo = 'RECU' AND anio = YEAR('{$postdata->fecha_inicial}') AND semana > 0 AND id_finca = {$postdata->finca}
                        GROUP BY causa, semana
                    ) AS tbl
                ) AS tbl
                GROUP BY dano");
            $row_totales_gropales = $this->db->queryRow("SELECT SUM(recusados)/SUM(cortados)*100 AS 'sum', MIN(cantidad) AS 'min', MAX(cantidad) AS 'max', AVG(cantidad) AS 'prom'
                FROM (
                    SELECT semana, SUM(recusados) AS recusados, SUM(cortados) AS cortados, IF(recusados > 0, recusados/cortados*100, NULL) AS cantidad
                    FROM (
                        SELECT semana, COUNT(1) AS cortados, SUM(IF(tipo = 'RECU', 1, 0)) AS recusados
                        FROM racimo_web
                        WHERE anio = YEAR('{$postdata->fecha_inicial}') AND id_finca = {$postdata->finca}
                        GROUP BY semana
                    ) AS tbl
                    GROUP BY semana
                ) AS tbl");

            $totales->prom = $row_totales_gropales->prom;
            $totales->sum = $row_totales_gropales->sum;
            $totales->min = $row_totales_gropales->min;
            $totales->max = $row_totales_gropales->max;
            $response->data = $this->db->queryAll($sql);
            $response->data[] = $totales;
        }
        $response->semanas = $this->db->queryAllOne("SELECT semana FROM racimo_web WHERE tipo = 'RECU' AND anio = YEAR('{$postdata->fecha_inicial}') AND semana > 0 AND id_finca = {$postdata->finca} GROUP BY semana");
        return $response;
    }

    private function getSemanaVar($var, $year, $id_finca){
        $sql = [];
        for($x = 1; $x <= 52; $x++){
            $sql[] = "SELECT sem_{$x} value, {$x} label_x FROM produccion_resumen_tabla WHERE variable = '{$var}' AND anio = '{$year}' AND id_finca = {$id_finca} AND sem_{$x} IS NOT NULL";
        }
        return implode("
        UNION ALL
        ", $sql);
    }

    public function muestreo(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $filters = (object)[
            "finca" => getValueFrom($postdata , "finca" , "" , FILTER_SANITIZE_STRING),
            "fecha_inicial" => getValueFrom($postdata , "fecha_inicial" , "" , FILTER_SANITIZE_STRING),
        ];

        $year = $this->db->queryOne("SELECT getYear('{$filters->fecha_inicial}')");

        $sql = "SELECT *
                FROM (
                    SELECT 
                        label_x,
                        value,
                        0 AS index_y, 
                        'MUESTREO MANOS' AS 'name'
                    FROM (
                        {$this->getSemanaVar('MUESTREO', $year, $filters->finca)}
                    ) tbl
                    UNION ALL
                    SELECT 
                        label_x,
                        value,
                        0 AS index_y, 
                        'MUESTREO CALIB 2DA' AS 'name'
                    FROM (
                        {$this->getSemanaVar('MUESTREO CALIB 2DA', $year, $filters->finca)}
                    ) tbl
                    UNION ALL
                    SELECT 
                        label_x,
                        value,
                        0 AS index_y, 
                        'MUESTREO CALIB ULT' AS 'name'
                    FROM (
                        {$this->getSemanaVar('MUESTREO CALIB ULT', $year, $filters->finca)}
                    ) tbl
                    UNION ALL
                    SELECT 
                        label_x,
                        value,
                        0 AS index_y, 
                        'MUESTREO DEDOS' AS 'name'
                    FROM (
                        {$this->getSemanaVar('MUESTREO DEDOS', $year, $filters->finca)}
                    ) tbl
                ) tbl
                ORDER BY label_x";
        $data_chart = $this->db->queryAll($sql);
        $semanas = array();
        foreach ($data_chart as $key => $row)
        {
            $row = (object) $row;
            $semanas[$key] = (int) $row->label_x;
        }
        array_multisort($semanas, SORT_ASC, $data_chart);

        $groups = [
			[
				"name" => '',
				"type" => 'line',
				'format' => '%'
			]
        ];
        $response->data = $this->grafica_z($data_chart, $groups, null);
        return $response;
    }

    public function pesoRacimo(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $filters = (object)[
            "finca" => getValueFrom($postdata , "finca" , "" , FILTER_SANITIZE_STRING),
            "fecha" => getValueFrom($postdata , "fecha_inicial" , "" , FILTER_SANITIZE_STRING),
            "grupo" => getValueFrom($postdata , "grupo" , "" , FILTER_SANITIZE_STRING),
            "id_lote" => getValueFrom($postdata , "id_lote_adicional" , 0 , FILTER_SANITIZE_STRING),
        ];

        $sWhere = "";
        if($filters->id_lote > 0){
            $sWhere .= " AND id_lote = {$filters->id_lote}";
        }

        // GRAFICA DIA

        $sql = "SELECT
                    IF(grupo = 0,
                        0,
                        CONCAT('[', (grupo*{$filters->grupo})-{$filters->grupo}, '.01, ', grupo*{$filters->grupo}, ']')
                    ) label_x,
                    value,
                    0 AS index_y,
                    'PESO PROM POR RAC' AS name
                FROM (
                    SELECT 
                        COUNT(1) value,
                        FLOOR(value/{$filters->grupo})+1 grupo
                    FROM (
                        SELECT 
                            peso_{$this->unidad} AS 'value'
                        FROM racimo_web
                        WHERE fecha = '{$filters->fecha}' AND id_finca = {$filters->finca} {$sWhere}
                        ORDER BY peso_{$this->unidad}
                    ) tbl
                    GROUP BY grupo
                    ORDER BY grupo
                ) tbl";
                
        $data_chart = $this->db->queryAll($sql);
        $groups = [
			[
				"name" => 'RAC',
				"type" => 'bar',
				'format' => ''
			]
        ];
        $response->dia = $this->grafica_z($data_chart, $groups, null, ['bar'], "PESO");

        // PESO RACIMOS VS MANOS POR RACIMO

        $sql = "SELECT 
                    manos AS index_1,
                    peso_{$this->unidad} AS index_0
                FROM racimo_web
                WHERE fecha = '{$filters->fecha}' AND id_finca = {$filters->finca} AND manos > 0 {$sWhere}
                ORDER BY manos";
        $data_chart = $this->db->queryAll($sql);
        $response->vs_manos = $this->grafica_scatter($data_chart, "MANOS", "PESO");

        return $response;
    }

    public function pesoCalibre(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $filters = (object)[
            "finca" => getValueFrom($postdata , "finca" , "" , FILTER_SANITIZE_STRING),
            "fecha" => getValueFrom($postdata , "fecha_inicial" , "" , FILTER_SANITIZE_STRING),
            "grupo" => getValueFrom($postdata , "grupo" , "" , FILTER_SANITIZE_STRING),
            "id_lote" => getValueFrom($postdata , "id_lote_adicional" , 0 , FILTER_SANITIZE_STRING),
        ];

        $sWhere = "";
        if($filters->id_lote > 0){
            $sWhere .= " AND id_lote = {$filters->id_lote}";
        }

        $year = $this->db->queryOne("SELECT getYear('{$filters->fecha}')");

        $sql = "SELECT 
                    label_x,
                    value,
                    0 AS index_y, 
                    name
                FROM (
                    SELECT 
                        semana AS label_x,
                        ROUND(AVG(peso_{$this->unidad}/calibre_segunda), 2) AS 'value',
                        'PESO PROM CALIB. 2DA' AS 'name'
                    FROM racimo_web
                    WHERE anio = {$year} AND id_finca = {$filters->finca} AND calibre_segunda > 0 AND peso_{$this->unidad} > 0 {$sWhere}
                    GROUP BY semana
                    UNION ALL
                    SELECT 
                        semana AS label_x,
                        ROUND(AVG(peso_{$this->unidad}/calibre_ultima), 2) AS 'value',
                        'PESO PROM CALIB. ULT' AS 'name'
                    FROM racimo_web
                    WHERE anio = {$year} AND id_finca = {$filters->finca} AND calibre_ultima > 0 AND peso_{$this->unidad} > 0 {$sWhere}
                    GROUP BY semana
                ) tbl
                ORDER BY label_x";
        $data_chart = $this->db->queryAll($sql);
        $semanas = array();
        foreach ($data_chart as $key => $row)
        {
            $row = (object) $row;
            $semanas[$key] = (int) $row->label_x;
        }
        array_multisort($semanas, SORT_ASC, $data_chart);

        $groups = [
			[
				"name" => 'PESO PROM POR ° CALIB',
				"type" => 'line',
				'format' => ''
			]
        ];
        $response->data = $this->grafica_z($data_chart, $groups, ((object) []), ['line', 'line'], 'SEM');

        // GRAFICA DIA

        $sql = "SELECT *
                FROM (
                    SELECT
                        CONCAT('[', (grupo*{$filters->grupo})-{$filters->grupo}, ', ', grupo*{$filters->grupo}, ']') label_x,
                        COUNT(1) AS value,
                        0 AS index_y,
                        'PESO PROM POR CALIB. 2DA' AS name
                    FROM (
                        SELECT 
                            value,
                            CEIL(value/{$filters->grupo}) grupo
                        FROM (
                            SELECT 
                                ROUND(peso_{$this->unidad}/calibre_segunda, 2) AS 'value'
                            FROM racimo_web
                            WHERE fecha = '{$filters->fecha}' AND id_finca = {$filters->finca} AND calibre_segunda > 0 AND peso_{$this->unidad} > 0 {$sWhere}
                        ) tbl
                        ORDER BY grupo
                    ) tbl
                    GROUP BY grupo
                    UNION ALL
                    SELECT
                        CONCAT('[', (grupo*{$filters->grupo})-{$filters->grupo}, ', ', grupo*{$filters->grupo}, ']') label_x,
                        COUNT(1) AS value,
                        0 AS index_y,
                        'PESO PROM POR CALIB. ULT' AS name
                    FROM (
                        SELECT 
                            value,
                            CEIL(value/{$filters->grupo}) grupo
                        FROM (
                            SELECT 
                                ROUND(peso_{$this->unidad}/calibre_ultima, 2) AS 'value'
                            FROM racimo_web
                            WHERE fecha = '{$filters->fecha}' AND id_finca = {$filters->finca} AND calibre_ultima > 0 AND peso_{$this->unidad} > 0 {$sWhere}
                        ) tbl
                        ORDER BY grupo
                    ) tbl
                    GROUP BY grupo
                ) tbl";
        $data_chart = $this->db->queryAll($sql);
        $semanas = array();
        foreach ($data_chart as $key => $row)
        {
            $row = (object) $row;
            $semanas[$key] = (int) $row->label_x;
        }
        array_multisort($semanas, SORT_ASC, $data_chart);

        $groups = [
			[
				"name" => 'RAC',
				"type" => 'bar',
				'format' => ''
			]
        ];
        $response->dia = $this->grafica_z($data_chart, $groups, ((object) []), ['bar', 'bar'], "PESO PROM");

        return $response;
    }

    public function pesoMano(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $filters = (object)[
            "finca" => getValueFrom($postdata , "finca" , "" , FILTER_SANITIZE_STRING),
            "fecha" => getValueFrom($postdata , "fecha_inicial" , "" , FILTER_SANITIZE_STRING),
            "grupo" => getValueFrom($postdata , "grupo" , 1 , FILTER_SANITIZE_STRING),
            "id_lote" => getValueFrom($postdata , "id_lote_adicional" , 0 , FILTER_SANITIZE_STRING),
        ];

        $sWhere = "";
        if($filters->id_lote > 0){
            $sWhere .= " AND id_lote = {$filters->id_lote}";
        }

        $year = $this->db->queryOne("SELECT getYear('{$filters->fecha}')");

        // GRAFICA DE TENDENCIA

        $sql = "SELECT 
                    label_x,
                    value,
                    0 AS index_y, 
                    'PESO PROM MANOS.' AS 'name'
                FROM (
                    SELECT 
                        semana AS label_x,
                        ROUND(AVG(peso_{$this->unidad}/manos), 2) AS 'value'
                    FROM racimo_web
                    WHERE anio = {$year} AND id_finca = {$filters->finca} AND manos > 0 AND peso_{$this->unidad} > 0 {$sWhere}
                    GROUP BY semana
                    ORDER BY semana
                ) tbl
                ORDER BY label_x";
        $data_chart = $this->db->queryAll($sql);
        $semanas = array();
        foreach ($data_chart as $key => $row)
        {
            $row = (object) $row;
            $semanas[$key] = (int) $row->label_x;
        }
        array_multisort($semanas, SORT_ASC, $data_chart);

        $groups = [
			[
				"name" => 'PESO PROM POR MANO',
				"type" => 'line',
				'format' => ''
			]
        ];
        $response->data = $this->grafica_z($data_chart, $groups, null, ['line'], 'SEM');

        // GRAFICA DEL DIA

        $sql = "SELECT
                    CONCAT('[', (grupo*{$filters->grupo})-{$filters->grupo}+0.1, ', ', grupo*{$filters->grupo}, ']') label_x,
                    COUNT(1) AS value,
                    0 AS index_y,
                    'PESO PROM POR MANO' AS name
                FROM (
                    SELECT 
                        value,
                        CEIL(value/{$filters->grupo}) grupo
                    FROM (
                        SELECT 
                            ROUND(peso_{$this->unidad}/manos, 2) AS 'value'
                        FROM racimo_web
                        WHERE fecha = '{$filters->fecha}' AND id_finca = {$filters->finca} AND manos > 0 AND peso_{$this->unidad} > 0 {$sWhere}
                    ) tbl
                    ORDER BY grupo
                ) tbl
                GROUP BY grupo";
        $data_chart = $this->db->queryAll($sql);

        $groups = [
			[
				"name" => 'RAC',
				"type" => 'bar',
				'format' => ''
			]
        ];
        $response->dia = $this->grafica_z($data_chart, $groups, null, ['bar'], "PESO PROM");

        return $response;
    }

    public function cantidadRacimoCalibreManos(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $filters = (object)[
            "finca" => getValueFrom($postdata , "finca" , "" , FILTER_SANITIZE_STRING),
            "fecha" => getValueFrom($postdata , "fecha_inicial" , "" , FILTER_SANITIZE_STRING),
            "grupo" => getValueFrom($postdata , "grupo" , 1 , FILTER_SANITIZE_STRING),
            "id_lote" => getValueFrom($postdata , "id_lote_adicional" , 0 , FILTER_SANITIZE_STRING),
            "edad_variables" => getValueFrom($postdata , "edad_variables" , "", FILTER_SANITIZE_STRING),
        ];

        $sWhere = "";
        if($filters->id_lote > 0){
            $sWhere .= " AND id_lote = {$filters->id_lote}";
        }

        $sql = "SELECT edad 
                FROM racimo_web 
                WHERE fecha = '{$filters->fecha}' 
                    AND (manos > 0 OR calibre_segunda > 0 OR calibre_segunda > 0 OR dedos > 0)
                    AND id_finca = '{$filters->finca}' 
                    AND edad > 0
                    $sWhere 
                GROUP BY edad";
        $response->edades = $this->db->queryAllOne($sql);

        if($filters->edad_variables != ''){
            $sWhere .= " AND edad = {$filters->edad_variables}";
        }

        // GRAFICA DEL DIA
        $sql = "SELECT
                    label_x,
                    value,
                    0 AS index_y,
                    'MANOS' AS name
                FROM (
                    SELECT manos AS label_x, COUNT(1) AS value
                    FROM racimo_web
                    WHERE fecha = '{$filters->fecha}' AND id_finca = {$filters->finca} AND manos > 0 $sWhere
                    GROUP BY manos
                    ORDER BY manos
                ) tbl";
        $data_chart = $this->db->queryAll($sql);
        $groups = [
			[
				"name" => 'RAC',
				"type" => 'bar',
				'format' => ''
			]
        ];
        $response->manos = $this->grafica_z($data_chart, $groups, null, ['bar'], "MANOS");

        // GRAFICA DEL DIA
        $sql = "SELECT
                    label_x,
                    value,
                    0 AS index_y,
                    'CALIB. 2DA' AS name
                FROM (
                    SELECT ROUND(calibre_segunda, 0) AS label_x, COUNT(1) AS value
                    FROM racimo_web
                    WHERE fecha = '{$filters->fecha}' AND id_finca = {$filters->finca} AND calibre_segunda > 0 $sWhere
                    GROUP BY ROUND(calibre_segunda, 0)
                    ORDER BY ROUND(calibre_segunda, 0)
                ) tbl";
        $data_chart = $this->db->queryAll($sql);
        $groups = [
			[
				"name" => 'RAC',
				"type" => 'bar',
				'format' => ''
			]
        ];
        $response->calibre_segunda = $this->grafica_z($data_chart, $groups, null, ['bar'], "CALIB");

        // GRAFICA DEL DIA
        $sql = "SELECT
                    label_x,
                    value,
                    0 AS index_y,
                    'CALIB. ULT' AS name
                FROM (
                    SELECT ROUND(calibre_ultima, 0) AS label_x, COUNT(1) AS value
                    FROM racimo_web
                    WHERE fecha = '{$filters->fecha}' AND id_finca = {$filters->finca} AND calibre_ultima > 0 $sWhere
                    GROUP BY ROUND(calibre_ultima, 0)
                    ORDER BY ROUND(calibre_ultima, 0)
                ) tbl";
        $data_chart = $this->db->queryAll($sql);
        $groups = [
			[
				"name" => 'RAC',
				"type" => 'bar',
				'format' => ''
			]
        ];
        $response->calibre_ultima = $this->grafica_z($data_chart, $groups, null, ['bar'], "PESO");

        return $response;
    }

    /* BEGIN CUADRE DE VIAJES */
    public function borrarViaje(){
        $response = new stdClass;
        $response->status = 400;
        $response->message = "";
        $postdata = (object)json_decode(file_get_contents("php://input"));
        
        if($postdata->id_viaje > 0){
            $response->status = 200;
            $viaje = $this->db->queryRow("SELECT fecha, numero, id_finca FROM viajes WHERE id = $postdata->id_viaje");

            $sql = "DELETE FROM viajes WHERE id = $postdata->id_viaje";
            $this->db->query($sql);

            // modificar secuencia de viajes
            #$sql = "UPDATE viajes SET numero = numero-1 WHERE fecha = '{$viaje->fecha}' AND id_finca = {$viaje->id_finca} AND numero > {$viaje->numero}";
            #$this->db->query($sql);
        }

        return $response;
    }

    public function crearFincaProceso(){
        $response = new stdClass;
        $response->status = 400;
        $postdata = (object)json_decode(file_get_contents("php://input"));

        if($postdata->fecha == ''){
            $response->message = "Fecha Incorrecta";
            return $response;
        }
        if(!$postdata->id_finca > 0){
            $response->message = "Finca Incorrecta";
            return $response;
        }

        $semana = $this->db->queryOne("SELECT getWeek('{$postdata->fecha}')");
        $anio = $this->db->queryOne("SELECT YEAR('{$postdata->fecha}')");

        if($postdata->muestreo){
            if(!$postdata->muestreo->calibre_segunda > 0){
                $postdata->muestreo->calibre_segunda = 0;
            }
            if(!$postdata->muestreo->calibre_ultima > 0){
                $postdata->muestreo->calibre_ultima = 0;
            }
            if(!$postdata->muestreo->manos > 0){
                $postdata->muestreo->manos = 0;
            }
            if(!$postdata->muestreo->dedos > 0){
                $postdata->muestreo->dedos = 0;
            }
            $sql = "INSERT INTO racimos_creados SET
                        id_finca = {$postdata->id_finca},
                        fecha = '{$postdata->fecha}',
                        calibre_segunda = '{$postdata->muestreo->calibre_segunda}',
                        calibre_ultima = '{$postdata->muestreo->calibre_ultima}',
                        manos = '{$postdata->muestreo->manos}',
                        dedos = '{$postdata->muestreo->dedos}'";
            $this->db->query($sql);
        }

        foreach($postdata->data as $index => $row){
            if($row->id_lote > 0 && $row->cintas){
                $causas_index = [];
                if($row->causas){
                    foreach($row->causas as $c){
                        for($x = 1; $x <= $c->cantidad; $x++){
                            $causas_index[] = $c->causa;
                        }
                    }
                }

                $index2 = -1;
                foreach($row->cintas as $edad => $cantidad){
                    if($edad > 0 && $cantidad > 0){
                        for($x = 1; $x <= $cantidad; $x++){
                            $index2++;
                            
                            $causa = isset($causas_index[$index2]) ? $causas_index[$index2] : 'null';
                            $peso = isset($row->peso_real) && $row->peso_real >= 0 ? $row->peso_real : $row->peso;
                            $sql = "INSERT INTO racimo_web SET
                                        origen = 'creado',
                                        id_finca = {$postdata->id_finca},
                                        anio = $anio,
                                        semana = $semana,
                                        anio_enfundado = IF(getSemanaEnfundada({$edad}, {$semana}, {$anio}) > {$semana}, $anio-1, $anio),
                                        semana_enfundada = getSemanaEnfundada({$edad}, {$semana}, {$anio}),
                                        fecha = '{$postdata->fecha}',
                                        id_lote = {$row->id_lote},
                                        unidad_peso = '{$this->session->unidad_racimos}',
                                        peso = {$peso},
                                        peso_reportes = {$peso},
                                        peso_kg = IF('{$this->session->unidad_racimos}' = 'kg', {$peso}, {$peso}*0.4536),
                                        peso_lb = IF('{$this->session->unidad_racimos}' = 'lb', {$peso}, {$peso}/0.4536),
                                        edad = {$edad},
                                        id_cinta = (SELECT id FROM cinta WHERE color = getCintaFromEdad({$edad}, {$semana}, $anio)),
                                        tipo = IF('{$causa}' = 'null', 'PROC', 'RECU'),
                                        causa = IF('{$causa}' != 'null', '{$causa}', ''),
                                        updated_at = CURRENT_TIMESTAMP";
                            $this->db->query($sql);
                        }
                    }
                }
                $response->status = 200;
            }
        }

        return $response;
    }

    public function racimosPorViaje(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $filters = (object)[
            "finca" => getValueFrom($postdata , "finca" , 1, FILTER_SANITIZE_STRING),
            "fecha" => getValueFrom($postdata , "fecha_inicial" , "" , FILTER_SANITIZE_STRING),
        ];

        $sql = "SELECT id, nombre
                FROM lotes
                WHERE idFinca = {$filters->finca}
                ORDER BY nombre+0";
        $response->lotes = $this->db->queryAll($sql);

        $response->dia_finalizado = $this->db->queryOne("SELECT IF('{$filters->fecha}' < CURRENT_DATE OR (SELECT COUNT(1) FROM produccion_jornada_finalizada WHERE fecha = '{$filters->fecha}') > 0, 1, 0)");

        $sql = "SELECT 
                    v.*,
                    MOD(v.numero, 1000) _numero,
                    (SELECT GROUP_CONCAT(DISTINCT viaje SEPARATOR ', ') FROM racimo_formulario_cabecera WHERE id_viaje = v.id) form_viaje,
                    (SELECT GROUP_CONCAT(DISTINCT lotes.nombre SEPARATOR ', ') FROM racimo_formulario_cabecera c INNER JOIN racimo_formulario_procesado ON id_racimo_cabecera = c.id INNER JOIN lotes ON id_lote = lotes.id WHERE id_viaje = v.id) form_lote,
                    (SELECT GROUP_CONCAT(DISTINCT palanca SEPARATOR ', ') FROM racimo_formulario_cabecera c INNER JOIN racimo_formulario_procesado ON id_racimo_cabecera = c.id WHERE id_viaje = v.id) form_palanca,
                    (SELECT MIN(hora) FROM racimo_formulario_cabecera WHERE id_viaje = v.id) form_hora,
                    (SELECT MAX(racimo) FROM racimo_formulario_cabecera c INNER JOIN racimo_formulario_procesado p ON id_racimo_cabecera = c.id WHERE id_viaje = v.id) form_racimos,
                    (SELECT GROUP_CONCAT(DISTINCT referencia SEPARATOR ',') FROM racimo_formulario_cabecera c INNER JOIN racimo_formulario_procesado p ON id_racimo_cabecera = c.id WHERE id_viaje = v.id) form_referencias,
                
                    (SELECT GROUP_CONCAT(DISTINCT viaje SEPARATOR ', ') FROM racimo_balanza_procesado WHERE id_viaje = v.id) blz_viaje,
                    (SELECT GROUP_CONCAT(DISTINCT lotes.nombre SEPARATOR ', ') FROM racimo_balanza_procesado INNER JOIN lotes ON id_lote = lotes.id WHERE id_viaje = v.id) blz_lote,
                    (SELECT GROUP_CONCAT(DISTINCT palanca SEPARATOR ', ') FROM racimo_balanza_procesado WHERE id_viaje = v.id) blz_palanca,
                    (SELECT MIN(hora) FROM racimo_balanza_procesado WHERE id_viaje = v.id) blz_hora,
                    (SELECT MAX(racimo) FROM racimo_balanza_procesado WHERE id_viaje = v.id) blz_racimos
                FROM viajes v
                WHERE v.`fecha` = '{$filters->fecha}' AND v.`id_finca` = {$filters->finca}
                ORDER BY _numero";
        $response->data = $this->db->queryAll($sql);

        foreach($response->data as $i => $row){
            if(count(explode(",", $row->form_palanca)) > 1) $row->form_error = true;

            $sql = "SELECT racimo as id, racimo_formulario_procesado.id as id_racimo, tipo, cinta.color as cinta, css_class cssClass, id_lote, lotes.nombre as lote
                    FROM racimo_formulario_cabecera c
                    INNER JOIN racimo_formulario_procesado ON id_racimo_cabecera = c.id
                    INNER JOIN lotes ON id_lote = lotes.id
                    LEFT JOIN cinta ON id_cinta = cinta.id
                    WHERE id_viaje = $row->id";
            $row->formulario = $this->db->queryAllIntoSpecial($sql);

            $sql = "SELECT racimo as id, racimo_balanza_procesado.id as id_racimo, tipo, cinta.color as cinta, css_class cssClass, lotes.nombre as lote
                    FROM racimo_balanza_procesado
                    LEFT JOIN cinta ON id_cinta = cinta.id
                    INNER JOIN lotes ON id_lote = lotes.id
                    WHERE id_viaje = $row->id";
            $row->balanza = $this->db->queryAllIntoSpecial($sql);

            if(!$response->dia_finalizado)
            if($i >= count($response->data)-3){
                $row->blocked = true;
            }
        }

        return $response;
    }

    public function crearRacimosViaje(){
        $response = new stdClass;
        $response->status = 400;
        $response->message = "";
        $postdata = (object)json_decode(file_get_contents("php://input"));
        
        if($postdata->id_viaje > 0){
            $rac_blz = $this->db->queryOne("SELECT COUNT(1) FROM viajes INNER JOIN racimo_balanza_procesado ON id_viaje = viajes.id WHERE viajes.id = {$postdata->id_viaje}");
            if($rac_blz == 0){
                $response->status = 200;
                $rac_form = $this->db->queryAll("SELECT c.fecha, c.id_finca, c.hora, c.viaje, f.id_lote, f.palanca, f.id_cinta, f.tipo, f.causa, f.edad, f.racimo FROM viajes INNER JOIN racimo_formulario_cabecera c ON id_viaje = viajes.id INNER JOIN racimo_formulario_procesado f ON id_racimo_cabecera = c.id WHERE viajes.id = {$postdata->id_viaje}");
                foreach($rac_form as $rac){
                    $peso = $this->db->queryOne("SELECT AVG(peso) FROM racimo_balanza_procesado WHERE fecha = '{$rac->fecha}' AND id_lote = {$rac->id_lote} AND id_finca = {$rac->id_finca}");
                    if(!$peso) $peso = $this->db->queryOne("SELECT AVG(peso) FROM racimo_balanza_procesado WHERE semana = getWeek('{$rac->fecha}') AND id_lote = {$rac->id_lote} AND id_finca = {$rac->id_finca}");
                    if(!$peso) $peso = 'NULL';

                    $sql = "INSERT INTO racimo_balanza_procesado SET
                                anio = YEAR('{$rac->fecha}'),
                                semana = getWeek('{$rac->fecha}'),
                                fecha = '{$rac->fecha}',
                                hora = '{$rac->hora}',
                                id_finca = '{$rac->id_finca}',
                                palanca = '{$rac->palanca}',
                                id_lote = '{$rac->id_lote}',
                                id_viaje = {$postdata->id_viaje},
                                id_cinta = '{$rac->id_cinta}',
                                edad = '{$rac->edad}',
                                peso = $peso,
                                tipo = '{$rac->tipo}',
                                causa = '{$rac->causa}',
                                viaje = {$rac->viaje},
                                racimo = {$rac->racimo}";
                    $this->db->query($sql);
                }
            }else{
                $response->message = "No puedes realizar esta acción";
            }
        }

        return $response;
    }

    public function borrarRacimosViaje(){
        $response = new stdClass;
        $response->status = 400;
        $response->message = "";
        $postdata = (object)json_decode(file_get_contents("php://input"));
        
        if($postdata->id_viaje > 0){
            $response->status = 200;
            $table = $postdata->tipo == 'BLZ' ? 'racimo_balanza_procesado' : 'racimo_formulario_procesado';
            foreach($postdata->racimos as $id_racimo){
                $sql = "DELETE FROM {$table} WHERE id = $id_racimo";
                $this->db->query($sql);
            }
        }

        return $response;
    }

    public function crearBalazaDeFormulario(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $response->status = 400;

        if($postdata->id_formulario > 0){
            $response->status = 200;

            $json = $this->db->queryOne("SELECT json FROM racimos_cosechados WHERE id = $postdata->id_formulario");
            $fecha = $this->db->queryOne("SELECT DATE(fecha) FROM racimos_cosechados WHERE id = $postdata->id_formulario");
            $forms = $this->db->queryAll("SELECT id, cantidad, color, lote, palanca FROM racimos_cosechados_by_color WHERE json = '{$json}'");
            $grupo_racimo = $this->db->queryOne("SELECT MAX(grupo_racimo) + 1 FROM produccion_historica WHERE id_finca = 1");

            $n = 1;
            foreach($forms as $rw){
                $rw->cantidad = (int) $rw->cantidad;
                $recus = $this->db->queryAll("SELECT causa FROM racimos_cosechados_detalle WHERE json = '{$json}' AND color_cinta = '{$rw->color}'");
                $peso_prom = (float) $this->db->queryOne("SELECT ROUND(AVG(peso), 2) FROM produccion_historica WHERE id_finca = 1 AND lote = '{$rw->lote}' AND cinta = '{$rw->color}' AND fecha = '{$fecha}'");

                for($x = 1; $x <= $rw->cantidad; $x++){
                    $type = 'PROC';
                    $causa = 'NULL';
                    if($x <= count($recus)){
                        $type = 'RECUSADO';
                        $causa = "'{$recus[$x-1]->causa}'";
                    }

                    $sql = "INSERT INTO produccion_historica SET
                                id_finca = 1,
                                finca = (SELECT nombre FROM fincas WHERE id = 1),
                                lote = '{$rw->lote}',
                                cuadrilla = '{$rw->palanca}',
                                fecha = '{$fecha}',
                                grupo_racimo = {$grupo_racimo},
                                num_racimo = $n,
                                cinta = '{$rw->color}',
                                peso = '{$peso_prom}',
                                edad = getEdadCinta(getWeek('{$fecha}'), '{$rw->color}', getYear('{$fecha}')),
                                semana = getWeek('{$fecha}'),
                                year = getYear('{$fecha}'),
                                tipo = '{$type}',
                                causa = {$causa}";
                    $this->db->query($sql);
                    $n++;
                }
            }
            $this->db->query("INSERT INTO racimo_web_viajes_relacionados SET id_finca = 1, grupo_racimo = $grupo_racimo, id_formulario = $postdata->id_formulario, lote = '{$postdata->lote}'");
        }

        return $response;
    }

    public function moverViajes(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;
        $response->status = 400;

        if($postdata->to_id_viaje > 0 && count($postdata->racimos) > 0){
            $response->status = 200;

            $table = $postdata->tipo == 'BLZ' ? 'racimo_balanza_procesado' : 'racimo_formulario_procesado';
            if($table == 'racimo_balanza_procesado'){
                $rac = $this->db->queryOne("SELECT COUNT(1) FROM {$table} WHERE id_viaje = {$postdata->to_id_viaje}");
                foreach($postdata->racimos as $i => $id_racimo){
                    $sql = "UPDATE {$table} 
                            SET 
                                id_viaje = $postdata->to_id_viaje,
                                racimo = ($rac+$i+1)
                            WHERE id = {$id_racimo}";
                    $this->db->query($sql);
                }
            }else{
                $rac = $this->db->queryOne("SELECT MAX(racimo) FROM racimo_formulario_procesado INNER JOIN racimo_formulario_cabecera c ON c.id = id_racimo_cabecera WHERE id_viaje = {$postdata->to_id_viaje}");
                $palanca = $this->db->queryOne("SELECT palanca FROM racimo_formulario_procesado INNER JOIN racimo_formulario_cabecera c ON c.id = id_racimo_cabecera WHERE id_viaje = {$postdata->to_id_viaje} LIMIT 1");
                $id_form = (int) $this->db->queryOne("SELECT id FROM racimo_formulario_cabecera WHERE id_viaje = {$postdata->to_id_viaje} LIMIT 1");

                if(!$id_form > 0){
                    $viaje = $this->db->queryRow("SELECT * FROM viajes WHERE id = $postdata->to_id_viaje");
                    $id_cabera = $this->db->queryOne("SELECT id_racimo_cabecera FROM racimo_formulario_procesado WHERE id = {$postdata->racimos[0]}");
                    $cabecera = $this->db->queryRow("SELECT * FROM racimo_formulario_cabecera WHERE id = $id_cabera");

                    $sql = "INSERT INTO racimo_formulario_cabecera SET 
                                referencia = '{$cabecera->referencia}',
                                responsable = '{$this->session->usuario}',
                                id_viaje = {$postdata->to_id_viaje}, 
                                fecha = '{$cabecera->fecha}', 
                                hora = '{$cabecera->hora}', 
                                id_finca = {$cabecera->id_finca},
                                viaje = '{$cabecera->viaje}',
                                semana = {$cabecera->semana},
                                anio = {$cabecera->anio},
                                updated_at = CURRENT_TIMESTAMP";
                    $this->db->query($sql);
                    $id_form = $this->db->getLastID();
                }

                $prev_id_form = [];
                foreach($postdata->racimos as $i => $id_racimo){
                    $id_cabecera = $this->db->queryOne("SELECT id_racimo_cabecera FROM racimo_formulario_procesado WHERE id = {$id_racimo}");
                    if(!in_array($id_cabecera, $prev_id_form)){
                        $prev_id_form[] = $id_cabecera;
                    }

                    $sql = "UPDATE racimo_formulario_procesado
                            SET 
                                id_racimo_cabecera = $id_form,
                                racimo = ($rac+$i+1)
                            WHERE id = {$id_racimo}";
                    $this->db->query($sql);

                    $sql = "UPDATE racimo_web 
                            SET
                                id_viaje = {$postdata->to_id_viaje}
                            WHERE id_racimo_formulario = {$id_racimo}";
                    $this->db->query($sql);
                }

                // MOVER RACIMOS HACIA ARRIBA 
                if(isset($prev_id_form[0])){
                    $id_viaje = $this->db->queryOne("SELECT id_viaje FROM racimo_formulario_cabecera WHERE id = {$prev_id_form[0]}");
                    $sql = "SELECT r.id
                            FROM racimo_formulario_procesado r
                            INNER JOIN racimo_formulario_cabecera c ON c.id = id_racimo_cabecera
                            WHERE id_viaje = {$id_viaje}
                            ORDER BY racimo";
                    $rac = $this->db->queryAllOne($sql);
                    foreach($rac as $i => $r){
                        $sql = "UPDATE racimo_formulario_procesado SET racimo = {$i} WHERE id = {$r}";
                        $this->db->query($sql);
                    }
                }

                foreach($prev_id_form as $id_p_form){
                    $this->eliminarFormularioVacio($id_p_form);
                }
            }
        }

        return $response;
    }

    // si un id_viaje queda vacio eliminar
    private function eliminarFormularioVacio($id_form){
        $e = (int) $this->db->queryOne("SELECT COUNT(1) FROM racimo_formulario_procesado WHERE id_racimo_cabecera = {$id_form}");
        if($e == 0){
            $this->db->query("DELETE FROM racimo_formulario_cabecera WHERE id = {$id_form}");
        }
    }

    public function moverViajesOtraFinca(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;
        $response->status = 400;

        if($postdata->to_id_viaje > 0 && $postdata->to_id_finca && count($postdata->racimos) > 0){
            $response->status = 200;

            $table = $postdata->tipo == 'BLZ' ? 'racimo_balanza_procesado' : 'racimo_formulario_procesado';
            if($table == 'racimo_balanza_procesado'){
                $rac = $this->db->queryOne("SELECT COUNT(1) FROM {$table} WHERE id_viaje = {$postdata->to_id_viaje}");
                foreach($postdata->racimos as $i => $id_racimo){

                    $lote = $this->db->queryRow("SELECT lotes.* FROM racimo_formulario_procesado INNER JOIN lotes ON id_lote = lotes.id WHERE id = {$id_racimo}");
                    $id_lote = $lote->id;
                    if($lote->idFinca != $postdata->to_id_finca){
                        $id_lote = $this->db->queryOne("SELECT id FROM lotes WHERE idFinca = {$postdata->to_id_finca} AND nombre = '{$lote->nombre}'");
                    }

                    $sql = "UPDATE {$table} 
                            SET 
                                id_viaje = $postdata->to_id_viaje,
                                racimo = ($rac+$i+1),
                                id_finca = {$postdata->to_id_finca},
                                id_lote = $id_lote
                            WHERE id = {$id_racimo}";
                    $this->db->query($sql);
                }
            }else{
                $rac = $this->db->queryOne("SELECT COUNT(1) FROM racimo_formulario_procesado INNER JOIN racimo_formulario_cabecera c ON c.id = id_racimo_cabecera WHERE id_viaje = {$postdata->to_id_viaje}");
                $palanca = $this->db->queryOne("SELECT palanca FROM racimo_formulario_procesado INNER JOIN racimo_formulario_cabecera c ON c.id = id_racimo_cabecera WHERE id_viaje = {$postdata->to_id_viaje} LIMIT 1");
                $id_form = (int) $this->db->queryOne("SELECT id FROM racimo_formulario_cabecera WHERE id_viaje = {$postdata->to_id_viaje} LIMIT 1");

                if(!$id_form > 0){
                    $viaje = $this->db->queryRow("SELECT * FROM viajes WHERE id = $postdata->to_id_viaje");
                    $id_cabera = $this->db->queryOne("SELECT id_racimo_cabecera FROM racimo_formulario_procesado WHERE id = {$postdata->racimos[0]}");
                    $cabecera = $this->db->queryRow("SELECT * FROM racimo_formulario_cabecera WHERE id = $id_cabera");

                    $sql = "INSERT INTO racimo_formulario_cabecera SET 
                                referencia = '{$cabecera->referencia}',
                                referencia = '{$this->session->usuario}',
                                id_viaje = {$postdata->to_id_viaje}, 
                                fecha = '{$cabecera->fecha}', 
                                hora = '{$cabecera->hora}', 
                                id_finca = {$postdata->to_id_finca},
                                viaje = '{$cabecera->viaje}',
                                semana = {$cabecera->semana},
                                anio = {$cabecera->anio},
                                updated_at = CURRENT_TIMESTAMP";
                    $this->db->query($sql);
                    $id_form = $this->db->getLastID();
                }

                $prev_id_form = [];
                foreach($postdata->racimos as $i => $id_racimo){
                    $id_cabecera = $this->db->queryOne("SELECT id_racimo_cabecera FROM racimo_formulario_procesado WHERE id = {$id_racimo}");
                    if(!in_array($id_cabecera, $prev_id_form)){
                        $prev_id_form[] = $id_cabecera;
                    }

                    $lote = $this->db->queryRow("SELECT lotes.* FROM racimo_formulario_procesado INNER JOIN lotes ON id_lote = lotes.id WHERE id = {$id_racimo}");
                    $id_lote = $lote->id;
                    if((int)$lote->idFinca != (int)$postdata->to_id_finca){
                        $id_lote = $this->db->queryOne("SELECT id FROM lotes WHERE idFinca = {$postdata->to_id_finca} AND nombre = '{$lote->nombre}'");
                    }

                    $sql = "UPDATE racimo_formulario_procesado
                            SET 
                                id_racimo_cabecera = $id_form,
                                racimo = ($rac+$i+1),
                                id_finca = {$postdata->to_id_finca},
                                id_lote = $id_lote
                            WHERE id = {$id_racimo}";
                    $this->db->query($sql);

                    $sql = "UPDATE racimo_web
                            SET 
                                id_finca = {$postdata->to_id_finca},
                                updated_at = CURRENT_TIMESTAMP
                            WHERE id_racimo_formulario = {$id_racimo}";
                    $this->db->query($sql);
                }

                // MOVER RACIMOS HACIA ARRIBA 
                if(isset($prev_id_form[0])){
                    $id_viaje = $this->db->queryOne("SELECT id_viaje FROM racimo_formulario_cabecera WHERE id = {$prev_id_form[0]}");
                    $sql = "SELECT r.id
                            FROM racimo_formulario_procesado r
                            INNER JOIN racimo_formulario_cabecera c ON c.id = id_racimo_cabecera
                            WHERE c.id = {$id_viaje}
                            ORDER BY racimo";
                    $rac = $this->db->queryAllOne($sql);
                    foreach($rac as $i => $r){
                        $sql = "UPDATE racimo_formulario_procesado SET racimo = {$i} WHERE id = {$r}";
                        $this->db->query($sql);
                    }
                }

                foreach($prev_id_form as $id_p_form){
                    $this->eliminarFormularioVacio($id_p_form);
                }
            }
        }

        return $response;
    }

    public function moverCrearViajes(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;
        $response->status = 400;

        if($postdata->id_finca > 0 && $postdata->fecha != '' && $postdata->numero > 0 && count($postdata->racimos) > 0){
            $response->status = 200;

            $e = (int) $this->db->queryOne("SELECT COUNT(1) e FROM viajes WHERE fecha = '{$postdata->fecha}' AND numero = {$postdata->numero} AND id_finca = {$postdata->id_finca}");
            if($e == 0){
                // CREAR
                $this->db->query("INSERT INTO viajes SET fecha = '{$postdata->fecha}', numero = {$postdata->numero}+1000, id_finca = {$postdata->id_finca}");
                $id_viaje_creado = $this->db->getLastID();

                // mover racimos

                $table = $postdata->tipo == 'BLZ' ? 'racimo_balanza_procesado' : 'racimo_formulario_procesado';
                if($table == 'racimo_balanza_procesado'){
                    foreach($postdata->racimos as $i => $id_racimo){
                        $sql = "UPDATE {$table} SET id_viaje = $id_viaje_creado WHERE id = $id_racimo";
                        $this->db->query($sql);
                    }
                }
                else{
                    $sql = "INSERT INTO racimo_formulario_cabecera SET
                                fecha = '{$postdata->fecha}',
                                semana = getWeek('{$postdata->fecha}'),
                                anio = YEAR('{$postdata->fecha}'),
                                id_finca = $postdata->id_finca,
                                id_viaje = $id_viaje_creado,
                                viaje = $postdata->numero";
                    $this->db->query($sql);
                    $id_form = $this->db->getLastID();

                    foreach($postdata->racimos as $i => $id_racimo){
                        $sql = "UPDATE {$table} SET id_racimo_cabecera = $id_form, racimo = ($i+1) WHERE id = $id_racimo";
                        $this->db->query($sql);
                    }
                }
            }
            else{
                $this->db->query("INSERT INTO viajes SET fecha = '{$postdata->fecha}', numero = {$postdata->numero}+1000, id_finca = {$postdata->id_finca}");
                $id_viaje_creado = $this->db->getLastID();

                // CREAR Y MOVER HACIA ABAJO
                $sql = "SELECT id, numero, EXISTS (SELECT * FROM viajes WHERE fecha = v.fecha AND id_finca = v.id_finca AND numero = v.numero-1) e
                        FROM viajes v
                        WHERE fecha = '{$postdata->fecha}' AND id_finca = {$postdata->id_finca} AND numero >= {$postdata->numero} AND id != $id_viaje_creado
                        ORDER BY numero";
                $viajes = $this->db->queryAll($sql);
                $vv = [];
                foreach($viajes as $v){
                    if($v->e == 0) break;
                    $vv[] = $v->id;
                }
                $vv = implode(",", $vv);

                #$sql = "UPDATE viajes SET numero = numero+1 WHERE id IN ({$vv})";
                #$this->db->query($sql);

                // mover racimos

                $table = $postdata->tipo == 'BLZ' ? 'racimo_balanza_procesado' : 'racimo_formulario_procesado';
                if($table == 'racimo_balanza_procesado'){
                    foreach($postdata->racimos as $i => $id_racimo){
                        $sql = "UPDATE {$table} SET id_viaje = $id_viaje_creado WHERE id = $id_racimo";
                        $this->db->query($sql);
                    }
                }
                else{
                    $sql = "INSERT INTO racimo_formulario_cabecera SET
                                fecha = '{$postdata->fecha}',
                                semana = getWeek('{$postdata->fecha}'),
                                anio = YEAR('{$postdata->fecha}'),
                                id_finca = $postdata->id_finca,
                                id_viaje = $id_viaje_creado,
                                viaje = $postdata->numero";
                    $this->db->query($sql);
                    $id_form = $this->db->getLastID();

                    foreach($postdata->racimos as $i => $id_racimo){
                        $sql = "UPDATE {$table} SET id_racimo_cabecera = $id_form, racimo = ($i+1) WHERE id = $id_racimo";
                        $this->db->query($sql);
                    }
                }
            }
        }

        return $response;
    }

    public function cambiarPalanca(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;
        $response->status = 400;

        if($postdata->palanca != '' && $postdata->id_viaje > 0){
            $response->status = 200;

            $sql = "UPDATE racimo_balanza_procesado SET palanca = '{$postdata->palanca}' WHERE id_viaje = {$postdata->id_viaje}";
            $this->db->query($sql);

            $sql = "UPDATE racimo_formulario_procesado INNER JOIN racimo_formulario_cabecera c ON c.id = id_racimo_cabecera SET palanca = '{$postdata->palanca}' WHERE id_viaje = {$postdata->id_viaje}";
            $this->db->query($sql);

            $sql = "UPDATE racimo_web SET palanca = '{$postdata->palanca}', updated_at = CURRENT_TIMESTAMP WHERE id_viaje = {$postdata->id_viaje}";
            $this->db->query($sql);
        }

        return $response;
    }

    public function cambiarLote(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;
        $response->status = 400;

        if($postdata->lote != '' && $postdata->id_viaje > 0){
            $response->status = 200;

            $sql = "UPDATE racimo_balanza_procesado SET id_lote = '{$postdata->lote}' WHERE id_viaje = {$postdata->id_viaje}";
            $this->db->query($sql);

            $sql = "UPDATE racimo_formulario_procesado INNER JOIN racimo_formulario_cabecera c ON c.id = id_racimo_cabecera SET id_lote = '{$postdata->lote}' WHERE id_viaje = {$postdata->id_viaje}";
            $this->db->query($sql);

            $sql = "UPDATE racimo_web SET id_lote = '{$postdata->lote}', updated_at = CURRENT_TIMESTAMP WHERE id_viaje = {$postdata->id_viaje}";
            $this->db->query($sql);
        }

        return $response;
    }

    public function getViajesFinca(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;
        $response->status = 400;
        $response->data = [];

        if($postdata->fecha != '' && $postdata->id_finca > 0){
            $sql = "SELECT *, MOD(numero, 1000) _numero
                    FROM viajes
                    WHERE fecha = '{$postdata->fecha}' AND id_finca = {$postdata->id_finca}";
            $response->data = $this->db->queryAll($sql);
        }

        return $response;
    }

    public function saveEditRacimo(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;
        $response->status = 400;

        if(isset($postdata->num_racimo) && isset($postdata->racimo) && isset($postdata->viaje)){
            F($postdata);
            if(!$postdata->racimo->cinta != ''){
                $response->message = "Colocar cinta";
            }
            else if(!$postdata->racimo->id_lote > 0){
                $response->message = "Colocar lote";
            }
            else if(!$postdata->racimo->tipo != ''){
                $response->message = "Colocar tipo";
            }else{
                $response->status = 200;

                if(isset($postdata->racimo->id_racimo) && $postdata->racimo->id_racimo > 0){
                    $sql = "UPDATE racimo_formulario_procesado SET
                                id_lote = {$postdata->racimo->id_lote},
                                id_cinta = (SELECT id FROM cinta WHERE color = '{$postdata->racimo->cinta}'),
                                tipo = '{$postdata->racimo->tipo}'
                            WHERE id = {$postdata->racimo->id_racimo}";
                    $this->db->query($sql);
                }else{
                    $id_cabecera = (int) $this->db->queryOne("SELECT id FROM racimo_formulario_cabecera WHERE id_viaje = {$postdata->viaje->id} LIMIT 1");
                    if(!$id_cabecera > 0){
                        $sql = "INSERT INTO racimo_formulario_procesado SET
                                    fecha = '{$postdata->viaje->fecha}',
                                    semana = getWeek('{$postdata->viaje->fecha}'),
                                    anio = YEAR('{$postdata->viaje->fecha}'),
                                    id_finca = {$postdata->viaje->id_finca},
                                    id_viaje = {$postdata->viaje->id},
                                    viaje = '{$postdata->viaje->numero}'";
                        $this->db->query($sql);
                        $id_cabecera = $this->db->getLastID();
                    }
                    $sql = "INSERT INTO racimo_formulario_procesado SET
                                id_racimo_cabecera = $id_cabecera,
                                racimo = '{$postdata->num_racimo}',
                                id_finca = {$postdata->viaje->id_finca},
                                id_lote = {$postdata->racimo->id_lote},
                                id_cinta = (SELECT id FROM cinta WHERE color = '{$postdata->racimo->cinta}'),
                                edad = getEdadCinta(getWeek('{$postdata->viaje->fecha}'), '{$postdata->racimo->cinta}', YEAR('{$postdata->viaje->fecha}')),
                                tipo = '{$postdata->racimo->tipo}'";
                    $this->db->query($sql);
                }
            }
        }

        return $response;
    }

    public function desbloquearViajes(){
        $response = new stdClass;
        $response->status = 400;
        $postdata = (object)json_decode(file_get_contents("php://input"));
        if($postdata->fecha != ''){
            $sql = "INSERT INTO produccion_jornada_finalizada SET fecha = '{$postdata->fecha}'";
            $this->db->query($sql);
            $response->status = 200;
        }
        
        return $response;
    }
    /* END CUADRE DE VIAJES */

    private function grafica_z($data = [], $group_y = [], $selected = [], $types = [], $xAxisName = ""){
		$options = [];
		$options["tooltip"] = [
			"trigger" => 'axis',
			"axisPointer" => [
				"type" => 'cross',
				"crossStyle" => [
					"color" => '#999'
				]
			]
		];
		$options["toolbox"] = [
			"feature" => [
				"dataView" => [
					"show" => true,
					"readOnly" => false
				],
				"magicType" => [
					"show" => true,
					"type" => ['line', 'bar']
				],
				"restore" => [
					"show" => true
				],
				"saveAsImage" => [
					"show" => true
				]
			]
		];
		$options["legend"]["data"] = [];
		$options["legend"]["bottom"] = "0%";
        $options["legend"]["left"] = "center";
        $options["legend"]["selected"] = $selected != null ? $selected : true;
		$options["xAxis"] = [
			[
                "name" => $xAxisName,
				"type" => 'category',
				"data" => [],
				"axisPointer" => [
					"type" => 'shadow'
				]
			]
		];
		/*
			[
				type => 'value',
				name => {String},
				min => 0,
				max => 200,
				interval => 5,
				axisLabel => [
					formatter => {value} KG
				]
			]
		*/
		$options["yAxis"] = [];
		/*
			[
				name => {String},
				type => 'line',
				data => [
					{double}, {double}, {double}
				]
			]
		*/
		$options["series"] = [];

		$maxs = [];
		$mins = [];
		$prepare_data = [];
		$_x = [];
		$_names = [];
		$_namess = [];
		foreach($data as $d){
			$d = (object) $d;
			if(!isset($maxs[$d->index_y])) if($d->value > 0)
				$maxs[$d->index_y] = $d->value;
			if($d->value > $maxs[$d->index_y]) if($d->value > 0)
				$maxs[$d->index_y] = $d->value;

			if(!isset($mins[$d->index_y])) if($d->value > 0)
				$mins[$d->index_y] = $d->value;
			if($d->value < $mins[$d->index_y]) if($d->value > 0)
				$mins[$d->index_y] = $d->value;

			if(!in_array($d->label_x, $_x)){
				$_x[] = $d->label_x;
			}
			if(!in_array($d->name, $_namess)){
				$_namess[] = $d->name;
				 
				$n = ["name" => $d->name, "group" => $d->index_y];
				if(isset($d->line)){
					$n["line"] = $d->line;
				}
				$_names[] = $n;
			}
			$prepare_data[$d->label_x][$d->name] = $d->value;
        }

		foreach($group_y as $key => $col){
			$col = (object) $col;
			$options["yAxis"][] = [
				'type' => 'value',
				'name' => $col->name,
				//'max' => ($key == 1) ? $maxs[$key] + ($maxs[$key] - $mins[$key]) * .05 : null,
                //'min' => ($key == 1) ? $mins[$key] - ($maxs[$key] - $mins[$key]) * .05 : null,
                'max' => null,
                'min' => 'dataMin',
				'axisLabel' => [
					'formatter' => "{value} $col->format"
				]
			];
		}

		foreach($_x as $row){
			$options["xAxis"][0]["data"][] = $row;
        }

		foreach($_names as $i => $name){
			$name = (object) $name;

			if(!in_array($name->name, $options["legend"]["data"]))
				$options["legend"]["data"][] = $name->name;

			$serie = [
				"name" => $name->name,
				"type" => isset($types[$i]) ? $types[$i] : 'line',
				"connectNulls" => true,
                "data" => [],
                "itemStyle" => [
                    "normal" => [
                        "barBorderRadius" => "0",
                        "barBorderWidth" => "6",
                        "label" => [
                            "show" => true
                        ]
                    ]
                ]
			];
			if($name->group > 0)
				$serie["yAxisIndex"] = $name->group;

			if(isset($name->line)){
				$serie["itemStyle"]["normal"]["lineStyle"]["width"] = 5;
			}

			foreach($_x as $row){
				$val = 0;
				if(isset($prepare_data[$row][$name->name]))
					$val = $prepare_data[$row][$name->name];

				if($val != null)
					$serie["data"][] = $val;
				else
					$serie["data"][] = null;
			}
			$options["series"][] = $serie;
		}

		return $options;
    }
    
    private function grafica_scatter($data = [], $yAxisName = "", $xAxisName = ""){
        $response = new stdClass;
        $response->yAxis = [
            "name" => $yAxisName,
            "min" => 'dataMin'
        ];
        $response->xAxis = [
            "name" => $xAxisName,
            "min" => 'dataMin'
        ];
        $response->toolbox = [
            "feature" => [
                "dataView" => ["show" => true, "readOnly" => false]
            ]
        ];

        $_data = [];
        foreach($data as $r){
            $_data[] = [$r->index_0, $r->index_1];
        }

        $response->series = [
            [
                "data" => $_data,
                "type" => 'scatter'
            ]
        ];

        return $response;
    }
}
