<?php defined('PHRAPI') or die("Direct access not allowed!");

class ReporteProduccion {
	public $name;
	private $db;
	private $config;

	public function __construct(){
		$this->config = $GLOBALS['config'];
		$this->session = Session::getInstance();
        $this->db = DB::getInstance($this->session->agent_user);
        $this->postdata = (object)json_decode(file_get_contents("php://input"));
    }

    public function last(){
        $response = new stdClass;
        $response->years = $this->db->queryAllOne("SELECT anio FROM racimo_web WHERE edad > 0 AND peso_kg > 0 GROUP BY anio ORDER BY anio");
        $response->sectores = $this->db->queryAllOne("SELECT sector FROM fincas WHERE sector != '' GROUP BY sector");
        $lastYear = $response->years[count($response->years)-1];
        $response->last_year = $lastYear;
        $response->weeks = $this->db->queryAllOne("SELECT semana FROM racimo_web WHERE edad > 0 AND peso_kg > 0 AND anio = $lastYear GROUP BY semana ORDER BY semana");
        $response->last_week = $this->db->queryOne("SELECT MAX(semana) FROM racimo_web WHERE anio = $lastYear");
        $response->fincas = $this->db->queryAllSpecial("SELECT id, nombre as label FROM fincas WHERE status_produccion = 'Activo'");
        return $response;
    }
    
    public function getWeeks(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));

        $response->semanas = $this->db->queryAllOne("SELECT semana FROM racimo_web WHERE edad > 0 AND peso_kg > 0 AND anio = $postdata->year GROUP BY semana");
        return $response;
    }

	public function main(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));

        $sWhere = "";
        $sWhereCa = "";
        if(isset($postdata->fecha_inicial) && isset($postdata->fecha_final) && $postdata->fecha_inicial && $postdata->fecha_final){
            $sWhere .= " AND fecha BETWEEN '{$postdata->fecha_inicial}' AND '{$postdata->fecha_final}'";
        }
        if($postdata->year != '' && $postdata->semana != ''){
            $sWhere .= " AND anio = $postdata->year AND semana = $postdata->semana";
            $sWhereCa .= " AND year = $postdata->year AND semana = $postdata->semana";
        }
        if($postdata->id_finca > 0){
            $sWhere .= " AND id_finca = $postdata->id_finca";
            $sWhereCa .= " AND id_finca = $postdata->id_finca";
        }
        /*if($postdata->sector != ''){
            $sWhere .= " AND sector = '{$postdata->sector}'";
        }*/

        $response->errores = $this->detectErrores();

        $sql = "SELECT id_lote, lotes.nombre lote, COUNT(1) AS cosechados, SUM(peso_kg) AS peso, SUM(IF(tipo = 'RECU', 1, 0)) AS recusados, SUM(IF(tipo = 'PROC', 1, 0)) AS procesados
                FROM racimo_web
                INNER JOIN lotes ON id_lote = lotes.id
                WHERE 1=1 $sWhere
                GROUP BY lotes.nombre
                ORDER BY CAST(lotes.nombre AS DECIMAL), lotes.nombre";
        $data = $this->db->queryAll($sql);

        $peso_cajas = $this->db->queryOne("SELECT SUM(valor) FROM produccion_resumen_variables_dia WHERE variable = 'PESO CAJAS (KG)' $sWhere");
        $peso_racimos_cosechados = $this->db->queryOne("SELECT SUM(valor) FROM produccion_resumen_variables_dia WHERE variable = 'PESO RACIMOS (KG)' $sWhere");
        $peso_racimos_procesados = $this->db->queryOne("SELECT SUM(peso_kg) FROM racimo_web WHERE tipo = 'PROC' $sWhere");
        $total_merma_lb_porc = ($peso_racimos_procesados - $peso_cajas);

        $response->totales = [
            "peso_cajas" => $peso_cajas,
            "peso_racimos_procesados" => $peso_racimos_procesados,
            "peso_racimos_cosechados" => $peso_racimos_cosechados,
            "merma_proc_lb" => $total_merma_lb_porc,
            "total_peso_racimos_procesados" => 0,
            "total_racimos_cosechados" => 0,
            "total_racimos_procesados" => 0,
            "total_racimos_recusados" => 0,
            "total_convertidas" => 0
        ];

        $response->edades = [];
        $response->cintas = [];

        foreach($data as $row){
            // EDADES
            $edades = $this->getEdades($row->id_lote);
            foreach($edades as $ed){
                if(!in_array($ed, $response->edades)) $response->edades[] = (int) $ed;
                $row->{"edad_{$ed}"} = $this->db->queryOne("SELECT 
                        COUNT(1)
                    FROM racimo_web 
                    WHERE id_lote = '{$row->id_lote}' AND edad = '{$ed}' $sWhere");

                if(!isset($response->totales["edad_{$ed}"])) $response->totales["edad_{$ed}"] = 0;
                $response->totales["edad_{$ed}"] += $row->{"edad_{$ed}"};

            }
            //$row->hectareas = $this->db->queryOne("SELECT IFNULL(SUM(area), 1) FROM lotes WHERE id = {$row->id_lote}");
            $row->hectareas = $this->db->queryOne("SELECT IFNULL(getHectareasLote({$row->id_lote}, {$postdata->year}, {$postdata->semana}), 1)");

            // COLUMNAS DE CAJAS (CALCULOS)
            $row->peso_prom_racimo = (float) $this->db->queryOne("SELECT AVG(peso_kg) FROM racimo_web WHERE id_lote = '{$row->id_lote}' $sWhere");
            $row->peso_racimos_cosechados = (float) $this->db->queryOne("SELECT SUM(peso_kg) FROM racimo_web WHERE id_lote = '{$row->id_lote}' $sWhere");
            $row->peso_racimos_recusados = (float) $this->db->queryOne("SELECT SUM(peso_kg) FROM racimo_web WHERE id_lote = '{$row->id_lote}' AND tipo = 'RECU' $sWhere");
            $row->peso_racimos_procesados = (float) $this->db->queryOne("SELECT SUM(peso_kg) FROM racimo_web WHERE id_lote = '{$row->id_lote}' AND tipo = 'PROC' $sWhere");

            $row->merma_lb_cosechada = $row->peso_racimos_cosechados - $peso_cajas;
            $row->merma_lb_procesada = ($row->peso_racimos_cosechados - $row->peso_racimos_recusados) - $peso_cajas;
            $row->porcentaje_distribucion_merma = ($row->peso_racimos_procesados / $peso_racimos_procesados) * 100;
            $row->merma_lb_proc = (($row->porcentaje_distribucion_merma/100) * $total_merma_lb_porc);
            $row->merma_lb_cosec = ($row->merma_lb_proc + $row->peso_racimos_recusados);
            $row->convertidas = ($row->peso_racimos_procesados - $row->merma_lb_proc) / (41.5 * 0.4536);
            $row->merma_cortada = ($row->merma_lb_cosec/$row->peso_racimos_cosechados*100);
            $row->merma_procesada = ($row->merma_lb_proc/($row->peso_racimos_procesados?$row->peso_racimos_procesados:1)*100);
            $row->cajas_lb = $row->peso_racimos_procesados - $row->merma_lb_proc;
            $row->cajas_ha = round($row->convertidas / $row->hectareas, 2);
            $row->ratio_cortado = round($row->convertidas / $row->cosechados, 2);
            $row->ratio_procesado = round($row->convertidas / ($row->procesados?$row->procesados:1), 2);
            $row->cajas_ha_proy = $row->cajas_ha * 52;

            // TOTALES
            $response->totales["total_racimos_cosechados"] += $row->cosechados;
            $response->totales["total_racimos_procesados"] += $row->procesados;
            $response->totales["total_racimos_recusados"] += $row->recusados;
            $response->totales["total_convertidas"] += $row->convertidas;
            $response->totales["total_peso_racimos_procesados"] += $row->peso_racimos_procesados;
        }

        $response->data = $data;
        sort($response->edades, SORT_NUMERIC);
        foreach($response->edades as $ed){
            $response->cintas[$ed] = $this->db->queryOne("SELECT class FROM produccion_colores WHERE color = getCintaFromEdad({$ed}, $postdata->semana, $postdata->year)");
        }

        $response->totales_general = $this->db->queryAll("SELECT variable, valor FROM produccion_resumen_semana WHERE id_finca = {$postdata->id_finca} AND anio = {$postdata->year} AND semana = {$postdata->semana}");
        $response->totales_general[] = [
            "variable" => 'CAJAS CONV / HA', 
            "valor" => $this->db->queryOne("SELECT sem_{$postdata->semana} FROM produccion_resumen_tabla WHERE anio = {$postdata->year} AND id_finca = {$postdata->id_finca} AND variable = 'CONV/HA'")
        ];
        return $response;
    }

    public function muestreo(){
        $response = new stdClass;
        $response->edades = [];

        $sWhere = "";
        $sWhereCa = "";

        if($this->postdata->year != '' && $this->postdata->semana != ''){
            $sWhere .= " AND anio = {$this->postdata->year} AND semana = {$this->postdata->semana}";
            $sWhereCa .= " AND year = {$this->postdata->year} AND semana = {$this->postdata->semana}";
        }
        if($this->postdata->id_finca > 0){
            $sWhere .= " AND id_finca = {$this->postdata->id_finca}";
            $sWhereCa .= " AND id_finca = {$this->postdata->id_finca}";
        }

        $sql = "SELECT 
                    id_lote, 
                    lotes.nombre lote, 
                    COUNT(1) AS cosechados, 
                    SUM(peso_kg) AS peso_racimos_cosechados, 
                    SUM(IF(tipo = 'PROC', peso_kg, 0)) peso_racimos_procesados,
                    SUM(IF(tipo = 'RECU', 1, 0)) AS recusados, 
                    SUM(IF(tipo = 'PROC', 1, 0)) AS procesados
                FROM racimo_web
                INNER JOIN lotes ON id_lote = lotes.id
                WHERE 1=1 $sWhere
                GROUP BY lotes.nombre
                ORDER BY CAST(lotes.nombre AS DECIMAL), lotes.nombre";
        $data = $this->db->queryAll($sql);

        $sum_merma_neta = 0;
        $sum_peso_racimos = (float) $this->db->queryOne("SELECT SUM(peso_kg) FROM racimo_web WHERE tipo = 'PROC' $sWhere");
        $sum_peso_cajas = (float) $this->db->queryOne("SELECT SUM(kg) FROM produccion_cajas WHERE 1=1 $sWhereCa");
        $sum_peso_merma = (float) $sum_peso_racimos - $sum_peso_cajas;
        foreach($data as $row){
            $edades = $this->getEdades($row->id_lote);
            foreach($edades as $ed){
                if(!in_array($ed, $response->edades)) $response->edades[] = (int) $ed;
                $row->{"edad_{$ed}"} = $this->db->queryOne("SELECT 
                        COUNT(1)
                    FROM racimo_web 
                    WHERE id_lote = '{$row->id_lote}' AND edad = '{$ed}' $sWhere");

                if(!isset($response->totales["edad_{$ed}"])) $response->totales["edad_{$ed}"] = 0;
                $response->totales["edad_{$ed}"] += $row->{"edad_{$ed}"};

            }

            $row->hectareas = (float) $this->db->queryOne("SELECT getHectareasLote($row->id_lote, {$this->postdata->year}, {$this->postdata->semana})");
            $row->peso_prom_racimo = (float) $this->db->queryOne("SELECT AVG(peso_kg) FROM racimo_web WHERE anio = {$this->postdata->year} AND semana = {$this->postdata->semana} AND id_lote = {$row->id_lote}");
            $row->porcentaje_tallo = $this->getPorcentajeTallo($row->id_lote);
            $row->merma_neta = $this->getMermaNeta($row->id_lote);
            $row->peso_tallo = $row->peso_racimos_cosechados * ($row->porcentaje_tallo/100);

            $sum_merma_neta += $row->merma_neta;
        }

        foreach($data as $row){
            $row->factor = $sum_merma_neta > 0 ? $row->merma_neta/$sum_merma_neta*100 : 1;
            $row->peso_merma_bruta = $sum_peso_merma*($row->factor/100);
            $row->peso_merma_neta = $row->peso_merma_bruta - $row->peso_tallo;
            $row->peso_cajas = $row->peso_racimos_procesados - $row->peso_merma_bruta;
            $row->convertidas = $row->peso_cajas / (41.5 * 0.4536);
            $row->merma_cortada = $row->peso_merma_bruta / ($row->peso_racimos_cosechados?$row->peso_racimos_cosechados:1) * 100;
            $row->merma_procesada = $row->peso_merma_bruta / ($row->peso_racimos_procesados>0?$row->peso_racimos_procesados:1) * 100;
            $row->ratio_cortado = $row->convertidas / ($row->cosechados?$row->cosechados:1);
            $row->ratio_procesado = $row->convertidas / ($row->procesados?$row->procesados:1);
            $row->cajas_ha = $row->convertidas / ($row->hectareas ? $row->hectareas : 1);
        }
        
        $response->data = $data;
        return $response;
    }

    private function getPorcentajeTallo($id_lote){
        $lote = $this->db->queryRow("SELECT * FROM lotes WHERE id = $id_lote");
        $muestras = (int) $this->db->queryOne("SELECT COUNT(1) FROM merma WHERE id_finca = $lote->idFinca AND bloque = '{$lote->nombre}' AND semana = {$this->postdata->semana} AND year = {$this->postdata->year}");
        if($muestras > 0){
            return (float) $this->db->queryOne("SELECT AVG(tallo/racimo*100) FROM merma WHERE id_finca = $lote->idFinca AND bloque = '{$lote->nombre}' AND semana = {$this->postdata->semana} AND year = {$this->postdata->year}");
        }
        return $this->getPromedioTallo($lote->idFinca);
    }

    private function getPromedioTallo($id_finca){
        $sql = "SELECT AVG(tallo/racimo*100) FROM merma WHERE id_finca = $id_finca AND semana = {$this->postdata->semana} AND year = {$this->postdata->year}";
        return (float) $this->db->queryOne($sql);
    }

    private function getMermaNeta($id_lote){
        $lote = $this->db->queryRow("SELECT * FROM lotes WHERE id = $id_lote");
        $muestras = (int) $this->db->queryOne("SELECT COUNT(1) FROM merma WHERE id_finca = $lote->idFinca AND bloque = '{$lote->nombre}' AND semana = {$this->postdata->semana} AND year = {$this->postdata->year}");
        if($muestras > 0){
            $sql = "SELECT 
                        (
                            SUM((SELECT SUM(cantidad) 
                                FROM merma_detalle
                                WHERE campo = 'Total peso Merma Neta (Kg)' AND id_merma = r.id
                            ))
                            / SUM(peso_neto) 
                            * 100
                        ) AS merma_neta
                    FROM merma r
                    WHERE porcentaje_merma > 0 AND semana = {$this->postdata->semana} AND year = {$this->postdata->year} AND id_finca = $lote->idFinca AND bloque = '{$lote->nombre}'";
            return (float) $this->db->queryOne($sql);
        }
        return $this->getMermaNetaPromedio($lote->idFinca);
    }

    private function getMermaNetaPromedio($id_finca){
        $sql = "SELECT 
                    (
                        SUM((SELECT SUM(cantidad) 
                            FROM merma_detalle
                            WHERE campo = 'Total peso Merma Neta (Kg)' AND id_merma = r.id
                        ))
                        / SUM(peso_neto) 
                        * 100
                    ) AS merma_neta
                FROM merma r
                WHERE porcentaje_merma > 0 AND semana = {$this->postdata->semana} AND year = {$this->postdata->year} AND id_finca = $id_finca";
        return (float) $this->db->queryOne($sql);
    }

    private function getEdades($id_lote){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $sWhere = "";
        if(isset($postdata->fecha_inicial) && isset($postdata->fecha_final) && $postdata->fecha_inicial && $postdata->fecha_final){
            $sWhere .= " AND fecha BETWEEN '{$postdata->fecha_inicial}' AND '{$postdata->fecha_final}'";
        }
        if($postdata->year != '' && $postdata->semana != ''){
            $sWhere .= " AND anio = $postdata->year AND semana = $postdata->semana";
        }

        $sql = "SELECT edad
                FROM racimo_web
                WHERE id_lote = '{$id_lote}' AND edad IS NOT NULL $sWhere AND edad > 0
                GROUP BY edad
                ORDER BY edad";
        return $this->db->queryAllOne($sql);
    }

    private function detectErrores(){
        $response = [];
        $sWhere = "";
        $sWhereCa = "";

        if($this->postdata->year != '' && $this->postdata->semana != ''){
            $sWhere .= " AND anio = {$this->postdata->year} AND semana = {$this->postdata->semana}";
            $sWhereCa .= " AND year = {$this->postdata->year} AND semana = {$this->postdata->semana}";
        }
        // HAY RACIMOS EN UN DIA PERO NO HAY CAJAS
        // eg. FECHA 2018-11-23 RAC 100 : CAJAS : 0
        $sql = "SELECT fecha, (SELECT COUNT(1) FROM produccion_cajas WHERE fecha = r.fecha) cajas, 'HAY COSECHA DE RACIMOS PERO NO HAY CAJAS' AS 'message'
                FROM racimo_web r
                WHERE 1=1 {$sWhere}
                GROUP BY fecha
                HAVING cajas = 0";
        $err = $this->db->queryAll($sql);
        $response = array_merge($response, $err);

        // NO HAY GUIA DE REMISION
        $sql = "SELECT fecha, id_finca, 
                    (SELECT COUNT(1) FROM produccion_cajas_guias_remision WHERE fecha = r.fecha AND id_finca = r.id_finca) guias, 
                    CONCAT('[', fincas.nombre, '] No hay guia de remisión') AS 'message'
                FROM produccion_cajas r
                INNER JOIN fincas ON id_finca = fincas.id
                WHERE 1=1 {$sWhereCa}
                GROUP BY fecha, id_finca
                HAVING guias = 0";
        $err = $this->db->queryAll($sql);
        $response = array_merge($response, $err);

        // CUADRE DE CAJA NO ES 100%
        $sql = "SELECT fecha, id_finca,
                    CONCAT('[', fincas.nombre, '] No tiene 100% de cuadre') AS 'message'
                FROM (
                    SELECT fecha, id_finca, id_marca, marca, cantidad,
                        IFNULL((SELECT SUM(cantidad) FROM produccion_cajas_pendiente_movimientos WHERE fecha = tbl.fecha AND id_finca = tbl.id_finca AND tipo = 'ASIGNADA' AND marca = tbl.marca), 0) asignadas,
                        IFNULL((SELECT SUM(cantidad) FROM produccion_cajas_pendiente_movimientos WHERE fecha = tbl.fecha AND id_finca = tbl.id_finca AND tipo = 'PENDIENTE' AND marca = tbl.marca), 0) pendientes,
                        IFNULL((SELECT SUM(cantidad) 
                            FROM produccion_cajas_guias_remision r
                            INNER JOIN produccion_cajas_guias_remision_marca d ON r.id = d.id_guia_remision
                            WHERE r.fecha = tbl.fecha AND r.id_finca = tbl.id_finca AND IF(tbl.id_marca IS NOT NULL, d.id_marca = tbl.id_marca, d.marca = tbl.marca)
                        ), 0) guias,
                        CONCAT('[', marca, '] No tiene 100% de cuadre') AS 'message'
                    FROM (
                        SELECT *,
                            (SELECT COUNT(1) FROM produccion_cajas_guias_remision WHERE fecha = r.fecha AND id_finca = r.id_finca 
                                AND IF(
                                    r.id_marca IS NULL,
                                    marca = r.marca,
                                    id_marca = r.id_marca
                                )
                            ) guias
                        FROM (
                            SELECT r.fecha, r.id_finca, r.id_marca, IF(m.nombre_alias != '', m.nombre_alias, m.nombre) marca,
                                COUNT(1) cantidad
                            FROM produccion_cajas r
                            INNER JOIN marcas_cajas m ON m.id = r.id_marca
                            WHERE 1=1 {$sWhereCa} AND id_marca IS NOT NULL
                            GROUP BY r.fecha, r.id_finca, r.id_marca
                            UNION ALL
                            SELECT r.fecha, r.id_finca, r.id_marca, r.marca,
                                COUNT(1) cantidad
                            FROM produccion_cajas r
                            WHERE 1=1 {$sWhereCa} AND id_marca IS NULL
                            GROUP BY r.fecha, r.id_finca, r.marca
                        ) r
                        HAVING guias > 0
                    ) tbl
                    HAVING cantidad != (guias + pendientes - asignadas)
                ) tbl
                INNER JOIN fincas ON id_finca = fincas.id
                GROUP BY fecha, id_finca";
        $err = $this->db->queryAll($sql);
        $response = array_merge($response, $err);

        return $response;
    }
}
