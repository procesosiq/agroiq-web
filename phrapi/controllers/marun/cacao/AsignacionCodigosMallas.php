<?php defined('PHRAPI') or die("Direct access not allowed!");

class AsignacionCodigosMallas {
	public $name;
	private $db;
	private $config;

	public function __construct(){
		$this->config = $GLOBALS['config'];
		$this->session = Session::getInstance();
        $this->maindb = DB::getInstance();
        $this->db = DB::getInstance('cacao_'.$this->session->agent_user);
    }

    public function revision(){
        $response = new stdClass;
        $response->data = $this->db->queryAll("SELECT id, fecha, hora, finca, responsable, codigo, cantidad, cosechador
            FROM cacao_asignacion_codigos_mallas");
        $response->data[] = $this->db->queryRow("SELECT '' AS id, 'TOTAL' AS fecha, '' AS hora, '' AS finca, '' AS responsable, '' AS codigo, SUM(cantidad) AS cantidad, '' AS cosechador FROM cacao_asignacion_codigos_mallas");
        return $response;
    }

    public function save(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));
        if($postdata->id > 0 && $postdata->campo != '' && $postdata->valor != ''){
            $this->db->query("UPDATE cacao_asignacion_codigos_mallas SET {$postdata->campo} = '{$postdata->valor}' WHERE id = {$postdata->id}");
            $response->status = 200;
        }else{
            $response->status = 400;
        }
        return $response;
    }
}
