<?php defined('PHRAPI') or die("Direct access not allowed!");

class BalanzaMallas {
	public $name;
	private $db;
	private $config;

	public function __construct(){
		$this->config = $GLOBALS['config'];
		$this->session = Session::getInstance();
        $this->db = DB::getInstance('cacao_'.$this->session->agent_user);
    }

    public function revision(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $formula = [
            "KG" => '',
            "LB" => '/ 0.4536',
            "QQ" => '/ 0.4536 / 100'
        ];

        $response->data = $this->db->queryAll("SELECT b.id, DATE(b.fecha) fecha, '' hora, 'SAN JOSE' finca, 
                IFNULL(b.sector, (SELECT sector FROM asignacion_area a WHERE a.fecha = b.fecha AND a.codigo = b.codigo)) sector,
                IFNULL(b.lote, (SELECT lote FROM asignacion_area a WHERE a.fecha = b.fecha AND a.codigo = b.codigo)) lote,
                b.codigo, 1 mallas, ROUND(peso {$formula[$postdata->unidad]}, 2) AS total_peso
            FROM balanza_agrosoft_cacao b
            WHERE b.fecha BETWEEN '{$postdata->fecha_inicial}' AND '{$postdata->fecha_final}'
            ORDER BY b.fecha DESC");
        $response->data[] = $this->db->queryRow("SELECT '' AS id, 'TOTAL' AS fecha, '' AS hora, '' AS finca, '' AS sector, '' AS lote, '' AS codigo, COUNT(1) mallas, ROUND(SUM(peso {$formula[$postdata->unidad]}), 2) AS total_peso
            FROM balanza_agrosoft_cacao b
            WHERE b.fecha BETWEEN '{$postdata->fecha_inicial}' AND '{$postdata->fecha_final}'");
        return $response;
    }

    public function save(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));
        if($postdata->id > 0 && $postdata->campo != '' && $postdata->valor != ''){
            $this->db->query("UPDATE balanza_agrosoft_cacao SET {$postdata->campo} = '{$postdata->valor}' WHERE id = {$postdata->id}");
            $response->status = 200;
        }else{
            $response->status = 400;
        }
        return $response;
    }
}
