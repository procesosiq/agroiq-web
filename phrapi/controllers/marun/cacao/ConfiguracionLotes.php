<?php defined('PHRAPI') or die("Direct access not allowed!");

class ConfiguracionLotes {
	public $name;
	private $db;
	private $config;

	public function __construct(){
        $this->config = $GLOBALS['config'];
        $this->session = Session::getInstance();
        $this->db = DB::getInstance('cacao_'.$this->session->agent_user);
    }

    public function index(){
        $response = new stdClass;
        $sql = "SELECT lotes.nombre, sectores.nombre as sector, (SELECT GROUP_CONCAT(nombre SEPARATOR ', ') FROM cat_sublotes WHERE id_lote = lotes.id) as sublotes
                FROM cat_lotes lotes
                LEFT JOIN cat_sectores sectores ON id_sector = sectores.id
                ORDER BY (lotes.nombre+0)";
        $response->data = $this->db->queryAll($sql);
        $response->grafica = $this->grafica();
        return $response;
    }

    private function grafica(){
        $data_chart = [];
        $sectores = $this->db->queryAll("SELECT id, nombre FROM cat_sectores ORDER BY nombre");
        foreach($sectores as $sector){
            $children_sector = [];
            $lotes = $this->db->queryAll("SELECT id, nombre FROM cat_lotes WHERE id_sector = $sector->id");
            foreach($lotes as $lote){
                $children_lote = [];
                $sublotes = $this->db->queryAll("SELECT id, nombre FROM cat_sublotes WHERE id_lote = $lote->id");
                foreach($sublotes as $sublote){
                    $children_lote[] = [
                        "name" => $sublote->nombre
                    ];
                }

                $children_sector[] = [
                    "name" => $lote->nombre,
                    "children" => $children_lote
                ];
            }

            $data_chart[] = [
                "name" => $sector->nombre,
                "children" => $children_sector
            ];
        }
        return $data_chart;
    }
}
