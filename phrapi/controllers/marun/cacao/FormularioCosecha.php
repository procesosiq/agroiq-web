<?php defined('PHRAPI') or die("Direct access not allowed!");

class FormularioCosecha {
	public $name;
	private $db;
	private $config;

	public function __construct(){
		$this->config = $GLOBALS['config'];
		$this->session = Session::getInstance();
        $this->maindb = DB::getInstance();
        $this->db = DB::getInstance('cacao_'.$this->session->agent_user);
    }

    public function revision(){
        $response = new stdClass;
        $response->data = $this->db->queryAll("SELECT *
            FROM asignacion_area");
        return $response;
    }

    public function save(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));
        if($postdata->id > 0 && $postdata->campo != '' && $postdata->valor != ''){
            $this->db->query("UPDATE asignacion_area SET {$postdata->campo} = '{$postdata->valor}' WHERE id = {$postdata->id}");
            $response->status = 200;
        }else{
            $response->status = 400;
        }
        return $response;
    }
}
