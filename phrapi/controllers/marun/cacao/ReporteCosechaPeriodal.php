<?php defined('PHRAPI') or die("Direct access not allowed!");

class ReporteCosechaPeriodal extends ReactGrafica {
	public $name;
	private $db;
	private $config;

	public function __construct(){
		$this->config = $GLOBALS['config'];
		$this->session = Session::getInstance();
        $this->db = DB::getInstance('cacao_'.$this->session->agent_user);
    }

    public function index(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $sWhere = "";
        if(isset($postdata->sector) && $postdata->sector != ''){
            $sWhere .= " AND id_sector = '{$postdata->sector}'";
        }

        $response->sectores = $this->db->queryAllSpecial("SELECT id_sector as id, sectores.nombre as label FROM resumen_cosecha_periodo_sector INNER JOIN cat_sectores sectores ON id_sector = sectores.id WHERE 1=1 GROUP BY id_sector ORDER BY sectores.nombre");
        $response->periodos = $this->db->queryAllOne("SELECT periodo FROM resumen_cosecha_periodo_sector WHERE 1=1 $sWhere GROUP BY periodo ORDER BY periodo");
        $formula = [
            "KG" => '',
            "LB" => '/ 0.4536',
            "QQ" => '/ 0.4536 / 100'
        ];

        $sql = "SELECT anio, -1 AS 'min', -1 AS 'max', 0 AS 'sum', 0 AS 'count'
                FROM resumen_cosecha_periodo_sector
                WHERE 1=1 $sWhere
                GROUP BY anio
                ORDER BY anio";
        $response->data = $this->db->queryAll($sql);
        $total = (object)["anio" => "SAN JOSE", "sum" => 0, "count" => 0, "max" => -1, "min" => -1];
        foreach($response->data as $row){
            foreach($response->periodos as $periodo){
                $row->{"periodo_$periodo"} = (float) $this->db->queryOne("SELECT ROUND(SUM(cantidad {$formula[$postdata->unidad]}), 2) FROM resumen_cosecha_periodo_sector WHERE periodo = $periodo AND anio = '{$row->anio}'");

                // MAX, MIN, AVG, SUM
                if($row->{"periodo_$periodo"} > 0){
                    if($row->max == -1 || $row->max < $row->{"periodo_$periodo"}) $row->max = $row->{"periodo_$periodo"};
                    if($row->min == -1 || $row->min > $row->{"periodo_$periodo"}) $row->min = $row->{"periodo_$periodo"};
                    $row->sum += $row->{"periodo_$periodo"};
                    $row->count++;
                    $row->avg = round($row->sum/$row->count, 2);

                    // TOTAL
                    $total->{"periodo_$periodo"} += $row->{"periodo_$periodo"};
                    if($total->max == -1 || $total->max < $total->{"periodo_$periodo"}) $total->max = $total->{"periodo_$periodo"};
                    if($total->min == -1 || $total->min > $total->{"periodo_$periodo"}) $total->min = $total->{"periodo_$periodo"};
                    $total->sum += $row->{"periodo_$periodo"};
                }else{
                    $row->{"periodo_$periodo"} = '';
                }
            }
        }
        $total->avg = round($total->sum / count($response->periodos), 2);
        $response->data[] = $total;

        // NUMBER FORMAT
        foreach($response->data as $row){
            if($row->sum > 0) $row->sum = number_format($row->sum, 2);
            if($row->avg > 0) $row->avg = number_format($row->avg, 2);
            if($row->min > 0) $row->min = number_format($row->min, 2);
            if($row->max > 0) $row->max = number_format($row->max, 2);
            foreach($response->semanas as $periodo){
                if($row->{"periodo_$periodo"} > 0) $row->{"periodo_$periodo"} = number_format($row->{"periodo_$periodo"}, 2);
                else $row->{"periodo_$periodo"} = '';
            }
        }

        return $response;
    }

    public function chart(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $sWhere = "";
        if(isset($postdata->sector) && $postdata->sector != ''){
            $sWhere .= " AND id_sector = '{$postdata->sector}'";
        }

        $formula = [
            "KG" => '',
            "LB" => '/ 0.4536',
            "QQ" => '/ 0.4536 / 100'
        ];
        $sql = "SELECT 
                    periodos.periodo AS legend, 
                    anios.anio AS label,
                    cantidad AS value,
                    TRUE AS selected
                FROM (
                    SELECT periodo
                    FROM resumen_cosecha_periodo_sector
                    WHERE 1=1 $sWhere
                    GROUP BY periodo
                ) periodos
                JOIN (
                    SELECT anio
                    FROM resumen_cosecha_periodo_sector
                    WHERE 1=1 $sWhere
                    GROUP BY anio
                    UNION ALL
                    SELECT 'SAN JOSE' AS anio
                ) anios
                LEFT JOIN (
                    SELECT anio, periodo, ROUND(SUM(cantidad {$formula[$postdata->unidad]}), 2) as cantidad
                    FROM resumen_cosecha_periodo_sector
                    WHERE 1=1 $sWhere
                    GROUP BY anio, periodo
                    UNION ALL
                    SELECT 'SAN JOSE' anio, periodo, ROUND(AVG(cantidad {$formula[$postdata->unidad]}), 2) as cantidad
                    FROM resumen_cosecha_periodo_sector
                    WHERE 1=1 $sWhere
                    GROUP BY periodo
                ) tbl ON anios.anio = tbl.anio AND periodos.periodo = tbl.periodo
                ORDER BY anios.anio, periodos.periodo";
        $data = $this->db->queryAll($sql);
        $data_chart = new stdClass;
		$data_chart->data = [];
		$data_chart->legend = [];

		foreach($data as $i => $row){
			if(!isset($data_chart->data[$row->label])){
				$data_chart->data[$row->label] = [
					"connectNulls" => true,
					"data" => [],
					"name" => $row->label,
					"type" => 'line',
					'itemStyle' => [
						"normal" => [
							"barBorderRadius" => "0",
							"barBorderWidth" => "6",
							"label" => [
								"show" => false,
								"position" => "insideTop"
							]
						]
					]
                ];
            }

            $data_chart->data[$row->label]["data"][] = $row->value;
			if(!in_array($row->legend, $data_chart->legend)){
				$data_chart->legend[] = $row->legend;
			}
        }
        $response->data = $data_chart;

        $sql = "SELECT ROUND(AVG(cantidad), 2)
                FROM (
                    SELECT AVG(cantidad {$formula[$postdata->unidad]}) AS cantidad
                    FROM resumen_cosecha_periodo_sector
                    WHERE 1=1 $sWhere
                    GROUP BY anio
                ) tbl";
        $response->data->umbral = (float) $this->db->queryOne($sql);
        return $response;
    }

    public function save(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));
        if($postdata->id > 0 && $postdata->campo != '' && $postdata->valor != ''){
            $this->db->query("UPDATE reporte_cosecha SET {$postdata->campo} = '{$postdata->valor}' WHERE id = {$postdata->id}");
            $response->status = 200;
        }else{
            $response->status = 400;
        }
        return $response;
    }
}
