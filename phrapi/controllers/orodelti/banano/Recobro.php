<?php defined('PHRAPI') or die("Direct access not allowed!");

class Recobro {
	public $name;
	private $db;
	private $config;

	public function __construct(){
        $this->config = $GLOBALS['config'];
		$this->session = Session::getInstance();
        $this->db = DB::getInstance($this->session->agent_user);
        $this->postdata = (object)json_decode(file_get_contents("php://input"));
    }

    // ENFUNDE VS RACIMOS COSECHADOS
	public function data(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));


        $sWhere = "";
        if($postdata->id_finca > 0){
            $sWhere .= " AND id_finca = '{$postdata->id_finca}'";
        }
        $response->lotes = $this->db->queryAllOne("SELECT lote FROM produccion_racimos WHERE 1=1 $sWhere GROUP BY lote ORDER BY lote");

        $sql_edades = "";
        $edades = $this->db->queryAllOne("SELECT edad FROM produccion_racimos WHERE semana_enfundada IS NOT NULL AND edad IS NOT NULL AND edad != 'N/A' GROUP BY edad ORDER BY CAST(edad AS DECIMAL)");
        foreach($edades as $e){
            if($postdata->lote != '')
                $sql_edades .= " (SELECT COUNT(1) FROM produccion_racimos WHERE edad = {$e} AND semana_enfundada = tbl.semana_enfundada AND anio_enfundado = tbl.anio_enfundado AND lote = tbl.lote $sWhere) sem_{$e}, ";
            else
                $sql_edades .= " (SELECT COUNT(1) FROM produccion_racimos WHERE edad = {$e} AND semana_enfundada = tbl.semana_enfundada AND anio_enfundado = tbl.anio_enfundado $sWhere) sem_{$e}, ";
        }
        $response->semanas_edad = $edades;

        if($postdata->lote == '')
            $response->data = $this->getDataBySemana($sql_edades);
        else
            $response->data = $this->getDataByLote($sql_edades, $postdata->lote);
        return $response;
    }

    private function getDataBySemana($sql_edades){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $sWhere = "";
        if($postdata->id_finca > 0){
            $sWhere .= " AND id_finca = '{$postdata->id_finca}'";
        }

        $sql = "SELECT
                    anio_enfundado,
                    semana_enfundada,
                    (SELECT SUM(usadas) FROM produccion_enfunde WHERE years = anio_enfundado AND semana = semana_enfundada $sWhere) AS enfunde,
                    {$sql_edades}
                    CONCAT(anio_enfundado, ' - ', semana_enfundada) AS sem_enf
                FROM (
                    SELECT anio_enfundado, semana_enfundada
                    FROM produccion_racimos
                    WHERE semana_enfundada IS NOT NULL $sWhere
                    GROUP BY anio_enfundado, semana_enfundada
                    UNION ALL
                    SELECT years, semana
                    FROM produccion_enfunde
                    WHERE semana IS NOT NULL $sWhere
                    GROUP BY years, semana
                ) tbl
                WHERE semana_enfundada > 0
                GROUP BY anio_enfundado, semana_enfundada
                ORDER BY anio_enfundado, semana_enfundada";
        $data = $this->db->queryAll($sql);
        
        foreach($data as $row){
            $row->total = 0;
            if($row->enfunde > 0) $row->enfunde = round($row->enfunde, 0);

            foreach($row as $col => $val){

                if($col != 'sem_enf')
                if(strpos($col, "sem_") !== false){
                    $row->total += (int) $val;

                    if(!$val > 0) $val = '';
                    if($val > 0) $val = round($val, 0);
                }
            }

            $row->saldo = $row->enfunde - $row->total;
            if($row->enfunde > 0 && $row->total > 0) $row->rec = round(($row->total / $row->enfunde) * 100, 2);

            $row->cinta = $this->db->queryOne("SELECT color FROM semanas_colores WHERE semana = $row->semana_enfundada AND year = $row->anio_enfundado");
            $row->class = $this->db->queryOne("SELECT class FROM produccion_colores WHERE color = '$row->cinta'");

            $row->porc_total = $row->rec;
            $row->no_recuperable = round(((100-$row->porc_total) / 100) * $row->enfunde, 0);
        }
        return $data;
    }

    private function getDataByLote($sql_edades, $lote){
        $sql = "SELECT
                    anio_enfundado,
                    semana_enfundada,
                    (SELECT SUM(usadas) FROM produccion_enfunde WHERE years = anio_enfundado AND semana = semana_enfundada AND lote = tbl.lote) AS enfunde,
                    {$sql_edades}
                    CONCAT(anio_enfundado, ' - ', semana_enfundada) AS sem_enf
                FROM (
                    SELECT anio_enfundado, semana_enfundada, lote
                    FROM produccion_racimos
                    WHERE semana_enfundada IS NOT NULL AND lote = '{$lote}'
                    GROUP BY anio_enfundado, semana_enfundada
                    UNION ALL
                    SELECT years, semana, lote
                    FROM produccion_enfunde
                    WHERE semana IS NOT NULL AND lote = '{$lote}'
                    GROUP BY years, semana
                ) tbl
                GROUP BY anio_enfundado, semana_enfundada
                ORDER BY anio_enfundado, semana_enfundada";
        $data = $this->db->queryAll($sql);
        foreach($data as $row){
            $row->total = 0;
            if($row->enfunde > 0) $row->enfunde = round($row->enfunde, 0);

            foreach($row as $col => $val){

                if($col != 'sem_enf')
                if(strpos($col, "sem_") !== false){
                    $row->total += (int) $val;

                    if(!$val > 0) $val = '';
                    if($val > 0) $val = round($val, 0);
                }
            }

            $row->saldo = $row->enfunde - $row->total;
            if($row->enfunde > 0 && $row->total > 0) $row->rec = round(($row->total / $row->enfunde) * 100, 2);

            $row->cinta = $this->db->queryOne("SELECT color FROM semanas_colores WHERE semana = $row->semana_enfundada AND year = $row->anio_enfundado");
            $row->class = $this->db->queryOne("SELECT class FROM produccion_colores WHERE color = '$row->cinta'");

            $row->porc_total = $row->rec;
            $row->no_recuperable = round(((100-$row->porc_total) / 100) * $row->enfunde, 0);
        }
        return $data;
    }

    // ENFUNDE VS PRECALIBRACION
    public function enfundePrecalibracion(){
        $response = new stdClass;
        $response->data = $this->db->queryAll("SELECT *, (total - enfunde) AS saldo, 
                ROUND((total / enfunde * 100), 2) AS rec,
                CONCAT(IF(semana + 14 > num_sem, anio + 1, anio), ' - ', IF(semana + 14 > num_sem, (semana + 14 - num_sem), semana + 14)) AS 's_proc',
                0 AS 't_sem'
            FROM(
                SELECT semanas.semana, semanas.anio, semanas.sem_enf, semanas.enfunde, data.total, colores.class, (SELECT COUNT(1) FROM semanas_colores WHERE year = semanas.anio) AS num_sem $sql_edades
                FROM (
                    SELECT semana, years AS anio, CONCAt(years, ' - ', semana) AS sem_enf, ROUND(SUM(usadas), 0) AS enfunde
                    FROM produccion_enfunde
                    GROUP BY years, semana
                    ORDER BY years, semana
                ) AS semanas
                LEFT JOIN (
                    SELECT semana, year, s.color, c.class
                    FROM semanas_colores s
                    INNER JOIN produccion_colores c ON c.color = s.color
                ) AS colores ON semanas.semana = colores.semana AND semanas.anio = colores.year
                LEFT JOIN (
                    SELECT semana, anio,
                        total 
                        $sql_edades
                    FROM recobro_data 
                ) AS data 
                ON data.anio = semanas.anio AND data.semana = semanas.semana
            ) AS tbl");
        return $response;
    }

    // PRECALIBRACION VS RACIMOS COSECHADOS
    public function precalibracionRacimos(){
        $response = new stdClass;

        return $response;
    }

    public function papiro(){
		$response = new stdClass;
		$response->status = 200;

		$sql = "SELECT *
                FROM (
                    SELECT anio_enfundado as year, semana_enfundada as week, COUNT(1) cantidad
                    FROM produccion_racimos
                    WHERE anio_enfundado > 0 AND semana_enfundada > 0 AND edad > 0 AND year = {$this->postdata->year}
                    GROUP BY anio_enfundado, semana_enfundada

                    UNION ALL
                    
                    SELECT years year, semana as week, 0 cantidad
                    FROM produccion_enfunde
                    WHERE years = {$this->postdata->year}
                    GROUP BY years, semana
                ) tbl
                GROUP BY year, week
                ORDER BY year, week";
		$response->semanas_enfunde = $this->db->queryAll($sql);
		$response->data = [];

        $total = new stdClass;
        $total->descripcion = "Total Cosec.";
		$cintas = new stdClass;
		$cintas->descripcion = "Cintas";
		$enfunde = new stdClass;
        $enfunde->descripcion = "Enfunde";
        $enfunde->total = 0;
		$sin_cinta = new stdClass;
		$sin_cinta->descripcion = "Sin cinta:";
		$caidos = new stdClass;
        $caidos->descripcion = "Caidos:";
        $caidos->total = 0;
		$saldo = new stdClass;
        $saldo->descripcion = "Saldo:";
        $saldo->total = 0;
        $fila_recobro = new stdClass;
        $fila_recobro->descripcion = "Recobro:";
        $fila_recobro->total = 0;
        $fila_recobro->sum = 0;
        $fila_recobro->count = 0;

		// FILA DE COLORES DE CINTA
		foreach($response->semanas_enfunde as $enf){
			$sql = "SELECT class as class_color, colores.color as cinta
					FROM semanas_colores semanas
					INNER JOIN produccion_colores colores ON semanas.color = colores.color
					WHERE year = {$enf->year} AND semana = {$enf->week}";
			$cintas->{"enfunde_{$enf->year}_{$enf->week}"} = $this->db->queryRow($sql);
		}
		$response->data[] = $cintas;

		// FILA DE ENFUNDE
		foreach($response->semanas_enfunde as $enf){
			$sql = "SELECT ROUND(SUM(usadas), 0) cantidad
					FROM produccion_enfunde
					WHERE years = {$enf->year} AND semana = {$enf->week}";
            $enfunde->{"enfunde_{$enf->year}_{$enf->week}"} = $this->db->queryRow($sql);
            $enfunde->total += $enfunde->{"enfunde_{$enf->year}_{$enf->week}"}->cantidad;

			if(!isset($saldo->{"enfunde_{$enf->year}_{$enf->week}"})) 
				$saldo->{"enfunde_{$enf->year}_{$enf->week}"} = (object) ["cantidad" => 0];
			$saldo->{"enfunde_{$enf->year}_{$enf->week}"}->cantidad += $enfunde->{"enfunde_{$enf->year}_{$enf->week}"}->cantidad;
		}
		$response->data[] = $enfunde;

		// FILAS DE RECOBRO
		$sql = "SELECT year, semana as week, CONCAT(year, '-', semana) as descripcion
				FROM produccion_racimos
				WHERE year > 0 AND semana > 0 AND edad > 0 AND year = {$this->postdata->year}
				GROUP BY year, semana
				ORDER BY year, semana";
		$data = $this->db->queryAll($sql);
		
		foreach($data as $recobro){
            $recobro->total = 0;

			foreach($response->semanas_enfunde as $enf){
				$sql = "SELECT COUNT(1) cantidad, edad
						FROM produccion_racimos 
						WHERE year = {$recobro->year} AND semana = {$recobro->week} AND anio_enfundado = {$enf->year} AND semana_enfundada = {$enf->week}";
                $rec = $this->db->queryRow($sql);
				$rec->porcentaje = round($rec->cantidad / $enfunde->{"enfunde_{$enf->year}_{$enf->week}"}->cantidad * 100, 2);
                $recobro->{"enfunde_{$enf->year}_{$enf->week}"} = $rec;
                $recobro->total += $rec->cantidad;

                $saldo->{"enfunde_{$enf->year}_{$enf->week}"}->cantidad -= $rec->cantidad;
                if(!isset($total->{"enfunde_{$enf->year}_{$enf->week}"})) 
                    $total->{"enfunde_{$enf->year}_{$enf->week}"} = (object) ["cantidad" => 0];
                $total->{"enfunde_{$enf->year}_{$enf->week}"}->cantidad += $rec->cantidad;
                $total->total += $rec->cantidad;
			}
		}

		$response->data = array_merge($response->data, $data);
		
		// FOOTER

		foreach($response->semanas_enfunde as $enf){
            // caidos
			$sql = "SELECT SUM(cantidad) cantidad
					FROM produccion_racimos_caidos
                    WHERE anio_enfundado = {$enf->year} AND semana_enfundada = {$enf->week}";
            $caidos->{"enfunde_{$enf->year}_{$enf->week}"} = $this->db->queryRow($sql);
            $caidos->total += $caidos->{"enfunde_{$enf->year}_{$enf->week}"}->cantidad;
            
            // recobro
            if(!$fila_recobro->{"enfunde_{$enf->year}_{$enf->week}"}){
                $fila_recobro->{"enfunde_{$enf->year}_{$enf->week}"} = new stdClass;
            }
            $fila_recobro->{"enfunde_{$enf->year}_{$enf->week}"}->tipo = 'Recobro';
            if($total->{"enfunde_{$enf->year}_{$enf->week}"}->cantidad  > 0 || $caidos->{"enfunde_{$enf->year}_{$enf->week}"}->cantidad > 0)
                $fila_recobro->{"enfunde_{$enf->year}_{$enf->week}"}->porcentaje = 
                    round(($total->{"enfunde_{$enf->year}_{$enf->week}"}->cantidad + $caidos->{"enfunde_{$enf->year}_{$enf->week}"}->cantidad) / $enfunde->{"enfunde_{$enf->year}_{$enf->week}"}->cantidad * 100, 2);
            else
                $fila_recobro->{"enfunde_{$enf->year}_{$enf->week}"}->porcentaje = 0;
            
            $fila_recobro->sum += $fila_recobro->{"enfunde_{$enf->year}_{$enf->week}"}->porcentaje;
            $fila_recobro->count++;
            $fila_recobro->total = round($fila_recobro->sum / $fila_recobro->count, 2);

            // saldo
            $saldo->{"enfunde_{$enf->year}_{$enf->week}"}->cantidad -= $caidos->{"enfunde_{$enf->year}_{$enf->week}"}->cantidad;
            $saldo->total += $saldo->{"enfunde_{$enf->year}_{$enf->week}"}->cantidad;
            
            // sin cinta
			$sql = "SELECT COUNT(1) cantidad
                    FROM produccion_racimos
                    WHERE cinta = 'S/C' AND anio_enfundado = {$enf->year} AND semana_enfundada = {$enf->week}";
            $value = $this->db->queryRow($sql);
            $sin_cinta->{"enfunde_{$enf->year}_{$enf->week}"} = $value;
		}

        $response->data[] = $total;
        $response->data[] = $caidos;
        $response->data[] = $fila_recobro;
        $response->data[] = $saldo;
        $response->data[] = $sin_cinta;

		return $response;
	}
}
