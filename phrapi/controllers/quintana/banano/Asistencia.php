<?php defined('PHRAPI') or die("Direct access not allowed!");

class Asistencia {
    private $db;
    private $config;
    private $token;
    private $Service;

    public function __construct(){
        $this->config = $GLOBALS['config'];
        $this->session = Session::getInstance();
        $this->db = DB::getInstance($this->session->agent_user);
    }

    public function index(){
        $response = new stdClass;
        $response->data = [];
        $response->recordsTotal = 0;

        $sWhere = "";
        $sOrder = " ORDER BY fecha DESC, id";
        $DesAsc = "ASC";
        $sOrder .= " {$DesAsc}";
        $sLimit = "";
        $sHaving = "";

        if(isset($_POST)){
            /*----------  ORDER BY ----------*/
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 1){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY id {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 2){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY fecha {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 3){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY hora {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 4){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY personal {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 5){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY centro_costo {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 6){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY codigo {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 7){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY labor {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 7){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY lotes {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 8){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY hectareas {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 10){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY horas_ordinarias {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 13){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY observaciones {$DesAsc}";
            }


            if(isset($_POST['id']) && trim($_POST['id']) != ""){
                $sWhere .= " AND id like '%".$_POST["id"]."%'";
            }   
            if(isset($_POST['order_date_from']) && trim($_POST['order_date_from']) != ""
                && isset($_POST['order_date_to']) && trim($_POST['order_date_to']) != ""){
                $sWhere .= " AND fecha BETWEEN '".$_POST["order_date_from"]."' AND '".$_POST["order_date_to"]."'";
            }    
            if(isset($_POST['hora']) && trim($_POST['hora']) != ""){
                $sWhere .= " AND hora like '%".$_POST["hora"]."%'";
            }   
            if(isset($_POST['personal']) && trim($_POST['personal']) != ""){
                $sWhere .= " AND personal LIKE '%".$_POST["personal"]."%'";
            }
            if(isset($_POST['centro_costo']) && trim($_POST['centro_costo']) != ""){
                $sWhere .= " AND centro_costo LIKE '%".$_POST["centro_costo"]."%'";
            }
            if(isset($_POST['codigo']) && trim($_POST['codigo']) != ""){
                $sWhere .= " AND codigo LIKE '%".$_POST["codigo"]."%'";
            } 
            if(isset($_POST['labor']) && trim($_POST['labor']) != ""){
                $sWhere .= " AND labor LIKE '%".$_POST["labor"]."%'";
            } 
            if(isset($_POST['lotes']) && trim($_POST['lotes']) != ""){
                $sWhere .= " AND lotes LIKE '%".$_POST["lotes"]."%'";
            }
            if(isset($_POST['hectareas']) && trim($_POST['hectareas']) != ""){
                $sWhere .= " AND hectareas LIKE '%".$_POST["hectareas"]."%'";
            }
            if(isset($_POST['horas_ordinarias']) && trim($_POST['horas_ordinarias']) != ""){
                $sWhere .= " AND horas_ordinarias LIKE '%".$_POST["horas_ordinarias"]."%'";
            }
            if(isset($_POST['observaciones']) && trim($_POST['observaciones']) != ""){
                $sWhere .= " AND observaciones LIKE '%".$_POST["observaciones"]."%'";
            }


            /*----------  LIMIT  ----------*/
            if(isset($_POST['length']) && $_POST['length'] > 0){
                $sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];
            }
        }

        $sql = "SELECT * FROM asistencia_detalle
                WHERE 1=1 $sWhere $sOrder $sLimit";
        $data = $this->db->queryAll($sql);
        foreach($data as $key => $value){
            $response->data[] = [
                $value->id,
                $value->id,
                $value->fecha,
                $value->personal,
                $value->centro_costo,
                $value->codigo,
                $value->labor,
                $value->lotes,
                $value->hectareas,
                $value->horas_ordinarias,
                $value->observaciones,
                ""
            ];
            $response->recordsTotal = $value->totalRows;
        }

        $response->recordsFiltered = count($response->data);
        $response->customActionStatus = "OK";
        return $response;
    }

    private function getFilters(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        
        $filters = (object)[
            "id_asistencia" => getValueFrom($postdata,'id_asistencia',1,FILTER_SANITIZE_PHRAPI_INT),
            "fecha" => getValueFrom($postdata,'fecha',1,FILTER_SANITIZE_STRING),
            "personal" => getValueFrom($postdata,'personal',1,FILTER_SANITIZE_STRING),
            "centro_costo" => getValueFrom($postdata,'centro_costo',1,FILTER_SANITIZE_STRING),
            "labor" => getValueFrom($postdata,'labor',1,FILTER_SANITIZE_STRING),
            "codigo" => getValueFrom($postdata,'codigo',1,FILTER_SANITIZE_STRING),
            "lotes" => getValueFrom($postdata,'lotes',1,FILTER_SANITIZE_STRING),
            "hectareas" => getValueFrom($postdata,'hectareas',1,FILTER_SANITIZE_STRING),
            "horas_ordinarias" => getValueFrom($postdata,'horas_ordinarias',1,FILTER_SANITIZE_STRING),
            "observaciones" => getValueFrom($postdata,'observaciones',1,FILTER_SANITIZE_STRING),
        ];

        return $filters;
    }

    public function saveAsistencia(){
        
        $filters = $this->getFilters();

    	$response = new stdClass;
        $sql = "UPDATE asistencia_detalle SET
                id_asistencia = '{$filters->id_asistencia}',
                fecha = '{$filters->fecha}',
                personal = '{$filters->personal}',
                centro_costo = '{$filters->centro_costo}',
                labor = '{$filters->labor}',
                codigo = '{$filters->codigo}',
                lotes = '{$filters->lotes}',
                hectareas = '{$filters->hectareas}',
                horas_ordinarias = '{$filters->horas_ordinarias}',
                observaciones = '{$filters->observaciones}'
                WHERE id = '{$data->id}'";
        $this->db->query($sql);

        $response->success = 200;
        return $response;
    }

    public function indexAsistencia(){
		$postdata = (object)json_decode(file_get_contents("php://input"));

		$filters = (object)[
			"finca" => getValueFrom($postdata , "finca" , "" , FILTER_SANITIZE_STRING),
			"fecha_inicial" => getValueFrom($postdata , "fecha_inicial" , "" , FILTER_SANITIZE_STRING),
			"fecha_final" => getValueFrom($postdata , "fecha_final" , "" , FILTER_SANITIZE_STRING),
		];

		$response = new stdClass;
		$sWhere = "";
		if(isset($filters->finca) && $filters->finca != ""){
			$sWhere .= " AND finca = '{$filters->finca}'";
		}
		if(isset($filters->fecha_inicial) && $filters->fecha_inicial != "" && isset($filters->fecha_final) && $filters->fecha_final != ""){
			$sWhere .= " AND DATE(fecha) BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}'";
		}
		$sql = "SELECT id ,finca , personal , fecha,DAYOFWEEK(fecha) AS dia_semana , WEEK(fecha) AS semana,DAY(fecha) AS dia,
				CASE 
					WHEN DAYNAME(fecha) = 'Monday' THEN 'LUN'
					WHEN DAYNAME(fecha) = 'Tuesday' THEN'MAR'
					WHEN DAYNAME(fecha) = 'Wednesday' THEN 'MIE'
					WHEN DAYNAME(fecha) = 'Thursday' THEN 'JUE'
					WHEN DAYNAME(fecha) = 'Friday' THEN 'VIE'
					WHEN DAYNAME(fecha) = 'Saturday' THEN 'SAB'
					WHEN DAYNAME(fecha) = 'Sunday' THEN 'D'
				END AS day_week,
				lotes,
				SUM(hectareas) AS hectareas,
				labor
				FROM asistencia_detalle
				WHERE 1=1 AND WEEK(fecha) = 24
				GROUP BY personal , fecha
				ORDER BY personal";
		$data = $this->db->queryAll($sql);
		/*----------  NEW METHOD  ----------*/
		$response->personal = [];
		$response->tittles = [];
		/*----------  NEW METHOD  ----------*/
		foreach ($data as $key => $value) {
			if($value->day_week != "D"){
				/*----------  NEW METHOD  ----------*/
				$response->tittles[$value->dia]["dia"] = (double)$value->dia;
				$response->tittles[$value->dia]["day_week"] = $value->day_week;
				$response->personal[$value->personal]["id"] = $value->id;
				$response->personal[$value->personal]["nombre"] = $value->personal;
				$response->personal[$value->personal]["dias"][$value->dia]["dia"] = (double)$value->dia;
				$response->personal[$value->personal]["dias"][$value->dia]["day_week"] = $value->day_week;
				$response->personal[$value->personal]["dias"][$value->dia]["hectareas"] = $value->hectareas;
				$response->personal[$value->personal]["dias"][$value->dia]["labor"] = $value->labor;
				$response->personal[$value->personal]["dias"][$value->dia]["lotes"] = $value->lotes;
				$response->personal[$value->personal]["total"] += (float)$value->hectareas;
				/*----------  NEW METHOD  ----------*/
			}
		}
		$response->fincas = $this->getFinca();
		return $response;
	}
    public function indexAsistencia2(){
		$postdata = (object)json_decode(file_get_contents("php://input"));

		$filters = (object)[
			"finca" => getValueFrom($postdata , "finca" , "" , FILTER_SANITIZE_STRING),
			"fecha_inicial" => getValueFrom($postdata , "fecha_inicial" , "" , FILTER_SANITIZE_STRING),
			"fecha_final" => getValueFrom($postdata , "fecha_final" , "" , FILTER_SANITIZE_STRING),
		];

		$response = new stdClass;
		$sWhere = "";
		if(isset($filters->finca) && $filters->finca != ""){
			$sWhere .= " AND finca = '{$filters->finca}'";
		}
		if(isset($filters->fecha_inicial) && $filters->fecha_inicial != "" && isset($filters->fecha_final) && $filters->fecha_final != ""){
			$sWhere .= " AND DATE(fecha) BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}'";
		}
		$sql = "SELECT id ,finca , personal , fecha,DAYOFWEEK(fecha) AS dia_semana , WEEK(fecha) AS semana,DAY(fecha) AS dia,
				DAYNAME(fecha) AS dia_nombre,
				IF(DAY(fecha) > 15 ,'2' , '1' ) AS quincena,
				CASE 
					WHEN DAYNAME(fecha) = 'Monday' THEN 'L'
					WHEN DAYNAME(fecha) = 'Tuesday' THEN'M'
					WHEN DAYNAME(fecha) = 'Wednesday' THEN 'MI'
					WHEN DAYNAME(fecha) = 'Thursday' THEN 'J'
					WHEN DAYNAME(fecha) = 'Friday' THEN 'V'
					WHEN DAYNAME(fecha) = 'Saturday' THEN 'S'
					WHEN DAYNAME(fecha) = 'Sunday' THEN 'D'
				END AS day_week,
				SUM(horas_ordinarias) AS hora
				FROM asistencia_detalle
				WHERE 1=1 {$sWhere}
				GROUP BY personal , fecha
				ORDER BY fecha,personal";
		$data = $this->db->queryAll($sql);
		/*----------  NEW METHOD  ----------*/
		$response->personal = [];
		$response->tittles = [];
		/*----------  NEW METHOD  ----------*/
		foreach ($data as $key => $value) {
			if($value->day_week != "D"){
				/*----------  NEW METHOD  ----------*/
				$response->tittles[$value->dia]["dia"] = (double)$value->dia;
				$response->tittles[$value->dia]["day_week"] = $value->day_week;
				$response->personal[$value->personal]["id"] = $value->id;
				$response->personal[$value->personal]["nombre"] = $value->personal;
				$response->personal[$value->personal]["dias"][$value->dia]["dia"] = (double)$value->dia;
				$response->personal[$value->personal]["dias"][$value->dia]["day_week"] = $value->day_week;
				$response->personal[$value->personal]["dias"][$value->dia]["horas"] = $value->hora;
				$response->personal[$value->personal]["total"] += (float)$value->hora;
				/*----------  NEW METHOD  ----------*/
			}
		}
		$response->fincas = $this->getFinca();
		return $response;
	}

    private function getFinca(){
		$response = new stdClass;
		$sql = "SELECT id AS id ,nombre AS label FROM fincas";
		$response = $this->db->queryAllSpecial($sql);
		return $response;
	}

    /*
    *   $type : [text, select, select-special, number, checkbox]
    *   $name : id y name del input
    *   $value : valor que tendra seteado
    *   $options : (opcional) en el caso de select [id, label] ... queryAllSpecial
    */
    private function getInput($type, $name, $value, $config = "", $options = []){
        $input = "";
        switch($type){
            case "text":
                $input = '<input type="text" class="form-control save" id="'.$name.'" name="'.$name.'" value="'.$value.'" '.$config.'/>';
                break;
            case "checkbox":
            	$input = '
            	<select class="form-control save" name="'.$name.'">
            		<option value="1" '.(($value)?'selected':'').'>Si</option>
            		<option value="0" '.((!$value)?'selected':'').'>No</option>
            	</select/>';
                break;
            case "select":
            	if($config == "multiple")
            	{
            		#value need it array
            		foreach($options as $key => $opt){
            			$input .= "{$opt}
            				<select class='form-control save' name='{$name}_{$key}'>
            					<option value='1' ".(($value)?'selected':'').">Si</option>
            					<option value='0' ".((!$value)?'selected':'').">No</option>
            				</select>";
            			$input .= "<br>";
            		}
            	}
            	else
            	{
            		$input = "<select class='form-control save' name='{$name}'>";
                	foreach($options as $opt){
                    	$input .= '<option class="form-control" value="'.$opt.'" '.(($opt == $value)?'selected':'').'>'.$opt.'</option>';
	                }
	                $input .= "</select>";
	            }
                break;
            case "select-special":
            	if($config == "multiple")
            	{
            		#value need it array
            		foreach($options as $key => $opt){
            			$input .= "{$opt->label}
            				<select class='form-control save' name='{$name}_{$opt->id}'>
            					<option value='1' ".((in_array(trim($opt->label), $value))?'selected':'').">Si</option>
            					<option value='0' ".((!in_array(trim($opt->label), $value))?'selected':'').">No</option>
            				</select>";
            			$input .= "<br>";
            		}
            	}
            	else
            	{
	                $input = "<select class='form-control save' name='{$name}'>";
	                foreach($options as $opt){
	                    $opt = (object) $opt;
	                    $input .= '<option value="'.$opt->id.'" '.((trim($opt->label) == $value)?'selected':'').'>'.$opt->label.'</option>';
	                }
	                $input .= "</select>";
	            }
                break;
            case "number":
                $input = '<input type="number" class="form-control save" id="'.$name.'" name="'.$name.'" value="'.$value.'" '.$config.'/>';
                break;
        }
        return $input;
    }
}