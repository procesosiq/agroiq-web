<?php defined('PHRAPI') or die("Direct access not allowed!");

class ProduccionCajas {
    public $name;
    private $db;
    private $config;

    public function __construct(){
        $this->config = $GLOBALS['config'];
        $this->session = Session::getInstance();
        $this->db = DB::getInstance($this->session->agent_user);
    }

    public function last(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));

        $sWhere = "";
        if(isset($postdata->newDate) && !$postdata->newDate){
            $sWhere .= " AND fecha = '{$postdata->filters->fecha_inicial}'";
        }

        $response->days = $this->db->queryAllOne("SELECT fecha FROM produccion_cajas GROUP BY fecha");
        $response->last = $this->db->queryRow("SELECT MAX(fecha) as fecha 
            FROM (
                SELECT MAX(fecha) AS fecha
                FROM produccion_cajas
                WHERE 1=1 $sWhere) AS tbl");
        $response->fincas = $this->db->queryAllSpecial("SELECT id_finca AS id, finca AS label FROM produccion_cajas WHERE fecha = '{$response->last->fecha}'");
        $response->available_fincas = $this->db->queryAllSpecial("SELECT id, nombre AS label FROM fincas WHERE status = 1");
        return $response;
    }

    public function cuadreCajas(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));
        
        $fincas = $this->db->queryAllOne("SELECT id_finca FROM produccion_cajas WHERE fecha = '{$postdata->fecha_inicial}' AND id_finca != '' GROUP BY id_finca");
        if($postdata->finca != ''){
            if(!in_array($postdata->finca, $fincas)){
                $postdata->finca = $fincas[0];
            }
        }

        $sWhere = "";
        if($postdata->finca != ''){
            $sWhere .= " AND id_finca = $postdata->finca";
        }

        $sql = "SELECT *,
                    (SELECT SUM(valor) FROM produccion_cajas_real WHERE marca = tbl.marca AND fecha BETWEEN '{$postdata->fecha_inicial}' AND '{$postdata->fecha_final}' AND id_finca = '{$postdata->finca}') AS suma_real,
                    IFNULL((SELECT SUM(cantidad) FROM produccion_cajas_pendiente_movimientos WHERE fecha = '{$postdata->fecha_inicial}' AND marca = tbl.marca AND id_finca = {$postdata->finca} AND tipo = 'PENDIENTE'), 0) pendiente,
                    IFNULL((SELECT SUM(cantidad) FROM produccion_cajas_pendiente_movimientos WHERE fecha = '{$postdata->fecha_inicial}' AND marca = tbl.marca AND id_finca = {$postdata->finca} AND tipo = 'ASIGNADA'), 0) asignada
                FROM(
                    SELECT 'CAJA' AS tipo, produccion_cajas.marca, id_finca, COUNT(1) AS balanza
                    FROM produccion_cajas 
                    WHERE fecha BETWEEN '{$postdata->fecha_inicial}' AND '{$postdata->fecha_final}' $sWhere
                    GROUP BY produccion_cajas.marca
                    UNION ALL
                    SELECT 'CAJA' AS tipo, marca, id_finca, 0 AS balanza
                    FROM produccion_cajas_real
                    WHERE fecha BETWEEN '{$postdata->fecha_inicial}' AND '{$postdata->fecha_final}' $sWhere
                        AND marca NOT IN(SELECT marca FROM produccion_cajas WHERE fecha BETWEEN '{$postdata->fecha_inicial}' AND '{$postdata->fecha_final}' $sWhere GROUP BY marca)
                    GROUP BY marca
                    UNION ALL
                    SELECT 'CAJA' AS tipo, marca, id_finca, 0 AS balanza
                    FROM produccion_cajas_pendiente_movimientos
                    WHERE fecha = '{$postdata->fecha_inicial}' AND id_finca = '{$postdata->finca}'
                    GROUP BY marca
                ) AS tbl
                GROUP BY marca";
                
        $response->data = $this->db->queryAll($sql);

        foreach($response->data as $row){
            $row->detalle = $this->db->queryAll("SELECT guia, SUM(valor) AS valor FROM produccion_cajas_real WHERE fecha BETWEEN '{$postdata->fecha_inicial}' AND '{$postdata->fecha_final}' AND marca = '{$row->marca}' $sWhere GROUP BY guia");
            if($row->balanza > 0 && $row->suma_real > 0)
                $row->porcentaje = $row->balanza / $row->suma_real * 100;
        }

        $response->guias = $this->db->queryAll("SELECT SUM(valor) AS cajas, guia, fecha, codigo_productor, codigo_magap, sello_seguridad, id_finca FROM produccion_cajas_real WHERE fecha BETWEEN '{$postdata->fecha_inicial}' AND '{$postdata->fecha_final}' $sWhere GROUP BY guia");
        foreach($response->guias as $row){
            $row->detalle = $this->db->queryAll("SELECT marca, SUM(valor) AS valor FROM produccion_cajas_real WHERE fecha BETWEEN '{$postdata->fecha_inicial}' AND '{$postdata->fecha_final}' AND guia = '{$row->guia}' $sWhere GROUP BY marca");
        }

        $sql = "SELECT
                    marca, finca, id_finca,
                    (pendiente-IFNULL((SELECT SUM(cantidad) FROM produccion_cajas_pendiente_movimientos WHERE tipo = 'ASIGNADA' AND id_finca_origen = tbl.id_finca AND marca = tbl.marca),0)) pendiente
                FROM (
                    SELECT 
                        marca, 
                        SUM(cantidad) pendiente, 
                        (SELECT nombre FROM fincas WHERE id_finca = fincas.id) finca,
                        id_finca
                    FROM produccion_cajas_pendiente_movimientos
                    WHERE tipo = 'PENDIENTE'
                    GROUP BY id_finca, marca
                ) tbl
                HAVING pendiente > 0";
        $response->saldos = $this->db->queryAll($sql);

        return $response;
    }

    public function guardarCuadrar(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));

        if(isset($postdata->fecha) && $postdata->fecha != "" && $postdata->finca != ''){
            foreach($postdata->marca as $marca => $value){
                if($marca != '' && $value > 0){
                    $exits = $this->db->queryOne("SELECT COUNT(1) FROM produccion_cajas_real WHERE fecha = '{$postdata->fecha}' AND marca = '{$marca}' AND guia = '{$postdata->guia}' AND id_finca = $postdata->finca") > 0;
                    if($exits){
                        $this->db->query("UPDATE produccion_cajas_real SET updated_at = CURRENT_TIMESTAMP, valor = '{$value}' WHERE marca = '{$marca}' AND fecha = '{$postdata->fecha}' AND guia = '{$postdata->guia}' AND id_finca = $postdata->finca");
                        $response->message = "Se actualizo con éxito";
                    }else{
                        $this->db->query("INSERT INTO produccion_cajas_real SET updated_at = CURRENT_TIMESTAMP, valor = '{$value}', marca = '{$marca}', fecha = '{$postdata->fecha}', guia = '{$postdata->guia}', codigo_productor = '{$postdata->productor}', codigo_magap = '{$postdata->magap}', sello_seguridad = '{$postdata->sello_seguridad}', id_finca = $postdata->finca");
                        $response->message = "Se inserto con éxito";
                    }
                }
            }
        }
        return $response;
    }

    public function procesar(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));

        $marcas = $this->db->queryAll("SELECT marca, SUM(valor) AS cantidad, id_finca FROM produccion_cajas_real WHERE fecha = '{$postdata->fecha}' GROUP BY marca, id_finca");
        foreach($marcas as $mr){
            $blz = $this->db->queryRow("SELECT SUM(cantidad) AS cantidad, AVG(peso) lb_prom, AVG(peso_kg) AS kg_prom
                                                    FROM(
                                                        SELECT COUNT(1) AS cantidad, AVG(lb) peso, AVG(kg) peso_kg
                                                        FROM produccion_cajas 
                                                        WHERE fecha = '{$postdata->fecha}' 
                                                            AND marca = '{$mr->marca}'
                                                            AND id_finca = $mr->id_finca
                                                    ) AS tbl");
            if($blz->cantidad < $mr->cantidad){
                $this->db->query("UPDATE produccion_cajas_real SET status = 'PROCESADO' WHERE fecha = '{$postdata->fecha}' AND marca = '{$mr->marca}' AND id_finca = $mr->id_finca");

                //INSERTAR DIFERENCIA
                $diff = $mr->cantidad - $blz->cantidad;
                if(!$blz->lb_prom > 0){
                    $last_fecha = $this->db->queryOne("SELECT MAX(fecha) FROM produccion_cajas WHERE fecha < '{$postdata->fecha}' AND id_finca = $mr->id_finca");
                    $blz->lb_prom = $this->db->queryOne("SELECT AVG(lb) FROM produccion_cajas WHERE fecha = '{$last_fecha}' AND id_finca = $mr->id_finca");
                    $blz->kg_prom = $this->db->queryOne("SELECT AVG(kg) FROM produccion_cajas WHERE fecha = '{$last_fecha}' AND id_finca = $mr->id_finca");
                }
                for($x = 0; $x < $diff; $x++){
                    $this->db->query("INSERT INTO produccion_cajas SET
                                        fecha = '{$postdata->fecha}',
                                        semana = getWeek('{$postdata->fecha}'),
                                        year = getYear('{$postdata->fecha}'),
                                        tipo_caja = '{$mr->marca}',
                                        marca = '{$mr->marca}',
                                        lb = {$blz->lb_prom},
                                        kg = {$blz->kg_prom},
                                        conver = ROUND({$blz->lb_prom} / 40.5, 2),
                                        convertidas_415 = ROUND({$blz->lb_prom} / 41.5, 2),
                                        tipo = 'cuadre',
                                        id_finca = $mr->id_finca");
                }
            }
        }
        return $response;
    }

    public function eliminar(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        if(count($postdata->ids) > 0){
            foreach($postdata->ids as $reg){
                $this->db->query("DELETE FROM produccion_cajas WHERE id = $reg->id");
            }
            return true;
        }
        return false;
    }

    public function filters(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));
        if(isset($postdata->year) && $postdata->year != ""){
            $sYear = " AND year = '{$postdata->year}'";
        }

        $response->years = $this->db->queryAllSpecial("SELECT year as id, year as label FROM(
            SELECT year FROM produccion_cajas GROUP BY year 
            ) AS tbl
            WHERE year > 0
            GROUP BY year");
        
        $sql = "SELECT semana as id, semana as label FROM(
            SELECT semana FROM produccion_cajas  WHERE 1=1 $sYear GROUP BY semana 
            ) AS tbl
            GROUP BY semana";
        $response->semanas = $this->db->queryAll($sql);
        return $response;
    }

    public function registros(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $filters = (object)[
            'actualPage' => (int) getValueFrom($postdata, 'actualPage', 0),
            'pagination' => (int) getValueFrom($postdata, 'pagination', 10),
            'fecha_inicial' => getValueFrom($postdata, 'fecha_inicial', ''),
            'fecha_final' => getValueFrom($postdata, 'fecha_final', ''),
            'unidad' => getValueFrom($postdata, 'unidad', 'lb'),
            'finca' => getValueFrom($postdata, 'finca', 0)
        ];

        $response = new stdClass;

        $fincas = $this->db->queryAllSpecial("SELECT id_finca AS id, finca AS label FROM produccion_cajas WHERE fecha = '{$filters->fecha_inicial}' GROUP BY id_finca");
        $sWhere = "";
        if(!isset($fincas[$filters->finca])){
            $filters->finca = array_keys($fincas)[0];
        }
        $sWhere .= " AND id_finca = $filters->finca";

        $response->pesos_prom_cajas = $this->db->queryAllSpecial("SELECT marca AS id, ROUND(AVG({$filters->unidad}), 2) AS label FROM produccion_cajas WHERE fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}' AND marca NOT IN((SELECT nombre FROM produccion_tipo_caja WHERE status = 'Activo')) $sWhere GROUP BY marca");
        $response->data = $this->db->queryAll("SELECT * 
            FROM (
                SELECT cajas.id, fecha, marca, {$filters->unidad} AS peso, 'CAJA' AS tipo, hora, 
                    IF(produccion_tipo_caja.id IS NOT NULL, IF({$filters->unidad} >= minimo_{$filters->unidad} AND {$filters->unidad} <= maximo_{$filters->unidad}, 'bg-green-haze bg-font-green-haze', IF({$filters->unidad} > maximo_{$filters->unidad}, 'bg-red-thunderbird bg-font-red-thunderbird' , 'bg-yellow-gold bg-font-yellow-gold')), '') AS class
                FROM produccion_cajas cajas
                LEFT JOIN produccion_tipo_caja ON nombre = marca
                WHERE fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}' $sWhere
            ) AS tbl
            ORDER BY fecha DESC, hora DESC");
        

        $response->marcas = $this->db->queryAll("SELECT m.*, mr.requerimiento, mr.max, mr.min FROM marcas_cajas m LEFT JOIN marcas_cajas_rangos mr ON mr.id = getRangoMarca(m.id, '{$filters->fecha_inicial}') WHERE status != 'Archivado' ORDER BY codigo+0");
        foreach($response->marcas as $m){
            $m->alias = $this->db->queryAll("SELECT id, alias FROM marcas_cajas_alias WHERE id_marca_caja = $m->id");
            $m->rangos = $this->db->queryAll("SELECT * FROM marcas_cajas_rangos WHERE id_marca_caja = $m->id");
            foreach($m->rangos as $r){
                $r->max = (float) $r->max;
                $r->min = (float) $r->min;
                $r->requerimiento = (float) $r->requerimiento;
            }
        }
        return $response;   
    }

    public function guardarCaja(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;
        $response->status = 400;

        if($postdata->id > 0){
            $exist = $this->db->queryOne("SELECT COUNT(1) FROM produccion_tipo_caja WHERE nombre = '{$postdata->marca}'");
            if($exist > 0){
                $response->status = 200;
                if($postdata->unidad == "lb")
                    $sql_add = ", lb = {$postdata->peso}, kg = ROUND({$postdata->peso} / 2.2046, 2)";
                else if($postdata->unidad == "kg")
                    $sql_add = ", kg = {$postdata->peso}, lb = ROUND({$postdata->peso} * 2.2046, 2)";
                $this->db->query("UPDATE produccion_cajas SET updated_at = CURRENT_TIMESTAMP, marca = '{$postdata->marca}', tipo_caja = (SELECT codigo_caja FROM produccion_tipo_caja WHERE nombre = '{$postdata->marca}') {$sql_add} WHERE id = {$postdata->id}");
            }else{
                $response->message = "Seleccione una marca de caja valida";
            }
        }else{
            $response->message = "Hubo un problema refresque la página y vuelva a intentar";
        }

        return $response;
    }

    public function resumenMarca(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $filters = (object)[
            'actualPage' => (int) getValueFrom($postdata, 'actualPage', 0),
            'pagination' => (int) getValueFrom($postdata, 'pagination', 10),
            'fecha_inicial' => getValueFrom($postdata, 'fecha_inicial', ''),
            'fecha_final' => getValueFrom($postdata, 'fecha_final', ''),
            'semana' => (int) getValueFrom($postdata, 'semana', 0),
            'unidad' => getValueFrom($postdata, 'unidad', 'lb'),
            "finca" => (int) getValueFrom($postdata, 'finca', 0),
        ];
        
        $sWhere = "";
        $response = new stdClass;
        $response->fincas = $this->db->queryAllSpecial("SELECT id_finca AS id, fincas.nombre AS label FROM produccion_cajas INNER JOIN fincas ON id_finca = fincas.id WHERE fecha = '{$filters->fecha_inicial}' GROUP BY id_finca");
        
        if(!isset($response->fincas[$filters->finca])){
            $filters->finca = array_keys($response->fincas)[0];
        }
        $sWhere .= " AND cajas.id_finca = $filters->finca";

        $sql = "SELECT marca, SUM(cantidad) AS cantidad, SUM(total_kg) AS total_kg, CONV AS 'conv', tipo, promedio, maximo, minimo, desviacion
                FROM(
                    SELECT
                        `cajas`.`marca` AS `marca`,
                        COUNT(1) AS `cantidad`,
                        ROUND(SUM(cajas.`{$filters->unidad}`), 2) AS total_kg,
                        ROUND(SUM(convertidas_415), 0) AS conv,
                        'CAJA' AS tipo,
                        ROUND(AVG(cajas.`{$filters->unidad}`), 2) AS promedio,
                        MAX(cajas.`{$filters->unidad}`) AS maximo,
                        MIN(cajas.`{$filters->unidad}`) AS minimo,
                        ROUND(STD(cajas.{$filters->unidad}), 2) AS desviacion
                    FROM `produccion_cajas` `cajas`
                    WHERE cajas.fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}' 
                        AND `cajas`.`marca` != '' $sWhere
                    GROUP BY `cajas`.`marca`
                ) AS tbl
                GROUP BY marca";
        $response->data = $this->db->queryAll($sql);
        
        /* LOAD TAGS */
        $response->tags = $this->tags($filters);
        $response->tags["cajas40"] = $this->sumOfValue($response->data, 'conv');

        return $response;
    }

    private function sumOfValue($data, $prop){
        $sum = 0;
        foreach($data as $row)
            if(is_object($row))
                $sum += (double) $row->{$prop};
            else
                $sum += (double) $row[$prop];
        return $sum;
    }

    private function getSemanasVariable($var, $year){
        $round = 0;
        if($var == 'CONV/HA') $round = 2;
		$sql = [];
		for($i = 1; $i <= 53; $i++){
			$sql[] = "SELECT $i AS semana, ROUND(AVG(sem_{$i}), $round) cantidad, fincas.nombre AS 'name' FROM produccion_resumen_tabla INNER JOIN fincas ON id_finca = fincas.id WHERE sem_{$i} > 0 AND variable = '$var' AND anio = $year GROUP BY id_finca";
		}
		return implode("
		UNION ALL
		", $sql);
	}

    public function historicoCajasSemanal(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));

        $namesVar = [
            'CONV' => 'CAJAS 41.5',
            'CONV/HA' => 'CONV/HA'
        ];

        $data_chart = $this->db->queryAll("SELECT semana AS label_x, sum(cantidad) as value, name, 0 index_y
            FROM(
                {$this->getSemanasVariable("{$namesVar[$postdata->var]}", $postdata->year)}
            ) AS tbl
            WHERE cantidad > 0
            GROUP BY label_x, name
            ORDER BY label_x, name");

        $groups = [
            [
                "name" => $postdata->var,
                "type" => 'line',
                'format' => ''
            ]
        ];
        $response->chart = $this->grafica_z($data_chart, $groups);
        return $response;
    }

    public function getMarcas(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));
        if(isset($postdata->year) && $postdata->year != ""){
            $sYear = " AND year = '{$postdata->year}'";
        }

        $response->data = $this->db->queryAllSpecial("SELECT marca as id, marca as label FROM(
            SELECT marca
            FROM produccion_cajas
            WHERE 1=1 $sYear
            GROUP BY marca
        ) AS tbl");
        return $response;
    }

    public function getGuias(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));
        
        if($postdata->guia != "" && $postdata->fecha != ""){
            $response->marcas = $this->db->queryAll("SELECT marca, valor, id_finca FROM produccion_cajas_real WHERE fecha = '{$postdata->fecha}' AND guia = '{$postdata->guia}'");
        }
        return $response;
    }

    public function tags($filters){
        $response = [];
        $sWhere = "";
        /*if($filters->semana > 0){
            $sWhere = " AND semana = {$filters->semana}";
        }*/
        $rows = $this->db->queryAll("SELECT hora, fecha
                                    FROM(
                                        SELECT hora, fecha
                                        FROM produccion_cajas 
                                        WHERE fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}' AND hora != '' AND marca != '' $sWhere
                                    ) AS tbl
                                    ORDER BY hora");
        if(count($rows)>0){
            $p = $rows[0];
            $u = $rows[count($rows)-1];
            $response["primera_caja"] = $p->hora;
            $response["ultima_caja"] = $u->hora;
            $response["fecha_primera"] = $p->fecha;
            $response["fecha_ultima"] = $u->fecha;
            $response["diferencia"] = $this->db->queryRow("SELECT TIMEDIFF('{$u->hora}}',  '{$p->hora}') AS dif")->dif;
        }else{
            $response["primera_caja"] = "";
            $response["ultima_caja"] = "";
            $response["fecha_primera"] = "";
            $response["fecha_ultima"] = "";
        }
        return $response;
    }

    public function graficasBarras(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $filters = (object)[
            'unidad' => getValueFrom($postdata, 'unidad', 'lb'),
        ];

        $response->marcas = $this->db->queryAll("SELECT marca, requerimiento_{$filters->unidad} AS requerimiento, maximo_{$filters->unidad} AS maximo, minimo_{$filters->unidad} AS minimo
                                                FROM (
                                                    SELECT marca, 1 AS sec FROM produccion_cajas WHERE fecha = '{$postdata->fecha_inicial}' AND id_finca = $postdata->finca GROUP BY marca
                                                ) AS tbl
                                                LEFT JOIN produccion_tipo_caja ON nombre = marca
                                                GROUP BY marca");

        $response->graficas = [];
        $response->pasteles = [];
        foreach($response->marcas as $marca){
            # BARRAS
            $response->graficas[$marca->marca] = (object)[
                "legends" => [],
                "series" => [
                    "Cantidad" => [
                        "name" => "Cantidad",
                        "connectNulls" => true,
                        "type" => "bar",
                        "itemStyle" => [
                            "normal" => [
                                "barBorderRadius" => 0,
                                "barBorderWidth" => 6
                            ]
                        ],
                        "label" => [
                            "normal" => [
                                "show" => true,
                                "position" => "inside"
                            ]
                        ],
                        "data" => [],
                    ]
                ]
            ];
            $pesos = $this->db->queryAllSpecial("SELECT CONCAT(
                                                            TRUNCATE(peso, 0)+(MOD(peso, 1) DIV 0.2)*0.2, ' - ',
                                                            TRUNCATE(peso, 0)+((MOD(peso, 1) DIV 0.2)*0.2+0.2)
                                                        ) AS 'id', SUM(cantidad) label
                                                FROM (
                                                    SELECT ROUND({$filters->unidad}, 2) as peso, COUNT(1) AS cantidad 
                                                    FROM produccion_cajas 
                                                    WHERE fecha = '{$postdata->fecha_inicial}' 
                                                        AND id_finca = $postdata->finca 
                                                        AND marca = '{$marca->marca}' 
                                                    GROUP BY ROUND({$filters->unidad}, 1)
                                                ) tbl
                                                GROUP BY peso DIV 0.2");
            
            foreach($pesos as $peso => $cantidad){
                $response->graficas[$marca->marca]->legends[] = $peso;
                $response->graficas[$marca->marca]->series["Cantidad"]["umbral"] = ["max" => $marca->maximo, "min" => $marca->minimo];
                $response->graficas[$marca->marca]->series["Cantidad"]["data"][] = $cantidad;
            }
            
            # PASTELES
            $response->pasteles[$marca->marca] = [];
            $cajas1840 = $this->db->queryAllOne("SELECT nombre FROM produccion_tipo_caja WHERE status = 'Activo'");
            if(in_array($marca->marca, $cajas1840) && ($marca->minimo > 0 && $marca->maximo > 0)){
                $sql = "SELECT value, label
                        FROM (
                            SELECT '{$marca->minimo}-{$marca->maximo}' AS label, COUNT(1) AS value
                            FROM produccion_cajas
                            WHERE marca = '{$marca->marca}' AND id_finca = $postdata->finca AND fecha = '{$postdata->fecha_inicial}' AND {$filters->unidad} >= {$marca->minimo} AND {$filters->unidad} <= {$marca->maximo}
                            UNION ALL
                            SELECT '> {$marca->maximo}' AS label, COUNT(1) AS value
                            FROM produccion_cajas
                            WHERE marca = '{$marca->marca}' AND id_finca = $postdata->finca AND fecha = '{$postdata->fecha_inicial}' AND {$filters->unidad} > {$marca->maximo}
                            UNION ALL
                            SELECT '< {$marca->minimo}' AS label, COUNT(1) AS value
                            FROM produccion_cajas
                            WHERE marca = '{$marca->marca}' AND id_finca = $postdata->finca AND fecha = '{$postdata->fecha_inicial}' AND {$filters->unidad} < {$marca->minimo}
                        ) AS tbl";
                $response->pasteles[$marca->marca] = $this->db->queryAll($sql);
            }else{
                $table = "produccion_cajas";
                if($table != ""){
                    $fieldPeso = $table == 'produccion_cajas' ? "{$filters->unidad}" : 'peso';

                    $peso_prom = $this->db->queryOne("SELECT ROUND(AVG(peso), 2) FROM (
                        SELECT AVG({$filters->unidad}) AS peso FROM produccion_cajas WHERE marca = '{$marca->marca}' AND fecha = '{$postdata->fecha_inicial}'
                    ) AS tbl");
                    $sql = "SELECT value, label
                            FROM (
                                SELECT '{$peso_prom}' AS label, COUNT(1) AS value
                                FROM $table
                                WHERE marca = '{$marca->marca}' AND id_finca = $postdata->finca AND fecha = '{$postdata->fecha_inicial}' AND $fieldPeso = {$peso_prom}
                                UNION ALL
                                SELECT '> {$peso_prom}' AS label, COUNT(1) AS value
                                FROM $table
                                WHERE marca = '{$marca->marca}' AND id_finca = $postdata->finca AND fecha = '{$postdata->fecha_inicial}' AND $fieldPeso > {$peso_prom}
                                UNION ALL
                                SELECT '< {$peso_prom}' AS label, COUNT(1) AS value
                                FROM $table
                                WHERE marca = '{$marca->marca}' AND id_finca = $postdata->finca AND fecha = '{$postdata->fecha_inicial}' AND $fieldPeso < {$peso_prom}
                            ) AS tbl";
                    $response->pasteles[$marca->marca] = $this->db->queryAll($sql);
                }
            }
        }
        return $response;
    }

    public function tablasDiferecias(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $filters = (object)[
            'unidad' => getValueFrom($postdata, 'unidad', 'lb'),
        ];
        $cajas1840 = $this->db->queryAll("SELECT nombre AS marca, requerimiento_{$filters->unidad} AS requerimiento FROM produccion_tipo_caja WHERE status = 'Activo' AND requerimiento_{$filters->unidad} IS NOT NULL");

        foreach($cajas1840 as $marca){
            $sql = "SELECT marca, ROUND(SUM(IF({$filters->unidad} > {$marca->requerimiento}, {$filters->unidad} - {$marca->requerimiento}, 0)), 2) AS kg_diff
                    FROM produccion_cajas
                    WHERE marca = '{$marca->marca}' AND id_finca = $postdata->finca AND fecha = '{$postdata->fecha_inicial}'";
            $row = $this->db->queryRow($sql);
            $row->cajas = round($row->kg_diff / $marca->requerimiento, 2);
            $row->dolares = round($row->cajas * 6.2, 2);
            if($row->kg_diff > 0) $response->tablas[] = $row;
        }

        return $response;
    }

    public function historicoExcedente(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $filters = (object)[
            'unidad' => getValueFrom($postdata, 'unidad', 'lb'),
        ];
        $response->data = [];
        $response->umbrales = new stdClass;
        
        //$marcas = $this->db->queryAllSpecial("SELECT marca AS id, marca AS label FROM produccion_cajas WHERE YEAR = {$postdata->year} AND id_finca = $postdata->finca AND {$filters->unidad} > (SELECT requerimiento_{$filters->unidad} FROM produccion_tipo_caja WHERE nombre = marca) AND marca IN(SELECT nombre FROM produccion_tipo_caja WHERE status = 'Activo') GROUP BY marca");
        /*$semanas = $this->db->queryAllSpecial("SELECT semana AS id, semana AS label FROM(
                SELECT semana
                FROM produccion_cajas
                WHERE YEAR = {$postdata->year} AND id_finca = $postdata->finca AND {$filters->unidad} > (SELECT requerimiento_{$filters->unidad} FROM produccion_tipo_caja WHERE nombre = marca) AND marca IN (SELECT nombre FROM produccion_tipo_caja WHERE status = 'Activo')
                GROUP BY semana
            ) AS tbl
            GROUP BY semana");
        $response->semanas = $semanas;*/
        $response->semanas = [];

        $marcas = [];

        $max_exce = []; $max_cajas = []; $max_dolares = [];
        $total = new stdClass;
        /*foreach($marcas as $marca){
            $row = new stdClass;
            $row->marca = $marca;
            $row->requerimiento = $this->db->queryOne("SELECT requerimiento_{$filters->unidad} FROM produccion_tipo_caja WHERE nombre = '{$row->marca}'");
            foreach($semanas as $semana){
                $val = $this->db->queryOne("SELECT ROUND(SUM(IF({$filters->unidad} > {$row->requerimiento}, {$filters->unidad} - {$row->requerimiento}, 0)), 2) FROM produccion_cajas WHERE YEAR = {$postdata->year} AND id_finca = $postdata->finca AND marca = '{$marca}' AND semana = {$semana}");
                $row->{"sem_exce_{$semana}"} = $val;
                $total->{"sem_exce_{$semana}"} += $val;
                $row->{"sem_cajas_{$semana}"} = round($val / $row->requerimiento, 2);
                $total->{"sem_cajas_{$semana}"} += round($val / $row->requerimiento, 2);
                $row->{"sem_dolares_{$semana}"} = round($row->{"sem_cajas_{$semana}"} * 6.2, 2);
                $total->{"sem_dolares_{$semana}"} += round($row->{"sem_cajas_{$semana}"} * 6.2, 2);

                if($val > 0) if(!isset($row->{"min_exce"}) || ($row->{"min_exce"} > $val)) $row->{"min_exce"} = $val;
                if($val > 0) if(!isset($row->{"max_exce"}) || ($row->{"max_exce"} < $val)) $row->{"max_exce"} = $val;
                $row->{"sum_exce"} += $val;
                $total->{"sum_exce"} += $val;

                if($val > 0) if((!isset($row->{"min_cajas"}) && $val > 0) || ($row->{"min_cajas"} > $row->{"sem_cajas_{$semana}"})) $row->{"min_cajas"} = $row->{"sem_cajas_{$semana}"};
                if($val > 0) if((!isset($row->{"max_cajas"}) && $val > 0) || ($row->{"max_cajas"} < $row->{"sem_cajas_{$semana}"})) $row->{"max_cajas"} = $row->{"sem_cajas_{$semana}"};
                $row->{"sum_cajas"} += $row->{"sem_cajas_{$semana}"};
                $total->{"sum_cajas"} += $row->{"sem_cajas_{$semana}"};

                if($val > 0) if(!isset($row->{"min_dolares"}) || ($row->{"min_dolares"} > $row->{"sem_dolares_{$semana}"})) $row->{"min_dolares"} = $row->{"sem_dolares_{$semana}"};
                if($val > 0) if(!isset($row->{"max_dolares"}) || ($row->{"max_dolares"} < $row->{"sem_dolares_{$semana}"})) $row->{"max_dolares"} = $row->{"sem_dolares_{$semana}"};
                $row->{"sum_dolares"} += $row->{"sem_dolares_{$semana}"};
                $total->{"sum_dolares"} += $row->{"sem_dolares_{$semana}"};

                if($val > 0) $row->{"count"} += 1;
            }
            $row->{"avg_exce"} = round($row->{"sum_exce"} / $row->{"count"}, 2);
            $row->{"avg_cajas"} = round($row->{"sum_cajas"} / $row->{"count"}, 2);
            $row->{"avg_dolares"} = round($row->{"sum_dolares"} / $row->{"count"}, 2);

            $response->data[] = $row;
        }
        $total->marca = 'TOTAL';
        foreach($total as $key => $value){
            if(strpos($key, "sem_exce") !== false){
                if($value > 0) if(!isset($total->{"max_exce"}) || $total->{"max_exce"} < $value) $total->{"max_exce"} = $value;
                if($value > 0) if(!isset($total->{"min_exce"}) || $total->{"min_exce"} > $value) $total->{"min_exce"} = $value;
            }
            if(strpos($key, "sem_cajas") !== false){
                if($value > 0) if(!isset($total->{"max_cajas"}) || $total->{"max_cajas"} < $value) $total->{"max_cajas"} = $value;
                if($value > 0) if(!isset($total->{"min_cajas"}) || $total->{"min_cajas"} > $value) $total->{"min_cajas"} = $value;
            }
            if(strpos($key, "sem_dolares") !== false){
                if($value > 0) if(!isset($total->{"max_dolares"}) || $total->{"max_dolares"} < $value) $total->{"max_dolares"} = $value;
                if($value > 0) if(!isset($total->{"min_dolares"}) || $total->{"min_dolares"} > $value) $total->{"min_dolares"} = $value;
            }
        }
        $total->{"avg_exce"} = round($total->{"sum_exce"} / count($semanas), 2);
        $total->{"avg_cajas"} = round($total->{"sum_cajas"} / count($semanas), 2);
        $total->{"avg_dolares"} = round($total->{"sum_dolares"} / count($semanas), 2);
        $response->data[] = $total;*/
        return $response;
    }

    private function grafica_z($data = [], $group_y = []){
        $options = [];
        $options["tooltip"] = [
            "trigger" => 'axis',
            "axisPointer" => [
                "type" => 'cross',
                "crossStyle" => [
                    "color" => '#999'
                ]
            ]
        ];
        $options["toolbox"] = [
            "feature" => [
                "dataView" => [
                    "show" => true,
                    "readOnly" => false
                ],
                "magicType" => [
                    "show" => true,
                    "type" => ['line', 'bar']
                ],
                "restore" => [
                    "show" => true
                ],
                "saveAsImage" => [
                    "show" => true
                ]
            ]
        ];
        $options["legend"]["data"] = [];
        $options["legend"]["bottom"] = "0%";
        $options["legend"]["left"] = "center";
        $options["xAxis"] = [
            [
                "type" => 'category',
                "data" => [],
                "axisPointer" => [
                    "type" => 'shadow'
                ]
            ]
        ];
        /*
            [
                type => 'value',
                name => {String},
                min => 0,
                max => 200,
                interval => 5,
                axisLabel => [
                    formatter => {value} KG
                ]
            ]
        */
        $options["yAxis"] = [];
        /*
            [
                name => {String},
                type => 'line',
                data => [
                    {double}, {double}, {double}
                ]
            ]
        */
        $options["series"] = [];

        $maxs = [];
        $mins = [];
        $prepare_data = [];
        $_x = [];
        $_names = [];
        $_namess = [];
        foreach($data as $d){
            $d = (object) $d;
            if(!isset($maxs[$d->index_y])) if($d->value > 0)
                $maxs[$d->index_y] = $d->value;
            if($d->value > $maxs[$d->index_y]) if($d->value > 0)
                $maxs[$d->index_y] = $d->value;

            if(!isset($mins[$d->index_y])) if($d->value > 0)
                $mins[$d->index_y] = $d->value;
            if($d->value < $mins[$d->index_y]) if($d->value > 0)
                $mins[$d->index_y] = $d->value;

            if(!in_array($d->label_x, $_x)){
                $_x[] = $d->label_x;
            }
            if(!in_array($d->name, $_namess)){
                $_namess[] = $d->name;
                 
                $n = ["name" => $d->name, "group" => $d->index_y];
                if(isset($d->line)){
                    $n["line"] = $d->line;
                }
                $_names[] = $n;
            }
            $prepare_data[$d->label_x][$d->name] = $d->value;
        }

        foreach($group_y as $key => $col){
            $col = (object) $col;
            $options["yAxis"][] = [
                'type' => 'value',
                'name' => $col->name,
                'min' => 'dataMin',
                'axisLabel' => [
                    'formatter' => "{value} $col->format"
                ]
            ];
        }

        foreach($_x as $row){
            $options["xAxis"][0]["data"][] = $row;
        }

        foreach($_names as $name){
            $name = (object) $name;

            if(!in_array($name->name, $options["legend"]["data"]))
                $options["legend"]["data"][] = $name->name;

            $serie = [
                "name" => $name->name,
                "type" => 'line',
                "connectNulls" => true,
                "data" => []
            ];
            if($name->group > 0)
                $serie["yAxisIndex"] = $name->group;

            if(isset($name->line)){
                $serie["itemStyle"]["normal"]["lineStyle"]["width"] = 5;
            }

            foreach($_x as $row){
                $val = 0;
                if(isset($prepare_data[$row][$name->name]))
                    $val = $prepare_data[$row][$name->name];

                if($val > 0)
                    $serie["data"][] = $val;
                else
                    $serie["data"][] = null;
            }
            $options["series"][] = $serie;
        }

        return $options;
    }
}
