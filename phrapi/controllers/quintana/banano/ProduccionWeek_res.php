<?php defined('PHRAPI') or die("Direct access not allowed!");

class ProduccionWeek {
	public $name;
	private $db;
	private $config;

	public function __construct(){
		$this->config = $GLOBALS['config'];
		$this->session = Session::getInstance();
		// D($this->session);
        $this->maindb = DB::getInstance();
        $this->db = DB::getInstance($this->session->agent_user);
	}


	public function index(){
		$postdata = (object)json_decode(file_get_contents("php://input"));

		$filters = (object)[
			"finca" => getValueFrom($postdata , "finca" , 0, FILTER_SANITIZE_INT),
			"idLote" => getValueFrom($postdata , "idLote" , 0, FILTER_SANITIZE_INT),
			"type" => getValueFrom($postdata , "type" , "cosechada" , FILTER_SANITIZE_STRING),
			"fecha_inicial" => getValueFrom($postdata , "fecha_inicial" , "" , FILTER_SANITIZE_STRING),
			"fecha_final" => getValueFrom($postdata , "fecha_final" , "" , FILTER_SANITIZE_STRING),
			"year" => getValueFrom($postdata , "year" , "" , FILTER_SANITIZE_STRING),
		];

		$sWhere = "";

		// if($filters->fecha_final != "" && $filters->fecha_inicial != ""){
		// 	$sWhere .= " AND DATE(fecha) BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}'";
		// }

		$sYear = " AND YEAR(fecha) = YEAR(CURRENT_DATE)";
		if($filters->idLote != "" && (int)$filters->idLote > 0){
			$sWhere .= " AND lote = '{$filters->idLote}'";
		}
		if($filters->year != "" && (int)$filters->year > 0){
			$sYear = " AND year = '{$filters->year}' ";
		}

		$sYearCampo = "YEAR(fecha) AS year";
		$sSemana = "WEEK(p.fecha)";
		if($this->session->id_company == 2 || $this->session->id_company == 3){
			$sSemana = "semana";
			$sYearCampo = "year";
		}

		$response = new stdClass;

		if($this->session->id_company == 2 || $this->session->id_company == 3){
			$sSemana = "p.semana";
			$sql = "SELECT $sSemana AS semana , $sYearCampo,
					SUM(total_cosechados) AS total_cosechados , 
					SUM(total_recusados) AS total_recusados,
					(SUM(total_cosechados) - SUM(total_recusados)) AS total_procesada,
					'false' AS expanded,
					((SELECT SUM(caja) AS caja FROM produccion_cajas WHERE year = YEAR(CURRENT_DATE) AND semana = p.semana GROUP BY semana) /
					SUM((total_cosechados))
					) AS ratio_cortado,
					((SELECT SUM(caja) AS caja FROM produccion_cajas WHERE year = YEAR(CURRENT_DATE) AND semana = p.semana GROUP BY semana) /
					SUM((total_cosechados-total_recusados))
					) AS ratio_procesado,
					(SELECT COUNT(*) FROM produccion_muestra WHERE WEEK(fecha) = $sSemana {$sYear}) AS muestrados,
					(SELECT AVG(peso) FROM produccion_peso WHERE semana = $sSemana {$sYear}) AS peso,
					(SELECT AVG(largo_dedos) FROM produccion_muestra WHERE WEEK(fecha) = $sSemana {$sYear}) AS largo_dedos,
					(SELECT AVG(calibracion) FROM produccion_calibracion WHERE semana = $sSemana {$sYear}) AS calibracion,
					(SELECT AVG(manos) FROM produccion_manos WHERE semana = $sSemana {$sYear}) AS manos
					FROM produccion AS p
					WHERE 1 = 1 {$sWhere}
					{$sYear}
					GROUP BY $sSemana";
			// D($sql);
		}else{
			$sql = "SELECT $sSemana AS semana , $sYearCampo,
					SUM(total_cosechados) AS total_cosechados , 
					SUM(total_recusados) AS total_recusados,
					(SUM(total_cosechados) - SUM(total_recusados)) AS total_procesada,
					'false' AS expanded,
					((SELECT SUM(caja) AS caja FROM produccion_cajas WHERE year = YEAR(CURRENT_DATE) AND semana = p.semana GROUP BY semana) /
					SUM((total_cosechados))
					) AS ratio_cortado,
					((SELECT SUM(caja) AS caja FROM produccion_cajas WHERE year = YEAR(CURRENT_DATE) AND semana = p.semana GROUP BY semana) /
					SUM((total_cosechados-total_recusados))
					) AS ratio_procesado,
					(SELECT COUNT(*) FROM produccion_muestra WHERE WEEK(fecha) = $sSemana {$sYear}) AS muestrados,
					(SELECT AVG(peso) FROM produccion_muestra WHERE WEEK(fecha) = $sSemana {$sYear}) AS peso,
					(SELECT AVG(calibracion) FROM produccion_muestra WHERE WEEK(fecha) = $sSemana {$sYear}) AS calibracion,
					(SELECT AVG(manos) FROM produccion_muestra WHERE WEEK(fecha) = $sSemana {$sYear}) AS manos
					FROM produccion AS p
					WHERE 1 = 1 {$sWhere}
					{$sYear}
					GROUP BY $sSemana";
					// (SELECT AVG(largo_dedos) FROM produccion_muestra WHERE WEEK(fecha) = $sSemana {$sYear}) AS largo_dedos,
		}
		// D($sql);
		$data = $this->db->queryAll($sql);		
		$response->data = [];
		$response->lotes = [];
		/*----------  GRAFICAS  ----------*/
		$racimos = [];
		$manos = [];
		$peso = [];
		$calibre = [];
		$dedo = [];
		$response->racimos = [];
		$response->manos = [];
		$response->calibre = [];
		$response->dedo = [];
		/*----------  GRAFICAS  ----------*/
		$dataMax = [];
		$response->anios = $this->db->queryAllSpecial("SELECT year as id, year as label FROM produccion GROUP BY year");
		if($this->session->id_company == 3){
			$response->anios = $this->db->queryAllSpecial("SELECT YEAR AS id, YEAR AS label FROM produccion WHERE year != '' GROUP BY YEAR ORDER BY YEAR DESC");
		}

		$response->peso = $this->generateDataPeso($filters);
		$response->manos = $this->generateDataManos($filters);
		$response->calibracion = $this->generateDataCalibracion($filters);
		$response->racimos = $this->generateDataCosecha($filters);

		if((int)$response->data["avg"]["peso"] <= 0){
			$response->data["avg"]["peso"] = $response->peso->promedio;
		}

		// $response->manos = $this->chartInit($manos , "vertical" , "Mano");
		// $response->calibre = $this->chartInit($calibre , "vertical" , "Calibración");
		$response->dedo = $this->chartInit($dedo , "vertical" , "Dedos");
		

		$weekofYear = (int)$this->db->queryOne("SELECT MAX(semana) AS semana FROM produccion WHERE year = YEAR(CURRENT_DATE)");
		if($filters->finca > 0){
			$sFinca = " AND idempacadora = '{$filters->finca}'";
		}
		$response->resumen = [
			"cosechados" => $this->db->queryRow("SELECT COUNT(1) AS suma FROM produccion_historica WHERE YEAR(fecha) = 2017  $sWhere $sFinca")->suma,
			"cosechados_year" => 0,
			"cosechados_week" => 0,
			"recusados" => 0,
			"procesados" => $this->db->queryRow("SELECT COUNT(1) AS suma FROM produccion_historica WHERE YEAR(fecha) = 2017  $sWhere $sFinca")->suma,
			"procesados_year" => 0,
			"procesados_week" => 0,
			"cajas_banapeel" => (float)$this->db->queryOne("SELECT ROUND(SUM(caja),2) AS cajas FROM produccion_cajas WHERE year = YEAR(CURRENT_DATE) AND marca = 'BANAPEEL'"),
			"cajas_battaglio" => (float)$this->db->queryOne("SELECT ROUND(SUM(caja),2) AS cajas FROM produccion_cajas WHERE year = YEAR(CURRENT_DATE) AND marca = 'BATTAGLIO'"),
			"cajas_derby" => (float)$this->db->queryOne("SELECT ROUND(SUM(caja),2) AS cajas FROM produccion_cajas WHERE year = YEAR(CURRENT_DATE) AND marca = 'DERBY'"),
			"cajas_dole" => (float)$this->db->queryOne("SELECT ROUND(SUM(caja),2) AS cajas FROM produccion_cajas WHERE year = YEAR(CURRENT_DATE) AND marca = 'DOLE AMERICANO'"),
			"cajas" => (float)$this->db->queryOne("SELECT ROUND(SUM(caja),2) AS cajas FROM produccion_cajas WHERE year = YEAR(CURRENT_DATE)")
		];


		$response->fincas = $this->db->queryAllSpecial("SELECT id_finca AS id, finca AS label FROM produccion_historica GROUP BY id_finca");
		if($filters->finca > 0){
			$sWhere = " AND id_finca = '{$filters->finca}'";
		}
		$response->lotes = $this->db->queryAllSpecial("SELECT lote AS id , lote AS label FROM produccion WHERE 1=1 {$sWhere} GROUP BY lote");

		$response->id_company = $this->session->id_company;
		$response->edades = $this->getPromedioEdadLote($filters);
		// $response->edades_2 = $this->getPromedioEdadWeek();

		/* TAGS MARIO */
		$response->tags->peso_prom_cajas = 19.09;
		$response->tags->otras_cajas = 21.82;
		$response->tags->calibracion = $this->db->queryRow("SELECT ROUND(AVG(calibracion), 2) AS prom FROM produccion_historica WHERE YEAR = '{$filters->year}'")->prom;
		$response->tags->racimo = $this->db->queryRow("SELECT ROUND(AVG(peso), 2) AS prom FROM produccion_historica WHERE YEAR = '{$filters->year}'")->prom;
		$response->tags->ratio_cortado = $this->db->queryRow("SELECT ROUND(AVG(IFNULL(cajas_primera, 0)/IFNULL(racimos_cortados, 0)), 2) AS prom FROM produccion WHERE YEAR = '{$filters->year}'")->prom;
		$response->tags->ratio_cortado_dos = $this->db->queryRow("SELECT ROUND(IFNULL(AVG(IFNULL(cajas_primera+cajas_segunda, 0)/IFNULL(racimos_cortados, 0)),0), 2) AS prom FROM produccion WHERE YEAR = '{$filters->year}'")->prom;
		$response->tags->ratio_procesado = $this->db->queryRow("SELECT ROUND(AVG(IFNULL(cajas_primera, 0)/IFNULL(racimos_procesados, 0)), 2) AS prom FROM produccion WHERE YEAR = '{$filters->year}'")->prom;
		$response->tags->ratio_procesado_dos = $this->db->queryRow("SELECT ROUND(IFNULL(AVG(IFNULL(cajas_primera+cajas_segunda, 0)/IFNULL(racimos_procesados, 0)),0), 2) AS prom FROM produccion WHERE YEAR = '{$filters->year}'")->prom;

		/*
		* peso_caja_primera = cajas_primera * 19.09
		* peso_caja_segunda = cajas_segunda * 19.09
		* peso_racimo_cortado = peso * racimo_cortado
		*
		* merma_cosechada 1era = (peso_racimo_cortado-peso_caja_primera)/peso_racimo_cortado
		* merma_cosechada 2da = (peso_racimo_cortado-(peso_caja_primera+peso_caja_segunda))/peso_racimo_cortado
		*/
		$response->tags->merma_cosechada = $this->db->queryRow("SELECT ROUND(AVG(((peso*racimos_cortados)-(cajas_primera*19.09))/(peso*racimos_cortados)*100),2) AS merma_cortada_1era FROM produccion WHERE YEAR = '{$filters->year}'")->merma_cortada_1era;
		$response->tags->merma_cosechada_dos = $this->db->queryRow("SELECT ROUND(AVG(((peso*racimos_cortados)-( cajas_primera*19.09 + cajas_segunda*19.09 ))/(peso*racimos_cortados)*100),2) AS merma_cortada_1era FROM produccion WHERE YEAR = '{$filters->year}'")->merma_cortada_1era;

		/*
		* peso_racimo_procesado = peso * racimos_procesados 
		*
		* merma_procesada 1era = (peso_racimo_procesado-peso_caja_primera)/peso_racimo_procesado
		* merma_procesada 2da = (peso_racimo_procesado-(peso_caja_primera+peso_caja_segunda))/peso_racimo_procesado
		*/
		$response->tags->merma_procesada = $this->db->queryRow("SELECT ROUND(AVG(((peso*racimos_procesados)-(cajas_primera*19.09))/(peso*racimos_procesados)*100),2) AS merma_procesada FROM produccion WHERE YEAR = '{$filters->year}'")->merma_procesada;
		$response->tags->merma_procesada_dos = $this->db->queryRow("SELECT ROUND(AVG(((peso*racimos_procesados)-(cajas_primera*19.09+cajas_segunda*19.09))/(peso*racimos_procesados)*100),2) AS merma_procesada FROM produccion WHERE YEAR = '{$filters->year}'")->merma_procesada;
		/*
		* merma_neta = merma_procesada - 10
		*
		* merma kg = merma_neta * acum_peso_racimo
		*/
		$response->tags->merma_kg = $this->db->queryRow("SELECT ROUND(acum * ".($response->tags->merma_procesada-10).",2) as acum FROM(SELECT SUM(peso) AS acum FROM produccion WHERE YEAR = '{$filters->year}') AS tbl")->acum;

		//$response->produccion = $this->produccionActual();


		$response->ha = (double)$this->getHA();
		$response->num_semanas = $this->db->queryOne("SELECT COUNT(semana) FROM
												(
													SELECT semana FROM produccion
													GROUP BY (semana)
												) AS details");
		$response->num_semanas_cajas = $this->db->queryAllSpecial("SELECT marca AS id , COUNT(marca) AS label FROM
												(
													SELECT semana , LOWER(marca) AS marca FROM produccion_cajas
													GROUP BY (semana) , marca
												) AS details
												GROUP BY marca");
		$response->num_semanas_cajas_total = $this->db->queryOne("SELECT COUNT(semana) FROM
												(
													SELECT semana FROM produccion_cajas
													GROUP BY (semana)
												) AS details");

		return $response;
	}

	private function getPromedioEdadLote($filters = []){
		$response = new stdClass;
		$sWhere = "";
		if($filters->finca > 0){
			$sWhere .= " AND id_finca = '{$filters->finca}'";
		}
		if($filters->idLote > 0){
			$sWhere .= " AND lote = '{$filters->idLote}'";
		}

		$sql = "(SELECT 
				semana AS label , 0 AS position , 'EDAD PROMEDIO' AS legend, 
				AVG(cable) AS value , 1 AS selected 
				FROM produccion
				WHERE 1=1 $sWhere
				GROUP BY (semana)
				ORDER BY (semana)) 
				UNION ALL
				(SELECT 
				semana AS label , IF(lote = 906 , 13 , lote) AS position ,lote AS legend,
				AVG(cable) AS value , 0 AS selected
				FROM produccion
				WHERE 1=1 $sWhere
				GROUP BY (semana) ,lote
				ORDER BY (semana),lote) ";
		$response->data = [];
		$response->data = $this->db->queryAll($sql);
		$minMax = [
			"min" => 9,
			"max" => 13,
		];
		$sql = "SELECT AVG(cable) FROM produccion";
		$response->edad = $this->db->queryOne($sql);
		$response->edad_promedio = $this->chartInit($response->data , "vertical" , "Edad Promedio" , "line" , $minMax);
		return $response;
	}

	private function produccionHistorica(){

	}

	public function produccionActual(){
		$data = $this->db->queryAll("SELECT *, WEEK(fecha) as semana, CEIL(WEEK(fecha)/4) as periodo FROM produccion WHERE year >= 2017");
		$data_procesada = [];
		foreach($data as $reg){
			if($reg->cosechados_blanco > 0){
				$data_procesada[$reg->semana]["blancas"]["sum_edades"] += $this->calcularEdad("BLANCA", (int) $reg->year, $reg->periodo, (int) $reg->semana);
				$data_procesada[$reg->semana]["blancas"]["prom_edades"][] = $this->calcularEdad("BLANCA", (int) $reg->year, $reg->periodo, (int) $reg->semana);
			}
			if($reg->cosechados_negro > 0){
				$data_procesada[$reg->semana]["negras"]["sum_edades"] += $this->calcularEdad("NEGRA", $reg->year, $reg->periodo, $reg->semana);
				$data_procesada[$reg->semana]["negras"]["prom_edades"][] = $this->calcularEdad("NEGRA", $reg->year, $reg->periodo, $reg->semana);
			}
			if($reg->cosechados_azul > 0){
				$data_procesada[$reg->semana]["azules"]["sum_edades"] += $this->calcularEdad("AZUL", $reg->year, $reg->periodo, $reg->semana);
				$data_procesada[$reg->semana]["azules"]["prom_edades"][] = $this->calcularEdad("AZUL", $reg->year, $reg->periodo, $reg->semana);
			}
		}
		foreach($data_procesada as $semana => $colores){
			foreach($colores as $color => $value){
				$data_sem[$semana][$color] = ($colores[$color]["sum_edades"] / count($colores[$color]["prom_edades"]));
			}
			#$data_sem[$semana]["blancas"] = ($blancas["blancas"]["sum_edades"] / count($blancas["blancas"]["prom_edades"]));
		}
		return $data_sem;
	}

	private function getHA(){
		$sql = "SELECT ha FROM config_produccion";
		$data = $this->db->queryOne($sql);
		return $data;
	}

	public function saveHA(){
		$postdata = (object)json_decode(file_get_contents("php://input"));

		$data = (object)[
			"ha" => getValueFrom($postdata , "ha" , "" , FILTER_SANITIZE_STRING),
			"idFinca" => getValueFrom($postdata , "idFinca" , "" , FILTER_SANITIZE_STRING),
		];

		$sql = "UPDATE config_produccion SET ha = {$data->ha}";
		$this->db->query($sql);

		return $this->getHA();
	}

	private function calcularEdad($color, $anio, $periodo, $semana){
		$secuencia = [
			"2016" => [
				10 => [ 37 => "CAFE", 38 => "AMARILLA", 39 => "VERDE", 40 => "AZUL"],
				11 => [ 41 => "BLANCA", 42 => "NEGRA", 43 => "LILA", 44 => "ROJA" ], 
				12 => [ 45 => "CAFE", 46 => "AMARILLA", 47 => "VERDE", 48 => "AZUL" ],
				13 => [ 49 => "BLANCA", 50 => "NEGRA", 51 => "LILA", 52 => "ROJA" ]
			],
			"2017" => [
				1 => [ 1 => "ROJA-CAFE", 2 => "CAFE", 3 => "NEGRA", 4 => "VERDE" ],
				2 => [ 5 => "AZUL", 6 => "BLANCA", 7 => "AMARILLA", 8 => "LILA", ],
				3 => [ 9 => "ROJA", 10 => "CAFE", 11 => "NEGRA", 12 => "VERDE" ],
				4 => [ 13 => "AZUL", 14 => "BLANCA", 15 => "AMARILLA", 16 => "LILA" ],
				5 => [ 17 => "ROJA", 18 => "CAFE", 19 => "NEGRA", 20 => "VERDE" ],
				6 => [ 21 => "AZUL", 22 => "BLANCA", 23 => "AMARILLA", 24 => "LILA" ],
				7 => [ 25 => "ROJA", 26 => "CAFE", 27 => "NEGRA", 28 => "VERDE" ],
				8 => [ 29 => "AZUL", 30 => "BLANCA", 31 => "AMARILLA", 32 => "LILA" ],
				9 => [ 33 => "ROJA", 34 => "CAFE", 35 => "NEGRA", 36 => "VERDE" ],
				10 => [ 37 => "AZUL", 38 => "BLANCA", 39 => "AMARILLA", 40 => "LILA" ],
				11 => [ 41 => "ROJA", 42 => "CAFE", 43 => "NEGRA", 44 => "VERDE" ],
				12 => [ 45 => "AZUL", 46 => "BLANCA", 47 => "AMARILLA", 48 => "LILA" ],
				13 => [ 49 => "ROJA", 50 => "CAFE", 51 => "NEGRA", 52 => "VERDE" ]
			]
		];
		$edad = 0;
		$encontro = false;
		$semana_edad = 0;
		$pr = 0;

		#$this->DD($semana);
		
		if($anio == 2017 && $semana < 18){
			if(in_array($semana, [14,15,16,17]))
				$pr = 13;
			if(in_array($semana, [10,11,12,13]))
				$pr = 12;
			if(in_array($semana, [6,8,9,10,3]))
				$pr = 11;
			if(in_array($semana, [5,7,4,2]))
				$pr = 10;

			foreach($secuencia["2016"][$pr] as $sec_periodo){
				if(!$encontro)
				foreach($sec_periodo as $sem => $col){					
					if($col == $color){
						$encontro = true;
						$semana_edad = $sem;
						break;
					}
				}
			}
			$edad = $semana_edad;
		}else if($semana >= 18){
			foreach($secuencia["2017"][$periodo-4] as $sec_periodo){
				foreach($sec_periodo as $sem => $col){
					if($col == $color){
						$encontro = true;
						$semana_edad = $sem;
					}
				}
			}
			$edad = $semana - $semana_edad;
		}else{
			foreach($secuencia["2017"][$periodo+13-5] as $sec_periodo){
				foreach($sec_periodo as $sem => $col){
					if($col == $color){
						$encontro = true;
						$semana_edad = $sem;
					}
				}
			}
			$edad = $semana + (13 - $semana_edad);
		}

		return $edad;
	}

	private function DD($string){
		echo "<pre>";
		D($string);
		echo "</pre>";
	}

	private function getLotes($semana = 0){
		$response = new stdClass;
		$response->data = [];
		if($semana > 0){
			$sYear = " AND YEAR(fecha) = YEAR(CURRENT_DATE)";
			$sSemana = "WEEK(p.fecha)";
			if($this->session->id_company == 2 || $this->session->id_company == 3){
				$sYear = " AND year = YEAR(CURRENT_DATE)";
				$sSemana = "semana";
			}

			if($this->session->id_company == 2 || $this->session->id_company == 3){
				$sql = "SELECT $sSemana AS semana , lote,
					SUM(total_cosechados) AS total_cosechados , 
					SUM(total_recusados) AS total_recusados,
					(SUM(total_cosechados) - SUM(total_recusados)) AS total_procesada,
					'false' AS expanded,
					((SELECT SUM(caja) AS caja FROM produccion_cajas WHERE year = YEAR(CURRENT_DATE) AND semana = p.semana AND lote = p.lote GROUP BY semana) /
					SUM((total_cosechados))
					) AS ratio_cortado,
					((SELECT SUM(caja) AS caja FROM produccion_cajas WHERE year = YEAR(CURRENT_DATE) AND semana = p.semana AND lote = p.lote GROUP BY semana) /
					SUM((total_cosechados-total_recusados))
					) AS ratio_procesado,
					(SELECT COUNT(*) FROM produccion_muestra WHERE WEEK(fecha) = {$semana}  AND lote = p.lote {$sYear}) AS muestrados,
					(SELECT AVG(largo_dedos) FROM produccion_muestra WHERE WEEK(fecha) = {$semana}  AND lote = p.lote {$sYear}) AS largo_dedos,
					(SELECT AVG(peso) FROM produccion_peso WHERE semana = $sSemana {$sYear} AND lote = p.lote) AS peso,
					(SELECT AVG(largo_dedos) FROM produccion_muestra WHERE WEEK(fecha) = $sSemana {$sYear} AND lote = p.lote) AS largo_dedos,
					(SELECT AVG(calibracion) FROM produccion_calibracion WHERE semana = $sSemana {$sYear} AND lote = p.lote) AS calibracion,
					(SELECT AVG(manos) FROM produccion_manos WHERE semana = $sSemana {$sYear} AND lote = p.lote) AS manos
					FROM produccion AS p
					WHERE 1 = 1 AND $sSemana = {$semana} {$sYear}
					GROUP BY lote";
			}else{
				$sql = "SELECT $sSemana AS semana , lote,
					SUM(total_cosechados) AS total_cosechados , 
					SUM(total_recusados) AS total_recusados,
					(SUM(total_cosechados) - SUM(total_recusados)) AS total_procesada,
					'false' AS expanded,
					((SELECT SUM(caja) AS caja FROM produccion_cajas WHERE year = YEAR(CURRENT_DATE) AND semana = p.semana AND lote = p.lote GROUP BY semana) /
					SUM((total_cosechados))
					) AS ratio_cortado,
					((SELECT SUM(caja) AS caja FROM produccion_cajas WHERE year = YEAR(CURRENT_DATE) AND semana = p.semana AND lote = p.lote GROUP BY semana) /
					SUM((total_cosechados-total_recusados))
					) AS ratio_procesado,
					(SELECT COUNT(*) FROM produccion_muestra WHERE WEEK(fecha) = {$semana}  AND lote = p.lote {$sYear}) AS muestrados,
					(SELECT AVG(peso) FROM produccion_muestra WHERE WEEK(fecha) = {$semana} AND lote = p.lote {$sYear}) AS peso,
					(SELECT AVG(calibracion) FROM produccion_muestra WHERE WEEK(fecha) = {$semana}  AND lote = p.lote {$sYear}) AS calibracion,
					(SELECT AVG(manos) FROM produccion_muestra WHERE WEEK(fecha) = {$semana}  AND lote = p.lote {$sYear}) AS manos
					FROM produccion AS p
					WHERE 1 = 1 AND $sSemana = {$semana} {$sYear}
					GROUP BY lote";
					// (SELECT AVG(largo_dedos) FROM produccion_muestra WHERE WEEK(fecha) = {$semana}  AND lote = p.lote {$sYear}) AS largo_dedos,
			}
			$data = $this->db->queryAll($sql);
			foreach ($data as $key => $value) {
				$response->data[$value->lote]["campo"] = $value->lote;
				$response->data[$value->lote]["muestrados"] = (double)$value->muestrados;
				$response->data[$value->lote]["peso"] = (double)$value->peso;
				$response->data[$value->lote]["manos"] = (double)$value->manos;
				// $response->data[$value->lote]["largo_dedos"] = (double)$value->largo_dedos;
				$response->data[$value->lote]["calibracion"] = (double)$value->calibracion;
				$response->data[$value->lote]["total_cosechados"] = (double)$value->total_cosechados;
				$response->data[$value->lote]["total_recusados"] = (double)$value->total_recusados;
				$response->data[$value->lote]["total_procesada"] = (double)$value->total_procesada;
				$response->data[$value->lote]["total_procesada"] = (double)$value->total_procesada;
				$response->data[$value->lote]["ratio_procesado"] = (double)$value->ratio_procesado;
				$response->data[$value->lote]["ratio_cortado"] = (double)$value->ratio_cortado;
			}
		}

		return $response->data;
	}

	private function getPromedioEdadWeek(){
		$sql = "SELECT WEEK(m.fecha) AS semana,SUM(cosechados_blanco) AS cosechados_blanco , SUM(cosechados_negro) AS cosechados_negro , 
				SUM(cosechados_lila) AS cosechados_lila ,SUM(cosechados_rojo) AS cosechados_rojo , SUM(cosechados_cafe) AS cosechados_cafe , 
				SUM(cosechados_amarillo) AS cosechados_amarillo , SUM(cosechados_verde) AS cosechados_verde , 
				SUM(cosechados_azul) AS cosechados_azul,SUM(cosechados_naranja) AS cosechados_naranja,SUM(cosechados_gris) AS cosechados_gris
				FROM produccion AS m
				GROUP BY WEEK(fecha)";
		$data = $this->db->queryAll($sql);
		$response = new stdClass;
		$response->data = [];
		foreach ($data as $key => $value) {
			$value->cosechados_blanco = (int)$value->cosechados_blanco;
			$value->cosechados_negro = (int)$value->cosechados_negro;
			$value->cosechados_lila = (int)$value->cosechados_lila;
			$value->cosechados_rojo = (int)$value->cosechados_rojo;
			$value->cosechados_cafe = (int)$value->cosechados_cafe;
			$value->cosechados_amarillo = (int)$value->cosechados_amarillo;
			$value->cosechados_verde = (int)$value->cosechados_verde;
			$value->cosechados_azul = (int)$value->cosechados_azul;
			$value->cosechados_naranja = (int)$value->cosechados_naranja;
			$value->cosechados_gris = (int)$value->cosechados_gris;
			if($value->cosechados_blanco > 0){
				$response->data[$value->semana]["blanco"]["edad"] = $this->getEdad("blanca" , $value->semana);
				$response->data[$value->semana]["blanco"]["cosechadas"] = $value->cosechados_blanco;
				$response->total[$value->semana]["sum"] += $value->cosechados_blanco * $response->data[$value->semana]["blanco"]["edad"];
				$response->total[$value->semana]["sum_cosecha"] += $value->cosechados_blanco;
			}
			if($value->cosechados_negro > 0){
				$response->data[$value->semana]["negra"]["edad"] = $this->getEdad("negra" , $value->semana);
				$response->data[$value->semana]["negra"]["cosechadas"] = $value->cosechados_negro;
				$response->total[$value->semana]["sum"] += $value->cosechados_negro * $response->data[$value->semana]["negra"]["edad"];
				$response->total[$value->semana]["sum_cosecha"] += $value->cosechados_negro;
			}
			if($value->cosechados_lila > 0){
				$response->data[$value->semana]["lila"]["edad"] = $this->getEdad("lila" , $value->semana);
				$response->data[$value->semana]["lila"]["cosechadas"] = $value->cosechados_lila;
				$response->total[$value->semana]["sum"] += $value->cosechados_lila * $response->data[$value->semana]["lila"]["edad"];
				$response->total[$value->semana]["sum_cosecha"] += $value->cosechados_lila;
			}
			if($value->cosechados_rojo > 0){
				$response->data[$value->semana]["roja"]["edad"] = $this->getEdad("roja" , $value->semana);
				$response->data[$value->semana]["roja"]["cosechadas"] = $value->cosechados_rojo;
				$response->total[$value->semana]["sum"] += $value->cosechados_rojo * $response->data[$value->semana]["roja"]["edad"];
				$response->total[$value->semana]["sum_cosecha"] += $value->cosechados_rojo;
			}
			if($value->cosechados_cafe > 0){
				$response->data[$value->semana]["cafe"]["edad"] = $this->getEdad("cafe" , $value->semana);
				$response->data[$value->semana]["cafe"]["cosechadas"] = $value->cosechados_cafe;
				$response->total[$value->semana]["sum"] += $value->cosechados_cafe * $response->data[$value->semana]["cafe"]["edad"];
				$response->total[$value->semana]["sum_cosecha"] += $value->cosechados_cafe;
			}
			if($value->cosechados_amarillo > 0){
				$response->data[$value->semana]["amarilla"]["edad"] = $this->getEdad("amarilla" , $value->semana);
				$response->data[$value->semana]["amarilla"]["cosechadas"] = $value->cosechados_amarillo;
				$response->total[$value->semana]["sum"] += $value->cosechados_amarillo * $response->data[$value->semana]["amarilla"]["edad"];
				$response->total[$value->semana]["sum_cosecha"] += $value->cosechados_amarillo;
			}
			if($value->cosechados_verde > 0){
				$response->data[$value->semana]["verde"]["edad"] = $this->getEdad("verde" , $value->semana);
				$response->data[$value->semana]["verde"]["cosechadas"] = $value->cosechados_verde;
				$response->total[$value->semana]["sum"] += $value->cosechados_verde * $response->data[$value->semana]["verde"]["edad"];
				$response->total[$value->semana]["sum_cosecha"] += $value->cosechados_verde;
			}
			if($value->cosechados_azul > 0){
				$response->data[$value->semana]["azul"]["edad"] = $this->getEdad("azul" , $value->semana);
				$response->data[$value->semana]["azul"]["cosechadas"] = $value->cosechados_azul;
				$response->total[$value->semana]["sum"] += $value->cosechados_azul * $response->data[$value->semana]["azul"]["edad"];
				$response->total[$value->semana]["sum_cosecha"] += $value->cosechados_azul;
			}
			if($value->cosechados_naranja > 0){
				$response->data[$value->semana]["naranja"]["edad"] = $this->getEdad("naranja" , $value->semana);
				$response->data[$value->semana]["naranja"]["cosechadas"] = $value->cosechados_naranja;
				$response->total[$value->semana]["sum"] += $value->cosechados_naranja * $response->data[$value->semana]["naranja"]["edad"];
				$response->total[$value->semana]["sum_cosecha"] += $value->cosechados_naranja;
			}
			if($value->cosechados_gris > 0){
				$response->data[$value->semana]["gris"]["edad"] = $this->getEdad("gris" , $value->semana);
				$response->data[$value->semana]["gris"]["cosechadas"] = $value->cosechados_gris;
				$response->total[$value->semana]["sum"] += $value->cosechados_gris * $response->data[$value->semana]["gris"]["edad"];
				$response->total[$value->semana]["sum_cosecha"] += $value->cosechados_gris;
			}
			$response->edad_promedio[$value->semana] = [
				"label" => $value->semana,
				"position" => 0,
				"legend" => "Edad Promedio",
				"value" => (double)($response->total[$value->semana]["sum"] / $response->total[$value->semana]["sum_cosecha"]),
			];

			$response->edad_prom[$value->semana] = (double)($response->total[$value->semana]["sum"] / $response->total[$value->semana]["sum_cosecha"]);
		}

		$response->edad = array_sum($response->edad_prom) / count($response->edad_prom);
		$minMax = [
			"min" => 9,
			"max" => 15,
		];
		$response->edad_promedio = $this->chartInit($response->edad_promedio , "vertical" , "Edad Promedio" , "line" , $minMax);
		return $response;
	}

	private function getPromedioEdad(){
		$sql = "SELECT DATE(fecha) AS fecha,WEEK(m.fecha) AS semana,SUM(cosechados_blanco) AS cosechados_blanco , SUM(cosechados_negro) AS cosechados_negro , 
				SUM(cosechados_lila) AS cosechados_lila ,SUM(cosechados_rojo) AS cosechados_rojo , SUM(cosechados_cafe) AS cosechados_cafe , 
				SUM(cosechados_amarillo) AS cosechados_amarillo , SUM(cosechados_verde) AS cosechados_verde , 
				SUM(cosechados_azul) AS cosechados_azul,SUM(cosechados_naranja) AS cosechados_naranja,SUM(cosechados_gris) AS cosechados_gris
				FROM produccion AS m
				GROUP BY DATE(fecha)";
		$data = $this->db->queryAll($sql);
		$response = new stdClass;
		$response->data = [];
		$edad_promedio = [];
		$sum_edadxcosecha = [];
		foreach ($data as $key => $value) {
			$value->cosechados_blanco = (int)$value->cosechados_blanco;
			$value->cosechados_negro = (int)$value->cosechados_negro;
			$value->cosechados_lila = (int)$value->cosechados_lila;
			$value->cosechados_rojo = (int)$value->cosechados_rojo;
			$value->cosechados_cafe = (int)$value->cosechados_cafe;
			$value->cosechados_amarillo = (int)$value->cosechados_amarillo;
			$value->cosechados_verde = (int)$value->cosechados_verde;
			$value->cosechados_azul = (int)$value->cosechados_azul;
			$value->cosechados_naranja = (int)$value->cosechados_naranja;
			$value->cosechados_gris = (int)$value->cosechados_gris;
			if($value->cosechados_blanco > 0){
				$response->data[$value->fecha]["blanco"]["edad"] = $this->getEdad("blanca" , $value->semana);
				$response->data[$value->fecha]["blanco"]["cosechadas"] = $value->cosechados_blanco;
				$sum_edadxcosecha[$value->fecha]["sum"] += $response->data[$value->fecha]["blanco"]["edad"] * $response->data[$value->fecha]["blanco"]["cosechadas"];
				$sum_edadxcosecha[$value->fecha]["sum_cosecha"] += $response->data[$value->fecha]["blanco"]["cosechadas"];
			}
			if($value->cosechados_negro > 0){
				$response->data[$value->fecha]["negra"]["edad"] = $this->getEdad("negra" , $value->semana);
				$response->data[$value->fecha]["negra"]["cosechadas"] = $value->cosechados_negro;
				$sum_edadxcosecha[$value->fecha]["sum"] += $response->data[$value->fecha]["negra"]["edad"] * $response->data[$value->fecha]["negra"]["cosechadas"];
				$sum_edadxcosecha[$value->fecha]["sum_cosecha"] += $response->data[$value->fecha]["negra"]["cosechadas"];
			}
			if($value->cosechados_lila > 0){
				$response->data[$value->fecha]["lila"]["edad"] = $this->getEdad("lila" , $value->semana);
				$response->data[$value->fecha]["lila"]["cosechadas"] = $value->cosechados_lila;
				$sum_edadxcosecha[$value->fecha]["sum"] += $response->data[$value->fecha]["lila"]["edad"] * $response->data[$value->fecha]["lila"]["cosechadas"];
				$sum_edadxcosecha[$value->fecha]["sum_cosecha"] += $response->data[$value->fecha]["lila"]["cosechadas"];
			}
			if($value->cosechados_rojo > 0){
				$response->data[$value->fecha]["roja"]["edad"] = $this->getEdad("roja" , $value->semana);
				$response->data[$value->fecha]["roja"]["cosechadas"] = $value->cosechados_rojo;
				$sum_edadxcosecha[$value->fecha]["sum"] += $response->data[$value->fecha]["roja"]["edad"] * $response->data[$value->fecha]["roja"]["cosechadas"];
				$sum_edadxcosecha[$value->fecha]["sum_cosecha"] += $response->data[$value->fecha]["roja"]["cosechadas"];
			}
			if($value->cosechados_cafe > 0){
				$response->data[$value->fecha]["cafe"]["edad"] = $this->getEdad("cafe" , $value->semana);
				$response->data[$value->fecha]["cafe"]["cosechadas"] = $value->cosechados_cafe;
				$sum_edadxcosecha[$value->fecha]["sum"] += $response->data[$value->fecha]["cafe"]["edad"] * $response->data[$value->fecha]["cafe"]["cosechadas"];
				$sum_edadxcosecha[$value->fecha]["sum_cosecha"] += $response->data[$value->fecha]["cafe"]["cosechadas"];
			}
			if($value->cosechados_amarillo > 0){
				$response->data[$value->fecha]["amarilla"]["edad"] = $this->getEdad("amarilla" , $value->semana);
				$response->data[$value->fecha]["amarilla"]["cosechadas"] = $value->cosechados_amarillo;
				$sum_edadxcosecha[$value->fecha]["sum"] += $response->data[$value->fecha]["amarilla"]["edad"] * $response->data[$value->fecha]["amarilla"]["cosechadas"];
				$sum_edadxcosecha[$value->fecha]["sum_cosecha"] += $response->data[$value->fecha]["amarilla"]["cosechadas"];
			}
			if($value->cosechados_verde > 0){
				$response->data[$value->fecha]["verde"]["edad"] = $this->getEdad("verde" , $value->semana);
				$response->data[$value->fecha]["verde"]["cosechadas"] = $value->cosechados_verde;
				$sum_edadxcosecha[$value->fecha]["sum"] += $response->data[$value->fecha]["verde"]["edad"] * $response->data[$value->fecha]["verde"]["cosechadas"];
				$sum_edadxcosecha[$value->fecha]["sum_cosecha"] += $response->data[$value->fecha]["verde"]["cosechadas"];
			}
			if($value->cosechados_azul > 0){
				$response->data[$value->fecha]["azul"]["edad"] = $this->getEdad("azul" , $value->semana);
				$response->data[$value->fecha]["azul"]["cosechadas"] = $value->cosechados_azul;
				$sum_edadxcosecha[$value->fecha]["sum"] += $response->data[$value->fecha]["azul"]["edad"] * $response->data[$value->fecha]["azul"]["cosechadas"];
				$sum_edadxcosecha[$value->fecha]["sum_cosecha"] += $response->data[$value->fecha]["azul"]["cosechadas"];
			}
			if($value->cosechados_naranja > 0){
				$response->data[$value->fecha]["naranja"]["edad"] = $this->getEdad("naranja" , $value->semana);
				$response->data[$value->fecha]["naranja"]["cosechadas"] = $value->cosechados_naranja;
				$sum_edadxcosecha[$value->fecha]["sum"] += $response->data[$value->fecha]["naranja"]["edad"] * $response->data[$value->fecha]["naranja"]["cosechadas"];
				$sum_edadxcosecha[$value->fecha]["sum_cosecha"] += $response->data[$value->fecha]["naranja"]["cosechadas"];
			}
			if($value->cosechados_gris > 0){
				$response->data[$value->fecha]["gris"]["edad"] = $this->getEdad("gris" , $value->semana);
				$response->data[$value->fecha]["gris"]["cosechadas"] = $value->cosechados_gris;
				$sum_edadxcosecha[$value->fecha]["sum"] += $response->data[$value->fecha]["gris"]["edad"] * $response->data[$value->fecha]["gris"]["cosechadas"];
				$sum_edadxcosecha[$value->fecha]["sum_cosecha"] += $response->data[$value->fecha]["gris"]["cosechadas"];
			}

			$response->edad_promedio[$value->semana][$value->fecha] = $sum_edadxcosecha[$value->fecha]["sum"] / $sum_edadxcosecha[$value->fecha]["sum_cosecha"];
			$response->data[$value->semana] += $sum_edadxcosecha[$value->fecha]["sum"] / $sum_edadxcosecha[$value->fecha]["sum_cosecha"];
		}

		return $response;
	}

	private function getEdad($color = "" , $semana = 1){
		$edad = 0;
		$sql = "SELECT
				({$semana} - (
				SELECT semana FROM semanas_colores
				WHERE YEAR(CURRENT_DATE) = year AND color = UPPER('{$color}') AND semana < ({$semana} - 8) ORDER BY semana DESC LIMIT 1
				)) + 1 AS edad";
		$edad = (int)$this->db->queryOne($sql);
		return $edad;
	}

	private function generateDataCosecha($filters){
		$response = new stdClass;
		$sYearCampo = "YEAR(fecha)";
		$sSemana = "WEEK(p.fecha)";
		if($this->session->id_company == 2 || $this->session->id_company == 3){
			$sSemana = "semana";
			$sYearCampo = "year";
		}

		$type = [
			"cosechados" => "SUM(total_cosechados)",
			"recusados" => "SUM(total_recusados)",
			"procesada" => "(SUM(total_cosechados) - SUM(total_recusados))",
		];
		$title = "Cosechas";
		if($filters->type == "cajas"){
			$title = "Cajas";
			$sql_historico = "SELECT year AS legend,semana AS label,SUM(caja) AS value,IF(year > 2013 ,true,false) AS selected
								FROM produccion_cajas AS p
								GROUP BY year,semana";
		}else{
			$title = "Cosechas";
			$sql_historico = "SELECT {$sSemana} AS label,{$sYearCampo} AS legend,IF(year > 2013 ,true,false) AS selected,
				{$type[$filters->type]} AS value 
				FROM produccion AS p
				GROUP BY {$sYearCampo} , {$sSemana}";
		}
		
		$data = $this->db->queryAll($sql_historico);

		$minMax = [
			// "min" => 50,
			// "max" => 100,
		];
		//$response->historico = $this->chartInit($data,"vertical",$title,"line",$minMax);

		/* TAG : 2017-08-10 ELIMINAR LA GRAFICA DE COSECHADAS, EL FILTRO Y LA DE CALIBRE PARA FORMAR LA MISMA GRAFICA DE DOBLE EJE */
		$sWhere = "";
		$sWhere2 = "";
		if($filters->finca > 0){
			$sWhere .= " AND id_finca = '{$filters->finca}'";
			$sWhere2 .= " AND idempacadora = '{$filters->finca}'";
		}
		if($filters->idLote > 0){
			$sWhere .= " AND lote = '{$filters->idLote}'";
			$sWhere2 .= " AND lote = '{$filters->idLote}'";
		}
		$sql = "SELECT 
					WEEK(fecha) AS label_x, 
					SUM(total_cosechados) AS value, 
					'COSECHADOS' AS name,
					'0' AS index_y
				FROM produccion 
				WHERE 1=1 $sWhere
				GROUP BY WEEK(fecha)
				UNION ALL
				SELECT 
					WEEK(fecha) AS label_x, 
					(SUM(total_cosechados) - SUM(total_recusados)) AS value, 
					'PROCESADOS' AS name,
					'0' AS index_y
				FROM produccion
				WHERE 1=1 $sWhere
				GROUP BY WEEK(fecha)
				UNION ALL
				SELECT
					semana AS label_x,
					ROUND(AVG(calibracion), 2) AS value,
					'CALIBRACIÓN' AS name,
					'1' AS index_y
				FROM produccion_calibracion
				WHERE 1=1 $sWhere
				GROUP BY semana
				UNION ALL
				SELECT
					WEEK(fecha) AS label_x,
					ROUND(AVG(peso / 2.2), 2) AS value,
					'PESO' AS name,
					'1' AS index_y
				FROM produccion_historica
				WHERE 1=1 $sWhere2
				GROUP BY WEEK(fecha)
				UNION ALL
				(SELECT 
					WEEK(fecha) AS label_x , 
					ROUND(AVG(cable), 2) AS value, 
					'EDAD PROMEDIO' AS name, 
					'1' AS index_y
				FROM produccion
				WHERE fecha IS NOT NULL $sWhere
				GROUP BY WEEK(fecha)
				ORDER BY WEEK(fecha))";
		$data_chart = $this->db->queryAll($sql);
		$groups = [
			[
				"name" => '',
				"type" => 'line',
				'format' => ''
			],
			[
				"name" => '',
				"type" => 'line',
				'format' => ''
			]
		];
		$response->historico = $this->grafica_z($data_chart, $groups);

		return $response;
	}

	private function generateDataPeso($filters = []){
		$response = new stdClass;
		$sql_promedio = "SELECT AVG(peso) AS peso FROM (
						SELECT semana ,AVG(peso) as peso FROM (
						(SELECT lote,semana , AVG(peso) AS peso FROM produccion_peso
						WHERE year = YEAR(CURRENT_DATE)
						GROUP BY year,lote,semana)) AS p
						GROUP BY semana) AS p1";
		$response->promedio = (double)$this->db->queryOne($sql_promedio);

		$sql_historico = "SELECT year AS legend , semana AS label , ROUND(AVG(peso),2) AS value,IF(year > 2013 ,true,false) AS selected FROM (
							SELECT year , semana ,lote,AVG(peso) AS peso FROM produccion_peso
							GROUP BY year,lote,semana) AS details
							GROUP BY year,semana";
		$data = $this->db->queryAll($sql_historico);
		// $data = [];
		$minMax = [
			"min" => 80,
			"max" => 88,
		];
		//$response->historico = $this->chartInit($data,"vertical","Peso","line",$minMax);

		$sWhere = "";
		$sWhere2 = "";
		if($filters->finca > 0){
			$sWhere .= " AND id_finca = '{$filters->finca}'";
			$sWhere2 .= " AND idempacadora = '{$filters->finca}'";
		}
		if($filters->idLote > 0){
			$sWhere .= " AND lote = '{$filters->idLote}'";
			$sWhere2 .= " AND lote = '{$filters->idLote}'";
		}
		$data_chart = "SELECT
							WEEK(fecha) AS label_x,
							ROUND(AVG(peso), 2) AS value,
							'PESO' AS name,
							'1' AS index_y
						FROM produccion_historica
						WHERE 1=1 $sWhere2
						GROUP BY WEEK(fecha)
						UNION ALL
						SELECT
							semana AS label_x,
							ROUND(AVG(calibracion), 2) AS value,
							'CALIBRACIÓN' AS name,
							'0' AS index_y
						FROM produccion_calibracion
						WHERE 1=1 $sWhere
						GROUP BY semana";
		$data_chart = $this->db->queryAll($data_chart);
		$groups = [
			[
				"name" => '',
				"type" => 'line',
				'format' => ''
			],
			[
				"name" => 'Peso',
				"type" => 'line',
				'format' => ''
			]
		];
		$response->historico = $this->grafica_z($data_chart, $groups);

		return $response;
	}

	private function generateDataManos($filters = []){
		$response = new stdClass;

		$sWhere = "";
		if($filters->finca > 0){
			$sWhere .= " AND id_finca = '{$filters->finca}'";
		}
		if($filters->idLote > 0){
			$sWhere .= " AND lote = '{$filters->idLote}'";
		}

		$sql_promedio = "SELECT AVG(manos) AS manos FROM (
						SELECT semana ,AVG(manos) as manos FROM (
						(SELECT lote,semana , AVG(manos) AS manos FROM produccion_manos
						WHERE year = YEAR(CURRENT_DATE)
						GROUP BY year,lote,semana)) AS p
						GROUP BY semana) AS p1";
		$response->promedio = (double)$this->db->queryOne($sql_promedio);

		$sql_historico = "SELECT year AS legend , semana AS label , ROUND(AVG(manos),2) AS value,IF(year > 2013 ,true,false) AS selected FROM (
							SELECT year , semana ,lote,AVG(manos) AS manos FROM produccion_manos
							WHERE 1=1 $sWhere
							GROUP BY year,lote,semana) AS details
							GROUP BY year,semana";
		$data = $this->db->queryAll($sql_historico);
		// $data = [];
		$minMax = [
			"min" => 'dataMin',
			"max" => null,
		];
		$response->historico = $this->chartInit($data,"vertical","Manos","line",$minMax);

		return $response;
	}

	private function generateDataCalibracion(){
		$response = new stdClass;
		$sql_promedio = "SELECT AVG(calibracion) AS calibracion FROM (
						SELECT semana ,AVG(calibracion) as calibracion FROM (
						(SELECT lote,semana , AVG(calibracion) AS calibracion FROM produccion_calibracion
						WHERE year = YEAR(CURRENT_DATE)
						GROUP BY year,lote,semana)) AS p
                        GROUP BY semana) AS p1";
		$response->promedio = (double)$this->db->queryOne($sql_promedio);

		$sql_historico = "SELECT year AS legend , semana AS label , ROUND(AVG(calibracion),2) AS value,IF(year > 2013 ,true,false) AS selected 
                            FROM (
							    SELECT year , semana ,lote, AVG(calibracion) AS calibracion 
                                FROM produccion_historica
							    GROUP BY year, lote, semana
                            ) AS details
							GROUP BY year, semana";
		$data = $this->db->queryAll($sql_historico);
		// $data = [];
		$minMax = [
			"min" => 44,
			"max" => 46,
		];
		$response->historico = $this->chartInit($data,"vertical","Calibración","line",$minMax);

		return $response;
	}

	private function chartInit($data = [] , $mode = "vertical" , $name = "" , $type = "line" , $minMax = []){
		$response = new stdClass;
		$response->chart = [];
		if(count($data) > 0){
			$response->chart["title"]["show"] = true;
			$response->chart["title"]["text"] = $name;
			$response->chart["title"]["subtext"] = $this->session->sloganCompany;
			$response->chart["tooltip"]["trigger"] = "item";
			$response->chart["tooltip"]["axisPointer"]["type"] = "shadow";
			$response->chart["toolbox"]["show"] = true;
			$response->chart["toolbox"]["feature"]["mark"]["show"] = true;
			$response->chart["toolbox"]["feature"]["restore"]["show"] = true;
			$response->chart["toolbox"]["feature"]["magicType"]["show"] = true;
			$response->chart["toolbox"]["feature"]["magicType"]["type"] = ['bar' , 'line'];
			$response->chart["toolbox"]["feature"]["saveAsImage"]["show"] = true;
			$response->chart["legend"]["data"] = [];
			$response->chart["legend"]["left"] = "center";
			$response->chart["legend"]["bottom"] = "1%";
			$response->chart["grid"]["left"] = "3%";
			$response->chart["grid"]["right"] = "4%";
			$response->chart["grid"]["bottom"] = "15%";
			$response->chart["grid"]["containLabel"] = true;
			if($mode == "vertical"){
				$response->chart["xAxis"] = ["type" => "category" , "data" => [""]];
				$response->chart["yAxis"] = ["type" => "value"];
				if(isset($minMax["min"])){
					$response->chart["yAxis"] = ["type" => "value" , "min" => $minMax["min"], "max" => $minMax["max"]];
				}
			}else if($mode == "horizontal"){
				$response->chart["yAxis"] = ["type" => "category" , "data" => [""]];
				$response->chart["xAxis"] = ["type" => "value"];
			}
			$response->chart["series"] = [];
			$count = 0;
			$position = -1;
			$colors = [];
			if($type == "line"){
				$response->chart["xAxis"]["data"] = [];
			}
			foreach ($data as $key => $value) {
				$value = (object)$value;
				$value->legend = ucwords(strtolower($value->legend));
				$value->label = ucwords(strtolower($value->label));
				if(isset($value->position)){
					$position = $value->position;
				}
				if(isset($value->color)){
					$colors = [
						"normal" => ["color" => $value->color],
					];
				}

				if($type == "line"){
					if(!in_array($value->label, $response->chart["xAxis"]["data"])){
						$response->chart["xAxis"]["data"][] = $value->label;
					}
					if(!in_array($value->legend, $response->chart["legend"]["data"])){
						if(!isset($value->position)){
							$position++;
						}
						$response->chart["legend"]["data"][] = $value->legend;
						$response->chart["legend"]["selected"][$value->legend] = ($value->selected == 0) ? false : true;
						$response->chart["series"][$position] = [
							"name" => $value->legend,
							"type" => $type,
							"data" => [],
							"label" => [
								"normal" => ["show" => false , "position" => "top"],
								"emphasis" => ["show" => false , "position" => "top"],
							],
							"itemStyle" => $colors
						];
					}

					$response->chart["series"][$position]["data"][] = ROUND($value->value,2);

				}else{
					$response->chart["legend"]["data"][] = $value->label;
					// if($count == 0){
						$response->chart["series"][$count] = [
							"name" => $value->label,
							"type" => $type,
							"data" => [(int)$value->value],
							"label" => [
								"normal" => ["show" => true , "position" => "top"],
								"emphasis" => ["show" => true , "position" => "top"],
							],
							"itemStyle" => $colors
						];
					// }
				}
				$count++;
			}
		}

		return $response->chart;
	}
	private function pie($data = [] , $radius = ['50%', '70%'] , $name = "CATEGORIAS" , $roseType = "" , $type = "normal" , $legend = true , $position = ['45%', '40%'] , $version = 3){
		$response = new stdClass;
		$response->pie = [];
		if(count($data) > 0){
			$response->pie["title"]["show"] = true;
			$response->pie["title"]["text"] = $name;
			$response->pie["title"]["left"] = "center";
			// $response->pie["title"]["left"] = "right";
			$response->pie["title"]["subtext"] = $this->session->sloganCompany;
			$response->pie["tooltip"]["trigger"] = "item";
			$response->pie["tooltip"]["formatter"] = "{a} <br/>{b}: {c} ({d}%)";
			$response->pie["toolbox"]["show"] = true;
			$response->pie["toolbox"]["feature"]["mark"]["show"] = true;
			$response->pie["toolbox"]["feature"]["restore"]["show"] = true;
			$response->pie["toolbox"]["feature"]["magicType"]["show"] = false;
			$response->pie["toolbox"]["feature"]["magicType"]["type"] = ['pie'];
			$response->pie["toolbox"]["feature"]["saveAsImage"]["show"] = true;
			$response->pie["toolbox"]["feature"]["saveAsImage"]["name"] = $name;
			$response->pie["legend"]["show"] = $legend;
			$response->pie["legend"]["x"] = "center";
			$response->pie["legend"]["y"] = "bottom";
			$response->pie["calculable"] = true;
			$response->pie["legend"]["data"] = [];
			$response->pie["series"]["name"] = $name;
			$response->pie["series"]["type"] = "pie";
			$response->pie["series"]["center"] = $position;
			$response->pie["series"]["radius"] = $radius;
			if($version === 3){
				$response->pie["series"]["selectedMode"] = true;
				$response->pie["series"]["label"]["normal"]["show"] = true;
				$response->pie["series"]["label"]["emphasis"]["show"] = true;
				if($type != "normal"){
					$response->pie["series"]["roseType"] = $roseType;
					$response->pie["series"]["avoidLabelOverlap"] = "pie";
				}
				// $response->pie["series"]["label"]["normal"]["position"] = "center";
				// $response->pie["series"]["label"]["emphasis"]["textStyle"]["fontSize"] = "30";
				// $response->pie["series"]["label"]["emphasis"]["textStyle"]["fontWeight"] = "bold";
				$response->pie["series"]["labelLine"]["normal"]["show"] = true;
			}
			$response->pie["series"]["data"] = [];
			$colors = [];
			foreach ($data as $key => $value) {
				$value = (object)$value;
				$value->label = ucwords(strtolower($value->label));
				$response->pie["legend"]["data"][] = $value->label;
				if($version === 3){
					if(isset($value->color)){
							$colors = [
								"normal" => ["color" => $value->color],
							];
					}
					$response->pie["series"]["data"][] = ["name" => $value->label  , "value" => (float)$value->value , 
							"label" => [
								"normal" => ["show" => true , "position" => "outside" , "formatter" => "{b} \n {c} ({d}%)"],
								"emphasis" => ["show" => true , "position" => "outside"],
							],
							"itemStyle" => $colors
					];
				}else{
					// $response->pie["series"]["data"][] = ["name" => $value->label  , "value" => (float)$value->value];
					$response->pie["series"]["data"][] = ["name" => $value->label  , "value" => (float)$value->value,
						"itemStyle" => [
							"normal" => [ "label" => ["formatter" => "{b} \n {c} \n ({d}%)"] ],
							"emphasis" => [ "label" => ["formatter" => "{b} \n {c} \n ({d}%)"] ],
						]
					];
				}
			}
		}

		return $response->pie;
	}

	private function grafica_z($data = [], $group_y = []){
		$options = [];
		$options["tooltip"] = [
			"trigger" => 'axis',
			"axisPointer" => [
				"type" => 'cross',
				"crossStyle" => [
					"color" => '#999'
				]
			]
		];
		$options["toolbox"] = [
			"feature" => [
				"dataView" => [
					"show" => true,
					"readOnly" => false
				],
				"magicType" => [
					"show" => true,
					"type" => ['line', 'bar']
				],
				"restore" => [
					"show" => true
				],
				"saveAsImage" => [
					"show" => true
				]
			]
		];
		$options["legend"]["data"] = [];
		$options["legend"]["bottom"] = "0%";
		$options["legend"]["left"] = "center";
		$options["xAxis"] = [
			[
				"type" => 'category',
				"data" => [],
				"axisPointer" => [
					"type" => 'shadow'
				]
			]
		];
		/*
			[
				type => 'value',
				name => {String},
				min => 0,
				max => 200,
				interval => 5,
				axisLabel => [
					formatter => {value} KG
				]
			]
		*/
		$options["yAxis"] = [];
		/*
			[
				name => {String},
				type => 'line',
				data => [
					{double}, {double}, {double}
				]
			]
		*/
		$options["series"] = [];

		$maxs = [];
		$mins = [];
		$prepare_data = [];
		$_x = [];
		$_names = [];
		$_namess = [];
		foreach($data as $d){
			$d = (object) $d;
			if(!isset($maxs[$d->index_y])) if($d->value > 0)
				$maxs[$d->index_y] = $d->value;
			if($d->value > $maxs[$d->index_y]) if($d->value > 0)
				$maxs[$d->index_y] = $d->value;

			if(!isset($mins[$d->index_y])) if($d->value > 0)
				$mins[$d->index_y] = $d->value;
			if($d->value < $mins[$d->index_y]) if($d->value > 0)
				$mins[$d->index_y] = $d->value;

			if(!in_array($d->label_x, $_x)){
				$_x[] = $d->label_x;
			}
			if(!in_array($d->name, $_namess)){
				$_namess[] = $d->name;
				 
				$n = ["name" => $d->name, "group" => $d->index_y];
				if(isset($d->line)){
					$n["line"] = $d->line;
				}
				$_names[] = $n;
			}
			$prepare_data[$d->label_x][$d->name] = $d->value;
		}

		foreach($group_y as $key => $col){
			$col = (object) $col;
			$options["yAxis"][] = [
				'type' => 'value',
				'name' => $col->name,
				'min' => 'dataMin',
				'axisLabel' => [
					'formatter' => "{value} $col->format"
				]
			];
		}

		foreach($_x as $row){
			$options["xAxis"][0]["data"][] = $row;
		}

		foreach($_names as $name){
			$name = (object) $name;

			if(!in_array($name->name, $options["legend"]["data"]))
				$options["legend"]["data"][] = $name->name;

			$serie = [
				"name" => $name->name,
				"type" => 'line',
				"connectNulls" => true,
				"data" => []
			];
			if($name->group > 0)
				$serie["yAxisIndex"] = $name->group;

			if(isset($name->line)){
				$serie["itemStyle"]["normal"]["lineStyle"]["width"] = 5;
			}

			foreach($_x as $row){
				$val = 0;
				if(isset($prepare_data[$row][$name->name]))
					$val = $prepare_data[$row][$name->name];

				if($val > 0)
					$serie["data"][] = $val;
				else
					$serie["data"][] = null;
			}
			$options["series"][] = $serie;
		}

		return $options;
	}

	private function clear($String){
		$String = str_replace(array('á','à','â','ã','ª','ä'),"a",$String);
	    $String = str_replace(array('Á','À','Â','Ã','Ä'),"A",$String);
	    $String = str_replace(array('Í','Ì','Î','Ï'),"I",$String);
	    $String = str_replace(array('í','ì','î','ï'),"i",$String);
	    $String = str_replace(array('é','è','ê','ë'),"e",$String);
	    $String = str_replace(array('É','È','Ê','Ë'),"E",$String);
	    $String = str_replace(array('ó','ò','ô','õ','ö','º'),"o",$String);
	    $String = str_replace(array('Ó','Ò','Ô','Õ','Ö'),"O",$String);
	    $String = str_replace(array('ú','ù','û','ü'),"u",$String);
	    $String = str_replace(array('Ú','Ù','Û','Ü'),"U",$String);
	    $String = str_replace(array('[','^','´','`','¨','~',']'),"",$String);
	    $String = str_replace("ç","c",$String);
	    $String = str_replace("Ç","C",$String);
	    $String = str_replace("ñ","n",$String);
	    $String = str_replace("Ñ","N",$String);
	    $String = str_replace("Ý","Y",$String);
	    $String = str_replace("ý","y",$String);
	    $String = str_replace(".","",$String);
	    $String = str_replace("&aacute;","a",$String);
	    $String = str_replace("&Aacute;","A",$String);
	    $String = str_replace("&eacute;","e",$String);
	    $String = str_replace("&Eacute;","E",$String);
	    $String = str_replace("&iacute;","i",$String);
	    $String = str_replace("&Iacute;","I",$String);
	    $String = str_replace("&oacute;","o",$String);
	    $String = str_replace("&Oacute;","O",$String);
	    $String = str_replace("&uacute;","u",$String);
	    $String = str_replace("&Uacute;","U",$String);
	    return $String;
	}
	
}
