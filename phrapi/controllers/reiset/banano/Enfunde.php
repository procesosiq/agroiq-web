<?php defined('PHRAPI') or die("Direct access not allowed!");

class Enfunde extends ReactGrafica {
	public $name;
	private $db;
	private $config;

	public function __construct(){
        $this->config = $GLOBALS['config'];
        $this->session = Session::getInstance();
        $this->db = DB::getInstance($this->session->agent_user);
    }
    
    public function params(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $data = (object)[
            "week" => getValueFrom($postdata, 'week', ''),
            "year" => getValueFrom($postdata, 'year', date('y')),
            "sector" => getValueFrom($postdata, 'sector', '', FILTER_SANITIZE_PHRAPI_MYSQL)
        ];
        return $data;
    }

    public function last(){
        $response = new stdClass;
        $params = $this->params();

        $response->weeks = $this->db->queryAll("
            SELECT tbl.anio, tbl.semana, class
            FROM (
                SELECT anio_enfundado AS anio, semana_enfundada AS semana
                FROM produccion_racimos_formularios
                WHERE anio_enfundado > 0 AND semana_enfundada > 0
                GROUP BY anio_enfundado, semana_enfundada
                UNION ALL
                SELECT years AS anio, semana
                FROM produccion_enfunde
                WHERE years > 0 AND semana > 0
                GROUP BY years, semana
            ) tbl
            LEFT JOIN semanas_colores sc ON sc.semana = tbl.semana AND sc.year = tbl.anio
            LEFT JOIN produccion_colores pc ON pc.color = sc.color
            GROUP BY tbl.anio, tbl.semana
            ORDER BY tbl.anio DESC, tbl.semana DESC");

        $response->last_year = $response->weeks[0]->anio;
        $response->last_week = $response->weeks[0]->semana;
        return $response;
    }

	public function index(){
        $response = new stdClass;
        $params = $this->params();

        $sWhere = "";
        if($params->sector != ''){
            $sWhere .= " AND sector = '{$params->sector}'";
        }

        $response->color = $this->db->queryOne("SELECT color FROM semanas_colores WHERE year = {$params->year} AND semana = {$params->week}");
        $response->color = $this->db->queryRow("SELECT * FROM produccion_colores WHERE color = '{$response->color}'");

        $sql = "SELECT lote,
                    IFNULL((SELECT SUM(usadas) FROM produccion_enfunde WHERE years = {$params->year} AND semana = {$params->week} AND lote = tbl.lote {$sWhere}), 0) AS racimos_enfunde
                FROM (
                    SELECT lote
                    FROM produccion_enfunde
                    WHERE years = {$params->year} AND semana = {$params->week} {$sWhere}
                    GROUP BY lote
                    UNION ALL
                    SELECT lote
                    FROM produccion_racimos_formularios
                    WHERE anio_enfundado = {$params->year} AND semana_enfundada = {$params->week} {$sWhere}
                    GROUP BY lote
                ) tbl
                GROUP BY lote";
        $response->data = $this->db->queryAll($sql);
        $response->edades = $this->db->queryAll("SELECT cinta, IFNULL(edad, 'S/C') AS edad FROM produccion_racimos_formularios WHERE anio_enfundado = {$params->year} AND semana_enfundada = {$params->week} {$sWhere} GROUP BY edad ORDER BY edad");

        foreach($response->data as $row){
            foreach($response->edades as $e){
            $row->{"edad_{$e->edad}"} = (int) $this->db->queryOne("SELECT COUNT(1) FROM produccion_racimos_formularios WHERE anio_enfundado = {$params->year} AND semana_enfundada = {$params->week} AND cinta = '{$e->cinta}' AND lote = '{$row->lote}' AND edad = '{$e->edad}' {$sWhere}");
                $row->cosechados += $row->{"edad_{$e->edad}"};
            }
        }

        return $response;
    }
}
