<?php defined('PHRAPI') or die("Direct access not allowed!");

class ProduccionRacimosFormularios {
	public $name;
	private $db;
	private $config;

	public function __construct(){
		$this->session = Session::getInstance();
        $this->db = DB::getInstance($this->session->agent_user);
    }
    
    public function racimosEdad(){
		$postdata = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;
		$filters = (object)[
			"finca" => getValueFrom($postdata , "finca" , "" , FILTER_SANITIZE_STRING),
			"fecha_inicial" => getValueFrom($postdata , "fecha_inicial" , "" , FILTER_SANITIZE_STRING),
            "fecha_final" => getValueFrom($postdata , "fecha_final" , "" , FILTER_SANITIZE_STRING),
            "sector" => getValueFrom($postdata, "sector", "", FILTER_SANITIZE_STRING)
		];

        $sWhere = "";
        if($filters->sector != ''){
            $sWhere .= " AND sector = '{$filters->sector}'";
        }
        if($filters->finca != ''){
            $sWhere .= " AND id_finca = '{$filters->finca}'";
        }
		if($filters->fecha_final != "" && $filters->fecha_inicial != ""){
			$sWhere .= " AND fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}'";
		}

		$sql = "SELECT cinta AS cable, edad AS edad, class,
					(SELECT COUNT(1) FROM produccion_racimos_formularios WHERE tipo = 'RECU' AND cinta = produccion.cinta  {$sWhere}) AS recusados, 
    				(SELECT COUNT(1) FROM produccion_racimos_formularios WHERE tipo = 'PROC' AND cinta = produccion.cinta  {$sWhere}) AS procesados, 
                    (SELECT COUNT(1) FROM produccion_racimos_formularios WHERE tipo = 'PROC' AND cinta = produccion.cinta AND calibre > 0 {$sWhere}) AS muestreados, 
                    (SELECT AVG(peso) FROM produccion_racimos_formularios WHERE tipo = 'PROC' AND cinta = produccion.cinta  {$sWhere}) AS peso,
					(SELECT AVG(calibre_segunda) FROM produccion_racimos_formularios WHERE tipo = 'PROC' AND cinta = produccion.cinta AND cinta != 'n/a' AND calibre_segunda IS NOT NULL AND calibre_segunda > 0 {$sWhere}) AS calibracion,
                    (SELECT AVG(calibre_ultima) FROM produccion_racimos_formularios WHERE tipo = 'PROC' AND cinta = produccion.cinta AND cinta != 'n/a' AND calibre_ultima IS NOT NULL AND calibre_ultima > 0 {$sWhere}) AS calibre_ultima,
					(SELECT AVG(manos) FROM produccion_racimos_formularios WHERE tipo = 'PROC' AND cinta = produccion.cinta AND cinta != 'n/a' AND manos IS NOT NULL AND manos > 0 {$sWhere}) AS manos,
					(SELECT AVG(dedos) FROM produccion_racimos_formularios WHERE tipo = 'PROC' AND cinta = produccion.cinta AND cinta != 'n/a' AND dedos IS NOT NULL AND dedos > 0 {$sWhere}) AS dedos
				FROM(
                    SELECT *
                    FROM (
                        SELECT cinta , produccion.edad, class
                        FROM produccion_racimos_formularios produccion
                        LEFT JOIN produccion_colores colores ON cinta = color
                        WHERE 1 = 1  {$sWhere}
                        GROUP BY cinta
                    ) AS tbl
                    GROUP BY cinta
                ) AS produccion";
		$response->data = $this->db->queryAll($sql);
		foreach($response->data as $i => $val){
			$val->expanded = false;
            $val->cosechados = $val->procesados + $val->recusados;
			$val->lotes = $this->db->queryAll("SELECT lote, 
					(SELECT COUNT(1) FROM produccion_racimos_formularios WHERE tipo = 'RECU' AND cinta = '{$val->cable}' AND lote = produccion.lote {$sWhere}) AS recusados,
					(SELECT COUNT(1) FROM produccion_racimos_formularios WHERE tipo = 'PROC' AND cinta = '{$val->cable}' AND lote = produccion.lote {$sWhere}) AS procesados, 
                    (SELECT COUNT(1) FROM produccion_racimos_formularios WHERE tipo = 'PROC' AND cinta = '{$val->cable}' AND lote = produccion.lote AND calibre > 0 {$sWhere}) AS muestreados, 
					(SELECT AVG(peso) FROM produccion_racimos_formularios WHERE tipo = 'PROC' AND cinta = '{$val->cable}' AND lote = produccion.lote {$sWhere}) AS peso,
					(SELECT AVG(calibre) FROM produccion_racimos_formularios WHERE tipo = 'PROC' AND cinta = '{$val->cable}' AND cinta != 'n/a' AND lote = produccion.lote AND calibre IS NOT NULL AND calibre > 0 {$sWhere}) AS calibracion,
					(SELECT AVG(manos) FROM produccion_racimos_formularios WHERE tipo = 'PROC' AND cinta = '{$val->cable}' AND cinta != 'n/a' AND lote = produccion.lote AND manos IS NOT NULL AND manos > 0 {$sWhere}) AS manos,
					(SELECT AVG(dedos) FROM produccion_racimos_formularios WHERE tipo = 'PROC' AND cinta = '{$val->cable}' AND cinta != 'n/a' AND lote = produccion.lote AND dedos IS NOT NULL AND dedos > 0 {$sWhere}) AS dedos
				FROM(
                    SELECT *
                    FROM(
                        SELECT lote
                        FROM produccion_racimos_formularios
                        WHERE 1=1 AND cinta = '{$val->cable}' {$sWhere}
                        GROUP BY lote
                    ) AS tbl
                    GROUP BY lote
                ) AS produccion");
			foreach($val->lotes as $key => $value){
				$value->expanded = false;
				$value->cosechados = $value->procesados + $value->recusados;

				$value->cuadrillas = $this->db->queryAll("SELECT cuadrilla, 
						(SELECT COUNT(1) FROM produccion_racimos_formularios WHERE tipo = 'RECU' AND cinta = '{$val->cable}' AND lote = '{$value->lote}' AND cuadrilla = produccion.cuadrilla {$sWhere}) AS recusados,
						(SELECT COUNT(1) FROM produccion_racimos_formularios WHERE tipo = 'PROC' AND cinta = '{$val->cable}' AND lote = '{$value->lote}' AND cuadrilla = produccion.cuadrilla {$sWhere}) AS procesados, 
                        (SELECT COUNT(1) FROM produccion_racimos_formularios WHERE tipo = 'PROC' AND cinta = '{$val->cable}' AND lote = '{$value->lote}' AND cuadrilla = produccion.cuadrilla AND calibre > 0 {$sWhere}) AS muestreados, 
						(SELECT AVG(peso) FROM produccion_racimos_formularios WHERE tipo = 'PROC' AND cinta = '{$val->cable}' AND lote = '{$value->lote}' AND cuadrilla = produccion.cuadrilla {$sWhere}) AS peso,
						(SELECT AVG(calibre) FROM produccion_racimos_formularios WHERE tipo = 'PROC' AND cinta = '{$val->cable}' AND cinta != 'n/a' AND lote = '{$value->lote}' AND cuadrilla = produccion.cuadrilla AND calibre IS NOT NULL AND calibre > 0 {$sWhere}) AS calibracion,
						(SELECT AVG(manos) FROM produccion_racimos_formularios WHERE tipo = 'PROC' AND cinta = '{$val->cable}' AND cinta != 'n/a' AND lote = '{$value->lote}' AND cuadrilla = produccion.cuadrilla AND manos IS NOT NULL AND manos > 0 {$sWhere}) AS manos,
						(SELECT AVG(dedos) FROM produccion_racimos_formularios WHERE tipo = 'PROC' AND cinta = '{$val->cable}' AND cinta != 'n/a' AND lote = '{$value->lote}' AND cuadrilla = produccion.cuadrilla AND dedos IS NOT NULL AND dedos > 0 {$sWhere}) AS dedos
					FROM(
					SELECT cuadrilla
					FROM produccion_racimos_formularios
					WHERE 1=1 AND cinta = '{$val->cable}' AND lote = '{$value->lote}' {$sWhere}
					GROUP BY cuadrilla) AS produccion");
				foreach($value->cuadrillas as $row){
					$row->cosechados = $row->procesados + $row->recusados;
				}
			}
		}
		return $response;
    }

	public function lastDay(){
        $response = new stdClass;
        $response->days = $this->db->queryAllOne("SELECT fecha FROM produccion_racimos_formularios WHERE fecha != '0000-00-00' GROUP BY fecha");
		$response->last = $this->db->queryRow("SELECT MAX(fecha) AS fecha FROM produccion_racimos_formularios");
        $response->fincas = $this->db->queryAllSpecial("SELECT id_finca AS id, finca AS label FROM produccion_racimos_formularios WHERE fecha = '{$response->last->fecha}' AND id_finca > 0 GROUP BY id_finca");
        $response->lotes = $this->db->queryAll("SELECT id, nombre FROM lotes ORDER BY nombre");
		return $response;
    }

    public function tags(){
        $response = new stdClass;
        $response->tags = [];
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $filters = (object)[
            "finca" => getValueFrom($postdata , "finca" , "" , FILTER_SANITIZE_STRING),
            "sector" => getValueFrom($postdata , "sector" , "" , FILTER_SANITIZE_STRING),
			"fecha_inicial" => getValueFrom($postdata , "fecha_inicial" , "" , FILTER_SANITIZE_STRING),
            "fecha_final" => getValueFrom($postdata , "fecha_final" , "" , FILTER_SANITIZE_STRING),
        ];

        $fincas = $this->db->queryAllSpecial("SELECT id_finca AS id, finca AS label FROM produccion_racimos_formularios WHERE fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}' GROUP BY id_finca");
        if(!isset($fincas[$filters->finca]) && $filters->finca != ''){
            $filters->finca = array_keys($fincas)[0];
        }

        $sWhere = " AND fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}'";
        if($filters->sector != ""){
            $lotes = $this->db->queryOne("SELECT GROUP_CONCAT(CONCAT(\"'\",nombre,\"'\") SEPARATOR ',') FROM lotes WHERE sector = '{$filters->sector}'");
            $sWhere .= " AND lote IN ($lotes)";
        }
        if($filters->finca != ""){
            $sWhere .= " AND id_finca = $filters->finca";
        }

        $response->tags["procesados"] = $this->db->queryOne("SELECT COUNT(1) FROM produccion_racimos_formularios WHERE tipo = 'PROC' $sWhere");
        $response->tags["recusados"] = $this->db->queryOne("SELECT COUNT(1) FROM produccion_racimos_formularios WHERE tipo = 'RECU' $sWhere");
        $response->tags["cosechados"] = $response->tags["procesados"] + $response->tags["recusados"];
        $response->tags["kg_proc"] = $this->db->queryOne("SELECT SUM(peso) FROM produccion_racimos_formularios WHERE tipo = 'PROC' $sWhere");
        $response->tags["kg_recu"] = $this->db->queryOne("SELECT SUM(peso) FROM produccion_racimos_formularios WHERE tipo = 'RECU' $sWhere");
        $response->tags["kg_prom"] = $this->db->queryOne("SELECT ROUND(AVG(peso), 2) FROM produccion_racimos_formularios WHERE tipo = 'PROC' $sWhere");
        //$response->tags["calibre"] = $this->db->queryOne("SELECT ROUND(AVG(calibre)) FROM produccion_racimos_formularios WHERE 1=1 $sWhere");
        $response->tags["calibre_segunda"] = $this->db->queryOne("SELECT ROUND(AVG(calibre_segunda), 2) FROM produccion_racimos_formularios WHERE 1=1 $sWhere");
        $response->tags["calibre_ultima"] = $this->db->queryOne("SELECT ROUND(AVG(calibre_ultima), 2) FROM produccion_racimos_formularios WHERE 1=1 $sWhere");
        $response->tags["manos"] = $this->db->queryOne("SELECT ROUND(AVG(manos), 2) FROM produccion_racimos_formularios WHERE 1=1 $sWhere");
        $response->tags["edad"] = $this->db->queryOne("SELECT ROUND(AVG(edad), 2) FROM produccion_racimos_formularios WHERE 1=1 $sWhere");
        $response->tags["muestreo"] = $this->db->queryOne("SELECT ROUND(COUNT(1)/{$response->tags["procesados"]}*100, 2) FROM produccion_racimos_formularios WHERE manos IS NOT NULL AND manos > 0 AND tipo = 'PROC' $sWhere");

        $times = $this->db->queryAll("SELECT fecha, hora FROM produccion_racimos_formularios WHERE 1=1 AND hora != '' AND hora != '00:00:00' $sWhere ORDER BY hora");
        $p = $times[0];
        $u = $times[count($times)-1];
        $response->tags["ultima_fecha"] = $u->fecha;
        $response->tags["ultima_hora"] = $u->hora;
        $response->tags["primera_fecha"] = $p->fecha;
        $response->tags["primera_hora"] = $p->hora;
        $response->tags["diferencia"] = $this->db->queryRow("SELECT TIMEDIFF('{$u->hora}', '{$p->hora}') AS dif")->dif;

        return $response;
    }
    
    public function resumen(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $filters = (object)[
            "finca" => getValueFrom($postdata , "finca" , "" , FILTER_SANITIZE_STRING),
            "sector" => getValueFrom($postdata , "sector" , "" , FILTER_SANITIZE_STRING),
			"fecha_inicial" => getValueFrom($postdata , "fecha_inicial" , "" , FILTER_SANITIZE_STRING),
            "fecha_final" => getValueFrom($postdata , "fecha_final" , "" , FILTER_SANITIZE_STRING),
        ];

        $fincas = $this->db->queryAllSpecial("SELECT id_finca AS id, finca AS label FROM produccion_racimos_formularios WHERE fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}' GROUP BY id_finca");
        if(!isset($fincas[$filters->finca]) && $filters->finca != ''){
            $filters->finca = array_keys($fincas)[0];
        }

        $sWhere = " AND fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}'";
        if($filters->sector != ""){
            #$lotes = $this->db->queryOne("SELECT GROUP_CONCAT(CONCAT(\"'\",nombre,\"'\") SEPARATOR ',') FROM lotes WHERE sector = '{$filters->sector}'");
            $sWhere .= " AND sector = '{$filters->sector}'";
        }
        if($filters->finca != ""){
            $sWhere .= " AND id_finca = $filters->finca";
        }


        $sql = "SELECT racimos.edad, cinta, class
                FROM produccion_racimos_formularios racimos
                LEFT JOIN produccion_colores colores ON color = cinta
                WHERE 1=1 {$sWhere}
                GROUP BY edad
                ORDER BY edad";
        $edades = $this->db->queryAll($sql);

        $sql = "SELECT lote, 
                        SUM(IF(tipo = 'PROC', 1, 0)) AS procesados, 
                        SUM(IF(tipo = 'RECU', 1, 0)) AS recusados, 
                        ROUND(AVG(calibre), 2) AS calibre,
                        ROUND(AVG(IF(tipo = 'PROC', calibre_segunda, NULL)), 2) AS calibre_segunda,
                        ROUND(AVG(IF(tipo = 'PROC', calibre_ultima, NULL)), 2) AS calibre_ultima,
                        ROUND(AVG(IF(tipo = 'PROC', manos , NULL)), 2) AS manos,
                        ROUND(AVG(IF(tipo = 'PROC', dedos, NULL)), 2) AS dedos,
                        ROUND(AVG(peso), 2) AS peso
                FROM produccion_racimos_formularios
                WHERE 1=1 $sWhere
                GROUP BY lote
                ORDER BY lote";
        $data = $this->db->queryAll($sql);
        foreach($data as $row){
            foreach($edades as $edad){
                $row->{"edad_{$edad->edad}"} = (int) $this->db->queryOne("SELECT COUNT(1) FROM produccion_racimos_formularios WHERE lote = '{$row->lote}' AND edad = '{$edad->edad}' $sWhere");
            }
        }

        $response->edades = $edades;
        $response->data = $data;
        return $response;
    }

	public function historico(){
		$postdata = (object)json_decode(file_get_contents("php://input"));

		$filters = (object)[
            "finca" => getValueFrom($postdata , "finca" , "" , FILTER_SANITIZE_STRING),
            "sector" => getValueFrom($postdata , "sector" , "" , FILTER_SANITIZE_STRING),
			"fecha_inicial" => getValueFrom($postdata , "fecha_inicial" , "" , FILTER_SANITIZE_STRING),
            "fecha_final" => getValueFrom($postdata , "fecha_final" , "" , FILTER_SANITIZE_STRING),
        ];

        $response = new stdClass;
        $fincas = $this->db->queryAllSpecial("SELECT id_finca AS id, finca AS label FROM produccion_racimos_formularios WHERE fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}' GROUP BY id_finca");
        
        if(!isset($fincas[$filters->finca]) && $filters->finca != ''){
            $filters->finca = array_keys($fincas)[0];
        }
        
        $sWhere = "";
        if($filters->sector != ""){
            $sWhere .= " AND sector = '{$filters->sector}'";
        }
        if($filters->finca != ""){
            $sWhere .= " AND id_finca = $filters->finca";
        }
        
		$response->data = $this->db->queryAll("SELECT historica.id, fecha, lote, causa, peso, IF(tipo = 'RECU', 0 , manos) AS manos, cinta, IF(tipo = 'RECU', 0, dedos) AS dedos, colores.class, historica.edad, cuadrilla, hora, tipo, calibre_segunda, calibre_ultima, viaje, num_racimo, 0 AS 'delete', lote AS '_lote', cuadrilla AS '_cuadrilla', historica.edad AS '_edad'
			FROM produccion_racimos_formularios historica
			LEFT JOIN produccion_colores colores ON colores.color = cinta
            WHERE fecha = '{$filters->fecha_inicial}' $sWhere
            ORDER BY hora");
        
        $response->data_balanza = $this->db->queryAll("SELECT historica.id, fecha, lote, causa, peso, cuadrilla, cinta, num_viaje AS viaje, num_racimo, 0 AS 'delete', lote AS '_lote', cuadrilla AS '_cuadrilla', historica.edad AS '_edad', hora, tipo, causa
			FROM produccion_historica historica
            WHERE fecha = '{$filters->fecha_inicial}' $sWhere
            ORDER BY hora");
        
        $response->viajes = $this->db->queryAll("SELECT *
            FROM (
                SELECT viaje, COUNT(1) racimos, GROUP_CONCAT(DISTINCT lote SEPARATOR ', ') lote, GROUP_CONCAT(DISTINCT cuadrilla SEPARATOR ', ') AS palanca, GROUP_CONCAT(DISTINCT json SEPARATOR ', ') referencias, hora, hora_llegada_servidor, edad AS _edad, lote AS _lote
                FROM produccion_racimos_formularios
                WHERE fecha = '{$filters->fecha_inicial}'
                GROUP BY viaje
            ) tbl
            GROUP BY viaje
            ORDER BY viaje");

        $response->viajes_balanza = $this->db->queryAll("SELECT *
            FROM (
                SELECT num_viaje AS viaje, COUNT(1) racimos, GROUP_CONCAT(DISTINCT lote SEPARATOR ', ') lote, GROUP_CONCAT(DISTINCT cuadrilla SEPARATOR ', ') AS palanca, hora, hora AS hora_llegada_servidor
                FROM produccion_historica
                WHERE fecha = '{$filters->fecha_inicial}' $sWhere
                GROUP BY num_viaje
            ) tbl
            GROUP BY viaje
            ORDER BY viaje");

        $sql = "SELECT *, (racimos_blz - racimos_form) AS diff,
                    IF(palanca_blz = palanca_form AND lote_blz = lote_form, 1, 0) AS valid
                FROM (
                    SELECT 
                        viaje,
                        (SELECT REPLACE(cuadrilla, 'CUA-', '') FROM produccion_historica WHERE fecha = '{$filters->fecha_inicial}' AND num_viaje = tbl.viaje LIMIT 1) palanca_blz,
                        (SELECT cuadrilla FROM produccion_racimos_formularios WHERE fecha = '{$filters->fecha_inicial}' AND viaje = tbl.viaje LIMIT 1) palanca_form,
                        (SELECT lote FROM produccion_historica WHERE fecha = '{$filters->fecha_inicial}' AND num_viaje = tbl.viaje LIMIT 1) lote_blz,
                        (SELECT lote FROM produccion_racimos_formularios WHERE fecha = '{$filters->fecha_inicial}' AND viaje = tbl.viaje LIMIT 1) lote_form,
                        (SELECT COUNT(1) FROM produccion_historica WHERE fecha = '{$filters->fecha_inicial}' AND num_viaje = tbl.viaje) racimos_blz,
                        (SELECT COUNT(1) FROM produccion_racimos_formularios WHERE fecha = '{$filters->fecha_inicial}' AND viaje = tbl.viaje) racimos_form,
                        (SELECT MAX(hora) FROM produccion_historica WHERE fecha = '{$filters->fecha_inicial}' AND num_viaje = tbl.viaje) hora_blz,
                        (SELECT MAX(hora) FROM produccion_racimos_formularios WHERE fecha = '{$filters->fecha_inicial}' AND viaje = tbl.viaje) hora_form
                    FROM (
                        SELECT num_viaje viaje
                        FROM produccion_historica
                        WHERE fecha = '{$filters->fecha_inicial}'
                        GROUP BY viaje
                        UNION ALL
                        SELECT viaje
                        FROM produccion_racimos_formularios
                        WHERE fecha = '{$filters->fecha_inicial}'
                        GROUP BY viaje
                    ) tbl
                    GROUP BY viaje
                ) tbl";
        $response->comparativo = $this->db->queryAll($sql);
		return $response;
    }
    
    public function defectos(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
		$filters = (object)[
            "finca" => getValueFrom($postdata , "finca" , "" , FILTER_SANITIZE_STRING),
            "sector" => getValueFrom($postdata , "sector" , "" , FILTER_SANITIZE_STRING),
			"fecha_inicial" => getValueFrom($postdata , "fecha_inicial" , "" , FILTER_SANITIZE_STRING),
            "fecha_final" => getValueFrom($postdata , "fecha_final" , "" , FILTER_SANITIZE_STRING),
        ];

        $response = new stdClass;
        $fincas = $this->db->queryAllSpecial("SELECT id_finca AS id, finca AS label FROM produccion_racimos_formularios WHERE fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}' GROUP BY id_finca");
        
        if(!isset($fincas[$filters->finca]) && $filters->finca != ''){
            $filters->finca = array_keys($fincas)[0];
        }

        $sWhere = "";
        if($filters->sector != ""){
            $lotes = $this->db->queryOne("SELECT GROUP_CONCAT(CONCAT(\"'\",nombre,\"'\") SEPARATOR ',') FROM lotes WHERE sector = '{$filters->sector}'");
            $sWhere .= " AND lote IN ($lotes)";
        }
        if($filters->finca != ""){
            $sWhere .= " AND id_finca = $filters->finca";
        }

        $sql = "SELECT causa, COUNT(1) AS cantidad
                FROM produccion_racimos_formularios
                WHERE tipo = 'RECU' AND fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}' {$sWhere}
                GROUP BY causa";
        $data = $this->db->queryAll($sql);

        $total_recusados = 0;
        foreach($data as $row){
            $total_recusados += $row->cantidad;
        }
        foreach($data as $row){
            $row->porcentaje = round($row->cantidad / $total_recusados * 100, 2);
        }
        
        $response->data = $data;
        return $response;
    }

    public function eliminar(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        if(count($postdata->ids) > 0){
            foreach($postdata->ids as $reg){
                $this->db->query("DELETE FROM produccion_racimos_formularios WHERE id = $reg->id");
            }
            return true;
        }
        return false;
    }
    
    public function editar(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        if(isset($postdata->id)){
            if($postdata->lote != "" && $postdata->cuadrilla != "" && $postdata->edad > 0 && $postdata->tipo != ""){
                $this->db->query("UPDATE produccion_racimos_formularios SET
                                        peso = '$postdata->peso',
                                        edad = $postdata->edad,
                                        cinta = getCintaFromEdad($postdata->edad, getWeek(fecha), YEAR(fecha)),
                                        cuadrilla = '{$postdata->cuadrilla}',
                                        lote = '{$postdata->lote}',
                                        tipo = '{$postdata->tipo}',
                                        causa = '{$postdata->causa}',
                                        manos = '{$postdata->manos}',
                                        calibre = '{$postdata->calibre}',
                                        dedos = '{$postdata->dedos}'
                                    WHERE id = {$postdata->id}");
            }
            return $this->db->queryRow("SELECT h.id, fecha, lote, causa, peso, manos, calibre, cinta, dedos, h.edad, cuadrilla, hora, tipo, class
                                        FROM produccion_racimos_formularios h
                                        INNER JOIN produccion_colores c ON h.cinta = c.color
                                        WHERE h.id = {$postdata->id}");
        }
        return false;
    }

    public function analizisRecusados(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));

        $filters = (object)[
            "finca" => getValueFrom($postdata , "finca" , "" , FILTER_SANITIZE_STRING),
            "sector" => getValueFrom($postdata , "sector" , "" , FILTER_SANITIZE_STRING),
			"fecha_inicial" => getValueFrom($postdata , "fecha_inicial" , "" , FILTER_SANITIZE_STRING),
            "fecha_final" => getValueFrom($postdata , "fecha_final" , "" , FILTER_SANITIZE_STRING),
        ];

        $sWhere = "";
        if($filters->sector != ""){
            $lotes = $this->db->queryOne("SELECT GROUP_CONCAT(CONCAT(\"'\",nombre,\"'\") SEPARATOR ',') FROM lotes WHERE sector = '{$filters->sector}'");
            $sWhere .= " AND lote IN ($lotes)";
        }
        if($filters->finca != ""){
            $sWhere .= " AND id_finca = $filters->finca";
        }

        if($postdata->var_recusado == 'cant'){
            $sql = "SELECT
                    causa AS dano,
                    SUM(cantidad) AS 'sum',
                    MIN(cantidad) AS 'min',
                    MAX(cantidad) AS 'max',
                    ROUND(AVG(cantidad), 2) AS 'prom',
                    SUM(IF(semana = 0, cantidad, 0)) AS 'sem_0',
                    SUM(IF(semana = 1, cantidad, 0)) AS 'sem_1',
                    SUM(IF(semana = 2, cantidad, 0)) AS 'sem_2',
                    SUM(IF(semana = 3, cantidad, 0)) AS 'sem_3',
                    SUM(IF(semana = 4, cantidad, 0)) AS 'sem_4',
                    SUM(IF(semana = 5, cantidad, 0)) AS 'sem_5',
                    SUM(IF(semana = 6, cantidad, 0)) AS 'sem_6',
                    SUM(IF(semana = 7, cantidad, 0)) AS 'sem_7',
                    SUM(IF(semana = 8, cantidad, 0)) AS 'sem_8',
                    SUM(IF(semana = 9, cantidad, 0)) AS 'sem_9',
                    SUM(IF(semana = 10, cantidad, 0)) AS 'sem_10',
                    SUM(IF(semana = 11, cantidad, 0)) AS 'sem_11',
                    SUM(IF(semana = 12, cantidad, 0)) AS 'sem_12',
                    SUM(IF(semana = 13, cantidad, 0)) AS 'sem_13',
                    SUM(IF(semana = 14, cantidad, 0)) AS 'sem_14',
                    SUM(IF(semana = 15, cantidad, 0)) AS 'sem_15',
                    SUM(IF(semana = 16, cantidad, 0)) AS 'sem_16',
                    SUM(IF(semana = 17, cantidad, 0)) AS 'sem_17',
                    SUM(IF(semana = 18, cantidad, 0)) AS 'sem_18',
                    SUM(IF(semana = 19, cantidad, 0)) AS 'sem_19',
                    SUM(IF(semana = 20, cantidad, 0)) AS 'sem_20',
                    SUM(IF(semana = 21, cantidad, 0)) AS 'sem_21',
                    SUM(IF(semana = 22, cantidad, 0)) AS 'sem_22',
                    SUM(IF(semana = 23, cantidad, 0)) AS 'sem_23',
                    SUM(IF(semana = 24, cantidad, 0)) AS 'sem_24',
                    SUM(IF(semana = 25, cantidad, 0)) AS 'sem_25',
                    SUM(IF(semana = 26, cantidad, 0)) AS 'sem_26',
                    SUM(IF(semana = 27, cantidad, 0)) AS 'sem_27',
                    SUM(IF(semana = 28, cantidad, 0)) AS 'sem_28',
                    SUM(IF(semana = 29, cantidad, 0)) AS 'sem_29',
                    SUM(IF(semana = 30, cantidad, 0)) AS 'sem_30',
                    SUM(IF(semana = 31, cantidad, 0)) AS 'sem_31',
                    SUM(IF(semana = 32, cantidad, 0)) AS 'sem_32',
                    SUM(IF(semana = 33, cantidad, 0)) AS 'sem_33',
                    SUM(IF(semana = 34, cantidad, 0)) AS 'sem_34',
                    SUM(IF(semana = 35, cantidad, 0)) AS 'sem_35',
                    SUM(IF(semana = 36, cantidad, 0)) AS 'sem_36',
                    SUM(IF(semana = 37, cantidad, 0)) AS 'sem_37',
                    SUM(IF(semana = 38, cantidad, 0)) AS 'sem_38',
                    SUM(IF(semana = 39, cantidad, 0)) AS 'sem_39',
                    SUM(IF(semana = 40, cantidad, 0)) AS 'sem_40',
                    SUM(IF(semana = 41, cantidad, 0)) AS 'sem_41',
                    SUM(IF(semana = 42, cantidad, 0)) AS 'sem_42',
                    SUM(IF(semana = 43, cantidad, 0)) AS 'sem_43',
                    SUM(IF(semana = 44, cantidad, 0)) AS 'sem_44',
                    SUM(IF(semana = 45, cantidad, 0)) AS 'sem_45',
                    SUM(IF(semana = 46, cantidad, 0)) AS 'sem_46',
                    SUM(IF(semana = 47, cantidad, 0)) AS 'sem_47',
                    SUM(IF(semana = 48, cantidad, 0)) AS 'sem_48',
                    SUM(IF(semana = 49, cantidad, 0)) AS 'sem_49',
                    SUM(IF(semana = 50, cantidad, 0)) AS 'sem_50',
                    SUM(IF(semana = 51, cantidad, 0)) AS 'sem_51',
                    SUM(IF(semana = 52, cantidad, 0)) AS 'sem_52',
                    SUM(IF(semana = 53, cantidad, 0)) AS 'sem_53'
                FROM (
                    SELECT semana, causa, COUNT(1) AS cantidad
                    FROM produccion_racimos_formularios
                    WHERE tipo = 'RECU' AND year = YEAR('{$postdata->fecha_inicial}') $sWhere
                    GROUP BY semana
                ) AS tbl
                GROUP BY causa
                UNION ALL
                SELECT
                    'TOTAL' AS dano,
                    SUM(cantidad) AS 'sum',
                    MIN(cantidad) AS 'min',
                    MAX(cantidad) AS 'max',
                    ROUND(AVG(cantidad), 2) AS 'prom',
                    SUM(IF(semana = 0, cantidad, 0)) AS 'sem_0',
                    SUM(IF(semana = 1, cantidad, 0)) AS 'sem_1',
                    SUM(IF(semana = 2, cantidad, 0)) AS 'sem_2',
                    SUM(IF(semana = 3, cantidad, 0)) AS 'sem_3',
                    SUM(IF(semana = 4, cantidad, 0)) AS 'sem_4',
                    SUM(IF(semana = 5, cantidad, 0)) AS 'sem_5',
                    SUM(IF(semana = 6, cantidad, 0)) AS 'sem_6',
                    SUM(IF(semana = 7, cantidad, 0)) AS 'sem_7',
                    SUM(IF(semana = 8, cantidad, 0)) AS 'sem_8',
                    SUM(IF(semana = 9, cantidad, 0)) AS 'sem_9',
                    SUM(IF(semana = 10, cantidad, 0)) AS 'sem_10',
                    SUM(IF(semana = 11, cantidad, 0)) AS 'sem_11',
                    SUM(IF(semana = 12, cantidad, 0)) AS 'sem_12',
                    SUM(IF(semana = 13, cantidad, 0)) AS 'sem_13',
                    SUM(IF(semana = 14, cantidad, 0)) AS 'sem_14',
                    SUM(IF(semana = 15, cantidad, 0)) AS 'sem_15',
                    SUM(IF(semana = 16, cantidad, 0)) AS 'sem_16',
                    SUM(IF(semana = 17, cantidad, 0)) AS 'sem_17',
                    SUM(IF(semana = 18, cantidad, 0)) AS 'sem_18',
                    SUM(IF(semana = 19, cantidad, 0)) AS 'sem_19',
                    SUM(IF(semana = 20, cantidad, 0)) AS 'sem_20',
                    SUM(IF(semana = 21, cantidad, 0)) AS 'sem_21',
                    SUM(IF(semana = 22, cantidad, 0)) AS 'sem_22',
                    SUM(IF(semana = 23, cantidad, 0)) AS 'sem_23',
                    SUM(IF(semana = 24, cantidad, 0)) AS 'sem_24',
                    SUM(IF(semana = 25, cantidad, 0)) AS 'sem_25',
                    SUM(IF(semana = 26, cantidad, 0)) AS 'sem_26',
                    SUM(IF(semana = 27, cantidad, 0)) AS 'sem_27',
                    SUM(IF(semana = 28, cantidad, 0)) AS 'sem_28',
                    SUM(IF(semana = 29, cantidad, 0)) AS 'sem_29',
                    SUM(IF(semana = 30, cantidad, 0)) AS 'sem_30',
                    SUM(IF(semana = 31, cantidad, 0)) AS 'sem_31',
                    SUM(IF(semana = 32, cantidad, 0)) AS 'sem_32',
                    SUM(IF(semana = 33, cantidad, 0)) AS 'sem_33',
                    SUM(IF(semana = 34, cantidad, 0)) AS 'sem_34',
                    SUM(IF(semana = 35, cantidad, 0)) AS 'sem_35',
                    SUM(IF(semana = 36, cantidad, 0)) AS 'sem_36',
                    SUM(IF(semana = 37, cantidad, 0)) AS 'sem_37',
                    SUM(IF(semana = 38, cantidad, 0)) AS 'sem_38',
                    SUM(IF(semana = 39, cantidad, 0)) AS 'sem_39',
                    SUM(IF(semana = 40, cantidad, 0)) AS 'sem_40',
                    SUM(IF(semana = 41, cantidad, 0)) AS 'sem_41',
                    SUM(IF(semana = 42, cantidad, 0)) AS 'sem_42',
                    SUM(IF(semana = 43, cantidad, 0)) AS 'sem_43',
                    SUM(IF(semana = 44, cantidad, 0)) AS 'sem_44',
                    SUM(IF(semana = 45, cantidad, 0)) AS 'sem_45',
                    SUM(IF(semana = 46, cantidad, 0)) AS 'sem_46',
                    SUM(IF(semana = 47, cantidad, 0)) AS 'sem_47',
                    SUM(IF(semana = 48, cantidad, 0)) AS 'sem_48',
                    SUM(IF(semana = 49, cantidad, 0)) AS 'sem_49',
                    SUM(IF(semana = 50, cantidad, 0)) AS 'sem_50',
                    SUM(IF(semana = 51, cantidad, 0)) AS 'sem_51',
                    SUM(IF(semana = 52, cantidad, 0)) AS 'sem_52',
                    SUM(IF(semana = 53, cantidad, 0)) AS 'sem_53'
                FROM (
                    SELECT semana, causa, COUNT(1) AS cantidad
                    FROM produccion_racimos_formularios
                    WHERE tipo = 'RECU' AND year = YEAR('{$postdata->fecha_inicial}') $sWhere
                    GROUP BY semana
                ) AS tbl";
            $response->data = $this->db->queryAll($sql);
        }else{
            $sql = "SELECT
                    causa AS dano,
                    SUM(cantidad) AS 'sum',
                    MIN(cantidad) AS 'min',
                    MAX(cantidad) AS 'max',
                    ROUND(AVG(cantidad), 2) AS 'prom',
                    SUM(IF(semana = 0, cantidad, 0)) AS 'sem_0',
                    SUM(IF(semana = 1, cantidad, 0)) AS 'sem_1',
                    SUM(IF(semana = 2, cantidad, 0)) AS 'sem_2',
                    SUM(IF(semana = 3, cantidad, 0)) AS 'sem_3',
                    SUM(IF(semana = 4, cantidad, 0)) AS 'sem_4',
                    SUM(IF(semana = 5, cantidad, 0)) AS 'sem_5',
                    SUM(IF(semana = 6, cantidad, 0)) AS 'sem_6',
                    SUM(IF(semana = 7, cantidad, 0)) AS 'sem_7',
                    SUM(IF(semana = 8, cantidad, 0)) AS 'sem_8',
                    SUM(IF(semana = 9, cantidad, 0)) AS 'sem_9',
                    SUM(IF(semana = 10, cantidad, 0)) AS 'sem_10',
                    SUM(IF(semana = 11, cantidad, 0)) AS 'sem_11',
                    SUM(IF(semana = 12, cantidad, 0)) AS 'sem_12',
                    SUM(IF(semana = 13, cantidad, 0)) AS 'sem_13',
                    SUM(IF(semana = 14, cantidad, 0)) AS 'sem_14',
                    SUM(IF(semana = 15, cantidad, 0)) AS 'sem_15',
                    SUM(IF(semana = 16, cantidad, 0)) AS 'sem_16',
                    SUM(IF(semana = 17, cantidad, 0)) AS 'sem_17',
                    SUM(IF(semana = 18, cantidad, 0)) AS 'sem_18',
                    SUM(IF(semana = 19, cantidad, 0)) AS 'sem_19',
                    SUM(IF(semana = 20, cantidad, 0)) AS 'sem_20',
                    SUM(IF(semana = 21, cantidad, 0)) AS 'sem_21',
                    SUM(IF(semana = 22, cantidad, 0)) AS 'sem_22',
                    SUM(IF(semana = 23, cantidad, 0)) AS 'sem_23',
                    SUM(IF(semana = 24, cantidad, 0)) AS 'sem_24',
                    SUM(IF(semana = 25, cantidad, 0)) AS 'sem_25',
                    SUM(IF(semana = 26, cantidad, 0)) AS 'sem_26',
                    SUM(IF(semana = 27, cantidad, 0)) AS 'sem_27',
                    SUM(IF(semana = 28, cantidad, 0)) AS 'sem_28',
                    SUM(IF(semana = 29, cantidad, 0)) AS 'sem_29',
                    SUM(IF(semana = 30, cantidad, 0)) AS 'sem_30',
                    SUM(IF(semana = 31, cantidad, 0)) AS 'sem_31',
                    SUM(IF(semana = 32, cantidad, 0)) AS 'sem_32',
                    SUM(IF(semana = 33, cantidad, 0)) AS 'sem_33',
                    SUM(IF(semana = 34, cantidad, 0)) AS 'sem_34',
                    SUM(IF(semana = 35, cantidad, 0)) AS 'sem_35',
                    SUM(IF(semana = 36, cantidad, 0)) AS 'sem_36',
                    SUM(IF(semana = 37, cantidad, 0)) AS 'sem_37',
                    SUM(IF(semana = 38, cantidad, 0)) AS 'sem_38',
                    SUM(IF(semana = 39, cantidad, 0)) AS 'sem_39',
                    SUM(IF(semana = 40, cantidad, 0)) AS 'sem_40',
                    SUM(IF(semana = 41, cantidad, 0)) AS 'sem_41',
                    SUM(IF(semana = 42, cantidad, 0)) AS 'sem_42',
                    SUM(IF(semana = 43, cantidad, 0)) AS 'sem_43',
                    SUM(IF(semana = 44, cantidad, 0)) AS 'sem_44',
                    SUM(IF(semana = 45, cantidad, 0)) AS 'sem_45',
                    SUM(IF(semana = 46, cantidad, 0)) AS 'sem_46',
                    SUM(IF(semana = 47, cantidad, 0)) AS 'sem_47',
                    SUM(IF(semana = 48, cantidad, 0)) AS 'sem_48',
                    SUM(IF(semana = 49, cantidad, 0)) AS 'sem_49',
                    SUM(IF(semana = 50, cantidad, 0)) AS 'sem_50',
                    SUM(IF(semana = 51, cantidad, 0)) AS 'sem_51',
                    SUM(IF(semana = 52, cantidad, 0)) AS 'sem_52',
                    SUM(IF(semana = 53, cantidad, 0)) AS 'sem_53'
                FROM (
                    SELECT semana, causa, ROUND(COUNT(1)/(SELECT COUNT(1) FROM produccion_racimos_formularios WHERE semana = main.semana AND YEAR = YEAR('{$postdata->fecha_inicial}'))*100, 2) AS cantidad
                    FROM produccion_racimos_formularios main
                    WHERE tipo = 'RECU' AND YEAR = YEAR('{$postdata->fecha_inicial}') $sWhere
                    GROUP BY semana
                ) AS tbl
                GROUP BY causa";
            $totales = $this->db->queryRow("SELECT tbl.*, 'TOTAL' AS dano, SUM(SUM) AS 'sum', SUM(MIN) AS 'min', SUM(MAX) AS 'max', SUM(prom) AS 'prom'
                FROM (
                    SELECT
                        'TOTAL' AS dano,
                        '' AS 'sum',
                        '' AS 'min',
                        '' AS 'max',
                        '' AS 'prom',
                        SUM(IF(semana = 0, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos_formularios WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 1)*100 AS 'sem_0',
                        SUM(IF(semana = 1, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos_formularios WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 1)*100 AS 'sem_1',
                        SUM(IF(semana = 2, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos_formularios WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 2)*100 AS 'sem_2',
                        SUM(IF(semana = 3, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos_formularios WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 3)*100 AS 'sem_3',
                        SUM(IF(semana = 4, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos_formularios WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 4)*100 AS 'sem_4',
                        SUM(IF(semana = 5, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos_formularios WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 5)*100 AS 'sem_5',
                        SUM(IF(semana = 6, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos_formularios WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 6)*100 AS 'sem_6',
                        SUM(IF(semana = 7, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos_formularios WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 7)*100 AS 'sem_7',
                        SUM(IF(semana = 8, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos_formularios WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 8)*100 AS 'sem_8',
                        SUM(IF(semana = 9, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos_formularios WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 9)*100 AS 'sem_9',
                        SUM(IF(semana = 10, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos_formularios WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 10)*100 AS 'sem_10',
                        SUM(IF(semana = 11, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos_formularios WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 11)*100 AS 'sem_11',
                        SUM(IF(semana = 12, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos_formularios WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 12)*100 AS 'sem_12',
                        SUM(IF(semana = 13, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos_formularios WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 13)*100 AS 'sem_13',
                        SUM(IF(semana = 14, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos_formularios WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 14)*100 AS 'sem_14',
                        SUM(IF(semana = 15, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos_formularios WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 15)*100 AS 'sem_15',
                        SUM(IF(semana = 16, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos_formularios WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 16)*100 AS 'sem_16',
                        SUM(IF(semana = 17, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos_formularios WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 17)*100 AS 'sem_17',
                        SUM(IF(semana = 18, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos_formularios WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 18)*100 AS 'sem_18',
                        SUM(IF(semana = 19, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos_formularios WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 19)*100 AS 'sem_19',
                        SUM(IF(semana = 20, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos_formularios WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 20)*100 AS 'sem_20',
                        SUM(IF(semana = 21, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos_formularios WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 21)*100 AS 'sem_21',
                        SUM(IF(semana = 22, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos_formularios WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 22)*100 AS 'sem_22',
                        SUM(IF(semana = 23, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos_formularios WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 23)*100 AS 'sem_23',
                        SUM(IF(semana = 24, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos_formularios WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 24)*100 AS 'sem_24',
                        SUM(IF(semana = 25, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos_formularios WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 25)*100 AS 'sem_25',
                        SUM(IF(semana = 26, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos_formularios WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 26)*100 AS 'sem_26',
                        SUM(IF(semana = 27, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos_formularios WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 27)*100 AS 'sem_27',
                        SUM(IF(semana = 28, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos_formularios WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 28)*100 AS 'sem_28',
                        SUM(IF(semana = 29, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos_formularios WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 29)*100 AS 'sem_29',
                        SUM(IF(semana = 30, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos_formularios WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 30)*100 AS 'sem_30',
                        SUM(IF(semana = 31, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos_formularios WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 31)*100 AS 'sem_31',
                        SUM(IF(semana = 32, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos_formularios WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 32)*100 AS 'sem_32',
                        SUM(IF(semana = 33, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos_formularios WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 33)*100 AS 'sem_33',
                        SUM(IF(semana = 34, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos_formularios WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 34)*100 AS 'sem_34',
                        SUM(IF(semana = 35, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos_formularios WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 35)*100 AS 'sem_35',
                        SUM(IF(semana = 36, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos_formularios WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 36)*100 AS 'sem_36',
                        SUM(IF(semana = 37, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos_formularios WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 37)*100 AS 'sem_37',
                        SUM(IF(semana = 38, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos_formularios WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 38)*100 AS 'sem_38',
                        SUM(IF(semana = 39, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos_formularios WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 39)*100 AS 'sem_39',
                        SUM(IF(semana = 40, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos_formularios WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 40)*100 AS 'sem_40',
                        SUM(IF(semana = 41, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos_formularios WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 41)*100 AS 'sem_41',
                        SUM(IF(semana = 42, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos_formularios WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 42)*100 AS 'sem_42',
                        SUM(IF(semana = 43, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos_formularios WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 43)*100 AS 'sem_43',
                        SUM(IF(semana = 44, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos_formularios WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 44)*100 AS 'sem_44',
                        SUM(IF(semana = 45, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos_formularios WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 45)*100 AS 'sem_45',
                        SUM(IF(semana = 46, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos_formularios WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 46)*100 AS 'sem_46',
                        SUM(IF(semana = 47, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos_formularios WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 47)*100 AS 'sem_47',
                        SUM(IF(semana = 48, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos_formularios WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 48)*100 AS 'sem_48',
                        SUM(IF(semana = 49, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos_formularios WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 49)*100 AS 'sem_49',
                        SUM(IF(semana = 50, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos_formularios WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 50)*100 AS 'sem_50',
                        SUM(IF(semana = 51, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos_formularios WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 51)*100 AS 'sem_51',
                        SUM(IF(semana = 52, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos_formularios WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 52)*100 AS 'sem_52',
                        SUM(IF(semana = 53, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos_formularios WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 53)*100 AS 'sem_53'
                    FROM (
                        SELECT semana, causa, COUNT(1) AS cantidad
                        FROM produccion_racimos_formularios
                        WHERE tipo = 'RECU' AND YEAR = YEAR('{$postdata->fecha_inicial}') $sWhere
                        GROUP BY causa, semana
                    ) AS tbl
                ) AS tbl
                GROUP BY dano");
            $row_totales_gropales = $this->db->queryRow("SELECT SUM(recusados)/SUM(cortados)*100 AS 'sum', MIN(cantidad) AS 'min', MAX(cantidad) AS 'max', AVG(cantidad) AS 'prom'
                FROM (
                    SELECT semana, SUM(recusados) AS recusados, SUM(cortados) AS cortados, IF(recusados > 0, recusados/cortados*100, NULL) AS cantidad
                    FROM (
                        SELECT semana, COUNT(1) AS cortados, SUM(IF(tipo = 'RECU', 1, 0)) AS recusados
                        FROM produccion_racimos_formularios
                        WHERE YEAR = YEAR('{$postdata->fecha_inicial}') $sWhere
                        GROUP BY semana
                    ) AS tbl
                    GROUP BY semana
                ) AS tbl");

            $totales->prom = $row_totales_gropales->prom;
            $totales->sum = $row_totales_gropales->sum;
            $totales->min = $row_totales_gropales->min;
            $totales->max = $row_totales_gropales->max;
            $response->data = $this->db->queryAll($sql);
            $response->data[] = $totales;
        }
        $response->semanas = $this->db->queryAllOne("SELECT semana FROM produccion_racimos_formularios WHERE tipo = 'RECU' AND YEAR = YEAR('{$postdata->fecha_inicial}') $sWhere GROUP BY semana");
        return $response;
    }

    private function getSemanaVar($var, $year, $id_finca){
        $sql = [];
        for($x = 1; $x <= 52; $x++){
            $sql[] = "SELECT sem_{$x} value, {$x} label_x FROM produccion_resumen_tabla WHERE variable = '{$var}' AND anio = '{$year}' AND id_finca = {$id_finca} AND sem_{$x} IS NOT NULL";
        }
        return implode("
        UNION ALL
        ", $sql);
    }

    public function muestreo(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $filters = (object)[
            "finca" => getValueFrom($postdata , "finca" , "" , FILTER_SANITIZE_STRING),
            "fecha_inicial" => getValueFrom($postdata , "fecha_inicial" , "" , FILTER_SANITIZE_STRING),
        ];

        $year = $this->db->queryOne("SELECT getYear('{$filters->fecha_inicial}')");

        $sql = "SELECT 
                    label_x,
                    value,
                    0 AS index_y, 
                    'MUESTREO' AS 'name'
                FROM (
                    {$this->getSemanaVar('MUESTREO', $year, 1)}
                ) tbl
                ORDER BY label_x";
        $data_chart = $this->db->queryAll($sql);
        $semanas = array();
        foreach ($data_chart as $key => $row)
        {
            $row = (object) $row;
            $semanas[$key] = (int) $row->label_x;
        }
        array_multisort($semanas, SORT_ASC, $data_chart);

        $groups = [
			[
				"name" => '',
				"type" => 'line',
				'format' => '%'
			]
        ];
        $response->data = $this->grafica_z($data_chart, $groups, $selected);
        return $response;
    }

    private function grafica_z($data = [], $group_y = [], $selected = [], $types = []){
		$options = [];
		$options["tooltip"] = [
			"trigger" => 'axis',
			"axisPointer" => [
				"type" => 'cross',
				"crossStyle" => [
					"color" => '#999'
				]
			]
		];
		$options["toolbox"] = [
			"feature" => [
				"dataView" => [
					"show" => true,
					"readOnly" => false
				],
				"magicType" => [
					"show" => true,
					"type" => ['line', 'bar']
				],
				"restore" => [
					"show" => true
				],
				"saveAsImage" => [
					"show" => true
				]
			]
		];
		$options["legend"]["data"] = [];
		$options["legend"]["bottom"] = "0%";
        $options["legend"]["left"] = "center";
        $options["legend"]["selected"] = $selected;
		$options["xAxis"] = [
			[
				"type" => 'category',
				"data" => [],
				"axisPointer" => [
					"type" => 'shadow'
				]
			]
		];
		/*
			[
				type => 'value',
				name => {String},
				min => 0,
				max => 200,
				interval => 5,
				axisLabel => [
					formatter => {value} KG
				]
			]
		*/
		$options["yAxis"] = [];
		/*
			[
				name => {String},
				type => 'line',
				data => [
					{double}, {double}, {double}
				]
			]
		*/
		$options["series"] = [];

		$maxs = [];
		$mins = [];
		$prepare_data = [];
		$_x = [];
		$_names = [];
		$_namess = [];
		foreach($data as $d){
			$d = (object) $d;
			if(!isset($maxs[$d->index_y])) if($d->value > 0)
				$maxs[$d->index_y] = $d->value;
			if($d->value > $maxs[$d->index_y]) if($d->value > 0)
				$maxs[$d->index_y] = $d->value;

			if(!isset($mins[$d->index_y])) if($d->value > 0)
				$mins[$d->index_y] = $d->value;
			if($d->value < $mins[$d->index_y]) if($d->value > 0)
				$mins[$d->index_y] = $d->value;

			if(!in_array($d->label_x, $_x)){
				$_x[] = $d->label_x;
			}
			if(!in_array($d->name, $_namess)){
				$_namess[] = $d->name;
				 
				$n = ["name" => $d->name, "group" => $d->index_y];
				if(isset($d->line)){
					$n["line"] = $d->line;
				}
				$_names[] = $n;
			}
			$prepare_data[$d->label_x][$d->name] = $d->value;
        }

		foreach($group_y as $key => $col){
			$col = (object) $col;
			$options["yAxis"][] = [
				'type' => 'value',
				'name' => $col->name,
				//'max' => ($key == 1) ? $maxs[$key] + ($maxs[$key] - $mins[$key]) * .05 : null,
                //'min' => ($key == 1) ? $mins[$key] - ($maxs[$key] - $mins[$key]) * .05 : null,
                'max' => null,
                'min' => 'dataMin',
				'axisLabel' => [
					'formatter' => "{value} $col->format"
				]
			];
		}

		foreach($_x as $row){
			$options["xAxis"][0]["data"][] = $row;
        }

		foreach($_names as $i => $name){
			$name = (object) $name;

			if(!in_array($name->name, $options["legend"]["data"]))
				$options["legend"]["data"][] = $name->name;

			$serie = [
				"name" => $name->name,
				"type" => isset($types[$i]) ? $types[$i] : 'line',
				"connectNulls" => true,
                "data" => [],
                "itemStyle" => [
                    "normal" => [
                        "barBorderRadius" => "0",
                        "barBorderWidth" => "6",
                        "label" => [
                            "show" => true
                        ]
                    ]
                ]
			];
			if($name->group > 0)
				$serie["yAxisIndex"] = $name->group;

			if(isset($name->line)){
				$serie["itemStyle"]["normal"]["lineStyle"]["width"] = 5;
			}

			foreach($_x as $row){
				$val = 0;
				if(isset($prepare_data[$row][$name->name]))
					$val = $prepare_data[$row][$name->name];

				if($val > 0)
					$serie["data"][] = $val;
				else
					$serie["data"][] = null;
			}
			$options["series"][] = $serie;
		}

		return $options;
    }

    public function editarVariableViaje(){
        $postdata = (object)json_decode(file_get_contents("php://input"));

        if($postdata->viaje > 0 && $postdata->fecha != '')
        if($postdata->valor != '' && $postdata->variable != '' && in_array($postdata->variable, ['lote', 'cuadrilla'])){
            $sql = "UPDATE produccion_racimos_formularios SET {$postdata->variable} = {$postdata->valor} WHERE fecha = '{$postdata->fecha}' AND viaje = '{$postdata->viaje}'";
            $this->db->query($sql);
        }
    }

    public function borrarViaje(){
        $postdata = (object)json_decode(file_get_contents("php://input"));

        if($postdata->viaje > 0 && $postdata->fecha != ''){
            $sql = "DELETE FROM produccion_racimos_formularios WHERE fecha = '{$postdata->fecha}' AND viaje = $postdata->viaje";
            $this->db->query($sql);
            return true;
        }

        return false;
    }

    public function borrarViajeBalanza(){
        $postdata = (object)json_decode(file_get_contents("php://input"));

        if($postdata->viaje > 0 && $postdata->fecha != ''){
            $sql = "DELETE FROM produccion_historica WHERE fecha = '{$postdata->fecha}' AND num_viaje = $postdata->viaje";
            $this->db->query($sql);
            return true;
        }

        return false;
    }

    public function agregarRacimo(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        if(!empty($postdata->fecha) && !empty($postdata->lote) && !empty($postdata->viaje) && !empty($postdata->cinta) && !empty($postdata->cuadrilla)){

            $semana = $this->db->queryOne("SELECT getWeek('{$postdata->fecha}')");
            $anio = $this->db->queryOne("SELECT getYear('{$postdata->fecha}')");
            $peso_prom = $this->db->queryOne("SELECT AVG(peso) FROM produccion_racimos_formularios WHERE fecha = '{$postdata->fecha}' AND cinta = '{$postdata->cinta}'");
            if(!$peso_prom) $peso_prom = $this->db->queryOne("SELECT AVG(peso) FROM produccion_racimos_formularios WHERE fecha = '{$postdata->fecha}'");
            $num_racimo = $this->db->queryOne("SELECT IFNULL(MAX(num_racimo), 0)+1 FROM produccion_racimos_formularios WHERE fecha = '{$postdata->fecha}' AND viaje = {$postdata->viaje}");
            if(!$num_racimo) $num_racimo = 1;
            $edad = $this->db->queryOne("SELECT getEdadCinta($semana, '{$postdata->cinta}', $anio)");

            $sql = "INSERT INTO produccion_racimos_formularios SET 
                        fecha = '{$postdata->fecha}',
                        hora = DATE_ADD(CURRENT_TIMESTAMP, INTERVAL 2 HOUR),
                        semana = $semana,
                        year = $anio,
                        lote = '{$postdata->lote}', 
                        sector = (SELECT sector FROM lotes WHERE nombre = '{$postdata->lote}'),
                        id_finca = (SELECT idFinca FROM lotes WHERE nombre = '{$postdata->lote}'),
                        finca = (SELECT fincas.nombre FROM lotes INNER JOIN fincas ON fincas.id = idFinca WHERE lotes.nombre = '{$postdata->lote}'),
                        viaje = $postdata->viaje, 
                        num_racimo = $num_racimo,
                        cinta = '{$postdata->cinta}', 
                        edad = $edad,
                        cuadrilla = '{$postdata->cuadrilla}',
                        peso = $peso_prom,
                        tipo = IF('{$postdata->causa}' = '', 'PROC', 'RECU'),
                        causa = '{$postdata->causa}',
                        semana_enfundada = getSemanaEnfundada($edad, $semana, $anio),
                        anio_enfundado = IF(getSemanaEnfundada($edad, $semana, $anio) > $semana, $anio-1, $anio)";
            $this->db->query($sql);
            return true;
        }

        return false;
    }

    public function procesarRacimos(){
        $sql = "INSERT INTO ";
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $fecha = $postdata->fecha;
        $semena = $this->db->queryOne("SELECT getWeek('$fecha')");
        $year = $this->db->queryOne("SELECT getYear('$fecha')");
        $viajes = $this->db->queryAll("SELECT viaje FROM (
                SELECT viaje FROM produccion_racimos_formularios WHERE fecha = '{$fecha}' GROUP BY viaje
                UNION ALL
                SELECT num_viaje viaje FROM produccion_historica WHERE fecha = '{$fecha}' GROUP BY num_viaje
            ) tbl
            GROUP BY viaje
            ORDER BY viaje");
        foreach($viajes as $viaje){
            $blz = (int) $this->db->queryOne("SELECT COUNT(1) FROM produccion_historica WHERE fecha = '{$fecha}' AND num_viaje = $viaje->viaje");
            $form = (int) $this->db->queryOne("SELECT COUNT(1) FROM produccion_racimos_formularios WHERE fecha = '{$fecha}' AND viaje = $viaje->viaje");

            if($blz != $form){
                if($blz > $form){
                    for($i = 1; $i < ($blz-$form); $i++){
                        $racimo = $this->db->queryRow("SELECT * FROM produccion_historica WHERE fecha = '{$fecha}' AND num_viaje = $viaje->viaje AND num_racimo = $i");
                        $sql = "INSERT INTO produccion_racimos_formularios SET
                                    id_finca = $racimo->id_finca,
                                    finca = '{$racimo->finca}',
                                    fecha = '{$fecha}',
                                    hora = TIME(CURRENT_TIMESTAMP),
                                    semana = $semana,
                                    year = $year,
                                    lote = '{$racimo->lote}',
                                    sector = (SELECT sector FROM lotes WHERE nombre = '{$racimo->lote}' AND idFinca = $racimo->id_finca),
                                    cuadrilla = REPLACE('{$racimo->cuadrilla}', 'CUA-', ''),
                                    viaje = $viaje->viaje,
                                    num_racimo = $i,
                                    peso = $racimo->peso,
                                    cinta = 'S/C',
                                    tipo = 'PROC'";
                        D($sql);
                    }
                }

                if($form > $blz){
                    for($i = 1; $i < ($form-$blz); $i++){
                        $sql = "UPDATE produccion_racimos_formularios SET
                                    peso = (SELECT AVG(peso) FROM produccion_racimos_formularios WHERE fecha = '{$fecha}' AND peso > 0)
                                WHERE viaje = $viaje->viaje AND num_racimo = $i AND fecha = '{$fecha}'";
                        D($sql);
                    }
                }
            }
        }

        $response = new stdClass;
        $response->status = 200;
        return $response;
    }
}
