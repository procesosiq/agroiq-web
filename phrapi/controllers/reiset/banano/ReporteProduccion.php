<?php defined('PHRAPI') or die("Direct access not allowed!");

class ReporteProduccion {
	public $name;
	private $db;
	private $config;

	public function __construct(){
		$this->config = $GLOBALS['config'];
		$this->session = Session::getInstance();
        $this->db = DB::getInstance($this->session->agent_user);
    }

    public function last(){
        $response = new stdClass;
        $response->years = $this->db->queryAllOne("SELECT YEAR FROM produccion_racimos_formularios WHERE edad > 0 AND peso > 0 GROUP BY year ORDER BY year");
        $lastYear = $response->years[count($response->years)-1];
        $response->last_year = $lastYear;
        $response->weeks = $this->db->queryAllOne("SELECT semana FROM produccion_racimos_formularios WHERE year = {$lastYear} GROUP BY semana ORDER BY semana");
        $response->last_week = $this->db->queryOne("SELECT MAX(semana) FROM produccion_racimos_formularios WHERE year = {$lastYear}");
        #$response->fecha_inicial = "{$lastYear}-01-01";
        #$response->fecha_final = "{$lastYear}-12-31";
        return $response;
    }
    
    public function getWeeks(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));

        $response->semanas = $this->db->queryAllOne("SELECT semana FROM produccion_racimos_formularios WHERE edad > 0 AND peso > 0 AND year = $postdata->year GROUP BY semana");
        return $response;
    }

	public function main(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));
        if($postdata->fecha_inicial && $postdata->fecha_final){
            $sWhere .= " AND fecha BETWEEN '{$postdata->fecha_inicial}' AND '{$postdata->fecha_final}'";
        }
        if($postdata->sector != ''){
            $sWhereSector .= " AND sector = '{$postdata->sector}'";
        }
        if($postdata->year != '' && $postdata->semana != ''){
            $sWhere .= " AND year = $postdata->year AND semana = $postdata->semana";
        }

        $sql = "SELECT sector
                FROM produccion_racimos_formularios
                WHERE 1=1 $sWhere AND sector != ''
                GROUP BY sector";
        $sectores = $this->db->queryAllOne($sql);

        $sql = "SELECT lote, COUNT(1) AS cosechados, SUM(peso ) AS peso, SUM(IF(tipo = 'RECU', 1, 0)) AS recusados, SUM(IF(tipo = 'PROC', 1, 0)) AS procesados
                FROM produccion_racimos_formularios
                WHERE 1=1 $sWhere $sWhereSector
                GROUP BY lote";
        $data = $this->db->queryAll($sql);

        $peso_cajas = $this->db->queryOne("SELECT SUM(lb) FROM produccion_cajas WHERE 1=1 $sWhere");
        $peso_racimos_procesados = $this->db->queryOne("SELECT SUM(peso) FROM produccion_racimos_formularios WHERE 1=1 $sWhere");
        $total_merma_lb_porc = ($peso_racimos_procesados - $peso_cajas);

        $response->totales = [
            "peso_cajas" => $peso_cajas,
            "peso_racimos_procesados" => $peso_racimos_procesados,
            "merma_proc_lb" => $total_merma_lb_porc,
            "total_peso_racimos_procesados" => 0,
            "total_racimos_procesados" => 0
        ];
        $response->edades = [];
        foreach($data as $row){
            // EDADES
            $edades = $this->getEdades($row->lote);
            foreach($edades as $ed){
                if(!in_array($ed, $response->edades)) $response->edades[] = (int) $ed;
                $row->{"edad_{$ed}"} = $this->db->queryOne("SELECT 
                        COUNT(1)
                    FROM produccion_racimos_formularios 
                    WHERE lote = '{$row->lote}' AND edad = '{$ed}' $sWhere");
                $response->totales["edad_{$ed}"] += $row->{"edad_{$ed}"};
            }

            $row->hectareas = $this->db->queryOne("SELECT IFNULL(SUM(area), 1) FROM lotes WHERE nombre = '{$row->lote}'");
            $row->peso_prom_racimo = $this->db->queryOne("SELECT ROUND(AVG(peso), 2) FROM produccion_racimos_formularios WHERE lote = '{$row->lote}' AND tipo = 'PROC' $sWhere");
            $row->peso_racimos_cosechados = $this->db->queryOne("SELECT SUM(peso) FROM produccion_racimos_formularios WHERE lote = '{$row->lote}' $sWhere");
            $row->peso_racimos_recusados = $this->db->queryOne("SELECT SUM(peso) FROM produccion_racimos_formularios WHERE lote = '{$row->lote}' AND tipo = 'RECU' $sWhere");
            $row->peso_racimos_procesados = $row->peso_racimos_cosechados - $row->peso_racimos_recusados;
            

            // COLUMNAS DE CAJAS (CALCULOS)
            $row->merma_lb_cosechada = $row->peso_racimos_cosechados - $peso_cajas;
            $row->merma_lb_procesada = ($row->peso_racimos_cosechados - $row->peso_racimos_recusados) - $peso_cajas;
            $row->porcentaje_distribucion_merma = ($row->peso_racimos_procesados / $peso_racimos_procesados);
            $row->merma_lb_proc = ($row->porcentaje_distribucion_merma * $total_merma_lb_porc);
            $row->merma_lb_cosec = ($row->merma_lb_proc - $row->peso_racimos_recusados);
            $row->convertidas = round(($row->peso_racimos_cosechados - $row->merma_lb_cosec) / 41.5, 0);
            $row->cajas_lb = $row->peso_racimos_cosechados - $row->merma_lb_proc;
            $row->cajas_ha = round($row->convertidas / $row->hectareas, 0);
            $row->ratio_cortado = round($row->convertidas / $row->cosechados, 2);
            $row->ratio_procesado = round($row->convertidas / $row->procesados, 2);
            $row->cajas_ha_proyeccion = round($row->cajas_ha * 52, 2);

            // TOTALES
            $response->totales["total_racimos_cosechados"] += $row->cosechados;
            $response->totales["total_racimos_procesados"] += $row->procesados;
            $response->totales["total_racimos_recusados"] += $row->recusados;
            $response->totales["total_convertidas"] += $row->convertidas;
            $response->totales["total_peso_racimos_procesados"] += $row->peso_racimos_procesados;
        }

        $response->sectores = $sectores;
        $response->data = $data;
        sort($response->edades, SORT_NUMERIC);
        return $response;
    }

    private function getEdades($lote){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        if($postdata->fecha_inicial && $postdata->fecha_final){
            $sWhere .= " AND fecha BETWEEN '{$postdata->fecha_inicial}' AND '{$postdata->fecha_final}'";
        }
        if($postdata->year != '' && $postdata->semana != ''){
            $sWhere .= " AND year = $postdata->year AND semana = $postdata->semana";
        }

        $sql = "SELECT edad
                FROM produccion_racimos_formularios
                WHERE lote = '{$lote}' AND edad IS NOT NULL $sWhere
                GROUP BY edad
                ORDER BY edad";
        return $this->db->queryAllOne($sql);
    }

    public function reporte2(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        if($postdata->fecha_inicial && $postdata->fecha_final){
            $sWhere .= " AND fecha BETWEEN '{$postdata->fecha_inicial}' AND '{$postdata->fecha_final}'";
            $sWhereM .= " AND date_fecha BETWEEN '{$postdata->fecha_inicial}' AND '{$postdata->fecha_final}'";
        }
        if($postdata->sector != ''){
            $sWhereSector .= " AND sector = '{$postdata->sector}'";
        }
        if($postdata->year != '' && $postdata->semana != ''){
            $sWhere .= " AND year = $postdata->year AND semana = $postdata->semana";
            $sWhere2 .= " AND years = $postdata->year AND semana = $postdata->semana";
            $sWhereM .= " AND getYear(date_fecha) = $postdata->year AND getWeek(date_fecha) = $postdata->semana";
        }

        $sql = "SELECT lote, COUNT(1) AS cosechados, SUM(peso) AS peso, SUM(IF(tipo = 'RECU', 1, 0)) AS recusados, SUM(IF(tipo = 'PROC', 1, 0)) AS procesados, ROUND(AVG(manos), 2) AS manos, ROUND(AVG(calibre_segunda), 2) AS calibre, ROUND(AVG(dedos), 2) AS dedos
                FROM produccion_racimos_formularios
                WHERE lote != '' $sWhere $sWhereSector
                GROUP BY lote";
        $data = $this->db->queryAll($sql);

        $peso_cajas = $this->db->queryOne("SELECT SUM(lb) FROM produccion_cajas WHERE 1=1 $sWhere");
        $peso_racimos_cosechados = $this->db->queryOne("SELECT SUM(peso) FROM produccion_racimos_formularios WHERE 1=1 $sWhere");
        $total_merma_lb_porc = ($peso_racimos_cosechados - $peso_cajas);

        foreach($data as $row){
            $row->hectareas = $this->db->queryOne("SELECT IFNULL(SUM(area), 1) FROM lotes WHERE nombre = '{$row->lote}'");
            $sql = "SELECT IFNULL(ROUND(SUM(usadas), 0), '')
                    FROM produccion_enfunde
                    INNER JOIN lotes ON lote = lotes.nombre
                    WHERE lote = '{$row->lote}' $sWhere2";
            $row->enfunde = $this->db->queryOne($sql);
            $row->enfunde_ha = round($row->enfunde / $row->hectareas, 0);

            /*$sql = "SELECT 
					ROUND((SELECT SUM(cantidad) 
                        FROM merma 
                        INNER JOIN lotes ON bloque = lotes.nombre
                        INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
                        WHERE porcentaje_merma > 0 
                            AND bloque = '{$row->lote}'
                            AND campo = 'Total peso Merma'
                            $sWhereM $sWhereSector) / SUM(peso_neto) * 100, 2) AS merma_neta
				FROM merma 
                INNER JOIN lotes ON bloque = lotes.nombre
                WHERE porcentaje_merma > 0 AND bloque = '{$row->lote}' $sWhereM $sWhereSector";
            $row->merma_total = $this->db->queryOne($sql);*/
            $row->responsable = $this->db->queryOne("SELECT responsable FROM lotes WHERE nombre = '{$row->lote}'");

            // COLUMNAS DE CAJAS (CALCULOS)
            $row->racimos_recu_ha = round($row->recusados / $row->hectareas, 0);
            $row->racimos_proc_ha = round($row->procesados / $row->hectareas, 0);
            $row->porc_racimos_recu = round($row->recusados / $row->cosechados * 100, 2);
            $row->racimo_ha = round($row->cosechados / $row->hectareas, 0);
            $row->peso_racimos_cosechados = $this->db->queryOne("SELECT SUM(peso) FROM produccion_racimos_formularios WHERE lote = '{$row->lote}' $sWhere");
            $row->peso_prom_racimo = $this->db->queryOne("SELECT ROUND(AVG(peso), 2) FROM produccion_racimos_formularios WHERE lote = '{$row->lote}' AND tipo = 'PROC' $sWhere");
            $row->peso_racimos_recusados = $this->db->queryOne("SELECT SUM(peso) FROM produccion_racimos_formularios WHERE lote = '{$row->lote}' AND tipo = 'RECU' $sWhere");
            $row->peso_racimos_procesados = $row->peso_racimos_cosechados - $row->peso_racimos_recusados;
            $row->merma_lb_cosechada = $row->peso_racimos_cosechados - $peso_cajas;
            $row->merma_lb_procesada = ($row->peso_racimos_cosechados - $row->peso_racimos_recusados) - $peso_cajas;
            $row->porcentaje_distribucion_merma = ($row->peso_racimos_procesados / $peso_racimos_cosechados);
            $row->merma_lb_proc = ($row->porcentaje_distribucion_merma * $total_merma_lb_porc);
            $row->merma_lb_cosec = ($row->merma_lb_proc + $row->peso_racimos_recusados);
            $row->merma_total = round($row->merma_lb_cosec / $row->peso_racimos_procesados, 2);
            $row->convertidas = round(($row->peso_racimos_cosechados - $row->merma_lb_cosec) / 41.5, 0);
            $row->cajas_lb = $row->peso_racimos_cosechados - $row->merma_lb_proc;
            $row->cajas_ha = round($row->convertidas / $row->hectareas, 0);
            $row->ratio_cortado = round($row->convertidas / $row->cosechados, 2);
            $row->ratio_procesado = round($row->convertidas / $row->procesados, 2);
            $row->cajas_ha_proyeccion = round($row->cajas_ha * 52, 2);
        }

        $response->data = $data;
        return $response;
    }
}
