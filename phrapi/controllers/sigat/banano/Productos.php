<?php defined('PHRAPI') or die("Direct access not allowed!");

	class Productos 
	{
		private $db;
		private $session;
		
		public function __construct()
		{
			$this->config = $GLOBALS['config'];
        	$this->session = Session::getInstance();
        	$this->db = DB::getInstance($this->session->db);
		}

		public function index(){
			$response = new stdClass;
			$response->success = 400;
			$response->data = "";
			$data = $this->params();

			$sql = "SELECT 
				IFNULL((SELECT nombre FROM cat_formuladoras WHERE id = id_formuladora),'') AS formuladora, 
				IFNULL((SELECT nombre FROM cat_proveedores WHERE id = id_proveedor),'') AS proveedor, 
				IFNULL((SELECT nombre FROM cat_tipo_productos WHERE id = id_tipo_producto),'') AS tipo_producto, 
				nombre_comercial AS nombreComercial,
				ingrediente_activo AS ingrediente_activo,
				frac AS frac,
				accion AS action,
				id AS id_producto,
				id AS codigo
				FROM products";
			$datos = $this->db->queryAll($sql);
			foreach ($datos as $key => $value) {
				$value = (object)$value;
				$value->codigo = (int)$value->codigo;
			}

			return $datos;
		}

		public function params(){
			$data = (object)json_decode(file_get_contents("php://input"));
			$data->id_producto = (int)$data->id_producto;
			if(isset($data->precio)){
				$data->precio = (double)$data->precio;
			}
			return $data;
		}

		public function create(){
			$response = new stdClass;
			$response->success = 400;
			$response->data = "";
			$data = $this->params();
			if($data->nombreComercial != ""){
				$sql = "INSERT INTO `products` SET
						id_formuladora = '{$data->formuladora}',
						id_proveedor = '{$data->proveedor}',
						id_tipo_producto = '{$data->tipo_producto}',
						nombre_comercial = '{$data->nombreComercial}',
						ingrediente_activo = '{$data->ingrediente_activo}',
						frac = '{$data->frac}',
						dosis = '{$data->dosis}',
						dosis_2 = '{$data->dosis_2}',
						dosis_3 = '{$data->dosis_3}',
						accion = '{$data->action}'";
				$this->db->query($sql);
				$ids = $this->db->getLastID();
				if($ids > 0){
					$response->success = 200;
					$response->data = $ids;
				}
			}

			return $response;
		}

		public function update(){
			$response = new stdClass;
			$response->success = 400;
			$response->data = "";
			$data = $this->params();
			if($data->id_producto > 0){
				$sql = "UPDATE `products` SET
						id_formuladora = '{$data->formuladora}',
						id_proveedor = '{$data->proveedor}',
						id_tipo_producto = '{$data->tipo_producto}',
						nombre_comercial = '{$data->nombreComercial}',
						ingrediente_activo = '{$data->ingrediente_activo}',
						frac = '{$data->frac}',
						dosis = '{$data->dosis}',
						dosis_2 = '{$data->dosis_2}',
						dosis_3 = '{$data->dosis_3}',
						accion = '{$data->action}'
						WHERE id = $data->id_producto";
				$this->db->query($sql);
				if($data->id_producto > 0){
					$response->success = 200;
					$response->data = $data->id_producto;
				}
			}
			return $response;
		}

		public function show(){
			$response = new stdClass;
			$response->success = 400;
			$response->data = "";
			$data = $this->params();
			if($data->id_producto > 0){
				$sql = "SELECT 
						id_formuladora AS formuladora, 
						id_proveedor AS proveedor, 
						id_tipo_producto AS tipo_producto, 
						nombre_comercial AS nombreComercial,
						ingrediente_activo AS ingrediente_activo,
						frac AS frac,
						accion AS action,
						id AS id_producto,
						id AS codigo,
						dosis,
						dosis_2,
						dosis_3
					FROM products
					WHERE id = $data->id_producto";
				$response->success = 200;
				$response->data = $res = $this->db->queryRow($sql);
			}
			return $response;
		}

		public function getPrecios(){
			$response = new stdClass;
			$response->success = 400;
			$response->data = [];
			$data = $this->params();
			if($data->id_producto > 0){
				$sql = "SELECT id , precio , fecha FROM products_price WHERE id_producto = $data->id_producto";
				$res = $this->db->queryAll($sql);
				foreach($res as $fila){
					$fila->precio = (double)$fila->precio;
					$fila->id = (int)$fila->id;
					$response->data[] = $fila;
				}
			}

			return $response;
		}

		public function savePrecios(){
			$response = new stdClass;
			$data = $this->params();
			if($data->id_producto > 0 && $data->precio > 0){
				$fecha = "CURRENT_DATE";
				if($data->fecha != ""){
					$fecha = "'{$data->fecha}'";
				}
				$sql = "INSERT INTO products_price SET id_producto = {$data->id_producto} , precio = '{$data->precio}' ,  fecha = $fecha";
				$res = $this->db->query($sql);
				return $this->getPrecios();
			}
			return ["error" => 500];
		}

		public function changeStatus(){
			extract($_POST);
			if((int)$id > 0 ){
				$status = ($estado == 'activo') ? 1 : 0;
				$sql = "UPDATE cat_gerentes SET status={$status} WHERE id = {$id} AND id_usuario = '{$this->session->logged}'";
				// echo $sql;
				$this->conexion->Consultas(1,$sql);
        		return $id;
			}
		}

		public function getFormuladoras(){
			$formuladoras = [];
			$sql = "SELECT id , nombre as label FROM cat_formuladoras WHERE status > 0";
			$formuladoras = $this->db->queryAllSpecial($sql);

			return $formuladoras;
		}

        public function getProveedores(){
			$proveedores = [];
			$sql = "SELECT id , nombre as label FROM cat_proveedores WHERE status > 0";
			$proveedores = $this->db->queryAllSpecial($sql);

			return $proveedores;
		}

        public function getTipoProductos(){
			$tipo_productos = [];
			$sql = "SELECT id , nombre as label FROM cat_tipo_productos WHERE status > 0";
			$tipo_productos = $this->db->queryAllSpecial($sql);

			return $tipo_productos;
		}

		public function getFrac(){
			$frac = [];
			$sql = "SELECT frac AS id , frac AS label FROM products WHERE frac != '' GROUP BY frac";
			$frac = $this->db->queryAllSpecial($sql);

			return $frac;
		}

		public function getAccion(){
			$accion = [];
			$sql = "SELECT accion AS id , accion AS label FROM products WHERE accion != '' GROUP BY accion";
			$accion = $this->db->queryAllSpecial($sql);

			return $accion;
		}

		private function getFincas($id_gerente){
			$selected = "";
			if($id_gerente){
				$selected = ", IF(id_gerente = '{$id_gerente}' , 'selected' , '') AS selected";
			}
			$cliente = [];
			$sql = "SELECT cat_fincas.id , cat_fincas.nombre  , 
					IF(id_gerente > 0 OR id_gerente != '' , 'Fincas con Gerentes' , 'Fincas sin Gerentes') AS grupo
					,id_gerente  {$selected}
					FROM cat_fincas 
					LEFT JOIN cat_gerentes ON cat_fincas.id_gerente = cat_gerentes.id
					WHERE cat_fincas.status > 0 
					ORDER BY id_gerente , nombre";
			$cliente = $this->db->queryAll($sql);

			return $cliente;
		}

		public  function saveConfiguracion(){
			$response = new stdClass;
			$response->success = 400;
			$response->data = [];
			$data = $this->params();
			if($data->modo == "formuladora"){
				$sql = "INSERT INTO cat_formuladoras SET nombre = '{$data->campo}'";
			}
			if($data->modo == "proveedor"){
				$sql = "INSERT INTO cat_proveedores SET nombre = '{$data->campo}'";
			}
			if($data->modo == "tipo_producto"){
				$sql = "INSERT INTO cat_tipo_productos SET nombre = '{$data->campo}'";
			}
			$this->db->query($sql);
			$ids = (int) $this->db->getLastID();

			return $ids;
		}
	}
?>
