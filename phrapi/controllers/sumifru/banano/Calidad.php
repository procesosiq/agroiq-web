<?php defined('PHRAPI') or die("Direct access not allowed!");

class Calidad {
	public $name;
	private $db;
	private $config;

	public function __construct(){
		$this->config = $GLOBALS['config'];
		$this->session = Session::getInstance();
        $this->maindb = DB::getInstance();
        $this->db = DB::getInstance($this->session->agent_user);
    }
    
    public function last(){
        $response = new stdClass;

        $response->fecha = $this->db->queryOne("SELECT MAX(fecha) FROM calidad");

        return $response;
    }

    public function empaque(){
        $response = new stdClass;

        $filters = $this->ProccessParams();
        $sWhere = "";
        if($filters->finca != ""){
			$sWhere .= " AND finca = '{$filters->finca}'";
		}
		if($filters->idExportador > 0){
			$sWhere .= " AND id_exportador = '{$filters->idExportador}'";
		}
		if($filters->idCliente > 0){
			$sWhere .= " AND id_cliente = '{$filters->idCliente}'";
		}
		if($filters->idMarca > 0){
			$sWhere .= " AND id_marca = '{$filters->idMarca}'";
		}
		if($filters->contenedor != ""){
			$sWhere .= " AND TRIM(UPPER(contenedor)) = '{$filters->contenedor}'";
        }
        if($filters->fecha_inicial  != "" && $filters->fecha_final != ""){
            $sWhere .= " AND fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}'";
        }

        $sql = "SELECT detalle AS label, SUM(cantidad) AS value
                FROM calidad
                INNER JOIN calidad_empaque ON id_calidad = calidad.`id`
                WHERE cantidad > 0 $sWhere
                GROUP BY detalle";
        $data_chart = $this->db->queryAll($sql);
        $response->chart = $this->pie($data_chart, ["0%", "60%"], "", "area", "normal", true);

        return $response;
    }

    public function pesoCluster(){
        $response = new stdClass;
        $filters = $this->ProccessParams();
        $sWhere = "";
        if($filters->finca != ""){
			$sWhere .= " AND finca = '{$filters->finca}'";
		}
		if($filters->idExportador > 0){
			$sWhere .= " AND id_exportador = '{$filters->idExportador}'";
		}
		if($filters->idCliente > 0){
			$sWhere .= " AND id_cliente = '{$filters->idCliente}'";
		}
		if($filters->idMarca > 0){
			$sWhere .= " AND id_marca = '{$filters->idMarca}'";
		}
		if($filters->contenedor != ""){
			$sWhere .= " AND TRIM(UPPER(contenedor)) = '{$filters->contenedor}'";
        }
        /*if($filters->fecha_inicial  != "" && $filters->fecha_final != ""){
            $sWhere .= " AND fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}'";
        }*/

        $sql = "SELECT *
                FROM (
                    SELECT semana AS label_x, 'PROM' AS name, ROUND(AVG(cantidad), 2) AS value, '0' AS index_y
                    FROM calidad
                    INNER JOIN calidad_pesos_cluster ON id_calidad = calidad.`id`
                    WHERE 1=1 $sWhere
                    GROUP BY semana
                    UNION ALL
                    SELECT semana AS label_x, 'MIN' AS name, ROUND(MIN(cantidad), 2) AS value, '0' AS index_y
                    FROM calidad
                    INNER JOIN calidad_pesos_cluster ON id_calidad = calidad.`id`
                    WHERE 1=1 $sWhere
                    GROUP BY semana
                    UNION ALL
                    SELECT semana AS label_x, 'MAX' AS name, ROUND(MAX(cantidad), 2) AS value, '0' AS index_y
                    FROM calidad
                    INNER JOIN calidad_pesos_cluster ON id_calidad = calidad.`id`
                    WHERE 1=1 $sWhere
                    GROUP BY semana
                ) AS tbl
                ORDER BY label_x";
        $data_chart = $this->db->queryAll($sql);
        $groups = [
            [
                "name" => 'g',
                "type" => 'line',
                'format' => ''
            ]
        ];
        $response->chart = $this->grafica_z($data_chart, $groups);

        ## DATATABLE
        $semanas = $this->db->queryAllOne("SELECT semana FROM calidad INNER JOIN calidad_pesos_cluster ON id_calidad = calidad.id WHERE 1=1 $sWhere GROUP BY semana");
        $data_table = [
            [ "variable" => "PROM" ],
            [ "variable" => "MAX" ],
            [ "variable" => "MIN" ],
        ];
        foreach($semanas as $sem){
            $values = $this->db->queryRow("SELECT ROUND(AVG(cantidad), 2) AS prom, ROUND(MAX(cantidad)) AS 'max', ROUND(MIN(cantidad)) AS 'min' FROM calidad INNER JOIN calidad_pesos_cluster ON id_calidad = calidad.id WHERE semana = $sem $sWhere");
            $data_table[0]["sem_{$sem}"] = (float) $values->prom;
            $data_table[1]["sem_{$sem}"] = (float) $values->max;
            $data_table[2]["sem_{$sem}"] = (float) $values->min;

            /* PROM TOTAL */
            if($data_table[0]["sem_{$sem}"] > 0){
                $data_table[0]["sum"] += $data_table[0]["sem_{$sem}"];
                $data_table[0]["count"] += 1;
                $data_table[0]["prom_total"] = round(($data_table[0]["sum"] / $data_table[0]["count"]), 2);
            }
            if($data_table[1]["sem_{$sem}"] > 0){
                $data_table[1]["sum"] += $data_table[1]["sem_{$sem}"];
                $data_table[1]["count"] += 1;
                $data_table[1]["prom_total"] = round(($data_table[1]["sum"] / $data_table[1]["count"]), 2);
            }
            if($data_table[2]["sem_{$sem}"] > 0){
                $data_table[2]["sum"] += $data_table[2]["sem_{$sem}"];
                $data_table[2]["count"] += 1;
                $data_table[2]["prom_total"] = round(($data_table[2]["sum"] / $data_table[2]["count"]), 2);
            }
        }

        $response->table = $data_table;
        $response->semanas = $semanas;

        return $response;
    }

    public function fotos(){
        $response = new stdClass;

        $filters = $this->ProccessParams();
        $sWhere = "";
        if($filters->finca != ""){
			$sWhere .= " AND finca = '{$filters->finca}'";
		}
		if($filters->idExportador > 0){
			$sWhere .= " AND id_exportador = '{$filters->idExportador}'";
		}
		if($filters->idCliente > 0){
			$sWhere .= " AND id_cliente = '{$filters->idCliente}'";
		}
		if($filters->idMarca > 0){
			$sWhere .= " AND id_marca = '{$filters->idMarca}'";
		}
		if($filters->contenedor != ""){
			$sWhere .= " AND TRIM(UPPER(contenedor)) = '{$filters->contenedor}'";
        }
        if($filters->fecha_inicial  != "" && $filters->fecha_final != ""){
            $sWhere .= " AND fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}'";
        }

        $fincas = $this->db->queryAll("SELECT id_finca, finca FROM calidad WHERE 1=1 $sWhere GROUP BY id_finca");
        foreach($fincas as $fin){
            $fin->total_fotos = 0;
            $fin->semanas = $this->db->queryAll("SELECT semana FROM calidad WHERE id_finca = $fin->id_finca $sWhere GROUP BY semana");
            foreach($fin->semanas as $sem){
                $sem->total_fotos = 0;
                $sem->marcas = $this->db->queryAll("SELECT id_marca, marca FROM calidad WHERE id_finca = $fin->id_finca AND semana = $sem->semana $sWhere GROUP BY id_marca");
                foreach($sem->marcas as $marca){
                    $marca->fotos = $this->db->queryAll("SELECT type, json, DATE(fecha) AS fecha, calidad_cluster, path FROM calidad INNER JOIN calidad_images ON id_calidad = calidad.id WHERE id_finca = $fin->id_finca AND semana = $sem->semana AND id_marca = $marca->id_marca $sWhere");
                    // TOTAL FOTOS
                    $sem->total_fotos += count($marca->fotos);
                    $fin->total_fotos += count($marca->fotos);
                }
            }
        }

        $response->data = $fincas;
        return $response;
    }

	// 22/02/2017 - TAG: Peticion de Indicadores
	public function index(){
		$filters = $this->ProccessParams();
		$response = new stdClass;
		$response->tags = $this->tags($filters);
		$response->filters = $this->getFilters($filters);
		return $response;
	}
	// 22/02/2017 - TAG: Peticion de la Grafica Principal
	public function Principal(){
		$filters = $this->ProccessParams();
		$response = new stdClass;	
		$principal = $this->HistoricaCalidad($filters);
		$response->calidad->data = $principal->data;
		$response->calidad->legend = $principal->legend;
		$response->calidad->avg = $principal->legend;

		$principal = $this->HistoricaDesviacionEstandar($filters);
		$response->desviacion->data = $principal->data;
		$response->desviacion->legend = $principal->legend;
		$response->desviacion->avg = $principal->legend;

		$principal = $this->HistoricaPesoCaja($filters);
		$response->peso_caja->data = $principal->data;
		$response->peso_caja->legend = $principal->legend;
		$response->peso_caja->avg = $principal->legend;

		$principal = $this->HistoricaDedos($filters);
		$response->dedos->data = $principal->data;
		$response->dedos->legend = $principal->legend;
		$response->dedos->avg = $principal->legend;

		$principal = $this->HistoricaCluster($filters);
		$response->cluster->data = $principal->data;
		$response->cluster->legend = $principal->legend;
		$response->cluster->avg = $principal->legend;
		return $response;
	}
	// 22/02/2017 - TAG: Peticion de la Grafica ClusterCaja
	public function historicaClusterCaja(){
		$filters = $this->ProccessParams();
		$response = new stdClass;	
		$historica = $this->HistoricaCategorias($filters);
		$response->data = $historica->data;
		$response->legend = $historica->legend;
		$response->avg = $historica->legend;
		return $response;
	}
	// 22/02/2017 - TAG: Peticion de la Tabla promedio de dedos
	public function tablePromedioDedos(){
		$filters = $this->ProccessParams();
		$response = new stdClass;	
		$table = $this->promedioDedos($filters);
		$response = $table->data;
		return $response;
	}
	// 22/02/2017 - TAG: Peticion de la Tabla promedio de clusters por caja
	public function tablePromedioClusters(){
		$filters = $this->ProccessParams();
		$response = new stdClass;	
		$table = $this->promedioClusters($filters);
		$response = $table->data;
		return $response;
	}
	// 23/02/2017 - TAG: Peticion Grafica de Defectos
	public function graficaDanhos(){
		$filters = $this->ProccessParams();
		$response = new stdClass;
		$Defectos = $this->graphicDanhos($filters);
		$response->data = $Defectos->data;
		$response->categorias = $Defectos->categorias;
		$response->categoriasFilters = $Defectos->categoriasFilters;
		return $response;
	}

	public function graficaDefectos(){
		$filters = $this->ProccessParams();
		$response = new stdClass;
		$Defectos = $this->graphicDefectos($filters);
		$response->data = $Defectos->data;
		$response->categorias = $Defectos->categorias;
		$response->categoriasFilters = $Defectos->categoriasFilters;
		return $response;
	}

	// 24/02/2017 - TAG: Metodos de subfincas
	// 24/02/2017 - TAG: Peticion de la Tabla de Sub Fincas
	public function getTableSubFinca(){
		$filters = $this->ProccessParams();
		$response = new stdClass;	
		$data = $this->getDataSubFinca($filters);
		$response->defectos = $data->defectos;
		$response->muestras = $data->muestras;
		$response->totales = $data->totales;
		return $response;
	}

	private function getDataSubFinca($filters = []){
		$inicio = $filters->fecha_inicial;
		$fin = $filters->fecha_final;
		$sWhere = "";

		if($filters->idFinca > 0){
			$sWhere .= " AND id_finca = '{$filters->idFinca}' ";
		}

		$response = new stdClass;
		$response->defectos = [];
		$response->muestras = [];
		$response->totales = [];
		$data = [];

		$sql = "SELECT 
					id ,
					peso_caja AS peso,  
					cantidad_cluster_caja AS num_cluster,
					cantidad_dedos_caja AS num_dedos,
					danhos_por_dedo AS for_dedos,
					danhos_por_cluster AS for_cluster,
					calidad_dedos,
					calidad_cluster 
				FROM calidad
				WHERE
					1 = 1 {$sWhere} AND
					fecha BETWEEN '{$inicio}' AND '{$fin}'
				ORDER BY fecha";
		$data = $this->db->queryAll($sql);
		foreach ($data as $key => $value) {
			$value->id = (double)$value->id;
			$value->peso = (double)$value->peso;
			$value->num_cluster = (double)$value->num_cluster;
			$value->num_dedos = (double)$value->num_dedos;
			$value->for_dedos = (double)$value->for_dedos;
			$value->for_cluster = (double)$value->for_cluster;
			$value->calidad_dedos = (double)$value->calidad_dedos;
			$value->calidad_cluster = (double)$value->calidad_cluster;
			$response->muestras[$value->id] = $value;
			$response->muestras[$value->id]->defectos = $this->getDefectosTable($value->id);
		}

		$response->totales = $this->db->queryRow("SELECT 
					id ,
					AVG(peso_caja) AS peso,  
					AVG(cantidad_cluster_caja) AS num_cluster,
					AVG(cantidad_dedos_caja) AS num_dedos,
					AVG(danhos_por_dedo) AS for_dedos,
					AVG(danhos_por_cluster) AS for_cluster,
					AVG(calidad_dedos) AS calidad_dedos,
					AVG(calidad_cluster) AS  calidad_cluster,
					'' AS defectos
				FROM calidad
				WHERE
				1 = 1 {$sWhere} AND
				fecha BETWEEN '{$inicio}' AND '{$fin}'");
		
		$response->totales->defectos = $this->db->queryAllSpecial("SELECT 
										IFNULL((SELECT siglas FROM calidad_defectos WHERE nombre = details.campo AND id_categoria = details.id_categoria),'S/DF') AS id , 
										SUM(cantidad) AS label 
										FROM calidad_detalle AS details
										INNER JOIN calidad ON details.id_calidad = calidad.id
										WHERE
										1 = 1 {$sWhere} AND
										fecha BETWEEN '{$inicio}' AND '{$fin}'
										GROUP BY IFNULL((SELECT siglas FROM calidad_defectos WHERE nombre = details.campo AND id_categoria = details.id_categoria),'S/DF')
										ORDER BY fecha");
		$response->defectos = $this->db->queryAllSpecial("SELECT 
										IFNULL((SELECT siglas FROM calidad_defectos WHERE nombre = details.campo AND id_categoria = details.id_categoria) ,'S/DF') AS id , 
										IFNULL(campo,'S/DF') AS label
										FROM calidad_detalle AS details
										INNER JOIN calidad ON details.id_calidad = calidad.id
										WHERE 1 = 1 {$sWhere} AND fecha BETWEEN '{$inicio}' AND '{$fin}'
										GROUP BY IFNULL((SELECT siglas FROM calidad_defectos WHERE nombre = details.campo AND id_categoria = details.id_categoria),'S/DF')
										ORDER BY fecha");

		return $response;
	}

	private function getDefectosTable($idCalidad = []){

		$sql = "SELECT 
					IFNULL((SELECT siglas FROM calidad_defectos WHERE nombre = details.campo AND id_categoria = details.id_categoria),'S/DF') AS id , 
					cantidad AS label 
				FROM calidad_detalle AS details
				WHERE 1 = 1 AND id_calidad = {$idCalidad}";
		$response = $this->db->queryAllSpecial($sql);
		return $response;
	}

	// 24/02/2017 - TAG: Metodos de subfincas
	// 22/02/2017 - TAG: Sanitize Filters
	private function ProccessParams(){
		$postdata = (object)json_decode(file_get_contents("php://input"));

		$response = (object)[
			"idFinca" => getValueFrom($postdata , "idFinca" , 1 , FILTER_SANITIZE_PHRAPI_INT),
			"idFincaDia" => getValueFrom($postdata , "idFincaDia" , 1 , FILTER_SANITIZE_PHRAPI_INT),
			"idExportador" => getValueFrom($postdata , "idExportador" , 1 , FILTER_SANITIZE_PHRAPI_INT),
			"idCliente" => getValueFrom($postdata , "idCliente" , 1 , FILTER_SANITIZE_PHRAPI_INT),
			"idMarca" => getValueFrom($postdata , "idMarca" , 1 , FILTER_SANITIZE_PHRAPI_INT),
			"contenedor" => getValueFrom($postdata , "contenedor" , "" , FILTER_SANITIZE_STRING),
			"fecha_inicial" => getValueFrom($postdata , "fecha_inicial" , "" , FILTER_SANITIZE_STRING),
			"fecha_final" => getValueFrom($postdata , "fecha_final" , "" , FILTER_SANITIZE_STRING),
			"year" => getValueFrom($postdata , "year" , 2017 , FILTER_SANITIZE_PHRAPI_INT),
			"finca" => getValueFrom($position, "finca", "", FILTER_SANITIZE_STRING)
		];
		
		$response = $postdata;
		$response->id_company = (int)$this->session->id_company;

		return $response;
	}
	// 23/02/2017 - TAG: Filtros [Fincas , Sub Fincas]
	private function getFilters($filters = []){
		$response = new stdClass;

		$inicio = $filters->fecha_inicial;
		$fin = $filters->fecha_final;

		$response->fincas = ["" => "Todas"] + $this->db->queryAllSpecial("SELECT finca as label, finca as id FROM calidad 
																			WHERE fecha BETWEEN '{$inicio}' AND '{$fin}'
																			GROUP BY finca");
		/*$response->exportadores = ["" => "Todos"] + $this->db->queryAllSpecial("
												SELECT (SELECT nombre FROM exportadores WHERE id = id_exportador) as label, 
													id_exportador as id 
												FROM calidad 
												WHERE fecha BETWEEN '{$inicio}' AND '{$fin}'
												GROUP BY id_exportador");

		$response->clientes = ["" => "Todos"] + $this->db->queryAllSpecial("
															SELECT (SELECT nombre FROM clients WHERE id = id_cliente) as label, 
																id_cliente as id 
															FROM calidad 
															WHERE fecha BETWEEN '{$inicio}' AND '{$fin}'
															GROUP BY id_cliente");
		
		$response->marcas = ["" => "Todos"] + $this->db->queryAllSpecial("
															SELECT (SELECT nombre FROM brands WHERE id = id_marca) as label, 
																id_marca as id 
															FROM calidad 
															WHERE fecha BETWEEN '{$inicio}' AND '{$fin}' 
															GROUP BY id_marca");*/
		
		$response->contenedores = ["" => "Todos"] + $this->db->queryAllSpecial("SELECT UPPER(TRIM(contenedor)) AS id , UPPER(TRIM(contenedor)) AS label FROM calidad 
																				WHERE fecha BETWEEN '{$inicio}' AND '{$fin}' AND TRIM(contenedor) != ''
																				GROUP BY TRIM(UPPER(contenedor))");
		return $response;
	}
	// 22/02/2017 - TAG: Indicadores
	private function tags($filters = []){
		$inicio = $filters->fecha_inicial;
		$fin = $filters->fecha_final;
		$sWhere = "";

		if($filters->finca != ""){
			$sWhere .= " AND finca = '{$filters->finca}' ";
		}
		if($filters->idExportador > 0){
			$sWhere .= " AND id_exportador = '{$filters->idExportador}'";
		}
		if($filters->idCliente > 0){
			$sWhere .= " AND id_cliente = '{$filters->idCliente}'";
		}
		if($filters->idMarca > 0){
			$sWhere .= " AND id_marca = '{$filters->idMarca}'";
		}
		if($filters->contenedor != ""){
			$sWhere .= " AND TRIM(UPPER(contenedor)) = '{$filters->contenedor}'";
		}
		$sql = "SELECT MAX(calidad_dedos) AS calidad_maxima FROM calidad WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND calidad_dedos > 0  $sWhere";
		$rp2 = $this->db->queryRow($sql);
		$sql = "SELECT MIN(calidad_dedos) AS calidad_minima FROM calidad WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND calidad_dedos > 0 $sWhere";
		$rp3 = $this->db->queryRow($sql);
		$sql = "SELECT STD(calidad_dedos) AS desviacion_estandar FROM calidad WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND calidad_dedos > 0 $sWhere";
		$rp4 = $this->db->queryRow($sql);
		$sql = "SELECT AVG(peso / 2.2) AS peso FROM calidad WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND peso > 0 $sWhere";
		$rp5 = $this->db->queryRow($sql);
		$sql = "SELECT AVG(cantidad_cluster_caja) AS cluster FROM calidad WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND cantidad_cluster_caja > 0 $sWhere";
		$rp6 = $this->db->queryRow($sql);
		$sql = "SELECT AVG(calidad_dedos) AS calidad_dedos FROM calidad WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND calidad_dedos > 0 $sWhere";
		$rp7 = $this->db->queryRow($sql);
		$sql = "SELECT AVG(calidad_cluster) AS calidad_cluster FROM calidad WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND calidad_cluster > 0 $sWhere";
		$rp8 = $this->db->queryRow($sql);
		$sql = "SELECT AVG(cantidad_dedos_caja) AS dedos_promedio FROM calidad WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND cantidad_dedos_caja > 0 $sWhere";
		$rp9 = $this->db->queryRow($sql);
		$tags = [];
		// D($sql);
		$Desviacion_estandar = $rp4->desviacion_estandar;


		$tags = array(
			'calidad' => 100,
			'calidad_maxima' => $rp2->calidad_maxima,
			'calidad_dedos' => $rp7->calidad_dedos,
			'calidad_minima' => $rp3->calidad_minima,
			'desviacion_estandar' => $Desviacion_estandar,
			'peso' => $rp5->peso,
			'cluster' => $rp6->cluster,
			'calidad_cluster' => $rp8->calidad_cluster,
			'dedos_promedio' => $rp9->dedos_promedio
		);
		return $tags;
	}	
	// 22/02/2017 - TAG: Grafica Historica
	private function HistoricaCalidad($filters = []){
		$sWhere = "";
		$sWhere_PromFincas = "";
		$subSql = "";
		$Nivel1 = "";
		$Nivel2 = "";
		$Nivel3 = "";
		if($filters->finca != ""){
			$sWhere .= " AND finca = '{$filters->finca}'";
		}
		if($filters->idExportador > 0){
			$sWhere .= " AND id_exportador = '{$filters->idExportador}'";
		}
		if($filters->idCliente > 0){
			$sWhere .= " AND id_cliente = '{$filters->idCliente}'";
		}
		if($filters->idMarca > 0){
			$sWhere .= " AND id_marca = '{$filters->idMarca}'";
		}
		if($filters->contenedor != ""){
			$sWhere .= " AND UPPER(TRIM(contenedor)) = '{$filters->contenedor}'";
		}

		$inicio = $filters->fecha_inicial;
        $fin = $filters->fecha_final;
        $year = explode("-", $filters->fecha_inicial)[0];
        #$sWhere .= " AND fecha BETWEEN '{$inicio}' AND '{$fin}'";
        $sWhere .= " AND YEAR(fecha) = $year";

		$Umbral = 4;

		$sql = "SELECT nameFinca AS finca , idFinca AS id_finca , dateFecha AS fecha , calidad FROM
				(SELECT id_finca AS idFinca , finca AS nameFinca,fechas AS dateFecha FROM calidad,
				(SELECT getWeek(fecha) AS fechas FROM calidad 
					WHERE id_finca > 0 
                        AND YEAR(fecha) = $year
                        #AND fecha BETWEEN '{$inicio}' AND '{$fin}'
					GROUP BY getWeek(fecha)) AS fechas
					WHERE calidad_dedos > 0 {$sWhere}
					GROUP BY finca , fechas
					ORDER BY finca , getWeek(fechas)) AS fechas 
				LEFT JOIN
				(SELECT 			
					TRIM(finca) AS finca ,id_finca, getWeek(fecha) AS fecha, AVG(calidad_dedos) calidad
					FROM calidad
					WHERE calidad_dedos > 0 {$sWhere}
					GROUP BY finca , getWeek(fecha)
					ORDER BY getWeek(fecha),finca) AS promedio ON fecha = dateFecha AND finca = nameFinca
                HAVING fecha != '0000-00-00'
                ORDER BY finca, fecha";		
                
		$response = new stdClass;
		$res = $this->db->queryAll($sql);
		$response->data = [];
		$response->legend = [];
		$response->avg = 0;
		$count = 0;
		$sum = 0;
		$series = new stdClass;
		$finca = "";
		$flag_count = 0;
		$markLine = new stdClass;

		$fechas = [];
		foreach($res as $key => $value){
			if(!in_array($value->fecha, $fechas)){
				$fechas[] = $value->fecha;
			}
		}

		foreach ($res as $key => $value) {
			$value = (object)$value;
			$value->calidad = (float)$value->calidad;
			$sum += $value->calidad;
			$response->legend[$value->fecha] = $value->fecha;
			$value->calidad = (double)$value->calidad;

			#if($finca != $value->id_finca){
			if(!isset($response->data[$value->finca]->itemStyle)){
				$series = new stdClass;
				$finca = $value->finca;
				$series->name = $value->finca;
				$series->type = "line";
				$series->connectNulls = true;
				foreach($fechas as $fec){
					$series->data[] = null;
				}
				if($value->calidad > 0){
					$series->data[array_search($value->fecha, $fechas)] = round($value->calidad ,2);
				}
				$series->itemStyle = (object)[
					"normal" => [
						// "color" => '#007537',
						// "barBorderColor" => '#007537',
						"barBorderWidth" => 6,
						"barBorderRadius" => 0,
						"label" => [ "show" => false , "position" => "insideTop"]
					]
				];
				
				$response->data[$value->finca] = $series;
			}else if($value->calidad != null){
				#if($finca == $value->id_finca){
					$response->data[$value->finca]->data[array_search($value->fecha, $fechas)] = round($value->calidad ,2);
				#}
			}
			$count++;
		}

		$response->avg = ($sum / $count);

		return $response; 
	}

	// 22/02/2017 - TAG: Grafica Historica Desviacion
	private function HistoricaDesviacionEstandar($filters = []){
		$sWhere = "";
		$sWhere_PromFincas = "";
		$subSql = "";
		$Nivel1 = "";
		$Nivel2 = "";
		$Nivel3 = "";
		if($filters->finca != ""){
			$sWhere .= " AND finca = '{$filters->finca}'";
		}
		if($filters->idExportador > 0){
			$sWhere .= " AND id_exportador = '{$filters->idExportador}'";
		}
		if($filters->idCliente > 0){
			$sWhere .= " AND id_cliente = '{$filters->idCliente}'";
		}
		if($filters->idMarca > 0){
			$sWhere .= " AND id_marca = '{$filters->idMarca}'";
		}
		if($filters->contenedor != ""){
			$sWhere .= " AND TRIM(UPPER(contenedor)) = '{$filters->contenedor}'";
		}

		$inicio = $filters->fecha_inicial;
        $fin = $filters->fecha_final;
        $year = explode("-", $inicio)[0];
        #$sWhere .= " AND fecha BETWEEN '{$inicio}' AND '{$fin}'";
        $sWhere .= " AND YEAR(fecha) = $year";
		$Umbral = 4;

		$sql = "SELECT nameFinca AS finca , idFinca AS id_finca , dateFecha AS fecha , calidad FROM
				(SELECT id_finca AS idFinca , finca AS nameFinca,fechas AS dateFecha FROM calidad,
				(SELECT getWeek(fecha) AS fechas FROM calidad 
					WHERE id_finca > 0 
                        AND YEAR(fecha) = $year
                        #AND fecha BETWEEN '{$inicio}' AND '{$fin}'
					GROUP BY getWeek(fecha)) AS fechas
					WHERE calidad_dedos > 0 {$sWhere}
					GROUP BY finca , fechas
					ORDER BY finca , getWeek(fechas)) AS fechas 
				LEFT JOIN
				(SELECT 			
					TRIM(finca) AS finca ,id_finca, getWeek(fecha) AS fecha, STD(calidad_dedos) calidad
					FROM calidad
					WHERE calidad_dedos > 0 {$sWhere}
					GROUP BY finca , getWeek(fecha)
					ORDER BY getWeek(fecha),finca) AS promedio ON fecha = dateFecha AND finca = nameFinca
                HAVING fecha != '0000-00-00'
                ORDER BY finca, fecha";		
		$response = new stdClass;
		$res = $this->db->queryAll($sql);
		$response->data = [];
		$response->legend = [];
		$response->avg = 0;
		$count = 0;
		$sum = 0;
		$series = new stdClass;
		$finca = "";
		$flag_count = 0;
		$markLine = new stdClass;

		$fechas = [];
		foreach($res as $key => $value){
			if(!in_array($value->fecha, $fechas)){
				$fechas[] = $value->fecha;
			}
		}

		foreach ($res as $key => $value) {
			$value = (object)$value;
			$value->calidad = (float)$value->calidad;
			$sum += $value->calidad;
			$response->legend[$value->fecha] = $value->fecha;
			$value->calidad = (double)$value->calidad;

			#if($finca != $value->id_finca){
			if(!isset($response->data[$value->finca]->itemStyle)){
				$series = new stdClass;
				$finca = $value->finca;
				$series->name = $value->finca;
				$series->type = "line";
				$series->connectNulls = true;
				foreach($fechas as $fec){
					$series->data[] = null;
				}
				if($value->calidad > 0){
					$series->data[array_search($value->fecha, $fechas)] = round($value->calidad ,2);
				}
				$series->itemStyle = (object)[
					"normal" => [
						// "color" => '#007537',
						// "barBorderColor" => '#007537',
						"barBorderWidth" => 6,
						"barBorderRadius" => 0,
						"label" => [ "show" => true , "position" => "insideTop"]
					]
				];
				
				$response->data[$value->finca] = $series;
			}else if($value->calidad != null){
				#if($finca == $value->id_finca){
					$response->data[$value->finca]->data[array_search($value->fecha, $fechas)] = round($value->calidad ,2);
				#}
			}
			$count++;
		}

		$response->avg = ($sum / $count);

		return $response; 
	}

	// 22/02/2017 - TAG: Grafica Historica Peso
	private function HistoricaPesoCaja($filters = []){
		$sWhere = "";
		$sWhere_PromFincas = "";
		$subSql = "";
		$Nivel1 = "";
		$Nivel2 = "";
		$Nivel3 = "";
		if($filters->finca != ""){
			$sWhere .= " AND finca = '{$filters->finca}'";
		}
		if($filters->idExportador > 0){
			$sWhere .= " AND id_exportador = '{$filters->idExportador}'";
		}
		if($filters->idCliente > 0){
			$sWhere .= " AND id_cliente = '{$filters->idCliente}'";
		}
		if($filters->idMarca > 0){
			$sWhere .= " AND id_marca = '{$filters->idMarca}'";
		}
		if($filters->contenedor != ""){
			$sWhere .= " AND TRIM(UPPER(contenedor)) = '{$filters->contenedor}'";
		}

		$inicio = $filters->fecha_inicial;
        $fin = $filters->fecha_final;
        $year = explode("-", $inicio)[0];
        #$sWhere .= " AND fecha BETWEEN '{$inicio}' AND '{$fin}'";
        $sWhere .= " AND YEAR(fecha) = $year";
		$Umbral = 4;

		$sql = "SELECT nameFinca AS finca , idFinca AS id_finca , dateFecha AS fecha , calidad 
                FROM(
                    SELECT id_finca AS idFinca , finca AS nameFinca,fechas AS dateFecha 
                    FROM calidad,
                        (SELECT getWeek(fecha) AS fechas FROM calidad 
                            WHERE id_finca > 0 
                                AND YEAR(fecha) = $year
                                #AND fecha BETWEEN '{$inicio}' AND '{$fin}'
                            GROUP BY getWeek(fecha)) AS fechas
                            WHERE calidad_dedos > 0 {$sWhere}
                            GROUP BY finca , fechas
                            ORDER BY finca , getWeek(fechas)) AS fechas 
				    LEFT JOIN
                        (SELECT 			
                            TRIM(finca) AS finca ,id_finca, getWeek(fecha) AS fecha, AVG(ROUND((peso / 2.2) , 2)) calidad
                            FROM calidad
                            WHERE calidad_dedos > 0 AND peso > 0 {$sWhere}
                            GROUP BY finca , getWeek(fecha)
                            ORDER BY getWeek(fecha),finca) AS promedio ON fecha = dateFecha AND finca = nameFinca
                HAVING fecha != '0000-00-00'
                ORDER BY finca, fecha";
                		
		$response = new stdClass;
		$res = $this->db->queryAll($sql);
		$response->data = [];
		$response->legend = [];
		$response->avg = 0;
		$count = 0;
		$sum = 0;
		$series = new stdClass;
		$finca = "";
		$flag_count = 0;
		$markLine = new stdClass;

		$fechas = [];
		foreach($res as $key => $value){
			if(!in_array($value->fecha, $fechas)){
				$fechas[] = $value->fecha;
			}
		}

		foreach ($res as $key => $value) {
			$value = (object)$value;
			$value->calidad = (float)$value->calidad;
			$sum += $value->calidad;
			$response->legend[$value->fecha] = $value->fecha;
			$value->calidad = (double)$value->calidad;

			#if($finca != $value->id_finca){
			if(!isset($response->data[$value->finca]->itemStyle)){
				$series = new stdClass;
				$finca = $value->finca;
				$series->name = $value->finca;
				$series->type = "line";
				$series->connectNulls = true;
				foreach($fechas as $fec){
					$series->data[] = null;
				}
				if($value->calidad > 0){
					$series->data[array_search($value->fecha, $fechas)] = round($value->calidad ,2);
				}
				$series->itemStyle = (object)[
					"normal" => [
						// "color" => '#007537',
						// "barBorderColor" => '#007537',
						"barBorderWidth" => 6,
						"barBorderRadius" => 0,
						"label" => [ "show" => false , "position" => "insideTop"]
					]
				];
				
				$response->data[$value->finca] = $series;
			}else if($value->calidad != null){
				#if($finca == $value->id_finca){
					$response->data[$value->finca]->data[array_search($value->fecha, $fechas)] = round($value->calidad ,2);
				#}
			}
			$count++;
		}

		$response->avg = ($sum / $count);

		return $response; 
	}

	// 22/02/2017 - TAG: Grafica Historica Dedos
	private function HistoricaDedos($filters = []){
		$sWhere = "";
		$sWhere_PromFincas = "";
		$subSql = "";
		$Nivel1 = "";
		$Nivel2 = "";
		$Nivel3 = "";
		if($filters->finca != ""){
			$sWhere .= " AND finca = '{$filters->finca}'";
		}
		if($filters->idExportador > 0){
			$sWhere .= " AND id_exportador = '{$filters->idExportador}'";
		}
		if($filters->idCliente > 0){
			$sWhere .= " AND id_cliente = '{$filters->idCliente}'";
		}
		if($filters->idMarca > 0){
			$sWhere .= " AND id_marca = '{$filters->idMarca}'";
		}
		if($filters->contenedor != ""){
			$sWhere .= " AND TRIM(UPPER(contenedor)) = '{$filters->contenedor}'";
		}

		$inicio = $filters->fecha_inicial;
        $fin = $filters->fecha_final;
        $year = explode("-", $inicio)[0];
        #$sWhere .= " AND fecha BETWEEN '{$inicio}' AND '{$fin}'";
        $sWhere .= " AND YEAR(fecha) = $year";
		$Umbral = 4;

		$sql = "SELECT nameFinca AS finca , idFinca AS id_finca , dateFecha AS fecha , calidad FROM
				(SELECT id_finca AS idFinca , finca AS nameFinca,fechas AS dateFecha FROM calidad,
				(SELECT getWeek(fecha) AS fechas FROM calidad 
					WHERE id_finca > 0 
                        AND YEAR(fecha) = $year
                        #AND fecha BETWEEN '{$inicio}' AND '{$fin}'
					GROUP BY getWeek(fecha)) AS fechas
					WHERE calidad_dedos > 0 {$sWhere}
					GROUP BY finca , fechas
					ORDER BY finca , getWeek(fechas)) AS fechas 
				LEFT JOIN
				(SELECT 			
					TRIM(finca) AS finca ,id_finca, getWeek(fecha) AS fecha, AVG(cantidad_dedos_caja) calidad
					FROM calidad
					WHERE calidad_dedos > 0 {$sWhere}
					GROUP BY finca , getWeek(fecha)
					ORDER BY getWeek(fecha),finca) AS promedio ON fecha = dateFecha AND finca = nameFinca
                HAVING fecha != '0000-00-00'
                ORDER BY finca, fecha";		
		$response = new stdClass;
		$res = $this->db->queryAll($sql);
		$response->data = [];
		$response->legend = [];
		$response->avg = 0;
		$count = 0;
		$sum = 0;
		$series = new stdClass;
		$finca = "";
		$flag_count = 0;
		$markLine = new stdClass;

		$fechas = [];
		foreach($res as $key => $value){
			if(!in_array($value->fecha, $fechas)){
				$fechas[] = $value->fecha;
			}
		}

		foreach ($res as $key => $value) {
			$value = (object)$value;
			$value->calidad = (float)$value->calidad;
			$sum += $value->calidad;
			$response->legend[$value->fecha] = $value->fecha;
			$value->calidad = (double)$value->calidad;

			#if($finca != $value->id_finca){
			if(!isset($response->data[$value->finca]->itemStyle)){
				$series = new stdClass;
				$finca = $value->finca;
				$series->name = $value->finca;
				$series->type = "line";
				$series->connectNulls = true;
				foreach($fechas as $fec){
					$series->data[] = null;
				}
				if($value->calidad > 0){
					$series->data[array_search($value->fecha, $fechas)] = round($value->calidad ,2);
				}
				$series->itemStyle = (object)[
					"normal" => [
						// "color" => '#007537',
						// "barBorderColor" => '#007537',
						"barBorderWidth" => 6,
						"barBorderRadius" => 0,
						"label" => [ "show" => false , "position" => "insideTop"]
					]
				];
				
				$response->data[$value->finca] = $series;
			}else if($value->calidad != null){
				#if($finca == $value->id_finca){
					$response->data[$value->finca]->data[array_search($value->fecha, $fechas)] = round($value->calidad ,2);
				#}
			}
			$count++;
		}

		$response->avg = ($sum / $count);

		return $response; 
	}

	// 16/06/2017 - TAG: Grafica Historica Cluster
	private function HistoricaCluster($filters = []){
		$sWhere = "";
		$sWhere_PromFincas = "";
		$subSql = "";
		$Nivel1 = "";
		$Nivel2 = "";
		$Nivel3 = "";
		if($filters->finca != ""){
			$sWhere .= " AND finca = '{$filters->finca}'";
		}
		if($filters->idExportador > 0){
			$sWhere .= " AND id_exportador = '{$filters->idExportador}'";
		}
		if($filters->idCliente > 0){
			$sWhere .= " AND id_cliente = '{$filters->idCliente}'";
		}
		if($filters->idMarca > 0){
			$sWhere .= " AND id_marca = '{$filters->idMarca}'";
		}
		if($filters->contenedor != ""){
			$sWhere .= " AND TRIM(UPPER(contenedor)) = '{$filters->contenedor}'";
		}

		$inicio = $filters->fecha_inicial;
        $fin = $filters->fecha_final;
        $year = explode("-", $inicio)[0];
        #$sWhere .= " AND fecha BETWEEN '{$inicio}' AND '{$fin}'";
        $sWhere .= " AND YEAR(fecha) = $year";
		$Umbral = 4;

		$sql = "SELECT nameFinca AS finca , idFinca AS id_finca , dateFecha AS fecha , calidad FROM
				(SELECT id_finca AS idFinca , finca AS nameFinca,fechas AS dateFecha FROM calidad,
				(SELECT getWeek(fecha) AS fechas FROM calidad 
					WHERE id_finca > 0 
                        AND YEAR(fecha) = $year
                        #AND fecha BETWEEN '{$inicio}' AND '{$fin}'
					GROUP BY getWeek(fecha)) AS fechas
					WHERE calidad_dedos > 0 {$sWhere}
					GROUP BY finca , fechas
					ORDER BY finca , getWeek(fechas)) AS fechas 
				LEFT JOIN
				(SELECT 			
					TRIM(finca) AS finca ,id_finca, getWeek(fecha) AS fecha, AVG(cantidad_cluster_caja) calidad
					FROM calidad
					WHERE calidad_dedos > 0 {$sWhere}
					GROUP BY finca , getWeek(fecha)
					ORDER BY getWeek(fecha),finca) AS promedio ON fecha = dateFecha AND finca = nameFinca
                HAVING fecha != '0000-00-00'
                ORDER BY finca, fecha";
		$response = new stdClass;
		$res = $this->db->queryAll($sql);
		$response->data = [];
		$response->legend = [];
		$response->avg = 0;
		$count = 0;
		$sum = 0;
		$series = new stdClass;
		$finca = "";
		$flag_count = 0;
		$markLine = new stdClass;

		$fechas = [];
		foreach($res as $key => $value){
			if(!in_array($value->fecha, $fechas)){
				$fechas[] = $value->fecha;
			}
		}

		foreach ($res as $key => $value) {
			$value = (object)$value;
			$value->calidad = (float)$value->calidad;
			$sum += $value->calidad;
			$response->legend[$value->fecha] = $value->fecha;
			$value->calidad = (double)$value->calidad;

			#if($finca != $value->id_finca){
			if(!isset($response->data[$value->finca]->itemStyle)){
				$series = new stdClass;
				$finca = $value->finca;
				$series->name = $value->finca;
				$series->type = "line";
				$series->connectNulls = true;
				foreach($fechas as $fec){
					$series->data[] = null;
				}
				if($value->calidad > 0){
					$series->data[array_search($value->fecha, $fechas)] = round($value->calidad ,2);
				}
				$series->itemStyle = (object)[
					"normal" => [
						// "color" => '#007537',
						// "barBorderColor" => '#007537',
						"barBorderWidth" => 6,
						"barBorderRadius" => 0,
						"label" => [ "show" => false , "position" => "insideTop"]
					]
				];
				
				$response->data[$value->finca] = $series;
			}else if($value->calidad != null){
				#if($finca == $value->id_finca){
					$response->data[$value->finca]->data[array_search($value->fecha, $fechas)] = round($value->calidad ,2);
				#}
			}
			$count++;
		}

		$response->avg = ($sum / $count);

		return $response; 
	}

	// 22/02/2017 - TAG: Grafica Cluster por Caja
	private function HistoricaCategorias($filters = []){
		$sWhere = "";
		$sWhere_PromFincas = "";
		$subSql = "";
		if($filters->finca != ""){
			$sWhere .= " AND finca = '{$filters->finca}'";
		}
		if($filters->idExportador > 0){
			$sWhere .= " AND id_exportador = '{$filters->idExportador}'";
		}
		if($filters->idCliente > 0){
			$sWhere .= " AND id_cliente = '{$filters->idCliente}'";
		}
		if($filters->idMarca > 0){
			$sWhere .= " AND id_marca = '{$filters->idMarca}'";
		}
		if($filters->contenedor != ""){
			$sWhere .= " AND TRIM(UPPER(contenedor)) = '{$filters->contenedor}'";
		}
		$inicio = $filters->fecha_inicial;
        $fin = $filters->fecha_final;
        $year = explode("-", $inicio)[0];
        #$sWhere .= " AND fecha BETWEEN '{$inicio}' AND '{$fin}'";
        $sWhere .= " AND YEAR(fecha) = $year";
		$Umbral = 4; 

		$sql = "SELECT 
                    id_categoria ,
                    (SELECT nombre FROM calidad_categorias WHERE id = id_categoria) AS categoria , 
                    fechas , 
                    IFNULL(value,0) AS value 
                FROM
                    (
                        SELECT id_categoria ,  fechas 
                        FROM
                        (
                            SELECT getWeek(fecha) AS fechas 
                            FROM calidad 
                            WHERE id_finca > 0 
                                AND YEAR(fecha) = $year
                                #AND fecha BETWEEN '{$inicio}' AND '{$fin}' 
                            GROUP BY getWeek(fecha)
                        ) AS fechas ,
                        (
                            SELECT 
                                id_categoria
                            FROM calidad_detalle
                            INNER JOIN calidad ON calidad_detalle.id_calidad = calidad.id
                            WHERE 1 = 1 
                            GROUP BY  id_categoria, getWeek(fecha)
                            ORDER BY id_categoria , getWeek(fecha)
                        ) AS detalle 
                    ) AS detalle
				LEFT JOIN
                    (
                        SELECT 
                            id_categoria AS idCategoria,
                            getWeek(fecha) AS fecha,
                            SUM(cantidad) AS value
                        FROM calidad_detalle
                    	INNER JOIN 
                        	calidad 
                    	ON calidad_detalle.id_calidad = calidad.id
                    	WHERE 1 = 1 {$sWhere} 
                    	GROUP BY id_categoria,getWeek(fecha)
                    ) 
                AS promedio 
                ON fechas = fecha AND idCategoria = id_categoria
				GROUP BY id_categoria, fechas";

		$response = new stdClass;
		$res = $this->db->queryAll($sql);
		$response->data = [];
		$response->legend = [];
		$response->avg = 0;
		$count = 0;
		$sum = 0;
		$series = new stdClass;
		$categoria = "";
		$flag_count = 0;
		$markLine = new stdClass;
		
		foreach ($res as $key => $value) {
			$value = (object)$value;
			$value->value = (float)$value->value;
			$sum += $value->value;
			$response->legend[$value->fechas] = $value->fechas;
			if($categoria != $value->id_categoria){
				$series = new stdClass;
				$categoria = $value->id_categoria;
				$series->name = $value->categoria;
				$series->type = "line";
				$series->data = [];
				$series->data[] = round($value->value ,2);
				$series->itemStyle = (object)[
					"normal" => [
						// "color" => '#007537',
						// "barBorderColor" => '#007537',
						"barBorderWidth" => 6,
						"barBorderRadius" => 0,
						"label" => [ "show" => false , "position" => "insideTop"]
					]
				];
				
				$response->data[$value->id_categoria] = $series;
			}else{
				if($categoria == $value->id_categoria){
					$response->data[$value->id_categoria]->data[] = round($value->value ,2);
				}
			}
			$count++;
		}

		$response->avg = ($sum / $count);

		return $response; 
	}
	// 23/02/2017 - TAG: Tabla de Cluster
	private function promedioClusters($filters = []){
		$inicio = $filters->fecha_inicial;
		$fin = $filters->fecha_final;
		$sWhere = "";
		$response = new stdClass;

		if($filters->finca != ""){
			$sWhere .= " AND finca = '{$filters->finca}'";
		}
		if($filters->idExportador > 0){
			$sWhere .= " AND id_exportador = '{$filters->idExportador}'";
		}
		if($filters->idCliente > 0){
			$sWhere .= " AND id_cliente = '{$filters->idCliente}'";
		}
		if($filters->idMarca > 0){
			$sWhere .= " AND id_marca = '{$filters->idMarca}'";
		}
		if($filters->contenedor != ""){
			$sWhere .= " AND TRIM(UPPER(contenedor)) = '{$filters->contenedor}'";
		}
		#finca promedios
		$nivel1 =  "SELECT id_finca, finca, cluster, SUM(promedio) AS promedio, SUM(porcentaje) AS porcentaje
                    FROM (
                        SELECT id_finca, nombre AS finca, cluster_dedos AS cluster, 
                            ROUND(SUM(cantidad) / (
                                    SELECT SUM(cantidad) 
                                    FROM calidad 
                                    INNER JOIN calidad_dedos ON id_calidad = calidad.`id`
                                    WHERE id_finca = c.id_finca
                                ) * (
                                    SELECT AVG(cantidad_cluster_caja)
                                    FROM calidad
                                    WHERE id_finca = c.id_finca
                                )
                            , 2) AS promedio,
                            ROUND(SUM(cantidad) / (
                                SELECT SUM(cantidad) 
                                FROM calidad 
                                INNER JOIN calidad_dedos ON id_calidad = calidad.`id`
                                WHERE id_finca = c.id_finca
                            ), 2) AS porcentaje
                        FROM calidad c
                        INNER JOIN calidad_dedos ON id_calidad = c.`id`
                        INNER JOIN fincas ON id_finca = fincas.`id`
                        WHERE 1=1 $sWhere
                        GROUP BY id_finca, cluster_dedos
                    ) AS tbl";
		$table->data = $this->db->queryAll($nivel1);

		foreach($table->data as $row){
			$nivel2 = "SELECT cluster_dedos AS cluster, 
                            ROUND(SUM(cantidad) / (
                                    SELECT SUM(cantidad) 
                                    FROM calidad 
                                    INNER JOIN calidad_dedos ON id_calidad = calidad.`id`
                                    WHERE id_finca = c.id_finca
                                ) * (
                                    SELECT AVG(cantidad_cluster_caja)
                                    FROM calidad
                                    WHERE id_finca = c.id_finca
                                )
                            , 2) AS promedio,
                            ROUND(SUM(cantidad) / (
                                SELECT SUM(cantidad) 
                                FROM calidad 
                                INNER JOIN calidad_dedos ON id_calidad = calidad.`id`
                                WHERE id_finca = c.id_finca
                            ), 2) AS porcentaje
                        FROM calidad c
                        INNER JOIN calidad_dedos ON id_calidad = c.`id`
                        INNER JOIN fincas ON id_finca = fincas.`id`
                        WHERE id_finca = $row->id_finca $sWhere
                        GROUP BY cluster_dedos";
			$row->details = $this->db->queryAll($nivel2);
		}
		$response->data = $table;
		return $response;
	}
	// 23/02/2017 - TAG: Tabla de Dedos
	private function promedioDedos($filters = []){
		$inicio = $filters->fecha_inicial;
		$fin = $filters->fecha_final;
		$sWhere = "";
		$response = new stdClass;

		if($filters->finca != ""){
			$sWhere .= " AND finca = '{$filters->finca}'";
		}
		if($filters->idExportador > 0){
			$sWhere .= " AND id_exportador = '{$filters->idExportador}'";
		}
		if($filters->idCliente > 0){
			$sWhere .= " AND id_cliente = '{$filters->idCliente}'";
		}
		if($filters->idMarca > 0){
			$sWhere .= " AND id_marca = '{$filters->idMarca}'";
		}
		if($filters->contenedor != ""){
			$sWhere .= " AND TRIM(UPPER(contenedor)) = '{$filters->contenedor}'";
		}

		#finca promedios
        $nivel1 = "SELECT id_finca, finca,
                        ROUND(AVG(cantidad_dedos_caja), 2) AS promedio, 
                        100 AS porcentaje, 
                        SUM(cantidad_dedos_caja) AS cantidad
                    FROM (
                        SELECT id_finca, finca, SUM(cantidad * cluster_dedos) AS cantidad_dedos_caja
                        FROM calidad
                        INNER JOIN calidad_dedos ON id_calidad = calidad.`id`
                        WHERE fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}' AND cantidad > 0 $sWhere
                        GROUP BY id_calidad
                    ) AS tbl
                    WHERE cantidad_dedos_caja > 0
                    GROUP BY finca";
		$table->data = $this->db->queryAll($nivel1);

		foreach($table->data as $row){
            $nivel2 = "SELECT cluster_dedos AS dedos, 
                            ROUND(AVG(cantidad_dedos_caja), 2) AS promedio_real, 
                            ROUND((SUM(cantidad_dedos_caja) / $row->cantidad * 100), 2) AS porcentaje
                        FROM (
                            SELECT cluster_dedos, finca, SUM(cantidad * cluster_dedos) AS cantidad_dedos_caja
                            FROM calidad
                            INNER JOIN calidad_dedos ON id_calidad = calidad.`id`
                            WHERE fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}' AND id_finca = '{$row->id_finca}' AND cantidad > 0 $sWhere
                            GROUP BY cluster_dedos
                        ) AS tbl
                        GROUP BY cluster_dedos";
            
			$row->expand = false;
            $row->details = $this->db->queryAll($nivel2);
            foreach($row->details as $r){
                $r->promedio = round($row->promedio * ($r->porcentaje / 100), 2);
            }
		}
		$response->data = $table;
		return $response;
	}
	// 23/02/2017 - TAG: Grafica de Pastel de Defectos
	private function graphicDanhos($filters = []){
		$response = new stdClass;
		$sWhere = "";

		if($filters->fecha_inicial != "" && $filters->fecha_final != ""){
			$sWhere .= " AND fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}'";
		}
		if($filters->finca != ""){
			$sWhere .= " AND finca = '{$filters->finca}'";
		}
		if($filters->idExportador > 0){
			$sWhere .= " AND id_exportador = '{$filters->idExportador}'";
		}
		if($filters->idCliente > 0){
			$sWhere .= " AND id_cliente = '{$filters->idCliente}'";
		}
		if($filters->idMarca > 0){
			$sWhere .= " AND id_marca = '{$filters->idMarca}'";
		}
		if($filters->contenedor != ""){
			$sWhere .= " AND TRIM(UPPER(contenedor)) = '{$filters->contenedor}'";
		}
		

		$pie = [];
		$data = [];
		
		$sqlCategorias = "SELECT 
				id_categoria,
				(SELECT nombre FROM calidad_categorias WHERE id = id_categoria) AS label,
				SUM(cantidad) AS value
				FROM calidad_detalle
				INNER JOIN calidad ON calidad_detalle.id_calidad = calidad.id
				WHERE 1 = 1 $sWhere
				GROUP BY id_categoria";
		$data = $this->db->queryAll($sqlCategorias);
		$response->data = $this->pie($data, ["0%", "60%"], "", "area", "normal", true);

		$response->categorias = [];
		foreach ($data as $key => $value) {
			$sql = "SELECT  campo AS label, SUM(cantidad) AS value
					FROM calidad_detalle
					INNER JOIN calidad ON id_calidad = calidad.id
					WHERE 1=1 AND id_categoria = {$value->id_categoria} $sWhere
					GROUP BY campo";
			$pie = $this->db->queryAll($sql);
			if(count($pie) > 0){
				$response->categorias[$value->id_categoria] = $this->pie($pie, ["0%", "60%"], "", "area", "normal", true);
			}
		}

		$response->categoriasFilters = $this->db->queryAllSpecial("SELECT id_categoria AS id ,
				(SELECT nombre FROM calidad_categorias WHERE id = id_categoria) AS label FROM calidad_detalle
				INNER JOIN calidad ON calidad_detalle.id_calidad = calidad.id
				WHERE 1 = 1 $sWhere
				GROUP BY id_categoria");

		return $response;
    }

	// 23/02/2017 - TAG: Metodo para devolver la configuracion de la Grafica de pastel
	private function pie($data = [] , $radius = ['50%', '70%'] , $name = "CATEGORIAS" , $roseType = "" , $type = "normal" , $legend = true , $position = ['45%', '40%'] , $version = 3){
		$response = new stdClass;
		$response->pie = [];
		if(count($data) > 0){
			$response->pie["title"]["show"] = true;
			$response->pie["title"]["text"] = $name;
			$response->pie["title"]["left"] = "center";
			// $response->pie["title"]["left"] = "right";
			$response->pie["title"]["subtext"] = $this->session->sloganCompany;
			$response->pie["tooltip"]["trigger"] = "item";
			$response->pie["tooltip"]["formatter"] = "{a} <br/>{b}: {c} ({d}%)";
			$response->pie["toolbox"]["show"] = true;
			$response->pie["toolbox"]["feature"]["mark"]["show"] = true;
			$response->pie["toolbox"]["feature"]["restore"]["show"] = true;
			$response->pie["toolbox"]["feature"]["magicType"]["show"] = false;
			$response->pie["toolbox"]["feature"]["magicType"]["type"] = ['pie'];
			$response->pie["toolbox"]["feature"]["saveAsImage"]["show"] = true;
			$response->pie["toolbox"]["feature"]["saveAsImage"]["name"] = $name;
			$response->pie["legend"]["show"] = $legend;
			$response->pie["legend"]["x"] = "center";
			$response->pie["legend"]["y"] = "bottom";
			$response->pie["calculable"] = true;
			$response->pie["legend"]["data"] = [];
			$response->pie["series"]["name"] = $name;
			$response->pie["series"]["type"] = "pie";
			$response->pie["series"]["center"] = $position;
			$response->pie["series"]["radius"] = $radius;
			if($version === 3){
				$response->pie["series"]["selectedMode"] = true;
				$response->pie["series"]["label"]["normal"]["show"] = true;
				$response->pie["series"]["label"]["emphasis"]["show"] = true;
				if($type != "normal"){
					$response->pie["series"]["roseType"] = $roseType;
					$response->pie["series"]["avoidLabelOverlap"] = "pie";
				}
				// $response->pie["series"]["label"]["normal"]["position"] = "center";
				// $response->pie["series"]["label"]["emphasis"]["textStyle"]["fontSize"] = "30";
				// $response->pie["series"]["label"]["emphasis"]["textStyle"]["fontWeight"] = "bold";
				$response->pie["series"]["labelLine"]["normal"]["show"] = true;
			}
			$response->pie["series"]["data"] = [];
			$colors = [];
			foreach ($data as $key => $value) {
				$value = (object)$value;
				$value->label = ucwords(strtolower($value->label));
				$response->pie["legend"]["data"][] = $value->label;
				if($version === 3){
					if(isset($value->color)){
							$colors = [
								"normal" => ["color" => $value->color],
							];
					}
					$response->pie["series"]["data"][] = ["name" => $value->label  , "value" => (float)$value->value , 
							"label" => [
								#"normal" => ["show" => true , "position" => "inside" , "formatter" => "{b} \n {c} ({d}%)"],
								"normal" => ["show" => true , "position" => "inside" , "formatter" => "{c}\n ({d}%)"],
								"emphasis" => ["show" => true , "position" => "outside"],
							],
							"itemStyle" => $colors
					];
				}else{
					// $response->pie["series"]["data"][] = ["name" => $value->label  , "value" => (float)$value->value];
					$response->pie["series"]["data"][] = ["name" => $value->label  , "value" => (float)$value->value,
						"itemStyle" => [
							"normal" => [ "label" => ["formatter" => "{b} \n {c} \n ({d}%)"] ],
							"emphasis" => [ "label" => ["formatter" => "{b} \n {c} \n ({d}%)"] ],
						]
					];
				}
			}
		}
		return $response->pie;
    }
    
    private function grafica_z($data = [], $group_y = []){
        $options = [];
        $options["tooltip"] = [
            "trigger" => 'axis',
            "axisPointer" => [
                "type" => 'cross',
                "crossStyle" => [
                    "color" => '#999'
                ]
            ]
        ];
        $options["toolbox"] = [
            "feature" => [
                "dataView" => [
                    "show" => true,
                    "readOnly" => false
                ],
                "magicType" => [
                    "show" => true,
                    "type" => ['line', 'bar']
                ],
                "restore" => [
                    "show" => true
                ],
                "saveAsImage" => [
                    "show" => true
                ]
            ]
        ];
        $options["legend"]["data"] = [];
        $options["legend"]["bottom"] = "0%";
        $options["legend"]["left"] = "center";
        $options["xAxis"] = [
            [
                "type" => 'category',
                "data" => [],
                "axisPointer" => [
                    "type" => 'shadow'
                ]
            ]
        ];
        /*
            [
                type => 'value',
                name => {String},
                min => 0,
                max => 200,
                interval => 5,
                axisLabel => [
                    formatter => {value} KG
                ]
            ]
        */
        $options["yAxis"] = [];
        /*
            [
                name => {String},
                type => 'line',
                data => [
                    {double}, {double}, {double}
                ]
            ]
        */
        $options["series"] = [];

        $maxs = [];
        $mins = [];
        $prepare_data = [];
        $_x = [];
        $_names = [];
        $_namess = [];
        foreach($data as $d){
            $d = (object) $d;
            if(!isset($maxs[$d->index_y])) if($d->value > 0)
                $maxs[$d->index_y] = $d->value;
            if($d->value > $maxs[$d->index_y]) if($d->value > 0)
                $maxs[$d->index_y] = $d->value;

            if(!isset($mins[$d->index_y])) if($d->value > 0)
                $mins[$d->index_y] = $d->value;
            if($d->value < $mins[$d->index_y]) if($d->value > 0)
                $mins[$d->index_y] = $d->value;

            if(!in_array($d->label_x, $_x)){
                $_x[] = $d->label_x;
            }
            if(!in_array($d->name, $_namess)){
                $_namess[] = $d->name;
                 
                $n = ["name" => $d->name, "group" => $d->index_y];
                if(isset($d->line)){
                    $n["line"] = $d->line;
                }
                $_names[] = $n;
            }
            $prepare_data[$d->label_x][$d->name] = $d->value;
        }

        foreach($group_y as $key => $col){
            $col = (object) $col;
            $options["yAxis"][] = [
                'type' => 'value',
                'name' => $col->name,
                'min' => 'dataMin',
                'axisLabel' => [
                    'formatter' => "{value} $col->format"
                ]
            ];
        }

        foreach($_x as $row){
            $options["xAxis"][0]["data"][] = $row;
        }

        foreach($_names as $name){
            $name = (object) $name;

            if(!in_array($name->name, $options["legend"]["data"]))
                $options["legend"]["data"][] = $name->name;

            $serie = [
                "name" => $name->name,
                "type" => 'line',
                "connectNulls" => true,
                "data" => []
            ];
            if($name->group > 0)
                $serie["yAxisIndex"] = $name->group;

            if(isset($name->line)){
                $serie["itemStyle"]["normal"]["lineStyle"]["width"] = 5;
            }

            foreach($_x as $row){
                $val = 0;
                if(isset($prepare_data[$row][$name->name]))
                    $val = $prepare_data[$row][$name->name];

                if($val > 0)
                    $serie["data"][] = $val;
                else
                    $serie["data"][] = null;
            }
            $options["series"][] = $serie;
        }

        return $options;
    }
}