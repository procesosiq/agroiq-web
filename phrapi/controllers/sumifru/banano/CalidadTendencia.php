<?php defined('PHRAPI') or die("Direct access not allowed!");

class CalidadTendencia {
	public $name;
	private $db;
	private $config;

	public function __construct(){
		$this->session = Session::getInstance();
		$this->db = DB::getInstance($this->session->agent_user);
		$this->postdata = (object)json_decode(file_get_contents("php://input"));
    }
    
    public function last(){
        $response = new stdClass;
		$response->anio = $this->db->queryOne("SELECT MAX(year) FROM calidad");
        return $response;
	}

	public function index(){
		$response = new stdClass;

		$sql = "SELECT marca
				FROM calidad
				WHERE year = {$this->postdata->anio}
				GROUP BY marca";
		$response->marcas = $this->db->queryAllOne($sql);

		$response->tabla = $this->tabla();
		$response->grafica = $this->grafica();
		return $response;
	}

	public function grafica(){
		$response = new stdClass;

		$sWhere = "";
		if(isset($this->postdata->marca) && $this->postdata->marca != ''){
			$sWhere .= " AND marca = '{$this->postdata->marca}'";
		}

		$sql = "SELECT {$this->postdata->period}
				FROM calidad
				WHERE year = {$this->postdata->anio} {$sWhere}
				GROUP BY {$this->postdata->period}
				ORDER BY {$this->postdata->period}";
		$response->legends = $this->db->queryAllOne($sql);

		$sql = "SELECT {$this->postdata->mode} as detalle
				FROM calidad
				INNER JOIN fincas ON id_finca = fincas.id
				WHERE year = {$this->postdata->anio} {$sWhere}
				GROUP BY {$this->postdata->mode}";
		$zonas = $this->db->queryAllOne($sql);

		$response->series = [];
		foreach($zonas as $zona){
			$response->series[] = [
                "name" => $zona,
				"connectNulls" => true,
				"min" => 'dataMin',
				"type" => "line",
				"label" => [
					"normal" => [
						"show" => false,
						"position" => "inside"
					]
				],
				"data" => [],
			];
			
			foreach($response->legends as $sem){
				if($this->postdata->var == 'peso_prom_cluster'){
					$sql = "SELECT ROUND(AVG(cantidad), 2) 
							FROM calidad 
							INNER JOIN fincas ON id_finca = fincas.id
							INNER JOIN calidad_pesos_cluster ON id_calidad = calidad.id 
							WHERE year = {$this->postdata->anio} AND {$this->postdata->period} = {$sem} AND {$this->postdata->mode} = '{$zona}' {$sWhere}";
					$val = (float) $this->db->queryOne($sql);
				}else{
					$sql = "SELECT ROUND(AVG({$this->postdata->var}), 2)
							FROM calidad
							INNER JOIN fincas ON id_finca = fincas.id
							WHERE year = {$this->postdata->anio} AND {$this->postdata->period} = {$sem} AND {$this->postdata->mode} = '{$zona}' {$sWhere}";
					$val = (float) $this->db->queryOne($sql);
				}
				$response->series[count($response->series)-1]["data"][] = $val > 0 ? round($val, 2) : null;
			}
		}

		return $response;
	}

	public function tabla(){
		$response = new stdClass;

		$sWhere = "";
		if(isset($this->postdata->marca) && $this->postdata->marca != ''){
			$sWhere .= " AND marca = '{$this->postdata->marca}'";
		}

		$sql = "SELECT {$this->postdata->period}
				FROM calidad
				WHERE year = {$this->postdata->anio} {$sWhere}
				GROUP BY {$this->postdata->period}
				ORDER BY {$this->postdata->period}";
		$response->semanas = $this->db->queryAllOne($sql);

		$sql = "SELECT {$this->postdata->mode} as detalle
				FROM calidad
				INNER JOIN fincas ON id_finca = fincas.id
				WHERE year = {$this->postdata->anio} {$sWhere}
				GROUP BY {$this->postdata->mode}";
		$zonas = $this->db->queryAll($sql);

		foreach($zonas as $zona){

			$sum = 0;
			$count = 0;
			$max = null;
			$min = null;

			foreach($response->semanas as $sem){
				if($this->postdata->var == 'peso_prom_cluster'){
					$sql = "SELECT ROUND(AVG(cantidad), 2) 
							FROM calidad 
							INNER JOIN fincas ON id_finca = fincas.id
							INNER JOIN calidad_pesos_cluster ON id_calidad = calidad.id 
							WHERE year = {$this->postdata->anio} AND {$this->postdata->period} = {$sem} AND {$this->postdata->mode} = '{$zona->detalle}' {$sWhere}";
					$val = (float) $this->db->queryOne($sql);
				}else{
					$sql = "SELECT ROUND(AVG({$this->postdata->var}), 2)
							FROM calidad
							INNER JOIN fincas ON id_finca = fincas.id
							WHERE year = {$this->postdata->anio} AND {$this->postdata->period} = {$sem} AND {$this->postdata->mode} = '{$zona->detalle}' {$sWhere}";
					$val = (float) $this->db->queryOne($sql);
				}
				$zona->{"sem_{$sem}"} = $val > 0 ? number_format($val, 2) : '';

				if($val){
					$sum += $val;
					$count++;
					if($max == null || $val > $max) $max = $val;
					if($min == null || $val < $min) $min = $val;
				}
			}

			$zona->max = $max;
			$zona->min = $min;
			$zona->prom = round($sum/$count, 2);
		}

		$response->data = $zonas;
		return $response;
	}
}