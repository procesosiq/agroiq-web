<?php defined('PHRAPI') or die("Direct access not allowed!");

class NuevoEnfundeSemanal {
	public $name;
	private $db;
	private $config;

	public function __construct(){
		$this->config = $GLOBALS['config'];
		$this->session = Session::getInstance();
        $this->db = DB::getInstance($this->session->agent_user);
        $this->postdata = (object)json_decode(file_get_contents("php://input"));
    }

    public function last(){
        $response = new stdClass;
        $response->anio = $this->db->queryOne("SELECT MAX(years) FROM produccion_enfunde");
        $response->semana = $this->db->queryOne("SELECT MAX(semana) FROM produccion_enfunde WHERE years = $response->anio");
        return $response;
    }

    public function index(){
        $response = new stdClass;
        $sWhere = " AND years = {$this->postdata->anio} AND semana = {$this->postdata->semana}";
        $sWhere2 = " AND year = {$this->postdata->anio} AND semana = {$this->postdata->semana}";

        $sql = "SELECT s.color, year as anio, semana, class 
                FROM semanas_colores s 
                LEFT JOIN produccion_colores p ON s.color = p.color 
                WHERE (semana <= getWeek(CURRENT_DATE) AND year = YEAR(CURRENT_DATE)) OR year < YEAR(CURRENT_DATE) 
                ORDER BY year DESC, semana DESC";
        $response->semanas = $this->db->queryAll($sql);
        $response->color = $this->db->queryOne("SELECT class FROM semanas_colores s LEFT JOIN produccion_colores p ON s.color = p.color WHERE 1=1 $sWhere2");

        $sql = "SELECT GROUP_CONCAT(DISTINCT idFinca SEPARATOR ',') ids, nombre
                FROM lotes
                WHERE status = 'Activo' AND visible_enfunde = 1
                GROUP BY nombre
                ORDER BY nombre+0";
        $response->lotes = $this->db->queryAll($sql);
        $response->lotes[] = (object)["ids" => "", "nombre" => "TOTAL"];

        $sql = "SELECT id as id_finca, fincas.nombre as finca
                FROM  fincas
                WHERE status = 1 AND status_enfunde = 'Activo'";
        $response->data = $this->db->queryAll($sql);

        foreach($response->data as $finca){
            foreach($response->lotes as $lote){

                if($lote->nombre != 'TOTAL'){
                    $sql = "SELECT SUM(usadas)
                            FROM produccion_enfunde
                            WHERE id_finca = $finca->id_finca AND lote = '{$lote->nombre}' $sWhere";
                }else{
                    $sql = "SELECT SUM(usadas)
                            FROM produccion_enfunde
                            WHERE id_finca = $finca->id_finca $sWhere";
                }

                $finca->{"lote_{$lote->nombre}"} = $this->db->queryOne($sql);
                if($finca->{"lote_{$lote->nombre}"}){
                    $finca->{"lote_{$lote->nombre}"} = (int) $finca->{"lote_{$lote->nombre}"};
                }
            }
        }

        return $response;
    }

    public function guardar(){
        $response = new stdClass;
        $response->status = 200;

        $id_finca = $this->postdata->id_finca;
        $lote = $this->postdata->lote;
        $anio = $this->postdata->anio;
        $semana = $this->postdata->semana;
        $usadas = (int) $this->postdata->usadas;

        if($id_finca > 0 && $lote != '' && $anio > 0 && $semana > 0){
            $sql = "SELECT COUNT(1) FROM produccion_enfunde WHERE years = $anio AND semana = $semana AND id_finca = $id_finca AND lote = '{$lote}'";
            $e = (int) $this->db->queryOne($sql);
            if($e > 0){
                if($usadas > 0){
                    $this->db->query("UPDATE produccion_enfunde SET usadas = $usadas WHERE years = $anio AND semana = $semana AND id_finca = $id_finca AND lote = '{$lote}'");
                }else{
                    $this->db->query("DELETE FROM produccion_enfunde WHERE years = $anio AND semana = $semana AND id_finca = $id_finca AND lote = '{$lote}'");    
                }
            }else{
                $this->db->query("INSERT INTO produccion_enfunde SET usadas = $usadas, years = $anio, semana = $semana, id_finca = $id_finca, lote = '{$lote}'");
            }
        }

        return $response;
    }
}
