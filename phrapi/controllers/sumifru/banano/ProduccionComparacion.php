<?php defined('PHRAPI') or die("Direct access not allowed!");

class ProduccionComparacion {
	public $name;
	private $db;
	private $config;

	public function __construct(){
        $this->config = $GLOBALS['config'];
		$this->session = Session::getInstance();
        $this->db = DB::getInstance($this->session->agent_user);
    }

    public function last(){
        $response = new stdClass;
        $response->last_year = $this->db->queryOne("SELECT MAX(anio) FROM produccion_resumen_zonas_semanas");
        $response->last_week = $this->db->queryOne("SELECT MAX(semana) FROM produccion_resumen_zonas_semanas WHERE anio = $response->last_year");

        $response->anios = $this->db->queryAllOne("SELECT anio FROM produccion_resumen_zonas_semanas WHERE anio > 0 GROUP BY anio");
        $response->semanas = $this->db->queryAllOne("SELECT semana FROM produccion_resumen_zonas_semanas WHERE semana > 0 AND anio = $response->last_year GROUP BY semana");
        return $response;
    }
    
    public function index(){
        $response = new stdClass;
        $response->data = $this->zonas();
        foreach($response->data as $zona){
            $zona->children = $this->fincas($zona->zona);
            foreach($zona->children as $finca){
                $finca->children = $this->empacadoras($finca->area);
            }
        }

        $postdata = (object)json_decode(file_get_contents("php://input"));
        $response->semanas = $this->db->queryAllOne("SELECT semana FROM produccion_resumen_zonas_semanas WHERE anio = $postdata->anio GROUP BY semana ORDER BY semana");
        $response->data2 = $this->dataHistorica($response->semanas);
        return $response;
    }

    private function getSemanasVariable($var, $year){
		$sql = [];
		for($i = 1; $i <= 53; $i++){
			$sql[] = "SELECT $i AS semana, zona AS 'name', AVG(sem_{$i}) cantidad FROM produccion_resumen_zonas WHERE sem_{$i} > 0 AND variable = '$var' AND anio = $year GROUP BY zona";
		}
		return implode("
		UNION ALL
		", $sql);
	}

    public function grafica(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;

        $sql = "SELECT semana as label_x, ROUND(AVG(cantidad), 2) AS 'value', 0 AS index_y, name
                FROM (
                    {$this->getSemanasVariable($postdata->variable, $postdata->anio)}
                ) tbl
                WHERE cantidad > 0
                GROUP BY semana, name
                ORDER BY semana, name";

        $data_chart = $this->db->queryAll($sql);
        $groups = [
			[
				"name" => $postdata->variable,
				"type" => 'line',
				'format' => ''
			]
        ];
        $selected = [];
        $types = ['line'];
        $response->data = $this->grafica_z($data_chart, $groups, $selected, $types);
        return $response;
    }

    private function dataHistorica($semanas){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $sql = "SELECT variable, variable AS variable_name FROM produccion_resumen_zonas WHERE anio = $postdata->anio GROUP BY variable ORDER BY orden";
        $variables = $this->db->queryAll($sql);
        foreach($variables as $var){
            foreach($semanas as $sem){
                $oper = "AVG";
                if(in_array($var->variable, ["RACIMOS CORTADOS", "RACIMOS PROCESDOS", "RACIMOS RECUSADOS", "ENFUNDE", "CAJAS CONV"])) $oper = "SUM";
				$var->{"sem_$sem"} = (float) $this->db->queryOne("SELECT {$oper}(sem_{$sem}) FROM produccion_resumen_zonas WHERE anio = $postdata->anio AND variable = '{$var->variable}'");
				$var->{"sem_$sem"} = $var->{"sem_$sem"} > 0 ? round($var->{"sem_$sem"}, 2) : $var->{"sem_$sem"};
            }

            $var->promedio = $this->db->queryOne("SELECT ROUND(AVG(promedio), 2) FROM produccion_resumen_zonas WHERE anio = $postdata->anio AND variable = '{$var->variable}'");
            $sql = "SELECT *, zona AS zona_num, CONCAT('ZONA ', zona) AS variable, '{$var->variable}' AS variable_name FROM produccion_resumen_zonas WHERE anio = $postdata->anio AND variable = '{$var->variable}'";
            $var->children = $this->db->queryAll($sql);
            foreach($var->children as $zona){
                $sql = "SELECT *, finca AS variable, '{$var->variable}' AS variable_name FROM produccion_resumen_fincas WHERE anio = $postdata->anio AND variable = '{$var->variable}' AND finca IN (SELECT nombre FROM fincas WHERE zona = '{$zona->zona_num}')";
                $zona->children = $this->db->queryAll($sql);
            }
        }
        return $variables;
    }

    private function zonas(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $sWhere = "";
        if(isset($postdata->anio) && $postdata->anio > 0){
            $sWhere .= " AND anio = $postdata->anio";
        }
        if(isset($postdata->semana) && $postdata->semana > 0){
            $sWhere .= " AND semana = $postdata->semana";
        }

        $sql = "SELECT *, CONCAT('ZONA ', zona) AS 'area'
                FROM produccion_resumen_zonas_semanas
                WHERE 1=1 $sWhere
                ORDER BY zona";
        return $this->db->queryAll($sql);
    }

    private function fincas($zona){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $sWhere = "";
        if(isset($postdata->anio) && $postdata->anio > 0){
            $sWhere .= " AND anio = $postdata->anio";
        }
        if(isset($postdata->semana) && $postdata->semana > 0){
            $sWhere .= " AND semana = $postdata->semana";
        }

        $sql = "SELECT sector AS 'area'
                FROM fincas
                WHERE zona = '{$zona}'
                GROUP BY sector";
        $fincas = $this->db->queryAll($sql);
        $_data = [];
        foreach($fincas as $f){
            $ff = $this->db->queryRow("SELECT * FROM produccion_resumen_fincas_semanas WHERE finca = '{$f->area}' $sWhere");
            $ff->area = $f->area;
            $_data[] = $ff;
        }
        return $_data;
    }

    private function empacadoras($finca){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $sWhere = "";
        if(isset($postdata->anio) && $postdata->anio > 0){
            $sWhere .= " AND anio = $postdata->anio";
        }
        if(isset($postdata->semana) && $postdata->semana > 0){
            $sWhere .= " AND semana = $postdata->semana";
        }

        $sql = "SELECT id, nombre
                FROM fincas
                WHERE sector = '{$finca}'";
        $empacadoras = $this->db->queryAll($sql);
        $_empacadoras = [];

        if(count($empacadoras) > 1){
            foreach($empacadoras as $row){
                if($row->nombre != $finca){
                    $ff = $this->db->queryRow("SELECT * FROM produccion_resumen_tabla_semanas WHERE id_finca = '{$row->id}' $sWhere");

                    if(!($postdata->anio == 2018 && $postdata->semana <= 20))
                    if($ff->racimos_cosechados > 0 || $ff->cajas > 0){
                        $ff->area = $row->nombre;
                        $_empacadoras[] = $ff;
                    }
                }
            }
        }

        return $_empacadoras;
    }

    private function grafica_z($data = [], $group_y = [], $selected = null, $types = []){
		$options = [];
		$options["tooltip"] = [
			"trigger" => 'axis',
			"axisPointer" => [
				"type" => 'cross',
				"crossStyle" => [
					"color" => '#999'
				]
			]
		];
		$options["toolbox"] = [
			"feature" => [
				"dataView" => [
					"show" => true,
					"readOnly" => false
				],
				"magicType" => [
					"show" => true,
					"type" => ['line', 'bar']
				],
				"restore" => [
					"show" => true
				],
				"saveAsImage" => [
					"show" => true
				]
			]
		];
		$options["legend"]["data"] = [];
		$options["legend"]["bottom"] = "0%";
        $options["legend"]["left"] = "center";
        $options["legend"]["selected"] = $selected;
		$options["xAxis"] = [
			[
				"type" => 'category',
				"data" => [],
				"axisPointer" => [
					"type" => 'shadow'
				]
			]
		];
		/*
			[
				type => 'value',
				name => {String},
				min => 0,
				max => 200,
				interval => 5,
				axisLabel => [
					formatter => {value} KG
				]
			]
		*/
		$options["yAxis"] = [];
		/*
			[
				name => {String},
				type => 'line',
				data => [
					{double}, {double}, {double}
				]
			]
		*/
		$options["series"] = [];

		$maxs = [];
		$mins = [];
		$prepare_data = [];
		$_x = [];
		$_names = [];
		$_namess = [];
		foreach($data as $d){
			$d = (object) $d;
			if(!isset($maxs[$d->index_y])) if($d->value > 0)
				$maxs[$d->index_y] = $d->value;
			if($d->value > $maxs[$d->index_y]) if($d->value > 0)
				$maxs[$d->index_y] = $d->value;

			if(!isset($mins[$d->index_y])) if($d->value > 0)
				$mins[$d->index_y] = $d->value;
			if($d->value < $mins[$d->index_y]) if($d->value > 0)
				$mins[$d->index_y] = $d->value;

			if(!in_array($d->label_x, $_x)){
				$_x[] = $d->label_x;
			}
			if(!in_array($d->name, $_namess)){
				$_namess[] = $d->name;
				 
				$n = ["name" => $d->name, "group" => $d->index_y];
				if(isset($d->line)){
					$n["line"] = $d->line;
				}
				$_names[] = $n;
			}
			$prepare_data[$d->label_x][$d->name] = $d->value;
        }

		foreach($group_y as $key => $col){
			$col = (object) $col;
			$options["yAxis"][] = [
				'type' => 'value',
				'name' => $col->name,
				//'max' => ($key == 1) ? $maxs[$key] + ($maxs[$key] - $mins[$key]) * .05 : null,
                //'min' => ($key == 1) ? $mins[$key] - ($maxs[$key] - $mins[$key]) * .05 : null,
                'max' => null,
                'min' => 'dataMin',
				'axisLabel' => [
					'formatter' => "{value} $col->format"
				]
			];
		}

		foreach($_x as $row){
			$options["xAxis"][0]["data"][] = $row;
        }

		foreach($_names as $i => $name){
			$name = (object) $name;

			if(!in_array($name->name, $options["legend"]["data"]))
				$options["legend"]["data"][] = $name->name;

			$serie = [
				"name" => $name->name,
				"type" => isset($types[$i]) ? $types[$i] : 'line',
				"connectNulls" => true,
				"data" => []
			];
			if($name->group > 0)
				$serie["yAxisIndex"] = $name->group;

			if(isset($name->line)){
				$serie["itemStyle"]["normal"]["lineStyle"]["width"] = 5;
			}

			foreach($_x as $row){
				$val = 0;
				if(isset($prepare_data[$row][$name->name]))
					$val = $prepare_data[$row][$name->name];

				if($val > 0)
					$serie["data"][] = $val;
				else
					$serie["data"][] = null;
			}
			$options["series"][] = $serie;
		}

		return $options;
	}
}
