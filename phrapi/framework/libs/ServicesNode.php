<?php

class ServicesNode
{
	private $api;
	private $token;
	public $debug = false;

	public function __construct($api, $token) {
		$this->api = $api;
		$this->token = $token;
	}

	public function call($resource = '', $method = 'OPTIONS', $args = [], $logs = []) {
		$headers = ['xpublic: ' . $this->token];
		// if ($this->debug) {
		// 	$headers[] = 'suppress_response_codes: true';
		// 	$headers[] = 'debug_mode: true';
		// }

		// !isset($logs['type'])? $logs['type'] = hte_LogsConsts::TIPO_NO_DEFINIDO : 0;
		// !isset($logs['place'])? $logs['place'] = '' : 0;

		// $headers[] = "X-Log-Type: {$logs['type']}";
		// $headers[] = "X-Log-Place: {$logs['place']}";
		D($this->api . $resource);
		return getRemote([
			'url' => $this->api . $resource,
			'headers' => $headers,
			'method' => $method,
			'return' => 'raw',
			'debug' => $this->debug,
			'save' => false,
			'args' => $args
		]);
	}

	public function get($resource = '', $args = [], $logs = []) {
		return $this->call($resource, 'GET', $args, $logs);
	}

	public function post($resource = '', $args = [], $logs = []) {
		return $this->call($resource, 'POST', $args, $logs);
	}

	public function put($resource = '', $args = [], $logs = []) {
		return $this->call($resource, 'PUT', $args, $logs);
	}

	public function delete($resource = '', $args = [], $logs = []) {
		return $this->call($resource, 'DELETE', $args, $logs);
	}
}
