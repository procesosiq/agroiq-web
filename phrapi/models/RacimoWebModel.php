<?php defined('PHRAPI') or die("Direct access not allowed!");

class RacimoWebModel {
	public $data;
	private $db;

	public function __construct(){
		$this->session = Session::getInstance();
        $this->db = DB::getInstance($this->session->agent_user);
        $this->piq = DB::getInstance();

        $this->unidad_default = $this->db->queryOne("SELECT baalaza_racimos_unidad FROM companies WHERE id_company = {$this->session->id_company}");
        $this->unidad_web = $this->session->unidad_racimos;
    }

    public function getById($id){
        $this->data = $this->db->queryRow("SELECT * FROM racimo_web WHERE id = {$id}");
        return $this->data;
    }

    // ACTUALIZAR RACIMO
    public function update($data = []){
        $response = new stdClass;
        $response->status = 400;
        if($this->data && isset($this->data->id) && $this->data->id > 0){
            
        }else{
            $response->error = "Es necesario iniciliar el racimo primero";
        }
        return $response;
    }
    
    // CAMBIAR PESO
    public function setWeight($peso){
        $response = new stdClass;
        $response->status = 400;
        if($this->data && isset($this->data->id) && $this->data->id > 0){
            $peso = (float) $peso;
            if($peso >= 0){
                $response->status = 200;
                $sql = "UPDATE racimo_web
                        SET
                            peso            = convertir({$peso}, '{$this->unidad_web}', '{$this->unidad_default}'),
                            peso_reportes   = convertir({$peso}, '{$this->unidad_web}', '{$this->unidad_default}'),
                            peso_kg         = convertir({$peso}, '{$this->unidad_default}', 'kg'),
                            peso_lb         = convertir({$peso}, '{$this->unidad_default}', 'lb'),
                            updated_at = CURRENT_TIMESTAMP
                        WHERE id = {$this->data->id}";
                $response->result = $this->db->query($sql);
            }else{
                $response->error = "Peso invalido";
            }
        }else{
            $response->error = "Es necesario iniciliar el racimo primero";
        }
        return $response;
    }

    // MOVER RACIMO A OTRO VIAJE
    public function moveToAnotherRide($id_viaje){
        $response = new stdClass;
        $response->status = 400;
        if($this->data && isset($this->data->id) && $this->data->id > 0){
            /* 
                VALIDAR VIAJE EXISTE
            */
            $viaje = $this->db->queryOne("SELECT numero FROM viajes WHERE id = {$id_viaje}");
            if(!$viaje){
                $response->error = "Viaje invalido";
                return $response;
            }

            /*
                CALCULAR NUMERO DE RACIMO EN EL NUEVO VIAJE (COLOCAR AL FINAL)
            */
            $racimo = 0;
            $num_racimos = $this->db->queryRow("SELECT COUNT(1) count, MAX(racimo) max FROM racimo_web WHERE id_viaje = {$id_viaje}");
            if($num_racimos->count == 0){
                $racimo = 1;
            }else{
                $racimo = ((int) $num_racimos->max) + 1;
            }

            $response->status = 200;
            $sql = "UPDATE racimo_web SET 
                        id_viaje = {$id_viaje},
                        viaje = {$viaje},
                        racimo = {$racimo}
                    WHERE id = {$this->data->id}";
            $response->result = $this->db->query($sql);
        }else{
            $response->error = "Es necesario iniciliar el racimo primero";
        }
        return $response;
    }

    // CAMBIAR EDAD (CAMBIAR CINTA)
    public function setAge($edad){
        $response = new stdClass;
        $response->status = 400;
        if($this->data && isset($this->data->id) && $this->data->id > 0){
            if($edad){
                $response->status = 200;

                if($edad != $this->data->edad || !$this->data->id_cinta){
                    $semana_enfundada = $this->db->queryOne("SELECT getSemanaEnfundada({$edad}, {$this->data->semana}, {$this->anio})");
                    $sql = "UPDATE racimo_web SET 
                                id_cinta = getIdCintaFromEdad({$edad}, semana, anio),
                                edad = {$edad},
                                semana_enfundada = {$semana_enfundada},
                                anio_enfundado = IF({$semana_enfundada} > semana, anio-1, anio),
                                updated_at = CURRENT_TIMESTAMP
                            WHERE id = {$this->data->id}";
                    $response->result = $this->db->query($sql);
                }
            }else{
                $response->error = "Edad invalida";
            }
        }else{
            $response->error = "Es necesario iniciliar el racimo primero";
        }
        return $response;
    }
}