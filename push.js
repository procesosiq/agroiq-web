const readline  = require('readline');
const shell     = require('shelljs');
const colors    = require('colors');
const ProgressBar = require('progress');
let rl = null

function resetRL(){
    rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout
    });
}

function clear(){
    console.log('\x1Bc');
}

function esperar(text = '', segundos = 5){
    return new Promise((resolve) => {
        console.log('')
        let bar = new ProgressBar(`[${text}] | Piense su respuesta [:bar]`, { total: segundos, width : 20 });
        let timer = setInterval(function () {
            bar.tick();
            if (bar.complete) {
                bar.terminate()
                clearInterval(timer);
                resolve()
            }
        }, 1000);
    })
}

function preguntar(question, _esperar = true){
    return new Promise(async (resolve) => {
        if(_esperar){
            await esperar(question)
        }
        
        resetRL()
        rl.question(question, (answer) => {
            resolve(answer)
            rl.close();
            rl.clearLine()
        });
    })
}

async function ejecutar(){
    clear()
    if (!shell.which('git')) {
        shell.echo('Sorry, this script requires git');
        shell.exit(1);
    }
    console.log(colors.yellow('Archivos modificados'))
    console.log(colors.yellow('--------------------'))
    shell.exec(`git diff --name-only`)
    console.log('')

    let confirm = await preguntar('¿Seguro de incluir todos estos archivos (y/n)? ', false)
    confirm = (confirm || "").toUpperCase()
    if(confirm === 'Y'){
        let commit_desc = await preguntar('Descripción del commit : ', false)
        if(commit_desc){
            let from_rama = (await preguntar('Rama actual (default : master) ', false)) || 'master'
            let to_rama = (await preguntar('Rama de producción (default : stable) ', false)) || 'stable'
            let remote = (await preguntar('Remote de producción (default : production) ', false)) || 'production'

            console.log(colors.yellow('Confirmación'))
            console.log(`git add -A`)
            console.log(`git commit -m "${commit_desc}"`)
            console.log(`git pull --rebase origin ${from_rama}`)
            console.log(`git checkout ${to_rama}`)
            console.log(`git pull --rebase origin ${to_rama}`)
            console.log(`git merge --ff-only ${from_rama}`)
            console.log(`git push ${remote} ${to_rama}`)
            console.log(`git push origin ${from_rama}`)
            console.log(`git push origin ${to_rama}`)
            console.log(`git checkout ${from_rama}`)
            console.log('')

            let r = ((await preguntar('¿Todo OK? (y/n) ', false)) || '').toUpperCase()
            if(r === 'Y'){
                console.log('Haciendo push ...')
                shell.exec(`git add -A`)
                shell.exec(`git commit -m "${commit_desc}"`)
                shell.exec(`git pull --rebase origin ${from_rama}`)
                shell.exec(`git checkout ${to_rama}`)
                shell.exec(`git pull --rebase origin ${to_rama}`)
                shell.exec(`git merge --ff-only ${from_rama}`)
                shell.exec(`git push ${remote} ${to_rama}`)
                shell.exec(`git push origin ${from_rama}`)
                shell.exec(`git push origin ${to_rama}`)
                shell.exec(`git checkout ${from_rama}`)
            }
        }else{
            console.log(colors.red('La descripción es requerida'))
        }
    }
    shell.exit(1)
}

ejecutar()