const {Builder, By, Key, until} = require('selenium-webdriver');
const moment = require('moment')

async function example() {
    let driver = await new Builder().forBrowser('chrome').build();

    try {
        await driver.get('http://dev-app.procesos-iq.com/login.php');
        await driver.findElement(By.name('username')).sendKeys('orodelti')
        await driver.findElement(By.name('password')).sendKeys('1234')
        await driver.findElement(By.css('button[type=submit]')).click()

        let fecha = moment().format('YYYY-MM-DD')
        let id_finca = 1
        await driver.get('http://dev-app.procesos-iq.com/pdfProduccionDia?fecha_inicial='+fecha+'&finca='+id_finca)
        await driver.sleep(60 * 1000)
    } 
    catch(e){
        console.log(e)
    }
    finally {
        await driver.quit();
    }
}
example()