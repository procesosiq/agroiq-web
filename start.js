var app = angular.module('procesosiq', []);
app.run(function($window, $rootScope) {
    $rootScope.online = navigator.onLine;
    $window.addEventListener("offline", function () {
        $rootScope.$apply(function() {
            alert("Se perdio la conexion a Internet" , "Procesos IQ");
        });
    }, false);
    $window.addEventListener("online", function () {
        $rootScope.$apply(function() {
        });
    }, false);
});

function ahttp (){
    var _this = this;
    _this = {
    url : "",
    method : "",
    header : {},
    data : {},
    callback : "" ,
    required : function(){
        if(!this.url) throw Error("url es Requida");
        if(!this.method) throw Error("Metodo es Requida");
        if(!this.dataType) this.dataType = 'json';
    },
    block : function(){
        load.block(ahttp.target);
    },
    unblock: function() { load.unblock(ahttp.target);  }
    }

    function call(url , method , callback,  data, header , dataType){
        _this.url = url;
        _this.method = method;
        _this.header = header;
        _this.data = data;
        _this.callback = callback;
        _this.required();
        //_this.block();
        var request = $.ajax({
            url: _this.url,
            method: _this.method ,
            data: _this.data,
            dataType: _this.dataType
        });

        request.success(function( r ) {
            // console.log(r);
            callback(r , _this.unblock);
        });

        request.fail(function( jqXHR, textStatus ) {
            load.unblock(ahttp.target);
            // console.log(jqXHR);
        });
    }

    ahttp.target  = "";

    ahttp.prototype.get = function(url , callback, data , header , dataType){
        return call(url , "get" , callback, data , header , dataType)
    }
    ahttp.prototype.post = function(url , callback, data , header , dataType){
        return call(url , "post" , callback, data , header , dataType)
    }
    ahttp.prototype.put = function(url , callback, data , header , dataType){
        return call(url , "put" , callback, data , header , dataType)
    }
    ahttp.prototype.delete = function(url , callback, data , header ,dataType){
        return call(url , "delete" , callback, data , header , dataType)
    }
};
var ahttp = new ahttp();

$(".module-secundary").hide()
app.controller('controller', ['$scope', function($scope){

    $scope.cultivoSelected = ''
    $scope.cultivos = [
        'CACAO',
        'BANANO',
        'PALMA'
    ];
    $scope.back = ''

    $scope.setModule = (modulee, back = '') => {
        if(modulee){
            $scope.back = back
            $scope.cultivoSelected = modulee
            $(".module-primary").hide()
            $(`#${modulee}-modules .module-secundary`).show()
            ahttp.post('phrapi/access/setModule', (r) => {
                console.log(r)
            },  {
                module : modulee
            })
        }
    }

    $scope.clearModule = () => {
        $scope.cultivoSelected = ''
        $(".module-primary").show()
        $(".module-secundary").hide()
        $scope.setModule($scope.back)
    }

    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
        $(".arrow-back").css('position', 'relative')
    }

}])

$(document).ready(function() {
    $('#merma_lara').click((e) => {
        e.preventDefault();
        window.location = 'http://agroiq-lara.procesosiq.com/';
    });

    $('#merma_rufcorp').click((e) => {
        e.preventDefault();
        window.location = 'http://agroiq-ruforcorp.procesosiq.com/';
    });
});
