<? include_once 'phrapi/index.php' ?>
<? $factory->Access->if_not_logged_redirect() ?>
<?php
    $session = Session::getInstance();
    $paths = new stdClass;
    $paths->sigat = 'http://sigat.procesos-iq.com/controllers/Aceso.php?token='.$session->settings->token;
    $paths->sigat_module = '/sigat';
    if(in_array($session->id_company, [4, 6, 12]))
        $paths->agroaudit = '/laboresAgricolas';
    else if(in_array($session->id_company, [17, 3]))
        $paths->agroaudit = '/laboresAgricolasDia';
    else
        $paths->agroaudit = '/reportes';

    $paths->merma = '/merma';
    $paths->calidad = '/calidad';
    if(in_array($session->id_company, [7,11,12])) $paths->calidad = 'calidad2';
    $paths->membresias = '/membresias';
    $paths->configuracion = '/configLotes';

    $paths->produccion = '/produccionsemana';
    $paths->benchmark = '/produccionComparacion';
    $paths->lancofruit = $session->logged == 49 ? '/perchasDia' : '/rastreoLancofruit';
    $paths->clima = '/climaDiario';
    $paths->sensores = '/analisisSensorial';
    $paths->codigos_mallas = '/asignacionMallas';
    $paths->cosecha = '/balanzaCosecha';

    if($session->id_company == 3){
        $paths->tthh = '/revisionAsistencia';
    }else{
        $paths->tthh = '/bonificacion';
    }

    if($session->id_company == 6){
        $paths->produccion = '/produccionsemana';
    }
    $paths->palma_labores_agricolas = "palmaLaboresAgricolasDia";
    
    $LARA_COMPANY = 3;
    $RUFORCORP_COMPANY = 20;
    
    $settings = new stdClass;
    $settings->href = new stdClass;
    $settings->href->sigat = ($session->settings->sigat == 'Activo') ? 'href="'.$paths->sigat.'" target="_blank"' : '';
    $settings->href->agroaudit = ($session->settings->agroaudit == 'Activo') ? 'href="'.$paths->agroaudit.'"' : '';
    
    if($session->id_company == $LARA_COMPANY) {
      $settings->href->merma = ($session->settings->merma == 'Activo') ? 'href="'.$paths->merma.'"'.' id="merma_lara"' :'';
    } else if($session->id_company == $RUFORCORP_COMPANY) {
      $settings->href->merma = ($session->settings->merma == 'Activo') ? 'href="'.$paths->merma.'"'.' id="merma_rufcorp"' :'';
    } else {
      $settings->href->merma = ($session->settings->merma == 'Activo') ? 'href="'.$paths->merma.'"' : '';
    }

    $settings->href->calidad = ($session->settings->calidad == 'Activo') ? 'href="'.$paths->calidad.'"' : '';
    $settings->href->membresias = ($session->settings->membresias == 'Activo') ? 'href="'.$paths->membresias.'"' : '';
    $settings->href->configuracion = ($session->settings->configuracion == 'Activo') ? 'href="'.$paths->configuracion.'"' : '';
    $settings->href->produccion = ($session->settings->produccion == 'Activo') ? 'href="'.$paths->produccion.'"' : '';
    $settings->href->tthh = ($session->settings->tthh == 'Activo') ? 'href="'.$paths->tthh.'"' : '';
    $settings->href->lancofruit = ($session->settings->lancofruit == 'Activo') ? 'href="'.$paths->lancofruit.'"' : '';
    $settings->href->benchmark = ($session->settings->benchmark == 'Activo') ? 'href="'.$paths->benchmark.'"' : '';
    $settings->href->clima = ($session->settings->clima == 'Activo') ? 'href="'.$paths->clima.'"' : '';
    $settings->href->sensores = ($session->settings->sensores == 'Activo') ? 'href="'.$paths->sensores.'"' : '';
    $settings->href->codigos_mallas = ($session->settings->codigos_mallas == 'Activo') ? 'href="'.$paths->codigos_mallas.'"' : '';
    $settings->href->cosecha = ($session->settings->cosecha == 'Activo') ? 'href="'.$paths->cosecha.'"' : '';
    $settings->href->palma_labores_agricolas = ($session->settings->palma_labores_agricolas == 'Activo') ? 'href="'.$paths->palma_labores_agricolas.'"' : '';

    $settings->css = new stdClass;
    $settings->css->active = 'webapp-btn';
    $settings->css->sigat = ($session->settings->sigat == 'Activo' || $session->settings->sigat_module == 'Activo') ? 'webapp-btn' : 'webapp-btn-disabled';
    $settings->css->agroaudit = ($session->settings->agroaudit == 'Activo') ? 'webapp-btn' : 'webapp-btn-disabled';
    $settings->css->merma = ($session->settings->merma == 'Activo') ? 'webapp-btn' : 'webapp-btn-disabled';
    $settings->css->calidad = ($session->settings->calidad == 'Activo') ? 'webapp-btn' : 'webapp-btn-disabled';
    $settings->css->membresias = ($session->settings->membresias == 'Activo') ? 'webapp-btn' : 'webapp-btn-disabled';
    $settings->css->configuracion = ($session->settings->configuracion == 'Activo') ? 'webapp-btn' : 'webapp-btn-disabled';
    $settings->css->tthh = ($session->settings->tthh == 'Activo') ? 'webapp-btn' : 'webapp-btn-disabled';
    $settings->css->produccion = ($session->settings->produccion == 'Activo') ? 'webapp-btn' : 'webapp-btn-disabled';
    $settings->css->lancofruit = ($session->settings->lancofruit == 'Activo') ? 'webapp-btn' : 'webapp-btn-disabled';
    $settings->css->benchmark = ($session->settings->benchmark == 'Activo') ? 'webapp-btn' : 'webapp-btn-disabled';
    $settings->css->clima = ($session->settings->clima == 'Activo') ? 'webapp-btn' : 'webapp-btn-disabled';
    $settings->css->sensores = ($session->settings->sensores == 'Activo') ? 'webapp-btn' : 'webapp-btn-disabled';
    $settings->css->codigos_mallas = ($session->settings->codigos_mallas == 'Activo') ? 'webapp-btn' : 'webapp-btn-disabled';
    $settings->css->cosecha = ($session->settings->cosecha == 'Activo') ? 'webapp-btn' : 'webapp-btn-disabled';
    $settings->css->palma_labores_agricolas = ($session->settings->palma_labores_agricolas == 'Activo') ? 'webapp-btn' : 'webapp-btn-disabled';

    /* IMAGEN DE PERFIL */
    $profile = "../assets/layouts/layout4/img/avatar9.jpg";
    $profile = "";
    if($session->id_company == 2)
        $profile = "./logos/logo_pubenza.png";
    if($session->id_company == 11)
        $profile = "./logos/logo_sumifru.png";

?>
<!DOCTYPE html>
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>Dashboard | Seccion Sistema</title>
        <base href="<?=$config['url']?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN LAYOUT FIRST STYLES -->
        <link href="//fonts.googleapis.com/css?family=Oswald:400,300,700" rel="stylesheet" type="text/css" />
        <!-- END LAYOUT FIRST STYLES -->
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="assets/layouts/layout6/css/layout.css" rel="stylesheet" type="text/css" />
        <link href="assets/layouts/layout6/css/custom.css" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" /> 
    </head>
    <!-- END HEAD -->
    <style>
        .webapp-btn-disabled {
            width: 100%;
            display: block;
            cursor: auto !important;
            text-align: left;
            padding: 11px 15px;
            border: 1px solid #384047; 
            text-decoration: none !important;
        }
        .webapp-btn-disabled h3 {
            margin: 0;
            font-size: 42px;
            font-weight: 500;
            line-height: 1.4;
            color: #7C8284;
            /*color: #009dc7;*/
            text-transform: uppercase;
            font-family: 'Oswald', sans-serif; 
        }
        .webapp-btn-disabled p {
            color: #586978;
            font-size: 17px;
            margin-bottom: 0;
            text-transform: capitalize; 
        }

        .webapp-btn-disabled img {
            filter: url('#grayscale'); /* Versión SVG para IE10, Chrome 17, FF3.5, Safari 5.2 and Opera 11.6 */
            -webkit-filter: grayscale(100%);
            -moz-filter: grayscale(100%);
            -ms-filter: grayscale(100%);
            -o-filter: grayscale(100%);
            filter: grayscale(100%); /* Para cuando es estándar funcione en todos */
            filter: Gray(); /* IE4-8 and 9 */

            -webkit-transition: all 0.5s ease;
            -moz-transition: all 0.5s ease;
            -ms-transition: all 0.5s ease;
            -o-transition: all 0.5s ease;
            transition: all 0.5s ease;
            }
            .grises img:hover { 
            -webkit-filter: grayscale(0%);
            -moz-filter: grayscale(0%);
            -ms-filter: grayscale(0%);
            -o-filter: grayscale(0%);
            filter: none;

            -webkit-transition: all 0.5s ease;
            -moz-transition: all 0.5s ease;
            -ms-transition: all 0.5s ease;
            -o-transition: all 0.5s ease;
            transition: all 0.5s ease;
        }
        @media (min-width: 768px){
            .center-body {
                text-align: center !important; 
                display: table-cell !important; 
                vertical-align: none !important; 
            }
        }
        .arrow-back {
            position: absolute;
            width : 50px;
            height : 50px;
            margin-left : 20px;
        }
        .module, .module-primary {
            transition: all 2s linear;
        }
        .img-circle {
            float: left;
            height: 39px;
            margin-top: -8px;
            margin-right: 7px;
        }
    </style>
    <body class="" ng-app="procesosiq" ng-controller="controller">
        <!-- BEGIN HEADER -->
        <header class="page-header">
            <nav class="navbar" role="navigation">
                <div class="container-fluid">
                    <div class="havbar-header">
                        <!-- BEGIN LOGO -->
                        <a id="index" class="navbar-brand" href="start.php">
                            <img src="./logo.png" alt="Logo"> 
                        </a>
                        <!-- END LOGO -->
                        <!-- BEGIN TOPBAR ACTIONS -->
                        <div class="top-menu">
                            <ul class="nav navbar-nav pull-right">
                                <li class="dropdown dropdown-user dropdown-dark">
                                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                        <span class="username username-hide-on-mobile"> <?php print Session::getInstance()->nombre; ?> </span>
                                        <img alt="" class="img-circle" <?php if($profile != ''): ?>src="<?= $profile ?>" <?php endif; ?> />
                                    </a>
                                    <ul class="dropdown-menu dropdown-menu-default">
                                        <li>
                                            <a href="phrapi/access/logout">
                                                <i class="icon-key"></i> Log Out 
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <!-- END TOPBAR ACTIONS -->
                    </div>
                </div>
                <!--/container-->
            </nav>
        </header>
        <!-- END HEADER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-content">
            <img src="img/arrow-back.png" ng-show="cultivoSelected" class="arrow-back" ng-click="clearModule()">
            <div class="container">
                <!-- BEGIN PAGE BASE CONTENT -->
                <!-- Center Wrap BEGIN -->
                <div class="center-wrap">                    
                    <div class="center-align">
                        <div class="center-body">
                            <div id="cultivos" ng-hide="cultivoSelected">
                                <? if($session->cultivos->banano == 'Activo'): ?>
                                <div class="col-sm-4 sm-margin-bottom-30 module-primary">
                                    <a class="<?=$settings->css->active?>" style="border:none; height: 150px;" ng-click="setModule('BANANO')">
                                        <h3>Banano</h3>
                                        <p>Banano</p>
                                    </a>
                                </div>
                                <? endif; ?>
                                <? if($session->cultivos->cacao == 'Activo'): ?>
                                <div class="col-sm-4 sm-margin-bottom-30 module-primary">
                                    <a class="<?=$settings->css->active?>" style="border:none; height: 150px;" ng-click="setModule('CACAO')">
                                        <h3>Cacao</h3>
                                        <p>Cacao</p>
                                    </a>
                                </div>
                                <? endif; ?>
                                <? if($session->cultivos->palma == 'Activo'): ?>
                                <div class="col-sm-4 sm-margin-bottom-30 module-primary">
                                    <a class="<?=$settings->css->active?>" style="border:none; height: 150px;" ng-click="setModule('PALMA')">
                                        <h3>Palma</h3>
                                        <p>Palma</p>
                                    </a>
                                </div>
                                <? endif; ?>

                                <? if($session->type_users == 'MONITOR'): ?>
                                <div class="col-sm-4 sm-margin-bottom-30 module-primary">
                                    <a class="<?=$settings->css->active?>" style="border:none; height: 150px;" ng-click="setModule('MONITOR')">
                                        <h3>Monitor</h3>
                                        <p>Monitor</p>
                                    </a>
                                </div>
                                <? endif; ?>
                            </div>
                            <div id="PALMA-modules">
                                <div class="row">
                                    <div class="col-sm-4 sm-margin-bottom-30 module-secundary">
                                        <a class="<?=$settings->css->palma_labores_agricolas?>" style="border:none; height: 150px;" <?=$settings->href->palma_labores_agricolas?>>
                                            <h3>Labores</h3>
                                            <p>Labores Agricolas</p>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div id="CACAO-modules">
                                <div class="row">
                                    <div class="col-sm-4 sm-margin-bottom-30 module-secundary">
                                        <a class="<?=$settings->css->sensores?>" style="border:none; height: 150px;" <?=$settings->href->sensores?>>
                                            <h3>Análisis</h3>
                                            <p>Análisis de Sensorial</p>
                                        </a>
                                    </div>
                                    <div class="col-sm-4 sm-margin-bottom-30 module-secundary">
                                        <a class="<?=$settings->css->codigos_mallas?>" style="border:none; height: 150px;" <?=$settings->href->codigos_mallas?>>
                                            <h3>Asignación de Tareas</h3>
                                            <p>Asignación de Tareas</p>
                                        </a>
                                    </div>
                                    <div class="col-sm-4 sm-margin-bottom-30 module-secundary">
                                        <a class="<?=$settings->css->cosecha?>" style="border:none; height: 150px;" <?=$settings->href->cosecha?>>
                                            <h3>Cosecha</h3>
                                            <p>Cosecha</p>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div id="BANANO-modules">
                                <div class="row">
                                    <div class="col-sm-4 margin-bottom-30 module-secundary">
                                        <a class="<?=$settings->css->sigat?>" style="border:none; height: 150px;" <?=$settings->href->sigat?>>
                                            <img src="./logos/sigat.png" style="width:50%" alt="">
                                            <p>Sigatoka</p>
                                        </a>
                                    </div>
                                    <div class="col-sm-4 margin-bottom-30 module-secundary">
                                        <a class="<?=$settings->css->agroaudit?>" style="border:none; height: 150px;" <?=$settings->href->agroaudit?>>
                                            <img src="./logos/agroaudit.png" style="width:75%;" alt="">
                                            <p>Labores Agrícolas</p>
                                        </a>
                                    </div>
                                    <div class="col-sm-4 margin-bottom-30 module-secundary">
                                        <a class="<?=$settings->css->merma?>" style="border:none; height: 150px;" <?=$settings->href->merma?>>
                                            <img src="./logos/merma.png" style="width:75%;" alt="">
                                            <p>Merma</p>
                                        </a>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4 margin-bottom-30 module-secundary">
                                        <a class="<?=$settings->css->calidad?>" style="border:none; height: 150px;" <?=$settings->href->calidad?>>
                                            <img src="./logos/calidad.png" style="width:75%;" alt="">
                                            <p>Calidad</p>
                                        </a>
                                    </div>
                                    <div class="col-sm-4 margin-bottom-30 module-secundary">
                                        <a class="<?=$settings->css->produccion?>" style="border:none; height: 150px;" ng-click="clearModule(); setModule('BANANO-PRODUCCION', 'BANANO');">
                                            <img src="./logos/produccion.png" style="height:70%;" alt="">
                                            <p>
                                                Produccion 
                                                <?php if($session->id_company != 2): ?>
                                                <span class="pull-right">
                                                    <i class="fa fa-arrow-right"></i>
                                                    <i class="icon-grid"></i>
                                                </span>
                                                <?php endif ; ?>
                                                <?php if($session->id_company == 2): ?>
                                                <span class="pull-right">
                                                    <i class="fa fa-arrow-right"></i>
                                                    <i class="icon-grid"></i>
                                                </span>
                                                <?php endif ; ?>
                                            </p>
                                        </a>
                                    </div>
                                    <? if($session->settings->benchmark == 'Activo'): ?>
                                    <div class="col-sm-4 sm-margin-bottom-30 module-secundary">
                                        <a class="<?=$settings->css->benchmark?>" style="border:none; height: 150px;" <?=$settings->href->benchmark?>>
                                            <h3>Benchmark</h3>
                                            <p>Benchmark</p>
                                        </a>
                                    </div>
                                    <? endif; ?>
                                    <? if($session->settings->lancofruit == 'Activo'): ?>
                                    <div class="col-sm-4 margin-bottom-30 module-secundary">
                                        <a class="<?=$settings->css->lancofruit?>" style="border:none; height: 150px;" ng-click="clearModule(); setModule('BANANO-LANCOFRUIT', 'BANANO');">
                                            <img src="./logos/lancofruit.png" style="height:70%; display:inline;" alt="">
                                            <p>
                                                Lancofruit
                                                <span class="pull-right">
                                                    <i class="fa fa-arrow-right"></i>
                                                    <i class="icon-grid"></i>
                                                </span>
                                            </p>
                                        </a>
                                    </div>
                                    <? endif; ?>
                                    <? if($session->settings->clima == 'Activo'): ?>
                                    <div class="col-sm-4 margin-bottom-30 module-secundary">
                                        <a class="<?=$settings->css->clima?>" style="border:none; height: 150px;" <?=$settings->href->clima?>>
                                            <img src="./logos/clima.png" style="width:60%; display:inline;" alt="">
                                            <p>Clima</p>
                                        </a>
                                    </div>
                                    <? endif; ?>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4 sm-margin-bottom-30 module-secundary">
                                        <a class="<?=$settings->css->membresias?>" style="border:none; height: 150px;" <?=$settings->href->membresias?>>
                                            <h3>Membresías</h3>
                                            <p>Agregar Membresías</p>
                                        </a>
                                    </div>
                                    <div class="col-sm-4 sm-margin-bottom-30 module-secundary">
                                        <a class="<?=$settings->css->configuracion?>" style="border:none; height: 150px;" <?=$settings->href->configuracion?>>
                                            <h3>Configuración</h3>
                                            <p>Configuración</p>
                                        </a>
                                    </div>
                                    <div class="col-sm-4 sm-margin-bottom-30 module-secundary">
                                        <a class="<?=$settings->css->tthh?>" style="border:none; height: 150px;" <?=$settings->href->tthh?>>
                                            <h3>TTHH</h3>
                                            <p>TTHH</p>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div id="BANANO-PRODUCCION-modules">
                                <div class="row">
                                    <div class="col-sm-4 margin-bottom-30 module-secundary">
                                        <a class="<?= $session->settings->produccion == 'Activo' ? 'webapp-btn' : 'webapp-btn-disabled' ?>" 
                                            style="border:none; height: 180px;" 
                                            href="<?= $session->settings->produccion == 'Activo' 
                                                        ? $session->id_company == 2 ? 'produccion' : 'produccionRacimos' 
                                                        : '#' 
                                                    ?>">

                                            <!--<img src="logos/balanza_racimos.png" style="height:70%;" alt="">-->
                                            <h3>Balanza de Racimos</h3>
                                            <p>Balanza de Racimos</p>
                                        </a>
                                    </div>
                                    <div class="col-sm-4 margin-bottom-30 module-secundary">
                                        <a class="<?= $session->settings->produccion == 'Activo' ? 'webapp-btn' : 'webapp-btn-disabled' ?>" 
                                            style="border:none; height: 180px;" 
                                            href="<?= $session->settings->produccion == 'Activo' ? 'produccioncajas' : '#' ?>">

                                            <!--<img src="logos/balanza_cajas.png" style="height:70%;" alt="">-->
                                            <h3>Balanza de Cajas</h3>
                                            <p>Balanza de Cajas</p>
                                        </a>
                                    </div>
                                    <div class="col-sm-4 margin-bottom-30 module-secundary">
                                        <a class="<?= $session->settings->produccion_enfunde == 'Activo' ? 'webapp-btn' : 'webapp-btn-disabled' ?>" 
                                            style="border:none; height: 180px;" 
                                            href="<?= $session->settings->produccion_enfunde == 'Activo' ? 'produccionenfunde' : '#' ?>">

                                            <!--<img src="logos/enfunde.png" style="height:70%;" alt="">-->
                                            <h3>Enfunde</h3>
                                            <p>Enfunde</p>
                                        </a>
                                    </div>
                                    <div class="col-sm-4 margin-bottom-30 module-secundary">
                                        <a class="<?= $session->settings->produccion == 'Activo' ? 'webapp-btn' : 'webapp-btn-disabled' ?>" 
                                            style="border:none; height: 180px;" 
                                            href="<?= $session->settings->produccion == 'Activo' ? 'produccionsemana' : '#' ?>">

                                            <!--<img src="logos/resumen_general.png" style="height:70%;" alt="">-->
                                            <h3>Resumen General</h3>
                                            <p>Resumen General</p>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div id="BANANO-LANCOFRUIT-modules">
                                <div class="row">
                                    <div class="col-sm-4 margin-bottom-30 module-secundary">
                                        <a class="<?= $session->settings->lancofruit == 'Activo' ? 'webapp-btn' : 'webapp-btn-disabled' ?>" 
                                            style="border:none; height: 150px;" 
                                            href="<?= $session->settings->lancofruit == 'Activo' ? 'revisionLancofruit' : '#' ?>">
                                            <h3>Geolocalización</h3>
                                            <p>Geolocalización</p>
                                        </a>
                                    </div>
                                    <div class="col-sm-4 margin-bottom-30 module-secundary">
                                        <a class="<?= $session->settings->lancofruit == 'Activo' ? 'webapp-btn' : 'webapp-btn-disabled' ?>" 
                                            style="border:none; height: 150px;" 
                                            href="<?= $session->settings->lancofruit == 'Activo' ? 'rastreoLancofruit' : '#' ?>">

                                            <h3>Ventas</h3>
                                            <p>Ventas</p>
                                        </a>
                                    </div>
                                    <div class="col-sm-4 margin-bottom-30 module-secundary">
                                        <a class="<?= $session->settings->lancofruit == 'Activo' ? 'webapp-btn' : 'webapp-btn-disabled' ?>" 
                                            style="border:none; height: 150px;" 
                                            href="<?= $session->settings->lancofruit == 'Activo' ? 'perchasDia' : '#' ?>">

                                            <h3>Perchas</h3>
                                            <p>Perchas</p>
                                        </a>
                                    </div>
                                    <div class="col-sm-4 margin-bottom-30 module-secundary">
                                        <a class="<?= $session->settings->lancofruit == 'Activo' ? 'webapp-btn' : 'webapp-btn-disabled' ?>" 
                                            style="border:none; height: 150px;" 
                                            href="<?= $session->settings->lancofruit == 'Activo' ? 'lancofruitTemperatura' : '#' ?>">

                                            <h3>Temperatura</h3>
                                            <p>Temperatura</p>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div id="MONITOR-modules">
                                <div class="col-sm-4 sm-margin-bottom-30 module-secundary">
                                    <a class="webapp-btn" style="border:none; height: 150px;" href="monitorProntoforms">
                                        <h3>PRONTOFORMS</h3>
                                        <p>PRONTOFORMS</p>
                                    </a>
                                </div>

                                <div class="col-sm-4 sm-margin-bottom-30 module-secundary">
                                    <a class="webapp-btn" style="border:none; height: 150px;" href="monitorBalanzas">
                                        <h3>BALANZAS</h3>
                                        <p>BALANZAS</p>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END QUICK SIDEBAR TOGGLER -->
                <p class="copyright">2016 © Procesos IQ.</p>
                <!-- END FOOTER -->
            </div>
        </div>

        <!-- END QUICK SIDEBAR -->
        <!--[if lt IE 9]>
        <script src="assets/global/plugins/respond.min.js"></script>
        <script src="assets/global/plugins/excanvas.min.js"></script> 
        <![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.3.2/angular.min.js"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="assets/layouts/layout6/scripts/layout.min.js" type="text/javascript"></script>
        <script src="assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
        <!-- END THEME LAYOUT SCRIPTS -->
        <script src="start.js?1"></script>
    </body>

</html>