<?php

require './../phrapi/controllers/Access.php';
use PHPUnit\Framework\TestCase;

final class LoginTest extends TestCase
{
    public function testLogin()
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,"http://app.test/phrapi/index.php?resource=access/login");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,"user=victor&pass=victor");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $data = curl_exec($ch);
        curl_close ($ch);

        $result = json_decode($data, true);

        $this->assertEquals(true, $result);
    }
}