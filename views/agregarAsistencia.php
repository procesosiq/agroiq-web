        
<script src="assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/clockface/js/clockface.js" type="text/javascript"></script>
<script src="assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>

<div ng-controller="agregarAsistencia" ng-cloak>
     <h3 class="page-title"> 
          Agregar Asistencia
     </h3>
     <div class="page-bar" ng-init="nocache()">
         <ul class="page-breadcrumb">
             <li>
                 <i class="icon-home"></i>
                 <a href="/revisionAsistencia">Asistencia</a>
                 <i class="fa fa-angle-right"></i>
             </li>
         </ul>
        <div class="page-toolbar">
           
        </div>
    </div>
    <form id="formularioOrden"  role="form" method="post" class="form-horizontal form-row-seperated">
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    Registro Asistencia </div>
                <div class="actions btn-set">
                    <!-- <button type="button" name="back" class="btn btn-secondary-outline">
                        <i class="fa fa-angle-left"></i> Regresar</button> -->
                   <!--  <button type="button" class="btn btn-secondary-outline" ng-click="limpiar()">
                        <i class="fa fa-reply"></i> Limpiar</button> -->
                    <button type="button" class="btn blue" id="btnaddord" ng-click="saveDatos()">
                        <i class="fa fa-check"></i> Guardar</button>
                    <!-- <button type="button" class="btn blue" id="btnaddord" ng-if="Personal.id_contrato == 0 && Personal.idPersonal > 0" ng-click="saveContrato()">
                        <i class="fa fa-check"></i> Contratar</button> -->
                    <!-- <a href='print_reporte.php?id={{Personal.id_order}}' target="_blank" class="btn btn-success" ng-show="Personal.id_order>0">
                        <i class="fa fa-check"></i> Impresión</a> -->
                    <!-- <button type="button" class="btn btn-success">
                        <i class="fa fa-check-circle save"></i> Guardar & Listar</button> -->
                </div>
                <input type="hidden" value="0" ng-model="Personal.idPersonal" id="idPersonal">
            </div>
            <div class="form-body portlet-body">
                <div class="row">
                    <div class="col-md-12">
                        <fieldset class="scheduler-border">
                            <legend class="scheduler-border">Fechas</legend>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Fecha :
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-8">
                                         <input class="form-control form-control-inline input-medium date-picker" size="16" type="text" value="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Hora :
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-8">
                                        <div class="input-group">
                                            <input type="text" class="form-control timepicker timepicker-no-seconds" ng-model="data.hora" placeholder="HH:MM:ss">
                                            <span class="input-group-btn">
                                                <button class="btn default" type="button">
                                                    <i class="fa fa-clock-o"></i>
                                                </button>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
                <br><br>
                <div class="row">
                    <div class="col-md-6">
                        <fieldset class="scheduler-border">
                            <legend class="scheduler-border">Detalle</legend>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Responsable:
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-8">
                                    <input type="text" ng-model="data.responsable" class="form-control" name="responsable" id="responsable" placeholder="" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Tipo de Trabajador:
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-8">
                                    <input type="text" ng-model="data.trabajador" class="form-control" name="trabajador" id="trabajador" placeholder="" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Nombre del Trabajador Nuevo :
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-8">
                                     <input type="text" ng-model="data.nombre_trabajador_nuevo" class="form-control" name="nombre_trabajador_nuevo" id="nombre_trabajador_nuevo" placeholder="" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Cédula :
                                </label>
                                <div class="col-md-8">
                                     <input type="text" ng-model="data.cedula" class="form-control" name="cedula" id="cedula" placeholder="" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Hacienda :
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-8">
                                     <input type="text" ng-model="data.hacienda" class="form-control" name="hacienda" id="hacienda" placeholder="" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Lote :
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-8">
                                     <input type="text" ng-model="data.lote" class="form-control" name="lote" id="lote" placeholder="" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Cables :
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-8">
                                     <input type="text" ng-model="data.cables" class="form-control" name="cables" id="cables" placeholder="" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Almuerzo :
                                </label>
                                <div class="col-md-8">
                                     <input type="checkbox" ng-model="data.almuerzo" ng-checked="parseInt(data.almuerzo) > 0" class="input-sm" name="almuerzo" id="almuerzo" placeholder="" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Observaciones :
                                </label>
                                <div class="col-md-8">
                                     <input type="text" ng-model="data.observaciones" class="form-control" name="observaciones" id="observaciones" placeholder="" />
                                </div>
                            </div>
                            
                        </fieldset>
                    </div>
                    <div class="col-md-6">
                        <fieldset class="scheduler-border">
                            <legend class="scheduler-border">Labores</legend>
                			<label class="col-md-12" ng-repeat="labor in labores">
                                <input type="checkbox" id="labor_{{ labor.id }}" name="{{labor.id}}" class="labor" ng-checked="labor.selected > 0"/> {{ labor.nombre }}
                            </label>
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>