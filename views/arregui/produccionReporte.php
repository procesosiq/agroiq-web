<style>
	td, th  {
		text-align: center;
	}
</style>
<div ng-controller="controller" ng-cloak>
	<h3 class="page-title"> 
          Reporte Producción
     </h3>
     <div class="page-bar" ng-init="produccion.nocache()">
         <ul class="page-breadcrumb">
             
         </ul>
         <div class="page-toolbar">
			<label for="">Año</label>
			<select class="input-sm" ng-model="filters.year" ng-change="changeYear()">
				<option value="{{y}}" ng-repeat="y in years" ng-selected="y == filters.year">{{y}}</option>
			</select>
			<label for="">Semana</label>
			<select class="input-sm" ng-model="filters.semana" ng-change="changeWeek()">
				<option value="{{s}}" ng-repeat="s in semanas" ng-selected="s == filters.semana">{{s}}</option>
			</select>
			<label for="">Finca</label>
			<select class="input-sm" ng-model="filters.id_finca" ng-change="changeWeek()">
				<option value="">TODOS</option>
				<option value="{{key}}" ng-repeat="(key, value) in fincas" ng-selected="key == filters.id_finca">{{value}}</option>
			</select>
        </div>
    </div>
    
	<div id="contenedor" class="div2">
		<div class="row">
			<div class="col-md-12">
				<div class="portlet box green">
					<div class="portlet-title">
						<span class="caption">RESUMEN CAJAS</span>
						<div class="tools">
							
						</div>
					</div>
					<div class="portlet-body">
						<div class="table-scrollable" id="div_table_2">
							<table class="table table-striped table-bordered table-hover" id="table_2">
								<thead>
									<tr>
                                        <th></th>
                                        <th ng-show="cintaSC"></th>
										<th ng-if="edades.length > 0" colspan="{{ edades.length }}">EDAD</th>
										<th></th>
										<th></th>
										<th></th>
										<th></th>
										<th></th>
										<th></th>
										<th></th>
										<th></th>
										<th></th>
									</tr>
									<tr>
                                        <th>LOTE</th>
                                        <th ng-show="cintaSC">N/A</th>
										<th ng-repeat="e in edades">{{e}}</th>
										<th>RAC<br>COSE</th>
										<th>PESO PROM<br>RAC KG</th>
										<th>RAC<br>RECU</th>
										<th>RAC<br>PROC</th>
										<th>CONV</th>
										<th>CAJAS/ha</th>
										<th>RATIO<br>COSE</th>
										<th>RATIO<br>PROC</th>
										<th>CAJAS/ha<br>PROYEC.</th>
									</tr>
								</thead>
								<tbody>
									<tr ng-repeat="row in data">
                                        <td>{{ row.lote }}</td>
                                        <td ng-show="cintaSC">{{ row['edad_sc'] > 0 ? row['edad_sc'] : '' }}</td>
										<td ng-repeat="e in edades">{{ row['edad_'+e] }}</td>
										<td>{{ row.cosechados }}</td>
                                        <td>{{ row.peso_prom_racimo }}</td>
										<td>{{ row.recusados }}</td>
										<td>{{ row.procesados }}</td>
										<td>{{ row.convertidas }}</td>
										<td>{{ row.cajas_ha }}</td>
										<td>{{ row.ratio_cortado }}</td>
										<td>{{ row.ratio_procesado }}</td>
										<td>{{ row.cajas_ha_proyeccion | number }}</td>
									</tr>
								</tbody>
								<tfoot>
									<tr>
                                        <th></th>
                                        <th ng-show="cintaSC">{{ totales['edad_sc'] }}</th>
										<th ng-repeat="e in edades">{{ totales['edad_'+e] }}</th>
										<th>{{ totales.total_racimos_cosechados }}</th>
										<th>{{ data | avgOfValue : 'peso_prom_racimo' | number: 2 }}</th>
										<th>{{ totales.total_racimos_recusados }}</th>
										<th>{{ totales.total_racimos_procesados }}</th>
										<th>{{ totales.total_convertidas }}</th>
										<th>{{ data | avgOfValue : 'cajas_ha' | number: 2 }}</th>
										<th>{{ data | avgOfValue : 'ratio_cortado' | number: 2  }}</th>
										<th>{{ data | avgOfValue : 'ratio_procesado' | number: 2 }}</th>
										<th>{{ data | avgOfValue : 'cajas_ha_proyeccion' | number: 2 }}</th>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>

    </div>
</div>