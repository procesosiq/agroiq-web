<style>
    .label_chart {
        background-color: #fff;
        padding: 2px;
        margin-bottom: 8px;
        border-radius: 3px 3px 3px 3px;
        border: 1px solid #E6E6E6;
        display: inline-block;
        margin: 0 auto;
    }
    .legendLabel{
        padding: 3px !important;
    }
</style>
    <!-- BEGIN CONTENT BODY -->
<div ng-controller="informe_calidad" ng-cloak>
    <h3 class="page-title"> 
          Merma
    </h3>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <i class="icon-home"></i>
                <a>Merma</a>
                <i class="fa fa-angle-right"></i>
            </li>
        </ul>
        <div class="page-toolbar">
            <label ng-if="id_company != 7" class="btn red-thunderbird btn-outline btn-circle btn-sm" ng-click="convertKg()">
                {{labelLbKg}}
            </label>
            <label for="">
                Palanca : 
            </label>
            <select ng-change="cambiosPalanca()" name="palanca" id="palanca" ng-model="calidad.params.palanca" style="margin: 2px;height: 36px;">
                <option ng-repeat="(key, value) in palancas" value="{{key}}">{{value}}</option>
            </select>
            <ng-calendarapp  search="search"></ng-calendarapp>
        </div>
    </div>

    <?php include("./views/tags_merma_".$this->session->agent_user.".php");?>           

    <div id="reportes_all" ng-include="calidad.templatePath[calidad.step]" onload="last()">

    </div>
</div>

<script src="https://unpkg.com/react@15/dist/react.js"></script>
<script src="https://unpkg.com/react-dom@15/dist/react-dom.js"></script>
<script src="componentes/FilterableSortableTable.js"></script>