
<style>
    .ms-container {
        width : 100% !important;
    }
</style>
<div ng-controller="lancofruit">
	<h3 class="page-title"> 
          Configuración Lancofruit
     </h3>
     <div class="page-bar">
         <ul class="page-breadcrumb">
             <li>
                 <i class="icon-home"></i>
                 <a>Configuración Lancofruit</a>
                 <i class="fa fa-angle-right"></i>
             </li>
         </ul>
         <div class="tools">
            <div class="buttons">
                
            </div>
         </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue ">
                <div class="portlet-title">
                    <div class="caption"></div>
                    <div class="tools">
                        <a href="" class="collapse" data-original-title="" title=""> </a>
                    </div>
                </div>
                <div class="portlet-body form">
                    <div class="row">
                        <!-- CATEGORIAS -->
                        <div class="col-md-12">
                            <h4 style="margin: 10px;display: inline-block;">Categorías</h4>
                            <button class="btn green-haze pull-right" ng-click="nuevaCategoria()" style="margin: 5px;">Nueva Categoría</button>                        
                        </div>
                        <div class="col-md-12" style="margin-top: 10px">
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th width="10%">ID</th>
                                            <th>NOMBRE</th>
                                            <th width="10%">ESTATUS</th>
                                            <th width="20%">ACCIONES</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat="row in categorias">
                                            <td>{{row.id}}</td>
                                            <td>{{row.nombre}}</td>
                                            <td><small class="label {{ colorStatus[row.status] }}">{{row.status}}</small></td>
                                            <td>
                                                <a class="btn bg-blue-soft bg-font-blue-soft" title="Editar" ng-click="modificarCategoria(row)"><i class="fa fa-pencil"></i></a>
                                                <a class="btn bg-yellow-gold bg-font-yellow-gold" title="Cambiar estatus" ng-click="toggleEstatusCategoria(row)"><i class="fa fa-refresh"></i></a>
                                                <a class="btn bg-red bg-font-red" title="Borrar" ng-click="borrarCategoria(row, $index)"><i class="fa fa-trash"></i></a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>    
                            </div>
                        </div>
                        <!-- SUBCATEGORIAS -->
                        <div class="col-md-12">
                            <h4 style="margin: 10px;display: inline-block;">Subcategorías</h4>
                            <button class="btn green-haze pull-right" ng-click="nuevaSubcategoria()" style="margin: 5px;">Nueva Subcategoría</button>
                        </div>
                        <div class="col-md-12" style="margin-top: 10px">
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th width="10%">ID</th>
                                            <th>NOMBRE</th>
                                            <th>CATEGORIA</th>
                                            <th width="10%">ESTATUS</th>
                                            <th width="20%">ACCIONES</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat="row in subcategorias">
                                            <td>{{row.id}}</td>
                                            <td>{{row.nombre}}</td>
                                            <td>{{row.categoria}}</td>
                                            <td><small class="label {{ colorStatus[row.status] }}">{{row.status}}</small></td>
                                            <td>
                                                <a class="btn bg-blue-soft bg-font-blue-soft" title="Editar" ng-click="modificarSubcategoria(row)"><i class="fa fa-pencil"></i></a>
                                                <a class="btn bg-yellow-gold bg-font-yellow-gold" title="Cambiar estatus" ng-click="toggleEstatusSubcategoria(row)"><i class="fa fa-refresh"></i></a>
                                                <a class="btn bg-red bg-font-red" title="Borrar" ng-click="borrarSubcategoria(row, $index)"><i class="fa fa-trash"></i></a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>    
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="modal_categoria" class="modal fade" tabindex="-1" data-width="400">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header blue">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">CATEGORIA</h4>
                </div>
                <div class="modal-body form">
                    <form role="form" class="form-horizontal">
                        <div class="form-actions">
                            <div class="col-md-2">
                                <label class="control-label">Nombre: </label>
                            </div>
                            <div class="col-md-6" style="font-size: 24px;">
                                <input type="text" class="form-control" ng-model="newCategoria.nombre"/>
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="col-md-12">
                                <select id="select_multiple" multiple="multiple" class="multi-select form-control">
                                    <option value="{{sub.id}}" ng-repeat="sub in subcategorias_disponibles">{{ sub.nombre }}</option>
                                    <option value="{{sub.id}}" ng-repeat="sub in newCategoria.subcategorias" selected>{{ sub.nombre }}</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="row">                                
                                <button type="button" ng-click="guardarCategoria()" class="btn dark btn-outline pull-right">Guardar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div id="modal_subcategoria" class="modal fade" tabindex="-1" data-width="400">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header blue">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">SUBCATEGORIA</h4>
                </div>
                <div class="modal-body form">
                    <form role="form" class="form-horizontal">
                        <div class="form-actions">
                            <div class="col-md-2">
                                <label class="control-label">Nombre: </label>
                            </div>
                            <div class="col-md-6" style="font-size: 24px;">
                                <input type="text" class="form-control" ng-model="newSubcategoria.nombre"/>
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="col-md-2">
                                <label class="control-label">Categoría: </label>
                            </div>
                            <div class="col-md-6" style="font-size: 24px;">
                                <select class="form-control" ng-model="newSubcategoria.id_categoria">
                                    <option value="">N/A</option>
                                    <option value="{{row.id}}" ng-repeat="row in categorias">{{row.nombre}}</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="row">                                
                                <button type="button" ng-click="guardarSubcategoria()" class="btn dark btn-outline pull-right">Guardar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>