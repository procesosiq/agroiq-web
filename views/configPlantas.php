<style>
	.accordion .panel .panel-title .accordion-toggle {
		display: block !important;
    	padding: 0px 14px !important;
	}
	.margin-5 {
		margin-bottom: 5px !important;
	}
	/*table > th,td{
		text-align: center !important;
	}*/
	.center-th {
		text-align: center !important;
	}
	.left-td {
		text-align: left !important;
		padding-left: 10px !important;
	}
	table {
		cursor: pointer !important;
	}
	.alginCenter {
		text-align: center !important;
		padding-top: 20px;
	}
	.portlet.box .dataTables_wrapper .dt-buttons{
        margin-top: 0px !important; 
        padding-bottom: 5px !important;
    }
    .fixedHeader-floating{top:60px!important;}
	.form-control, .input-sm, select, .btn, .portlet {
        border-radius : 3px !important;
    }
</style>
<div ng-controller="controller">
	<h3 class="page-title"> 
    Configuración de Lotes
     </h3>
     <div class="page-bar">
         <ul class="page-breadcrumb">
             <li>
                 <i class="icon-home"></i>
                 <a>Configuración</a>
                 <i class="fa fa-angle-right"></i>
             </li>
         </ul>
         <div class="page-toolbar">
		 	Año :
			<select class="input-sm" ng-model="filters.anio" ng-change="init()">
				<option ng-repeat="a in anios" value="{{ a }}" ng-selected="filters.anio == a">{{ a }}</option>
			</select>
        </div>
	</div>

	<div class="tabbable tabbable-tabdrop">
        <ul class="nav nav-tabs">
            <li ng-repeat="f in fincas" class="{{ filters.id_finca == f.id ? 'active' : '' }}">
                <a ng-click="filters.id_finca = f.id; init()">{{f.nombre}}</a>
            </li>
        </ul>
    </div>

	<div class="portlet box green-haze">
		<div class="portlet-title">
		    <span class="caption">Configuración de Hectáreas</span>
		    <div class="tools">
                <div class="btn-group">
                    <button class="btn blue hide"> Nuevo Lote </button>
                </div>
	        </div>
		</div>
		<div class="portlet-body">
			<label for="" class="label font-red-thunderbird">Presionar <b>Enter</b> para guardar</label>
			<div class="table-container">
                <div id="table-react"></div>
			</div>
		</div>
	</div>
</div>

<script src="componentes/FilterableSortableTable.js"></script>