        

<div ng-controller="control" ng-cloak>
     <h3 class="page-title"> 
          <?= isset($_GET["id"]) ? "Editar" : "Agregar" ?> Categoría
     </h3>
     <div class="page-bar" ng-init="nocache()">
         <ul class="page-breadcrumb">
             <li>
                 <i class="icon-home"></i>
                 <a href="/configCategorias">Listado de Categorías</a>
                 <i class="fa fa-angle-right"></i>
             </li>
         </ul>
        <div class="page-toolbar">
           
        </div>
    </div>
    <form id="formularioOrden"  role="form" method="post" class="form-horizontal form-row-seperated">
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <?= isset($_GET["id"]) ? "Editar" : "Agregar" ?> Categoría </div>
                <div class="actions btn-set">
                    <button type="button" class="btn blue" ng-click="saveDatos()">
                        <i class="fa fa-check"></i> Guardar</button>
                </div>
                <input type="hidden" value="0" ng-model="Personal.idPersonal" id="idPersonal">
            </div>
            <div class="form-body portlet-body">
                <div class="row">
                    <div class="col-md-6">
                        <fieldset class="scheduler-border">
                            <legend class="scheduler-border">Información</legend>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Categoría:
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-8">
                                    <input type="text" ng-model="data.nombre" />
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>