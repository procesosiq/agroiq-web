        
<script src="assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/clockface/js/clockface.js" type="text/javascript"></script>
<script src="assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>

<div ng-controller="control" ng-cloak>
     <h3 class="page-title"> 
          <?= isset($_GET["id"]) ? "Editar" : "Agregar" ?> Marca
     </h3>
     <div class="page-bar" ng-init="nocache()">
         <ul class="page-breadcrumb">
             <li>
                 <i class="icon-home"></i>
                 <a href="/configMarcas">Listado de Marcas</a>
                 <i class="fa fa-angle-right"></i>
             </li>
         </ul>
        <div class="page-toolbar">
           
        </div>
    </div>
    <form id="formularioOrden"  role="form" method="post" class="form-horizontal form-row-seperated">
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <?= isset($_GET["id"]) ? "Editar" : "Agregar" ?> Marca </div>
                <div class="actions btn-set">
                    <button type="button" class="btn blue" ng-click="saveDatos()">
                        <i class="fa fa-check"></i> Guardar</button>
                </div>
                <input type="hidden" value="0" ng-model="Personal.idPersonal" id="idPersonal">
            </div>
            <div class="form-body portlet-body">
                <div class="row">
                    <div class="col-md-6">
                        <fieldset class="scheduler-border">
                            <legend class="scheduler-border">Información</legend>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Cliente:
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-8">
                                    <select name="cliente" class="form-control" ng-model="data.cliente" id="changeCliente">
                                        <option ng-repeat="(key, value) in clientes" value="{{key}}" ng-selected="key == data.cliente">{{ value }}</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Exportador:
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-8">
                                    <select name="exportador" class="form-control" ng-model="data.exportador">
                                        <option ng-repeat="(key, value) in exportadores" value="{{key}}" ng-selected="key == data.exportador">{{ value }}</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Marca :
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-8">
                                     <input type="text" ng-model="data.marca" class="form-control" name="marca" id="marca" placeholder="" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Tipo de Caja :
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-8">
                                     <input type="text" ng-model="data.tipo_caja" class="form-control" name="tipo_caja" id="tipo_caja" placeholder="" />
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>