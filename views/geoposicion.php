<div ng-controller="mapas" ng-cloak>
     <h3 class="page-title"> 
          Auditoría de Labores Agrícolas
          <small>Mapas</small>
     </h3>
     <div class="page-bar">
         <ul class="page-breadcrumb">
             <li>
                 <i class="icon-home"></i>
                 <a>Inicio</a>
                 <i class="fa fa-angle-right"></i>
             </li>
             <li>
                 <span><a>Mapas</a></span>
                 <i class="fa fa-angle-right"></i>
             </li>
         </ul>
     </div>       
     <div ng-init="init()">
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN BASIC PORTLET-->
                <div class="portlet light portlet-fit bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class=" icon-layers font-red"></i>
                            <span class="caption-subject font-red bold uppercase">Muestras de Clientes</span>
                        </div>
                        <div class="actions">
                        <div class="btn-group btn-group-devided" data-toggle="buttons">
                            <!-- <label class="btn red btn-outline btn-circle btn-sm active" ng-click="changeType('MES')">
                            <input type="radio" name="options" class="toggle" id="option1">Mes</label>
                            <label class="btn red btn-outline btn-circle btn-sm" ng-click="changeType('SEMANA')">
                            <input type="radio" name="options" class="toggle" id="option2">Semana</label>
                             <label ng-hide="id_company == 3" class="btn red btn-outline btn-circle btn-sm" ng-click="changeType('AUDITORIA')">
                            <input type="radio" name="options" class="toggle" id="option2">Auditoria</label> -->
                            <select name="fincas" id="fincas" ng-if="id_company == 7" onchange="changeFincas()" class="form-control" style="width: 100px; float: left;">
                                <option value="-1">Seleccione</option>
                                <option ng-repeat="(key , value) in fincas" value="{{key}}">{{value}}</option>
                            </select>
                            <select name="auditorias" id="auditorias" class="form-control" style="width: 100px;">
                                <option value="-1">Seleccione</option>
                                <option ng-repeat="(key , value) in auditorias" value="{{key}}">PERIODO {{value}}</option>
                            </select>
                        </div>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <!--<div id="geo_map" class="gmaps" style="height: 500px !important"></div>-->
                        <div id="mapa_agroaudit" class="gmaps" style="height: 500px !important"></div>
                    </div>
                </div>
                <!-- END BASIC PORTLET-->
            </div>
        </div>
     </div>  
</div>
