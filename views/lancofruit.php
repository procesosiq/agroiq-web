    <style>
        .label_chart {
            background-color: #fff;
            padding: 2px;
            margin-bottom: 8px;
            border-radius: 3px 3px 3px 3px;
            border: 1px solid #E6E6E6;
            display: inline-block;
            margin: 0 auto;
        }
        .legendLabel{
            padding: 3px !important;
        }
    </style>
      <!-- BEGIN CONTENT BODY -->
<div ng-controller="lancofruit" ng-cloak>
     <h3 class="page-title"> 
          Lancofruit
     </h3>
     <div class="page-bar" ng-init="calidad.nocache()">
         <ul class="page-breadcrumb">
             <li>
                 <i class="icon-home"></i>
                 <a href="revisionLancofruit">Listado Lancofruit</a>
                 <i class="fa fa-angle-right"></i>
             </li>
         </ul>
        <div class="page-toolbar">

        </div>
     </div>   
     <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue ">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift"></i> Formulario </div>
                    <div class="tools">
                        <a href="" class="collapse" data-original-title="" title=""> </a>
                    </div>
                </div>
                <div class="portlet-body form">
                    <div ng-include="'./views/templetes/lancofruit/editar.html?id=<?=rand(1,999)?>'" onload="load()">
                    </div>
                </div>
            </div>
        </div>     
</div>