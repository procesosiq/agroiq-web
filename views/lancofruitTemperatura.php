    <style>
        .label_chart {
            background-color: #fff;
            padding: 2px;
            margin-bottom: 8px;
            border-radius: 3px 3px 3px 3px;
            border: 1px solid #E6E6E6;
            display: inline-block;
            margin: 0 auto;
        }
        .legendLabel{
            padding: 3px !important;
        }
        td {
            text-align: center;
        }
    </style>
      <!-- BEGIN CONTENT BODY -->
<div ng-controller="temperatura" ng-cloak>
     <h3 class="page-title"> 
          TEMPERATURA
     </h3>
     <div class="page-bar" ng-init="service.all()">
         <ul class="page-breadcrumb">
             <li>
                 <i class="icon-home"></i>
                 <a >Lancofruit</a>
                 <i class="fa fa-angle-right"></i>
             </li>
         </ul>
        <div class="page-toolbar" style="display:flex">
        </div>
     </div>   
    <div class="row">
        <div class="col-md-12"> 
            <div class="portlet box blue ">
                <div class="portlet-title">
                    <div class="caption">
                        TEMP MAX
                    </div>
                </div>
                <div class="portlet-body">
                    <div id="temp_maxima" style="height:500px;"></div>
                </div>
            </div>
        </div>
        <div class="col-md-12"> 
            <div class="portlet box blue ">
                <div class="portlet-title">
                    <div class="caption">
                        TEMP MIN
                    </div>
                </div>
                <div class="portlet-body">
                    <div id="temp_minima" style="height:500px;"></div>
                </div>
            </div>
        </div>
        <div class="col-md-12"> 
            <div class="portlet box blue ">
                <div class="portlet-title">
                    <div class="caption">
                        TEMP PROM
                    </div>
                </div>
                <div class="portlet-body">
                    <div id="temp_promedio" style="height:500px;"></div>
                </div>
            </div>
        </div>
        <div class="col-md-12"> 
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        BASE DE DATOS
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-m-12 text-right" style="margin-right: 15px">
                            Variable: 
                            <select class="input-sm" ng-model="filters.variable" ng-change="reRenderTable()">
                                <option value="max">MAX</option>
                                <option value="min">MIN</option>
                                <option value="prom">PROM</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div id="table-react"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="componentes/FilterableSortableTable.js"></script>