        
<script src="assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/clockface/js/clockface.js" type="text/javascript"></script>
<script src="assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>

<div ng-controller="agregarAsistencia" ng-cloak>
     <h3 class="page-title"> 
          <?= (isset($_GET["id"]) && $_GET["id"] > 0) ? 'Editar' : 'Agregar' ?> Asistencia
     </h3>
     <div class="page-bar" ng-init="nocache()">
         <ul class="page-breadcrumb">
             <li>
                 <i class="icon-home"></i>
                 <a href="/revisionAsistencia">Asistencia</a>
                 <i class="fa fa-angle-right"></i>
             </li>
         </ul>
        <div class="page-toolbar">
           
        </div>
    </div>
    <form id="formularioOrden"  role="form" method="post" class="form-horizontal form-row-seperated">
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    Registro Asistencia </div>
                <div class="actions btn-set">
                    <!-- <button type="button" name="back" class="btn btn-secondary-outline">
                        <i class="fa fa-angle-left"></i> Regresar</button> -->
                   <!--  <button type="button" class="btn btn-secondary-outline" ng-click="limpiar()">
                        <i class="fa fa-reply"></i> Limpiar</button> -->
                    <button type="button" class="btn blue" id="btnaddord" ng-click="saveDatos()">
                        <i class="fa fa-check"></i> <?= (isset($_GET["id"]) && $_GET["id"] > 0) ? 'Editar' : 'Agregar' ?> </button>
                    <!-- <button type="button" class="btn blue" id="btnaddord" ng-if="Personal.id_contrato == 0 && Personal.idPersonal > 0" ng-click="saveContrato()">
                        <i class="fa fa-check"></i> Contratar</button> -->
                    <!-- <a href='print_reporte.php?id={{Personal.id_order}}' target="_blank" class="btn btn-success" ng-show="Personal.id_order>0">
                        <i class="fa fa-check"></i> Impresión</a> -->
                    <!-- <button type="button" class="btn btn-success">
                        <i class="fa fa-check-circle save"></i> Guardar & Listar</button> -->
                </div>
                <input type="hidden" value="0" ng-model="Personal.idPersonal" id="idPersonal">
            </div>
            <div class="form-body portlet-body">
                <div class="row">
                    <div class="col-md-12">
                        <fieldset class="scheduler-border">
                            <legend class="scheduler-border">Fechas</legend>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Fecha :
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-8">
                                         <input type="text" class="table-group-action-input form-control date-picker" data-date-format="yyyy-mm-dd" readonly name="fecha" ng-model="data.fecha" id="fecha" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Hora :
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-8">
                                        <div class="input-group">
                                            <input type="text" class="form-control timepicker timepicker-no-seconds" ng-model="data.hora" placeholder="HH:MM:ss">
                                            <span class="input-group-btn">
                                                <button class="btn default" type="button">
                                                    <i class="fa fa-clock-o"></i>
                                                </button>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
                <br><br>
                <div class="row">
                    <div class="col-md-6">
                        <fieldset class="scheduler-border">
                            <legend class="scheduler-border">Detalle</legend>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Administrador:
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-8">
                                    <input type="text" ng-model="data.administrador" class="form-control" name="administrador" id="administrador" placeholder="" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Trabajador:
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-8">
                                    <select id="trabajador" class="form-control" ng-model="data.trabajador" ng-change="changeTrabajador()">
                                        <option ng-repeat="tbj in trabajadores" value="{{ tbj.id }}" ng-selected="tbj.id == data.id_personal" data-index="{{ $index }}"> {{ tbj.nombre }} </option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Perfil :
                                </label>
                                <div class="col-md-8">
                                     <input type="text" ng-model="data.perfil" class="form-control" name="perfil" id="perfil" placeholder="" readonly/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Cédula :
                                </label>
                                <div class="col-md-8">
                                     <input type="text" ng-model="data.cedula" class="form-control" name="cedula" id="cedula" placeholder="" readonly/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Hectáreas :
                                </label>
                                <div class="col-md-8">
                                     <input type="text" ng-model="data.hectareas" class="form-control" name="hectareas" id="hectareas" placeholder="" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Hacienda :
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-8">
                                     <input type="text" ng-model="data.finca" class="form-control" name="finca" id="finca" placeholder="" readonly/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Lote :
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-8">
                                    <select id="lotes" ng-model="lotes" class="form-control js-example-basic-multiple" multiple="multiple" style="border: 1px solid gray;">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Observaciones :
                                </label>
                                <div class="col-md-8">
                                     <input type="text" ng-model="data.observaciones" class="form-control" name="observaciones" id="observaciones" placeholder="" />
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>