
<style>
    .label_chart {
        background-color: #fff;
        padding: 2px;
        margin-bottom: 8px;
        border-radius: 3px 3px 3px 3px;
        border: 1px solid #E6E6E6;
        display: inline-block;
        margin: 0 auto;
    }
    .legendLabel{
        padding: 3px !important;
    }
    .lenguage {
        padding-bottom: 9px;
        padding-top: 9px;
    }
    
    .card {
        /* Add shadows to create the "card" effect */
        box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
        transition: 0.3s;
        margin-bottom: 15px;
    }

    /* On mouse-over, add a deeper shadow */
    .card:hover {
        box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
    }

    /* Add some padding inside the card container */
    .container {
        padding: 2px 16px;
}
</style>
      <!-- BEGIN CONTENT BODY -->
<?php
    if(Session::getInstance()->type_users == 'ADMIN' || ((Session::getInstance()->type_users != 'ADMIN' && Session::getInstance()->agent_user == 'marcel') && (isset($_GET['c']) && isset($_GET['m'])))):
?>
<div ng-controller="informe_calidad" id="informe_calidad" ng-cloak>
    <div style="width: 100%;margin-bottom: 10px;display:table;">
         <h3 class="page-title" style="display: table-cell !important;"> 
              Inspección de Calidad
         </h3>
        
    </div>
     <div class="page-bar" ng-init="calidad.nocache()">
         <ul class="page-breadcrumb">
             <li>
                 <i class="icon-home"></i>
                 <a>Calidad</a>
                 <i class="fa fa-angle-right"></i>
             </li>
         </ul>
         <div class="page-toolbar">
            <div class="inline-block" ng-show="<?= Session::getInstance()->id_company ?> == 6">
                <span>Contenedor</span>
                <select class="input-sm" ng-model="calidad.params.contenedor" ng-change="changeContenedor()">
                    <option value="">TODOS</option>
                    <option ng-repeat="cont in contenedores" value="{{ cont.id }}">{{ cont.label }}</option>
                </select>
            </div>
            <a href="quality?c=<?=$_GET['c']?>&m=<?=$_GET['m']?>" ng-hide="<?= Session::getInstance()->id_company ?> == 6" class="btn btn-primary lenguage">EN</a>
            <a href="qualitat?c=<?=$_GET['c']?>&m=<?=$_GET['m']?>" ng-hide="<?= Session::getInstance()->id_company ?> == 6" class="btn btn-primary lenguage">Puerto</a>
            <ng-calendarapp  search="search"></ng-calendarapp>
         </div>
     </div>

    <div class="page-bar" style="padding: 0; color: #888" ng-if="checkParams()">

            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-2" style="text-align: left">CLIENTE:</div>
                    <div class="col-md-4">{{param_cliente}}</div>
                    <div class="col-md-4" style="text-align: left">PESO REFERENCIAL:</div>
                    <div class="col-md-2">{{param_peso}}</div>
                </div>
                <div class="row" style="margin-top: 12px;">
                    <div class="col-md-2" style="text-align: left">MARCA:</div>
                    <div class="col-md-4">{{param_marca}}</div>
                    <div class="col-md-2" style="text-align: left"> BUQUE:</div>
                    <div class="col-md-4">{{barco.nombre}}</div>
                </div>
            </div>
            <div class="col-md-2"><img ng-src="{{param_logo}}" height="50" /></div>
            <div class="col-md-2" style="display:none;">
					<select name="marcar" class="form-control">
					  <option ng-repeat="option in param_marcas" value="{{option.marca}}">{{option.marca}}</option>
					</select>
			</div>
    </div>

     <?php include("./views/marcel/tags_calidad.php");?>           
     <div id="reportes_all" ng-include="calidad.templatePath[calidad.step]"></div>

     <div class="portlet box green" ng-if="checkParams()">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-gift"></i> Fotos </div>
            <div class="tools">
            </div>
        </div>
        <div class="portlet-body">
            <div class="panel-group accordion" id="accordion3">
                <div class="panel panel-default" ng-repeat="(semana, row) in fotos | orderObjectBy : 'semana'">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_{{row.semana}}_{{$index}}" aria-expanded="false"> 
                                SEMANA {{ row.semana }} 
                                <span class="pull-right" style="margin-right : 10px;"><b>{{row.filtrado.length}} Fotos<b></span>
                            </a>
                        </h4>
                    </div>
                    <div id="collapse_3_{{row.semana}}_{{$index}}" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                        <div class="panel-body">
                            <div class="col-md-3" ng-repeat="calidad in row.filtrado = (row.data | filter : { marca: filters.marca })">
                                <div class="card">
                                    <img src="{{ calidad.url }}" alt="Imagen no disponible" style="width:100%; height: 170px;">
                                    <div class="container">
                                        <h4><b>Calidad : {{ calidad.calidad_cluster }} %</b></h4> 
                                        <p>{{ calidad.fecha }}</p> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
    endif;
?>