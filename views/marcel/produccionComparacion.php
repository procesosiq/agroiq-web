<style>
	table > th,td{
		text-align: center !important;
	}
	.center-th {
		text-align: center !important;
	}
	.left-td {
		text-align: left !important;
		padding-left: 10px !important;
	}
    .right-td {
        text-align: right !important;
		padding-right: 10px !important;
    }
	.alginLeft {
		text-align: left !important;
		padding-top: 20px;
	}
    .resizable {
        resize: both;
        overflow: auto;
    }
</style>
<div ng-controller="produccion" ng-cloak>
	<h3 class="page-title"> 
        Benchmark
     </h3>
     <div class="page-bar" ng-init="init()">
         <ul class="page-breadcrumb">
             <li>
                 <i class="icon-home"></i>
                 <a>Benchmark</a>
                 <i class="fa fa-angle-right"></i>
             </li>
         </ul>
         <div class="page-toolbar">
            <label style="margin-top: 7px; margin-right: 10px;">
                Año : 2018
            </label>
        </div>
    </div>
	<div id="contenedor" class="div2">
		<div class="row" id="graficas">
			<div class="col-md-12">
				<div class="portlet box green-haze">
					<div class="portlet-title">
					    <span class="caption">Benchmark</span>
					    <div class="tools">
                            <div class="col-md-12">
                                <!--<a href="javascript:;" class="btn btn-default {{ filters.type1 == 'line' ? 'pressed' : '' }}" ng-click="toggleLineBar(1, 'line')"><i class="fa fa-line-chart"></i></a>
                                <a href="javascript:;" class="btn btn-default {{ filters.type1 == 'bar' ? 'pressed' : '' }}" ng-click="toggleLineBar(1, 'bar')"><i class="fa fa-bar-chart"></i></a>-->
                                <div class="btn-group bootstrap-select bs-select">
                                    <button type="button" class="btn dropdown-toggle btn-default" data-toggle="dropdown" role="button" title="Mustard" aria-expanded="false">
                                        <span class="filter-option pull-left">{{ filters.var1 | uppercase }}</span>&nbsp;
                                        <span class="bs-caret"><span class="caret"></span></span>
                                    </button>
                                    <div class="dropdown-menu open" role="combobox" style="max-height: 357px; overflow: hidden; min-height: 105px;">
                                        <ul class="dropdown-menu inner" role="listbox" aria-expanded="false" style="max-height: 355px; overflow-y: auto; min-height: 103px;">
                                            <li class="dropdown-header" data-optgroup="{{ $index+1 }}" ng-repeat-start="(key, values) in variables"><span class="text"><b>{{ key | uppercase }}</b></span></li>
                                            <li data-original-index="0" data-optgroup="1" class="{{ selected(val) }}" ng-repeat-end="" ng-repeat="val in values" ng-if="val != filters.var2" ng-click="filters.var1 = val; initGraficaVariables();">
                                                <a tabindex="0" class="opt  " style="" data-tokens="null" role="option" aria-disabled="false" aria-selected="true">
                                                    <span class="text">{{ val }}</span>
                                                    <span class="fa fa-check check-mark"></span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <select class="bs-select" tabindex="-98" ng-model="filters.var1">
                                        <optgroup ng-repeat="(key, values) in variables" label="{{ key | uppercase }}">
                                            <option ng-repeat="val in values" ng-if="val != filters.var2">{{val}}</option>
                                        </optgroup>
                                    </select>
                                </div>
                            </div>
						</div>
					</div>
					<div class="portlet-body">
						<div class="row">
							<div class="col-md-12">
								<div id="variables" style="height:500px;"></div>
							</div>
						</div>
					</div>
				</div>
			</div>

            <div class="col-md-12">
                <div class="portlet box green-haze">
                    <div class="portlet-title">
					    <span class="caption">Correlación</span>
					    <div class="tools">
                            <div class="col-md-6">
                                <a href="javascript:;" class="btn btn-default {{ filtersCorrelacion.type1 == 'line' ? 'pressed' : '' }}" ng-click="toggleLineBar(1, 'line')"><i class="fa fa-line-chart"></i></a>
                                <a href="javascript:;" class="btn btn-default {{ filtersCorrelacion.type1 == 'bar' ? 'pressed' : '' }}" ng-click="toggleLineBar(1, 'bar')"><i class="fa fa-bar-chart"></i></a>
                                <div class="btn-group bootstrap-select bs-select">
                                    <button type="button" class="btn dropdown-toggle btn-default" data-toggle="dropdown" role="button" title="Mustard" aria-expanded="false">
                                        <span class="filter-option pull-left">{{ filtersCorrelacion.var1 | uppercase }}</span>&nbsp;
                                        <span class="bs-caret"><span class="caret"></span></span>
                                    </button>
                                    <div class="dropdown-menu open" role="combobox" style="max-height: 357px; overflow: hidden; min-height: 105px;">
                                        <ul class="dropdown-menu inner" role="listbox" aria-expanded="false" style="max-height: 355px; overflow-y: auto; min-height: 103px;">
                                            <li class="dropdown-header" data-optgroup="{{ $index+1 }}" ng-repeat-start="(key, values) in variables_correlacion"><span class="text"><b>{{ key | uppercase }}</b></span></li>
                                            <li data-original-index="0" data-optgroup="1" class="{{ selected(val) }}" ng-repeat-end="" ng-repeat="val in values" ng-if="val != filtersCorrelacion.var2" ng-click="filtersCorrelacion.var1 = val; initCorrelacion();">
                                                <a tabindex="0" class="opt  " style="" data-tokens="null" role="option" aria-disabled="false" aria-selected="true">
                                                    <span class="text">{{ val }}</span>
                                                    <span class="fa fa-check check-mark"></span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <select class="bs-select" tabindex="-98" ng-model="filtersCorrelacion.var1">
                                        <optgroup ng-repeat="(key, values) in variables_correlacion" label="{{ key | uppercase }}">
                                            <option ng-repeat="val in values" ng-if="val != filtersCorrelacion.var2">{{val}}</option>
                                        </optgroup>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <a href="javascript:;" class="btn btn-default {{ filtersCorrelacion.type2 == 'line' ? 'pressed' : '' }}" ng-click="toggleLineBar(2, 'line')"><i class="fa fa-line-chart"></i></a>
                                <a href="javascript:;" class="btn btn-default {{ filtersCorrelacion.type2 == 'bar' ? 'pressed' : '' }}" ng-click="toggleLineBar(2, 'bar')"><i class="fa fa-bar-chart"></i></a>

                                <div class="btn-group bootstrap-select bs-select">
                                    <button type="button" class="btn dropdown-toggle btn-default" data-toggle="dropdown" role="button" title="Mustard" aria-expanded="false">
                                        <span class="filter-option pull-left">{{ filtersCorrelacion.var2 | uppercase }}</span>&nbsp;
                                        <span class="bs-caret"><span class="caret"></span></span>
                                    </button>
                                    <div class="dropdown-menu open" role="combobox" style="max-height: 357px; overflow: hidden; min-height: 105px;">
                                        <ul class="dropdown-menu inner" role="listbox" aria-expanded="false" style="max-height: 355px; overflow-y: auto; min-height: 103px;">
                                            <li class="dropdown-header" data-optgroup="{{ $index+1 }}" ng-repeat-start="(key, values) in variables_correlacion"><span class="text"><b>{{ key | uppercase }}</b></span></li>
                                            <li data-original-index="0" data-optgroup="1" class="{{ selected(val) }}" ng-repeat-end="" ng-repeat="val in values" ng-if="val != filtersCorrelacion.var1" ng-click="filtersCorrelacion.var2 = val; initCorrelacion();">
                                                <a tabindex="0" class="opt  " style="" data-tokens="null" role="option" aria-disabled="false" aria-selected="true">
                                                    <span class="text">{{ val }}</span>
                                                    <span class="fa fa-check check-mark"></span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <select class="bs-select" tabindex="-98" ng-model="filtersCorrelacion.var2">
                                        <optgroup ng-repeat="(key, values) in variables_correlacion" label="{{ key | uppercase }}">
                                            <option ng-repeat="val in values" ng-if="val != filtersCorrelacion.var1">{{val}}</option>
                                        </optgroup>
                                    </select>
                                </div>
                            </div>
						</div>
					</div>
                    <div class="portlet-body" style="display:none"></div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="portlet box green-haze" id="portlet-laniado">
                    <div class="portlet-title">
                        <span class="caption">LANIADO</span>
                        <div class="tools">
                        </div>
                    </div>
					<div class="portlet-body">
						<div class="row">
							<div class="col-md-12">
								<div id="laniado" style="min-height:300px; height: 100%"></div>
							</div>
						</div>
					</div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="portlet box green-haze" id="portlet-quintana">
                    <div class="portlet-title">
					    <span class="caption">QUINTANA</span>
					    <div class="tools">
						</div>
					</div>
					<div class="portlet-body">
						<div class="row">
							<div class="col-md-12">
								<div id="quintana" style="height:400px;"></div>
							</div>
						</div>
					</div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="portlet box green-haze" id="portlet-marun">
                    <div class="portlet-title">
					    <span class="caption">MARUN</span>
					    <div class="tools">
						</div>
					</div>
					<div class="portlet-body">
						<div class="row">
							<div class="col-md-12">
								<div id="marun" style="height:400px;"></div>
							</div>
						</div>
					</div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="portlet box green-haze" id="portlet-reiset">
                    <div class="portlet-title">
					    <span class="caption">REISET</span>
					    <div class="tools">
						</div>
					</div>
					<div class="portlet-body">
						<div class="row">
							<div class="col-md-12">
								<div id="reiset" style="height:400px;"></div>
							</div>
						</div>
					</div>
                </div>
            </div>
		</div>
	</div>
</div>

<script src="componentes/FilterableSortableTable.js"></script>