<style>
	td, th {
        text-align : center;
    }
    .black {
        color : black;
    }
    .no-margin {
        margin: 0px;
    }
    .form-control {
        border-radius : 3px !important;
    }
    .btn {
        border-radius : 3px !important;
    }
    .portlet {
        border-radius : 3px !important;
    }
</style>
<div ng-controller="produccion" ng-cloak>
	<h3 class="page-title"> 
          Producción. Precalibración
     </h3>
     <div class="page-bar">
         <ul class="page-breadcrumb">
             <li>
                 <i class="icon-home"></i>
                 <a>Precalibración</a>
                 <i class="fa fa-angle-right"></i>
             </li>
         </ul>
         <div class="page-toolbar" style="display: flex;">
            <label for="semana" style="margin: auto;">
                Sem : 
            </label>
            <div class="btn-group pull-right">
                <button type="button" class="btn btn-fit-height dropdown-toggle {{colorClass}}" data-toggle="dropdown" data-hover="dropdown" data-delay="2000" data-close-others="true" aria-expanded="false"> 
                    {{ filters.year }} - {{ filters.week }}
                    <i class="fa fa-angle-down"></i>
                </button>
                <ul class="dropdown-menu pull-right" role="menu" style="max-height: 300px; overflow-y: scroll;">
                    <li ng-repeat="sem in semanas" class="{{sem.class}}" ng-click="filters.week = sem.semana; filters.year = sem.anio; init()">
                        <a href="javascript:;" class="{{sem.class}}">
                            {{ sem.anio }} -  {{ sem.semana }}
                        </a>
                    </li>
                </ul>
            </div>
         </div>
    </div>
	<div id="contenedor" class="div2">
		<div class="row">
            <div class="col-md-12">
				<div class="portlet box green-haze">
					<div class="portlet-title">
						<span class="caption">ENFUNDE - PRECALIBRACION</span>
						<div class="tools">
						</div>
					</div>
					<div class="portlet-body">
                        <div class="table-responsive" id="precalibracion">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th rowspan="2">LOTE</th>
                                        <th rowspan="2">RAC<br>ENF</th>
                                        <th ng-if="edades.length > 0" class="text-center sm" colspan="{{edades.length}}">RACIMOS COSECHADOS POR EDAD (SEM)</th>
                                        <th rowspan="2">TOTAL<br>PREC</th>
                                        <th rowspan="2">SALDO<br>PREC</th>
                                        <th rowspan="2">%</th>
                                    </tr>
                                    <tr>
                                        <th ng-repeat="e in edades | orderObjectBy : 'edad'" class="sm">{{ e.edad }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="row in precalibracion">
                                        <td>{{row.lote}}</td>
                                        <td class="{{ colorClass }}">
                                            {{ row.racimos_enfunde > 0 ? (row.racimos_enfunde | number: 0) : '' }}
                                        </td>
                                        <td ng-if="edades.length > 0" class="sm" ng-repeat="e in edades | orderObjectBy : 'edad'">
                                            {{ row['edad_'+e.edad] > 0 ? row['edad_'+e.edad] : '' }}
                                        </td>
                                        <td>{{row.cosechados}}</td>
                                        <td>{{row.saldo = (row.racimos_enfunde - row.cosechados)}}</td>
                                        <td class="{{ getUmbral(row.enfunde) }}">{{ row.enfunde }}%</td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>TOTAL</th>
                                        <th class="{{ colorClass }}">{{ totales.racimos_enfunde | number }}</th>
                                        <th ng-if="edades.length > 0" class="sm" ng-repeat="e in edades | orderObjectBy : 'edad'">
                                            {{ (precalibracion | sumOfValue : 'edad_'+e.edad) > 0 ? (precalibracion | sumOfValue : 'edad_'+e.edad | number) : '' }}
                                        </th>
                                        <th>{{ totales.cosechados = (precalibracion | sumOfValue : 'cosechados') | number }}</th>
                                        <th>{{ totales.saldo = (precalibracion | sumOfValue : 'saldo') | number }}</th>
                                        <th class="{{ getUmbral(totales.recobro) }}">{{ totales.recobro | number : 2 }}%</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
					</div>
				</div>
            </div>
		</div>
	</div>
</div>