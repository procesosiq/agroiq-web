
<!-- END PAGE HEADER-->
<div class="row">
    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <div class="dashboard-stat2 ">
            <div class="display">
                <div class="number">
                    <h3 class="font-green-jungle">
                        <span class="counter_tags" data-value="">{{ tags.acumulado  | number: 0 }}</span>
                        <small class="font-green-jungle"></small>
                    </h3>
                    <small>ACUMULADO ENFUNDE ({{filters.year}})</small>
                </div>
                <div class="icon">
                    <i class="icon-pie-chart"></i>
                </div>
            </div>
            <div class="progress-info">
                <div class="progress">
                    <span style="width: 0%;" class="progress-bar progress-bar-succes">
                        <span class="sr-only"></span>
                    </span>
                </div>
                <div class="status">
                    <div class="status-title"></div>
                    <div class="status-number"> </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <div class="dashboard-stat2 ">
            <div class="display">
                <div class="number">
                    <h3 class="font-green-jungle">
                        <span class="counter_tags" data-value="">{{tags.enfunde_sem}}</span>
                        <small class="font-green-jungle"></small>
                    </h3>
                    <small>PROM ENFUNDE/SEM</small>
                </div>
                <div class="icon">
                    <i class="icon-pie-chart"></i>
                </div>
            </div>
            <div class="progress-info">
                <div class="progress">
                    <span style="width: 0%;" class="progress-bar progress-bar-succes">
                        <span class="sr-only"></span>
                    </span>
                </div>
                <div class="status">
                    <div class="status-title"></div>
                    <div class="status-number"> </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <div class="dashboard-stat2 ">
            <div class="display">
                <div class="number">
                    <h3 class="font-green-jungle">
                        <span class="counter_tags" data-value="">{{tags.enfunde_ha}}</span>
                        <small class="font-green-jungle"></small>
                    </h3>
                    <small>PROM ENFUNDE HA/SEM</small>
                </div>
                <div class="icon">
                    <i class="icon-pie-chart"></i>
                </div>
            </div>
            <div class="progress-info">
                <div class="progress">
                    <span style="width: 0%;" class="progress-bar progress-bar-succes">
                        <span class="sr-only"></span>
                    </span>
                </div>
                <div class="status">
                    <div class="status-title"></div>
                    <div class="status-number"> </div>
                </div>
            </div>
        </div>
    </div>
</div>