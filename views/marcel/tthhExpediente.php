    <style>
        .label_chart {
            background-color: #fff;
            padding: 2px;
            margin-bottom: 8px;
            border-radius: 3px 3px 3px 3px;
            border: 1px solid #E6E6E6;
            display: inline-block;
            margin: 0 auto;
        }
        .legendLabel{
            padding: 3px !important;
        }
        .nobmp-reporte-accordion{
            border: 0 !important;
            margin: 0 !important;
            padding: 0 !important;
        }
        .green {
            bg-green-haze 
            bg-font-green-haze
        }
    </style>
      <!-- BEGIN CONTENT BODY -->
<div ng-controller="informe_tthh" ng-cloak>
     <h3 class="page-title"> 
          EXPEDIENTE LABORAL
     </h3>
     <div class="page-bar" ng-init="calidad.nocache()">
         <ul class="page-breadcrumb">
             <li>
                 <i class="icon-home"></i>
                 <a>EXPEDIENTE LABORAL</a>
                 <i class="fa fa-angle-right"></i>
             </li>
         </ul>
        <div class="page-toolbar">
            <!--<div class="col-md-6">
                <label for="trabajador">TRABAJADOR</label>
                <select class="input-sm" id="trabajador" name="trabajador" ng-model="filters.trabajador" ng-change="changeTrabajador()">
                    <option ng-repeat="(key, value) in trabajadores" value="{{ key }}"> {{ value }} </option>
                </select>
            </div>-->
        </div>
     </div>
     <div id="reportes_all">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-settings"></i>RESUMEN EXPEDIENTE</div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse" data-original-title="Expandir/Contraer" title=""> </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="col-md-12" style="padding-bottom: 5px;">
                            <div class="col-md-6">
                                <div class="btn-group">
                                    <!--<button id="sample_editable_1_new" class="btn green"> Agregar Personal
                                        <i class="fa fa-plus"></i>
                                    </button>-->
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="btn-group pull-right">
                                    <button class="btn btn-primary dropdown-toggle" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false">
                                        Exportar
                                    </button>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="javascript:;" ng-click="exportExcel('sample_editable_1', 'Resumen Expediente')">Excel</a></li>
                                        <li><a href="javascript:;">PDF</a></li>
                                        <li><a href="javascript:;" ng-click="exportPrint('sample_editable_1')">Imprimir</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="table-scrollable">
                        <table class="table table-striped table-hover table-bordered" id="sample_editable_1">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>PERSONAL</th>
                                        <th style="text-align: center;" ng-repeat="doc in documentos | orderObjectBy : 'id'">{{ doc. id }}</th>
                                        <th>%</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="(nombre, key) in personal" >
                                        <td>{{ key }}</td>
                                        <td>{{ nombre }}</td>
                                        <th class="{{ data[key][doc.id-1].status == 'SI' 
                                                        ? 'bg-green-haze bg-font-green-haze'
                                                        : data[key][doc.id-1].status == 'NO'
                                                            ? 'bg-red-thunderbird bg-font-red-thunderbird'
                                                            : '' }}"
                                            style="text-align: center;" ng-repeat="doc in documentos | orderObjectBy : 'id'">
                                            {{ data[key][doc.id-1].status }}
                                        </th>
                                        <th class="{{ data[key].total == 0
                                                        ? ''
                                                        : (data[key].total < 80
                                                            ? 'bg-red-thunderbird bg-font-red-thunderbird'
                                                            : 'bg-green-haze bg-font-green-haze') }}">
                                            {{ (data[key].total > 0) ? data[key].total : '' }}
                                        </th>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <th class="{{ data.totales[doc.id] == 0
                                                        ? ''
                                                        : (data.totales[doc.id] < 80
                                                            ? 'bg-red-thunderbird bg-font-red-thunderbird'
                                                            : 'bg-green-haze bg-font-green-haze') }}" ng-repeat="doc in documentos">
                                            {{ (data.totales[doc.id] > 0) ? (data.totales[doc.id] | number: 1) : '' }}
                                        </th>
                                        <th class="{{ data.totales.total == 0
                                                        ? ''
                                                        : (data.totales.total < 80
                                                            ? 'bg-red-thunderbird bg-font-red-thunderbird'
                                                            : 'bg-green-haze bg-font-green-haze') }}">
                                            {{ data.totales.total | number: 1 }}
                                        </th>
                                    </tr>
                                </tfoot>
                            </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
     </div>  
</div>