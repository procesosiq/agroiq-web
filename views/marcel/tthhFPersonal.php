<div ng-controller="personal_tthh" ng-cloak>
     <h3 class="page-title"> 
          Personal
     </h3>
     <div class="page-bar" ng-init="nocache()">
         <ul class="page-breadcrumb">
             <li>
                 <i class="icon-home"></i>
                 <a href="/tthhPersonal">Personal</a>
                 <i class="fa fa-angle-right"></i>
             </li>
         </ul>
        <div class="page-toolbar">
           
        </div>
    </div>

    <form id="formularioOrden"  role="form" method="post" class="form-horizontal form-row-seperated">
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-shopping-cart"></i>Registro / Edicion Personal </div>
                <div class="actions btn-set">
                    <!-- <button type="button" name="back" class="btn btn-secondary-outline">
                        <i class="fa fa-angle-left"></i> Regresar</button> -->
                   <!--  <button type="button" class="btn btn-secondary-outline" ng-click="limpiar()">
                        <i class="fa fa-reply"></i> Limpiar</button> -->
                    <button type="button" class="btn blue" id="btnaddord" ng-click="saveDatos()">
                        <i class="fa fa-check"></i> Guardar</button>
                    <!-- <button type="button" class="btn blue" id="btnaddord" ng-if="Personal.id_contrato == 0 && Personal.idPersonal > 0" ng-click="saveContrato()">
                        <i class="fa fa-check"></i> Contratar</button> -->
                    <!-- <a href='print_reporte.php?id={{Personal.id_order}}' target="_blank" class="btn btn-success" ng-show="Personal.id_order>0">
                        <i class="fa fa-check"></i> Impresión</a> -->
                    <!-- <button type="button" class="btn btn-success">
                        <i class="fa fa-check-circle save"></i> Guardar & Listar</button> -->
                </div>
                <input type="hidden" value="0" ng-model="Personal.idPersonal" id="idPersonal">
            </div>
            <div class="form-body portlet-body">
                <div class="row">
                    <div class="col-md-12">
                        <fieldset class="scheduler-border">
                            <legend class="scheduler-border">Fechas</legend>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Ingreso :
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-8">
                                         <input type="text" class="table-group-action-input form-control date-picker" data-date-format="yyyy-mm-dd" readonly name="fecha_ingreso" ng-model="Personal.fecha_ingreso" id="fecha_ingreso" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Contrato :
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-8">
                                         <input type="text" class="table-group-action-input form-control date-picker" data-date-format="yyyy-mm-dd" readonly name="fecha_contrato" ng-model="Personal.fecha_contrato" id="fecha_contrato" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Salida :
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-8">
                                         <input type="text" class="table-group-action-input form-control date-picker" data-date-format="yyyy-mm-dd" readonly name="fecha_salida" ng-model="Personal.fecha_salida" id="fecha_salida" />
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
                <br><br>
                <div class="row">
                    <div class="col-md-6">
                        <fieldset class="scheduler-border">
                            <legend class="scheduler-border">Información del Personal</legend>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Nombre Completo:
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-8">
                                    <input type="text" ng-model="Personal.nombre" class="form-control" name="nombre" id="nombre" placeholder="" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Cedula:
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-8">
                                    <input type="text" ng-model="Personal.cedula" class="form-control" name="cedula" id="cedula" placeholder="" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Fecha Nacimiento :
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-8">
                                     <input type="text" class="table-group-action-input form-control date-picker" data-date-format="yyyy-mm-dd" readonly name="nacimiento" ng-model="Personal.nacimiento" id="nacimiento" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Sexo :
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-8">
                                     <select class="table-group-action-input form-control input-medium" name="sexo" ng-model="Personal.sexo" id="sexo">
                                        <option value="Masculino">Masculino</option>
                                        <option value="Femenino">Femenino</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Estado Civil :
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-8">
                                     <select class="table-group-action-input form-control input-medium" name="civil" ng-model="Personal.civil" id="civil">
                                        <option value="Casado">Casado</option>
                                        <option value="Unión libre">Unión libre</option>
                                        <option value="Soltero">Soltero</option>
                                        <option value="Divorciado">Divorciado</option>
                                        <option value="Viudo">Viudo</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Cargas Familiares :
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" ng-model="Personal.familiares" name="familiares" id="familiares" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Tarjeta Bancaria :
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" ng-model="Personal.cuenta_banco" name="cuenta_banco" id="cuenta_banco" />
                                </div>
                            </div>
                            <br><br>
                            <fieldset class="scheduler-border">
                                <legend class="scheduler-border">Datos Domicilio</legend>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Provincia :
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" ng-model="Personal.provincia" name="provincia" id="provincia" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Canton :
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" ng-model="Personal.canton" name="canton" id="canton" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Sector :
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" ng-model="Personal.sector" name="sector" id="sector" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Dirección :
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" ng-model="Personal.direccion" name="direccion" id="direccion" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Telefono :
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" ng-model="Personal.telefono" name="telefono" id="telefono" />
                                    </div>
                                </div>
                            </fieldset>
                        </fieldset>
                    </div>
                    <div class="col-md-6">
                        <fieldset class="scheduler-border">
                            <legend class="scheduler-border">Información Laboral</legend>
                			<div class="form-group" ng-if="showFinca > 0">
                                <label class="col-md-4 control-label">Finca :
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-8">
                                    <select class="table-group-action-input form-control input-medium" name="idFinca" ng-model="Personal.idFinca" id="idFinca">
                                        <option ng-repeat="(key, value) in fincas" ng-selected="key == Personal.idFinca" value="{{key}}">{{value}}</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Codigo:
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-8">
                                    <input type="text" ng-model="Personal.codigo" class="form-control" name="codigo" id="codigo" placeholder="" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Perfil :
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-8">
                                    <select class="table-group-action-input form-control input-medium" name="perfil" ng-model="Personal.perfil" id="perfil">
                                        <option ng-repeat="value in perfiles" value="{{ value }}" ng-selected="Personal.perfil == value"> {{ value }} </option>
                                        <!--<option value="OPERADOR AGRICOLA">OPERADOR AGRICOLA</option>
                                        <option value="ADMINISTRADOR">ADMINISTRADOR</option>
                                        <option value="SUPERVISOR DE CAMPO">SUPERVISOR DE CAMPO</option>-->
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Cargo :
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-8">
                                    <select class="table-group-action-input form-control input-medium" name="cargo" ng-model="Personal.cargo" id="cargo">
                                        <option ng-repeat="value in cargos" value="{{ value }}" ng-selected="Personal.cargo == value"> {{ value }} </option>
                                        <!--<option value="OPERADOR AGRICOLA">OPERADOR AGRICOLA</option>
                                        <option value="ADMINISTRADOR">ADMINISTRADOR</option>
                                        <option value="SUPERVISOR DE CAMPO">SUPERVISOR DE CAMPO</option>-->
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Clase :
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-8">
                                     <select class="table-group-action-input form-control input-medium" name="clase" ng-model="Personal.clase" id="clase">
                                        <option ng-repeat="value in clase" value="{{ value }}" ng-selected="Personal.clase == value">{{value}}</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Discapacitado :
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-8">
                                     <select class="table-group-action-input form-control input-medium" name="discapacitado" ng-model="Personal.discapacitado" id="discapacitado">
                                        <option value="Si">Si</option>
                                        <option value="No">No</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Afiliado :
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-8">
                                     <select class="table-group-action-input form-control input-medium" name="afiliado" ng-model="Personal.afiliado" id="afiliado">
                                        <option value="Afiliado">Afiliado</option>
                                        <option value="No Afiliado">No Afiliado</option>
                                        <option value="Eventual">Eventual</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Beneficios :
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-8">
                                     <select class="table-group-action-input form-control input-medium" name="beneficios" ng-model="Personal.beneficios" id="beneficios">
                                        <option value="Acomula">Acomula</option>
                                        <option value="Mensual">Mensual</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Horas :
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-8">
                                     <select class="table-group-action-input form-control input-medium" name="horas" ng-model="Personal.horas" id="horas">
                                        <option value="6">6</option>
                                        <option value="8">8</option>
                                    </select>
                                </div>
                            </div>
                            <br>
                            <fieldset class="scheduler-border">
                                <legend class="scheduler-border">Salario</legend>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="col-md-6 control-label">Sueldo :
                                            </label>
                                            <div class="col-md-6">
                                                 <input type="text" ng-model="Personal.sueldo" class="form-control" name="sueldo" id="sueldo" placeholder="" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="col-md-5 control-label">Fecha :
                                            </label>
                                            <div class="col-md-7">
                                                 <input type="text" class="table-group-action-input form-control date-picker" data-date-format="yyyy-mm-dd" readonly name="fecha_sueldo" ng-model="Personal.fecha_sueldo" id="fecha_sueldo" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <button ng-click="saveSal()" class="btn blue"><i class="fa fa-plus-square-o"></i></button>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="table-scrollable table-scrollable-borderless"></div>
                                            <table class="table table-hover table-light">
                                                <thead>
                                                    <tr class="uppercase">
                                                        <th> Sueldo </th>
                                                        <th> Fecha </th>
                                                    </tr>
                                                    <tr ng-repeat="sueldo in Personal.sueldos">
                                                        <td>
                                                            {{sueldo.salario | number : 2}}
                                                        </td>
                                                        <td>
                                                            {{sueldo.fecha}}
                                                        </td>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        </fieldset>
                    </div>
                </div>
                <div class="row">
                    <iframe id="iframe" style="display:none;"></iframe>
                    <div class="col-md-12 table-scrollable" ng-show="<?= isset($_GET['idPersonal']) ? $_GET['idPersonal'] : 0 ?> > 0">
                        <table class="table table-striped table-hover table-bordered" id="expediente_table" style="background-color: white;">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Descripción</th>
                                    <th>Estatus</th>
                                    <th>Doc</th>
                                    <th>Fecha act</th>
                                    <th>Observaciones</th>
                                    <th>Acción</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="doc in expediente">
                                    <td>{{ doc.id }}</td>
                                    <td>{{ doc.descripcion }}</td>
                                    <td>{{ doc.status }}</td>
                                    <td>
                                        <a ng-show="doc.doc != ''" href="http://app.procesos-iq.com/expediente_documentos/marcel/expediente_d={{doc.id}}_p={{doc.id_personal}}.{{doc.ext}}" class="download-file" download><i class="fa fa-file-pdf-o"></i></a>
                                    </td>
                                    <td>{{ doc.fecha_actual }}</td>
                                    <td>{{ doc.observaciones }}</td>
                                    <td>
                                        <a href="tthhFPersonalExpediente?idPersonal=<?= $_GET['idPersonal'] ?>&doc={{doc.id}}" class="btn btn-primary">Editar</a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>