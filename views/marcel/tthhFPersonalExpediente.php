<div ng-controller="personal_tthh" ng-cloak>
     <h3 class="page-title"> 
          Personal
     </h3>
     <div class="page-bar" ng-init="nocache()">
         <ul class="page-breadcrumb">
             <li>
                 <i class="icon-home"></i>
                 <a href="/tthhPersonal">Personal</a>
                 <i class="fa fa-angle-right"></i>
             </li>
             <li>
                 <a href="/tthhFPersonal?idPersonal=<?= $_GET['idPersonal'] ?>">Editar Personal</a>
                 <i class="fa fa-angle-right"></i>
             </li>
         </ul>
        <div class="page-toolbar">
           
        </div>
    </div>

    <form id="formularioOrden" enctype="multipart/form-data"  role="form" method="POST" action="phrapi/expediente/save" class="form-horizontal form-row-seperated">
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-shopping-cart"></i>Registro / Edicion De Documentos </div>
                <div class="actions btn-set">
                    <!-- <button type="button" name="back" class="btn btn-secondary-outline">
                        <i class="fa fa-angle-left"></i> Regresar</button> -->
                   <!--  <button type="button" class="btn btn-secondary-outline" ng-click="limpiar()">
                        <i class="fa fa-reply"></i> Limpiar</button> -->
                    <button type="button" class="btn blue" id="btnaddord" ng-click="saveDatos()">
                        <i class="fa fa-check"></i> Guardar</button>
                    <!-- <button type="button" class="btn blue" id="btnaddord" ng-if="Personal.id_contrato == 0 && Personal.idPersonal > 0" ng-click="saveContrato()">
                        <i class="fa fa-check"></i> Contratar</button> -->
                    <!-- <a href='print_reporte.php?id={{Personal.id_order}}' target="_blank" class="btn btn-success" ng-show="Personal.id_order>0">
                        <i class="fa fa-check"></i> Impresión</a> -->
                    <!-- <button type="button" class="btn btn-success">
                        <i class="fa fa-check-circle save"></i> Guardar & Listar</button> -->
                </div>
            </div>
            <div class="form-body portlet-body">
                <input type="hidden" value="<?= $_GET['doc'] ?>" name="id_doc" />
                <input type="hidden" value="<?= $_GET['idPersonal'] ?>" name="id_personal">
                <div class="row">
                    <div class="col-md-6">
                        <fieldset class="scheduler-border">
                            <legend class="scheduler-border">Documento</legend>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Personal:
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-8">
                                    <input type="text" ng-model="data.personal" class="form-control" name="personal" id="personal" placeholder="" readonly/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Descripción:
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-8">
                                    <input type="text" ng-model="data.descripcion" class="form-control" name="descripcion" id="descripcion" placeholder="" readonly/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Estatus:
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-8">
                                    <select class="table-group-action-input form-control input-medium" name="estatus" ng-model="data.status">
                                        <option value="NO">NO</option>
                                        <option value="SI">SI</option>
                                        <option value="N/A">N/A</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Fecha act:
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-8">
                                    <input type="text" ng-model="data.fecha" class="form-control" name="fecha" id="fecha" placeholder="" readonly/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Archivo:
                                </label>
                                <div class="col-md-8">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="input-group input-large">
                                            <div class="form-control uneditable-input input-fixed input-medium" data-trigger="fileinput">
                                                <i class="fa fa-file fileinput-exists"></i>&nbsp;
                                                <span class="fileinput-filename"> </span>
                                            </div>
                                            <span class="input-group-addon btn default btn-file">
                                                <span class="fileinput-new"> Seleccionar Archivo </span>
                                                <span class="fileinput-exists"> Cambiar </span>
                                                <input type="file" name="archivo" accept="application/pdf, image/*"> </span>
                                            <a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput"> Remover </a>
                                        </div>
                                    </div>
                                    <div class="row" ng-if="data.ext != ''">
                                        <label class="font-red-thunderbird">
                                            Ya existe un archivo (con extension : "{{ data.ext }}"), Al subir uno nuevo se reemplazara el anterior <a class="btn" href="javascript:;" ng-click="eliminarArchivo()">Eliminar archivo</a>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Observaciones:</label>
                                <div class="col-md-8">
                                    <textarea class="form-control" name="observaciones" rows="3" style="margin: 0px -1px 0px 0px; height: 99px; width: 470px;" ng-model="data.observaciones"></textarea>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>