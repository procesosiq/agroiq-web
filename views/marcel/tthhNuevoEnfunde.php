<style>
    .form-group {
        margin-bottom : 5px;
    }
    .form-control, .input-sm, select, .btn, .portlet {
        border-radius : 3px !important;
    }
</style>

<div ng-controller="produccion">
    <h3 class="page-title"> 
          Nuevo Enfunde
    </h3>
    <div class="page-bar">
         <ul class="page-breadcrumb">
             <li>
                 <i class="icon-home"></i>
                 <a href="tthhEnfunde">Enfunde</a>
                 <i class="fa fa-angle-right"></i>
             </li>
             <li>
                 <a>Nuevo</a>
             </li>
         </ul>
        <div class="page-toolbar">
           
        </div>
    </div>
    <div class="portlet box green-haze">
        <div class="portlet-title">
            <div class="caption">
                DATOS DEL ENFUNDE
            </div>
            <div class="tools">
                <button class="btn green-jungle" ng-click="guardar()">GUARDAR</button>
            </div>
        </div>
        <div class="portlet-body">
            <div class="row">
                <div class="col-md-6">
                    <fieldset class="scheduler-border">
                        <legend class="scheduler-border"></legend>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-md-4 control-label">Fecha :
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-8">
                                    <input type="text" class="table-group-action-input form-control date-picker ng-pristine ng-valid ng-touched" data-date-format="yyyy-mm-dd" readonly="" name="fecha" ng-model="data.fecha" ng-change="changeFecha()" id="fecha"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-md-4 control-label">Semana :
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" readonly name="semana" ng-model="data.semana" id="semana" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-md-4 control-label">Color Cinta :
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-8">
                                    <div class="{{cinta.class}}" style="padding: 5px;">
                                        {{ cinta.color }}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-md-4 control-label">Enfundador :
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-8">
                                    <select class="form-control" id="enfundador" ng-model="data.enfundador" ng-change="changeSaldo()">
                                        <option ng-repeat="row in enfundadores" value="{{row.id}}" ng-selected="row.id == data.enfundador">{{ row.nombre }}</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-md-4 control-label">Lote :
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-8">
                                    <select class="form-control" ng-model="data.lote">
                                        <option ng-repeat="row in lotes" value="{{row.lote}}">{{ row.lote }}</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-md-4 control-label">Saldo Inicial :
                                </label>
                                <div class="col-md-8">
                                    <input type="number" class="form-control" readonly name="saldo_inicial" ng-model="data.saldo_inicial" id="saldo_inicial" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-md-4 control-label">Fundas Entregadas :
                                </label>
                                <div class="col-md-8">
                                    <input type="number" class="form-control" name="fundas_entregadas" ng-model="data.entregadas" id="fundas_entregadas" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-md-4 control-label">Total Fundas :
                                </label>
                                <div class="col-md-8">
                                    <span style="padding-left: 10px;"><b>{{ data.total_fundas = (data.entregadas + data.saldo_inicial) }}</b></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-md-4 control-label">Usadas :
                                </label>
                                <div class="col-md-8">
                                    <input type="number" class="form-control" name="usadas" ng-model="data.usadas" id="usadas" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-md-4 control-label">Saldo Fundas :
                                </label>
                                <div class="col-md-8">
                                    <span style="padding-left: 10px;"><b>{{ data.total_fundas - data.usadas }}</b></span>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div>
    </div>
</div>