 	<!-- BEGIN CONTENT BODY -->

<div ng-controller="mapas" ng-cloak>
     <h3 class="page-title"> 
          Auditoría de Labores Agrícolas
          <small>Mapas</small>
     </h3>
     <div class="page-bar">
         <ul class="page-breadcrumb">
             <li>
                 <i class="icon-home"></i>
                 <a>Inicio</a>
                 <i class="fa fa-angle-right"></i>
             </li>
             <li>
                 <span><a>Mapas</a></span>
                 <i class="fa fa-angle-right"></i>
             </li>
         </ul>
     </div>       
     <div ng-init="init()">
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN BASIC PORTLET-->
                <div class="portlet light portlet-fit bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class=" icon-layers font-red"></i>
                            <span class="caption-subject font-red bold uppercase">Puntos de muestreo</span>
                        </div>
                        <div class="actions">
                            PERIODO
                            <div class="btn-group btn-group-devided" data-toggle="buttons">
                                <select name="auditorias" id="auditorias" class="form-control" style="width: 100px;" ng-model="auditoria">
                                    <option value="">Seleccione</option>
                                    <option ng-repeat="a in auditorias" value="{{a.id}}">{{a.label}}</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <!--<div id="geo_map" class="gmaps" style="height: 500px !important"></div>-->
                        <div id="mapa_agroaudit" class="gmaps" style="height: 500px !important"></div>
                    </div>
                </div>
                <!-- END BASIC PORTLET-->
            </div>
        </div>
     </div>  
</div>
