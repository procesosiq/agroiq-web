<?php
    /*=============================================================
    =            Insertar aqui validacion  de usuarios            =
    =============================================================*/
    
    
    // $report = $factory->Reportes;
    /*=====  End of Insertar aqui validacion  de usuarios  ======*/
?>
<style>
    .label_chart {
        background-color: #fff;
        padding: 2px;
        margin-bottom: 8px;
        border-radius: 3px 3px 3px 3px;
        border: 1px solid #E6E6E6;
        display: inline-block;
        margin: 0 auto;
    }
    .legendLabel{
        padding: 3px !important;
    }
</style>
<div ng-controller="labores" ng-cloak>
    <div class="row" ng-init = "labores.init()">
        <div class="col-md-12 col-sm-12">
            <div class="portlet box light portlet-fit bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-settings"></i>Auditoría de Labores Agrícolas</div>
                    <div class="actions">
                        <div class="btn-group hide">
                            <button class="btn btn-primary dropdown-toggle" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false">
                                Exportar
                            </button>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="javascript:;" ng-click="exportExcel('lote_table', 'Merma por Lote')">Excel</a></li>
                                <li><a href="javascript:;" ng-click="exportPdf('lote_table')">PDF</a></li>
                                <li><a href="javascript:;">Imprimir</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12 col-sm-12">
            <div class="portlet box light portlet-fit bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <!--<i class="icon-settings"></i>Labores-->
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="portlet-body">
                        <div class="table-scrollable">
                            <table class="table table-striped table-bordered table-hover" id="labores_agricolas">
                                <thead>
                                    <tr style="cursor: pointer; text-align: center !important;" class="withCell">
                                        <th ng-click="orderByField = 'lote';reverseSort = !reverseSort" style="text-align: center;">
                                            TIPO LABOR
                                        </th>
                                        <th ng-click="orderByField = 'merma';reverseSort = !reverseSort" style="text-align: center;"> 
                                            % 
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat-start = "row in data_labores" ng-click = "row.expand = !row.expand">
                                        <td> {{ row.nombre }} </td> 
                                        <td> {{row.porcentaje}} </td>
                                    </tr>
                                    <tr ng-show = "row.expand">
                                        <td style="text-align: center;"> LABOR </td>
                                        <td style="text-align: center;"> % </td>
                                    </tr>
                                    <tr ng-repeat-end = "row" ng-show = "row.expand" ng-repeat = "labor in row.labor">
                                        <td>{{ labor.nombre }} </td>
                                        <td>{{ labor.porcentaje }} </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>