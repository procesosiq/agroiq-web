/*----------  UTILIDADES SOBRE ARRAYS  ----------*/
Array.prototype.amax = function() {
    return {
        label : this.indexOf(Math.max.apply(null, this)),
        value : Math.round(Math.max.apply(null, this))
    }
};

Array.prototype.amin = function() {
    return {
        label : this.indexOf(Math.min.apply(null, this)),
        value : Math.round(Math.min.apply(null, this))
    }
};

Array.prototype.sum = function (prop) {
    var property = prop || -1;
    var total = 0
    if(property == prop){
        for ( var i = 0, _len = this.length; i < _len; i++ ) {
            total += this[i][prop]
        }
    }else{
        for ( var i = 0, _len = this.length; i < _len; i++ ) {
            total += this[i]
        }
    }
    return total
}

Array.prototype.avg = function (prop) {
    var property = prop || -1;
    var total = 0 , avg = 0;
    if(property == prop){
        for ( var i = 0, _len = this.length; i < _len; i++ ) {
            total += this[i][prop]
        }
    }else{
        for ( var i = 0, _len = this.length; i < _len; i++ ) {
            total += this[i]
        }
    }

    avg = (total / this.length);

    return Math.round(avg)
}

Array.prototype.getUnique = function(){
   var u = {}, a = [];
   for(var i = 0, l = this.length; i < l; ++i){
      if(u.hasOwnProperty(this[i])) {
         continue;
      }
      a.push(this[i]);
      u[this[i]] = 1;
   }
   return a;
}
/*----------  UTILIDADES SOBRE ARRAYS  ----------*/
app.filter('orderObjectBy', function() {
  return function(items, field, reverse) {
      //alert(field);
    var filtered = [];
    angular.forEach(items, function(item) {
      filtered.push(item);
    });
    filtered.sort(function (a, b) {
        //alert(a[field]);
      return (a[field] > b[field] ? 1 : -1);
    });
    if(reverse) filtered.reverse();
    return filtered;
  };
});

app.filter('unique', function() {
   return function(collection, keyname) {
      var output = [], 
          keys = [];

      angular.forEach(collection, function(item) {
          var key = item[keyname];
          if(keys.indexOf(key) === -1) {
              keys.push(key);
              output.push(item);
          }
      });

      return output;
   };
});


/*----------  OBJETO PARA GRAFICAS ECHARTS  ----------*/
var globalEcharts;
var appEcharts = {
    require : require,
    options : [],
    options_principal : [],
    grafica_principal_calidad: [],
    grafica_principal_de: [],
    grafica_principal_peso: [],
    grafica_principal_cluster: [],
    grafica_danos_total: [],
    grafica_danos_seleccion: [],
    grafica_danos_seleccion_campos: [],
    grafica_danos_empaque: [],
    grafica_danos_empaque_campos: [],
    grafica_danos_otros: [],
    grafica_danos_otros_campos: [],
    type : "seleccion",
    init : function(callback){
        callback = callback || this.loadModules;
        this.require.config({
            paths: {
                echarts: '/assets/global/plugins/echarts/'
            }
        });

        this.require(
            [
                'echarts',
                'echarts/chart/bar',
                'echarts/chart/chord',
                'echarts/chart/eventRiver',
                'echarts/chart/force',
                'echarts/chart/funnel',
                'echarts/chart/gauge',
                'echarts/chart/heatmap',
                'echarts/chart/k',
                'echarts/chart/line',
                'echarts/chart/map',
                'echarts/chart/pie',
                'echarts/chart/radar',
                'echarts/chart/scatter',
                'echarts/chart/tree',
                'echarts/chart/treemap',
                'echarts/chart/venn',
                'echarts/chart/wordCloud'
            ],
            callback
        );
    },
    getOptionsPrincipalCalidad : function(){
        //console.log(this.grafica_principal_calidad);
        var category = [];
        var series = [];
        var serie_calidad = [];
        var serie_calidad_maxima = [];
        var serie_calidad_minima = [];
        var legend = [];

        for(var n in this.grafica_principal_calidad){
            category.push(n);
            serie_calidad.push(this.grafica_principal_calidad[n].calidad);
            serie_calidad_maxima.push(this.grafica_principal_calidad[n].calidad_maxima);
            serie_calidad_minima.push(this.grafica_principal_calidad[n].calidad_minima);
        }

        series.push({ name:'Calidad', type:'line', data:serie_calidad });
        series.push({ name:'Calidad Máxima', type:'line', data:serie_calidad_maxima });
        series.push({ name:'Calidad Mínima', type:'line', data:serie_calidad_minima });

        legend.push('Calidad');
        legend.push('Calidad Máxima');
        legend.push('Calidad Mínima');

        var option = {
            tooltip : {
                trigger: 'axis'
            },
            legend: {
                data:legend
            },
            toolbox: {
                show : true,
                feature : {
                    mark : {show: true},
                    dataZoom : {show: true},
                    dataView : {show: false},
                    magicType : {show: true, type: ['line', 'bar']},
                    restore : {show: true},
                    saveAsImage : {show: true}
                }
            },
            calculable : true,
            dataZoom : {
                show : true,
                realtime : true,
                start : 20,
                end : 80
            },
            xAxis : [
                {
                    type : 'category',
                    boundaryGap : false,
                    data : category
                }
            ],
            yAxis : [
                {
                    type : 'value'
                }
            ],
            series : series
        };

        return option;
    },

    getOptionsPrincipalDE : function(){
        //console.log(this.grafica_principal_de);
        var category = [];
        var series = [];
        var serie_de = [];
        var legend = [];

        for(var n in this.grafica_principal_de){
            category.push(n);
            serie_de.push(this.grafica_principal_de[n].desviacion_estandar);
        }

        series.push({ name:'Desviación Estándar', type:'line', data:serie_de });

        legend.push('Desviación Estándar');

        var option = {
            tooltip : {
                trigger: 'axis'
            },
            legend: {
                data:legend
            },
            toolbox: {
                show : true,
                feature : {
                    mark : {show: true},
                    dataZoom : {show: true},
                    dataView : {show: false},
                    magicType : {show: true, type: ['line', 'bar']},
                    restore : {show: true},
                    saveAsImage : {show: true}
                }
            },
            calculable : true,
            dataZoom : {
                show : true,
                realtime : true,
                start : 20,
                end : 80
            },
            xAxis : [
                {
                    type : 'category',
                    boundaryGap : false,
                    data : category
                }
            ],
            yAxis : [
                {
                    type : 'value'
                }
            ],
            series : series
        };

        return option;
    },

    getOptionsPrincipalPeso : function(){
        //console.log(this.grafica_principal_peso);
        var category = [];
        var series = [];
        var serie_peso = [];
        var legend = [];

        for(var n in this.grafica_principal_peso){
            category.push(n);
            serie_peso.push(this.grafica_principal_peso[n].peso);
        }

        series.push({ name:'Peso', type:'line', data:serie_peso });

        legend.push('Peso');

        var option = {
            tooltip : {
                trigger: 'axis'
            },
            legend: {
                data:legend
            },
            toolbox: {
                show : true,
                feature : {
                    mark : {show: true},
                    dataZoom : {show: true},
                    dataView : {show: false},
                    magicType : {show: true, type: ['line', 'bar']},
                    restore : {show: true},
                    saveAsImage : {show: true}
                }
            },
            calculable : true,
            dataZoom : {
                show : true,
                realtime : true,
                start : 20,
                end : 80
            },
            xAxis : [
                {
                    type : 'category',
                    boundaryGap : false,
                    data : category
                }
            ],
            yAxis : [
                {
                    type : 'value'
                }
            ],
            series : series
        };

        return option;
    },

    getOptionsPrincipalCluster : function(){
        console.log(this.grafica_principal_cluster);
        var category = [];
        var series = [];
        var serie_cluster = [];
        var legend = [];

        for(var n in this.grafica_principal_cluster){
            category.push(n);
            serie_cluster.push(this.grafica_principal_cluster[n].cluster);
        }

        series.push({ name:'Clúster', type:'line', data:serie_cluster });

        legend.push('Clúster');

        var option = {
            tooltip : {
                trigger: 'axis'
            },
            legend: {
                data:legend
            },
            toolbox: {
                show : true,
                feature : {
                    mark : {show: true},
                    dataZoom : {show: true},
                    dataView : {show: false},
                    magicType : {show: true, type: ['line', 'bar']},
                    restore : {show: true},
                    saveAsImage : {show: true}
                }
            },
            calculable : true,
            dataZoom : {
                show : true,
                realtime : true,
                start : 20,
                end : 80
            },
            xAxis : [
                {
                    type : 'category',
                    boundaryGap : false,
                    data : category
                }
            ],
            yAxis : [
                {
                    type : 'value'
                }
            ],
            series : series
        };

        return option;
    },

    getOptionsDanosTotal : function(){
        //console.log(this.grafica_danos_total);
        var category = [];
        var series = [];
        var serie_seleccion = [];
        var serie_empaque = [];
        var serie_otros = [];
        var legend = [];

        for(var n in this.grafica_danos_total){
            category.push(n);
            serie_seleccion.push(this.grafica_danos_total[n].seleccion);
            serie_empaque.push(this.grafica_danos_total[n].empaque);
            serie_otros.push(this.grafica_danos_total[n].otros);
        }

        series.push({ name:'Selección', type:'line', data:serie_seleccion });
        series.push({ name:'Empaque', type:'line', data:serie_empaque });
        series.push({ name:'Otros', type:'line', data:serie_otros });

        legend.push('Selección');
        legend.push('Empaque');
        legend.push('Otros');

        var option = {
            tooltip : {
                trigger: 'axis'
            },
            legend: {
                data:legend
            },
            toolbox: {
                show : true,
                feature : {
                    mark : {show: true},
                    dataZoom : {show: true},
                    dataView : {show: false},
                    magicType : {show: true, type: ['line', 'bar']},
                    restore : {show: true},
                    saveAsImage : {show: true}
                }
            },
            calculable : true,
            dataZoom : {
                show : true,
                realtime : true,
                start : 20,
                end : 80
            },
            xAxis : [
                {
                    type : 'category',
                    boundaryGap : false,
                    data : category
                }
            ],
            yAxis : [
                {
                    type : 'value'
                }
            ],
            series : series
        };

        return option;
    },

    getOptionsDanosSeleccion : function(){
        //console.log(this.grafi);
        var category = [];
        var series = [];
        var legend = [];

        // indicadores
        for(var n in this.grafica_danos_seleccion_campos){
            if(!isNaN(n)){
                legend.push(this.grafica_danos_seleccion_campos[n]);
                series.push({ name: this.grafica_danos_seleccion_campos[n], type:'line', data:[] });
            }
        }

        for(var n in this.grafica_danos_seleccion){
            category.push(n); // barra de abajo

            for(var j in series){ // ciclo a todos los campos
                if(!isNaN(j)){
                    var ele = series[j];
                    var existe = false;

                    for(var m in this.grafica_danos_seleccion[n]){
                        if(m == ele.name){ // si el campo coincide se agrega el valor
                            ele.data.push( this.grafica_danos_seleccion[n][m] );
                            existe = true;
                            break;
                        }
                    }

                    if(existe == false) // si no se encontró el campo se agrega un cero
                        ele.data.push( 0 );
                }
            }
        }

        var option = {
            tooltip : {
                trigger: 'axis'
            },
            legend: {
                data:legend
            },
            toolbox: {
                show : true,
                feature : {
                    mark : {show: true},
                    dataZoom : {show: true},
                    dataView : {show: false},
                    magicType : {show: true, type: ['line', 'bar']},
                    restore : {show: true},
                    saveAsImage : {show: true}
                }
            },
            calculable : true,
            dataZoom : {
                show : true,
                realtime : true,
                start : 20,
                end : 80
            },
            xAxis : [
                {
                    type : 'category',
                    boundaryGap : false,
                    data : category
                }
            ],
            yAxis : [
                {
                    type : 'value'
                }
            ],
            series : series
        };

        return option;
    },

    getOptionsDanosEmpaque : function(){
        //console.log(this.grafi);
        var category = [];
        var series = [];
        var legend = [];

        // indicadores
        for(var n in this.grafica_danos_empaque_campos){
            if(!isNaN(n)){
                legend.push(this.grafica_danos_empaque_campos[n]);
                series.push({ name: this.grafica_danos_empaque_campos[n], type:'line', data:[] });
            }
        }

        for(var n in this.grafica_danos_empaque){
            category.push(n); // barra de abajo

            for(var j in series){ // ciclo a todos los campos
                if(!isNaN(j)){
                    var ele = series[j];
                    var existe = false;

                    for(var m in this.grafica_danos_empaque[n]){
                        if(m == ele.name){ // si el campo coincide se agrega el valor
                            ele.data.push( this.grafica_danos_empaque[n][m] );
                            existe = true;
                            break;
                        }
                    }

                    if(existe == false) // si no se encontró el campo se agrega un cero
                        ele.data.push( 0 );
                }
            }
        }

        var option = {
            tooltip : {
                trigger: 'axis'
            },
            legend: {
                data:legend
            },
            toolbox: {
                show : true,
                feature : {
                    mark : {show: true},
                    dataZoom : {show: true},
                    dataView : {show: false},
                    magicType : {show: true, type: ['line', 'bar']},
                    restore : {show: true},
                    saveAsImage : {show: true}
                }
            },
            calculable : true,
            dataZoom : {
                show : true,
                realtime : true,
                start : 20,
                end : 80
            },
            xAxis : [
                {
                    type : 'category',
                    boundaryGap : false,
                    data : category
                }
            ],
            yAxis : [
                {
                    type : 'value'
                }
            ],
            series : series
        };

        return option;
    },

    getOptionsDanosOtros : function(){
        //console.log(this.grafi);
        var category = [];
        var series = [];
        var legend = [];

        // indicadores
        for(var n in this.grafica_danos_otros_campos){
            if(!isNaN(n)){
                legend.push(this.grafica_danos_otros_campos[n]);
                series.push({ name: this.grafica_danos_otros_campos[n], type:'line', data:[] });
            }
        }

        for(var n in this.grafica_danos_otros){
            category.push(n); // barra de abajo

            for(var j in series){ // ciclo a todos los campos
                if(!isNaN(j)){
                    var ele = series[j];
                    var existe = false;

                    for(var m in this.grafica_danos_otros[n]){
                        if(m == ele.name){ // si el campo coincide se agrega el valor
                            ele.data.push( this.grafica_danos_otros[n][m] );
                            existe = true;
                            break;
                        }
                    }

                    if(existe == false) // si no se encontró el campo se agrega un cero
                        ele.data.push( 0 );
                }
            }
        }

        var option = {
            tooltip : {
                trigger: 'axis'
            },
            legend: {
                data:legend
            },
            toolbox: {
                show : true,
                feature : {
                    mark : {show: true},
                    dataZoom : {show: true},
                    dataView : {show: false},
                    magicType : {show: true, type: ['line', 'bar']},
                    restore : {show: true},
                    saveAsImage : {show: true}
                }
            },
            calculable : true,
            dataZoom : {
                show : true,
                realtime : true,
                start : 20,
                end : 80
            },
            xAxis : [
                {
                    type : 'category',
                    boundaryGap : false,
                    data : category
                }
            ],
            yAxis : [
                {
                    type : 'value'
                }
            ],
            series : series
        };

        return option;
    },

    getOptionsPrincipal : function(){
        var category = [] , serie = [];
        //console.log(this.options_principal);
        this.options_principal.amap(function(item){
            category.push(item.fecha)
            serie.push(parseFloat(item.calidad))
            return item.name;
        });
        //console.log(category)
        //console.log(serie)
        var option = {
            tooltip : {
                trigger: 'axis'
            },
            legend: {
                data:['REWE']
            },
            toolbox: {
                show : true,
                feature : {
                    mark : {show: true},
                    dataZoom : {show: true},
                    dataView : {show: false},
                    magicType : {show: true, type: ['line', 'bar']},
                    restore : {show: true},
                    saveAsImage : {show: true}
                }
            },
            calculable : true,
            dataZoom : {
                show : true,
                realtime : true,
                start : 20,
                end : 80
            },
            xAxis : [
                {
                    type : 'category',
                    boundaryGap : false,
                    data : category
                }
            ],
            yAxis : [
                {
                    type : 'value'
                }
            ],
            series : [
                {
                    name:'REWE',
                    type:'line',
                    data:serie
                }
            ]
        };

        return option;
    },
    getOptionType : function(){
        //console.log('SUPERMAN');
        //console.log(this.options[this.type]);
        var legend = [];
        this.options[this.type].amap(function(item){
            legend.push(item.name)
            return item.name;
        })
        //console.log(legend)
        var options = {
            tooltip : {
                trigger: 'item',
                formatter: "{a} <br/>{b} : {c} ({d}%)"
            },
            legend: {
                orient : 'vertical',
                x : 'left',
                data:legend
            },
            toolbox: {
                show : true,
                feature : {
                    mark : {show: true},
                    dataView : {show: false, readOnly: false},
                    magicType : {
                        show: true, 
                        type: ['pie', 'funnel'],
                        option: {
                            funnel: {
                                x: '25%',
                                width: '50%',
                                funnelAlign: 'center',
                                max: 1548
                            }
                        }
                    },
                    restore : {show: true},
                    saveAsImage : {show: true}
                }
            },
            calculable : true,
            series : [
                {
                    name:'PROMEDIO GENERAL',
                    type:'pie',
                    radius : ['50%', '70%'],
                    itemStyle : {
                        normal : {
                            label : {
                                show : false
                            },
                            labelLine : {
                                show : false
                            }
                        },
                        emphasis : {
                            label : {
                                show : true,
                                position : 'center',
                                textStyle : {
                                    fontSize : '30',
                                    fontWeight : 'bold'
                                }
                            }
                        }
                    },
                    data:this.options[this.type]
                }
            ]
        };
        return options;
    },
    generateOptions : function(){
        var serie = this.options.series;
        var options = {
            tooltip : {
                trigger: 'item',
                formatter: "{a} <br/>{b} : {c} ({d}%)"
            },
            legend: {
                orient : 'vertical',
                x : 'left',
                data:['SELECCION' , 'EMPAQUE' , 'OTROS']
            },
            toolbox: {
                show : true,
                feature : {
                    mark : {show: true},
                    dataView : {show: false, readOnly: false},
                    magicType : {
                        show: true, 
                        type: ['pie', 'funnel'],
                        option: {
                            funnel: {
                                x: '25%',
                                width: '50%',
                                funnelAlign: 'center',
                                max: 1548
                            }
                        }
                    },
                    restore : {show: true},
                    saveAsImage : {show: true}
                }
            },
            calculable : true,
            series : [
                {
                    name:'PROMEDIO GENERAL',
                    type:'pie',
                    radius : ['50%', '70%'],
                    itemStyle : {
                        normal : {
                            label : {
                                show : false
                            },
                            labelLine : {
                                show : false
                            }
                        },
                        emphasis : {
                            label : {
                                show : true,
                                position : 'center',
                                textStyle : {
                                    fontSize : '30',
                                    fontWeight : 'bold'
                                }
                            }
                        }
                    },
                    data:serie
                }
            ]
        };
        return options;
    },
    loadModules : function(echarts){
        globalEcharts = echarts;
        var general = [];
        var general_echarts = [];
        var option = appEcharts.generateOptions();
        var option2 = appEcharts.getOptionType()
        var option_principal = appEcharts.getOptionsPrincipal();
        var grafica_principal_calidad = appEcharts.getOptionsPrincipalCalidad();
        var grafica_principal_de = appEcharts.getOptionsPrincipalDE();
        var grafica_principal_peso = appEcharts.getOptionsPrincipalPeso();
        var grafica_principal_cluster = appEcharts.getOptionsPrincipalCluster();
        var grafica_danos_total = appEcharts.getOptionsDanosTotal();
        var grafica_danos_seleccion = appEcharts.getOptionsDanosSeleccion();
        var grafica_danos_empaque = appEcharts.getOptionsDanosEmpaque();
        var grafica_danos_otros = appEcharts.getOptionsDanosOtros();
        
        // general_echarts.push(option);
        // general_echarts.push(option);
        // general_echarts.push(option);
        
        // daños
        general_echarts = echarts.init(document.getElementById('highstock_1') , 'infographic');
        general_echarts.setOption(option_principal);

        // calidad
        general_echarts = echarts.init(document.getElementById('highstock_calidad') , 'infographic');
        general_echarts.setOption(grafica_principal_calidad);

        // daños total
        general_echarts = echarts.init(document.getElementById('highstock_danos_total') , 'infographic');
        general_echarts.setOption(grafica_danos_total);

        general_echarts = echarts.init(document.getElementById('highstock_danos_seleccion') , 'infographic');
        general_echarts.setOption(grafica_danos_seleccion);
        general_echarts = echarts.init(document.getElementById('highstock_danos_empaque') , 'infographic');
        general_echarts.setOption(grafica_danos_empaque);
        general_echarts = echarts.init(document.getElementById('highstock_danos_otros') , 'infographic');
        general_echarts.setOption(grafica_danos_otros);

        

        general.push(echarts.init(document.getElementById('echarts_generales') , 'infographic'));
        general[0].setOption(option);


        general.push(echarts.init(document.getElementById('echarts_generales_detalles') , 'infographic'));
        general[1].setOption(option2);


        general[0].setTheme('infographic');
        general[1].setTheme('infographic');
        general_echarts.setTheme('infographic');
        window.onresize = function(){
            general[0].resize();
            general[1].resize();
            general_echarts.resize();
        }
    }
}
/*----------  OBJETO PARA GRAFICAS ECHARTS  ----------*/

function getQueryParams() {
    var params = {};

    if (location.search) {
        var parts = location.search.substring(1).split('&');

        for (var i = 0; i < parts.length; i++) {
            var nv = parts[i].split('=');
            if (!nv[0]) continue;
            params[nv[0]] = nv[1] || true;
        }
    }

    // Now you can get the parameters you want like so:
    console.log(params);
    return params;
}

function loadScript(options){
  // HIGHSTOCK DEMOS
  // COMPARE MULTIPLE SERIES
    // var seriesOptions = [],
   //      seriesCounter = 0,
   //      names = ['REWE'],
   //      // create the chart when all data is loaded
   //      createChart = function () {
   //       console.log(seriesOptions);
   //          $('#highstock_1').highcharts('StockChart', {
   //              chart : {
   //                  style: {
   //                      fontFamily: 'Open Sans'
   //                  }
   //              },

   //              rangeSelector: {
   //                  selected: 1
   //              },
   //              xAxis:{
   //               // type: 'datetime',
   //               ordinal: false
   //              },
   //              // yAxis: {
   //              //     labels: {
   //              //         formatter: function () {
   //              //             return (this.value > 0 ? ' + ' : '') + this.value + '%';
   //              //         }
   //              //     },
   //              //     plotLines: [{
   //              //         value: 0,
   //              //         width: 2,
   //              //         color: 'silver'
   //              //     }]
   //              // },

   //              // plotOptions: {
   //              //     series: {
   //              //         compare: 'percent'
   //              //     }
   //              // },

   //              tooltip: {
   //                  pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.change}%)<br/>'
   //              },

   //              series: seriesOptions
   //          });
   //      };
    
   //  $.each(names, function (i, name) {

   //      $.getJSON('phrapi/calidad/index',    function (data) {

   //       var xdata = [];

   //       for(var x in data.data){
   //           xdata.push([
   //               new Date(data.data[x].fecha).getTime(),
   //               parseFloat(data.data[x].calidad)
   //           ])
   //       }

   //          seriesOptions[i] = {
   //              name: name,
   //              data: xdata,
            //  tooltip: {
   //                  valueDecimals: 2
   //              },
   //              connectNulls: true
   //          };

   //          // As we're loading the data asynchronously, we don't know what order it will arrive. So
   //          // we keep a counter and create the chart when all the data is loaded.
   //          seriesCounter += 1;

   //          if (seriesCounter === names.length) {
   //              createChart();
   //          }
   //      });
   //  });
    // $.each(names, function (i, name) {

    //     $.getJSON('http://www.highcharts.com/samples/data/jsonp.php?filename=' + name.toLowerCase() + '-c.json&callback=?',    function (data) {

    //         seriesOptions[i] = {
    //             name: name,
    //             data: data
    //         };

    //         // As we're loading the data asynchronously, we don't know what order it will arrive. So
    //         // we keep a counter and create the chart when all the data is loaded.
    //         seriesCounter += 1;

    //         if (seriesCounter === names.length) {
    //             createChart();
    //         }
    //     });
    // });


    appEcharts.options = options.danhos;
    //console.log(options.main)
    appEcharts.options_principal = options.main;
    //console.log(appEcharts.options_principal)

    appEcharts.grafica_principal_calidad = options.grafica_principal_calidad;
    appEcharts.grafica_principal_de = options.grafica_principal_de;
    appEcharts.grafica_principal_peso = options.grafica_principal_peso;
    appEcharts.grafica_principal_cluster = options.grafica_principal_cluster;

    appEcharts.grafica_danos_total = options.grafica_danos_total;
    appEcharts.grafica_danos_seleccion = options.grafica_danos_seleccion;
    appEcharts.grafica_danos_seleccion_campos = options.grafica_danos_seleccion_campos;
    appEcharts.grafica_danos_empaque = options.grafica_danos_empaque;
    appEcharts.grafica_danos_empaque_campos = options.grafica_danos_empaque_campos;
    appEcharts.grafica_danos_otros = options.grafica_danos_otros;
    appEcharts.grafica_danos_otros_campos = options.grafica_danos_otros_campos;

    appEcharts.init();
}

function viewGraphPrincipal(id, a){
    var id_div;
    $("#highstock_calidad").addClass("hide");
    $("#highstock_de").addClass("hide");
    $("#highstock_peso").addClass("hide");
    $("#highstock_cluster").addClass("hide");

    if(1 == id){
        id_div = 'highstock_calidad';
        appEcharts.grafica_principal_calidad = a;
        var options = appEcharts.getOptionsPrincipalCalidad();
    }
    if(2 == id){
        id_div = 'highstock_de';
        appEcharts.grafica_principal_de = a;
        var options = appEcharts.getOptionsPrincipalDE();
    }
    if(3 == id){
        id_div = 'highstock_peso';
        appEcharts.grafica_principal_peso = a;
        var options = appEcharts.getOptionsPrincipalPeso();
    }
    if(4 == id){
        id_div = 'highstock_cluster';
        appEcharts.grafica_principal_cluster = a;
        var options = appEcharts.getOptionsPrincipalCluster();
    }


    $("#" + id_div).removeClass("hide");
    general_echarts = globalEcharts.init(document.getElementById(id_div) , 'infographic');
    general_echarts.setOption(options);
}

function viewGraphDanos(id, a){
    var id_div;
    $("#highstock_danos_total").addClass("hide");
    $("#highstock_danos_seleccion").addClass("hide");
    $("#highstock_danos_empaque").addClass("hide");
    $("#highstock_danos_otros").addClass("hide");

    if(1 == id){
        id_div = 'highstock_danos_total';
        appEcharts.grafica_danos_total = a;
        var options = appEcharts.getOptionsDanosTotal();
    }
    if(2 == id){
        id_div = 'highstock_danos_seleccion';
        appEcharts.grafica_danos_seleccion = a;
        var options = appEcharts.getOptionsDanosSeleccion();
    }
    if(3 == id){
        id_div = 'highstock_danos_empaque';
        appEcharts.grafica_danos_empaque = a;
        var options = appEcharts.getOptionsDanosEmpaque();
    }
    if(4 == id){
        id_div = 'highstock_danos_otros';
        appEcharts.grafica_danos_otros = a;
        var options = appEcharts.getOptionsDanosOtros();
    }


    $("#" + id_div).removeClass("hide");
    general_echarts = globalEcharts.init(document.getElementById(id_div) , 'infographic');
    general_echarts.setOption(options);
}

app.factory('Excel',function($window){
    var uri='data:application/vnd.ms-excel;base64,',
        template='<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>',
        base64=function(s){return $window.btoa(unescape(encodeURIComponent(s)));},
        format=function(s,c){return s.replace(/{(\w+)}/g,function(m,p){return c[p];})};
    return {
        tableToExcel:function(tableId,worksheetName){
            var table=$(tableId),
                ctx={worksheet:worksheetName,table:table.html()},
                href=uri+base64(format(template,ctx));
            return href;
        }
    };
});

app.controller('informe_calidad', ['$scope','$http','$interval','client','$controller','Excel','$timeout' ,'$window', '$routeParams', function($scope,$http,$interval,client, $controller,Excel , $timeout,$window, $routeParams){
    

    var cliente_marca = getQueryParams();
    var param_cliente = "";
    var param_marca   = "";

    if(cliente_marca.c && cliente_marca.m){
        param_cliente = decodeURI(cliente_marca.c);
        param_marca   = decodeURI(cliente_marca.m);
    }

    $scope.leyendaGeneralTitle = 'Calidad';
    $scope.data_grafica_calidad = [];
    $scope.data_grafica_de      = [];
    $scope.data_grafica_peso    = [];
    $scope.data_grafica_cluster = [];
    $scope.data_grafica_danos_total = [];
    $scope.data_grafica_danos_seleccion = [];
    $scope.data_grafica_danos_empaque = [];
    $scope.data_grafica_danos_otros = [];

    $scope.param_cliente = param_cliente;
    $scope.param_marca   = param_marca;
    $scope.param_peso    = "";
    $scope.param_cluster = "";
    $scope.param_logo = "";

    $scope.checkParams = function(){
        if(param_cliente && param_marca){
            console.log('>' + param_marca + '<')
            if(param_cliente == "" && param_marca == "")
                return false;
            else
                return true;
        }
        return false;
    }

    $scope.id_company = 0;
    $scope.tags = {
        calidad : {
            value : 0,
            label : ""
        },
        calidad_maxima : {
            value : 0,
            label : ""
        },
        calidad_minima : {
            value : 0,
            label : ""
        },
        cluster : {
            value : 0,
            label : ""
        },
        desviacion_estandar : {
            value : 0,
            label : ""
        },
        peso : {
            value : 0,
            label : ""
        }
    };

    $scope.umbrales = {};
    $scope.calidad = {
        params : {
            idFinca : 1,
            idLote : 0,
            idLabor : 0,
            fecha_inicial : '2015-06-01',
            fecha_final : '2016-07-30',
            cliente: param_cliente,
            marca: param_marca
        },
        step : 0,
        path : ['phrapi/calidad/index' , 'phrapi/calidad/labores' , 'phrapi/calidad/causas'],
        templatePath : [],
        nocache : function(){
            this.templatePath.push('/views/templetes/calidad/step1.html?' +Math.random());
            //console.log(this.templatePath);
            // this.templatePath.push('/views/step2.html?' +Math.random());
            // this.templatePath.push('/views/step1.html?' +Math.random());
        }

    }

    $scope.tab = function(tab){
        appEcharts.type = tab;
        appEcharts.init();
    }

    $scope.loadExternal = function(){
        console.log($scope.calidad.path[$scope.calidad.step]);
        if($scope.calidad.path[$scope.calidad.step] != ""){
            var data = $scope.calidad.params;
            client.post($scope.calidad.path[$scope.calidad.step] , $scope.startDetails , data);
        }
    }

    $scope.startDetails = function(r , b){
        b();
        if(r){
            var options = {
                main : r.data || [],
                danhos : r.danhos || [],
                grafica_principal_calidad: r.grafica_principal_calidad || [],
                grafica_principal_de: r.grafica_principal_de || [],
                grafica_principal_peso: r.grafica_principal_peso || [],
                grafica_principal_cluster: r.grafica_principal_cluster || [],
                
                grafica_danos_total: r.grafica_danos_total || [],
                grafica_danos_seleccion: r.grafica_danos_seleccion || [],
                grafica_danos_seleccion_campos: r.grafica_danos_seleccion_campos || [],
                grafica_danos_empaque: r.grafica_danos_empaque || [],
                grafica_danos_empaque_campos: r.grafica_danos_empaque_campos || [],
                grafica_danos_otros: r.grafica_danos_otros || [],
                grafica_danos_otros_campos: r.grafica_danos_otros_campos || []
            };
        
            // data grafica
            $scope.data_grafica_calidad = r.grafica_principal_calidad   || [];
            $scope.data_grafica_de      = r.grafica_principal_de        || [];
            $scope.data_grafica_peso    = r.grafica_principal_peso      || [];
            $scope.data_grafica_cluster = r.grafica_principal_cluster   || [];
            $scope.data_grafica_danos_total = r.grafica_danos_total     || [];
            $scope.data_grafica_danos_seleccion = r.grafica_danos_seleccion     || [];
            $scope.data_grafica_danos_empaque = r.grafica_danos_empaque     || [];
            $scope.data_grafica_danos_otros = r.grafica_danos_otros     || [];

            $scope.umbrales = r.umbrals || {};
            $scope.tags.calidad.value = Math.round(r.tags.calidad);
            $scope.tags.calidad_maxima.value = Math.round(r.tags.calidad_maxima);
            $scope.tags.calidad_minima.value = Math.round(r.tags.calidad_minima);
            $scope.tags.cluster.value = Math.round(r.tags.cluster);
            $scope.tags.desviacion_estandar.value = Math.round(r.tags.desviacion_estandar);
            $scope.tags.peso.value = Math.round(r.tags.peso);

            // tablas
            $scope.tabla_cliente_marca_calidad = r.tabla_principal_calidad;
            $scope.tabla_cliente_marca_danos = r.tabla_principal_danos;

            // header
            if(r.data_header){
                $scope.param_peso    = r.data_header.peso;
                $scope.param_cluster = r.data_header.cluster;
                $scope.param_logo = r.data_header.logo;
            }

            setTimeout(function(){
                loadScript(options)
                // console.log($scope.tags);
                $(".counter_tags").counterUp({
                    delay: 10,
                    time: 1000
                });
            } , 1000);
        }
    }

    $scope.revision = function(porcentaje){
        if(porcentaje >=  parseFloat($scope.umbrales.green_umbral_1))
            return 'green-jungle';
        else if (porcentaje >= parseFloat($scope.umbrales.yellow_umbral_1) && porcentaje <= parseFloat($scope.umbrales.yellow_umbral_2))
            return 'yellow-lemon';
        else 
            return 'red-thunderbird'; 
    }

    $scope.viewGraph = function(i){
        if(1 == i){
            viewGraphPrincipal(i, $scope.data_grafica_calidad);
            $scope.leyendaGeneralTitle = 'Calidad';
        }
        if(2 == i){
            viewGraphPrincipal(i, $scope.data_grafica_de);
            $scope.leyendaGeneralTitle = 'Desviación Estándar';
        }
        if(3 == i){
            viewGraphPrincipal(i, $scope.data_grafica_peso);
            $scope.leyendaGeneralTitle = 'Peso';
        }
        if(4 == i){
            viewGraphPrincipal(i, $scope.data_grafica_cluster);
            $scope.leyendaGeneralTitle = 'Clúster';
        }
    }

    $scope.viewGraphDanos = function(i){
        if(1 == i){
            viewGraphDanos(i, $scope.data_grafica_danos_total);
            $scope.leyendaGeneralTitle = 'Total';
        }
        if(2 == i){
            viewGraphDanos(i, $scope.data_grafica_danos_seleccion);
            $scope.leyendaGeneralTitle = 'Selección';
        }
        if(3 == i){
            viewGraphDanos(i, $scope.data_grafica_danos_empaque);
            $scope.leyendaGeneralTitle = 'Empaque';
        }
        if(4 == i){
            viewGraphDanos(i, $scope.data_grafica_danos_otros);
            $scope.leyendaGeneralTitle = 'Otros';
        }
    }

}]);




