<style>
	th, td{
		text-align: center !important;
	}
	.center-th {
		text-align: center !important;
	}
	table {
		cursor: pointer !important;
	}
	.anchoTable {
		width: 100px;
	}
    .delete  {
        background-color : #c8d8e8 !important;
    }
    .chart {
        height : 400px;
    }
</style>
<div ng-controller="produccion" ng-cloak>
	<h3 class="page-title"> 
          Producción <small>{{ subTittle }}</small>
     </h3>
     <div class="page-bar" ng-init="getLastDay()">
         <ul class="page-breadcrumb">
             <li>
                 <i class="icon-home"></i>
                 <a>Producción</a>
                 <i class="fa fa-angle-right"></i>
             </li>
         </ul>
         <div class="page-toolbar">
            <button class="btn green-jungle" ng-disabled="cargandoHistorico" ng-click="exportarFormatoEspecial()">Excel</button>
		 	<label for="">
                Finca : 
            </label>
            <select ng-change="produccion.nocache()" name="finca" id="finca" ng-model="produccion.params.finca" style="margin: 2px;height: 36px;">
                <option value="">TODOS</option>
                <option ng-repeat="(key, value) in fincas" value="{{key}}" ng-selected="key == produccion.params.finca">{{value}}</option>
            </select>
            <input class="input-sm" sytle="height: 36px;" data-provide="datepicker" data-date-format="yyyy-mm-dd" ng-model="table.fecha_inicial" ng-change="changeRangeDate({ first_date: table.fecha_inicial, second_date: table.fecha_inicial })" readonly>
        </div>
    </div>

	<?php include('./views/mario/tags_produccion_dia_demo.php') ?>

    <div class="row">
        <div class="col-md-6">
            <div class="portlet box green">
				<div class="portlet-title">
					<span class="caption">RESUMEN POR EDAD</span>
				</div>
				<div class="portlet-body">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>EDAD</th>
                                    <th>CALIBRE PROM</th>
                                    <th>PROC</th>
                                    <th>RECU</th>
                                    <th>CORT</th>
                                    <th>%</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="row in resumen_edades | orderObjectBy : 'edad'">
                                    <td class="{{ row.class }}">{{ row.edad > 0 ? row.edad : 'S/C' }}</td>
                                    <td>{{row.calibracion | number: 2}}</td>
                                    <td>{{row.procesados}}</td>
                                    <td>{{row.recusados}}</td>
                                    <td>{{row.cosechados}}</td>
                                    <td>{{row.cosechados / (resumen_edades | sumOfValue : 'cosechados') * 100 | number: 2 }}%</td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>{{ resumen_edades | avgOfValue : 'edad' | number:2 }}</th>
                                    <th>{{ (resumen_edades | avgOfValue : 'calibracion' | number: 2) || '' }}</th>
                                    <th>{{ resumen_edades | sumOfValue : 'procesados' }}</th>
                                    <th>{{ resumen_edades | sumOfValue : 'recusados' }}</th>
                                    <th>{{ resumen_edades | sumOfValue : 'cosechados' }}</th>
                                    <th>100%</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="portlet box green">
				<div class="portlet-title">
					<span class="caption">RESUMEN DE RECUSADOS</span>
				</div>
				<div class="portlet-body">
                    <div class="row">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>CAUSA</th>
                                        <th>CANTIDAD</th>
                                        <th>PORCENTAJE</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="r in recusados">
                                        <td>{{ r.causa }}</td>
                                        <td>{{ r.cantidad }}</td>
                                        <td>{{ r.porcentaje }}</td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>RECUSADOS</th>
                                        <th>{{ recusados | sumOfValue : 'cantidad' | number }}</th>
                                        <th>100</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

	<div class="row">
		<div class="col-md-12">
			<div class="portlet box green">
				<div class="portlet-title">
					<span class="caption">RESUMEN POR LOTE</span>
				</div>
				<div class="portlet-body" >
					<div class="table-responsive" id="promedios_lotes">
						<table class="table table-striped table-bordered table-hover">
							<thead>
                                <tr>
                                    <th class="center-th"></th>
									<th class="center-th" colspan="{{edades.length}}" ng-if="edades.length > 0">EDAD</th>
                                    <th class="center-th"></th>
                                    <th class="center-th"></th>
									<th class="center-th"></th>
                                    <th class="center-th"></th>
									<th class="center-th"></th>
									<th class="center-th"></th>
                                </tr>
								<tr>
									<th class="center-th">LOTE</th>
									<th class="center-th {{ e.class }}" ng-repeat="e in edades | orderObjectBy:'edad'">{{ e.edad }}</th>
                                    <th class="center-th">COSE</th>
									<th class="center-th">PROC</th>
                                    <th class="center-th">RECU</th>
									<th class="center-th">PESO</th>
								    <th class="center-th">CALIBRE</th>
									<th class="center-th">MANOS</th>
								</tr>
							</thead>
							<tbody>
								<tr ng-repeat="row in resumen">
									<td>{{ row.lote }}</td>
									<td ng-repeat="e in edades | orderObjectBy:'edad'">{{ row['edad_'+e.edad] }}</td>
                                    <td>{{ row.cosechados = ((row.procesados | num) + (row.recusados | num)) | number }}</td>
									<td>{{ row.procesados | number }}</td>
                                    <td>{{ row.recusados }}</td>
									<td>{{ row.peso }}</td>
									<td>{{ (row.calibre_segunda > 0) ? row.calibre_segunda : '' }}</td>
									<td>{{ (row.manos > 0) ? row.manos : '' }}</td>
								</tr>
							</tbody>
                            <tfoot>
                                <tr>
                                    <th>TOTAL</th>
                                    <th class="center-th" ng-repeat="e in edades | orderObjectBy:'edad'">{{ resumen | sumOfValue : 'edad_'+e.edad }}</th>
                                    <th class="center-th">{{ resumen | sumOfValue : 'cosechados' | number }}</th>
                                    <th class="center-th">{{ resumen | sumOfValue : 'procesados' | number }}</th>
                                    <th class="center-th">{{ resumen | sumOfValue : 'recusados' | number }}</th>
                                    <th class="center-th">{{ resumen | avgOfValue : 'peso' | number: 2 }}</th>
                                    <th class="center-th">{{ resumen | avgOfValue : 'calibre_segunda' | number: 2 }}</th>
                                    <th class="center-th">{{ resumen | avgOfValue : 'manos' | number: 2 }}</th>
                                </tr>
                            </tfoot>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>

    <div class="portlet box green">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-gift"></i> Racimos </div>
            <div class="tools">
            </div>
        </div>
        <div class="portlet-body">
            <div class="panel-group accordion" id="accordion3">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_7" aria-expanded="false"> Análisis de Defectos </a>
                        </h4>
                    </div>
                    <div id="collapse_3_7" class="panel-collapse collapse" aria-expanded="true" style="">
                        <div class="panel-body">
                            <div class="col-md-3 pull-right">
                                <select class="form-control" ng-model="produccion.params.var_recusado" ng-init="produccion.params.var_recusado = 'cant';" ng-change="getAnalisisRecusados()">
                                    <option value="cant">CANTIDAD</option>
                                    <option value="porc">% </option>
                                </select>
                            </div>
                            <div id="table-defectos"></div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a ng-click="openGraficasAdicionales()" class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_10" aria-expanded="false"> Gráficas Adicionales </a>
                        </h4>
                    </div>
                    <div id="collapse_3_10" class="panel-collapse collapse" aria-expanded="true" style="">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12"> 
                                    <div class="col-md-2 col-md-offset-8">
                                        Lote:<br>
                                        <select class="form-control input-md" ng-model="produccion.params.lote_adicional" ng-change="openGraficasAdicionales()">
                                            <option value="">TODOS</option>
                                            <option value="{{l.lote}}" ng-repeat="l in resumen">{{ l.lote }}</option>
                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                        <button class="btn btn-primary" ng-click="openGraficasAdicionales()"> <i class="fa fa-refresh"></i> Refrescar Gráficas</button>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <fieldset>
                                    <legend>Del Día</legend>
                                    <div class="col-md-6">
                                        <div class="col-md-10">
                                            <h3>PESO RACIMOS VS MANOS POR RACIMO (KG)</h3>
                                        </div>
                                        <div class="col-md-2">
                                            
                                        </div>
                                        <div id="grafica-manos-peso" class="chart"></div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="col-md-10">
                                            <h3>PESO POR RACIMO</h3>
                                        </div>
                                        <div class="col-md-2">
                                            Grupo <br>
                                            <input type="number" class="form-control" ng-model="grupo_racimo" step="0.1">
                                        </div>
                                        <div id="grafica-peso-racimo" class="chart"></div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="col-md-10">
                                            <h3>PESO PROM POR MANO</h3>
                                        </div>
                                        <div class="col-md-2">
                                            Grupo <br>
                                            <input type="number" class="form-control" ng-model="grupo_mano" step="0.1">
                                        </div>
                                        <div id="grafica-peso-mano" class="chart"></div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="col-md-10">
                                            <h3>PESO PROM POR °CALIB</h3>
                                        </div>
                                        <div class="col-md-2">
                                            Grupo <br>
                                            <input type="number" class="form-control" ng-model="grupo_calibre" step="0.1">
                                        </div>
                                        <div id="grafica-peso-calibre" class="chart"></div>
                                    </div>
                                    
                                    <div class="col-md-12" style="margin-top : 15px;">
                                        <div class="portlet box default">
                                            <div class="portlet-title">
                                                <ul class="nav nav-tabs">
                                                    <li ng-class="{'active' : graficas_add.var == 'manos'}">
                                                        <a ng-click="graficas_add.var = 'manos'; renderCantidad()">Manos</a>
                                                    </li>
                                                    <li ng-class="{'active' : graficas_add.var == 'calibre_segunda'}">
                                                        <a ng-click="graficas_add.var = 'calibre_segunda'; renderCantidad()">Calib. 2da</a>
                                                    </li>
                                                    <li class="hide" ng-class="{'active' : graficas_add.var == 'calibre_ultima'}">
                                                        <a ng-click="graficas_add.var = 'calibre_ultima'; renderCantidad()">Calib. Ult</a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="portlet-body">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div id="grafica-cantidad-manos-calibre" class="chart"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                                <fieldset>
                                    <legend>Tendencia</legend>
                                    <div class="col-md-12">
                                        <div id="grafica-peso-mano-tendencia" class="chart"></div>
                                    </div>
                                    <div class="col-md-12">
                                        <div id="grafica-peso-calibre-tendencia" class="chart"></div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_4" aria-expanded="false"> Base de Datos Racimos </a>
                        </h4>
                    </div>
                    <div id="collapse_3_4" class="panel-collapse collapse" aria-expanded="true" style="">
                        <div class="panel-body">
                            <div class="btn-group pull-right" style="margin-right: 5px;">
                                <button class="btn dark btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false"> 
                                    Exportar <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-right">
                                    <li>
                                        <a href="javascript:;" ng-click="exportPrint('registros')"> Imprimir </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;" ng-click="exportExcel('registros')">Excel</a>
                                    </li>
                                </ul>
                            </div>
                            <button class="btn red-thunderbird pull-right" style="height: 30px;" ng-click="eliminar()">Eliminar</button>
                            <button class="btn green-jungle pull-right" ng-show="editing" style="height: 30px;" ng-click="guardar()">Guardar</button>
                            <br>
                            <div class="table-responsive table-scrollable">
                                <table class="table table-striped table-bordered table-hover" id="edit_registros">
                                    <thead>
                                        <tr role="row" class="heading">
                                            <th class="center-th anchoTable" ng-click="setOrderTable('id')"> ID </th>
                                            <th class="center-th anchoTable" style="min-width: 120px;" style="min-width: 70px;" ng-click="setOrderTable('fecha')"> Fecha </th>
                                            <th class="center-th anchoTable" ng-click="setOrderTable('hora')"> Hora </th>
                                            <th class="center-th anchoTable" ng-click="setOrderTable('cuadrilla')"> Cuadrilla  </th>
                                            <th class="center-th anchoTable" ng-click="setOrderTable('lote')"> Lote </th>
                                            <th class="center-th anchoTable" ng-click="setOrderTable('edad')"> Edad </th>
                                            <th class="center-th anchoTable" ng-click="setOrderTable('peso')"> LB </th>
                                            <th class="center-th anchoTable" ng-click="setOrderTable('manos')"> Manos </th>
                                            <th class="center-th anchoTable" ng-click="setOrderTable('calibre_segunda')"> Calibre Segunda </th>
                                            <!--<th class="center-th anchoTable" ng-click="setOrderTable('calibre_ultima')"> Calibre Ultima </th>-->
                                            <th class="center-th anchoTable" ng-click="setOrderTable('dedos')"> Dedos </th>
                                            <th class="center-th anchoTable" style="min-width: 100px;" ng-click="setOrderTable('tipo')"> Tipo </th>
                                            <th class="center-th anchoTable" ng-click="setOrderTable('causa')"> Causa </th>
                                        </tr>
                                        <tr>
                                            <th><input type="text" class="form-control input-filter" ng-model="searchFilter.id" /></th>
                                            <th><input type="text" class="form-control input-filter" ng-model="searchFilter.fecha" /></th>
                                            <th><input type="text" class="form-control input-filter" ng-model="searchFilter.hora" /></th>
                                            <th><input type="text" class="form-control input-filter" ng-model="searchFilter.cuadrilla" /></th>
                                            <th><input type="text" class="form-control input-filter" ng-model="searchFilter.lote" /></th>
                                            <th><input type="text" class="form-control input-filter" ng-model="searchFilter.edad" /></th>
                                            <th><input type="text" class="form-control input-filter" ng-model="searchFilter.peso" /></th>
                                            <th><input type="text" class="form-control input-filter" ng-model="searchFilter.manos" /></th>
                                            <th><input type="text" class="form-control input-filter" ng-model="searchFilter.calibre_segunda" /></th>
                                            <!--<th><input type="text" class="form-control input-filter" ng-model="searchFilter.calibre_ultima" /></th>-->
                                            <th><input type="text" class="form-control input-filter" ng-model="searchFilter.dedos" /></th>
                                            <th><input type="text" class="form-control input-filter" ng-model="searchFilter.tipo" /></th>
                                            <th><input type="text" class="form-control input-filter" ng-model="searchFilter.causa" /></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr class="{{ row.delete ? 'delete' : '' }}" 
                                            data-id="{{row.id}}" 
                                            data-marca="{{row.marca}}" 
                                            data-index="{{$index}}"
                                            ng-dblclick="edit(row)"
                                            ng-repeat="row in resultValue = (registros | filter: searchFilter | orderObjectBy : searchTable.orderBy : searchTable.reverse | startFrom: searchTable.startFrom | limitTo : searchTable.limit)">
                                            <td class="anchoTable">
                                                <input type="checkbox" ng-model="row.delete" />
                                                {{ row.id }}
                                            </td>
                                            <td class="anchoTable">{{ row.fecha }}</td>
                                            <td class="anchoTable">{{ row.hora }}</td>
                                            <td class="anchoTable">
                                                <span ng-show="!row.editing">{{row.cuadrilla}}</span>
                                                <input type="text" class="form-control input-sm" ng-if="row.editing" ng-model="row.cuadrilla"/>
                                            </td>
                                            <td class="anchoTable">
                                                <span ng-show="!row.editing">{{ row.lote }}</span>
                                                <input type="text" class="form-control input-sm" ng-if="row.editing" ng-model="row.lote"/>
                                            </td>
                                            <td class="anchoTable {{ row.class }}">
                                                <span ng-show="!row.editing">{{ row.edad }}</span>
                                                <input type="text" class="form-control input-sm" ng-if="row.editing" ng-model="row.edad"/>
                                            </td>
                                            <td class="anchoTable">
                                                <span ng-show="!row.editing">{{ (row.peso > 0) ? (row.peso  | number : 2) : '' }}</span>
                                                <input type="text" class="form-control input-sm" ng-if="row.editing" ng-model="row.peso"/>
                                            </td>
                                            <td class="anchoTable">
                                                <span ng-show="!row.editing">{{ (row.manos > 0) ? (row.manos  | number : 2) : '' }}</span>
                                                <input type="text" class="form-control input-sm" ng-if="row.editing" ng-model="row.manos"/>
                                            </td>
                                            <td class="anchoTable">
                                                <span ng-show="!row.editing">{{ (row.calibre_segunda > 0) ? (row.calibre_segunda  | number : 2) : '' }}</span>
                                                <input type="text" class="form-control input-sm" ng-if="row.editing" ng-model="row.calibre"/>
                                            </td>
                                            <!--<td class="anchoTable">
                                                <span ng-show="!row.editing">{{ (row.calibre_ultima > 0) ? (row.calibre_ultima  | number : 2) : '' }}</span>
                                                <input type="text" class="form-control input-sm" ng-if="row.editing" ng-model="row.calibre"/>
                                            </td>-->
                                            <td class="anchoTable">
                                                <span ng-show="!row.editing">{{ (row.dedos > 0) ? (row.dedos  | number : 2) : '' }}</span>
                                                <input type="text" class="form-control input-sm" ng-if="row.editing " ng-model="row.dedos"/>
                                            </td>
                                            <td class="anchoTable">
                                                <span ng-show="!row.editing">{{ row.tipo == 'RECU' ? 'RECU' : row.tipo }}</span>
                                                <select class="form-control" ng-model="row.tipo" ng-if="row.editing">
                                                    <option value="PROC">PROC</option>
                                                    <option value="RECUSADO">RECU</option>
                                                </select>
                                            </td>
                                            <td class="anchoTable">
                                                <span ng-show="!row.editing && row.tipo == 'RECU'">{{ row.causa }}</span>
                                                <input type="text" class="form-control input-sm" ng-if="row.editing && row.tipo == 'RECU'" ng-model="row.causa"/>
                                            </td>
                                        </tr>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td>Total: {{ (resultValue | countOfValue:'id') | number }}</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>{{ (resultValue | avgOfValue:'edad') | number : 2 }}</td>
                                            <td>{{ (resultValue | sumOfValue:'peso') | number : 2 }}</td>
                                            <td>{{ (resultValue | avgOfValue:'manos') | number : 2 }}</td>
                                            <td>{{ (resultValue | avgOfValue:'calibre_segunda') | number : 2 }}</td>
                                            <!--<td>{{ (resultValue | sumOfValue:'calibre_ultima') | number : 2 }}</td> -->
                                            <td>{{ (resultValue | sumOfValue:'dedos') | number : 2 }}</td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td colspan="12">
                                                <div class="col-md-5 col-sm-12">
                                                    Página {{ (searchTable.actual_page | num) || 0 }} de {{ searchTable.numPages }}
                                                </div>
                                                <div class="col-md-7 col-sm-12">
                                                    <div class="pull-left">
                                                        <a class="btn btn-sm default prev" ng-click="prev(registros)"><</a>
                                                        <input type="text" maxlength="5" class="pagination-panel-input form-control input-sm input-inline input-mini" ng-model="searchTable.actual_page" readonly/>
                                                        <a class="btn btn-sm default next" ng-click="next(registros)">></a>

                                                        <select class="input-sm pull-right" style="color: #555;" ng-model="searchTable.limit" ng-change="searchTable.changePagination()">
                                                            <option value="{{ registros.length }}">TODOS</option>
                                                            <option ng-repeat="opt in searchTable.optionsPagination" value="{{ opt > 0 ? opt : '' }}" ng-selected="opt == searchTable.limit">{{ opt > 0 ? opt : 'TODOS' }}</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>

                                <table class="table table-striped table-bordered table-hover" style="display: none;" id="registros">
                                    <thead>
                                        <tr role="row" class="heading">
                                            <th class="center-th anchoTable"> ID </th>
                                            <th class="center-th anchoTable" style="min-width: 120px;" style="min-width: 70px;"> Fecha </th>
                                            <th class="center-th anchoTable"> Hora </th>
                                            <th class="center-th anchoTable"> Cuadrilla  </th>
                                            <th class="center-th anchoTable"> Lote </th>
                                            <th class="center-th anchoTable"> Edad </th>
                                            <th class="center-th anchoTable"> Cinta </th>
                                            <th class="center-th anchoTable"> LB </th>
                                            <th class="center-th anchoTable"> Manos </th>
                                            <th class="center-th anchoTable"> Calibre Segunda </th>
                                            <th class="center-th anchoTable"> Calibre Ultima </th>
                                            <th class="center-th anchoTable"> Dedos </th>
                                            <th class="center-th anchoTable" > Tipo </th>
                                            <th class="center-th anchoTable"> Causa </th>
                                        </tr>
                                    <tbody>
                                        <tr ng-repeat="row in resultValue = (registros | orderObjectBy : searchTable.orderBy : searchTable.reverse | startFrom: searchTable.startFrom | limitTo : searchTable.limit)">
                                            <td class="anchoTable">{{ row.id }}</td>
                                            <td class="anchoTable">{{ row.fecha }}</td>
                                            <td class="anchoTable">{{ row.hora }}</td>
                                            <td class="anchoTable">{{row.cuadrilla}}</td>
                                            <td class="anchoTable">{{ row.lote }}</td>
                                            <td class="anchoTable">{{ row.edad }}</td>
                                            <td class="anchoTable">{{ row.cinta }}</td>
                                            <td class="anchoTable">{{ (row.peso > 0) ? (row.peso  | number : 2) : '' }}</td>
                                            <td class="anchoTable">{{ (row.manos > 0 && row.tipo != 'RECU') ? (row.manos  | number : 2) : '' }}</td>
                                            <td class="anchoTable">{{ (row.calibre_segunda > 0 && row.tipo != 'RECU') ? (row.calibre_segunda  | number : 2) : '' }}</td>
                                            <td class="anchoTable">{{ (row.calibre_ultima > 0 && row.tipo != 'RECU') ? (row.calibre_ultima  | number : 2) : '' }}</td>
                                            <td class="anchoTable">{{ (row.dedos > 0 && row.tipo != 'RECU') ? (row.dedos  | number : 2) : '' }}</td>
                                            <td class="anchoTable">{{ row.tipo == 'RECU' ? 'RECU' : row.tipo  }}</td>
                                            <td class="anchoTable">{{ row.causa }}</td>
                                        </tr>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td>Total:</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>{{ (resultValue | avgOfValue:'edad') | number : 2 }}</td>
                                            <td></td>
                                            <td>{{ (resultValue | sumOfValue:'peso') | number : 2 }}</td>
                                            <td>{{ (resultValue | sumOfValue:'manos') | number : 2 }}</td>
                                            <td>{{ (resultValue | sumOfValue:'calibre_segunda') | number : 2 }}</td>
                                            <td>{{ (resultValue | sumOfValue:'calibre_ultima') | number : 2 }}</td>
                                            <td>{{ (resultValue | sumOfValue:'dedos') | number : 2 }}</td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<script src="componentes/FilterableSortableTable.js"></script>
<script src="assets/global/plugins/FileSaver.min.js"></script>