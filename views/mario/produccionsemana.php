<style>
	table > th,td{
		text-align: center !important;
	}
	.center-th {
		text-align: center !important;
	}
	.left-td {
		text-align: left !important;
		padding-left: 10px !important;
	}
    .right-td {
        text-align: right !important;
		padding-right: 10px !important;
    }
	.alginLeft {
		text-align: left !important;
		padding-top: 20px;
	}
	.form-control {
        border-radius : 3px !important;
    }
    .btn {
        border-radius : 3px !important;
    }
    .portlet {
        border-radius : 3px !important;
    }
</style>
<div ng-controller="produccion" ng-cloak>
	<h3 class="page-title"> 
          Producción
     </h3>
     <div class="page-bar" ng-init="init()">
         <ul class="page-breadcrumb">
             <li>
                 <i class="icon-home"></i>
                 <a>Producción</a>
                 <i class="fa fa-angle-right"></i>
             </li>
         </ul>
         <div class="page-toolbar">
            <label for="">
                Año : 
            </label>
            <select ng-change="changeYear()" name="year" id="year" ng-model="filters.year" style="margin: 2px;height: 36px;">
                <option ng-repeat="value in anios" value="{{value}}" ng-selected="value == filters.year">{{value}}</option>
            </select>
        </div>
    </div>
    <?php include("./views/tags_produccion_".$this->session->agent_user.".php");?>  
	<div id="contenedor" class="div2">
		<div class="row">
			<div class="col-md-6 hide">
				<div class="portlet box green-haze">
					<div class="portlet-title">
					    <span class="caption">Resumen</span>
					    <div class="tools">
				            <label>Hectareas: <input type="number" ng-model="hectareas" style="color: #000;" ng-init="hectareas = 94.84;" readonly/></label>
				        </div> 
					</div>
					<div class="portlet-body" id="resumen-acumulado">
						<div class="table-responsive">
							<table id="table_racimos" class="table table-striped table-bordered table-hover">
								<thead>
                                    <tr role="row" class="heading">
                                        <th width="{{withTable}}" class="center-th"> Detalle </th>
                                        <th width="{{withTable}}" class="center-th"> Acumulado  </th>
                                        <th width="{{withTable}}" class="center-th"> Por Ha Sem  </th>
                                        <th width="{{withTable}}" class="center-th"> Por Ha Año  </th>
                                    </tr>
                                    <tr role="row" class="heading subtittleTr">
                                        <th width="{{withTable}}" class="center-th"> <span class="Titlespan">Racimos</span> </th>
                                        <th width="{{withTable}}" class="center-th">  </th>
                                        <th width="{{withTable}}" class="center-th">  </th>
                                        <th width="{{withTable}}" class="center-th">  </th>
                                    </tr>
								</thead>
								<tbody>
									<tr ng-repeat="(key, row) in resumen.racimos">
										<td width="{{withTable}}" class="left-td">{{ key | uppercase }}</td>
										<td width="{{withTable}}">{{ (row.acumulado > 0) ? (row.acumulado | number) : '' }}</td>
										<td width="{{withTable}}" class="right-td">{{ (row.ha_semana > 0) ? (row.ha_semana | number: 2) : '' }}</td>
										<td width="{{withTable}}" class="right-td">{{ (row.ha_semana > 0) ? (row.ha_semana * 52 | number: 2) : '' }}</td>
									</tr>
									<tr role="row" class="heading subtittleTr">
										<th width="{{withTable}}" class="center-th"> <span class="Titlespan">Cajas</span> </th>
										<th width="{{withTable}}" class="center-th">  </th>
										<th width="{{withTable}}" class="center-th">  </th>
										<th width="{{withTable}}" class="center-th">  </th>
									</tr>
                                    <tr ng-click="expandCajas = !expandCajas">
                                        <td width="{{withTable}}" class="left-td"> <b>CONVERTIDAS</b> </td>
					                    <td width="{{withTable}}" class="center-th"> {{ (resumen.total['TOTAL CONVERTIDAS'].acumulado > 0) ? (resumen.total['TOTAL CONVERTIDAS'].acumulado) : '' }} </td>
					                    <td width="{{withTable}}" class="right-td"> {{ (resumen.total['TOTAL CONVERTIDAS'].ha_semana > 0) ? (resumen.total['TOTAL CONVERTIDAS'].ha_semana | number: 2) : '' }}</td>
					                    <td width="{{withTable}}" class="right-td"> {{ (resumen.total['TOTAL CONVERTIDAS'].ha_semana > 0) ? (resumen.total['TOTAL CONVERTIDAS'].ha_semana * 52 | number: 2) : '' }} </td>
                                    </tr>
									<tr ng-show="expandCajas" ng-repeat="row in resumen.cajas | orderByCajas : 'id'">
					                    <td width="{{withTable}}" class="left-td"> {{ row.id | uppercase }} </td>
					                    <td width="{{withTable}}" class="center-th"> {{ (row.acumulado > 0) ? (row.acumulado) : '' }} </td>
					                    <td width="{{withTable}}" class="right-td"> {{ (row.ha_semana > 0) ? (row.ha_semana | number: 2) : '' }}</td>
					                    <td width="{{withTable}}" class="right-td"> {{ (row.ha_semana > 0) ? (row.ha_semana * 52 | number: 2) : '' }} </td>
					                </tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="portlet box green-haze">
					<div class="portlet-title">
					    <span class="caption">Edad Promedio Semana</span>
					    <!-- <div class="tools">
				            <a href="javascript:;" class="expand" data-original-title="Expandir/Contraer" title=""> </a>
				        </div> -->
					</div>
					<div class="portlet-body">
						<div id="edad_promedio" style="height:350px;"></div>
					</div>
				</div>
			</div>
		</div>
		<div class="row ">
			<div class="col-md-12">
                <div class="portlet box green-haze">
                    <div class="portlet-title">
                        <span class="caption">Reporte de Producción</span>
                        <div class="tools">
                            <a href="javascript:;" class="expand" data-original-title="Expandir/Contraer" title=""> </a>
                        </div>
                    </div>
					<div class="portlet-body" style="display: hidden">
                        <div class="table-responsive">
                            <div id="reporte-produccion"></div>
                        </div>
                    </div>
                </div>

				<div class="portlet box green-haze" id="graficas">
					<div class="portlet-title">
					    <span class="caption">Gráfica de Correlación</span>
					    <div class="tools">
                            <div class="col-md-6">
                                <a href="javascript:;" class="btn btn-default {{ filters.type1 == 'line' ? 'pressed' : '' }}" ng-click="toggleLineBar(1, 'line')"><i class="fa fa-line-chart"></i></a>
                                <a href="javascript:;" class="btn btn-default {{ filters.type1 == 'bar' ? 'pressed' : '' }}" ng-click="toggleLineBar(1, 'bar')"><i class="fa fa-bar-chart"></i></a>
                                <div class="btn-group bootstrap-select bs-select">
                                    <button type="button" class="btn dropdown-toggle btn-default" data-toggle="dropdown" role="button" title="Mustard" aria-expanded="false">
                                        <span class="filter-option pull-left">{{ filters.var1 | uppercase }}</span>&nbsp;
                                        <span class="bs-caret"><span class="caret"></span></span>
                                    </button>
                                    <div class="dropdown-menu open" role="combobox" style="max-height: 357px; overflow: hidden; min-height: 105px;">
                                        <ul class="dropdown-menu inner" role="listbox" aria-expanded="false" style="max-height: 355px; overflow-y: auto; min-height: 103px;">
                                            <li class="dropdown-header" data-optgroup="{{ $index+1 }}" ng-repeat-start="(key, values) in variables"><span class="text"><b>{{ key | uppercase }}</b></span></li>
                                            <li data-original-index="0" data-optgroup="1" class="{{ selected(val) }}" ng-repeat-end="" ng-repeat="val in values" ng-if="val != filters.var2" ng-click="filters.var1 = val; initGraficaVariables();">
                                                <a tabindex="0" class="opt  " style="" data-tokens="null" role="option" aria-disabled="false" aria-selected="true">
                                                    <span class="text">{{ val }}</span>
                                                    <span class="fa fa-check check-mark"></span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <select class="bs-select" tabindex="-98" ng-model="filters.var1">
                                        <optgroup ng-repeat="(key, values) in variables" label="{{ key | uppercase }}">
                                            <option ng-repeat="val in values" ng-if="val != filters.var2">{{val}}</option>
                                        </optgroup>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <a href="javascript:;" class="btn btn-default {{ filters.type2 == 'line' ? 'pressed' : '' }}" ng-click="toggleLineBar(2, 'line')"><i class="fa fa-line-chart"></i></a>
                                <a href="javascript:;" class="btn btn-default {{ filters.type2 == 'bar' ? 'pressed' : '' }}" ng-click="toggleLineBar(2, 'bar')"><i class="fa fa-bar-chart"></i></a>

                                <div class="btn-group bootstrap-select bs-select">
                                    <button type="button" class="btn dropdown-toggle btn-default" data-toggle="dropdown" role="button" title="Mustard" aria-expanded="false">
                                        <span class="filter-option pull-left">{{ filters.var2 | uppercase }}</span>&nbsp;
                                        <span class="bs-caret"><span class="caret"></span></span>
                                    </button>
                                    <div class="dropdown-menu open" role="combobox" style="max-height: 357px; overflow: hidden; min-height: 105px;">
                                        <ul class="dropdown-menu inner" role="listbox" aria-expanded="false" style="max-height: 355px; overflow-y: auto; min-height: 103px;">
                                            <li class="dropdown-header" data-optgroup="{{ $index+1 }}" ng-repeat-start="(key, values) in variables"><span class="text"><b>{{ key | uppercase }}</b></span></li>
                                            <li data-original-index="0" data-optgroup="1" class="{{ selected(val) }}" ng-repeat-end="" ng-repeat="val in values" ng-if="val != filters.var1" ng-click="filters.var2 = val; initGraficaVariables();">
                                                <a tabindex="0" class="opt  " style="" data-tokens="null" role="option" aria-disabled="false" aria-selected="true">
                                                    <span class="text">{{ val }}</span>
                                                    <span class="fa fa-check check-mark"></span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <select class="bs-select" tabindex="-98" ng-model="filters.var2">
                                        <optgroup ng-repeat="(key, values) in variables" label="{{ key | uppercase }}">
                                            <option ng-repeat="val in values" ng-if="val != filters.var1">{{val}}</option>
                                        </optgroup>
                                    </select>
                                </div>
                            </div>
					        <!--<select style="float:right; font-size:100%; width:125px; height: 35px;" class="form-control form-filter input-sm" ng-model="filters.var2" ng-change="initGraficaVariables()">
								<option ng-repeat="va in variables" ng-if="va != filters.var1">{{ va }}</option>
							</select>
                            <select style="float:right; font-size:100%; width:125px; height: 35px;" class="form-control form-filter input-sm" ng-model="filters.var1" ng-change="initGraficaVariables()">
								<option ng-repeat="va in variables" ng-if="va != filters.var2">{{ va }}</option>
							</select>-->
						</div>
					</div>
					<div class="portlet-body">
						<!--<div class="row">
							<div class="col-md-12">
								<div id="racimos" style="height:500px;"></div>
							</div>
						</div>-->
						<div class="row">
							<div class="col-md-12">
								<div id="variables" style="height:500px;"></div>
							</div>
							<!--<div class="col-md-6">
								<div id="manos" style="height:500px;"></div>
							</div>-->
						</div>
						<div class="row hide">
							<div class="col-md-6">
								<div id="calibracion" style="height:500px;"></div>
							</div>
							<!--<div class="col-md-6">
								<div id="dedo" style="height:500px;"></div>
							</div>-->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script src="componentes/FilterableSortableTable.js"></script>