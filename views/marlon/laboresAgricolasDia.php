<style>
    .pointer {
        cursor : pointer;
    }
    .main-row {
        border: 2px solid #d6d6d6;
        background-color: #dfdfdf87;
    }
    .chart {
        height : 400px;
    }
    select.input-sm {
        color : black;
    }
</style>

<?php 
    $session = Session::getInstance();
?>

<div ng-controller="controller">
    <h3 class="page-title"> 
        Reporte Día
    </h3>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <i class="icon-home"></i>
                <a>Labores</a>
                <i class="fa fa-angle-right"></i>
             </li>
        </ul>
        <div class="page-toolbar">
            
        </div>
    </div>

    <div class="row">
        <div id="tags" class="col-md-12">
            <?php include("./views/{$session->agent_user}/tags_labores_agricolas_dia.php") ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet box green-haze">
                <div class="portlet-title">
                    <div class="caption">
                        <span class="caption-subject bold uppercase">Mapa. Geolocalización</span>
                    </div>
                </div>
                <div class="portlet-body">

                </div>
            </div>
        </div>
    </div>  

    <div class="row">
        <div class="col-md-7">
            <div class="portlet box green-haze">
                <div class="portlet-title">
                    <div class="caption">
                        <span class="caption-subject bold uppercase">Tabla Resumen</span>
                    </div>
                    <div class="tools">
                        <span class="badge badge-warning" title="Las columnas es el total de muestras"><i class="fa fa-info"></i></span>
                    </div>
                </div>
                <div class="portlet-body text-right">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>LABOR</th>
                                    <th ng-repeat="(i, index) in muestras">
                                        
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat-start="row in tabla_resumen" ng-click="row.expanded = !row.expanded" class="pointer main-row">
                                    <td>{{ row.nombre }}</td>
                                    <td ng-repeat="(i, index) in muestras"></td>
                                </tr>
                                <tr ng-repeat-end ng-repeat="subrow in row.detalle" ng-show="row.expanded">
                                    <td>{{ subrow.causa }}</td>
                                    <td ng-repeat="(i, index) in muestras">{{ subrow['muestra_'+(i+1)] }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-5">
            <div class="portlet box green-haze">
                <div class="portlet-title">
                    <div class="caption">
                        <span class="caption-subject bold uppercase">Causas</span>
                    </div>
                    <div class="actions">
                        <select class="input-sm" ng-change="refreshPieCausas()" ng-model="filters.pieCausa.lote">
                            <option value="">Lote</option>
                            <option value="{{ l.idLote }}" ng-repeat="l in pieCausa.lotes" ng-selected="l.idLote == filters.pieCausa.lote">{{ l.nombre }}</option>
                        </select>
                        <select class="input-sm" ng-change="refreshPieCausas()" ng-model="filters.pieCausa.labor">
                            <option value="">Labor</option>
                            <option value="{{ l.idLabor }}" ng-repeat="l in pieCausa.labores" ng-selected="l.idLabor == filters.pieCausa.labor">{{ l.nombre }}</option>
                        </select>
                        <button class="btn btn-primary btn-sm" ng-click="clearPieCausa()" title="Restablecer filtros">
                            <i class="fa fa-refresh"></i>
                        </button>
                    </div>
                </div>
                <div class="portlet-body" id="pie-causas-container">
                    <div id="pie-causas" class="chart"></div>
                </div>
            </div>
        </div>
    </div> 

    <div class="row">
        <div class="col-md-6">
            <div class="portlet box green-haze">
                <div class="portlet-title">
                    <div class="caption">
                        <span class="caption-subject bold uppercase">% Calidad por Labor</span>
                    </div>
                    <div class="actions">
                        <select class="input-sm">
                            <option value="">Lote</option>
                        </select>
                    </div>
                </div>
                <div class="portlet-body">
                    <div id="chart-calidad-by-labor" class="chart"></div>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="portlet box green-haze">
                <div class="portlet-title">
                    <div class="caption">
                        <span class="caption-subject bold uppercase">% Calidad por Lote</span>
                    </div>
                    <div class="actions">
                        <select class="input-sm">
                            <option value="">Labor</option>
                        </select>
                    </div>
                </div>
                <div class="portlet-body">
                    <div id="chart-calidad-by-lote" class="chart"></div>
                </div>
            </div>
        </div>
    </div>  
</div>