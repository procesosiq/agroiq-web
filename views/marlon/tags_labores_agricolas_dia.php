
<div class="row">
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat2 ">
            <div class="display">
                <div class="number">
                    <h3 class="font-{{ revision(tags.prom_general) }}">
                        <span class="counter_tags" data-value="{{tags.prom_general}}">0</span>
                        <small class="font-{{ revision(tags.prom_general) }}">%</small>
                    </h3>
                    <small>% PROM GENERAL</small>
                </div>
                <div class="icon">
                    <i class="icon-pie-chart"></i>
                </div>
            </div>
            <div class="progress-info">
                <div class="progress">
                    <span style="width: {{tags.prom_general}}%;" class="progress-bar progress-bar-success {{ revision(tags.prom_general) }}">
                        <span class="sr-only">{{tags.prom_general}}% progress</span>
                    </span>
                </div>
                <div class="status">
                    <div class="status-title"></div>
                    <div class="status-number"> {{tags.prom_general}}% </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat2 ">
            <div class="display">
                <div class="number">
                    <h3 class="font-{{ revision(tags.promedio) }}">
                        <span class="counter_tags" data-value="{{tags.promedio}}">0</span>
                        <small class="font-{{ revision(tags.promedio) }}">%</small>
                    </h3>
                    <small>% PROM</small>
                </div>
                <div class="icon">
                    <i class="icon-pie-chart"></i>
                </div>
            </div>
            <div class="progress-info">
                <div class="progress">
                    <span style="width: {{tags.promedio}}%;" class="progress-bar progress-bar-success {{ revision(tags.promedio) }}">
                        <span class="sr-only">{{tags.promedio}}% progress</span>
                    </span>
                </div>
                <div class="status">
                    <div class="status-title"></div>
                    <div class="status-number"> {{tags.promedio}}% </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat2 ">
            <div class="display">
                <div class="number">
                    <h3 class="font-{{ revision(tags.mas_alto) }}">
                        <span class="counter_tags" data-value=" {{tags.mas_alto}}">0</span>
                        <small class="font-{{ revision(tags.mas_alto) }}"></small>
                    </h3>
                    <small>% MÁS ALTO</small>
                </div>
                <div class="icon">
                    <i class="icon-bar-chart"></i>
                </div>
            </div>
            <div class="progress-info">
                <div class="progress">
                    <span style="width:  {{tags.mas_alto}}%;" class="progress-bar progress-bar-success {{ revision(tags.mas_alto) }}">
                        <span class="sr-only"> {{tags.mas_alto}}% change</span>
                    </span>
                </div>
                <div class="status">
                    <div class="status-title"></div>
                    <div class="status-number">  {{tags.mas_alto}}% </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat2 ">
            <div class="display">
                <div class="number">
                    <h3 class="font-{{ revision(tags.mas_bajo) }}">
                        <span class="counter_tags" data-value=" {{tags.mas_bajo}}">0</span>
                        <small class="font-{{ revision(tags.mas_bajo) }}"></small>
                    </h3>
                    <small>% MÁS BAJO</small>
                </div>
                <div class="icon">
                    <i class="icon-pie-chart"></i>
                </div>
            </div>
            <div class="progress-info">
                <div class="progress">
                    <span style="width: {{tags.mas_bajo}}%;" class="progress-bar progress-bar-success  {{ revision(tags.mas_bajo) }}">
                        <span class="sr-only">0% grow</span>
                    </span>
                </div>
                <div class="status">
                    <div class="status-title"></div>
                    <div class="status-number"> {{tags.mas_bajo}}% </div>
                </div>
            </div>
        </div>
    </div>
</div>