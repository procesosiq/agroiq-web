<style>
    .bold {
        font-weight: bold;
        color : red;
    }
    .charts {
        height : 400px;
    }
    .card {
        /* Add shadows to create the "card" effect */
        box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
        transition: 0.3s;
        margin-bottom: 15px;
    }

    /* On mouse-over, add a deeper shadow */
    .card:hover {
        box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
    }

    /* Add some padding inside the card container */
    .container {
        padding: 2px 16px;
    }
</style>
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.0.3/socket.io.js"></script>-->
<div ng-controller="controller" ng-cloak>
	<h3 class="page-title"> 
          Análisis Sensorial
     </h3>
     <div class="page-bar">
         <ul class="page-breadcrumb">
             <li>
                 <i class="icon-home"></i>
                 <a>Análisis Sensorial</a>
                 <i class="fa fa-angle-right"></i>
             </li>
         </ul>
         <div class="page-toolbar" id="filters">
		 	<label for="">
                Finca : 
            </label>
            <select name="finca" id="finca" ng-model="filters.finca" style="margin: 2px;height: 36px;" ng-change="reloadFilters()">
            	<option value="">Todos</option>
                <option ng-repeat="finca in fincas" value="{{finca}}" ng-selected="finca == filters.finca">{{finca}}</option>
            </select>
            <label for="">
                Responsable : 
            </label>
            <select name="responsable" id="responsable" ng-model="filters.responsable" style="margin: 2px;height: 36px;" ng-change="reloadFilters()">
            	<option value="">Todos</option>
                <option ng-repeat="responsable in responsables" value="{{responsable}}" ng-selected="responsable == filters.responsable">{{responsable}}</option>
            </select>
            <label for="">
                Código : 
            </label>
            <select name="codigo" id="codigo" ng-model="filters.codigo" style="margin: 2px;height: 36px;" ng-change="getDataAnalisis()">
                <option ng-repeat="codigo in codigos" value="{{codigo}}" ng-selected="codigo == filters.codigo">{{codigo}}</option>
            </select>
            <label for="">
                Fecha : 
            </label>
            <select name="fecha" id="fecha" ng-model="filters.fecha" style="margin: 2px;height: 36px;" ng-change="reloadFilters()">
            	<option value="">Todos</option>
                <option ng-repeat="fecha in fechas" value="{{fecha}}" ng-selected="fecha == filters.fecha">{{fecha}}</option>
            </select>
        </div>
    </div>

    <div class="portlet box green">
        <div class="portlet-title">
            <div class="caption">

            </div>
            <div class="tools">
            </div>
        </div>
        <div class="portlet-body">
            <div class="row">
                <div class="col-md-4">
                    <div class="table-scrollable">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>VARIABLE</th>
                                    <th>VALOR</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="row in data">
                                    <td>{{ row.name }}</td>
                                    <td>{{ row.value | number: 0 }}</td>
                                </tr>
                                <tr>
                                    <th>TOTAL</th>
                                    <th>{{ data | sumOfValue : 'value' }}</th>
                                </tr>
                                <tr>
                                    <th>PROMEDIO</th>
                                    <th>{{ (data | sumOfValue : 'value') / 7 | number: 2 }}</th>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-md-8">
                    <div id="grafica-radar" class="charts"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="portlet box green">
        <div class="portlet-title">
            <div class="caption">
                Fotos
            </div>
            <div class="tools">
            </div>
        </div>
        <div class="portlet-body">
            <div class="row">
                <div class="col-md-12">
                    <p>{{ observaciones }}</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3" ng-repeat="imagen in imagenes">
                    <div class="card">
                        <img src="http://app.procesos-iq.com/json/marun/image/analisisCacao/{{ imagen }}" alt="Imagen no disponible" style="width:100%; height: 170px;">
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>