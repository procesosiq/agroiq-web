<style>
    .chart {
        height : 400px;
    }
</style>
<div ng-controller="controller">
	<h3 class="page-title"> 
        Mapa Lotes
    </h3>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <i class="icon-home"></i>
                <a>Mapa Lotes</a>
                <i class="fa fa-angle-right"></i>
            </li>
        </ul>
        <div class="page-toolbar" id="filters">
            Sector:
            <select class="input-sm" ng-change="init()" ng-model="filters.sector">
                <option value="">Todos</option>
                <option value="{{s}}" ng-repeat="s in sectores">{{s}}</option>
            </select>
        </div>
    </div>

    <div class="portlet box green">
        <div class="portlet-title">
            <div class="caption">
                Mapa
            </div>
            <div class="tools">
                <a class="btn btn-primary" href="cacaoConfigLotes" style="height : 36px">Configurar</a>
            </div>
        </div>
        <div class="portlet-body">
            <div id="chart-lotes" class="chart">

            </div>
            <div id="table-data"></div>
        </div>
    </div>

</div>
<script src="componentes/FilterableSortableTable.js"></script>