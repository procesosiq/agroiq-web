<style>
    .chart {
        height : 400px;
    }
    td, th {
        text-align : center;
    }
    .card {
        /* Add shadows to create the "card" effect */
        box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
        transition: 0.3s;
        margin-bottom: 15px;
    }

    /* On mouse-over, add a deeper shadow */
    .card:hover {
        box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
    }

    /* Add some padding inside the card container */
    .container {
        padding: 2px 16px;
    }

    .main-row {
        border: 2px solid #d6d6d6;
        background-color: #dfdfdf87;
    }
    .pointer {
        cursor : pointer;
    }
    .chart {
        height : 400px;
    }
</style>

<div ng-controller="controller" ng-cloak>
    <div style="width: 100%;margin-bottom: 10px;display:table;">
        <h3 class="page-title" style="display: table-cell !important;"> 
            Reporte de Tendencia
        </h3>
    </div>

    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <i class="icon-home"></i>
                <a>Calidad</a>
                <i class="fa fa-angle-right"></i>
            </li>
        </ul>
        <div class="page-toolbar">
            Marca 
            <select class="input-sm" ng-model="filters.marca" ng-change="index()">
                <option value="">TODOS</option>
                <option value="{{marca}}" ng-repeat="marca in marcas">{{ marca }}</option>
            </select>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <ul class="nav nav-tabs">
                <li ng-class="{ 'active' : filters.period == 'semana' }">
                    <a ng-click="filters.period = 'semana'; index()">Sem</a>
                </li>
                <li ng-class="{ 'active' : filters.period == 'periodo' }">
                    <a ng-click="filters.period = 'periodo'; index()">Per</a>
                </li>
            </ul>
        </div>
        <div class="col-md-6">
            <ul class="nav nav-tabs pull-right">
                <li ng-class="{ 'active' : filters.mode == 'zona' }">
                    <a ng-click="filters.mode = 'zona'; index()">Zona</a>
                </li>
                <li ng-class="{ 'active' : filters.mode == 'finca' }">
                    <a ng-click="filters.mode = 'finca'; index()">Finca</a>
                </li>
            </ul>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet box green-haze">
                <div class="portlet-title">
                    <div class="caption">
                        <span class="caption-subject bold uppercase">General</span>
                    </div>
                    <ul class="nav nav-tabs">
                        <li ng-class="{ 'active' : filters.var == 'calidad_cluster' }">
                            <a ng-click="filters.var = 'calidad_cluster'; index()">% Cal. Cluster</a>
                        </li>
                        <li ng-class="{ 'active' : filters.var == 'calidad_dedos' }">
                            <a ng-click="filters.var = 'calidad_dedos'; index()">% Cal. Dedos</a>
                        </li>
                        <li ng-class="{ 'active' : filters.var == 'calidad_empaque' }" ng-if="config.calidad_empaque">
                            <a ng-click="filters.var = 'calidad_empaque'; index()">% Cal. Empaque</a>
                        </li>
                        <li ng-class="{ 'active' : filters.var == 'cantidad_dedos' }">
                            <a ng-click="filters.var = 'cantidad_dedos'; index()">Cant Dedos</a>
                        </li>
                        <li ng-class="{ 'active' : filters.var == 'cantidad_cluster_caja' }">
                            <a ng-click="filters.var = 'cantidad_cluster_caja'; index()">Cant Cluster</a>
                        </li>
                        <li ng-class="{ 'active' : filters.var == 'peso' }" ng-if="config.peso_prom_cluster">
                            <a ng-click="filters.var = 'peso'; index()">Peso Prom Cluster</a>
                        </li>
                    </ul>
                </div>
                <div class="portlet-body" id="principal">
                    <div>
                        <div id="chart" class="chart"></div>
                    </div>

                    <div class="table table-hover table-bordered" id="tabla-general">
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="componentes/FilterableSortableTable.js"></script>