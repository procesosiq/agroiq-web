<style>
    .bold {
        font-weight: bold;
        color : red;
    }
    .charts {
        height : 400px;
    }
    .card {
        /* Add shadows to create the "card" effect */
        box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
        transition: 0.3s;
        margin-bottom: 15px;
    }

    /* On mouse-over, add a deeper shadow */
    .card:hover {
        box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
    }

    /* Add some padding inside the card container */
    .container {
        padding: 2px 16px;
    }
</style>
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.0.3/socket.io.js"></script>-->
<div ng-controller="controller" ng-cloak>
	<h3 class="page-title"> 
        Consolidado de Cosecha
     </h3>
     <div class="page-bar">
         <ul class="page-breadcrumb">
             <li>
                 <i class="icon-home"></i>
                 <a>Consolidado de Cosecha</a>
                 <i class="fa fa-angle-right"></i>
             </li>
         </ul>
         <div class="page-toolbar" id="filters" style="display: flex; align-items: center;">
            Fecha : 
            <!--<input class="input-sm" sytle="height: 36px;" data-provide="datepicker" data-date-format="yyyy-mm-dd" ng-model="filters.fecha" ng-change="init()" readonly>-->
            <ng-calendarapp search="search"></ng-calendarapp>
        </div>
    </div>

    <div class="portlet box green">
        <div class="portlet-title">
            <div class="caption">
            </div>
            <div class="tools">
                <select ng-model="filters.unidad" class="form-control" ng-change="init()">
                    <option value="KG">KG</option>
                    <option value="LB">LB</option>
                    <option value="QQ">QQ</option>
                </select>
            </div>
        </div>
        <div class="portlet-body">
            <div id="table-data"></div>
        </div>
    </div>

</div>
<script src="componentes/FilterableSortableTable.js"></script>