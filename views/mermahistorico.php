<style>
	.accordion .panel .panel-title .accordion-toggle {
		display: block !important;
    	padding: 0px 14px !important;
	}
	.margin-5 {
		margin-bottom: 5px !important;
	}
	table > th,td{
		text-align: center !important;
	}
	.center-th {
		text-align: center !important;
	}
	.left-td {
		text-align: left !important;
		padding-left: 10px !important;
	}
	table {
		cursor: pointer !important;
	}
</style>
<div ng-controller="bonificacion" ng-cloak>
	<h3 class="page-title"> 
          Historico
     </h3>
     <div class="page-bar" ng-init="bonificacion.nocache()">
         <ul class="page-breadcrumb">
             <li>
                 <i class="icon-home"></i>
                 <a>Historico</a>
                 <i class="fa fa-angle-right"></i>
             </li>
         </ul>
         <div class="page-toolbar">          
           <select name="type" id="type" class="form-control" ng-model="historica.type" onchange="changeHistorica()">
           		<option value="ENFUNDE">ENFUNDE</option>
           		<option value="COSECHA">COSECHA</option>
           	</select>
        </div>
    </div>
    <div class="portlet box green">
		<div class="portlet-title">
		    <span class="caption">Historico</span>
		    <!-- <div class="tools">
	        </div> -->
		</div>
		<div class="portlet-body">
			<div id="historico" style="height: 400px"></div>
		</div>
	</div>
	<div class="portlet box green">
		<div class="portlet-title">
		    <span class="caption">Historico</span>
		    <div class="tools">
				<select name="semana" id="semana" class="form-control" ng-model="bonificacion.params.semana" onchange="changeweek()">
	           		<option value="">SEMANAS</option>
	           		<option ng-repeat="(key, value) in semanas" value="{{key}}">{{value}}</option>
	           	</select>
	        </div>
		</div>
		<div class="portlet-body">
			<div class="table-responsive">
				<table class="table table-striped table-bordered table-hover">
					<thead>
			                <tr role="row" class="heading">
			                    <th width="8.33%" class="center-th" ng-click="orderByField= 'lote';reverseSort = !reverseSort"> {{title.column_one}}  </th>
			                    <th width="8.33%" class="center-th" ng-click="orderByField= 'enfundador';reverseSort = !reverseSort"> {{title.column_two}}  </th>
			                    <th width="8.33%" class="center-th" ng-repeat="dia in dias" ng-bind="dia"></th>
			                    <th width="8.33%" class="center-th" ng-click="orderByField= 'total';reverseSort = !reverseSort"> Total Daños </th> 
			                    <th width="8.33%" class="center-th" ng-click="orderByField= 'exedente';reverseSort = !reverseSort"> Excedente </th>                                                  
			                    <th width="8.33%" class="center-th" ng-click="orderByField= 'usd';reverseSort = !reverseSort">  USD  </th>                                                  
			                </tr>
					</thead>
					<tbody>
						<tr ng-repeat-start="lote in data | orderObjectBy:orderByField:reverseSort" ng-click="openDetalle(lote)">
							<td class="center-td">{{lote.lote}}</td>
							<td>{{lote.enfundador}}</td>
							<td ng-repeat="dia in campos" >
								<span class="{{tagsFlags(lote.cantidad[dia])}}"></span>
								{{lote.cantidad[dia]}}
							</td>
							<td>
								<span class="{{tagsFlags(lote.total)}}"></span>
								{{lote.total}}
							</td>
							<td>
								{{lote.exedente}}
							</td>
							<td>{{lote.usd}}</td>
						</tr>	
						<!-- <tr ng-show="lote.expanded" ng-repeat-end="">
							<td colspan="{{colspan}}">
								<table class="table table-striped table-bordered table-hover">
									<thead>
						                <tr role="row" class="heading">
						                    <th width="16.66%" colspan="2" class="center-th"> Daño  </th>
						                    <th width="8.33%" class="center-th" ng-repeat="dia in dias" ng-bind="dia"></th>
						                    <th width="8.33%" class="center-th"> Total  </th>
						                    <td width="16.66%" colspan="2" class="left-td "></td>
						                </tr>
									</thead>
									<tr ng-repeat="defecto in lote.defectos">
										<td colspan="2" class="left-td ">{{defecto.defecto}}</td>
										<td ng-repeat="dia in campos" >
											<span class="{{tagsFlags(defecto.valor[dia])}}"></span>
											{{defecto.valor[dia]}}
										</td>
										<td class="center-td ">
											<span class="{{tagsFlags(defecto.total)}}"></span>
											{{defecto.total}}
										</td>
										<td colspan="2" class="center-td "></td>
									</tr>
								</table>
							</td>
						</tr> -->
					</tbody>
					<tfoot>
						<tr ng-repeat="total in totales">
							<td>Total : </td>
							<td></td>
							<td ng-repeat="dia in campos">
								{{total.cantidad[dia]}}
							</td>
							<td>{{total.defectos}}</td>
							<td>{{total.exedente}}</td>
							<td>{{total.usd}}</td>
						</tr>
					</tfoot>
				</table>
			</div>
		</div>
	</div>
</div>