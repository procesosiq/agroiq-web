<?php
	$session = Session::getInstance();
	$db = DB::getInstance($session->agent_user);
	$proceso_hoy = (int) $db->queryOne("SELECT COUNT(1) FROM fincas_dias_proceso WHERE fecha = CURRENT_DATE");
?>

<style>
	td, th {
		text-align : center;
	}
	.calendar-table td.available {
		background-color: green !important;
    	color: white !important;
	}
	.icalendar-table td.available:hover {
		background-color: #285e8e !important;
    	color: white !important;
	}
	.cursor {
		cursor : pointer;
	}
	.w3-badge, .w3-tag {
		background-color: #000;
		color: #fff;
		display: inline-block;
		padding-left: 8px;
		padding-right: 8px;
		text-align: center;
	}
	.w3-round-large {
		border-radius: 8px !important;
	}
	.w3-center {
		text-align: center!important;
	}
	.w3-padding {
		padding: 8px 16px!important;
	}
</style>

<?php 
	include_once 'modal_fincas_procesos.php';
?>

<div ng-controller="controller">
	<h3 class="page-title"> 
		Monitor
    </h3>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <i class="icon-home"></i>
                <a>Monitor</a>
                <i class="fa fa-angle-right"></i>
            </li>
        </ul>
        <div class="page-toolbar">
		 	<h4 class="text-center" style="margin-right: 10px">{{ hora }} <i class="fa fa-clock-o"></i> </h4>
        </div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="portlet box green-haze">
				<div class="portlet-title">
					<span class="caption">  </span>
					<div class="actions">
						<button id="proceso_hoy" class="btn btn-md btn-primary" ng-click="filtroProceso()">Filtrar por proceso</button>
					</div>
				</div>
				<div class="portlet-body">
					<div class="row">
						<div class="col-md-12">
							<div class="table-responsive">
								<table class="table">
									<thead>
										<tr>
											<th rowspan="2">Finca</th>
											<th colspan="3">Racimos</th>
											<th colspan="3">Cajas</th>
											<th rowspan="2">Comentarios</th>
										</tr>
										
										<tr>
											<th class="tooltips cursor" data-container="body" data-placement="bottom" data-original-title="Red">R</th>
											<th class="tooltips cursor" data-container="body" data-placement="bottom" data-original-title="Internet">I</th>
											<th class="tooltips cursor" data-container="body" data-placement="bottom" data-original-title="Datos">D</th>

											<th class="tooltips cursor" data-container="body" data-placement="bottom" data-original-title="Red">R</th>
											<th class="tooltips cursor" data-container="body" data-placement="bottom" data-original-title="Internet">I</th>
											<th class="tooltips cursor" data-container="body" data-placement="bottom" data-original-title="Datos">D</th>
										</tr>
									</thead>
									<tbody>
										<tr ng-repeat="row in data">
											<td>{{ row.finca }}</td>

											<!-- RACIMOS -->
											<td>
												<span 
													id="{{ row.racimos.uuid_red }}" 
													class="badge badge-{{ row.racimos.red == null ? 'default' : row.racimos.red ? 'success' : 'danger' }}" 
													ng-init="pulsate(row.racimos.red, row.racimos.uuid_red)"
													role="button"
													ng-click="filterUUID(row.racimos.red, row.racimos.uuid_red)">
													{{ row.racimos.red ? 'On' : 'Off' }}
												</span>
											</td>
											<td>
												<span 
													id="{{ row.racimos.uuid_internet }}" 
													class="badge badge-{{ row.racimos.internet == null ? 'default' : row.racimos.internet ? 'success' : 'danger' }}" 
													ng-init="pulsate(row.racimos.internet, row.racimos.uuid_internet)"
													role="button"
													ng-click="filterUUID(row.racimos.internet, row.racimos.uuid_internet)">
													{{ row.racimos.internet ? 'On' : 'Off' }}
												</span>
											</td>
											<td>
												<span 
													id="{{ row.racimos.uuid_datos }}" 
													class="badge badge-{{ row.racimos.datos == null ? 'default' : row.racimos.datos ? 'success' : 'danger' }}" 
													ng-init="pulsate(row.racimos.datos, row.racimos.uuid_datos)"
													role="button"
													ng-click="filterUUID(row.racimos.datos, row.racimos.uuid_datos)">
													{{ row.racimos.datos ? 'On' : 'Off' }}
												</span>
											</td>
											<!-- RACIMOS -->

											<!-- CAJAS -->
											<td>
												<span 
													id="{{ row.cajas.uuid_red }}" 
													class="badge badge-{{ row.cajas.red == null ? 'default' : row.cajas.red ? 'success' : 'danger' }}" 
													ng-init="pulsate(row.cajas.red, row.cajas.uuid_red)"
													role="button"
													ng-click="filterUUID(row.cajas.red, row.cajas.uuid_red)">
													{{ row.cajas.red ? 'On' : 'Off' }}
												</span>
											</td>
											<td>
												<span 
													id="{{ row.cajas.uuid_internet }}" 
													class="badge badge-{{ row.cajas.internet == null ? 'default' : row.cajas.internet ? 'success' : 'danger' }}" 
													ng-init="pulsate(row.cajas.internet, row.cajas.uuid_internet)"
													role="button"
													ng-click="filterUUID(row.cajas.uuid_internet, row.cajas.uuid_internet)">
													{{ row.cajas.internet ? 'On' : 'Off' }}
												</span>
											</td>
											<td>
												<span 
													id="{{ row.cajas.uuid_datos }}" 
													class="badge badge-{{ row.cajas.datos == null ? 'default' : row.cajas.datos ? 'success' : 'danger' }}" 
													ng-init="pulsate(row.cajas.datos, row.cajas.uuid_datos)"
													role="button"
													ng-click="filterUUID(row.cajas.uuid_datos, row.cajas.uuid_datos)">
													{{ row.cajas.datos ? 'On' : 'Off' }}
												</span>
											</td>
											<!-- CAJAS -->

											<td>
												<input 
													type="text" 
													class="form-control" 
													ng-blur="noedit()"
													ng-focus="edit()"
													ng-model="row.comentarios" 
													ng-change="saveComentario(row)" 
													ng-model-options="{ updateOn: 'blur' }">
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="col-md-9">
			<div class="portlet box green-haze">
				<div class="portlet-title">
					<span class="caption"> Logs </span>
				</div>
				<div class="portlet-body">
					<div class="row">
						<div class="col-md-12">

							<div class="table-responsive">
								<table class="table">
									<thead>
										<tr>
											<th>Fecha</th>
											<th class="text-left">Nombre</th>
											<th class="text-left">Descripción</th>
										</tr>
									</thead>
									<tbody>
										<tr ng-repeat="row in (logs_data | filter : filters_logs)">
											<td>{{ row.date | dateFormat }}</td>
											<td class="text-left">{{ row.name }}</td>
											<td class="text-left">{{ row.description }}</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="col-md-3">
			<div class="portlet box green-haze">
				<div class="portlet-title">
					<span class="caption"> Filtros </span>
				</div>
				<div class="portlet-body">
					<div class="row">
						<div class="col-md-12">
							<span class="w3-tag w3-padding w3-round-large w3-center bg-red-flamingo" ng-repeat="(key, value) in filters_logs" style="margin:3px">
								<a role="button" ng-click="delete(filters_logs, key)" class="w3-badge w3-round-large font-white">X</a> {{key}} : <small>{{value}}</small>
							</span>
							<div class="table-responsive">
								<table class="table">
									<thead>
										<tr>
											<th>Nombre</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td class="text-left">
												<a role="button" ng-click="delete(filters_logs, 'name')">Todos</a>
											</td>
										</tr>
										<tr ng-repeat="l in (logs_data | filter : filters_logs | getNoRepeat : 'name')">
											<td class="text-left">
												<a role="button" ng-click="filters_logs.name = l">{{l}}</a>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script src="assets/global/plugins/jquery.pulsate.min.js" type="text/javascript"></script>