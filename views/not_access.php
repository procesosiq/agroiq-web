        <!-- BEGIN PAGE LEVEL STYLES -->
        <link href="assets/pages/css/error.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL STYLES -->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Acceso restringido
                    <small>Pagina no Autorizada</small>
                </h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="/">Inicio</a>
            </li>
            <li>
                <span class="active">Acceso restrigido</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMB -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                <div class="details">
                    <h2>Oops! Te perdiste.</h2>
                    <p> No cuentas con la version de pago para este modulo del sistema.
                        <br/>
                        <a href="/"> Regresar </a> e intenda de nuevo. </p>
                    <!-- <form action="#">
                        <div class="input-group input-medium">
                            <input type="text" class="form-control" placeholder="keyword...">
                            <span class="input-group-btn">
                                <button type="submit" class="btn green">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                    </form> -->
                </div>
            </div>
        </div>