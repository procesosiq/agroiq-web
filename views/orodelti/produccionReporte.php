<style>
	td, th  {
		text-align: center;
	}
	.form-control, .input-sm, select, .btn, .portlet {
        border-radius : 3px !important;
    }
</style>
<div ng-controller="controller" ng-cloak>
	<h3 class="page-title"> 
          Reporte Producción
     </h3>
     <div class="page-bar" ng-init="produccion.nocache()">
         <ul class="page-breadcrumb">
             
         </ul>
         <div class="page-toolbar">
			<label for="">Año</label>
			<select class="input-sm" ng-model="filters.year" ng-change="changeYear()">
				<option value="{{y}}" ng-repeat="y in years" ng-selected="y == filters.year">{{y}}</option>
			</select>
			<label for="">Semana</label>
			<select class="input-sm" ng-model="filters.semana" ng-change="changeWeek()">
				<option value="{{s}}" ng-repeat="s in semanas" ng-selected="s == filters.semana">{{s}}</option>
			</select>
        </div>
    </div>
    
	<div id="contenedor" class="div2">
		<div class="row">
			<div class="col-md-12">
				<div class="portlet box green-haze">
					<div class="portlet-title">
						<span class="caption">RESUMEN CAJAS</span>
						<div class="tools">
							
						</div>
					</div>
					<div class="portlet-body">
						<div class="table-scrollable" id="div_table_2" style="padding-right: 5px;">
							<table class="table table-striped table-bordered table-hover" id="table_2">
								<thead>
									<tr>
                                        <th></th>
                                        <th ng-show="cintaSC"></th>
										<th ng-if="edades.length > 0" class="" colspan="{{ edades.length }}">EDAD</th>
										<th rowspan="2">RAC<br>COSE</th>
										<th rowspan="2">PESO PROM<br>RAC LB</th>
										<th rowspan="2">RAC<br>RECU</th>
										<th rowspan="2">RAC<br>PROC</th>
										<th></th>
										<th></th>
										<th rowspan="2">RATIO<br>COSE</th>
										<th rowspan="2">RATIO<br>PROC</th>
										<th rowspan="2">CAJAS/ha<br>PROYEC.</th>
									</tr>
									<tr>
                                        <th>LOTE</th>
                                        <th ng-show="cintaSC">N/A</th>
										<th class="{{ cintas[e] }}" ng-repeat="e in edades">{{e}}</th>
										<!-- RAC COSE -->
										<!-- PESO PROM RAC LB -->
										<!-- RAC RECU -->
										<!-- RAC PROC -->
										<th>CONV</th>
										<th>CAJAS/ha</th>
										<!-- RATIO COSE -->
										<!-- RATIO PROC -->
										<!-- CAJAS/ha PROYEC. -->
									</tr>
								</thead>
								<tbody>
									<tr ng-repeat="row in data">
                                        <td>{{ row.lote }}</td>
                                        <td ng-show="cintaSC">{{ row['edad_sc'] > 0 ? row['edad_sc'] : '' }}</td>
										<td ng-repeat="e in edades">{{ row['edad_'+e] }}</td>
										<td>{{ row.cosechados }}</td>
                                        <td>{{ row.peso_prom_racimo | number : 2 }}</td>
										<td>{{ row.recusados }}</td>
										<td>{{ row.procesados }}</td>
										<td>{{ row.convertidas | number : 2 }}</td>
										<td>{{ row.cajas_ha }}</td>
										<td>{{ row.ratio_cortado }}</td>
										<td>{{ row.ratio_procesado }}</td>
										<td>{{ row.cajas_ha_proyeccion | number }}</td>
									</tr>
								</tbody>
								<tfoot>
									<tr>
                                        <th></th>
                                        <th ng-show="cintaSC">{{ totales['edad_sc'] }}</th>
										<th ng-repeat="e in edades">{{ totales['edad_'+e] }}</th>
										<th>{{ totales.total_racimos_cosechados }}</th>
										<th>{{ data | avgOfValue : 'peso_prom_racimo' | number: 2 }}</th>
										<th>{{ totales.total_racimos_recusados }}</th>
										<th>{{ totales.total_racimos_procesados }}</th>
										<th>{{ totales.total_convertidas | number : 2 }}</th>
										<th>{{ data | avgOfValue : 'cajas_ha' | number: 2 }}</th>
										<th>{{ data | avgOfValue : 'ratio_cortado' | number: 2  }}</th>
										<th>{{ data | avgOfValue : 'ratio_procesado' | number: 2 }}</th>
										<th>{{ data | avgOfValue : 'cajas_ha_proyeccion' | number: 2 }}</th>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>

    </div>
</div>