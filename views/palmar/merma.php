<style>
    .label_chart {
        background-color: #fff;
        padding: 2px;
        margin-bottom: 8px;
        border-radius: 3px 3px 3px 3px;
        border: 1px solid #E6E6E6;
        display: inline-block;
        margin: 0 auto;
    }
    .legendLabel{
        padding: 3px !important;
    }
</style>
      <!-- BEGIN CONTENT BODY -->
<div ng-controller="informe_calidad" ng-cloak>
     <h3 class="page-title"> 
          Merma
     </h3>
     <div class="page-bar" ng-init="calidad.nocache()">
         <ul class="page-breadcrumb">
             <li>
                 <i class="icon-home"></i>
                 <a>Merma</a>
                 <i class="fa fa-angle-right"></i>
             </li>
         </ul>
        <div class="page-toolbar">
            <label for="">
                Finca : 
            </label>
            <select ng-change="cambiosFincas()" name="idFinca" id="idFinca" ng-model="calidad.params.idFinca" style="margin: 2px;height: 36px;">
                <option ng-repeat="(key, value) in fincas" value="{{key}}" ng-selected="calidad.params.idFinca == key">{{value}}</option>
            </select>
            <label for="">
                Palanca : 
            </label>
            <select ng-change="cambiosPalanca()" name="palanca" id="palanca" ng-model="calidad.params.palanca" style="margin: 2px;height: 36px;">
                <option ng-repeat="(key, value) in palancas" value="{{key}}">{{value}}</option>
            </select>
            <ng-calendarapp  search="search"></ng-calendarapp>
        </div>
     </div>

    <div class="page-bar" style="padding: 0; color: #888" ng-if="checkParams()">
        <div class="col-md-9">
            <div class="row">
                <div class="col-md-2" style="text-align: left">CLIENTE:</div>
                <div class="col-md-4">{{param_cliente}}</div>
                <div class="col-md-4" style="text-align: left">PESO REFERENCIAL:</div>
                <div class="col-md-2">{{param_peso}}</div>
            </div>
            <div class="row" style="margin-top: 12px;">
                <div class="col-md-2" style="text-align: left">MARCA:</div>
                <div class="col-md-4">{{param_marca}}</div>
                <div class="col-md-4" style="text-align: left"># CLÚSTER REF.:</div>
                <div class="col-md-2">{{param_cluster}}</div>
            </div>
        </div>
        <div class="col-md-3"><img ng-src="{{param_logo}}" height="50" /></div>
    </div>

     <?php include("./views/tags_merma_".$this->session->agent_user.".php");?>           
     <div id="reportes_all" ng-include="calidad.templatePath[calidad.step]" onload="load()">

     </div>  
</div>

<script src="https://unpkg.com/react@15/dist/react.js"></script>
<script src="https://unpkg.com/react-dom@15/dist/react-dom.js"></script>
<script src="componentes/FilterableSortableTable.js"></script>