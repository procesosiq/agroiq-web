<style>
    td, th {
        text-align: center;
    }
    .charts {
        height : 400px;
    }
</style>
      
<div ng-controller="perchas" ng-cloak>
     <h3 class="page-title"> 
          CALIDAD EN PERCHAS
     </h3>
     <div class="page-bar" ng-init="service.all()">
         <ul class="page-breadcrumb">
             <li>
                 <i class="icon-home"></i>
                 <a >Lancofruit</a>
                 <i class="fa fa-angle-right"></i>
             </li>
         </ul>
        <div class="page-toolbar" style="display:flex">
            <div>
                <label for="">
                    CLIENTE :
                </label>
                <select style="margin: 5px;height: 36px; min-width:150px !important" ng-model="filters.cliente" ng-change="getDataTable()">
                    <option ng-if="logged != 49" value="">TODOS</option>
                    <option ng-repeat="cliente in clientes" value="{{cliente}}">{{cliente}}</option>
                </select>
            </div>
            <div>
                <label for="">
                    LOCAL : 
                </label>
                <select  style="margin: 5px;height: 36px; min-width:150px !important" ng-model="filters.local" ng-change="filterLocales()">
                    <option value="">TODOS</option>
                    <option ng-repeat="(key, value) in locales" value="{{value.local}}">{{value.local}}</option>
                </select>
            </div>
            <ng-calendarapp  search="search"></ng-calendarapp>
        </div>
     </div>   
    <div id="indicadores"></div>
    
    <div class="row">
        <div class="col-md-6">
            <div class="portlet light portlet-fit bordered">
                <div class="portlet-title">
                    <div class="caption"></div>
                </div>
                <div class="portlet-body">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>PROM</th>
                                    <th>MAX</th>
                                    <th>MIN</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th class="text-left">Temp. Bodega</th>
                                    <th>{{ data.prom_bodega | number:2 }}</th>
                                    <th>{{ data.max_bodega | number }}</th>
                                    <th>{{ data.min_bodega | number }}</th>
                                </tr>
                                <tr>
                                    <th class="text-left">Temp. Percha 1</th>
                                    <th>{{ data.prom_percha_1 | number:2 }}</th>
                                    <th>{{ data.max_percha_1 | number }}</th>
                                    <th>{{ data.min_percha_1 | number }}</th>
                                </tr>
                                <tr>
                                    <th class="text-left">Temp. Percha 2</th>
                                    <th>{{ data.prom_percha_2 | number:2 }}</th>
                                    <th>{{ data.max_percha_2 | number }}</th>
                                    <th>{{ data.min_percha_2 | number }}</th>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6 hide">
            <div class="portlet light portlet-fit bordered">
                <div class="portlet-title">
                    <div class="caption"></div>
                </div>
                <div class="portlet-body">
                    <div id="pastel-kilos" class="charts"></div>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="portlet light portlet-fit bordered">
                <div class="portlet-title">
                    <div class="caption"></div>
                </div>
                <div class="portlet-body">
                    <div id="pastel-fruta" class="charts"></div>
                </div>
            </div>
        </div>
        <div class="col-md-6 hide">
            <div class="portlet light portlet-fit bordered">
                <div class="portlet-title">
                    <div class="caption"></div>
                </div>
                <div class="portlet-body">
                    <div id="pastel-perchas" class="charts"></div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="portlet light portlet-fit bordered">
                <div class="portlet-title">
                    <div class="caption"></div>
                </div>
                <div class="portlet-body">
                    <div id="barras-grados" class="charts"></div>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="portlet light portlet-fit bordered">
                <div class="portlet-title">
                    <div class="caption"></div>
                </div>
                <div class="portlet-body">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>CANT</th>
                                    <th>%</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="text-left">GRADO 1</td>
                                    <td>{{ data.grado_1 }}</td>
                                    <td>{{ data.grado_1 / data.total_grados * 100 | number: 2 }}</td>
                                </tr>
                                <tr>
                                    <td class="text-left">GRADO 2</td>
                                    <td>{{ data.grado_2 }}</td>
                                    <td>{{ data.grado_2 / data.total_grados * 100 | number: 2 }}</td>
                                </tr>
                                <tr>
                                    <td class="text-left">GRADO 3</td>
                                    <td>{{ data.grado_3 }}</td>
                                    <td>{{ data.grado_3 / data.total_grados * 100 | number: 2 }}</td>
                                </tr>
                                <tr>
                                    <td class="text-left">GRADO 4</td>
                                    <td>{{ data.grado_4 }}</td>
                                    <td>{{ data.grado_4 / data.total_grados * 100 | number: 2 }}</td>
                                </tr>
                                <tr>
                                    <td class="text-left">GRADO 5</td>
                                    <td>{{ data.grado_5 }}</td>
                                    <td>{{ data.grado_5 / data.total_grados * 100 | number: 2 }}</td>
                                </tr>
                                <tr>
                                    <td class="text-left">GRADO 6</td>
                                    <td>{{ data.grado_6 }}</td>
                                    <td>{{ data.grado_6 / data.total_grados * 100 | number: 2 }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="portlet light portlet-fit bordered">
                <div class="portlet-title">
                    <div class="caption"></div>
                </div>
                <div class="portlet-body">
                    <div id="pastel-categorias" class="charts"></div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="portlet light portlet-fit bordered">
                <div class="portlet-title">
                    <div class="caption"></div>
                    <div class="tools">
                        <select class="form-control" ng-model="filters.categoria" ng-change="reloadGraficaDefectos()">
                            <option ng-repeat="cat in categorias" value="{{cat}}" ng-selected="cat == filters.categoria">{{cat}}</option>
                        </select>
                    </div>
                </div>
                <div class="portlet-body">
                    <div id="pastel-defectos" class="charts"></div>
                </div>
            </div>
        </div>
    </div>
</div>