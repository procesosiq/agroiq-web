<style>
    .form-group {
        margin-bottom: 5px;
    }
    .div-margin{
        margin : 10px !important;
    }
    .table-style{
        border : 1;
    }
    .td-margin{
        margin : 20px;
    }
    .align{
        padding : 10px;
    }
    .flex-container{
        display : -webkit-box;
        display : -moz-box;
        display : -ms-flexbox;
        display : -webkit-flex;
        display : flex;
        align-items: center;
        justify-content : center;
        width : 100%;
    }
    .table-center{
        margin : 0px auto;
    }
    .button-si{
        background-color : green;
        width : 100px;
    }
    .button-no{
        background-color : red;
        width : 100px;
    }
    .btn{
        border-radius : 3px !important;
    }
    .form-control{
        border-radius : 3px !important;
    }
</style>
<div ng-controller="bdperchas">
	<h3 class="page-title"> 
          Lancofruit
     </h3>
     <div class="page-bar">
         <div class="page-toolbar">
           
        </div>
    </div>
    <div class="portlet box green">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-gift"></i> Base de datos </div>
            <div class="tools">
            </div>
        </div>
        <div class="portlet-body">
            <div class="row">
                <div class="col-md-12">
                    <div id="tabla_venta_dia" class="table-responsive table-scrollable">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th class="text-center">FECHA</th>
                                    <th class="text-center">HORA</th>
                                    <th class="text-center">CLIENTE</th>
                                    <th class="text-center">LOCAL</th>
                                    <th class="text-center">RESPONSABLE</th>
                                    <th class="text-center">ACCIONES</th>
                                </tr>
                                <tr>
                                    <td><input type = "text" ng-model = "search.fecha" class = "color-td-prac form-control form-filter"/></td>
                                    <td><input type = "text" ng-model = "search.hora" class = "color-td-prac form-control form-filter"/></td>
                                    <td><input type = "text" ng-model = "search.cliente" class = "color-td-pract form-control form-filter"/></td>
                                    <td><input type = "text" ng-model = "search.local" class = "color-td-prac form-control form-filter"/></td>
                                    <td><input type = "text" ng-model = "search.responsable" class = "color-td-prac form-control form-filter"/></td>
                                    <td></td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat = "row in listado">
                                    <td>{{ row.fecha }}</td>
                                    <td>{{ row.hora }}</td>
                                    <td>{{ row.cliente }}</td>
                                    <td>{{ row.local }}</td>
                                    <td>{{ row.responsable }}</td>
                                    <td class="text-center">
                                        <a href="https://s3.amazonaws.com/json-publicos/{{row.url}}" target="_blank" class="btn blue" title="VER PDF"><i class="fa fa-eye"></i></a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="componentes/FilterableSortableTable.js"></script>

                                                                  