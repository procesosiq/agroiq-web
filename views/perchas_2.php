    <style>
        .label_chart {
            background-color: #fff;
            padding: 2px;
            margin-bottom: 8px;
            border-radius: 3px 3px 3px 3px;
            border: 1px solid #E6E6E6;
            display: inline-block;
            margin: 0 auto;
        }
        .legendLabel{
            padding: 3px !important;
        }
        td {
            text-align: center;
        }
    </style>
      <!-- BEGIN CONTENT BODY -->
<div ng-controller="perchas" ng-cloak>
     <h3 class="page-title"> 
          CALIDAD EN PERCHAS
     </h3>
     <div class="page-bar" ng-init="service.all()">
         <ul class="page-breadcrumb">
             <li>
                 <i class="icon-home"></i>
                 <a >Lancofruit</a>
                 <i class="fa fa-angle-right"></i>
             </li>
         </ul>
        <div class="page-toolbar" style="display:flex">
            <div>
                <label for="">
                    Agencia : 
                </label>
                <select  name="agencia" disabled id="agencia" style="margin: 2px;height: 36px;">
                </select>
            </div>
            <label for="">
                Año : 
            </label>
            <select ng-change="changeYear()" name="year" id="year" ng-model="filters.year" style="margin: 2px;height: 36px;">
                <option ng-repeat="value in anios" value="{{value}}" ng-selected="value == filters.year">{{value}}</option>
            </select>
            <ng-calendarapp  search="search"></ng-calendarapp>
        </div>
     </div>   
    <div id="indicadores"></div>
    <div class="row">
        <div class="col-md-6"> 
            <div class="portlet light portlet-fit bordered" id="lotes_table">
                <div class="portlet-title">
                    <div class="caption"></div>
                </div>
                <div class="portlet-body">
                    <div class="table-scrollable">
                         <table class="table table-striped table-bordered table-hover">
                            <tr>
                                <th>GRADO</th>
                                <th>MANOS</th>
                                <th>DEDOS</th>
                                <th>DEDOS <br> RAJADOS</th>
                                <th>OTROS <br> DAÑOS</th>
                                <th>CORONAS <br> ROTAS</th>
                            </tr>
                            <tbody>
                                <tr ng-repeat="data in table">
                                    <td>{{data.grado}}</td>
                                    <td>{{data.manos}}</td>
                                    <td>{{data.dedos}}</td>
                                    <td>{{data.dedos_rajados}}</td>
                                    <td>{{data.otros_danios}}</td>
                                    <td>{{data.coronas_rotas}}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="portlet light portlet-fit bordered" id="lotes_table">
                <div class="portlet-title">
                    <div class="caption"></div>
                </div>
                <div class="portlet-body">
                    <div id="main_chart" style="height:300px;"></div>
                </div>
            </div>
        </div>
    </div>    
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light portlet-fit bordered" id="lotes_table">
                <div class="portlet-title">
                    <div class="caption"></div>
                </div>
                <div class="portlet-body">
                     <div class="table-scrollable">
                         <table class="table table-striped table-bordered table-hover">
                            <tr>
                                <th>GRADO</th>
                                <th>ENE</th>
                                <th>FEB</th>
                                <th>MAR</th>
                                <th>ABR</th>
                                <th>MAY</th>
                                <th>JUN</th>
                                <th>JUL</th>
                                <th>AGO</th>
                                <th>SEP</th>
                                <th>OCT</th>
                                <th>NOV</th>
                                <th>DIC</th>
                            </tr>
                            <tbody>
                                <tr ng-repeat="data in tableHistorica">
                                    <td>{{data.grado}}</td>
                                    <td>{{data.ENE}}</td>
                                    <td>{{data.FEB}}</td>
                                    <td>{{data.MAR}}</td>
                                    <td>{{data.ABR}}</td>
                                    <td>{{data.MAY}}</td>
                                    <td>{{data.JUN}}</td>
                                    <td>{{data.JUN}}</td>
                                    <td>{{data.JUL}}</td>
                                    <td>{{data.SEP}}</td>
                                    <td>{{data.OCT}}</td>
                                    <td>{{data.NOV}}</td>
                                    <td>{{data.DIC}}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div> 
    <div class="row">
        <div class="col-md-6"> 
            <div class="portlet light portlet-fit bordered" id="lotes_table">
                <div class="portlet-title">
                    <div class="caption">
                        COMPARACION DANIOS AGENCIAS
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-scrollable">
                         <table class="table table-striped table-bordered table-hover">
                            <tr>
                                <th>AGENCIA</th>
                                <th>% CORONAS <br> ROTAS</th>
                                <th>% DEDOS <br> RAJADOS</th>
                                <th>% OTROS <br> DAÑOS</th>
                            </tr>
                            <tbody>
                                <tr ng-repeat="data in tableComparation">
                                    <td>{{data.agencia}}</td>
                                    <td>{{data.dedos_rajados}}</td>
                                    <td>{{data.otros_danios}}</td>
                                    <td>{{data.coronas_rotas}}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="portlet light portlet-fit bordered" id="lotes_table">
                <div class="portlet-title">
                    <div class="caption">
                        % DE DAÑOS
                    </div>
                </div>
                <div class="portlet-body">
                    <div id="comparation_chart" style="height:300px;"></div>
                </div>
            </div>
        </div>
    </div>  
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light portlet-fit bordered" id="lotes_table">
                <div class="portlet-title">
                    <div class="caption">
                        COMPARACION TEMP HISTORICO
                    </div>
                </div>
                <div class="portlet-body">
                    <div id="historico_chart" style="height:500px;"></div>
                </div>
            </div>
        </div>
    </div>
</div>