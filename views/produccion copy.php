<!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="../assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
        <link href="../assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->

<div ng-controller="prodccion" ng-cloak>
	<h3 class="page-title"> 
          Producción
     </h3>
     <div class="page-bar" ng-init="calidad.nocache()">
         <ul class="page-breadcrumb">
             <li>
                 <i class="icon-home"></i>
                 <a>Producción</a>
                 <i class="fa fa-angle-right"></i>
             </li>
         </ul>
    </div>
<div class="portlet light portlet-fit ">
	<div class="portlet-title">
		<i class="icon-settings font-dark"></i>
	    <span class="caption-subject font-dark sbold uppercase">Control en línea</span>
	</div>


	<div class="portlet-body">
	    <div class="table-container">
	        <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax">
	            <thead>
	                <tr class="heading">
	                    <th width="5%"> Lote  </th>
	                    <th width="15%"> Cable </th>
	                    <th width="5%"> Cinta  </th>
	                    <th width="15%"> Racimos <br> Cosechados  </th>
	                    <th width="13%"> Racimos <br> Recusados </th>                                                    
	                    <th width="13%"> Racimos <br> Procesados </th>                                                    
	                    <th width="13%"> Racimos <br> Muestrados </th>                                                    
	                    <th width="13%"> Peso </th>                                                    
	                    <th width="13%"> Manos </th>
	                    <th width="13%"> Calibre </th>                                                  
	                    <th width="13%"> L.Dedos </th>                                                  
	                </tr>
	            </thead>
	            <tbody> </tbody>
	        </table>
	    </div>
	</div>
</div>

<div class="portlet box green">
	<div class="portlet-title">
	    <span class="caption">Resultados del día</span>
	    <div class="tools">
            <a href="javascript:;" class="collapse" data-original-title="Expandir/Contraer" title=""> </a>
        </div>
	</div>
	<div class="portlet-body">
		<table class="table table-striped table-bordered table-hover table-checkable">
			<thead>
	                <tr role="row" class="heading">
	                    <th width="5%"> Cinta  </th>
	                    <th width="15%"> Racimos <br> Cosechados  </th>
	                    <th width="13%"> Racimos <br> Recusados </th>                                                    
	                    <th width="13%"> Racimos <br> Muestrados </th>                                                    
	                    <th width="13%"> Peso </th>                                                    
	                    <th width="13%"> Manos </th>
	                    <th width="13%"> Calibre </th>                                                  
	                    <th width="13%"> L.Dedos </th>                                                  
	                </tr>
			<tbody>
				<tr>
					<td> <span style="background-color: cyan;width: 80%;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>	
				<tr>
					<td> <span style="background-color: blue;width: 80%;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td> <span style="background-color: yellow;width: 80%;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td> <span style="background-color: red;width: 80%;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>					
			</tbody>
		</table>
	</div>
</div>
</div>