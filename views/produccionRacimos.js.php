<link rel="stylesheet" href="assets/global/plugins/react-select/react-select.min.css">
<link rel="stylesheet" href="componentes/contextMenu/jquery.contextMenu.min.css">

<script src="componentes/FilterableSortableTable.js"></script>
<script src="assets/global/plugins/FileSaver.min.js"></script>
<script src="componentes/contextMenu/jquery.contextMenu.min.js"></script>
<script src="assets/global/plugins/react-input-autosize/react-input-autosize.min.js"></script>
<script src="assets/global/plugins/react-select/react-select.js"></script>