<style>
	table > th,td{
		text-align: center !important;
	}
	.center-th {
		text-align: center !important;
	}
	table {
		cursor: pointer !important;
	}
	.error {
		border : 0.5px solid #E26A6A;
	}
	tfoot > tr > td {
		font-size: 13px !important;
		padding: 0px !important;
	}	
    .delete  {
        background-color : #c8d8e8 !important;
    }

	.form-control, .input-sm, select, .btn, .portlet {
        border-radius : 3px !important;
    }
	.highlight {
		background-color: green;
    	color: white !important;
	}
	.highlight:hover {
		background-color: #285e8e !important;
    	color: white !important;
	}

	.crear-finca {
		padding-top: 7px;
	}
	.second-row {
		background-color: #eee !important;
	}

	div.datepicker.datepicker-dropdown.dropdown-menu 
		div.datepicker-days
			table tbody td.day.disabled:hover {
				background-color : #eeeeff !important;
			}
	div.datepicker.datepicker-dropdown.dropdown-menu 
		div.datepicker-days
			table tbody td.day.available:hover {
				background-color : blue !important;
			}
</style>

<div ng-controller="produccion" ng-cloak>
	<h3 class="page-title"> 
          Cajas
     </h3>
     <div class="page-bar">
         <ul class="page-breadcrumb">
             <li>
                 <i class="icon-home"></i>
                 <a>Cajas</a>
                 <i class="fa fa-angle-right"></i>
             </li>
         </ul>
         <div class="page-toolbar">
            <label class="btn red-thunderbird btn-outline btn-circle btn-sm" ng-click="changeUnidad()">
                {{ table.unidad }}
            </label>
            <label>Fecha</label>
            <input id="datepicker" class="input-sm" sytle="height: 36px;" data-provide="datepicker" data-date-format="yyyy-mm-dd" readonly>
        </div>
    </div>
    
	<div id="contenedor" class="div2">

		<div class="tabbable tabbable-tabdrop">
			<ul class="nav nav-tabs">
				<li ng-repeat="(key, value) in fincas" class="{{ table.finca == key ? 'active' : '' }}">
					<a ng-click="table.finca = key; changeFinca()">{{value}}</a>
				</li>

				<li class="crear-finca">
					<a title="Agregar Finca" data-toggle="modal" data-target="#crear-finca-proceso-modal">
						<span class="badge badge-success" sytle="width: 50px; height: 50px;">
							<i class="fa fa-plus"></i>
						</span>
					</a>
				</li>
			</ul>
		</div>

		<?php include('./views/marun/tags_produccion_cajas.php') ?>
		
		<div class="row">
			<div class="col-md-12">
				<div class="portlet box green-haze">
					<div class="portlet-title">
						<span class="caption">RESUMEN CAJAS</span>
						<div class="tools">
							<div class="btn-group">
								<a class="btn dark btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false"> 
									Exportar <i class="fa fa-angle-down"></i>
								</a>
								<ul class="dropdown-menu pull-right">
									<li>
										<a href="javascript:;" ng-click="exportPrint('table_2')"> Imprimir </a>
									</li>
									<li>
										<a href="javascript:;" ng-click="exportExcel('table_2')">Excel</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="portlet-body">
						<div style="height: 40px;">
							<button class="btn btn-primary" ng-click="changeRangeDate()" title="Restablecer filas ocultas" ng-show="hiddenRows"> 
								<i class="fa fa-refresh"></i> Restablecer
							</button>
							<span title="Ocultar filas: Click derecho sobre una fila">
								<i class="fa fa-eye"></i>
							</span>

                            <ng-calendarapp search="search"></ng-calendarapp>
                        </div>
						<div class="table-responsive" id="div_table_2">
							<table class="table table-striped table-bordered table-hover" id="table_2">
								<thead>
									<tr>
										<th class="center-th">TIPO</th>
										<th class="center-th">MARCA</th>
										<th class="center-th">CANTIDAD</th>
                                        <th class="center-th">
											<small>({{ convertidas_default }} lb)</small>
											<br>
											CONV
											<a title="Mostrar decimales" ng-click="mostrarDecimalesConv = !mostrarDecimalesConv">...</a>
										</th>
										<th class="center-th">TOTAL {{ table.unidad | uppercase }}</th>
										<th class="center-th">PROM</th>
										<th class="center-th">MAX</th>
										<th class="center-th">MIN</th>
										<th class="center-th">DESV</th>
										<th class="center-th">CANT.<br>BAJO PESO</th>
										<th class="center-th">%<br>BAJO PESO</th>
										<th></th>
									</tr>
								</thead>
								<tbody>
									<tr ng-repeat="row in resumen" class="context-menu-one" key="{{$index}}" ng-if="!row.hidden">
										<td>{{ row.tipo }}</td>
										<td>
											<span ng-if="row.id_marca > 0">{{ row.marca }}</span>
											<span class="font-red-thunderbird" 
												ng-if="!row.id_marca > 0" 
												title="Favor de registrar esta marca en la base de datos" 
												ng-click="asignarAliasF({ status : 'Activo', tipo : 'CAJA', nombre : row.marca })">

												<u>{{ row.marca }}</u>
											</span>
										</td>
										<td>{{ row.cantidad | number }}</td>
                                        <td>{{ row.conv | number : (mostrarDecimalesConv ? 2 : 0) }}</td>
										<td>{{ row.total_kg | number: 2 }}</td>
										<td>
											<span ng-if="!row.umbral_minimo > 0">
												{{ row.promedio | number: 2 }} <span class="badge badge-info" title="Este promedio no tiene una restriccion de peso minimo, por lo cual puede ser mas bajo de lo normal"><i class="fa fa-info"></i></span>
											</span>
											<span ng-if="row.umbral_minimo > 0">
												{{ row.promedio | number: 2 }}
											</span>
										</td>
										<td>{{ row.maximo | number: 2 }}</td>
										<td>{{ row.minimo | number: 2 }}</td>
										<td>{{ row.desviacion | number: 2 }}</td>
										<td>
											<span ng-if="row.umbral_minimo > 0 && row.minimo < row.umbral_minimo">
												<span title="Cajas por debajo del umbral : {{ row.umbral_minimo }}">{{ row.cajas_bajo_peso }}</span>
											</span>
										</td>
										<td>
											<span ng-if="row.umbral_minimo > 0 && row.minimo < row.umbral_minimo">
												{{ row.cajas_bajo_peso / row.cantidad * 100  | number : 2 }} %
											</span>
										</td>
										<td>
											<span ng-if="row.umbral_minimo > 0 && row.minimo < row.umbral_minimo">
												<button class="btn btn-primary" ng-click="procesarMarca(row.id_marca)">
													Procesar
												</button>
											</span>
										</td>
									</tr>
								</tbody>
								<tfoot>
									<tr>
										<th>TOTAL</th>
										<th></th>
										<th class="center-th">{{ (resumen | sumOfValue:'cantidad' | number) }}</th>
                                        <th class="center-th">{{ (resumen | sumOfValue:'conv' | number : 0) }}</th>
										<th class="center-th">{{ (resumen | sumOfValue:'total_kg') | number : 2 }}</th>
										<th class="center-th">{{ (resumen | avgOfValue:'promedio') | number : 2 }}</th>
										<th class="center-th">{{ (resumen | avgOfValue:'maximo') | number : 2 }}</th>
										<th class="center-th">{{ (resumen | avgOfValue:'minimo') | number : 2 }}</th>
										<th class="center-th">{{ (resumen | avgOfValue:'desviacion') | number : 2 }}</th>
										<th></th>
										<th></th>
										<th></th>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>

        <div class="row">
			<div class="col-md-12">
				<div class="portlet box green-haze">
					<div class="portlet-title">
						<span class="caption">EXCEDENTE <span class="badge badge-info" title="Para visualizar es necesario configurar marcas con Máximo y Mínimo"><i class="fa fa-info"></i></span> </span>
						<div class="actions">
							
						</div>
					</div>
					<div class="portlet-body" id="tablas">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th class="text-center">MARCA</th>
                                        <th class="text-center">TOTAL {{ table.unidad | uppercase }} </th>
                                        <th class="text-center">CAJAS CONV</th>
                                        <th class="text-center">DOLARES</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="row in tablasDiferencias">
                                        <td>{{ row.marca }}</td>
                                        <td>{{ row.kg_diff }}</td>
                                        <td>{{ row.cajas }}</td>
                                        <td>{{ row.dolares }}</td>
                                    </tr>
                                    <tr>
                                        <th></th>
                                        <th class="text-center">{{ tablasDiferencias | sumOfValue: 'kg_diff' | number: 2 }}</th>
                                        <th class="text-center">{{ tablasDiferencias | sumOfValue: 'cajas' | number: 2 }}</th>
                                        <th class="text-center">{{ tablasDiferencias | sumOfValue: 'dolares' | number: 2 }}</th>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
					</div>
				</div>
			</div>
		</div>

        <div class="row">
			<div class="col-md-12">
				<div class="portlet box green-haze">
					<div class="portlet-title">
						<span class="caption"></span>
						<div class="actions">
							<button class="btn btn-primary" ng-click="loadPasteles()"> <i class="fa fa-refresh"></i> Refrescar Gráficas</button>
						</div>
					</div>
					<div class="portlet-body">
                        <div class="row" id="barras">
                            <div ng-repeat="marca in marcasBarras" class="col-md-12" style="padding-bottom: 50px;">
								<div class="col-md-12">
									<input type="text" class="input-sm" name="{{ marca.marca }}" ng-model="rangos[marca.marca]"> Rango
								</div>
								<div class="col-md-12 font-red" ng-if="!marca.id_marca || !marca.maximo || !marca.minimo">
									<label class="control-label">Encontramos estos problemas</label>
									<ol>
										<li ng-if="!marca.id_marca">La marca no esta registrada</li>
										<li ng-if="!marca.maximo">No existe el peso máximo</li>
										<li ng-if="!marca.minimo">No existe el peso mínimo</li>
									</ol>
								</div>
                                <div class="col-md-6">
                                    <div id="barras_{{$index}}" class="chart"></div>
                                </div>
                                <div class="col-md-6">
                                    <div id="pastel_{{$index}}" class="chart"></div>
                                </div>
                            </div>
                        </div>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="portlet box green-haze">
					<div class="portlet-title">
						<span class="caption">HISTÓRICO CAJAS SEMANAL</span>
						<div class="tools pull-right">
							<select class="form-control" ng-model="table.var" ng-change="loadSemanal()">
								<option value="CONV">CONV</option>
								<option value="CONV/HA">CONV/HA</option>
							</select>
						</div>
					</div>
					<div class="portlet-body">
						<div id="cajas_semanal" style="height: 400px;">

						</div>
					</div>
				</div>
			</div>
		</div>

        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    Cajas
				</div>
                <div class="actions">
					<a class="btn btn-sm btn-primary font-white" title="Base de datos" href="produccioncajasbase?fecha={{table.fecha_inicial}}">
						<i class="fa fa-database"></i>
					</a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="panel-group accordion" id="accordion3">

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_3" aria-expanded="false"> HISTORICO EXCEDENTE </a>
                            </h4>
                        </div>
                        <div id="collapse_3_3" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                            <div class="panel-body">
                                <div class="pull-right col-md-2">
                                    <select class="form-control input-sm" ng-model="filters.var" ng-change="changeExcedente()">
                                        <option value="exce">{{ table.unidad | uppercase }}</option>
                                        <option value="cajas">CAJAS</option>
                                        <option value="dolares">DLL</option>
                                    </select>
                                </div>
                                <div id="table-historico-excedente"></div>
                            </div>
                        </div>
                    </div>
					
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a id="expand_cuadrar" ng-click="getCuadrar()" class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_2" aria-expanded="false"> CUADRE DE CAJAS </a>
                            </h4>
                        </div>
						<div id="collapse_3_2" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
							<br>

                            <div class="row">
                                <div class="col-md-12 text-right">
									<a ng-show="enabled.procesar && isValidGuia()" class="btn red" ng-click="procesarDia()">Procesar</a>
									<a class="btn btn-success" data-toggle="modal" data-target="#crear-cajas-modal"> <i class="fa fa-plus"></i> CREAR CAJAS </a>
									<a class="btn btn-primary" ng-click="modalCuadrar()"> GUIA </a> &nbsp;
                                </div>
							</div>
							
							<br>

                            <div class="row">
                                <div class="col-md-8">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered table-hover" id="table_4">
                                            <thead>
                                                <tr>
													<th class="center-th">TIPO</th>
                                                    <th class="center-th">MARCA</th>
                                                    <th class="center-th">BALANZA</th>
                                                    <th class="center-th">GUIA</th>
													<th class="center-th">PEND</th>
													<th class="center-th">ASIG</th>
													<th class="center-th">REAL</th>
                                                    <th class="center-th">%</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr ng-repeat-start="row in cuadrarCajas">
													<td class="center-th">{{ row.tipo }}</td>
                                                    <td class="center-th">{{ row.marca }}</td>
                                                    <td class="center-th">{{ row.balanza }}</td>
                                                    <td class="center-th">{{ row.suma_real }}</td>
													<td ng-dblclick="edit(row, 'pendiente')" escape="cancel(row)" enter="save(row)">
														<span  ng-if="row.editing != 'pendiente'">{{ row.pendiente > 0 ? row.pendiente : '' }}</span>
														<input ng-if="row.editing == 'pendiente'" type="text" class="input-md" ng-model="row.pendiente">
													</td>
													<td>
														{{ row.asignada }}
													</td>
													<td>
														<span ng-hide="true">{{ row.real = sum(row.suma_real, row.pendiente, row.asignada*-1) }}</span>
														{{ row.real ? row.real : '' }}
													</td>
                                                    <td class="center-th">
														<span ng-hide="true">{{ row.porcentaje = (row.real/row.balanza*100) }}</span>
														{{ row.porcentaje ? (row.porcentaje | number : 2) : '' }}
													</td>
                                                </tr>
                                                <tr ng-repeat-end="row" class="second-row" ng-repeat="d in row.detalle" ng-show="row.expanded">
													<td></td>
                                                    <td>{{d.guia}}</td>
                                                    <td></td>
                                                    <td>{{d.valor}}</td>
													<td></td>
													<td></td>
													<td></td>
                                                    <td>{{d.valor/row.suma_real*100 | number: 2}}</td>
                                                </tr>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <td></td>
                                                    <th class="center-th">TOTAL:</th>
                                                    <th class="center-th">{{ cuadrarCajas | sumOfValue : 'balanza' }}</th>
                                                    <th class="center-th">{{ cuadrarCajas | sumOfValue : 'suma_real' }}</th>
													<th class="center-th">{{ cuadrarCajas | sumOfValue : 'pendiente' }}</th>
													<th class="center-th">{{ cuadrarCajas | sumOfValue : 'asignada' }}</th>
													<th class="center-th">{{ cuadrarCajas | sumOfValue : 'real' }}</th>
                                                    <th class="center-th">{{ cuadrarCajas | avgOfValue : 'porcentaje' | number: 2 }}</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered table-hover" id="table_5">
                                            <thead>
                                                <tr>
                                                    <th class="center-th">GUIA</th>
                                                    <th class="center-th">CAJAS</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr ng-repeat-start="row in guias" ng-click="row.expanded = !row.expanded">
                                                    <td>{{ row.guia }}</td>
                                                    <td>{{ row.cajas }}</td>
                                                    <td><button class="btn" ng-click="editarGuia(row)">Editar</button></td>
                                                </tr>
                                                <tr ng-repeat-end="" class="second-row" ng-repeat="d in row.detalle" ng-show="row.expanded">
                                                    <td>{{d.marca}}</td>
                                                    <td>{{d.valor}}</td>
                                                    <td>
														<button class="btn btn-sm btn-danger" ng-click="borrarGuiaMarca(row, d)">
															<i class="fa fa-trash"></i>
														</button>
													</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

							<div class="row">
								<div class="col-md-8">
									<div class="table-responsive">
										<table class="table">
											<thead>
												<tr>
													<th class="text-center">ORIGEN</th>
													<th class="text-center">TIPO</th>
													<th class="text-center">MARCA</th>
													<th class="text-center">SALDO</th>
													<th class="text-center">ESTATUS</th>
													<th class="text-center">FINCA</th>
												</tr>
											</thead>
											<tbody>
												<tr ng-repeat="row in saldos">
													<td class="text-center">{{ row.finca }}</td>
													<td class="text-center">CAJA</td>
													<td class="text-center">{{ row.marca }}</td>
													<td class="text-center">{{ row.pendiente-row.asignada }}</td>
													<td ng-click="modalAsignar(row)" class="text-center {{ row.pendiente-row.asignada > 0 ? 'bg-red-thunderbird bg-font-red-thunderbird' : 'bg-green-haze bg-font-green-haze' }}">{{ row.pendiente-row.asignada > 0 ? 'POR ASIGNAR' : 'ASIGNADO'  }}</td>
													<td class="text-center"></td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-md-8">
									<div class="mt-element-list">
										<div class="mt-list-head list-simple ext-1 font-white bg-blue-chambray">
											<div class="list-head-title-container">
												<h3 class="list-title">Movimientos</h3>
											</div>
										</div>
										<div class="mt-list-container list-simple ext-1 group">
											<a class="list-toggle-container collapsed" data-toggle="collapse" href="#completed-simple" aria-expanded="false">
												<div class="list-toggle done uppercase"> Todas las fincas
													<span class="badge badge-default pull-right bg-white font-green bold">{{ saldos_movientos.length }}</span>
												</div>
											</a>
											<div class="panel-collapse collapse" id="completed-simple" aria-expanded="false" style="height : 0px;">
												<ul>
													<li class="mt-list-item done" ng-repeat="row in saldos_movientos">
														<div class="list-icon-container">
															<i class="icon-check"></i>
														</div>
														<div style="width : unset !important;" class="list-datetime"> <b>({{ row.finca }})</b> </div>
														<div class="list-item-content">
															<h3 class="uppercase">
																<span ng-show="row.tipo == 'PENDIENTE'">
																	<b>{{ row.cantidad }}</b> <u>{{ row.marca }}</u> EN <span class="font-red">PENDIENTES</span>
																</span>
																<span ng-show="row.tipo == 'ASIGNADA'">
																	<b>{{ row.cantidad }}</b> <u>{{ row.marca }}</u> <span class="font-red">ASIGNADAS</span> A : {{ row.finca_destino }}
																</span>
															</h3>
														</div>
													</li>
												</ul>
											</div>
										</div>
									</div>
								</div>
								<?php if(isset($_GET['dev'])): ?>
								<div class="col-md-4">
									<h4>
										Comentarios
										<a class="btn btn-success" ng-click="guardarComentarios()"><i class="fa fa-save"></i></a>
									</h4>
									<textarea class="form-control" cols="30" rows="10" ng-model="comentarios"></textarea>
								</div>
								<?php endif ; ?>
							</div>
                        </div>
                    </div>

					<div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a id="expand_cuadrar" class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_8" aria-expanded="false"> MARCAS DE CAJAS </a>
                            </h4>
                        </div>
                        <div name="collapse_3_8" id="collapse_3_8" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
							<div class="row">
								<div class="col-md-12 text-right">
									<button class="btn btn-sm btn-success" style="margin: 10" ng-click="editarMarca({ status : 'Activo', tipo : 'CAJA' })">+ NUEVA</button>
								</div>
							</div>
                            <div class="row" id="marcas">
								<div class="col-md-12">
									<div class="table-responsive">
										<table class="table table-bordered">
											<thead>
												<tr>
													<th></th>
													<th></th>
													<th class="text-center" colspan="4">PESO ({{ table.unidad | uppercase }})</th>
													<th></th>
													<th></th>
												</tr>
												<tr>
													<th class="text-center">CODIGO</th>
													<th class="text-center">EXPORTADOR</th>
													<th class="text-center">MARCA</th>
													<th class="text-center">REF</th>
													<th class="text-center">MAX</th>
													<th class="text-center">MIN</th>
													<th class="text-center">AJUSTE MIN</th>
													<th class="text-center">ESTATUS</th>
													<th></th>
												</tr>
												<tr>
													<th><input type="text" class="form-control" ng-model="filtersMarca.codigo" ng-change="deleteProp(filtersMarca, 'codigo')"></th>
													<th><input type="text" class="form-control" ng-model="filtersMarca.exportador"></th>
													<th><input type="text" class="form-control" ng-model="filtersMarca.nombre"></th>
													<th></th>
													<th></th>
													<th></th>
													<th></th>
													<th>
														<!--<input type="text" class="form-control" ng-model="filtersMarca.status">-->
														<select ng-model="filtersMarca.status" class="form-control" ng-init="filtersMarca.status = 'Activo'">
															<option value="">Todos</option>
															<option value="Activo">Activo</option>
															<option value="Inactivo">Inactivo</option>
														</select>
													</th>
													<th></th>
												</tr>
											</thead>
											<tbody>
												<!-- nombre : like , codigo : = -->
												<tr ng-repeat="row in marcas | filter : { nombre : filtersMarca.nombre } | filter : { codigo : filtersMarca.codigo, status : filtersMarca.status } : true">
													<td>{{ row.codigo }}</td>
													<td>{{ row.exportador }}</td>
													<td>{{ row.nombre }}</td>
													<td>{{ row.requerimiento | number : 2 }}</td>
													<td>{{ row.max | number : 2 }}</td>
													<td>{{ row.min | number : 2 }}</td>
													<td>{{ row.umbral_minimo | number : 2 }}</td>
													<td>{{ row.status }}</td>
													<td>
														<button class="btn btn-sm" ng-click="editarMarca(row)">
															Editar <i class="fa fa-pencil"></i>
														</button>
														<button class="btn btn-sm btn-danger" ng-click="deleteMarca(row, $index)" ng-if="enabled.eliminar_marca">
															Eliminar <i class="fa fa-trash"></i>
														</button>
													</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
                        </div>
                    </div>

					<div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a id="eficiencia" ng-click="loadEficienciaBalanza()" class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_9" aria-expanded="false"> EFICIENCIA DE LA BALANZA </a>
                            </h4>
                        </div>
						<div name="collapse_3_9" id="collapse_3_9" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
							<div class="row">
								<div class="col-md-12">
									<div id="grafica-eficiencia" class="chart"></div>
								</div>
							</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</div>

	<!-- BEGIN MODALES -->


	<div class="modal fade" id="rangos" role="basic" aria-hidden="true" style="z-index: 9999!important;">
		<div class="modal-dialog" style="background: white;">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Configurar Rangos</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-4">
							<label class="control-label">Marca: </label>
						</div>
						<div class="col-md-8">
							<select class="form-control" ng-model="config.rangos.newConfig.marca">
								<option ng-repeat="(key, value) in config.rangos.marcas" value="{{ value }}">{{ key }}</option>
							</select>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-4">
							<label class="control-label">Max: </label>
						</div>
						<div class="col-md-8">
							<input type="text" class="form-control" ng-model="config.rangos.newConfig.max"/>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-4">
							<label class="control-label">Min: </label>
						</div>
						<div class="col-md-8">
							<input type="text" class="form-control" ng-model="config.rangos.newConfig.min"/>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn default" data-dismiss="modal">Cancelar</button>
				<button type="button" class="btn blue" ng-click="configuracion.save()">Guardar</button>
			</div>
		</div>
	</div>

	<!-- GUIA DE REMISION -->
    <div class="modal fade" id="cuadrar" role="basic" aria-hidden="true" style="z-index: 9999!important;">
		<div class="modal-dialog" style="background: white;">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">ASIGNAR GUIA DE REMISION</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-4">
							<label class="control-label"># GUIA DE REMISIÓN <span class="text-danger">*</span></label>
						</div>
						<div class="col-md-8">
							<input type="text" class="form-control" ng-model="guia.guia" ng-blur="giasDia(guia.fecha, guia.id)" />
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-4">
							<label class="control-label">CODIGO PRODUCTOR </label>
						</div>
						<div class="col-md-8">
							<input type="text" class="form-control" ng-model="guia.productor"/>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-4">
							<label class="control-label">CODIGO MAGAP </label>
						</div>
						<div class="col-md-8">
							<input type="text" class="form-control" ng-model="guia.magap"/>
						</div>
					</div>
				</div>
                <div class="row">
					<div class="col-md-12">
						<div class="col-md-4">
							<label class="control-label">SELLO SEGURIDAD </label>
						</div>
						<div class="col-md-8">
							<input type="text" class="form-control" ng-model="guia.sello_seguridad"/>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-4">
							<label class="control-label">NUMERO CONTENEDOR </label>
						</div>
						<div class="col-md-8">
							<input type="text" class="form-control" ng-model="guia.numero_contenedor"/>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-4">
							<label class="control-label">FINCA <span class="text-danger">*</span></label>
						</div>
						<div class="col-md-8">
							<select ng-model="guia.finca" class="form-control">
								<option value="">Seleccione</option>
								<option value="{{ id }}" ng-repeat="(id, label) in available_fincas">{{ label }}</option>
							</select>
						</div>
					</div>
				</div>
                <div class="row">
					<div class="col-md-12">
						<div class="col-md-4">
							<label class="control-label">FECHA <span class="text-danger">*</span></label>
						</div>
						<div class="col-md-8">
							<input class="form-control" 
								data-provide="datepicker" 
								data-date-format="yyyy-mm-dd" 
								ng-model="guia.fecha" 
								ng-change="giasDia(guia.fecha, guia.guia)" readonly>
						</div>
					</div>
				</div>
                <div class="row">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="table_5">
                            <thead>
                                <tr>
                                    <th class="center-th">CAJAS</th>
                                    <th class="center-th">CANTIDAD</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <select class="form-control" ng-model="marcasSeleccionadas[0]">
											<option value="{{marca.id}}" 
													ng-selected="marca.id == marcasSeleccionadas[0]"
													ng-repeat="marca in marcas | filter : { status : 'Activo' } : true">{{marca.nombre}}</option>
                                        </select>
                                    </td>
                                    <td><input type="number" ng-model="guia.marca[marcasSeleccionadas[0]]"/></td>
                                </tr>
                                <tr ng-if="marcasSeleccionadas[0] != ''">
                                    <td>
                                        <select class="form-control" ng-model="marcasSeleccionadas[1]">
											<option value="{{marca.id}}" 
													ng-selected="marca.id == marcasSeleccionadas[1]"
													ng-repeat="marca in marcas | filter : { status : 'Activo' } : true">{{marca.nombre}}</option>
                                        </select>
                                    </td>
                                    <td><input type="number" ng-model="guia.marca[marcasSeleccionadas[1]]"/></td>
                                </tr>
                                <tr ng-if="marcasSeleccionadas[1] != ''">
                                    <td>
                                        <select class="form-control" ng-model="marcasSeleccionadas[2]">
											<option value="{{marca.id}}" 
												ng-selected="marca.id == marcasSeleccionadas[2]"
													ng-repeat="marca in marcas | filter : { status : 'Activo' } : true">{{marca.nombre}}</option>
                                        </select>
                                    </td>
                                    <td><input type="number" ng-model="guia.marca[marcasSeleccionadas[2]]"/></td>
                                </tr>
                                <tr ng-if="marcasSeleccionadas[2] != ''">
                                    <td>
                                        <select class="form-control" ng-model="marcasSeleccionadas[3]">
											<option value="{{marca.id}}" 
													ng-selected="marca.id == marcasSeleccionadas[3]"
													ng-repeat="marca in marcas | filter : { status : 'Activo' } : true">{{marca.nombre}}</option>
                                        </select>
                                    </td>
                                    <td><input type="number" ng-model="guia.marca[marcasSeleccionadas[3]]"/></td>
                                </tr>
                                <tr ng-if="marcasSeleccionadas[3] != ''">
                                    <td>
                                        <select class="form-control" ng-model="marcasSeleccionadas[4]">
											<option value="{{marca.id}}" 
													ng-selected="marca.id == marcasSeleccionadas[4]"
													ng-repeat="marca in marcas | filter : { status : 'Activo' } : true">{{marca.nombre}}</option>
                                        </select>
                                    </td>
                                    <td><input type="number" ng-model="guia.marca[marcasSeleccionadas[4]]"/></td>
                                </tr>
                                <tr ng-if="marcasSeleccionadas[4] != ''">
                                    <td>
                                        <select class="form-control" ng-model="marcasSeleccionadas[5]">
											<option value="{{marca.id}}" 
													ng-selected="marca.id == marcasSeleccionadas[5]"
													ng-repeat="marca in marcas | filter : { status : 'Activo' } : true">{{marca.nombre}}</option>
                                        </select>
                                    </td>
                                    <td><input type="number" ng-model="guia.marca[marcasSeleccionadas[5]]"/></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn default" data-dismiss="modal">Cancelar</button>
				<button type="button" class="btn blue" ng-click="guardarCuadrar()">Guardar</button>
			</div>
		</div>
	</div>

	<!-- ASIGNAR CAJAS PENDIENTES -->
	<div class="modal fade" id="asignar" role="basic" aria-hidden="true" style="z-index: 9999!important;">
		<div class="modal-dialog" style="background: white;">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Asignar Cajas</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-4">
							<label class="control-label">Marca: </label>
						</div>
						<div class="col-md-8">
							<select class="form-control" ng-model="asignar_form.marca">
								<option ng-repeat="(key, value) in saldos" value="{{ value.marca }}">{{ value.marca }}</option>
							</select>
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-4">
							<label class="control-label">Cantidad: </label>
						</div>
						<div class="col-md-8">
							<input type="number" class="form-control" ng-model="asignar_form.cantidad">
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-4">
							<label class="control-label">Finca: </label>
						</div>
						<div class="col-md-8">
							<select class="form-control" ng-model="asignar_form.finca">
								<option value="{{key}}" ng-repeat="(key, value) in available_fincas">{{value}}</option>
							</select>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn default" data-dismiss="modal">Cancelar</button>
				<button type="button" class="btn blue" ng-click="asignarGuia()">Guardar</button>
			</div>
		</div>
	</div>

	<!-- CREAR/EDITAR MARCA -->
	<div class="modal fade" id="marca-modal" role="basic" aria-hidden="true" style="z-index: 9999!important;">
		<div class="modal-dialog modal-lg" style="background: white;">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Marca</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-4">
							<label class="control-label">Nombre: </label>
						</div>
						<div class="col-md-8">
							<input type="text" class="form-control" ng-model="marcaSelected.nombre">
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-4">
							<label class="control-label">Código: </label>
						</div>
						<div class="col-md-8">
							<input type="text" class="form-control" ng-model="marcaSelected.codigo">
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-4">
							<label class="control-label">Alias: </label>
						</div>
						<div class="col-md-8">
							<input type="text" class="form-control" ng-model="marcaSelected.nombre_alias">
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-4">
							<label class="control-label">Cliente: </label>
						</div>
						<div class="col-md-8">
							<input type="text" class="form-control" ng-model="marcaSelected.cliente">
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-4">
							<label class="control-label">Exportador: </label>
						</div>
						<div class="col-md-8">
							<input type="text" class="form-control" ng-model="marcaSelected.exportador">
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-4">
							<label class="control-label">Tipo de caja: </label>
						</div>
						<div class="col-md-8">
							<select class="form-control" ng-model="marcaSelected.tipo">
								<option value="CAJA">CAJA</option>
								<option value="PRIMERA">PRIMERA</option>
								<option value="SEGUNDA">SEGUNDA</option>
								<option value="GAVETA">GAVETA</option>
							</select>
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-4">
							<label class="control-label">Estatus: </label>
						</div>
						<div class="col-md-8">
							<select class="form-control" ng-model="marcaSelected.status">
								<option value="Activo">Activo</option>
								<option value="Inactivo">Inactivo</option>
							</select>
						</div>
					</div>

					<div class="col-md-12" style="margin-top : 10px">
						<div class="col-md-6">
							<h3 class="inline-block no-margin">NOMBRE ALTERNATIVO</h3>
							<a class="btn btn-sm btn-success pull-right" style="border-radius : 50% !important; height : 30px; width : 30px;" ng-click="addAlias()">
								+
							</a>

							<div class="row">
								<div class="col-md-12">
									<div class="table-responsive">
										<table class="table">
											<thead>
												<tr>
													<th></th>
													<th>ACCIONES</th>
												</tr>
											</thead>
											<tbody>
												<tr ng-repeat="t in marcaSelected.alias">
													<td>
														<input type="text" class="form-control" ng-model="t.alias">
													</td>
													<td>
														<button class="btn btn-sm btn-success" title="Guardar" ng-click="saveAlias(t)" ng-show="t.alias != ''">
															<i class="fa fa-save"></i>
														</button>
														<button class="btn btn-sm btn-danger" title="Eliminar" ng-click="deleteAlias(t, $index)">
															<i class="fa fa-trash"></i>
														</button>
													</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>

						<div class="col-md-6">
							<h3 class="inline-block no-margin">RANGOS</h3>
							<a class="btn btn-sm btn-success pull-right" style="border-radius : 50% !important; height : 30px; width : 30px;" ng-click="addRango()">
								+
							</a>

							<div class="row">
								<div class="col-md-12">
									<div class="table-responsive">
										<table class="table">
											<thead>
												<tr>
													<th class="text-center" style="min-width: 100px;">FECHA</th>
													<th class="text-center">REF</th>
													<th class="text-center">MAX</th>
													<th class="text-center">MIN</th>
													<th class="text-center">AJUSTE MIN</th>
													<th class="text-center">ACCIONES</th>
												</tr>
											</thead>
											<tbody>
												<tr ng-repeat="r in marcaSelected.rangos">
													<td>
														<input class="input-sm" sytle="height: 36px;" data-provide="datepicker" data-date-format="yyyy-mm-dd" readonly ng-model="r.fecha">
													</td>
													<td><input type="number" class="form-control" ng-model="r.requerimiento"></td>
													<td><input type="number" class="form-control" ng-model="r.max"></td>
													<td><input type="number" class="form-control" ng-model="r.min"></td>
													<td><input type="number" class="form-control" ng-model="r.umbral_minimo"></td>
													<td>
														<button class="btn btn-sm btn-success" title="Guardar" ng-click="saveRango(r)">
															<i class="fa fa-save"></i>
														</button>
														<button class="btn btn-sm btn-danger" title="Eliminar" ng-click="deleteRango(r, $index)">
															<i class="fa fa-trash"></i>
														</button>
													</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn default" data-dismiss="modal">Cancelar</button>
				<button type="button" class="btn blue" ng-click="saveMarca()">Guardar</button>
			</div>
		</div>
	</div>

	<!-- CREAR FINCA -->
	<div class="modal fade" id="crear-finca-proceso-modal" role="basic" aria-hidden="true" style="z-index: 9999!important;">
		<div class="modal-dialog" style="background: white;">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Crear Finca con Proceso</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<p>
							Descripción:<br>
							Se creara información de la finca seleccionada para el día seleccionado ({{ table.fecha_inicial }})
						</p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<h4 class="font-blue-soft">Paso 1: Seleccionar finca</h4>
					</div>
					<div class="col-md-12">
						<div class="col-md-4">
							Finca :
						</div>
						<div class="col-md-8">
							<select class="form-control" ng-model="crearFincaProcesoModal.id_finca">
								<option value="">Seleccione</option>
								<option value="{{key}}" ng-repeat="(key, value) in produccion_fincas | notInArray : fincas">{{value}}</option>
							</select>
						</div>
					</div>
				</div>
				<div class="row" ng-show="crearFincaProcesoModal.id_finca > 0">
					<div class="col-md-12">
						<h4 class="font-blue-soft">Paso 2 : Seleccionar marcas de cajas para crear</h4>
					</div>
					<div class="col-md-12">
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover">
								<thead>
									<tr>
										<th width="70%" class="center-th">CAJAS</th>
										<th width="30%" class="center-th">CANTIDAD</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>
											<div id="crear-finca-proceso-marca_1"></div>
										</td>
										<td>
											<input 
												type="number" 
												ng-class="{'error' : crearFincaProcesoModal.marcasSeleccionadas[0] && !crearFincaProcesoModal.cantidadMarcasSeleccionadas[0]}" 
												class="input-sm" 
												ng-model="crearFincaProcesoModal.cantidadMarcasSeleccionadas[0]"/>
										</td>
									</tr>
									<tr>
										<td>
											<div id="crear-finca-proceso-marca_2"></div>
										</td>
										<td>
											<input 
												type="number" 
												ng-class="{'error' : crearFincaProcesoModal.marcasSeleccionadas[1] && !crearFincaProcesoModal.cantidadMarcasSeleccionadas[1]}" 
												class="input-sm" 
												ng-model="crearFincaProcesoModal.cantidadMarcasSeleccionadas[1]"/>
										</td>
									</tr>
									<tr>
										<td>
											<div id="crear-finca-proceso-marca_3"></div>
										</td>
										<td>
											<input 
												type="number" 
												ng-class="{'error' : crearFincaProcesoModal.marcasSeleccionadas[2] && !crearFincaProcesoModal.cantidadMarcasSeleccionadas[2]}" 
												class="input-sm" 
												ng-model="crearFincaProcesoModal.cantidadMarcasSeleccionadas[2]"/>
										</td>
									</tr>
									<tr>
										<td>
											<div id="crear-finca-proceso-marca_4"></div>
										</td>
										<td>
											<input 
												type="number" 
												ng-class="{'error' : crearFincaProcesoModal.marcasSeleccionadas[3] && !crearFincaProcesoModal.cantidadMarcasSeleccionadas[3]}" 
												class="input-sm" 
												ng-model="crearFincaProcesoModal.cantidadMarcasSeleccionadas[3]"/>
										</td>
									</tr>
									<tr>
										<td>
											<div id="crear-finca-proceso-marca_5"></div>
										</td>
										<td>
											<input 
												type="number" 
												ng-class="{'error' : crearFincaProcesoModal.marcasSeleccionadas[4] && !crearFincaProcesoModal.cantidadMarcasSeleccionadas[4]}" 
												class="input-sm" 
												ng-model="crearFincaProcesoModal.cantidadMarcasSeleccionadas[4]"/>
										</td>
									</tr>
									<tr>
										<td>
											<div id="crear-finca-proceso-marca_6"></div>
										</td>
										<td>
											<input 
												type="number" 
												ng-class="{'error' : crearFincaProcesoModal.marcasSeleccionadas[5] && !crearFincaProcesoModal.cantidadMarcasSeleccionadas[5]}" 
												class="input-sm" 
												ng-model="crearFincaProcesoModal.cantidadMarcasSeleccionadas[5]"/>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn default" data-dismiss="modal">Cancelar</button>
				<button type="button" class="btn blue" ng-click="crearFincaProcesoGuardar()" ng-disabled="!crearFincaProcesoModal.id_finca || !crearFincaProcesoModal.marcasSeleccionadas">Guardar</button>
			</div>
		</div>
	</div>

	<!-- CREAR CAJAS -->
	<div class="modal fade" id="crear-cajas-modal" role="basic" aria-hidden="true" style="z-index: 9999!important;">
		<div class="modal-dialog" style="background: white;">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Crear Finca con Proceso</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<p>
							Descripción:<br>
							Se creara la misma cantidad de cajas que coloque en las cajas de texto
						</p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover">
								<thead>
									<tr>
										<th class="center-th">CAJAS</th>
										<th class="center-th">CANTIDAD</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>
											<select class="form-control" ng-model="crearCajasModal.marcasSeleccionadas[0]">
												<option value="">Seleccione</option>
												<option value="{{marca.id}}" 
														ng-repeat="marca in marcas 
																	| filter : { status : 'Activo' } 
																	| notInArrayObject : 'id' : crearCajasModal.marcasSeleccionadas : '0'"
																	
													>
													{{marca.nombre}}
												</option>
											</select>
										</td>
										<td>
											<input 
												type="number" 
												ng-class="{'error' : crearCajasModal.marcasSeleccionadas[0] && !crearCajasModal.cantidadMarcasSeleccionadas[0]}" 
												class="input-sm" 
												ng-model="crearCajasModal.cantidadMarcasSeleccionadas[0]"/>
										</td>
									</tr>
									<tr>
										<td>
											<select class="form-control" ng-model="crearCajasModal.marcasSeleccionadas[1]">
												<option value="">Seleccione</option>
												<option value="{{marca.id}}" 
														ng-repeat="marca in marcas 
																	| filter : { status : 'Activo' } 
																	| notInArrayObject : 'id' : crearCajasModal.marcasSeleccionadas : '1'"
																	
													>
													{{marca.nombre}}
												</option>
											</select>
										</td>
										<td>
											<input 
												type="number" 
												ng-class="{'error' : crearCajasModal.marcasSeleccionadas[1] && !crearCajasModal.cantidadMarcasSeleccionadas[1]}" 
												class="input-sm" 
												ng-model="crearCajasModal.cantidadMarcasSeleccionadas[1]"/>
										</td>
									</tr>
									<tr>
										<td>
											<select class="form-control" ng-model="crearCajasModal.marcasSeleccionadas[2]">
												<option value="">Seleccione</option>
												<option value="{{marca.id}}" 
														ng-repeat="marca in marcas 
																	| filter : { status : 'Activo' } 
																	| notInArrayObject : 'id' : crearCajasModal.marcasSeleccionadas : '2'"
																	
													>
													{{marca.nombre}}
												</option>
											</select>
										</td>
										<td>
											<input 
												type="number" 
												ng-class="{'error' : crearCajasModal.marcasSeleccionadas[2] && !crearCajasModal.cantidadMarcasSeleccionadas[2]}" 
												class="input-sm" 
												ng-model="crearCajasModal.cantidadMarcasSeleccionadas[2]"/>
										</td>
									</tr>
									<tr>
										<td>
											<select class="form-control" ng-model="crearCajasModal.marcasSeleccionadas[3]">
												<option value="">Seleccione</option>
												<option value="{{marca.id}}" 
														ng-repeat="marca in marcas 
																	| filter : { status : 'Activo' } 
																	| notInArrayObject : 'id' : crearCajasModal.marcasSeleccionadas : '3'"
																	
													>
													{{marca.nombre}}
												</option>
											</select>
										</td>
										<td>
											<input 
												type="number" 
												ng-class="{'error' : crearCajasModal.marcasSeleccionadas[3] && !crearCajasModal.cantidadMarcasSeleccionadas[3]}" 
												class="input-sm" 
												ng-model="crearCajasModal.cantidadMarcasSeleccionadas[3]"/>
										</td>
									</tr>
									<tr>
										<td>
											<select class="form-control" ng-model="crearCajasModal.marcasSeleccionadas[4]">
												<option value="">Seleccione</option>
												<option value="{{marca.id}}" 
														ng-repeat="marca in marcas 
																	| filter : { status : 'Activo' } 
																	| notInArrayObject : 'id' : crearCajasModal.marcasSeleccionadas : '4'"
																	
													>
													{{marca.nombre}}
												</option>
											</select>
										</td>
										<td>
											<input 
												type="number" 
												ng-class="{'error' : crearCajasModal.marcasSeleccionadas[4] && !crearCajasModal.cantidadMarcasSeleccionadas[4]}" 
												class="input-sm" 
												ng-model="crearCajasModal.cantidadMarcasSeleccionadas[4]"/>
										</td>
									</tr>
									<tr>
										<td>
											<select class="form-control" ng-model="crearCajasModal.marcasSeleccionadas[5]">
												<option value="">Seleccione</option>
												<option value="{{marca.id}}" 
														ng-repeat="marca in marcas 
																	| filter : { status : 'Activo' } 
																	| notInArrayObject : 'id' : crearCajasModal.marcasSeleccionadas : '5'"
																	
													>
													{{marca.nombre}}
												</option>
											</select>
										</td>
										<td>
											<input 
												type="number" 
												ng-class="{'error' : crearCajasModal.marcasSeleccionadas[5] && !crearCajasModal.cantidadMarcasSeleccionadas[5]}" 
												class="input-sm" 
												ng-model="crearCajasModal.cantidadMarcasSeleccionadas[5]"/>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn default" data-dismiss="modal">Cancelar</button>
				<button type="button" class="btn blue" ng-click="crearCajasGuardar()" ng-disabled="!crearCajasModal.marcasSeleccionadas">Guardar</button>
			</div>
		</div>
	</div>

	<!-- ASIGNAR ALIAS A MARCA DE CAJA -->
	<div class="modal fade" id="asignar-alias-modal" role="basic" aria-hidden="true" style="z-index: 9999!important;">
		<div class="modal-dialog modal-lg" style="background: white;">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Asignar nombre alternativo a marca : <b>{{ asignarAlias.nombre }}</b></h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<p>
							Descripción:<br>
							Se puede asignar este nombre como nombre alternativo (Sugerencias)
						</p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-4">
							Filtrar
							<input type="text" class="form-control" ng-model="search_alias">
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-3" style="margin-bottom: 10px" ng-repeat="m in sugerenciasAlias | filter : { nombre : search_alias }">
							<button class="btn btn-outline" style="width: 100%" ng-click="agregarAlias(m)">
								<span class="pull-left">
									{{ m.nombre_alias ? m.nombre_alias : m.nombre }}
								</span>

								<span class="pull-right">
									<i class="fa fa-plus"></i>
								</span>
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- END MODALES -->
</div>