<style>
	.accordion .panel .panel-title .accordion-toggle {
		display: block !important;
    	padding: 0px 14px !important;
	}
	.margin-5 {
		margin-bottom: 5px !important;
	}
	table > th,td{
		text-align: center !important;
	}
	.center-th {
		text-align: center !important;
	}
	.left-td {
		text-align: left !important;
		padding-left: 10px !important;
	}
	table {
		cursor: pointer !important;
	}
	.alginCenter {
		text-align: center !important;
		padding-top: 20px;
	}
	.alginLeft {
		text-align: left !important;
		padding-top: 20px;
	}
	tfoot > tr > td {
		font-size: 13px !important;
		padding: 0px !important;
	}
	td-hidden {
		padding: 1px;
	}
	
	.subtittleTr {
	    padding: 2px 25px;
	}

	.Titlespan {
		font-size: 21px;
		/* text-align: left; */
		float: left;
	}

	@media print
		{
		body * { visibility: hidden; }
		.div2 * { visibility: visible; }
		.div2 { position: absolute; top: 40px; left: 30px; }
		}
</style>
<div ng-controller="produccion" ng-cloak>
	<h3 class="page-title"> 
          Producción
     </h3>
     <div class="page-bar" ng-init="init()">
         <ul class="page-breadcrumb">
             <li>
                 <i class="icon-home"></i>
                 <a>Producción</a>
                 <i class="fa fa-angle-right"></i>
             </li>
         </ul>
         <div class="page-toolbar">
            <!--<label for="">
                Lotes : 
            </label>
            <select onchange="cambiosLotes()" name="idLote" id="idLote" ng-model="produccion.params.idLote" style="margin: 2px;height: 36px;">
            	<option value="0">Todos</option>
                <option ng-repeat="(key, value) in lotes" value="{{key}}">{{value}}</option>
            </select>-->
			<label for="">
                Finca : 
            </label>
            <select ng-change="init()" name="finca" id="finca" ng-model="filters.finca" style="margin: 2px;height: 36px;">
            	<option value="">Todos</option>
                <option value="1">Maria Maria</option>
                <option value="2">Domenica</option>
            </select>
            <!--<label for="">
                Año : 
            </label>
            <select onchange="cambiosLotes()" name="year" id="year" ng-model="produccion.params.year" style="margin: 2px;height: 36px;">
                <option ng-repeat="(key, value) in anios" value="{{key}}" ng-selected="key == produccion.params.year">{{value}}</option>
            </select>
            <a class="btn sbold uppercase btn-outline grey-mint" ng-click="printNormal()">Imprimir</a>
            <a class="btn sbold uppercase btn-outline green" ng-click="printPdf()">PDF</a>
            <a download="somedata.csv" href="#" onclick="return ExcellentExport.csv(this, 'table_racimos');" class="btn sbold uppercase btn-outline yellow">Excel</a>-->
        </div>
    </div>
    <?php include("./views/tags_produccion_".$this->session->agent_user.".php");?>  
	<div id="contenedor" class="div2">
		<div class="row">
			<div class="col-md-6">
				<div class="portlet box green">
					<div class="portlet-title">
					    <span class="caption">Resumen</span>
					    <div class="tools">
				            <label>
								Ha: 
								<input type="number" ng-model="hectareas" style="color: #000;" ng-init="hectareas = 94.88;"/>
								<button class="btn blue" type="button" ng-click="saveHA()">Guardar</button>
							</label>
				        </div> 
					</div>
					<div class="portlet-body">

						<div class="table-responsive">
							<table id="table_racimos" class="table table-striped table-bordered table-hover">
								<thead>
						                <tr role="row" class="heading">
						                    <th width="{{withTable}}" class="center-th"> Detalle </th>
						                    <th width="{{withTable}}" class="center-th"> Acumulado  </th>
						                    <th width="{{withTable}}" class="center-th"> Por Ha Sem  </th>
						                    <th width="{{withTable}}" class="center-th"> Por Ha Año  </th>
						                </tr>
						                <tr role="row" class="heading subtittleTr">
						                    <th width="{{withTable}}" class="center-th"> <span class="Titlespan">Racimos</span> </th>
						                    <th width="{{withTable}}" class="center-th">  </th>
						                    <th width="{{withTable}}" class="center-th">  </th>
						                    <th width="{{withTable}}" class="center-th">  </th>
						                </tr>
								</thead>
								<tbody>
									<tr>
										<td width="{{withTable}}" class="center-th">Cosechados</td>
										<td width="{{withTable}}">{{ (tabla.resumen.cosechados > 0) ? (tabla.resumen.cosechados) : '' }}</td>
										<td width="{{withTable}}">{{ (tabla.resumen.cosechados > 0) ? (tabla.resumen.cosechados / hectareas / num_semanas | number : 2) : '' }}</td>
										<td width="{{withTable}}">{{ (tabla.resumen.cosechados > 0) ? ((tabla.resumen.cosechados / hectareas / num_semanas) * 52 | number : 2) : '' }}</td>
									</tr>
									<tr>
										<td width="{{withTable}}" class="center-th">Recusados</td>
										<td width="{{withTable}}">{{ (tabla.resumen.recusados > 0) ? (tabla.resumen.recusados) : '' }}</td>
										<td width="{{withTable}}">{{ (tabla.resumen.recusados > 0) ? (tabla.resumen.recusados / hectareas / num_semanas | number : 2) : '' }}</td>
										<td width="{{withTable}}">{{ (tabla.resumen.recusados > 0) ? ((tabla.resumen.recusados / hectareas / num_semanas) * 52 | number : 2) : '' }}</td>
									</tr>
									<tr>
										<td width="{{withTable}}" class="center-th">Procesados</td>
										<td width="{{withTable}}">{{ (tabla.resumen.procesados > 0) ? (tabla.resumen.procesados) : '' }}</td>
										<td width="{{withTable}}">{{ (tabla.resumen.procesados > 0) ? (tabla.resumen.procesados / hectareas / num_semanas | number : 2) : '' }}</td>
										<td width="{{withTable}}">{{ (tabla.resumen.procesados > 0) ? ((tabla.resumen.procesados / hectareas / num_semanas) * 52 | number : 2) : '' }}</td>
									</tr>
									<tr role="row" class="heading subtittleTr">
										<th width="{{withTable}}" class="center-th"> <span class="Titlespan">Cajas</span> </th>
										<th width="{{withTable}}" class="center-th">  </th>
										<th width="{{withTable}}" class="center-th">  </th>
										<th width="{{withTable}}" class="center-th">  </th>
									</tr>
									<tr>
					                    <td width="{{withTable}}" class="center-th"> Cajas de Banapeel </td>
					                    <td width="{{withTable}}" class="center-th"> {{ (tabla.resumen.cajas_banapeel > 0) ? (tabla.resumen.cajas_banapeel | number : 2) : '' }} </td>
					                    <td width="{{withTable}}" class="center-th"> {{ (tabla.resumen.cajas_banapeel > 0) ? (tabla.resumen.cajas_banapeel / hectareas / num_semanas_cajas["banapeel"] | number : 2) : '' }} </td>
					                    <td width="{{withTable}}" class="center-th"> {{ (tabla.resumen.cajas_banapeel > 0) ? ((tabla.resumen.cajas_banapeel / hectareas / num_semanas_cajas["banapeel"]) * 52 | number : 2) : '' }} </td>
					                </tr>
									<tr>
					                    <td width="{{withTable}}" class="center-th"> Cajas de Battaglio </td>
					                    <td width="{{withTable}}" class="center-th"> {{ (tabla.resumen.cajas_battaglio > 0) ? (tabla.resumen.cajas_battaglio | number : 2) : '' }} </td>
					                    <td width="{{withTable}}" class="center-th"> {{ (tabla.resumen.cajas_battaglio > 0) ? (tabla.resumen.cajas_battaglio / hectareas / num_semanas_cajas["battaglio"] | number : 2) : ''}} </td>
					                    <td width="{{withTable}}" class="center-th"> {{ (tabla.resumen.cajas_battaglio > 0) ? ((tabla.resumen.cajas_battaglio / hectareas / num_semanas_cajas["battaglio"]) * 52 | number : 2) : ''}} </td>
					                </tr>
									<tr>
					                    <td width="{{withTable}}" class="center-th"> Cajas de Derby </td>
					                    <td width="{{withTable}}" class="center-th"> {{ (tabla.resumen.cajas_derby > 0) ? (tabla.resumen.cajas_derby | number : 2) : '' }} </td>
					                    <td width="{{withTable}}" class="center-th"> {{ (tabla.resumen.cajas_derby > 0) ? (tabla.resumen.cajas_derby / hectareas / num_semanas_cajas["derby"] | number : 2) : ''}} </td>
					                    <td width="{{withTable}}" class="center-th"> {{ (tabla.resumen.cajas_derby > 0) ? ((tabla.resumen.cajas_derby / hectareas / num_semanas_cajas["derby"]) * 52 | number : 2) : ''}} </td>
					                </tr>
									<tr>
					                    <td width="{{withTable}}" class="center-th"> Cajas de Dole Americano </td>
					                    <td width="{{withTable}}" class="center-th"> {{ (tabla.resumen.cajas_dole > 0) ? (tabla.resumen.cajas_dole | number : 2) : '' }} </td>
					                    <td width="{{withTable}}" class="center-th"> {{ (tabla.resumen.cajas_dole > 0) ? (tabla.resumen.cajas_dole / hectareas / num_semanas_cajas["dole americano"] | number : 2) : ''}} </td>
					                    <td width="{{withTable}}" class="center-th"> {{ (tabla.resumen.cajas_dole > 0) ? ((tabla.resumen.cajas_dole / hectareas / num_semanas_cajas["dole americano"]) * 52 | number : 2) : ''}} </td>
					                </tr>
									<tr>
					                    <td width="{{withTable}}" class="center-th"> Cajas Total </td>
					                    <td width="{{withTable}}" class="center-th"> {{ (tabla.resumen.cajas > 0) ? (tabla.resumen.cajas | number : 2) : '' }} </td>
					                    <td width="{{withTable}}" class="center-th"> {{ (tabla.resumen.cajas > 0) ? (tabla.resumen.cajas / hectareas / num_semanas_cajas_total | number : 2) : '' }} </td>
					                    <td width="{{withTable}}" class="center-th"> {{ (tabla.resumen.cajas > 0) ? ((tabla.resumen.cajas / hectareas / num_semanas_cajas_total) * 52 | number : 2) : '' }} </td>
					                </tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="portlet box green">
					<div class="portlet-title">
					    <span class="caption">Resumen</span>
					    <!-- <div class="tools">
				            <a href="javascript:;" class="expand" data-original-title="Expandir/Contraer" title=""> </a>
				        </div> -->
					</div>
					<div class="portlet-body">
						<div id="edad_promedio" style="height:350px;"></div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="portlet box green">
					<div class="portlet-title">
					    <span class="caption">Reporte de Producción</span>
					    <div class="tools">
				            <a href="javascript:;" class="expand" data-original-title="Expandir/Contraer" title=""> </a>
				        </div>
					</div>
					<div class="portlet-body" style="display: hidden">
						<div class="btn-group btn-group-devided" data-toggle="buttons">
                            <a class="btn blue btn-outline btn-circle btn-sm ng-binding" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false">
                                Columnas <i class="fa fa-angle-down"></i>
                            </a>
                            <ul class="dropdown-menu main pull-left">
                                <li id="col_racimos_cortados" data-event="racimos_cortados" class="active" ng-click="reviewColumns('col_racimos_cortados')">
                                    <a href="javascript:;">Racimos Cortados</a>
                                </li>
                                <li id="col_racimos_procesados" data-event="racimos_procesados" class="active" ng-click="reviewColumns('col_racimos_procesados')">
                                    <a href="javascript:;">Racimos Procesados</a>
                                </li>
                                <li data-event="peso_prom_cajas" ng-click="reviewColumns('col_peso_prom_cajas')">
                                    <a href="javascript:;">Peso Prom Cajas</a>
                                </li>
                                <li id="col_peso_prom_otras_cajas" data-event="peso_prom_otras_cajas" class="active" ng-click="reviewColumns('col_peso_prom_otras_cajas')">
                                    <a href="javascript:;">Peso Prom Otras Cajas</a>
                                </li>
                                <li id="col_calibracion" data-event="calibracion" ng-click="reviewColumns('col_calibracion')">
                                    <a href="javascript:;">Calibración</a>
                                </li>
                                <li id="col_ratooning" data-event="ratooning" ng-click="reviewColumns('col_ratooning')">
                                    <a href="javascript:;">Ratooning</a>
                                </li>
                                <li id="col_peso_prom_racimo" data-event="peso_prom_racimo" class="active" ng-click="reviewColumns('col_peso_prom_racimo')">
                                    <a href="javascript:;">Peso Prom Racimo</a>
                                </li>
                                <li id="col_prom_edad_cosecha" data-event="prom_edad_cosecha" ng-click="reviewColumns('col_prom_edad_cosecha')">
                                    <a href="javascript:;">Prom Edad Cosecha</a>
                                </li>
                                <li id="col_ratio_cortado" data-event="ratio_cortado" class="active" ng-click="reviewColumns('col_ratio_cortado')">
                                    <a href="javascript:;">Ratio Cortado</a>
                                </li>
                                <li id="col_ratio_procesado" data-event="ratio_procesado" class="active" ng-click="reviewColumns('col_ratio_procesado')">
                                    <a href="javascript:;">Ratio Procesado</a>
                                </li>
                                <li id="col_merma_cosechada" data-event="merma_cosechada" ng-click="reviewColumns('col_merma_cosechada')">
                                    <a href="javascript:;">% Merma Cosechada</a>
                                </li>
                                <li id="col_merma_procesada" data-event="merma_procesada" ng-click="reviewColumns('col_merma_procesada')">
                                    <a href="javascript:;">% Merma Procesada</a>
                                </li>
                                <li id="col_enfunde_ha" data-event="enfunde_ha" ng-click="reviewColumns('col_enfunde_ha')">
                                    <a href="javascript:;">Enfunde Por HA</a>
                                </li>
                                <li id="col_recobro" data-event="recobro" ng-click="reviewColumns('col_recobro')">
                                    <a href="javascript:;">Recobro</a>
                                </li>
                                <li id="col_merma_neta" data-event="merma_neta" class="active" ng-click="reviewColumns('col_merma_neta')">
                                    <a href="javascript:;">% Merma Neta</a>
                                </li>
                                <li id="col_merma_kg" data-event="merma_kg" ng-click="reviewColumns('col_merma_kg')">
                                    <a href="javascript:;">Merma (KG)</a>
                                </li>
                                <li id="col_merma_cajas" data-event="merma_cajas" ng-click="reviewColumns('col_merma_cajas')">
                                    <a href="javascript:;">Merma Cajas</a>
                                </li>
                                <li id="col_merma_dolares" data-event="merma_dolares" ng-click="reviewColumns('col_merma_dolares')">
                                    <a href="javascript:;">Merma Dolares</a>
                                </li>
                                <li id="col_manos" data-event="manos" ng-click="reviewColumns('col_manos')">
                                    <a href="javascript:;"># Manos</a>
                                </li>
                                <li id="col_largo_dedos" data-event="largo_dedos" ng-click="reviewColumns('col_largo_dedos')">
                                    <a href="javascript:;">Largo Dedos</a>
                                </li>
                            </ul>
                        </div>
						<div class="table-responsive">
							<table id="table_racimos" class="table table-striped table-bordered table-hover">
								<thead>
						                <tr role="row" class="heading">
						                    <th width="{{withTable}}" class="center-th">  </th>
						                    <th width="{{withTable}}" class="center-th col_racimos_cortados {{ columns.col_racimos_cortados ? '' : 'hide' }}"> Racimos<br>Cortados  </th>
						                    <th width="{{withTable}}" class="center-th col_racimos_procesados {{ columns.col_racimos_procesados ? '' : 'hide' }}"> Racimos<br>Procesados  </th>
						                    <th width="{{withTable}}" class="center-th col_peso_prom_cajas {{ columns.col_peso_prom_cajas ? '' : 'hide' }}"> Peso Prom<br>Caja  </th>
						                    <th width="{{withTable}}" class="center-th col_peso_prom_otras_cajas {{ columns.col_peso_prom_otras_cajas ? '' : 'hide' }}"> Peso Prom<br>Otras Caja </th>
						                    <th width="{{withTable}}" class="center-th col_calibracion {{ columns.col_calibracion ? '' : 'hide' }}"> Calibración </th>
						                    <th width="{{withTable}}" class="center-th col_ratooning {{ columns.col_ratooning ? '' : 'hide' }}"> Ratooning </th>
						                    <th width="{{withTable}}" class="center-th col_peso_prom_racimo {{ columns.col_peso_prom_racimo ? '' : 'hide' }}"> Peso Prom<br>Racimo  </th>
						                    <th width="{{withTable}}" class="center-th col_prom_edad_cosecha {{ columns.col_prom_edad_cosecha ? '' : 'hide' }}"> Prom Edad<br>Cosecha  </th>
						                    <th width="{{withTable}}" class="center-th col_ratio_cortado {{ columns.col_ratio_cortado ? '' : 'hide' }}"> Ratio<br>Cortado  </th>
						                    <th width="{{withTable}}" class="center-th col_ratio_procesado {{ columns.col_ratio_procesado ? '' : 'hide' }}"> Ratio<br>Procesado </th>
						                    <th width="{{withTable}}" class="center-th col_merma_cosechada {{ columns.col_merma_cosechada ? '' : 'hide' }}"> % Merma<br>Cosechada </th>
						                    <th width="{{withTable}}" class="center-th col_merma_procesada {{ columns.col_merma_procesada ? '' : 'hide' }}"> % Merma<br>Procesada </th>
						                    <th width="{{withTable}}" class="center-th col_enfunde_ha {{ columns.col_enfunde_ha ? '' : 'hide' }}"> Enfunde<br>por Ha </th>
						                    <th width="{{withTable}}" class="center-th col_recobro {{ columns.col_recobro ? '' : 'hide' }}"> Recobro </th>
						                    <th width="{{withTable}}" class="center-th col_merma_neta {{ columns.col_merma_neta ? '' : 'hide' }}"> % Merma<br>Neta </th>
						                    <th width="{{withTable}}" class="center-th col_merma_kg {{ columns.col_merma_kg ? '' : 'hide' }}"> Merma (Kg) </th>
						                    <th width="{{withTable}}" class="center-th col_merma_cajas {{ columns.col_merma_cajas ? '' : 'hide' }}"> Merma<br>Cajas </th>
						                    <th width="{{withTable}}" class="center-th col_merma_dolares {{ columns.col_merma_dolares ? '' : 'hide' }}"> Merma<br>Dolares </th>
						                    <th width="{{withTable}}" class="center-th col_manos {{ columns.col_manos ? '' : 'hide' }}"> # Manos </th>
						                </tr>
								<tbody>
									<tr ng-repeat-start="anual in tabla.produccion | orderObjectBy :'campo':false" ng-click="openDetalle(anual)">
										<td width="{{withTable}}" class="center-th">{{anual.campo}}</td>
										<td id="col_racimos" class="col_racimos_cortados {{ columns.col_racimos_cortados ? '' : 'hide' }}" width="{{withTable}}">
											{{  ((anual.total_cosechados <= 0) ? '' : (anual.total_cosechados | number : 2)) }}
										</td>
										<td class="col_racimos_procesados {{ columns.col_racimos_procesados ? '' : 'hide' }}" width="{{withTable}}">
											{{  ((anual.total_procesada <= 0) ? '' : (anual.total_procesada | number : 2)) }}
										</td>
										<td class="col_peso_prom_cajas {{ columns.col_peso_prom_cajas ? '' : 'hide' }}" width="{{withTable}}">
											{{  ((anual.peso_prom_cajas <= 0) ? '' : (anual.peso_prom_cajas | number : 2)) }}
										</td>
										<td class="col_peso_prom_otras_cajas {{ columns.col_peso_prom_otras_cajas ? '' : 'hide' }}" width="{{withTable}}">
											{{  ((anual.peso_prom_otras_cajas <= 0) ? '' : (anual.peso_prom_otras_cajas | number : 2)) }}
										</td>
										<td class="col_calibracion {{ columns.col_calibracion ? '' : 'hide' }}" width="{{withTable}}">
											{{  ((anual.calibracion <= 0) ? '' : (anual.calibracion | number : 2)) }}
										</td>
										<td class="col_ratooning {{ columns.col_ratooning ? '' : 'hide' }}" width="{{withTable}}">
											{{  ((anual.ratooning <= 0) ? '' : (anual.ratooning | number : 2)) }}
										</td>
										<td class="col_peso_prom_racimo {{ columns.col_peso_prom_racimo ? '' : 'hide' }}" width="{{withTable}}">
											{{  ((anual.peso <= 0) ? '' : (anual.peso | number : 2)) }}
										</td>
										<td class="col_prom_edad_cosecha {{ columns.col_prom_edad_cosecha ? '' : 'hide' }}" width="{{withTable}}">
											{{  ((anual.prom_edad_cosecha <= 0) ? '' : (anual.prom_edad_cosecha | number : 2)) }}
										</td>
										<td class="col_ratio_cortado {{ columns.col_ratio_cortado ? '' : 'hide' }}" width="{{withTable}}">
											{{ ((anual.ratio_cortado <= 0) ? '' : anual.ratio_cortado | number : 2) }}
										</td>
										<td class="col_ratio_procesado {{ columns.col_ratio_procesado ? '' : 'hide' }}" width="{{withTable}}">
											{{  ((anual.ratio_procesado <= 0) ? '' : (anual.ratio_procesado | number : 2)) }}
										</td>
										<td class="col_merma_cosechada {{ columns.col_merma_cosechada ? '' : 'hide' }}" width="{{withTable}}">
											{{  ((anual.merma_cosechada <= 0) ? '' : (anual.merma_cosechada | number : 2)) }}
										</td>
										<td class="col_merma_procesada {{ columns.col_merma_procesada ? '' : 'hide' }}" width="{{withTable}}">
											{{  ((anual.merma_procesada <= 0) ? '' : (anual.merma_procesada | number : 2)) }}
										</td>
										<td class="col_enfunde_ha {{ columns.col_enfunde_ha ? '' : 'hide' }}" width="{{withTable}}">
											{{  ((anual.enfunde_ha <= 0) ? '' : (anual.enfunde_ha | number : 2)) }}
										</td>
										<td class="col_recobro {{ columns.col_recobro ? '' : 'hide' }}" width="{{withTable}}">
											{{ ((anual.recobro <= 0) ? '' : (anual.recobro | number : 2)) }}
										</td>
										<td class="col_merma_neta {{ columns.col_merma_neta ? '' : 'hide' }}" width="{{withTable}}">
											{{ ((anual.merma_neta <= 0) ? '' : (anual.merma_neta | number : 2)) }}
										</td>
										<td class="col_merma_kg {{ columns.col_merma_kg ? '' : 'hide' }}" width="{{withTable}}">
											{{ ((anual.merma_kg <= 0) ? '' : (anual.merma_kg | number : 2)) }}
										</td>
										<td class="col_merma_cajas {{ columns.col_merma_cajas ? '' : 'hide' }}" width="{{withTable}}">
											{{ ((anual.merma_cajas <= 0) ? '' : (anual.merma_cajas | number : 2)) }}
										</td>
										<td class="col_merma_dolares {{ columns.col_merma_dolares ? '' : 'hide' }}" width="{{withTable}}">
											{{ ((anual.merma_dolares <= 0) ? '' : (anual.merma_dolares | number : 2)) }}
										</td>
										<td class="col_manos {{ columns.col_manos ? '' : 'hide' }}" width="{{withTable}}">
											{{ ((anual.manos <= 0) ? '' : (anual.manos | number : 2)) }}
										</td>
									</tr>
									<tr ng-show="anual.expanded" ng-repeat-end="">
										<td colspan="{{ visibleColumns }}" class="td-hidden" style="padding: 0;">
											<table  class="table table-striped table-bordered table-hover">
												<tbody>
													<tr ng-repeat-start="semana in anual.semanas | orderObjectBy :'campo':true" ng-click="openDetalle(semana)">
														<td width="{{withTable}}" class="center-th">{{semana.campo}}</td>
														<td class="col_racimos_cortados center-th {{ columns.col_racimos_cortados ? '' : 'hide' }}" width="{{withTable}}">
															{{  ((semana.total_cosechados <= 0) ? '' : (semana.total_cosechados | number : 2)) }}
														</td>
														<td class="col_racimos_procesados center-th {{ columns.col_racimos_procesados ? '' : 'hide' }}" width="{{withTable}}">
															{{  ((semana.total_procesada <= 0) ? '' : (semana.total_procesada | number : 2)) }}
														</td>
														<td class="col_peso_prom_cajas center-th {{ columns.col_peso_prom_cajas ? '' : 'hide' }}" width="{{withTable}}">
															{{  ((semana.peso_prom_cajas <= 0) ? '' : (semana.peso_prom_cajas | number : 2)) }}
														</td>
														<td class="col_peso_prom_otras_cajas {{ columns.col_peso_prom_otras_cajas ? '' : 'hide' }} center-th" width="{{withTable}}">
															{{  ((semana.peso_prom_otras_cajas <= 0) ? '' : (semana.peso_prom_otras_cajas | number : 2)) }}
														</td>
														<td class="col_calibracion {{ columns.col_calibracion ? '' : 'hide' }} center-th" width="{{withTable}}">
															{{  ((semana.calibracion <= 0) ? '' : (semana.calibracion | number : 2)) }}
														</td>
														<td class="col_ratooning {{ columns.col_ratooning ? '' : 'hide' }} center-th" width="{{withTable}}">
															{{  ((semana.ratooning <= 0) ? '' : (semana.ratooning | number : 2)) }}
														</td>
														<td class="col_peso_prom_racimo {{ columns.col_peso_prom_racimo ? '' : 'hide' }} center-th" width="{{withTable}}">
															{{  ((semana.peso <= 0) ? '' : (semana.peso | number : 2)) }}
														</td>
														<td class="col_prom_edad_cosecha {{ columns.col_prom_edad_cosecha ? '' : 'hide' }} center-th" width="{{withTable}}">
															{{  ((semana.prom_edad_cosecha <= 0) ? '' : (semana.prom_edad_cosecha | number : 2)) }}
														</td>
														<td class="col_ratio_cortado {{ columns.col_ratio_cortado ? '' : 'hide' }} center-th" width="{{withTable}}">
															{{ ((semana.ratio_cortado <= 0) ? '' : semana.ratio_cortado | number : 2) }}
														</td>
														<td class="col_ratio_procesado {{ columns.col_ratio_procesado ? '' : 'hide' }} center-th" width="{{withTable}}">
															{{  ((semana.ratio_procesado <= 0) ? '' : (semana.ratio_procesado | number : 2)) }}
														</td>
														<td class="col_merma_cosechada {{ columns.col_merma_cosechada ? '' : 'hide' }} center-th" width="{{withTable}}">
															{{  ((semana.merma_cosechada <= 0) ? '' : (semana.merma_cosechada | number : 2)) }}
														</td>
														<td class="col_merma_procesada {{ columns.col_merma_procesada ? '' : 'hide' }} center-th" width="{{withTable}}">
															{{  ((semana.merma_procesada <= 0) ? '' : (semana.merma_procesada | number : 2)) }}
														</td>
														<td class="col_enfunde_ha {{ columns.col_enfunde_ha ? '' : 'hide' }} center-th" width="{{withTable}}">
															{{  ((semana.enfunde_ha <= 0) ? '' : (semana.enfunde_ha | number : 2)) }}
														</td>
														<td class="col_recobro {{ columns.col_recobro ? '' : 'hide' }} center-th" width="{{withTable}}">
															{{ ((semana.recobro <= 0) ? '' : (semana.recobro | number : 2)) }}
														</td>
														<td class="col_merma_neta {{ columns.col_merma_neta ? '' : 'hide' }} center-th" width="{{withTable}}">
															{{ ((semana.merma_neta <= 0) ? '' : (semana.merma_neta | number : 2)) }}
														</td>
														<td class="col_merma_kg {{ columns.col_merma_kg ? '' : 'hide' }} center-th" width="{{withTable}}">
															{{ ((semana.merma_kg <= 0) ? '' : (semana.merma_kg | number : 2)) }}
														</td>
														<td class="col_merma_cajas {{ columns.col_merma_cajas ? '' : 'hide' }} center-th" width="{{withTable}}">
															{{ ((semana.merma_cajas <= 0) ? '' : (semana.merma_cajas | number : 2)) }}
														</td>
														<td class="col_merma_dolares {{ columns.col_merma_dolares ? '' : 'hide' }} center-th" width="{{withTable}}">
															{{ ((semana.merma_dolares <= 0) ? '' : (semana.merma_dolares | number : 2)) }}
														</td>
														<td class="col_manos {{ columns.col_manos ? '' : 'hide' }} center-th" width="{{withTable}}">
															{{ ((semana.manos <= 0) ? '' : (semana.manos | number : 2)) }}
														</td>

													</tr>
													<tr ng-show="semana.expanded" ng-repeat-end="">
														<td colspan="10" class="td-hidden">
															<table class="table table-striped table-bordered table-hover">
																<tbody>
																	<tr ng-repeat="lote in semana.lotes | orderObjectBy :'campo':false">
																		<td width="{{withTable}}" class="center-th">{{lote.campo}}</td>
																		<td class="col_racimos_cortados {{ columns.col_racimos_cortados ? '' : 'hide' }}" width="{{withTable}}">
																			{{  ((lote.total_cosechados <= 0) ? '' : (lote.total_cosechados | number : 2)) }}
																		</td>
																		<td class="col_racimos_procesados {{ columns.col_racimos_procesados ? '' : 'hide' }}" width="{{withTable}}">
																			{{  ((lote.total_procesada <= 0) ? '' : (lote.total_procesada | number : 2)) }}
																		</td>
																		<td class="col_peso_prom_cajas hide center-th" width="{{withTable}}">
																			{{  ((lote.peso_prom_cajas <= 0) ? '' : (lote.peso_prom_cajas | number : 2)) }}
																		</td>
																		<td class="col_peso_prom_otras_cajas hide center-th" width="{{withTable}}">
																			{{ ((lote.peso_prom_otras_cajas <= 0) ? '' : (lote.peso_prom_otras_cajas | number : 2)) }}
																		</td>
																		<td class="col_calibracion hide center-th" width="{{withTable}}">
																			{{  ((lote.calibracion <= 0) ? '' : (lote.calibracion | number : 2)) }}
																		</td>
																		<td class="col_ratooning hide center-th" width="{{withTable}}">
																			{{ ((lote.ratooning <= 0) ? '' : (lote.ratooning | number : 2)) }}
																		</td>
																		<td class="col_peso_prom_racimo center-th" width="{{withTable}}">
																			{{  ((lote.peso <= 0) ? '' : (lote.peso | number : 2)) }}
																		</td>
																		<td class="col_prom_edad_cosecha hide center-th" width="{{withTable}}">
																			{{  ((lote.prom_edad_cosecha <= 0) ? '' : (lote.prom_edad_cosecha | number : 2)) }}
																		</td>
																		<td class="col_ratio_cortado center-th" width="{{withTable}}">
																			{{ ((lote.ratio_cortado <= 0) ? '' : lote.ratio_cortado | number : 2) }}
																		</td>
																		<td class="col_ratio_procesado center-th" width="{{withTable}}">
																			{{ ((lote.ratio_procesado <= 0) ? '' : (lote.ratio_procesado | number : 2)) }}
																		</td>
																		<td class="col_merma_cosechada center-th" width="{{withTable}}">
																			{{  ((lote.merma_cosechada <= 0) ? '' : (lote.merma_cosechada | number : 2)) }}
																		</td>
																		<td class="col_merma_procesada center-th" width="{{withTable}}">
																			{{  ((lote.merma_procesada <= 0) ? '' : (lote.merma_procesada | number : 2)) }}
																		</td>
																		<td class="col_enfunde_ha center-th" width="{{withTable}}">
																			{{  ((lote.enfunde_ha <= 0) ? '' : (lote.enfunde_ha | number : 2)) }}
																		</td>
																		<td class="col_recobro hide center-th" width="{{withTable}}">
																			{{ ((lote.recobro <= 0) ? '' : (lote.recobro | number : 2)) }}
																		</td>
																		<td class="col_merma_neta hide center-th" width="{{withTable}}">
																			{{ ((lote.merma_neta <= 0) ? '' : (lote.merma_neta | number : 2)) }}
																		</td>
																		<td class="col_merma_kg hide center-th" width="{{withTable}}">
																			{{ ((lote.merma_kg <= 0) ? '' : (lote.merma_kg | number : 2)) }}
																		</td>
																		<td class="col_merma_cajas hide center-th" width="{{withTable}}">
																			{{ ((lote.merma_cajas <= 0) ? '' : (lote.merma_cajas | number : 2)) }}
																		</td>
																		<td class="col_merma_dolares hide center-th" width="{{withTable}}">
																			{{ ((lote.merma_dolares <= 0) ? '' : (lote.merma_dolares | number : 2)) }}
																		</td>
																		<td class="col_manos hide center-th" width="{{withTable}}">
																			{{ ((lote.manos <= 0) ? '' : (lote.manos | number : 2)) }}
																		</td>
																	</tr>
																</tbody>
															</table>
														</td>
													</tr>
												</tbody>
											</table>
										</td>
									</tr>	
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="portlet box green">
					<div class="portlet-title">
					    <span class="caption">Gráficas</span>
					    <div class="tools">
				            <!-- <a href="javascript:;" class="collapse" data-original-title="Expandir/Contraer" title=""> </a> -->
					        <!--<select onchange="cambiarCosecha()" style="float:right; font-size:100%; width:125px" ng-model="produccion.params.type" class="form-control form-filter input-sm" id="detalles_danhos">
								<option value="cosechados">Cosechados</option>
								<option value="recusados">Recusados</option>
								<option value="procesada">Procesada</option>
								<option value="cajas">Cajas</option>
							</select>-->
						</div>
					</div>
					<div class="portlet-body">
						<div class="row">
							<div class="col-md-12">
								<div id="racimos" style="height:500px;"></div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div id="peso" style="height:500px;"></div>
							</div>
							<div class="col-md-6">
								<div id="manos" style="height:500px;"></div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div id="calibracion" style="height:500px;"></div>
							</div>
							<div class="col-md-6">
								<div id="dedo" style="height:500px;"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>