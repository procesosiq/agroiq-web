
<style>
    .no-padding {
        padding : 0px !important;
    }
    .no-margin {
        margin : 0px !important;
    }
    #listado {
        min-height: 560px;
		max-height: 560px;
        overflow-y : scroll;
        overflow-x: hide;
        cursor: pointer;
    }
	#gmap_routes {
		min-height: 600px;
		max-height: 600px;
		min-width: 100%;
		max-width: 100%;
	}
	.margin-5 {
		margin-bottom: 5px !important;
	}
	.center-th {
		text-align: center !important;
	}
    .right-th {
        text-align: right !important;
    }
	.left-td {
		text-align: left !important;
		padding-left: 10px !important;
	}
	table {
		cursor: pointer !important;
	}
    table td, th {
        text-align: center;
    }
	.alginCenter {
		text-align: center !important;
		padding-top: 20px;
	}
	.portlet.box .dataTables_wrapper .dt-buttons{
        margin-top: 0px !important; 
        padding-bottom: 5px !important;
    }
    .fixedHeader-floating{top:60px!important;}
    .chart-container {
        height: 400px;
    }
    .animate-show-hide.ng-hide {
        opacity: 0;
    }

    .animate-show-hide.ng-hide-add,
    .animate-show-hide.ng-hide-remove {
        transition: all linear 0.5s;
    }

    .check-element {
        border: 1px solid black;
        opacity: 1;
        padding: 10px;
    }
    .btn{
        margin : 1px;
    }
    .margin-top{
        margin-top : 40px;
    }
    .center-text{
        text-align : center;
        border-radius: 3px; 
        border: 1px solid yellow;
    }

</style>
<div ng-controller="rastreo">
	<h3 class="page-title"> 
          Lancofruit
     </h3>
     <div class="page-bar">
         <ul class="page-breadcrumb">
             <li>
                 <i class="icon-home"></i>
                 <a>Revisión Lancofruit</a>
                 <i class="fa fa-angle-right"></i>
             </li>
         </ul>
         <div class="page-toolbar">
            <label for="anio">
                Año : 
            </label>
            <select id="anio" ng-change="lastSemana()" name="year" id="year" ng-model="filters.year" style="margin: 2px;height: 36px;">
                <option value="">Todos</option>
                <option ng-repeat="value in anios" value="{{value}}" ng-selected="value == filters.year">{{value}}</option>
            </select>
            <label for="semana">
                Semana : 
            </label>
            <select id="semana" ng-change="lastDias()" name="semana" id="semana" ng-model="filters.semana" style="margin: 2px;height: 36px;">
                <option value="">Todos</option>
                <option ng-repeat="value in semanas" value="{{value}}" ng-selected="value == filters.semana">{{value}}</option>
            </select>
            <!--<div class="padding-top col-md-4">
                <ng-calendarapp search="search"></ng-calendarapp>
            </div>-->
        </div>
    </div>
    <div id="indicadores">
    </div>
    <div class="portlet box green">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-gift"></i> Ventas </div>
            <div class="tools">
            </div>
        </div>
        <div class="portlet-body">
            <div class="panel-group accordion" id="accordion3">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a ng-click="request.all_pestana_uno()" class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_1" aria-expanded="false"> Por Dia </a>
                        </h4>
                    </div>
                    <div id="collapse_3_1" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <div  id="line-ventadia" class="chart"></div>
                                    </div>
                                    <div class="col-md-6">
                                        <div id="pastel-ventadia" class="chart"></div>
                                    </div>
                                </div>
                                <div ng-if="user.logged != 43" class="col-md-12 pull-right margin-top">
                                    <div class="col-md-2 pull-right">
                                        <button id="guardar" name="guardar" class="form-control btn blue" ng-click="editar()">Guardar</button>
                                    </div>
                                    <div  class="col-md-2 pull-right">
                                        <button id="editar" name="editar" class="form-control btn yellow-crusta" ng-click="activar()">Editar</button>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div id="tabla_venta_dia" class="table-responsive table-scrollable">
                                        <table class="table table-striped table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th width = "8%">SEMANA</th>
                                                    <th width = "6%">DÍA</th>
                                                    <th width = "10%">RUTA</th>
                                                    <th width = "9%">CAMION</th>
                                                    <th width = "16%">PUNTOS</th>
                                                    <th width = "10%">$ VENTAS</th>
                                                    <th width = "10%">GAVETAS</th>
                                                    <th width = "7%">$/PT</th>
                                                    <th width = "6%">$/MAX</th>
                                                    <th width = "6%">$/MIN</th>
                                                    <th width = "10%"></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr ng-repeat-start = "row in data_ventas_dia" ng-click="row.expanded = !row.expanded">
                                                    <td width = "8%">{{ row.semana }}</td>
                                                    <td width = "6%">{{ row.dia }}</td>
                                                    <td width = "10%"></td>
                                                    <td width = "9%"></td>
                                                    <td width = "16%">{{ row.puntos }}</td>
                                                    <td width = "10%">{{ row.venta }}</td>
                                                    <td width = "10%">{{ row.venta / 6}}</td>
                                                    <td width = "7%">{{ row.venta / row.puntos | number : 2}}</td>
                                                    <td width = "6%"></td>
                                                    <td width = "6%"></td>
                                                    <td width = "10%"><i class="fa fa-{{row.expanded ? 'minus' : 'plus' }}"></i></td>
                                                </tr>
                                                <tr ng-show = "row.expanded" ng-repeat-start="rutas in row.rutas" ng-click="rutas.expanded = !rutas.expanded">
                                                    <td width = "8%"></td>
                                                    <td width = "6%"></td>
                                                    <td width = "10%">{{ rutas.ruta }}</td>
                                                    <td width = "9%"></td>
                                                    <td width = "16%">{{ rutas.puntos_t }}</td>
                                                    <td width = "10%">{{ rutas.venta }}</td>
                                                    <td width = "10%">{{ rutas.venta / 6}}</td>
                                                    <td width = "7%">{{ rutas.venta / rutas.puntos_t | number : 2}}</td>
                                                    <td width = "6%"></td>
                                                    <td width = "6%"></td>
                                                    <td width = "10%"><i class="fa fa-{{rutas.expanded ? 'minus' : 'plus' }}"></i></td>
                                                </tr>
                                                <tr ng-show = "row.expanded && rutas.expanded" ng-repeat = "puntos in rutas.puntos">
                                                    <td width = "8%"></td>
                                                    <td width = "6%"></td>
                                                    <td width = "10%"></td>
                                                    <td width = "9%"></td>
                                                    <td width = "16%">{{puntos.puntos}}</td>
                                                    <td width = "10%">
                                                        <span ng-if="mostrar"> {{puntos.total}} </span>    
                                                        <input ng-if="!mostrar" type="text" name="total" id="total" class="form-control center-text" ng-model="puntos.total" ng-change="filters.total_modificado[puntos.id_venta] = puntos.total">
                                                    </td>
                                                    <td width = "10%">{{ puntos.total / 6}}</td>
                                                    <td width = "7%"></td>
                                                    <td width = "6%"></td>
                                                    <td width = "6%"></td>
                                                    <td width = "10%">
                                                        <button ng-if="user.logged != 43" id="eliminar" name="eliminar" class="form-control btn red-thunderbird" ng-click="eliminar(puntos.id_venta)">Eliminar</button>
                                                    </td>
                                                </tr>
                                                <tr ng-repeat-end="" ng-hide="true"></tr>
                                                <tr ng-repeat-end="" ng-hide="true"></tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a ng-click="request.all_pestana_dos()" class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_2" aria-expanded="false"> Por Semana </a>
                        </h4>
                    </div>
                    <div id="collapse_3_2" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <div  id="line-ventasemana" class="chart"></div>
                                    </div>
                                    <div class="col-md-6">
                                        <div id="pastel-ventasemana" class="chart"></div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div id="tabla_venta_semana" class="table-responsive table-scrollable">
                                        <table class="table table-striped table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th width = "12%">SEMANA</th>
                                                    <th width = "10%">RUTA</th>
                                                    <th width = "9%">CAMION</th>
                                                    <th width = "16%">PUNTOS</th>
                                                    <th width = "10%">$ VENTA</th>
                                                    <th width = "10%">GAVETAS</th>
                                                    <th width = "7%">$/PT</th>
                                                    <th width = "6%">$/MAX</th>
                                                    <th width = "6%">$/MIN</th>
                                                    <td width = "6%"></td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr ng-repeat-start = "row in data_ventas_semana" ng-click="row.expanded = !row.expanded">
                                                    <td width = "12%">{{ row.semana }}</td>
                                                    <td width = "10%"></td>
                                                    <td width = "9%"></td>
                                                    <td width = "16%">{{ row.puntos }}</td>
                                                    <td width = "10%">{{ row.venta }}</td>
                                                    <td width = "10%">{{ row.venta / 6 }}</td>
                                                    <td width = "7%">{{ row.venta / row.puntos | number : 2}}</td>
                                                    <td width = "6%"></td>
                                                    <td width = "6%"></td>
                                                    <td width = "6%"><i class="fa fa-{{row.expanded ? 'minus' : 'plus' }}"></i></td>
                                                </tr>
                                                <tr ng-show = "row.expanded" ng-repeat-start="rutas in row.rutas" ng-click="rutas.expanded = !rutas.expanded">
                                                    <td width = "12%"></td>
                                                    <td width = "10%">{{ rutas.ruta }}</td>
                                                    <td width = "9%"></td>
                                                    <td width = "16%">{{ rutas.puntos }}</td>
                                                    <td width = "10%">{{ rutas.venta }}</td>
                                                    <td width = "10%"><span>{{ rutas.venta / 6 }}</span></td>
                                                    <td width = "7%">{{ rutas.venta / rutas.puntos | number : 2}}</td>
                                                    <td width = "6%"></td>
                                                    <td width = "6%"></td>
                                                    <td width = "6%"><i class="fa fa-{{rutas.expanded ? 'minus' : 'plus' }}"></i></td>
                                                </tr>
                                                <tr ng-show = "row.expanded && rutas.expanded" ng-repeat-start="detalle in rutas.dias" ng-click="detalle.expanded = !detalle.expanded">
                                                    <td width = "12%">{{ detalle.dia }}</td>
                                                    <td width = "10%">{{ detalle.ruta }}</td>
                                                    <td width = "9%"></td>
                                                    <td width = "16%">{{ detalle.puntos_t }}</td>
                                                    <td width = "10%">{{ detalle.venta }}</td>
                                                    <td width = "10%">{{ detalle.venta / 6 }}</td>
                                                    <td width = "7%"></td>
                                                    <td width = "6%"></td>
                                                    <td width = "6%"></td>
                                                    <td width = "6%"><i class="fa fa-{{detalle.expanded ? 'minus' : 'plus' }}"></i></td>
                                                </tr>
                                                <tr ng-show = "row.expanded && rutas.expanded && detalle.expanded" ng-repeat = "puntos in detalle.puntos">
                                                    <td width = "12%"></td>
                                                    <td width = "10%"></td>
                                                    <td width = "9%"></td>
                                                    <td width = "16%">{{puntos.puntos}}</td>
                                                    <td width = "10%">{{puntos.total}}</td>
                                                    <td width = "10%">{{puntos.total / 6 }}</td>
                                                    <td width = "7%"></td>
                                                    <td width = "6%"></td>
                                                    <td width = "6%"></td>
                                                    <td width = "6%"></td>
                                                </tr>
                                                <tr ng-repeat-end="" ng-hide="true"></tr>
                                                <tr ng-repeat-end="" ng-hide="true"></tr>
                                                <tr ng-repeat-end="" ng-hide="true"></tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a ng-click="request.all_pestana_tres()" class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_3" aria-expanded="false"> Por Mes </a>
                        </h4>
                    </div>
                    <div id="collapse_3_3" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <div  id="line-ventames" class="chart"></div>
                                    </div>
                                    <div class="col-md-6">
                                        <div id="pastel-ventames" class="chart"></div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div id="tabla_venta_mes" class="table-responsive table-scrollable">
                                        <table class="table table-striped table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th width = "12%" >MES</th>
                                                    <th width = "10%">RUTA</th>
                                                    <th width = "9%">CAMION</th>
                                                    <th width = "33%">PUNTOS</th>
                                                    <th width = "10%">$ VENTAS</th>
                                                    <th width = "10%">GAVETAS</th>
                                                    <th width = "7%">$/PT</th>
                                                    <th width = "7%">$/MAX</th>
                                                    <th width = "6%">$/MIN</th>
                                                    <td width = "6%"></td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr ng-repeat-start = "row in data_ventas_mes" ng-click="row.expanded = !row.expanded">
                                                    <td width = "12%">{{ row.name_mes }}</td>
                                                    <td width = "10%"></td>
                                                    <td width = "9%"></td>
                                                    <td width = "33%">{{ row.puntos }}</td>
                                                    <td width = "10%">{{ row.venta }}</td>
                                                    <td width = "10%">{{ row.venta / 6}}</td>
                                                    <td width = "7%">{{ row.venta / row.puntos | number : 2}}</td>
                                                    <td width = "7%"></td>
                                                    <td width = "6%"></td>
                                                    <td width = "6%"><i class="fa fa-{{row.expanded ? 'minus' : 'plus' }}"></i></td>
                                                </tr>
                                                <tr ng-show="row.expanded" ng-repeat-start="sem in row.semanas" ng-click="sem.expanded = !sem.expanded">
                                                    <td width = "12%">{{ sem.semana }}</td>
                                                    <td width = "10%"></td>
                                                    <td width = "9%"></td>
                                                    <td width = "33%">{{ sem.puntos }}</td>
                                                    <td width = "10%">{{ sem.venta }}</td>
                                                    <td width = "10%">{{ sem.venta / 6}}</td>
                                                    <td width = "7%">{{ sem.venta / sem.puntos | number : 2}}</td>
                                                    <td width = "7%"></td>
                                                    <td width = "6%"></td>
                                                    <td width = "6%"><i class="fa fa-{{sem.expanded ? 'minus' : 'plus' }}"></i></td>
                                                </tr>
                                                <tr ng-show="row.expanded && sem.expanded" ng-repeat-start="rut in sem.rutas" ng-click="rut.expanded = !rut.expanded">
                                                    <td width = "12%"></td>
                                                    <td width = "10%">{{ rut.ruta }}</td>
                                                    <td width = "9%"></td>
                                                    <td width = "33%">{{ rut.puntos }}</td>
                                                    <td width = "10%">{{ rut.venta }}</td>
                                                    <td width = "10%">{{ rut.venta / 6}}</td>
                                                    <td width = "7%">{{ rut.venta / rut.puntos | number : 2}}</td>
                                                    <td width = "7%"></td>
                                                    <td width = "6%"></td>
                                                    <td width = "6%"><i class="fa fa-{{rut.expanded ? 'minus' : 'plus' }}"></i></td>
                                                </tr>
                                                <tr ng-show="row.expanded && sem.expanded && rut.expanded" ng-repeat-start="dia in rut.dias" ng-click="dia.expanded = !dia.expanded">
                                                    <td width = "12%">{{ dia.dia }}</td>
                                                    <td width = "10%">{{ dia.ruta }}</td>
                                                    <td width = "9%"></td>
                                                    <td width = "33%">{{ dia.puntos_t}}</td>
                                                    <td width = "10%">{{ dia.venta }}</td>
                                                    <td width = "10%">{{ dia.venta / 6}}</td>
                                                    <td width = "7%">{{ dia.venta / dia.puntos_t | number : 2}}</td>
                                                    <td width = "7%"></td>
                                                    <td width = "6%"></td>
                                                    <td width = "6%"><i class="fa fa-{{dia.expanded ? 'minus' : 'plus' }}"></i></td>
                                                </tr>
                                                <tr ng-show = "row.expanded && sem.expanded && rut.expanded && dia.expanded" ng-repeat = "puntos in dia.puntos">
                                                    <td width = "12%"></td>
                                                    <td width = "10%"></td>
                                                    <td width = "9%"></td>
                                                    <td width = "33%">{{puntos.puntos}}</td>
                                                    <td width = "10%">{{puntos.total}}</td>
                                                    <td width = "10%">{{puntos.total / 6 }}</td>
                                                    <td width = "7%"></td>
                                                    <td width = "7%"></td>
                                                    <td width = "6%"></td>
                                                    <td width = "6%"></td>
                                                </tr>
                                                <tr ng-repeat-end="" ng-hide="true"></tr>
                                                <tr ng-repeat-end="" ng-hide="true"></tr>
                                                <tr ng-repeat-end="" ng-hide="true"></tr>
                                                <tr ng-repeat-end="" ng-hide="true"></tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a ng-click="request.all_pestana_cuatro()" class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_4" aria-expanded="false"> Por Año </a>
                        </h4>
                    </div>
                    <div id="collapse_3_4" class="panel-collapse collapse" aria-expanded="true" style="">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <div  id="line-ventaanio" class="chart"></div>
                                    </div>
                                    <div class="col-md-6">
                                        <div id="pastel-ventaanio" class="chart"></div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div id="tabla_venta_anio" class="table-responsive table-scrollable">
                                        <table class="table table-striped table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th width = "12%">AÑO</th>
                                                    <th width = "10%">RUTA</th>
                                                    <th width = "9%">CAMION</th>
                                                    <th width = "16%">PUNTOS</th>
                                                    <th width = "10%">$ VENTAS</th>
                                                    <th width = "10%">GAVETAS</th>
                                                    <th width = "7%">$/PT</th>
                                                    <th width = "6%">$/MAX</th>
                                                    <th width = "6%">$/MIN</th>
                                                    <td width = "6%"></td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr ng-repeat-start = "row in data_ventas_anio" ng-click="row.expanded = !row.expanded">
                                                    <td width = "12%">{{ row.anio }}</td>
                                                    <td width = "10%"></td>
                                                    <td width = "9%"></td>
                                                    <td width = "16%">{{ row.puntos }}</td>
                                                    <td width = "10%">{{ row.venta }}</td>
                                                    <td width = "10%">{{ row.venta / 6}}</td>
                                                    <td width = "7%">{{ row.venta / row.puntos | number : 2}}</td>
                                                    <td width = "7%"></td>
                                                    <td width = "6%"></td>
                                                    <td width = "6%"><i class="fa fa-{{row.expanded ? 'minus' : 'plus' }}"></i></td>
                                                </tr>
                                                <tr ng-show="row.expanded" ng-repeat-start="mes in row.data_venta_mes" ng-click="mes.expanded = !mes.expanded">
                                                    <td width = "12%">{{ mes.name_mes }}</td>
                                                    <td width = "10%"></td>
                                                    <td width = "9%"></td>
                                                    <td width = "16%">{{ mes.puntos }}</td>
                                                    <td width = "10%">{{ mes.venta }}</td>
                                                    <td width = "10%">{{ mes.venta / 6}}</td>
                                                    <td width = "7%">{{ mes.venta / mes.puntos | number : 2}}</td>
                                                    <td width = "7%"></td>
                                                    <td width = "6%"></td>
                                                    <td width = "6%"><i class="fa fa-{{mes.expanded ? 'minus' : 'plus' }}"></i></td>
                                                </tr>
                                                <tr ng-show="row.expanded && mes.expanded" ng-repeat-start="sem in mes.semanas" ng-click="sem.expanded = !sem.expanded">
                                                    <td width = "12%">{{ sem.semana }}</td>
                                                    <td width = "10%"></td>
                                                    <td width = "9%"></td>
                                                    <td width = "16%">{{ sem.puntos }}</td>
                                                    <td width = "10%">{{ sem.venta }}</td>
                                                    <td width = "10%">{{ sem.venta / 6}}</td>
                                                    <td width = "7%">{{ sem.venta / sem.puntos | number : 2}}</td>
                                                    <td width = "7%"></td>
                                                    <td width = "6%"></td>
                                                    <td width = "6%"><i class="fa fa-{{sem.expanded ? 'minus' : 'plus' }}"></i></td>
                                                </tr>
                                                <tr ng-show="row.expanded && mes.expanded && sem.expanded" ng-repeat-start="rut in sem.rutas" ng-click="rut.expanded = !rut.expanded">
                                                    <td width = "12%"></td>
                                                    <td width = "10%">{{ rut.ruta }}</td>
                                                    <td width = "9%"></td>
                                                    <td width = "16%">{{ rut.puntos }}</td>
                                                    <td width = "10%">{{ rut.venta }}</td>
                                                    <td width = "10%">{{ rut.venta / 6}}</td>
                                                    <td width = "7%">{{ rut.venta / rut.puntos | number : 2}}</td>
                                                    <td width = "7%"></td>
                                                    <td width = "6%"></td>
                                                    <td width = "6%"><i class="fa fa-{{rut.expanded ? 'minus' : 'plus' }}"></i></td>
                                                </tr>
                                                <tr ng-show="row.expanded && mes.expanded && sem.expanded && rut.expanded" ng-repeat-start="dia in rut.dias" ng-click="dia.expanded = !dia.expanded">
                                                    <td width = "12%">{{ dia.dia }}</td>
                                                    <td width = "10%"></td>
                                                    <td width = "9%"></td>
                                                    <td width = "16%">{{ dia.puntos_t }}</td>
                                                    <td width = "10%">{{ dia.venta }}</td>
                                                    <td width = "10%">{{ dia.venta / 6}}</td>
                                                    <td width = "7%">{{ dia.venta / dia.puntos_t | number : 2}}</td>
                                                    <td width = "7%"></td>
                                                    <td width = "6%"></td>
                                                    <td width = "6%"><i class="fa fa-{{dia.expanded ? 'minus' : 'plus' }}"></i></td>
                                                </tr>
                                                <tr ng-show="row.expanded && mes.expanded && sem.expanded && rut.expanded && dia.expanded" ng-show = "dia.expanded" ng-repeat = "puntos in dia.puntos">
                                                    <td width = "12%"></td>
                                                    <td width = "10%"></td>
                                                    <td width = "9%"></td>
                                                    <td width = "16%">{{puntos.puntos}}</td>
                                                    <td width = "10%">{{puntos.total}}</td>
                                                    <td width = "10%">{{puntos.total / 6}}</td>
                                                    <td width = "7%"></td>
                                                    <td width = "7%"></td>
                                                    <td width = "6%"></td>
                                                    <td width = "6%"></td>
                                                </tr>
                                                <tr ng-repeat-end="" ng-hide="true"></tr>
                                                <tr ng-repeat-end="" ng-hide="true"></tr>
                                                <tr ng-repeat-end="" ng-hide="true"></tr>
                                                <tr ng-repeat-end="" ng-hide="true"></tr>
                                                <tr ng-repeat-end="" ng-hide="true"></tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row" ng-hide="true">
        <div class="col-md-12">
            <div class="portlet light portlet-fit bordered" id="map_box">
                <div class="portlet-title">
                    <div class="caption">
                        <i class=" icon-layers font-blue"></i>
                        <span class="caption-subject font-blue bold uppercase">Rastreo en Mapa</span>
                    </div>
                    <div class="actions">
                        <select class="form-control input-sm" ng-model="filters.fecha" ng-change="changeDate()">
                            <option value="{{fecha}}" ng-repeat="fecha in fechas" ng-selected="fecha == filters.fecha">{{fecha}}</option>
                            <option value="{{ now }}" ng-if="notToday" selected>{{ now }}</option>
                        </select>
                    </div>
                </div>
                <div class="portlet-body">
                    <button class="btn green" ng-click="trazarRuta()">Trazar Ruta</button>
                    <img id="live" src="https://media0.giphy.com/media/sS2LbFGIKoTqU/200.gif" alt="Tiempo Real" style="width: 100px; height : 30px;">

                    <div id="gmap_routes" class="gmaps"> </div>
                    <ol id="gmap_routes_instructions"> </ol>

                    <div id="table-posiciones">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="componentes/FilterableSortableTable.js"></script>

                                                                  