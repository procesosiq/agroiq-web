
<style>
    .no-padding {
        padding : 0px !important;
    }
    .no-margin {
        margin : 0px !important;
    }
    #listado {
        min-height: 560px;
		max-height: 560px;
        overflow-y : scroll;
        overflow-x: hide;
        cursor: pointer;
    }
	#gmap_routes {
		min-height: 600px;
		max-height: 600px;
		min-width: 100%;
		max-width: 100%;
	}
	.margin-5 {
		margin-bottom: 5px !important;
	}
	.center-th {
		text-align: center !important;
	}
    .right-th {
        text-align: right !important;
    }
	.left-td {
		text-align: left !important;
		padding-left: 10px !important;
	}
	table {
		cursor: pointer !important;
	}
    table td, th {
        text-align: center;
    }
	.alginCenter {
		text-align: center !important;
		padding-top: 20px;
	}
	.portlet.box .dataTables_wrapper .dt-buttons{
        margin-top: 0px !important; 
        padding-bottom: 5px !important;
    }
    .fixedHeader-floating{top:60px!important;}
    .chart-container {
        height: 400px;
    }
    .animate-show-hide.ng-hide {
        opacity: 0;
    }

    .animate-show-hide.ng-hide-add,
    .animate-show-hide.ng-hide-remove {
        transition: all linear 0.5s;
    }

    .check-element {
        border: 1px solid black;
        opacity: 1;
        padding: 10px;
    }
</style>

<script src="http://www.openlayers.org/api/OpenLayers.js"></script>
<script src="assets/global/plugins/open-layers/ExtendedRenderers.js"></script>

<div ng-controller="rastreo">
	<h3 class="page-title"> 
          Lancofruit
     </h3>
     <div class="page-bar">
         <ul class="page-breadcrumb">
             <li>
                 <i class="icon-home"></i>
                 <a>Revisión Lancofruit</a>
                 <i class="fa fa-angle-right"></i>
             </li>
         </ul>
         <div class="page-toolbar" style="display:flex">
            <label for="">
                Año : 
            </label>
            <select ng-change="changeYear()" name="year" id="year" ng-model="filters.year" style="margin: 2px;height: 36px;">
                <option ng-repeat="value in anios" value="{{value}}" ng-selected="value == filters.year">{{value}}</option>
            </select>
        </div>
    </div>
    <div class="portlet box green">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-gift"></i> Rastreo </div>
            <div class="tools">
            </div>
        </div>
        <div class="portlet-body">
            <div class="panel-group accordion" id="accordion3">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a ng-click="request.all_pestana_uno()" class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_1" aria-expanded="false"> Por Dia </a>
                        </h4>
                    </div>
                    <div id="collapse_3_1" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="table-responsive table-scrollable">
                                        <table class="table table-striped table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>DÍA</th>
                                                    <th>RUTA</th>
                                                    <th>CAMION</th>
                                                    <th>PUNTOS</th>
                                                    <th>MIN EFEC</th>
                                                    <th>M/PT</th>
                                                    <th>% EFEC</th>
                                                    <th>$ VENTAS</th>
                                                    <th>$/PT</th>
                                                    <th>$/MIN</th>
                                                    <td></td
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr ng-repeat-start = "row in data_ventas_dia" ng-click="row.expanded = !row.expanded">
                                                    <td>{{ row.fecha }}</td>
                                                    <td>{{ row.ruta }}</td>
                                                    <td></td>
                                                    <td>{{ row.puntos }}</td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>{{ row.venta }}</td>
                                                    <td>{{ row.venta / row.puntos | number : 2}}</td>
                                                    <td></td>
                                                    <td><i class="fa fa-{{row.expanded ? 'minus' : 'plus' }}"></i></td>
                                                </tr>
                                                <tr ng-repeat-end = "row" ng-repeat="detalle in row.detalle" ng-show="row.expanded">
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>{{ detalle.puntos }}</td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>{{detalle.total}}</td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-6 portlet light portlet-fit bordered">
                                        <div class="chart-container" id="chart_1">
                                        </div>
                                    </div>
                                    <div class="col-md-6 portlet light portlet-fit bordered">
                                        <div class="chart-container" id="chart_2">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a ng-click="request.all_pestana_dos()" class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_2" aria-expanded="false"> Por Semana </a>
                        </h4>
                    </div>
                    <div id="collapse_3_2" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                        <div class="panel-body">
                            <!-- <div class = "pull-right">
                                <label for="">
                                    Semana : 
                                </label>
                                <select ng-change="filtraSemana()" name="semana" id="semana" ng-model="filters.semana" style="margin: 2px;height: 36px;">
                                    <option ng-repeat="(key, value) in data_semanas" value="{{value}}" ng-selected = "filters.semana = value">{{value}}</option>
                                </select>
                            </div>-->
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="table-responsive table-scrollable">
                                        <table class="table table-striped table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th width = "12%">SEMANA</th>
                                                    <th width = "10%">RUTA</th>
                                                    <th width = "9%">CAMION</th>
                                                    <th width = "16%">PUNTOS</th>
                                                    <th width = "10%">MIN EFEC</th>
                                                    <th width = "7%">M/PT</th>
                                                    <th width = "7%">% EFEC</th>
                                                    <th width = "10%">$ VENTA</th>
                                                    <th width = "7%">$/PT</th>
                                                    <th width = "6%">$/MIN</th>
                                                    <td width = "6%"></td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr ng-repeat-start = "row in data_ventas_semana" ng-click="row.expanded = !row.expanded">
                                                    <td width = "12%">{{ row.fecha }}</td>
                                                    <td width = "10%">{{ row.ruta }}</td>
                                                    <td width = "9%"></td>
                                                    <td width = "16%">{{ row.puntos }}</td>
                                                    <td width = "10%"></td>
                                                    <td width = "7%"></td>
                                                    <td width = "7%"></td>
                                                    <td width = "10%">{{ row.venta }}</td>
                                                    <td width = "7%">{{ row.venta / row.puntos | number : 2}}</td>
                                                    <td width = "6%"></td>
                                                    <td width = "6%"><i class="fa fa-{{row.expanded ? 'minus' : 'plus' }}"></i></td>
                                                </tr>
                                                <tr ng-repeat-end = "row" ng-show="row.expanded">
                                                    <td colspan = "11" style="padding: 0;">    
                                                        <table class="table table-striped table-bordered table-hover">
                                                            <tbody>
                                                                <tr ng-repeat-start="detalle in row.detalle_por_semana" ng-click="detalle.expanded = !detalle.expanded">
                                                                    <td width = "12%">{{ detalle.fecha }}</td>
                                                                    <td width = "10%">{{ detalle.ruta }}</td>
                                                                    <td width = "9%"></td>
                                                                    <td width = "16%">{{ detalle.puntos }}</td>
                                                                    <td width = "10%"></td>
                                                                    <td width = "7%"></td>
                                                                    <td width = "7%"></td>
                                                                    <td width = "10%">{{ detalle.venta }}</td>
                                                                    <td width = "7%">{{ detalle.venta / detalle.puntos | number : 2}}</td>
                                                                    <td width = "6%"></td>
                                                                    <td width = "6%"><i class="fa fa-{{detalle.expanded ? 'minus' : 'plus' }}"></i></td>
                                                                </tr>
                                                                <tr ng-repeat-end = "detalle" ng-show = "detalle.expanded" ng-repeat = "detalle_dia in detalle.detalle_por_dia">
                                                                    <td width = "12%"></td>
                                                                    <td width = "10%"></td>
                                                                    <td width = "9%"></td>
                                                                    <td width = "16%">{{detalle_dia.puntos}}</td>
                                                                    <td width = "10%"></td>
                                                                    <td width = "7%"></td>
                                                                    <td width = "7%"></td>
                                                                    <td width = "10%">{{detalle_dia.total}}</td>
                                                                    <td width = "7%"></td>
                                                                    <td width = "6%"></td>
                                                                    <td width = "6%"></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-6 portlet light portlet-fit bordered">
                                        <div class="chart-container" id="chart_3">
                                        </div>
                                    </div>
                                    <div class="col-md-6 portlet light portlet-fit bordered">
                                        <div class="chart-container" id="chart_4">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a ng-click="request.all_pestana_tres()" class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_3" aria-expanded="false"> Por Mes </a>
                        </h4>
                    </div>
                    <div id="collapse_3_3" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="table-responsive table-scrollable">
                                        <table class="table table-striped table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th width = "12%" >MES</th>
                                                    <th width = "10%">RUTA</th>
                                                    <th width = "9%">CAMION</th>
                                                    <th width = "16%">PUNTOS</th>
                                                    <th width = "10%">MIN EFEC</th>
                                                    <th width = "7%">M/PT</th>
                                                    <th width = "7%">% EFEC</th>
                                                    <th width = "10%">$ VENTAS</th>
                                                    <th width = "7%">$/PT</th>
                                                    <th width = "6%">$/MIN</th>
                                                    <td width = "6%"></td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr ng-repeat-start = "row in data_ventas_mes" ng-click="row.expanded = !row.expanded">
                                                    <td width = "12%">{{ row.fecha }}</td>
                                                    <td width = "10%">{{ row.ruta }}</td>
                                                    <td width = "9%"></td>
                                                    <td width = "16%">{{ row.puntos }}</td>
                                                    <td width = "10%"></td>
                                                    <td width = "7%"></td>
                                                    <td width = "7%"></td>
                                                    <td width = "10%">{{ row.venta }}</td>
                                                    <td width = "7%">{{ row.venta / row.puntos | number : 2}}</td>
                                                    <td width = "6%"></td>
                                                    <td width = "6%"><i class="fa fa-{{row.expanded ? 'minus' : 'plus' }}"></i></td>
                                                </tr>
                                                <tr ng-repeat-end = "row" ng-show="row.expanded">
                                                    <td colspan = "11" style="padding: 0;">    
                                                        <table class="table table-striped table-bordered table-hover">
                                                            <tbody>
                                                                <tr ng-repeat-start="detalle in row.detalle_por_semana" ng-click="detalle.expanded = !detalle.expanded">
                                                                    <td width = "12%">{{ detalle.fecha }}</td>
                                                                    <td width = "10%">{{ detalle.ruta }}</td>
                                                                    <td width = "9%"></td>
                                                                    <td width = "16%">{{ detalle.puntos }}</td>
                                                                    <td width = "10%"></td>
                                                                    <td width = "7%"></td>
                                                                    <td width = "7%"></td>
                                                                    <td width = "10%">{{ detalle.venta }}</td>
                                                                    <td width = "7%">{{ detalle.venta / detalle.puntos | number : 2}}</td>
                                                                    <td width = "6%"></td>
                                                                    <td width = "6%"><i class="fa fa-{{detalle.expanded ? 'minus' : 'plus' }}"></i></td>
                                                                </tr>
                                                                <tr ng-repeat-end = "detalle" ng-show = "detalle.expanded">
                                                                    <td colspan = "11" style="padding: 0;">    
                                                                        <table class="table table-striped table-bordered table-hover">
                                                                            <tbody>
                                                                                <tr ng-repeat-start="detalle_dia in detalle.detalle_por_dia" ng-click="detalledia.expanded = !detalledia.expanded">
                                                                                    <td width = "12%">{{ detalle_dia.fecha }}</td>
                                                                                    <td width = "10%">{{ detalle_dia.ruta }}</td>
                                                                                    <td width = "9%"></td>
                                                                                    <td width = "16%">{{ detalle_dia.puntos }}</td>
                                                                                    <td width = "10%"></td>
                                                                                    <td width = "7%"></td>
                                                                                    <td width = "7%"></td>
                                                                                    <td width = "10%">{{ detalle_dia.venta }}</td>
                                                                                    <td width = "7%">{{ detalle_dia.venta / detalle_dia.puntos | number : 2}}</td>
                                                                                    <td width = "6%"></td>
                                                                                    <td width = "6%"><i class="fa fa-{{detalledia.expanded ? 'minus' : 'plus' }}"></i></td>
                                                                                </tr>
                                                                                <tr ng-repeat-end = "detalle_dia" ng-show = "detalledia.expanded" ng-repeat = "detalle_por_dia in detalle_dia.detalle_del_dia">
                                                                                    <td width = "12%"></td>
                                                                                    <td width = "10%"></td>
                                                                                    <td width = "9%"></td>
                                                                                    <td width = "16%">{{detalle_por_dia.puntos}}</td>
                                                                                    <td width = "10%"></td>
                                                                                    <td width = "7%"></td>
                                                                                    <td width = "7%"></td>
                                                                                    <td width = "10%">{{detalle_por_dia.total}}</td>
                                                                                    <td width = "7%"></td>
                                                                                    <td width = "6%"></td>
                                                                                    <td width = "6%"></td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-6 portlet light portlet-fit bordered">
                                        <div class="chart-container" id="chart_5">
                                        </div>
                                    </div>
                                    <div class="col-md-6 portlet light portlet-fit bordered">
                                        <div class="chart-container" id="chart_6">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a ng-click="request.all_pestana_cuatro()" class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_4" aria-expanded="false"> Por Año </a>
                        </h4>
                    </div>
                    <div id="collapse_3_4" class="panel-collapse collapse" aria-expanded="true" style="">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="table-responsive table-scrollable">
                                        <table class="table table-striped table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th width = "12%">AÑO</th>
                                                    <th width = "10%">RUTA</th>
                                                    <th width = "9%">CAMION</th>
                                                    <th width = "16%">PUNTOS</th>
                                                    <th width = "10%">MIN EFEC</th>
                                                    <th width = "7%">M/PT</th>
                                                    <th width = "7%">% EFEC</th>
                                                    <th width = "10%">$ VENTAS</th>
                                                    <th width = "7%">$/PT</th>
                                                    <th width = "6%">$/MIN</th>
                                                    <td width = "6%"></td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr ng-repeat-start = "row in data_ventas_anio" ng-click="row.expanded = !row.expanded">
                                                    <td width = "12%">{{ row.fecha }}</td>
                                                    <td width = "10%">{{ row.ruta }}</td>
                                                    <td width = "9%"></td>
                                                    <td width = "16%">{{ row.puntos }}</td>
                                                    <td width = "10%"></td>
                                                    <td width = "7%"></td>
                                                    <td width = "7%"></td>
                                                    <td width = "10%">{{ row.venta }}</td>
                                                    <td width = "7%">{{ row.venta / row.puntos | number : 2}}</td>
                                                    <td width = "6%"></td>
                                                    <td width = "6%"><i class="fa fa-{{row.expanded ? 'minus' : 'plus' }}"></i></td>
                                                </tr>
                                                <tr ng-repeat-end = "row" ng-show="row.expanded">
                                                    <td colspan = "11" style="padding: 0;">    
                                                        <table class="table table-striped table-bordered table-hover">
                                                            <tbody>
                                                                <tr ng-repeat-start="por_mes in row.por_mes" ng-click="pormes.expanded = !porsemana.expanded">
                                                                    <td width = "12%">{{ por_mes.fecha }}</td>
                                                                    <td width = "10%">{{ por_mes.ruta }}</td>
                                                                    <td width = "9%"></td>
                                                                    <td width = "16%">{{ por_mes.puntos }}</td>
                                                                    <td width = "10%"></td>
                                                                    <td width = "7%"></td>
                                                                    <td width = "7%"></td>
                                                                    <td width = "10%">{{ por_mes.venta }}</td>
                                                                    <td width = "7%">{{ por_mes.venta / por_mes.puntos | number : 2}}</td>
                                                                    <td width = "6%"></td>
                                                                    <td width = "6%"><i class="fa fa-{{porsemana.expanded ? 'minus' : 'plus' }}"></i></td>
                                                                </tr>
                                                                <tr ng-repeat-end = "por_mes" ng-show = "pormes.expanded">
                                                                    <td colspan = "11" style="padding: 0;">    
                                                                        <table class="table table-striped table-bordered table-hover">
                                                                            <tbody>
                                                                                <tr ng-repeat-start="por_semana in por_mes.por_semana" ng-click="porsemana.expanded = !porsemana.expanded">
                                                                                    <td width = "12%">{{ por_semana.fecha }}</td>
                                                                                    <td width = "10%">{{ por_semana.ruta }}</td>
                                                                                    <td width = "9%"></td>
                                                                                    <td width = "16%">{{ por_semana.puntos }}</td>
                                                                                    <td width = "10%"></td>
                                                                                    <td width = "7%"></td>
                                                                                    <td width = "7%"></td>
                                                                                    <td width = "10%">{{ por_semana.venta }}</td>
                                                                                    <td width = "7%">{{ por_semana.venta / por_semana.puntos | number : 2}}</td>
                                                                                    <td width = "6%"></td>
                                                                                    <td width = "6%"><i class="fa fa-{{detalledia.expanded ? 'minus' : 'plus' }}"></i></td>
                                                                                </tr>
                                                                                <tr ng-repeat-end = "por_semana" ng-show = "porsemana.expanded">
                                                                                    <td colspan = "11" style="padding: 0;">    
                                                                                        <table class="table table-striped table-bordered table-hover">
                                                                                            <tbody>
                                                                                                <tr ng-repeat-start="por_dia in por_semana.por_dia" ng-click="pordia.expanded = !pordia.expanded">
                                                                                                    <td width = "12%">{{ por_dia.fecha }}</td>
                                                                                                    <td width = "10%">{{ por_dia.ruta }}</td>
                                                                                                    <td width = "9%"></td>
                                                                                                    <td width = "16%">{{ por_dia.puntos }}</td>
                                                                                                    <td width = "10%"></td>
                                                                                                    <td width = "7%"></td>
                                                                                                    <td width = "7%"></td>
                                                                                                    <td width = "10%">{{ por_dia.venta }}</td>
                                                                                                    <td width = "7%">{{ por_dia.venta / por_dia.puntos | number : 2}}</td>
                                                                                                    <td width = "6%"></td>
                                                                                                    <td width = "6%"><i class="fa fa-{{pordia.expanded ? 'minus' : 'plus' }}"></i></td>
                                                                                                </tr>
                                                                                                <tr ng-repeat-end = "por_dia" ng-show = "pordia.expanded" ng-repeat = "detalle_por_dia in por_dia.detalle_por_dia">
                                                                                                    <td width = "12%"></td>
                                                                                                    <td width = "10%"></td>
                                                                                                    <td width = "9%"></td>
                                                                                                    <td width = "16%">{{detalle_por_dia.puntos}}</td>
                                                                                                    <td width = "10%"></td>
                                                                                                    <td width = "7%"></td>
                                                                                                    <td width = "7%"></td>
                                                                                                    <td width = "10%">{{detalle_por_dia.total}}</td>
                                                                                                    <td width = "7%"></td>
                                                                                                    <td width = "6%"></td>
                                                                                                    <td width = "6%"></td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-6 portlet light portlet-fit bordered">
                                        <div class="chart-container" id="chart_7">
                                        </div>
                                    </div>
                                    <div class="col-md-6 portlet light portlet-fit bordered">
                                        <div class="chart-container" id="chart_8">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN ROUTES PORTLET-->
            <div class="portlet light portlet-fit bordered" id="map_box">
                <div class="portlet-title">
                    <div class="caption">
                        <i class=" icon-layers font-blue"></i>
                        <span class="caption-subject font-blue bold uppercase">Rastreo en Mapa</span>
                    </div>
                    <div class="actions">
                        <select class="form-control" ng-model="filters.fecha" ng-change="changeDate()">
                            <option value="{{fecha}}" ng-repeat="fecha in fechas">{{fecha}}</option>
                            <option value="{{ now }}" ng-if="notToday" selected>{{ now }}</option>
                        </select>
                    </div>
                </div>
                <div class="portlet-body">
                    <img id="live" src="https://media0.giphy.com/media/sS2LbFGIKoTqU/200.gif" alt="Tiempo Real" style="width: 100px; height : 30px;">

                    <div id="gmap_routes" class="gmaps"> </div>
                    <ol id="gmap_routes_instructions"> </ol>
                </div>
            </div>
            <!-- END ROUTES PORTLET-->
        </div>
    </div>
</div>