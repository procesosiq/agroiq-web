<style>
	.accordion .panel .panel-title .accordion-toggle {
		display: block !important;
    	padding: 0px 14px !important;
	}
	.margin-5 {
		margin-bottom: 5px !important;
	}
	table > th,td{
		text-align: center !important;
	}
	.center-th {
		text-align: center !important;
	}
	.left-td {
		text-align: left !important;
		padding-left: 10px !important;
	}
	table {
		cursor: pointer !important;
	}
	.page-bar .page-toolbar{
		display: inline-flex !important;
	}
	.subContent {
		font-style: italic !important;
	}

	.no-padding{
		padding : 0px !important;
	}

	.widthTable {
		min-width : 50px;
		max-width : 50px;
	}

	.widthTable2 {
		min-width : 200px;
		max-width : 200px;
	}

	.widthNames {
		min-width : 300px;
		max-width : 300px;
	}

	.widthNamesTable {
		min-width : 500px;
		max-width : 500px;
	}
</style>
<script src="assets/global/plugins/FileSaver.min.js"></script>
<div ng-controller="bonificacion" ng-cloak>
	<h3 class="page-title"> 
          Bonificación
     </h3>
     <div class="page-bar" ng-init="bonificacion.nocache()">
         <ul class="page-breadcrumb">
             <li>
                 <i class="icon-home"></i>
                 <a>Bonificación</a>
                 <i class="fa fa-angle-right"></i>
             </li>
         </ul>
         <div class="page-toolbar">
		 	<div class="col-md-4">
				<label for="" style="width: 53%;padding: 8px;">Año: </label>
				<select ng-model="bonificacion.params.year" style="margin: 2px;height: 36px; width: 100%;" ng-change="init()">
					<option value="2019">2019</option>
					<option value="2018">2018</option>
					<option value="2017">2017</option>
				</select>
			</div>
			<div class="col-md-4" ng-show="historica.type == 'COSECHA'">
				<label for="" style="width: 53%;padding: 8px;">
					Personal : 
				</label>
				<select name="palanca" id="palanca" ng-model="bonificacion.params.palanca" onchange="convertModeSelect()"  style="margin: 2px;height: 36px; width: 100%;">
					<option value="">Todos</option>
					<option ng-repeat="(key, value) in personal" value="{{ value }}">{{ value }}</option>
				</select>
			</div>
			<div class="col-md-4" ng-show="historica.type == 'ENFUNDE' || historica.type == 'DESHOJE'">
				<label for="lote" style="width: 53%;padding: 8px;">
					Lote : 
				</label>
				<select onchange="convertModeSelect()" name="lote" id="lote" ng-model="bonificacion.params.lote" style="margin: 2px;height: 36px; width: 100%;">
					<option value="">Todos</option>
					<option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
					<option value="5">5</option>
					<option value="6">6</option>
					<option value="7">7</option>
					<option value="8">8</option>
					<option value="9">9</option>
					<option value="10">10</option>
				</select>
			</div>
			<div class="col-md-4">
				<label for="" style="width: 53%;padding: 8px;">
					Tipo : 
				</label>
				<select name="variable" id="variable" ng-model="bonificacion.params.variable" ng-change="init()" style="margin: 2px;height: 36px; width: 100%;">
					<option value="Peso"> Peso </option>
					<option value="% Merma"> Merma </option>
					<option value="Daños"> Daños </option>
				</select>
			</div>
			<div class="col-md-4">
				<label for="" style="width: 53%;padding: 8px;">
					Finca : 
				</label>
				<select name="idFinca" id="idFinca" ng-model="bonificacion.params.idFinca" ng-change="init()" style="margin: 2px;height: 36px; width: 100%;">
					<option ng-repeat="(key, value) in fincas" value="{{key}}" ng-selected="key == bonificacion.params.idFinca">{{value}}</option>
				</select>
			</div>
			<div class="col-md-4">
				<label for="" style="width: 60%;padding: 6px;">
					Labor: 
				</label>
				<select name="labor" id="labor" class="form-control" ng-model="bonificacion.params.labor" ng-change="init()" style="margin: 2px;height: 36px; width: 100%;">
					<option value="ENFUNDE" selected>ENFUNDE</option>
					<option value="COSECHA" selected>COSECHA</option>
					<option value="DESHOJE">DESHOJE</option>
				</select>
			</div>
        </div>  
    </div>

    <div class="page-bar">
        <div class="page-toolbar">
            <div class="col-md-12">
                <label class="control-label col-md-3">Umbral:</label>
                <input type="number" min="0" class="input-sm" id="umbral" name="umbral" ng-model="umbral" placeholder="10">
                <button type="button" ng-click="saveUmbral()" class="btn green">Guardar</button>
            </div>
        </div>
    </div>

    <div class="portlet box green hide">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-gift"></i>Configuracion </div>
			<div class="tools">
				<a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
			</div>
		</div>
		<div class="portlet-body form">
			<form action="#" class="form-horizontal">
				<div class="form-body">
					<div class="row">
						<h3 class="form-section col-md-3">Datos de {{configuracion.title}}</h3>
						<div class="col-md-offset-7 col-md-2">
							<button type="button" ng-click="configuracion.save()" class="btn green">Guardar</button>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<label class="control-label col-md-3">Umbral : </label>
								<div class="col-md-9">
									<input type="number" min="0" class="form-control" id="umbral" name="umbral" ng-model="umbral" placeholder="10">
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label class="control-label col-md-3">Bono : </label>
								<div class="col-md-9">
									<input type="number" min="0" class="form-control" id="bono" name="bono" ng-model="configuracion.data.bono" placeholder="90">
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label class="control-label col-md-4">Descuento : </label>
								<div class="col-md-8">
									<input type="number" min="0" class="form-control" id="descuento" name="descuento" ng-model="configuracion.data.descuento" placeholder="0">
								</div>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>

    <div class="portlet box green" id="contentTable">
		<div class="portlet-title">
		    <span class="caption">Bonificación</span>
		    <div class="tools">
				<button id="saveUmbrals" class="btn btn-primary inline hide" ng-click="saveUmbrals()">Guardar</button>
				<select name="semana" id="semana" class="input-sm" style="color: black;" ng-model="bonificacion.params.semana" ng-change="reloadTable()">
	           		<option value="">SEMANAS</option>
	           		<option ng-repeat="(key, value) in semanas_historico | orderObjectBy : 'semana'" ng-selected="bonificacion.params.semana == value.semana" value="{{value.semana}}">{{value.semana}}</option>
	           	</select>
				<div class="btn-group">
					<button class="btn btn-primary dropdown-toggle" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false">
						Exportar
					</button>
					<ul class="dropdown-menu" role="menu">
						<li><a href="javascript:;" ng-click="exportExcel('table_bonificacion', 'Bonificación')">Excel</a></li>
						<li><a href="javascript:;" ng-click="exportPdf('table_bonificacion')">PDF</a></li>
						<li><a href="javascript:;">Imprimir</a></li>
					</ul>
				</div>
	        </div>
		</div>
		<div class="portlet-body">
			<div class="table-responsive">
				<table class="table table-striped table-bordered table-hover" id="table_bonificacion">
					<thead>
			                <tr role="row" class="heading">
			                    <th class="center-th widthTable" ng-click="orderByField= 'lote';reverseSort = !reverseSort"> {{ bonificacion.params.labor != 'COSECHA' ? 'LOTE' : 'CODIGO' }} </th>
			                    <th class="center-th widthNames" ng-click="orderByField= 'enfundador';reverseSort = !reverseSort"> {{ bonificacion.params.labor != 'COSECHA' ? 'ENFUNDADOR' : 'PALANCA' }}  </th>
			                    <th class="center-th widthTable" ng-repeat="dia in dias" ng-bind="dia.fullname"></th>
			                    <th class="center-th widthTable" ng-click="orderByField= 'total';reverseSort = !reverseSort"> {{ (bonificacion.params.mode == 2 && id_company != 2) ? 'Total' : 'Prom' }} Sem </th> 
			                    <th class="center-th widthTable" ng-click="orderByField= 'exedente';reverseSort = !reverseSort"> Exced </th>                                                  
			                    <th class="center-th widthTable" ng-click="orderByField= 'usd';reverseSort = !reverseSort">  USD  </th>                                                  
			                </tr>
					</thead>
					<tbody>
						<tr ng-repeat-start="lote in lotes | orderObjectBy:'lote'" ng-click="lote.expanded = !lote.expanded">
							<td class="center-td">{{lote.lote}}</td>
							<td class="left-td">{{lote.enfundador}}</td>
							<td ng-repeat="dia in dias" >
								<span class="{{tagsFlags(umbral, lote.detalle[dia.nombre])}}"></span>
								{{ (lote.detalle[dia.nombre] > 0) ? (lote.detalle[dia.nombre] | number : 2) : '' }}
							</td>
							<td>
								<span class="{{tagsFlags(umbral, lote.prom_sem)}}"></span>
								{{lote.prom_lote | number : 2}}
							</td>
							<td>
								{{ lote.prom_lote - umbral | number : 2 }}
							</td>
							<td>{{ lote.usd | number : 2 }}</td>
						</tr>	
						<tr ng-show="lote.expanded" ng-repeat-end="" ng-repeat="defecto in lote.defectos">
							<td colspan="2" class="left-td" >{{defecto.defecto}}</td>
							<td ng-repeat="dia in dias" >
								<span class="{{tagsFlags(umbral, defecto.detalle[dia.nombre])}}"></span>
								{{ defecto.detalle[dia.nombre] }}
							</td>
							<td class="center-td "></td>
							<td class="center-td "></td>
                            <td class="center-td "></td>
						</tr>
					</tbody>
					<tfoot>
						<tr>
                            <th>Total: </th>
                            <th></th>
                            <th class="text-center" ng-repeat="dia in dias">
                                {{ totales[dia.nombre] | number: 2 }}
                            </th>
                            <th class="text-center">{{ totales.prom_lote | number: 2 }}</th>
                            <th class="text-center">{{ totales.prom_lote - umbral | number: 2 }}</th>
                            <th></th>
                        </tr>
					</tfoot>
				</table>
			</div>
		</div>
	</div>
    
	<div id="contentHistorico">
		<div class="portlet box green">
			<div class="portlet-title">
			    <span class="caption">Historió</span>
			    <!-- <div class="tools">
		        </div> -->
			</div>
			<div class="portlet-body">
				<div id="historico" style="height: 400px"></div>
			</div>
		</div>
		<div class="portlet box green">
			<div class="portlet-title">
			    <span class="caption">Historicó Semanal</span>
			    <div class="tools">
					 <div class="btn-group pull-right" style="margin-right: 5px;">
                        <button class="btn dark btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false"> 
                            Exportar <i class="fa fa-angle-down"></i>
                        </button>
                        <ul class="dropdown-menu pull-right">
                            <li>
                                <a href="javascript:;" ng-click="fnExcelReport('table_by_lote')">Excel</a>
                            </li>
                        </ul>
                    </div>
		        </div>
			</div>
			<div class="portlet-body">
				<div class="table-responsive" id="table_historico">
                    <table class="table table-stroped table-bordered table-hover" id="table_by_semana">
                        <thead>
                            <tr role="row" class="heading">
                                <th width="8.33%" class="center-th" ng-click="bonificacion.historica.orderByField= 'semana';reverseSort = !reverseSort"> SEM/LOTE </th>
                                <th width="8.33%" class="center-th" ng-repeat="(key, lote) in historico_tabla | orderObjectBy:'lote':false">{{ lote.lote }}</th>
                                <th width="8.33%" class="center-th" ng-click="bonificacion.historica.orderByField= 'total';reverseSort = !reverseSort"> Total </th>
                            </tr>
						</thead>
						<tbody>
                            <tr>
                                <th class="center-th">MAX</th>
                                <th class="center-th {{ checkUmbralHistorica(totales_historico[lote.lote].max) }}" ng-repeat="(key, lote) in historico_tabla">{{ totales_historico[lote.lote].max }}</th>
                                <th class="center-th {{ checkUmbralHistorica(tot) }}">{{ tot = (totales_historico | avgOfObject : 'max' | number: 2) }}</th>
                            </tr>
                            <tr>
                                <th class="center-th">MIN</th>
                                <th class="center-th {{ checkUmbralHistorica(totales_historico[lote.lote].min) }}" ng-repeat="(key, lote) in historico_tabla">{{ totales_historico[lote.lote].min }}</th>
                                <th class="center-th {{ checkUmbralHistorica(tot_min) }}">{{ tot_min = (totales_historico | avgOfObject : 'min' | number: 2) }}</th>
                            </tr>
                            <tr>
                                <th class="center-th">AVG</th>
                                <th class="center-th {{ checkUmbralHistorica(totales_historico[lote.lote].avg) }}" ng-repeat="(key, lote) in historico_tabla">{{ totales_historico[lote.lote].avg | number: 2 }}</th>
                                <th class="center-th {{ checkUmbralHistorica(tot_avg) }}">{{ tot_avg = (totales_historico | avgOfObject : 'avg' | number: 2) }}</th>
                            </tr>
                            <tr>
                                <td></td>
                            </tr>
							<tr ng-repeat-start="semana in historico_tabla_semana | orderObjectBy:bonificacion.historica.orderByField:bonificacion.historica.reverseSort" ng-click="openDetalle(semana)">
								<td class="center-td">{{semana.semana}}</td>
								<td ng-repeat="(key, lote) in historico_tabla | orderObjectBy:'lote':false" class="{{ checkUmbralHistorica(semana.detalle[lote.lote]) }}">
									{{
										semana.detalle[lote.lote] | number : 2
									}}
								</td>
								<td class="{{ checkUmbralHistorica(total) }}">
									{{ total = (semana.detalle | avgOfArray | number : 2) }}
								</td>
							</tr>
							<tr id="sub_{{semana.semana}}" ng-show="semana.expanded" ng-repeat-end="" ng-repeat="campo in semana.defectos" style="font-style: italic !important;">
								<td class="center-td">{{campo.defecto}}</td>
								<td ng-repeat="(key, lote) in historico_tabla | orderObjectBy:'lote':false">
									 <!--<span ng-if="campo.danos[lote.lote] > 0" class="{{tagsFlags(campo.danos[lote.lote])}}"></span> -->
                                     {{ campo.danos[lote.lote] }}
								</td>
								<td>
									<span ng-if="campo.total > 0" class="{{tagsFlags(campo.total)}}"></span>
									{{campo.total | number : 2}}
								</td>
							</tr>	
						</tbody>
						<tfoot>
							<!--<tr ng-repeat="total in totales_historico">
								<td>Total : </td>
								<td ng-repeat="dia in semanas_historico">
									{{total.cantidad[dia]  | number : 2}}
								</td>
								<td>{{total.defectos  | number : 2}}</td>
							</tr>-->
						</tfoot>
                    </table>
				</div>
                <div class="table-responsive hide">
                    <table class="table table-striped table-bordered table-hover" id="table_by_lote">
						<thead>
				                <tr role="row" class="heading">
				                    <th width="8.33%" class="center-th" ng-click="bonificacion.historica.orderByField= 'lote';reverseSort = !reverseSort"> LOTE/SEM </th>
				                    <th width="8.33%" class="center-th" ng-repeat="semana in semanas_historico">{{ semana.semana }}</th>
				                    <th width="8.33%" class="center-th" ng-click="bonificacion.historica.orderByField= 'total';reverseSort = !reverseSort"> Total </th>                                  
				                </tr>
						</thead>
						<tbody>
							<tr ng-repeat-start="lote in historico_tabla | orderObjectBy:'lote':false" ng-click="lote.expanded = !lote.expanded;">
								<td class="center-td">{{lote.lote}}</td>
								<td ng-repeat="semana in semanas_historico" class="{{ checkUmbralHistorica(lote.detalle[semana.semana]) }}">
									{{
										lote.detalle[semana.semana] | number : 2
									}}
								</td>
								<td class="{{ checkUmbralHistorica(total) }}">
									{{ total = (lote.detalle | avgOfArray | number : 2) }}
								</td>
							</tr>
							<tr id="sub_{{lote.lote}}" ng-show="lote.expanded" ng-repeat-end="" ng-repeat="campo in lote.defectos" style="font-style: italic !important;">
								<td class="center-td">{{campo.defecto}}</td>
								<td ng-repeat="semana in semanas_historico" >
									 <span ng-if="campo.valor[dia] > 0" class="{{tagsFlags(campo.valor[dia])}}"></span> 
                                     {{ campo.danos[semana.semana] }}
								</td>
								<td>
									<span ng-if="campo.total > 0" class="{{tagsFlags(campo.total)}}"></span>
									{{campo.total | number : 2}}
								</td>
							</tr>	
						</tbody>
						<!--<tfoot>
							<tr ng-repeat="total in totales_historico">
								<td>Total : </td>
								<td ng-repeat="dia in semanas_historico">
									{{total.cantidad[dia]  | number : 2}}
								</td>
								<td>{{total.defectos  | number : 2}}</td>
							</tr>
						</tfoot>-->
					</table>
                </div>
			</div>
		</div>
	</div>
</div>