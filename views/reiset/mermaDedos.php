    <style>
        .label_chart {
            background-color: #fff;
            padding: 2px;
            margin-bottom: 8px;
            border-radius: 3px 3px 3px 3px;
            border: 1px solid #E6E6E6;
            display: inline-block;
            margin: 0 auto;
        }
        .legendLabel{
            padding: 3px !important;
        }
    </style>
      <!-- BEGIN CONTENT BODY -->
<div ng-controller="controller" >
     <h3 class="page-title"> 
          Merma
     </h3>
     <div class="page-bar">
         <ul class="page-breadcrumb">
             <li>
                 <i class="icon-home"></i>
                 <a>Merma</a>
                 <i class="fa fa-angle-right"></i>
             </li>
         </ul>
        <div class="page-toolbar">
           
        </div>
     </div>

    
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-settings"></i>PROMEDIO DE DEDOS GEMELOS POR RACIMO
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse" data-original-title="Expandir/Contraer" title=""> </a>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="portlet-body">
                        <div id="table-dedos-prom"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="componentes/FilterableSortableTable.js"></script>