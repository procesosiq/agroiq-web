<style>
    .label_chart {
        background-color: #fff;
        padding: 2px;
        margin-bottom: 8px;
        border-radius: 3px 3px 3px 3px;
        border: 1px solid #E6E6E6;
        display: inline-block;
        margin: 0 auto;
    }
    .legendLabel{
        padding: 3px !important;
    }
    .portlet.box.green>.portlet-title, .portlet.green, .portlet>.portlet-body.green {
        background-color: #009739 !important;
    }
    .calendar-table td.available {
        background-color: green !important;
        color: white !important;
    }
    .icalendar-table td.available:hover {
        background-color: #285e8e !important;
        color: white !important;
    }
</style>
<div ng-controller="informe_merma_dia"  ng-cloak>
     <h3 class="page-title" ng-init="calidad.nocache()">
          Merma Día
     </h3>
     <div class="page-bar">
         <ul class="page-breadcrumb">
             <li>
                 <i class="icon-home"></i>
                 <a>Merma</a>
                 <i class="fa fa-angle-right"></i>
             </li>
         </ul>
        <div class="page-toolbar">
            <label ng-if="id_company == 7 || id_company == 6" for="">
                Finca : 
            </label>
            <select ng-if="id_company == 7 || id_company == 6" ng-change="cambiosFincas()" name="idFinca" id="idFinca" ng-model="calidad.params.idFinca" style="margin: 2px;height: 36px;">
                <option ng-repeat="(key, value) in fincas" value="{{key}}">{{value}}</option>
            </select>
            <label ng-if="id_company == 6" for="">
                Tipo de Merma : 
            </label>
            <select ng-if="id_company == 6" ng-change="cambiosMerma()" name="idMerma" id="idMerma" ng-model="calidad.params.idMerma" style="margin: 2px;height: 36px;">
                <option ng-repeat="(key, value) in mermas" value="{{key}}">{{value}}</option>
            </select>
            <label ng-if="id_company != 4" for="">
                Categoría : 
            </label>
            <select ng-change="cambiosCategoria()" name="categoria" id="categoria" ng-model="calidad.params.categoria" style="margin: 2px;height: 36px;">
                <option value="COSECHA">COSECHA</option>
                <option value="ENFUNDE">ENFUNDE</option>
            </select>
            <ng-calendarapp  search="search"></ng-calendarapp>
        </div>
     </div>
     <div id="reportes_all" ng-include="calidad.templatePath[calidad.step]" onload="last()">
            
     </div>  
</div>