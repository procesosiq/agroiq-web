<style>
	th, td{
		text-align: center !important;
	}
	.center-th {
		text-align: center !important;
	}
	table {
		cursor: pointer !important;
	}
	.anchoTable {
		width: 100px;
	}
    .delete  {
        background-color : #c8d8e8 !important;
    }
    .chart {
        height : 400px;
    }
    .form-control, .input-sm, select, .btn, .portlet {
        border-radius : 3px !important;
    }
    .highlight {
		background-color: green;
    	color: white !important;
	}
	.highlight:hover {
		background-color: #285e8e !important;
    	color: white !important;
	}
</style>
<div ng-controller="produccion" ng-cloak>
	<h3 class="page-title"> 
          Producción <small>{{ subTittle }}</small>
     </h3>
     <div class="page-bar" ng-init="getLastDay()">
         <ul class="page-breadcrumb">
             <li>
                 <i class="icon-home"></i>
                 <a>Producción</a>
                 <i class="fa fa-angle-right"></i>
             </li>
         </ul>
         <div class="page-toolbar">
            <button class="btn green-jungle" ng-disabled="cargandoHistorico" ng-click="exportarFormatoEspecial()">Excel</button>
		 	<label for="">
                Finca : 
            </label>
            <select ng-change="produccion.params.initial = 0; produccion.nocache()" name="finca" id="finca" ng-model="produccion.params.finca" style="margin: 2px;height: 36px;">
                <option value="">TODOS</option>
                <option ng-repeat="(key, value) in fincas" value="{{key}}" ng-selected="key == produccion.params.finca">{{value}}</option>
            </select>
            <label for="">
                Sector : 
            </label>
            <select ng-change="produccion.nocache()" name="sector" id="sector" ng-model="produccion.params.sector" style="margin: 2px;height: 36px;">
                <option value="">TODOS</option>
                <option ng-if="produccion.params.finca == '' || produccion.params.finca == 2" value="361">361</option>
                <option ng-if="produccion.params.finca == '' || produccion.params.finca == 3" value="948">948</option>
                <option ng-if="produccion.params.finca == '' || produccion.params.finca == 3" value="R45">R45</option>
            </select>
            <input id="datepicker" class="input-sm" sytle="height: 36px;" data-provide="datepicker" data-date-format="yyyy-mm-dd" readonly>
        </div>
    </div>

    <div id="tags">
    	<?php include('./views/reiset/tags_produccion_dia_demo.php') ?>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="portlet box green-haze">
				<div class="portlet-title">
					<span class="caption">RESUMEN POR EDAD</span>
				</div>
				<div class="portlet-body">
                    <div class="table-responsive" id="edades">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>EDAD</th>
                                    <th>CALIBRE SEGUNDA</th>
                                    <th>CALIBRE ULTIMA</th>
                                    <th>PROC</th>
                                    <th>RECU</th>
                                    <th>CORT</th>
                                    <th>%</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="row in resumen_edades | orderBy : 'edad'">
                                    <td class="{{row.class}}">{{row.edad}}</td>
                                    <td>{{row.calibracion | number: 2}}</td>
                                    <td>{{row.calibre_ultima | number: 2}}</td>
                                    <td>{{row.procesados}}</td>
                                    <td>{{row.recusados}}</td>
                                    <td>{{row.cosechados}}</td>
                                    <td>{{row.cosechados / (resumen_edades | sumOfValue : 'cosechados') * 100 | number: 2 }}%</td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>{{ resumen_edades | avgOfValue : 'edad' | number: 2 }}</th>
                                    <th>{{ (resumen_edades | avgOfValue : 'calibracion' | number: 2) || '' }}</th>
                                    <th>{{ (resumen_edades | avgOfValue : 'calibre_ultima' | number: 2) || '' }}</th>
                                    <th>{{ resumen_edades | sumOfValue : 'procesados' }}</th>
                                    <th>{{ resumen_edades | sumOfValue : 'recusados' }}</th>
                                    <th>{{ resumen_edades | sumOfValue : 'cosechados' }}</th>
                                    <th>100%</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="portlet box green-haze">
				<div class="portlet-title">
					<span class="caption">RESUMEN DE RECUSADOS</span>
				</div>
				<div class="portlet-body">
                    <div class="row">
                        <div class="table-responsive" id="recusados">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>CAUSA</th>
                                        <th>CANTIDAD</th>
                                        <th>PORCENTAJE</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="r in recusados">
                                        <td>{{ r.causa }}</td>
                                        <td>{{ r.cantidad }}</td>
                                        <td>{{ r.porcentaje }}</td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>RECUSADOS</th>
                                        <th>{{ recusados | sumOfValue : 'cantidad' | number }}</th>
                                        <th>100</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

	<div class="row">
		<div class="col-md-12">
			<div class="portlet box green-haze">
				<div class="portlet-title">
					<span class="caption">RESUMEN POR LOTE</span>
				</div>
				<div class="portlet-body" >
					<div class="table-responsive" id="promedios_lotes">
						<table class="table table-striped table-bordered table-hover">
							<thead>
                                <tr>
                                    <th class="center-th"></th>
									<th class="center-th" colspan="{{edades.length}}" ng-if="edades.length > 0">EDAD</th>
                                    <th class="center-th"></th>
                                    <th class="center-th"></th>
									<th class="center-th"></th>
                                    <th class="center-th"></th>
									<th class="center-th">CALIBRE</th>
									<th class="center-th">CALIBRE</th>
									<th class="center-th"></th>
                                </tr>
								<tr>
									<th class="center-th">LOTE</th>
									<th class="center-th {{ e.class }}" ng-repeat="e in edades | orderObjectBy:'edad'">{{ e.edad }}</th>
                                    <th class="center-th">COSE</th>
									<th class="center-th">PROC</th>
                                    <th class="center-th">RECU</th>
									<th class="center-th">PESO</th>
									<th class="center-th">SEGUNDA</th>
                                    <th class="center-th">ULTIMA</th>
									<th class="center-th">MANOS</th>
								</tr>
							</thead>
							<tbody>
								<tr ng-repeat="row in resumen">
									<td>{{ row.lote }}</td>
									<td ng-repeat="e in edades | orderObjectBy:'edad'">{{ row['edad_'+e.edad] }}</td>
                                    <td>{{ row.cosechados = ((row.procesados | num) + (row.recusados | num)) | number }}</td>
									<td>{{ row.procesados | number }}</td>
                                    <td>{{ row.recusados }}</td>
									<td>{{ row.peso }}</td>
									<td>{{ (row.calibre_segunda > 0) ? row.calibre_segunda : '' }}</td>
                                    <td>{{ (row.calibre_ultima > 0) ? row.calibre_ultima : '' }}</td>
									<td>{{ (row.manos > 0) ? row.manos : '' }}</td>
								</tr>
							</tbody>
                            <tfoot>
                                <tr>
                                    <th>TOTAL</th>
                                    <th class="center-th" ng-repeat="e in edades | orderObjectBy:'edad'">{{ resumen | sumOfValue : 'edad_'+e.edad }}</th>
                                    <th class="center-th">{{ resumen | sumOfValue : 'cosechados' | number }}</th>
                                    <th class="center-th">{{ resumen | sumOfValue : 'procesados' | number }}</th>
                                    <th class="center-th">{{ resumen | sumOfValue : 'recusados' | number }}</th>
                                    <th class="center-th">{{ resumen | avgOfValue : 'peso' | number: 2 }}</th>
                                    <th class="center-th">{{ resumen | avgOfValue : 'calibre_segunda' | number: 2 }}</th>
                                    <th class="center-th">{{ resumen | avgOfValue : 'calibre_ultima' | number: 2 }}</th>
                                    <th class="center-th">{{ resumen | avgOfValue : 'manos' | number: 2 }}</th>
                                </tr>
                            </tfoot>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>

    <div class="portlet box green-haze">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-gift"></i> Racimos </div>
            <div class="tools">
            </div>
        </div>
        <div class="portlet-body">
            <div class="panel-group accordion" id="accordion3">

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_7" aria-expanded="false"> Análisis de Defectos </a>
                        </h4>
                    </div>
                    <div id="collapse_3_7" class="panel-collapse collapse" aria-expanded="true" style="">
                        <div class="panel-body">
                            <div class="col-md-3 pull-right">
                                <select class="form-control" ng-model="produccion.params.var_recusado" ng-init="produccion.params.var_recusado = 'cant';" ng-change="getAnalisisRecusados()">
                                    <option value="cant">CANTIDAD</option>
                                    <option value="porc">% </option>
                                </select>
                            </div>
                            <div id="table-defectos"></div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_4" aria-expanded="false"> Base de Datos Racimos Formularios </a>
                        </h4>
                    </div>
                    <div id="collapse_3_4" class="panel-collapse collapse" aria-expanded="true" style="">
                        <div class="panel-body">
                            <div class="btn-group pull-right" style="margin-right: 5px;">
                                <button class="btn dark btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false"> 
                                    Exportar <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-right">
                                    <li>
                                        <a href="javascript:;" ng-click="exportPrint('registros')"> Imprimir </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;" ng-click="exportExcel('registros')">Excel</a>
                                    </li>
                                </ul>
                            </div>
                            <button class="btn red-thunderbird pull-right" style="height: 30px;" ng-click="eliminar()">Eliminar</button>
                            <button class="btn green-jungle pull-right" ng-show="editing" style="height: 30px;" ng-click="guardar()">Guardar</button>
                            <button class="btn btn-primary pull-right" ng-show="!editing" style="height: 30px;" ng-click="abrirModalAgregarRacimos()">Agregar</button>
                            <br>
                            <div class="table-responsive table-scrollable">
                                <table class="table table-striped table-bordered table-hover" id="edit_registros">
                                    <thead>
                                        <tr role="row" class="heading">
                                            <th class="center-th anchoTable" ng-click="setOrderTable('id')"> ID </th>
                                            <th class="center-th anchoTable" ng-click="setOrderTable('hora')"> Hora </th>
                                            <th class="center-th anchoTable" ng-click="setOrderTable(['viaje', 'num_racimo'])"> Viaje </th>
                                            <th class="center-th anchoTable" ng-click="setOrderTable(['viaje', 'num_racimo'])"> # Rac. </th>
                                            <th class="center-th anchoTable" ng-click="setOrderTable('cuadrilla')"> Cuadrilla  </th>
                                            <th class="center-th anchoTable" ng-click="setOrderTable('lote')"> Lote </th>
                                            <th class="center-th anchoTable" ng-click="setOrderTable('edad')"> Edad </th>
                                            <th class="center-th anchoTable" ng-click="setOrderTable('peso')"> LB </th>
                                            <th class="center-th anchoTable" ng-click="setOrderTable('manos')"> Manos </th>
                                            <th class="center-th anchoTable" ng-click="setOrderTable('calibre_segunda')"> Calibre Segunda </th>
                                            <th class="center-th anchoTable" ng-click="setOrderTable('calibre_ultima')"> Calibre Ultima </th>
                                            <th class="center-th anchoTable" style="min-width: 100px;" ng-click="setOrderTable('tipo')"> Tipo </th>
                                            <th class="center-th anchoTable" ng-click="setOrderTable('causa')"> Causa </th>
                                        </tr>
                                        <tr>
                                            <th><input type="text" class="form-control input-filter" ng-model="searchFilter.id" /></th>
                                            <th><input type="text" class="form-control input-filter" ng-model="searchFilter.hora" /></th>
                                            <th><input type="text" class="form-control input-filter" ng-model="searchFilter.viaje" /></th>
                                            <th><input type="text" class="form-control input-filter" ng-model="searchFilter.num_racimo" /></th>
                                            <th><input type="text" class="form-control input-filter" ng-model="searchFilter._cuadrilla" /></th>
                                            <th><input type="text" class="form-control input-filter" ng-model="searchFilter._lote" /></th>
                                            <th><input type="text" class="form-control input-filter" ng-model="searchFilter._edad" /></th>
                                            <th><input type="text" class="form-control input-filter" ng-model="searchFilter.peso" /></th>
                                            <th><input type="text" class="form-control input-filter" ng-model="searchFilter.manos" /></th>
                                            <th><input type="text" class="form-control input-filter" ng-model="searchFilter.calibre_segunda" /></th>
                                            <th><input type="text" class="form-control input-filter" ng-model="searchFilter.calibre_ultima" /></th>
                                            <th><input type="text" class="form-control input-filter" ng-model="searchFilter.tipo" /></th>
                                            <th><input type="text" class="form-control input-filter" ng-model="searchFilter.causa" /></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr class="{{ row.delete > 0 ? 'delete' : '' }}" 
                                            data-id="{{row.id}}" 
                                            data-marca="{{row.marca}}" 
                                            data-index="{{$index}}"
                                            ng-dblclick="edit(row)"
                                            ng-repeat="row in resultValueBD = (registros | filter: searchFilter | orderObjectBy : searchTable.orderBy : searchTable.reverse | startFrom: searchTable.startFrom | limitTo : searchTable.limit)">
                                            
                                            <td class="anchoTable">
                                                <input type="checkbox" ng-checked="row.delete == 1" ng-true-value="1" ng-false-value="0" ng-model="row.delete" />
                                                {{ row.id }}
                                            </td>
                                            <td class="anchoTable">{{ row.hora }}</td>
                                            <td class="anchoTable">{{ row.viaje }}</td>
                                            <td class="anchoTable">{{ row.num_racimo }}</td>
                                            <td class="anchoTable">
                                                <span ng-show="!row.editing">{{row.cuadrilla}}</span>
                                                <input type="text" class="form-control input-sm" ng-if="row.editing" ng-model="row.cuadrilla"/>
                                            </td>
                                            <td class="anchoTable">
                                                <span ng-show="!row.editing">{{ row.lote }}</span>
                                                <input type="text" class="form-control input-sm" ng-if="row.editing" ng-model="row.lote"/>
                                            </td>
                                            <td class="anchoTable {{ row.class }}">
                                                <span ng-show="!row.editing">{{ row.edad }}</span>
                                                <input type="text" class="form-control input-sm" ng-if="row.editing" ng-model="row.edad"/>
                                            </td>
                                            <td class="anchoTable">
                                                <span ng-show="!row.editing">{{ (row.peso > 0) ? (row.peso  | number : 2) : '' }}</span>
                                                <input type="text" class="form-control input-sm" ng-if="row.editing" ng-model="row.peso"/>
                                            </td>
                                            <td class="anchoTable">
                                                <span ng-show="!row.editing">{{ (row.manos > 0 && row.tipo != 'RECU') ? (row.manos  | number : 2) : '' }}</span>
                                                <input type="text" class="form-control input-sm" ng-if="row.editing && row.tipo != 'RECU'" ng-model="row.manos"/>
                                            </td>
                                            <td class="anchoTable">
                                                <span ng-show="!row.editing">{{ (row.calibre_segunda > 0 && row.tipo != 'RECU') ? (row.calibre_segunda  | number : 2) : '' }}</span>
                                                <input type="text" class="form-control input-sm" ng-if="row.editing && row.tipo != 'RECU'" ng-model="row.calibre"/>
                                            </td>
                                            <td class="anchoTable">
                                                <span ng-show="!row.editing">{{ (row.calibre_ultima > 0 && row.tipo != 'RECU') ? (row.calibre_ultima  | number : 2) : '' }}</span>
                                                <input type="text" class="form-control input-sm" ng-if="row.editing && row.tipo != 'RECU'" ng-model="row.calibre"/>
                                            </td>
                                            <td class="anchoTable">
                                                <span ng-show="!row.editing">{{ row.tipo == 'RECU' ? 'RECU' : row.tipo }}</span>
                                                <select class="form-control" ng-model="row.tipo" ng-if="row.editing">
                                                    <option value="PROC">PROC</option>
                                                    <option value="RECUSADO">RECU</option>
                                                </select>
                                            </td>
                                            <td class="anchoTable">
                                                <span ng-show="!row.editing && row.tipo == 'RECU'">{{ row.causa }}</span>
                                                <input type="text" class="form-control input-sm" ng-if="row.editing && row.tipo == 'RECU'" ng-model="row.causa"/>
                                            </td>
                                        </tr>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td>Total: {{ (resultValueBD | countOfValue:'id') | number }}</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>{{ (resultValueBD | avgOfValue:'edad') | number : 2 }}</td>
                                            <td>{{ (resultValueBD | sumOfValue:'peso') | number : 2 }}</td>
                                            <td>{{ (resultValueBD | avgOfValue:'manos') | number : 2 }}</td>
                                            <td>{{ (resultValueBD | avgOfValue:'calibre_segunda') | number : 2 }}</td>
                                            <td>{{ (resultValueBD | avgOfValue:'calibre_ultima') | number : 2 }}</td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td colspan="12">
                                                <div class="col-md-5 col-sm-12">
                                                    Página {{ (searchTable.actual_page | num) || 0 }} de {{ searchTable.numPages }}
                                                </div>
                                                <div class="col-md-7 col-sm-12">
                                                    <div class="pull-left">
                                                        <a class="btn btn-sm default prev" ng-click="prev(registros, searchTable)"><</a>
                                                        <input type="text" maxlength="5" class="pagination-panel-input form-control input-sm input-inline input-mini" ng-model="searchTable.actual_page" readonly/>
                                                        <a class="btn btn-sm default next" ng-click="next(registros, searchTable)">></a>

                                                        <select class="input-sm pull-right" style="color: #555;" ng-model="searchTable.limit" ng-change="searchTable.changePagination()">
                                                            <option value="{{ registros.length }}">TODOS</option>
                                                            <option ng-repeat="opt in searchTable.optionsPagination" value="{{ opt > 0 ? opt : '' }}" ng-selected="opt == searchTable.limit">{{ opt > 0 ? opt : 'TODOS' }}</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>

                                <table class="table table-striped table-bordered table-hover" style="display: none;" id="registros">
                                    <thead>
                                        <tr role="row" class="heading">
                                            <th class="center-th anchoTable"> ID </th>
                                            <th class="center-th anchoTable"> Hora </th>
                                            <th class="center-th anchoTable"> Viaje </th>
                                            <th class="center-th anchoTable"> # Rac. </th>
                                            <th class="center-th anchoTable"> Cuadrilla  </th>
                                            <th class="center-th anchoTable"> Lote </th>
                                            <th class="center-th anchoTable"> Edad </th>
                                            <th class="center-th anchoTable"> Cinta </th>
                                            <th class="center-th anchoTable"> LB </th>
                                            <th class="center-th anchoTable"> Manos </th>
                                            <th class="center-th anchoTable"> Calibre Segunda </th>
                                            <th class="center-th anchoTable"> Calibre Ultima </th>
                                            <th class="center-th anchoTable" > Tipo </th>
                                            <th class="center-th anchoTable"> Causa </th>
                                        </tr>
                                    <tbody>
                                        <tr ng-repeat="row in resultValueBD">
                                            <td class="anchoTable">{{ row.id }}</td>
                                            <td class="anchoTable">{{ row.hora }}</td>
                                            <td class="anchoTable">{{ row.viaje }}</td>
                                            <td class="anchoTable">{{ row.num_racimo }}</td>
                                            <td class="anchoTable">{{row.cuadrilla}}</td>
                                            <td class="anchoTable">{{ row.lote }}</td>
                                            <td class="anchoTable">{{ row.edad }}</td>
                                            <td class="anchoTable">{{ row.cinta }}</td>
                                            <td class="anchoTable">{{ (row.peso > 0) ? (row.peso  | number : 2) : '' }}</td>
                                            <td class="anchoTable">{{ (row.manos > 0 && row.tipo != 'RECU') ? (row.manos  | number : 2) : '' }}</td>
                                            <td class="anchoTable">{{ (row.calibre_segunda > 0 && row.tipo != 'RECU') ? (row.calibre_segunda  | number : 2) : '' }}</td>
                                            <td class="anchoTable">{{ (row.calibre_ultima > 0 && row.tipo != 'RECU') ? (row.calibre_ultima  | number : 2) : '' }}</td>
                                            <td class="anchoTable">{{ row.tipo == 'RECU' ? 'RECU' : row.tipo  }}</td>
                                            <td class="anchoTable">{{ row.causa }}</td>
                                        </tr>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td>Total:</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>{{ (resultValueBDExportar | avgOfValue:'edad') | number : 2 }}</td>
                                            <td></td>
                                            <td>{{ (resultValueBDExportar | sumOfValue:'peso') | number : 2 }}</td>
                                            <td>{{ (resultValueBDExportar | sumOfValue:'manos') | number : 2 }}</td>
                                            <td>{{ (resultValueBDExportar | sumOfValue:'calibre_segunda') | number : 2 }}</td>
                                            <td>{{ (resultValueBDExportar | sumOfValue:'calibre_ultima') | number : 2 }}</td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_8" aria-expanded="false"> Base de Datos Racimos Balanza </a>
                        </h4>
                    </div>
                    <div id="collapse_3_8" class="panel-collapse collapse" aria-expanded="true" style="">
                        <div class="panel-body">
                            <div class="btn-group pull-right" style="margin-right: 5px;">
                                <button class="btn dark btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false"> 
                                    Exportar <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-right">
                                    <li>
                                        <a href="javascript:;" ng-click="exportPrint('registros_balanza')"> Imprimir </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;" ng-click="exportExcel('registros_balanza')">Excel</a>
                                    </li>
                                </ul>
                            </div>
                            <br>
                            <div class="table-responsive table-scrollable">
                                <table class="table table-striped table-bordered table-hover" id="edit_registros_balanza">
                                    <thead>
                                        <tr role="row" class="heading">
                                            <th class="center-th anchoTable" ng-click="setOrderTable('id')"> ID </th>
                                            <th class="center-th anchoTable" ng-click="setOrderTable('hora')"> Hora </th>
                                            <th class="center-th anchoTable" ng-click="setOrderTable(['viaje', 'num_racimo'])"> Viaje </th>
                                            <th class="center-th anchoTable" ng-click="setOrderTable(['viaje', 'num_racimo'])"> # Rac. </th>
                                            <th class="center-th anchoTable" ng-click="setOrderTable('cuadrilla')"> Cuadrilla  </th>
                                            <th class="center-th anchoTable" ng-click="setOrderTable('lote')"> Lote </th>
                                            <th class="center-th anchoTable" ng-click="setOrderTable('peso')"> LB </th>
                                        </tr>
                                        <tr>
                                            <th><input type="text" class="form-control input-filter" ng-change="cleanFilter(searchFilterBalanza)" ng-model="searchFilterBalanza.id" /></th>
                                            <th><input type="text" class="form-control input-filter" ng-change="cleanFilter(searchFilterBalanza)" ng-model="searchFilterBalanza.hora" /></th>
                                            <th><input type="text" class="form-control input-filter" ng-change="cleanFilter(searchFilterBalanza)" ng-model="searchFilterBalanza.viaje" /></th>
                                            <th><input type="text" class="form-control input-filter" ng-change="cleanFilter(searchFilterBalanza)" ng-model="searchFilterBalanza.num_racimo" /></th>
                                            <th><input type="text" class="form-control input-filter" ng-change="cleanFilter(searchFilterBalanza)" ng-model="searchFilterBalanza.cuadrilla" /></th>
                                            <th><input type="text" class="form-control input-filter" ng-change="cleanFilter(searchFilterBalanza)" ng-model="searchFilterBalanza.lote" /></th>
                                            <th><input type="text" class="form-control input-filter" ng-change="cleanFilter(searchFilterBalanza)" ng-model="searchFilterBalanza.peso" /></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat="row in resultValueBDBalanza = (registrosBalanza | filter : searchFilterBalanza | orderObjectBy : searchTableBalanza.orderBy : searchTableBalanza.reverse | startFrom: searchTableBalanza.startFrom | limitTo : searchTableBalanza.limit)">
                                            
                                            <td class="anchoTable">
                                                {{ row.id }}
                                            </td>
                                            <td class="anchoTable">{{ row.hora }}</td>
                                            <td class="anchoTable">{{ row.viaje }}</td>
                                            <td class="anchoTable">{{ row.num_racimo }}</td>
                                            <td class="anchoTable">
                                                <span>{{row.cuadrilla}}</span>
                                            </td>
                                            <td class="anchoTable">
                                                <span>{{ row.lote }}</span>
                                            </td>
                                            <td class="anchoTable">
                                                <span>{{ (row.peso > 0) ? (row.peso  | number : 2) : '' }}</span>
                                            </td>
                                        </tr>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td>Total: {{ (resultValueBDBalanza | countOfValue:'id') | number }}</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>{{ (resultValueBDBalanza | sumOfValue:'peso') | number : 2 }}</td>
                                        </tr>
                                        <tr>
                                            <td colspan="12">
                                                <div class="col-md-5 col-sm-12">
                                                    Página {{ (searchTableBalanza.actual_page | num) || 0 }} de {{ searchTableBalanza.numPages }}
                                                </div>
                                                <div class="col-md-7 col-sm-12">
                                                    <div class="pull-left">
                                                        <a class="btn btn-sm default prev" ng-click="prev(registros, searchTableBalanza)"><</a>
                                                        <input type="text" maxlength="5" class="pagination-panel-input form-control input-sm input-inline input-mini" ng-model="searchTableBalanza.actual_page" readonly/>
                                                        <a class="btn btn-sm default next" ng-click="next(registros, searchTableBalanza)">></a>

                                                        <select class="input-sm pull-right" style="color: #555;" ng-model="searchTableBalanza.limit" ng-change="searchTableBalanza.changePagination()">
                                                            <option value="{{ registros.length }}">TODOS</option>
                                                            <option ng-repeat="opt in searchTableBalanza.optionsPagination" value="{{ opt > 0 ? opt : '' }}" ng-selected="opt == searchTableBalanza.limit">{{ opt > 0 ? opt : 'TODOS' }}</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>

                                <table class="table table-striped table-bordered table-hover" style="display: none;" id="registros_balanza">
                                    <thead>
                                        <tr role="row" class="heading">
                                            <th class="center-th anchoTable"> ID </th>
                                            <th class="center-th anchoTable"> Hora </th>
                                            <th class="center-th anchoTable"> Viaje </th>
                                            <th class="center-th anchoTable"> # Rac. </th>
                                            <th class="center-th anchoTable"> Cuadrilla  </th>
                                            <th class="center-th anchoTable"> Lote </th>
                                            <th class="center-th anchoTable"> LB </th>
                                        </tr>
                                    <tbody>
                                        <tr ng-repeat="row in resultValueBDBalanza">
                                            <td class="anchoTable">{{ row.id }}</td>
                                            <td class="anchoTable">{{ row.hora }}</td>
                                            <td class="anchoTable">{{ row.viaje }}</td>
                                            <td class="anchoTable">{{ row.num_racimo }}</td>
                                            <td class="anchoTable">{{row.cuadrilla}}</td>
                                            <td class="anchoTable">{{ row.lote }}</td>
                                            <td class="anchoTable">{{ (row.peso > 0) ? (row.peso  | number : 2) : '' }}</td>
                                        </tr>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td>Total:</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>{{ (resultValueBDBalanza | sumOfValue:'peso') | number : 2 }}</td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_5" aria-expanded="false"> Viajes Formularios </a>
                        </h4>
                    </div>
                    <div id="collapse_3_5" class="panel-collapse collapse" aria-expanded="true" style="">
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Hora Serv.</th>
                                            <th>Hora Envio</th>
                                            <th>Viaje</th>
                                            <th>Lote</th>
                                            <th>Palanca</th>
                                            <th># Racimos</th>
                                            <th style="min-width: 200px;">Referencias</th>
                                            <th></th>
                                        </tr>
                                        <tr>
                                            <th><input type="text" class="form-control" ng-change="cleanFilter(filterViajes)" ng-model="filterViajes.hora_llegada_servidor"></th>
                                            <th><input type="text" class="form-control" ng-change="cleanFilter(filterViajes)" ng-model="filterViajes.hora"></th>
                                            <th><input type="text" class="form-control" ng-change="cleanFilter(filterViajes)" ng-model="filterViajes.viaje"></th>
                                            <th><input type="text" class="form-control" ng-change="cleanFilter(filterViajes)" ng-model="filterViajes.lote"></th>
                                            <th><input type="text" class="form-control" ng-change="cleanFilter(filterViajes)" ng-model="filterViajes.palanca"></th>
                                            <th><input type="text" class="form-control" ng-change="cleanFilter(filterViajes)" ng-model="filterViajes.racimos"></th>
                                            <th><input type="text" class="form-control" ng-change="cleanFilter(filterViajes)" ng-model="filterViajes.json"></th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr 
                                            ng-repeat="row in viajesFiltrados = (viajes | filter: filterViajes)" 
                                            ng-init="row.jsons = row.referencias.split(',')" 
                                            class="{{ row.jsons.length > 1 ? 'red' : '' }}">

                                            <td>{{ row.hora_llegada_servidor }}</td>
                                            <td>{{ row.hora }}</td>
                                            <td>{{ row.viaje }}</td>
                                            <td>
                                                <span ng-dblclick="editViaje(row, 'loteEditing')" ng-if="row.jsons.length == 1 && !row.loteEditing">{{ row.lote }}</span>
                                                <span ng-if="row.jsons.length > 1 && !row.loteEditing" ng-repeat="lote in row.lote.split(',')">
                                                    {{ lote }} <br />
                                                </span>
                                                <input enter="saveViaje(row, 'lote')" ng-if="row.loteEditing" type="text" class="form-control" ng-model="row.lote">
                                            <td>
                                                <span ng-dblclick="editViaje(row, 'palancaEditing')" ng-if="row.jsons.length == 1 && !row.palancaEditing">{{ row.palanca }}</span>
                                                <span ng-if="row.jsons.length > 1 && !row.palancaEditing" ng-repeat="palanca in row.palanca.split(',')">
                                                    {{ palanca }} <br />
                                                </span>
                                                <input enter="saveViaje(row, 'palanca')" ng-if="row.palancaEditing" type="text" class="form-control" ng-model="row.palanca">
                                            </td>
                                            <td>{{ row.racimos }}</td>
                                            <td>
                                                <span ng-repeat="json in row.jsons">
                                                    <a download target="_blank" href="https://s3.amazonaws.com/json-publicos/banano/reiset/racimosCosechados/pdf/{{ json.trim().replace('banano/reiset/racimosCosechados/new/', '').replace('.json', '.pdf') }}">
                                                        {{ json.trim().replace('banano/reiset/racimosCosechados/new/', '').replace('.json', '') }}
                                                    </a> <br />
                                                </span>
                                            </td>
                                            <td><button class="btn btn-danger" title="Eliminar" ng-click="borrarViaje(row)"><i class="fa fa-trash"></i></button></td>
                                        </tr>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th></th>
                                            <th></th>
                                            <th>{{ viajesFiltrados | countOfValue : 'viaje' }}</th>
                                            <th></th>
                                            <th></th>
                                            <th>{{ viajesFiltrados | sumOfValue : 'racimos' }}</th>
                                            <th></th>
                                            <th></th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_3" aria-expanded="false"> Viajes Balanza </a>
                        </h4>
                    </div>
                    <div id="collapse_3_3" class="panel-collapse collapse" aria-expanded="true" style="">
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Hora Serv.</th>
                                            <th>Hora Envio</th>
                                            <th>Viaje</th>
                                            <th>Lote</th>
                                            <th>Palanca</th>
                                            <th># Racimos</th>
                                            <th></th>
                                        </tr>
                                        <tr>
                                            <th><input type="text" class="form-control" ng-change="cleanFilter(filterViajesBalanza)" ng-model="filterViajesBalanza.hora_llegada_servidor"></th>
                                            <th><input type="text" class="form-control" ng-change="cleanFilter(filterViajesBalanza)" ng-model="filterViajesBalanza.hora"></th>
                                            <th><input type="text" class="form-control" ng-change="cleanFilter(filterViajesBalanza)" ng-model="filterViajesBalanza.viaje"></th>
                                            <th><input type="text" class="form-control" ng-change="cleanFilter(filterViajesBalanza)" ng-model="filterViajesBalanza.lote"></th>
                                            <th><input type="text" class="form-control" ng-change="cleanFilter(filterViajesBalanza)" ng-model="filterViajesBalanza.palanca"></th>
                                            <th><input type="text" class="form-control" ng-change="cleanFilter(filterViajesBalanza)" ng-model="filterViajesBalanza.racimos"></th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr 
                                            ng-repeat="row in viajesFiltradosBalanza = (viajesBalanza | filter: filterViajesBalanza : true)" 
                                            class="{{ row.jsons.length > 1 ? 'red' : '' }}">

                                            <td>{{ row.hora_llegada_servidor }}</td>
                                            <td>{{ row.hora }}</td>
                                            <td>{{ row.viaje }}</td>
                                            <td>
                                                <span>{{ row.lote }}</span>
                                            <td>
                                                <span>{{ row.palanca }}</span>
                                            </td>
                                            <td>{{ row.racimos }}</td>
                                            <td><button class="btn btn-danger" title="Eliminar" ng-click="borrarViajeBalanza(row)"><i class="fa fa-trash"></i></button></td>
                                        </tr>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th></th>
                                            <th></th>
                                            <th>{{ viajesFiltradosBalanza | countOfValue : 'viaje' }}</th>
                                            <th></th>
                                            <th></th>
                                            <th>{{ viajesFiltradosBalanza | sumOfValue : 'racimos' }}</th>
                                            <th></th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_9" aria-expanded="false"> Comparativo </a>
                        </h4>
                    </div>
                    <div id="collapse_3_9" class="panel-collapse collapse" aria-expanded="true" style="">
                        <div class="panel-body">
                            <div class="row">
                                <div class="btn-group pull-right" style="margin-right: 5px;">
                                    <button class="btn dark btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false" style="height: 34px">
                                        Exportar <i class="fa fa-angle-down"></i>
                                    </button>
                                    <ul class="dropdown-menu pull-right">
                                        <li>
                                            <a href="javascript:;" ng-click="exportPrint('comparativo')"> Imprimir </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" ng-click="exportExcel('comparativo')">Excel</a>
                                        </li>
                                    </ul>
                                </div>
                                <button class="btn btn-primary pull-right" title="Cuadre de racimos por viaje" ng-click="procesarRacimosViajes()">
                                    Procesar
                                </button>
                            </div>
                            <br>
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Viaje</th>
                                            <th>Hora - B</th>
                                            <th>Hora - F</th>
                                            <th>Lote - B</th>
                                            <th>Lote - F</th>
                                            <th>Palanca - B</th>
                                            <th>Palanca - F</th>
                                            <th># Racimos - B</th>
                                            <th># Racimos - F</th>
                                            <th></th>
                                        </tr>
                                        <tr>
                                            <th><input type="text" class="form-control" ng-change="cleanFilter(filterViajesComparativo)" ng-model="filterViajesComparativo.viaje"></th>
                                            <th></th>
                                            <th></th>
                                            <th><input type="text" class="form-control" ng-change="cleanFilter(filterViajesComparativo)" ng-model="filterViajesComparativo.lote_blz"></th>
                                            <th><input type="text" class="form-control" ng-change="cleanFilter(filterViajesComparativo)" ng-model="filterViajesComparativo.lote_form"></th>
                                            <th><input type="text" class="form-control" ng-change="cleanFilter(filterViajesComparativo)" ng-model="filterViajesComparativo.palanca_blz"></th>
                                            <th><input type="text" class="form-control" ng-change="cleanFilter(filterViajesComparativo)" ng-model="filterViajesComparativo.palanca_form"></th>
                                            <th></th>
                                            <th></th>
                                            <th>
                                                <select class="form-control" ng-change="cleanFilter(filterViajesComparativo)" ng-model="filterViajesComparativo.valid">
                                                    <option value="">TODOS</option>
                                                    <option value="1" class="bg-green-jungle bg-font-green-jungle">BIEN</option>
                                                    <option value="0" class="bg-red-thunderbird bg-font-red-thunderbird">MAL</option>
                                                </select>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat="row in viajesFiltradosComparativo = (comparativo | filter: filterViajesComparativo : true)" >
                                            <td>{{ row.viaje }}</td>
                                            <td>{{ row.hora_blz }}</td>
                                            <td>{{ row.hora_form }}</td>
                                            <td class="font-{{ row.lote_blz == row.lote_form ? '' : 'red-thunderbird bold' }}">{{ row.lote_blz }}</td>
                                            <td class="font-{{ row.lote_blz == row.lote_form ? '' : 'red-thunderbird bold' }}">{{ row.lote_form }}</td>
                                            <td class="font-{{ row.palanca_blz == row.palanca_form ? '' : 'red-thunderbird bold' }}">{{ row.palanca_blz }}</td>
                                            <td class="font-{{ row.palanca_blz == row.palanca_form ? '' : 'red-thunderbird bold' }}">{{ row.palanca_form }}</td>
                                            <td class="font-{{ row.racimos_blz == row.racimos_form ? '' : 'red-thunderbird bold' }}">{{ row.racimos_blz }}</td>
                                            <td class="font-{{ row.racimos_blz == row.racimos_form ? '' : 'red-thunderbird bold' }}">{{ row.racimos_form }}</td>
                                            <td><i class="fa fa-{{ row.valid == 1 ? 'check' : 'times' }} font-{{ row.valid == 1 ? 'green-jungle' : 'red-thunderbird' }}"></i></td>
                                        </tr>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>#{{ viajesFiltradosComparativo | countOfValue : 'viaje' }}</th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th>{{ viajesFiltradosComparativo | sumOfValue : 'racimos_blz' }}</th>
                                            <th>{{ viajesFiltradosComparativo | sumOfValue : 'racimos_form' }}</th>
                                            <th>{{ ((viajesFiltradosComparativo | sumOfValue : 'valid')/viajesFiltradosComparativo.length*100) | number:2 }}%</th>
                                        </tr>
                                    </tfoot>
                                </table>

                                <table class="table table-bordered hide" id="comparativo">
                                    <thead>
                                        <tr>
                                            <th>Viaje</th>
                                            <th>Hora - B</th>
                                            <th>Hora - F</th>
                                            <th>Lote - B</th>
                                            <th>Lote - F</th>
                                            <th>Palanca - B</th>
                                            <th>Palanca - F</th>
                                            <th># Racimos - B</th>
                                            <th># Racimos - F</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat="row in viajesFiltradosComparativo" >
                                            <td>{{ row.viaje }}</td>
                                            <td>{{ row.hora_blz }}</td>
                                            <td>{{ row.hora_form }}</td>
                                            <td class="font-{{ row.lote_blz == row.lote_form ? '' : 'red-thunderbird bold' }}">{{ row.lote_blz }}</td>
                                            <td class="font-{{ row.lote_blz == row.lote_form ? '' : 'red-thunderbird bold' }}">{{ row.lote_form }}</td>
                                            <td class="font-{{ row.palanca_blz == row.palanca_form ? '' : 'red-thunderbird bold' }}">{{ row.palanca_blz }}</td>
                                            <td class="font-{{ row.palanca_blz == row.palanca_form ? '' : 'red-thunderbird bold' }}">{{ row.palanca_form }}</td>
                                            <td class="font-{{ row.racimos_blz == row.racimos_form ? '' : 'red-thunderbird bold' }}">{{ row.racimos_blz }}</td>
                                            <td class="font-{{ row.racimos_blz == row.racimos_form ? '' : 'red-thunderbird bold' }}">{{ row.racimos_form }}</td>
                                            <td><i class="fa fa-{{ row.valid == 1 ? 'check' : 'times' }} font-{{ row.valid == 1 ? 'green-jungle' : 'red-thunderbird' }}"></i></td>
                                        </tr>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>#{{ viajesFiltradosComparativo | countOfValue : 'viaje' }}</th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th>{{ viajesFiltradosComparativo | sumOfValue : 'racimos_blz' }}</th>
                                            <th>{{ viajesFiltradosComparativo | sumOfValue : 'racimos_form' }}</th>
                                            <th>{{ ((viajesFiltradosComparativo | sumOfValue : 'valid')/viajesFiltradosComparativo.length*100) | number:2 }}%</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a ng-click="openMuestreo()" class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_6" aria-expanded="false"> Muestreo </a>
                        </h4>
                    </div>
                    <div id="collapse_3_6" class="panel-collapse collapse" aria-expanded="true" style="">
                        <div class="panel-body">
                            <div id="grafica-muestreo" class="chart"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="agregar-racimo" role="basic" aria-hidden="true">
		<div class="modal-dialog" style="background: white;">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">AGREGAR RACIMO</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
                        <div class="form-group">
                            <div class="col-md-4">
                                <label class="control-label">Lote</label>
                            </div>
                            <div class="col-md-8">
                                <select class="form-control" ng-model="newRacimo.lote">
                                    <option value="{{ l.id }}" ng-repeat="l in lotes">{{ l.nombre }}</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-4">
                                <label class="control-label">Cuadrilla</label>
                            </div>
                            <div class="col-md-8">
                                <input type="text" class="form-control" ng-model="newRacimo.cuadrilla">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-4">
                                <label class="control-label">Viaje</label>
                            </div>
                            <div class="col-md-8">
                                <input type="number" class="form-control" ng-model="newRacimo.viaje">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-4">
                                <label class="control-label">Cinta</label>
                            </div>
                            <div class="col-md-8">
                                <select class="form-control" ng-model="newRacimo.cinta">
                                    <option value="S/C">S/C</option>
                                    <option value="{{ color }}" class="{{ classColor }}" ng-repeat="(color, classColor) in coloresClass">{{ color }}</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-4">
                                <label class="control-label">Causa</label>
                            </div>
                            <div class="col-md-8">
                                <select ng-model="newRacimo.causa" class="form-control">
                                    <option value="">N/A</option>
                                    <option value="{{ c.nombre }}" ng-repeat="c in causas">{{ c.nombre }}</option>
                                </select>
                            </div>
                        </div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn default" data-dismiss="modal">Cancelar</button>
				<button type="button" class="btn blue" ng-click="guardarAgregarRacimo()">Guardar</button>
			</div>
		</div>
	</div>

</div>

<script src="componentes/FilterableSortableTable.js"></script>
<script src="assets/global/plugins/FileSaver.min.js"></script>