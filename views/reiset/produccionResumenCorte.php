<style>
	.margin-5 {
		margin-bottom: 5px !important;
	}
	table > th,td{
		text-align: center !important;
	}
	.center-th {
		text-align: center !important;
	}
	table {
		cursor: pointer !important;
	}
	.anchoTable {
		width: 100px;
	}
    .delete  {
        background-color : #c8d8e8 !important;
    }
    .text-left {
        text-align: left !important;
		padding-left: 10px !important;
    }
    .text-right {
        text-align: right !important;
		padding-right: 10px !important;
    }
    .form-control, .input-sm, select, .btn, .portlet {
        border-radius : 3px !important;
    }
</style>
<script src="assets/global/plugins/FileSaver.min.js"></script>
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.0.3/socket.io.js"></script>-->
<div ng-controller="produccion" ng-cloak>
	<h3 class="page-title"> 
          Produccion. Resumen Corte
     </h3>
     <div class="page-bar" ng-init="getLastDay()">
         <ul class="page-breadcrumb">
             <li>
                 <i class="icon-home"></i>
                 <a>Producción</a>
                 <i class="fa fa-angle-right"></i>
             </li>
         </ul>
         <div class="page-toolbar">
            <label for="">
                Sector : 
            </label>
            <select ng-change="changeRangeDate()" name="sector" id="sector" ng-model="filters.sector" style="margin: 2px;height: 36px;">
                <option value="">TODOS</option>
                <option value="361">361</option>
                <option value="948">948</option>
                <option value="R45">R45</option>
            </select>
            <input class="input-sm" sytle="height: 36px;" data-provide="datepicker" data-date-format="yyyy-mm-dd" ng-model="fecha" ng-change="changeRangeDate({ first_date: fecha, second_date : fecha })" readonly>
        </div>
    </div>



	<div class="row">
		<div class="col-md-5">
			<div class="portlet box green-haze">
				<div class="portlet-title">
					<span class="caption">RESUMEN POR EDAD</span>
				</div>
				<div class="portlet-body" >
					<div class="table-responsive" id="racimos_edad">
						<table class="table table-striped table-bordered table-hover">
							<thead>
								<tr>
                                    <th class="center-th">EDAD</th>
                                    <th class="center-th">CALIBRE<br>SEGUNDA</th>
                                    <th class="center-th">PROC</th>
                                    <th class="center-th">RECU</th>
                                    <th class="center-th">CORT</th>
                                    <th class="center-th">%</th>
								</tr>
							</thead>
							<tbody>
								<tr ng-repeat="color in tabla.edades | orderObjectBy : 'edad'" ng-show="color.cosechados > 0">
                                    <td class="{{ color.class }}">{{ (color.edad > 0) ? color.edad : 'S/C' }}</td>
                                    <td>{{ (color.calibracion > 0) ? (color.calibracion  | number : 2) : '' }}</td>
                                    <td>{{color.procesados}}</td>
                                    <td>{{color.recusados}}</td>
                                    <td>{{color.cosechados}}</td>
                                    <td>{{color.cosechados/totales.total_cosechados*100 | number : 2}}</td>
								</tr>
							</tbody>
							<tfoot>
								<tr>
                                    <th class="text-center">{{totales.edad | number: 2}}</th>
                                    <th class="text-center">{{tabla.edades | avgOfValue : 'calibracion' | number:2 }}</th>
                                    <th class="text-center">{{tabla.edades | sumOfValue : 'procesados'}}</th>
                                    <th class="text-center">{{tabla.edades | sumOfValue : 'recusados'}}</th>
                                    <th class="text-center">{{tabla.edades | sumOfValue : 'cosechados'}}</th>
                                    <th class="text-center">100</th>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
		</div>
        <div class="col-md-7">
            <div class="portlet box green-haze">
				<div class="portlet-title">
					<span class="caption">GUIA DE REMISION</span>
				</div>
				<div class="portlet-body" >
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="table_4">
                            <thead>
                                <tr>
                                    <th class="center-th">GUIA</th>
                                    <th class="center-th">MARCA</th>
                                    <th class="center-th">UNIDADES</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat-start="row in cuadrarCajas" ng-hide="true">
                                </tr>
                                <tr ng-repeat-end ng-repeat="detalle in row.detalle">
                                    <td class="center-th">{{ detalle.guia }}</td>
                                    <td class="center-th">{{ row.marca }}</td>
                                    <td class="center-th">{{ detalle.valor | number }}</td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th class="center-th">TOTAL:</th>
                                    <th></th>
                                    <th class="center-th">{{ cuadrarCajas | sumOfValue : 'balanza' }}</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
	</div>

    <div class="row">
        <div class="col-md-5">
            <div class="portlet box green-haze">
				<div class="portlet-title">
					<span class="caption">RESUMEN PROCESO</span>
				</div>
				<div class="portlet-body" >
                    <div class="table-responsive" id="resumen_produccion">
                        <table class="table table-bordered">
                            <tbody>
                                <tr ng-repeat="row in resumenProduccion">
                                    <td class="text-left">{{ row.name }}</td>
                                    <td class="text-right">{{ row.value | number : 2 }}</td>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-7">
            <div class="portlet box green-haze">
				<div class="portlet-title">
					<span class="caption">RESUMEN CAJAS CONVERTIDAS A PRIMERA</span>
				</div>
				<div class="portlet-body" >
                    <div class="table-responsive" id="div_table_2">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th class="center-th">MARCA</th>
                                    <th class="center-th">CANTIDAD</th>
                                    <th class="center-th">PESO {{ table.unidad | uppercase }}</th>
                                    <th class="center-th">PROM</th>
                                    <th class="center-th">MAX</th>
                                    <th class="center-th">MIN</th>
                                    <th class="center-th">PALLETS</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="row in resumen">
                                    <td>{{ row.marca }}</td>
                                    <td>{{ row.conv | number }}</td>
                                    <td>{{ row.total_kg | number: 2 }}</td>
                                    <td>{{ row.promedio | number: 2 }}</td>
                                    <td>{{ row.maximo | number: 2 }}</td>
                                    <td>{{ row.minimo | number: 2 }}</td>
                                    <td></td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td></td>
                                    <th class="center-th">{{ (resumen | sumOfValue:'conv') | number }}</th>
                                    <th class="center-th">{{ (resumen | sumOfValue:'total_kg') | number : 2 }}</th>
                                    <th class="center-th">{{ (resumen | avgOfValue:'promedio') | number : 2 }}</th>
                                    <th class="center-th">{{ (resumen | avgOfValue:'maximo') | number : 2 }}</th>
                                    <th class="center-th">{{ (resumen | avgOfValue:'minimo') | number : 2 }}</th>
                                    <td></td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<script src="componentes/FilterableSortableTable.js"></script>