<style>
	td, th {
        text-align : center;
    }
    .black {
        color : black;
    }
    .form-control, .input-sm, select, .btn, .portlet {
        border-radius : 3px !important;
    }
</style>
<div ng-controller="produccion" ng-cloak>
	<h3 class="page-title"> 
          Producción. Recobro
     </h3>
     <div class="page-bar">
         <ul class="page-breadcrumb">
             <li>
                 <i class="icon-home"></i>
                 <a>Recobro</a>
                 <i class="fa fa-angle-right"></i>
             </li>
         </ul>
         <div class="page-toolbar" style="display: flex; align-items: center;">
            <label for="">
                Sector : 
            </label>
            <select ng-change="init(); getDataTable()" name="sector" id="sector" ng-model="filters.sector" style="margin: 2px;height: 36px;">
                <option value="">TODOS</option>
                <option value="361">361</option>
                <option value="948">948</option>
                <option value="R45">R45</option>
            </select>
            <label for="semana" style="margin: auto;">
                Sem : 
            </label>
            <div class="btn-group pull-right">
                <button type="button" class="btn btn-fit-height dropdown-toggle {{colorClass}}" data-toggle="dropdown" data-hover="dropdown" data-delay="2000" data-close-others="true" aria-expanded="false"> 
                    {{ filters.year }} - {{ filters.week }}
                    <i class="fa fa-angle-down"></i>
                </button>
                <ul class="dropdown-menu pull-right" role="menu" style="max-height: 300px; overflow-y: scroll;">
                    <li ng-repeat="sem in semanas" class="{{sem.class}}" ng-click="filters.week = sem.semana; filters.year = sem.anio; init()">
                        <a href="javascript:;" class="{{sem.class}}">
                            {{ sem.anio }} -  {{ sem.semana }}
                        </a>
                    </li>
                </ul>
            </div>
         </div>
    </div>
	<div id="contenedor" class="div2">

        <div class="row">
            <div class="col-md-12">
                <div class="portlet box green-haze">
					<div class="portlet-title">
						<span class="caption">SALDO DE FUNDAS</span>
						<div class="tools">
                            <button class="btn btn-secundary black" ng-click="editing = !editing">{{ editing ? 'EDITANDO' : 'EDITAR' }}</button>
						</div>
					</div>
					<div class="portlet-body">
                        <div class="table-responsive" id="saldo_fundas">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th rowspan="2">LOTE</th>
                                        <th rowspan="2">RAC<br>ENF</th>
                                        <th ng-if="edades.length > 0" class="text-center sm" colspan="{{edades.length}}">RACIMOS COSECHADOS POR EDAD (SEM)</th>
                                        <th rowspan="2">TOTAL<br>COSE</th>
                                        <th rowspan="2">SALDO<br>RAC</th>
                                        <th rowspan="2">% RECOBRO</th>
                                        <th rowspan="2">CINTAS<br>CAIDAS</th>
                                        <th rowspan="2">% CAIDAS</th>
                                        <th rowspan="2">% TOTAL</th>
                                        <th rowspan="2">NO RECUP</th>
                                    </tr>
                                    <tr>
                                        <th ng-repeat="e in edades | orderObjectBy : 'edad'" class="sm">{{ e.edad }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="row in data">
                                        <td>{{row.lote}}</td>
                                        <td class="{{ colorClass }}">
                                            {{ row.racimos_enfunde > 0 ? (row.racimos_enfunde | number: 0) : '' }}
                                        </td>
                                        <td ng-if="edades.length > 0" class="sm" ng-repeat="e in edades | orderObjectBy : 'edad'">
                                            {{ row['edad_'+e.edad] > 0 ? row['edad_'+e.edad] : '' }}
                                        </td>
                                        <td>{{row.cosechados}}</td>
                                        <td>{{row.saldo = (row.racimos_enfunde - row.cosechados)}}</td>
                                        <td class="{{ getUmbral(row.enfunde) }}">{{row.enfunde = (row.racimos_enfunde > 0 ? (row.cosechados/row.racimos_enfunde*100 | number:2) : 0)}}%</td>
                                        <td>
                                            <span ng-if="!editing">{{row.cintas_caidas}}</span>
                                            <input ng-if="editing" type="number" ng-model="row.cintas_caidas" class="form-control" ng-change="reloadTotales()" required/>
                                        </td>
                                        <td>{{row.caidas = ((row.cintas_caidas > 0 && row.racimos_enfunde > 0) ? (row.cintas_caidas/row.racimos_enfunde*100) : 0)}}%</td>
                                        <td>{{row.total = (row.caidas+row.enfunde) | number}}%</td>
                                        <td>{{ ((100-row.total) /100 ) *row.racimos_enfunde | number: 0}}</td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>TOTAL</th>
                                        <th class="{{ colorClass }}">{{ totales.racimos_enfunde | number }}</th>
                                        <th ng-if="edades.length > 0" class="sm" ng-repeat="e in edades | orderObjectBy : 'edad'">
                                            {{ (data | sumOfValue : 'edad_'+e.edad) > 0 ? (data | sumOfValue : 'edad_'+e.edad | number) : '' }}
                                        </th>
                                        <th>{{ totales.cosechados = (data | sumOfValue : 'cosechados') | number }}</th>
                                        <th>{{ totales.saldo = (data | sumOfValue : 'saldo') | number }}</th>
                                        <th class="{{ getUmbral(totales.recobro) }}">{{ totales.recobro | number : 2 }}%</th>
                                        <th>{{ totales.cintas_caidas | number }}</th>
                                        <th>{{ totales.p_cintas_caidas | number : 2 }}%</th>
                                        <th>{{ totales.total = (totales.recobro + totales.p_cintas_caidas) | number: 2 }}%</th>
                                        <th>{{ (100-totales.total)*(totales.racimos_enfunde/100) | number: 2 }}</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
					</div>
				</div>
            </div>
        </div>
	
		<div class="row">
			<div class="col-md-12">
				<div class="portlet box green-haze">
					<div class="portlet-title">
						<span class="caption">ENFUNDE - RACIMOS COSECHADOS</span>
						<div class="tools">
                            <select class="input-sm form-control" ng-model="filters.lote" ng-change="getDataTable()" style="width: 90px">
                                <option value="">LOTES</option>
                                <option value="{{lote}}" ng-repeat="lote in lotes | orderBy" ng-selected="lote == filters.lote">{{ lote }}</option>
                            </select>
						</div>
					</div>
					<div class="portlet-body">
                        <div id="data-table"></div>
					</div>
				</div>
            </div>
            
            <div class="col-md-12 hide">
				<div class="portlet box green-haze">
					<div class="portlet-title">
						<span class="caption">ENFUNDE - PRECALIBRACION</span>
						<div class="tools">
						</div>
					</div>
					<div class="portlet-body">
                        <div id="data-table-2"></div>
					</div>
				</div>
            </div>
            
            <div class="col-md-12 hide">
				<div class="portlet box green-haze">
					<div class="portlet-title">
						<span class="caption">PRECALIBRACION - RACIMOS COSECHADOS</span>
						<div class="tools">
						</div>
					</div>
					<div class="portlet-body">
                        <div id="data-table-2"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="componentes/FilterableSortableTable.js"></script>