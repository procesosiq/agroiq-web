
<!-- END PAGE HEADER-->
<div class="row">
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat2 ">
            <div class="display">
                <div class="number">
                    <h3 class="font-green-jungle">
                        <span class="counter_tags" data-value="">{{tags.primera_caja}}</span>
                        <small class="font-green-jungle"></small>
                    </h3>
                    <small>PRIMERA CAJA DEL DIA</small>
                </div>
                <div class="icon">
                    <i class="icon-pie-chart"></i>
                </div>
            </div>
            <div class="progress-info">
                <div class="progress">
                    <span style="width: 0%;" class="progress-bar progress-bar-succes">
                        <span class="sr-only"></span>
                    </span>
                </div>
                <div class="status">
                    <div class="status-title">{{tags.fecha_primera}}</div>
                    <div class="status-number"> </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat2 ">
            <div class="display">
                <div class="number">
                    <h3 class="font-green-jungle">
                        <span class="counter_tags" data-value="">{{tags.ultima_caja}}</span>
                        <small class="font-green-jungle"></small>
                    </h3>
                    <small>ULTIMA CAJA DEL DIA</small>
                </div>
                <div class="icon">
                    <i class="icon-pie-chart"></i>
                </div>
            </div>
            <div class="progress-info">
                <div class="progress">
                    <span style="width: 0%;" class="progress-bar progress-bar-succes">
                        <span class="sr-only"></span>
                    </span>
                </div>
                <div class="status">
                    <div class="status-title">{{tags.fecha_ultima}}</div>
                    <div class="status-number"> </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat2 ">
            <div class="display">
                <div class="number">
                    <h3 class="font-green-jungle">
                        <span class="counter_tags" data-value="">{{tags.diferencia}}</span>
                        <small class="font-green-jungle"></small>
                    </h3>
                    <small>HORAS DE PRODUCCIÓN</small>
                </div>
                <div class="icon">
                    <i class="icon-pie-chart"></i>
                </div>
            </div>
            <div class="progress-info">
                <div class="progress">
                    <span style="width: 0%;" class="progress-bar progress-bar-succes">
                        <span class="sr-only"></span>
                    </span>
                </div>
                <div class="status">
                    <div class="status-title"></div>
                    <div class="status-number"> </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat2 ">
            <div class="display">
                <div class="number">
                    <h3 class="font-green-jungle">
                        <span class="counter_tags" data-value="">{{tags.cajas40}}</span>
                        <small class="font-green-jungle"></small>
                    </h3>
                    <small>CAJAS DE 41.5</small>
                </div>
                <div class="icon">
                    <i class="icon-pie-chart"></i>
                </div>
            </div>
            <div class="progress-info">
                <div class="progress">
                    <span style="width: 0%;" class="progress-bar progress-bar-succes">
                        <span class="sr-only"></span>
                    </span>
                </div>
                <div class="status">
                    <div class="status-title"></div>
                    <div class="status-number"> </div>
                </div>
            </div>
        </div>
    </div>
</div>