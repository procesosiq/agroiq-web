
                    <!-- END PAGE HEADER-->
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-green-jungle">
                                            <span class="counter_tags" data-value="">{{tags.primera_hora}}</span>
                                            <small class="font-green-jungle"></small>
                                        </h3>
                                        <small>PRIMERA PALANCA DEL DIA</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-pie-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width: 0%;" class="progress-bar progress-bar-success">
                                            <span class="sr-only"></span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title">{{tags.primera_fecha}}</div>
                                        <div class="status-number"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-green-jungle">
                                            <span class="counter_tags" data-value="">{{tags.ultima_hora}}</span>
                                            <small class="font-green-jungle"></small>
                                        </h3>
                                        <small>ULTIMA PALANCA DEL DIA</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-pie-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width: 0%;" class="progress-bar progress-bar-success">
                                            <span class="sr-only"></span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title">{{tags.ultima_fecha}}</div>
                                        <div class="status-number"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-green-jungle">
                                            <span class="counter_tags" data-value="">{{tags.diferencia}}</span>
                                            <small class="font-green-jungle"></small>
                                        </h3>
                                        <small>HORAS DE PRODUCCIÓN</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-pie-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width: 0%;" class="progress-bar progress-bar-success">
                                            <span class="sr-only"></span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.cosechados) }}">
                                            <span class="counter_tags" data-value="{{tags.cosechados}}">{{tags.cosechados}}</span>
                                            <small class="font-{{ revision(tags.cosechados) }}"></small>
                                        </h3>
                                        <small>COSECHADOS</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-pie-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width: {{tags.cosechados}}%;" class="progress-bar progress-bar-success {{ revision(tags.cosechados) }}">
                                            <span class="sr-only">{{tags.cosechados}}</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number"> {{tags.cosechados}} </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.procesados) }}">
                                            <span class="counter_tags" data-value="{{tags.procesados}}">{{tags.procesados}}</span>
                                            <small class="font-{{ revision(tags.procesados) }}"></small>
                                        </h3>
                                        <small>PROCESADOS</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-bar-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width:  {{tags.procesados}}%;" class="progress-bar progress-bar-success {{ revision(tags.recusados) }}">
                                            <span class="sr-only"> {{tags.procesados}}</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title">RECUSADOS</div>
                                        <div class="status-number">{{ tags.recusados }}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.muestreo) }}">
                                            <span class="counter_tags" data-value=" {{tags.muestreo}}">{{tags.muestreo}} <small>%</small></span>
                                            <small class="font-{{ revision(tags.muestreo) }}"></small>
                                        </h3>
                                        <small>% DE MUESTREO</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-bar-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width:  {{tags.muestreo}}%;" class="progress-bar progress-bar-success {{ revision(tags.muestreo) }}">
                                            <span class="sr-only"> {{tags.muestreo}}% change</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number">  {{tags.muestreo}} %</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.calibre_segunda) }}">
                                            <span class="counter_tags" data-value=" {{tags.calibre_segunda}}">{{tags.calibre_segunda | number:2}}</span>
                                            <small class="font-{{ revision(tags.calibre_segunda) }}"></small>
                                        </h3>
                                        <small>CALIBRACIÓN SEGUNDA PROM</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-bar-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width:  {{tags.calibre_segunda}}%;" class="progress-bar progress-bar-success {{ revision(tags.calibre_segunda) }}">
                                            <span class="sr-only"> {{tags.calibre_segunda}}</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title">CALIBRE ULTIMA MANO PROM</div>
                                        <div class="status-number">{{ tags.calibre_ultima | number: 2 }}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3>
                                            <span class="counter_tags" data-value="{{tags.kg_prom}}">{{ tags.manos }}</span>
                                            <small></small>
                                        </h3>
                                        <small>MANOS PROM</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-pie-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width: {{tags.manos}}%;" class="progress-bar progress-bar-success">
                                            <span class="sr-only">{{tags.kg_prom}}</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number"> {{ tags.manos }} </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.edad) }}">
                                            <span class="counter_tags" data-value=" {{tags.edad}}">{{ tags.edad }}</span>
                                            <small class="font-{{ revision(tags.edad) }}"></small>
                                        </h3>
                                        <small>EDAD PROM</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-bar-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width: {{tags.edad}}%;" class="progress-bar progress-bar-success {{ revision(tags.edad) }}">
                                            <span class="sr-only">{{tags.edad}}</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number">{{ tags.edad }}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3>
                                            <span class="counter_tags" data-value="{{ tags.kg_proc }}">{{ tags.kg_proc | number: 2 }}</span>
                                            <small></small>
                                        </h3>
                                        <small>PESO LB PROC</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-pie-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width: 100%;" class="progress-bar progress-bar-success">
                                            <span class="sr-only">{{ tags.kg_proc }}</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number">{{ tags.kg_proc | number: 2 }}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3>
                                            <span class="counter_tags" data-value="{{ tags.kg_recu }}">{{ tags.kg_recu | number: 2 }}</span>
                                            <small></small>
                                        </h3>
                                        <small>PESO LB RECU</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-pie-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width: 100%;" class="progress-bar progress-bar-success">
                                            <span class="sr-only">{{ tags.kg_recu }}</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number"> {{ tags.kg_recu | number: 2 }}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.kg_prom) }}">
                                            <span class="counter_tags" data-value="{{tags.kg_prom}}">{{tags.kg_prom}}</span>
                                            <small class="font-{{ revision(tags.kg_prom) }}"></small>
                                        </h3>
                                        <small>PESO PROM RACIMO LB</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-pie-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width: {{tags.kg_prom}}%;" class="progress-bar progress-bar-success {{ revision(tags.kg_prom) }}">
                                            <span class="sr-only">{{tags.kg_prom}}</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number"> {{tags.kg_prom}} </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>