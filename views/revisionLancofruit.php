<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.4/jspdf.debug.js"></script>
<script type="text/javascript" src="//cdn.rawgit.com/niklasvh/html2canvas/0.5.0-alpha2/dist/html2canvas.min.js"></script>
<style>
    .no-padding {
        padding : 0px !important;
    }
    .no-margin {
        margin : 0px !important;
    }
    #listado {
        min-height: 560px;
		max-height: 560px;
        overflow-y : scroll;
        overflow-x: hide;
        cursor: pointer;
    }
	#map {
		min-height: 600px;
		max-height: 600px;
		min-width: 100%;
		max-width: 100%;
	}
    .accordion .panel .panel-title .accordion-toggle {
		display: block !important;
    	padding: 0px 14px !important;
	}
	.margin-5 {
		margin-bottom: 5px !important;
	}
	/*table > th,td{
		text-align: center !important;
	}*/
	.center-th {
		text-align: center !important;
	}
	.left-td {
		text-align: left !important;
		padding-left: 10px !important;
	}
	table {
		cursor: pointer !important;
	}
	.alginCenter {
		text-align: center !important;
		padding-top: 20px;
	}
	.portlet.box .dataTables_wrapper .dt-buttons{
        margin-top: 0px !important; 
        padding-bottom: 5px !important;
    }
    .fixedHeader-floating{top:60px!important;}

/* OCULTAR COLUMNAS */
.checkbox .cr,
.radio .cr {
    position: relative;
    display: inline-block;
    border: 1px solid #a9a9a9;
    border-radius: .25em;
    width: 1.3em;
    height: 1.3em;
    float: left;
    margin-right: .5em;
}

.radio .cr {
    border-radius: 50%;
}

.checkbox .cr .cr-icon,
.radio .cr .cr-icon {
    position: absolute;
    font-size: .8em;
    line-height: 0;
    top: 50%;
    left: 20%;
}

.radio .cr .cr-icon {
    margin-left: 0.04em;
}

.labelFilters {
    width: 150px;
    padding-top: 9px;
}

.checkbox label input[type="checkbox"],
.radio label input[type="radio"] {
    display: none;
}

.checkbox label input[type="checkbox"] + .cr > .cr-icon,
.radio label input[type="radio"] + .cr > .cr-icon {
    transform: scale(3) rotateZ(-20deg);
    opacity: 0;
    transition: all .3s ease-in;
}

.checkbox label input[type="checkbox"]:checked + .cr > .cr-icon,
.radio label input[type="radio"]:checked + .cr > .cr-icon {
    transform: scale(1) rotateZ(0deg);
    opacity: 1;
}

.checkbox label input[type="checkbox"]:disabled + .cr,
.radio label input[type="radio"]:disabled + .cr {
    opacity: .5;
}
</style>
<div ng-controller="lancofruit">
	<h3 class="page-title"> 
          Lancofruit
     </h3>
     <div class="page-bar">
         <ul class="page-breadcrumb">
             <li>
                 <i class="icon-home"></i>
                 <a>Revisión Lancofruit</a>
                 <i class="fa fa-angle-right"></i>
             </li>
         </ul>
         <div class="page-toolbar" style="display:flex">
            <label for="" class="labelFilters">
                Tipo: 
            </label>
            <select class="form-control" id="tipo" name="tipo" onchange="changeRuta()" ng-model="filters.tipo" style="margin: 2px;height: 36px;">
                <option value="">Todos</option>
                <option value="{{tipo.id}}" ng-repeat="tipo in tipos" ng-selected="tipo.id == filters.tipo">{{tipo.nombre}}</option>
            </select>
            <label for="" class="labelFilters">
                Subtipo: 
            </label>
            <select class="form-control" id="subtipo" name="subtipo" onchange="changeRuta()" ng-model="filters.subtipo" style="margin: 2px;height: 36px;">
                <option value="">Todos</option>
                <option value="{{s.id}}" ng-selected="s.id == filters.subtipo" ng-repeat="s in subtipos | filter : { id_categoria : filters.tipo } : 'strict'">{{s.nombre}}</option>
            </select>
            <label for="" class="labelFilters">
                Sector: 
            </label>
            <select class="form-control" id="sector" name="sector" onchange="changeSector()" ng-model="filters.sector" style="margin: 2px;height: 36px;">
                <option value="">Todos</option>
                <option value="{{sector}}" ng-repeat="sector in sectores">{{sector}}</option>
            </select>
            <label for="" class="labelFilters">
                Ruta: 
            </label>
            <select class="form-control" id="ruta" name="ruta" onchange="changeRuta()" ng-model="filters.ruta" style="margin: 2px;height: 36px;">
                <option value="">Todos</option>
                <option value="{{ruta}}" ng-repeat="ruta in rutas">{{ruta}}</option>
            </select>
            <label for="" class="labelFilters">
                Status: 
            </label>
            <select class="form-control" id="status" name="status" onchange="changeRuta()" ng-model="filters.status" style="margin: 2px;height: 36px;">
                <option value="">Todos</option>
                <option value="{{sta}}" ng-repeat="sta in status">{{sta}}</option>
            </select>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue ">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift"></i> Mapa </div>
                    <div class="tools">
                        <a href="" class="collapse" data-original-title="" title=""> </a>
                    </div>
                </div>
                <div class="portlet-body form">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="panel panel-warning">
                                <div class="panel-heading">
                                    <form id="form-datos" role="form" method="post" class="form-horizontal form-row-seperated no-margin">
                                        <div>
                                            <div class="form-group no-padding" style="border-bottom: none;">
                                                <!--<label class="col-md-4 control-label" for="name">Buscar <span class="required" aria-required="true"> * </span></label>-->
                                                <div class="col-md-12">
                                                    <input type="text" class="form-control" ng-model="filters.search" id="search" placeholder="Buscar....">
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <ul class="list-group" id="listado">
                                    <li class="list-group-item" ng-click="markers.openInfoWindow(marker)" ng-repeat="marker in data | filter : {cliente : filters.search}"> {{marker.cliente}} </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div id="map"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="portlet box green">
                <div class="portlet-title">
                    <span class="caption">Información General</span>
                    <div class="tools">
                        <button class="btn btn-primary" ng-click="config.open()">Configuración</button>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-container table-scrollable">
                        <div class="table-actions-wrapper">
                        </div>
                        <div class="actions col-md-12">
                            <div class="inline-block">
                                <select class="form-control" ng-model="search.limit">
                                    <option value="10">10</option>
                                    <option value="20">20</option>
                                    <option value="50">50</option>
                                    <option value="100">100</option>
                                    <option value="999999">TODOS</option>
                                </select>
                            </div>
                            <div class="btn-group" style="float: right;">
                                <button class="btn btn-primary dropdown-toggle" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false">
                                    Exportar
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="javascript:;" ng-click="exportExcel('lancofruit', 'Información General')">Excel</a></li>
                                    <li><a href="javascript:;" ng-click="exportPdf()">PDF</a></li>
                                    <li><a href="javascript:;">Imprimir</a></li>
                                </ul>
                            </div>
                            <div class="btn-group" style="float: right;">
                                <button class="btn btn-default dropdown-toggle" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false">
                                    <i class="glyphicon glyphicon-th icon-th"></i>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                    <li class="active">                                                
                                        <div class="checkbox active">
                                            <label ng-click="disableColumns('id', $event)" class="active" ng-init="show.id = true;">
                                                <input type="checkbox" value="" checked="checked">
                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                ID
                                            </label>
                                        </div>
                                        <!--<label class="form-check-label"><input type="checkbox" class="form-check-input" checked="checked" ng-click="disableColumns(1, $event)" href="javascript:;"/>Nombre<span></span></label>-->
                                    </li>
                                    <li class="">
                                        <div class="checkbox">
                                            <label ng-click="disableColumns('fecha', $event)" ng-init="show.fecha = false;">
                                                <input type="checkbox" value="">
                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                Fecha
                                            </label>
                                        </div>
                                    </li>
                                     <li class="active">
                                        <div class="checkbox active">
                                            <label ng-click="disableColumns('status', $event)" class="active" ng-init="show.status = true;">
                                                <input type="checkbox" value="" checked="checked">
                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                Status
                                            </label>
                                        </div>
                                    </li>
                                    <li class="active">
                                        <div class="checkbox active">
                                            <label ng-click="disableColumns('tipo_local', $event)" class="active" ng-init="show.tipo_local = true;">
                                                <input type="checkbox" value="" checked="checked">
                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                Tipo
                                            </label>
                                        </div>
                                    </li>
                                    <li class="active">
                                        <div class="checkbox active">
                                            <label ng-click="disableColumns('tipo_local_subcategoria', $event)" class="active" ng-init="show.tipo_local_subcategoria = true;">
                                                <input type="checkbox" value="" checked="checked">
                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                Subtipo
                                            </label>
                                        </div>
                                    </li>
                                    <li class="active">
                                        <div class="checkbox active">
                                            <label ng-click="disableColumns('nombre_propietario', $event)" class="active" ng-init="show.nombre_propietario = true;">
                                                <input type="checkbox" value="" checked="checked">
                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                Nombre
                                            </label>
                                        </div>
                                    </li>
                                    <li class="">
                                        <div class="checkbox">
                                            <label ng-click="disableColumns('tendero', $event)">
                                                <input type="checkbox" value="">
                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                Tendero
                                            </label>
                                        </div>
                                    </li>
                                    <li class="active">
                                        <div class="checkbox active">
                                            <label ng-click="disableColumns('nombre_local', $event)" class="active" ng-init="show.nombre_local = true;">
                                                <input type="checkbox" value="" checked="checked">
                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                Local
                                            </label>
                                        </div>
                                    </li>
                                    <li class="">
                                        <div class="checkbox ">
                                            <label ng-click="disableColumns('referencia', $event)" ng-init="show.referencia = false;">
                                                <input type="checkbox" value="">
                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                Referencia
                                            </label>
                                        </div>
                                    </li>
                                    <li class="">
                                        <div class="checkbox">
                                            <label ng-click="disableColumns('direccion', $event)" ng-init="show.direccion = false;">
                                                <input type="checkbox" value="">
                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                Dirección
                                            </label>
                                        </div>
                                    </li>
                                    <li class="">
                                        <div class="checkbox">
                                            <label ng-click="disableColumns('ruc', $event)" ng-init="show.ruc = false;">
                                                <input type="checkbox" value="" >
                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                Ruc
                                            </label>
                                        </div>
                                    </li>
                                    <li class="">
                                        <div class="checkbox">
                                            <label ng-click="disableColumns('telefono', $event)" ng-init="show.telefono = false;">
                                                <input type="checkbox" value="">
                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                Teléfono
                                            </label>
                                        </div>
                                    </li>
                                    <li class="">
                                        <div class="checkbox">
                                            <label ng-click="disableColumns('correo', $event)" ng-init="show.correo = false;">
                                                <input type="checkbox" value="">
                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                Correo
                                            </label>
                                        </div>
                                    </li>
                                    <li class="active">
                                        <div class="checkbox active">
                                            <label ng-click="disableColumns('ruta', $event)" class="active" ng-init="show.ruta = true;">
                                                <input type="checkbox" value="" checked="checked">
                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                Ruta
                                            </label>
                                        </div>
                                    </li>
                                    <li class="active">
                                        <div class="checkbox active">
                                            <label ng-click="disableColumns('sector', $event)" class="active" ng-init="show.sector = true;">
                                                <input type="checkbox" value="" checked="checked">
                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                Sector
                                            </label>
                                        </div>
                                    </li>
                                    <li class="active">
                                        <div class="checkbox active">
                                            <label ng-click="disableColumns('acciones', $event)" class="active" ng-init="show.acciones = true;">
                                                <input type="checkbox" value="" checked="checked">
                                                <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                Acciones
                                            </label>
                                        </div>
                                    </li>
                                </ul>
                                <a href="javascript:;" class="collapse" data-original-title="Expandir/Contraer" title=""> </a>
                            </div>
                        </div>
                        <table class="table table-striped table-bordered table-hover table-header-fixed dataTable no-footer" ng-init="search.orderBy = 'id'; search.reverse = false; search.limit = '10'; search.actual_page = 1;">
                            <thead>
                                <tr role="row" class="heading">
                                    <th class="center-th {{ show.id ? '' : 'hide' }} selected sorting_asc" id="id_column" ng-click="changeSort('id')"> &nbsp;ID&nbsp; </th>
                                    <th class="center-th {{ show.fecha ? '' : 'hide' }}" id="fecha_column" ng-click="changeSort('fecha')"> &nbsp;&nbsp;&nbsp;Fecha&nbsp;&nbsp; </th>
                                    <th class="center-th {{ show.status ? '' : 'hide' }}" id="status_column" ng-click="changeSort('status')"> Status</th>
                                    <th class="center-th {{ show.tipo_local ? '' : 'hide' }}" id="tipo_local_column" ng-click="changeSort('tipo_local')"> Tipo </th>
                                    <th class="center-th {{ show.tipo_local_subcategoria ? '' : 'hide' }}" id="tipo_local_subcategoria_column" ng-click="changeSort('tipo_local_subcategoria')"> Subtipo </th>
                                    <th class="center-th {{ show.nombre_propietario ? '' : 'hide' }}" id="nombre_propietario_column" ng-click="changeSort('nombre_propietario')"> Nombre </th>
                                    <th class="center-th {{ show.tendero ? '' : 'hide' }}" id="nombre_local_colmn" ng-click="changeSort('tendero')"> Tendero </th>
                                    <th class="center-th {{ show.nombre_local ? '' : 'hide' }}" id="nombre_local_column" ng-click="changeSort('nombre_local')"> Local </th>
                                    <th class="center-th {{ show.referencia ? '' : 'hide' }}" id="referencia_column" ng-click="changeSort('referencia')"> Referencia </th>
                                    <th class="center-th {{ show.direccion ? '' : 'hide' }}" id="direccion_column" ng-click="changeSort('direccion')"> Dirección </th>
                                    <th class="center-th {{ show.ruc ? '' : 'hide' }}" id="ruc_column" ng-click="changeSort('ruc')"> Ruc </th>
                                    <th class="center-th {{ show.telefono ? '' : 'hide' }}" id="telefono_column" ng-click="changeSort('telefono')"> Teléfono </th>
                                    <th class="center-th {{ show.correo ? '' : 'hide' }}" id="correo_column" ng-click="changeSort('correo')"> Correo </th>
                                    <th class="center-th {{ show.ruta ? '' : 'hide' }}" id="ruta_column" ng-click="changeSort('ruta')"> Ruta </th>
                                    <th class="center-th {{ show.sector ? '' : 'hide' }}" id="sector_column" ng-click="changeSort('sector')"> Sector </th>
                                    <th class="center-th {{ show.acciones ? '' : 'hide' }}"> Acciones </th>
                                </tr>
                                <tr role="row" class="filter">
                                    <td><input type="text" class="form-control form-filter input-sm {{ show.id ? '' : 'hide' }}" ng-model="search.id" name="id" id="id"> </td>
                                    <td class="{{ show.fecha ? '' : 'hide' }}">
                                        <div class="input-group date date-picker margin-bottom-5" data-date-format="yyyy-mm-dd">
                                            <input type="text" class="form-control form-filter input-sm" readonly="" name="fecha_inicial" placeholder="From">
                                            <span class="input-group-btn">
                                                <button class="btn btn-sm default" type="button">
                                                    <i class="fa fa-calendar"></i>
                                                </button>
                                            </span>
                                        </div>
                                        <div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
                                            <input type="text" class="form-control form-filter input-sm" readonly="" name="fecha_final" placeholder="To">
                                            <span class="input-group-btn">
                                                <button class="btn btn-sm default" type="button">
                                                    <i class="fa fa-calendar"></i>
                                                </button>
                                            </span>
                                        </div>
                                    </td>
                                    <td><input type="text" class="form-control form-filter input-sm {{ show.status ? '' : 'hide' }}" ng-model="search.status" name="status" id="status"> </td>
                                    <td><input type="text" class="form-control form-filter input-sm {{ show.tipo_local ? '' : 'hide' }}" ng-model="search.tipo_local" name="tipo_local" id="tipo_local"/></td>
                                    <td><input type="text" class="form-control form-filter input-sm {{ show.tipo_local_subcategoria ? '' : 'hide' }}" ng-model="search.tipo_local_subcategoria" name="tipo_local_subcategoria" id="tipo_local_subcategoria"/></td>
                                    <td><input type="text" class="form-control form-filter input-sm {{ show.nombre_propietario ? '' : 'hide' }}" ng-model="search.nombre_propietario" name="nombre_local" id="nombre_local"/> </td>
                                    <td class="hide"><input type="text" class="form-control form-filter input-sm {{ show.tendero ? '' : 'hide' }}" ng-model="search.tendero" name="tendero" id="tendero"/> </td>
                                    <td><input type="text" class="form-control form-filter input-sm {{ show.nombre_local ? '' : 'hide' }}" ng-model="search.nombre_local" name="nombre_local" id="nombre_local"/> </td>                            
                                    <td class="hide"><input type="text" class="form-control form-filter input-sm {{ show.referencia ? '' : 'hide' }}" ng-model="search.referencia" name="referencia" id="referencia"/></td>
                                    <td class="hide"><input type="text" class="form-control form-filter input-sm {{ show.direccion ? '' : 'hide' }}" ng-model="search.direccion" name="direccion" id="direccion"/></td>
                                    <td class="hide"><input type="text" class="form-control form-filter input-sm {{ show.ruc ? '' : 'hide' }}" ng-model="search.ruc" name="ruc" id="ruc"/></td>
                                    <td class="hide"><input type="text" class="form-control form-filter input-sm {{ show.telefono ? '' : 'hide' }}" ng-model="search.telefono" name="telefono" id="telefono"/></td>
                                    <td class="hide"><input type="text" class="form-control form-filter input-sm {{ show.correo ? '' : 'hide' }}" ng-model="search.correo" name="correo" id="correo"/></td>
                                    <td><input type="text" class="form-control form-filter input-sm {{ show.ruta ? '' : 'hide' }}" ng-model="search.ruta" name="ruta" id="ruta"/></td>
                                    <td><input type="text" class="form-control form-filter input-sm {{ show.sector ? '' : 'hide' }}" ng-model="search.sector" name="sector" id="sector"/></td>
                                    <td></td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr role="row" ng-repeat="row in table | filter : {id: search.id, nombre_propietario : search.nombre_propietario , status : search.status, nombre_local : search.nombre_local, tipo_local : search.tipo_local, referencia: search.referencia, direccion: search.direccion, ruc: search.ruc, telefono: search.telefono, correo: search.correo, ruta: search.ruta , sector: search.sector} | orderObjectBy : search.orderBy : search.reverse | startFrom:(search.actual_page-1)*(search.limit | num) | limitTo : search.limit">
                                    <td class="center-th {{ show.id ? '' : 'hide' }}">{{ row.id }}</td>
                                    <td class="center-th {{ show.fecha ? '' : 'hide' }}">{{ row.fecha }}</td>
                                    <td class="center-th {{ show.status ? '' : 'hide' }}">{{ row.status }}</td>
                                    <td class="center-th {{ show.tipo_local ? '' : 'hide' }}">{{ row.tipo_local }}</td>
                                    <td class="center-th {{ show.tipo_local_subcategoria ? '' : 'hide' }}">{{ row.tipo_local_subcategoria }}</td>
                                    <td class="center-th {{ show.nombre_propietario ? '' : 'hide' }}">{{ row.nombre_propietario }}</td>
                                    <td class="center-th {{ show.tendero ? '' : 'hide' }}">{{ row.tendero }}</td>
                                    <td class="center-th {{ show.nombre_local ? '' : 'hide' }}">{{ row.nombre_local }}</td>
                                    <td class="center-th {{ show.referencia ? '' : 'hide' }}">{{ row.referencia }}</td>
                                    <td class="center-th {{ show.direccion ? '' : 'hide' }}">{{ row.direccion }}</td>
                                    <td class="center-th {{ show.ruc ? '' : 'hide' }}">{{ row.ruc }}</td>
                                    <td class="center-th {{ show.telefono ? '' : 'hide' }}">{{ row.telefono }}</td>
                                    <td class="center-th {{ show.correo ? '' : 'hide' }}">{{ row.correo }}</td>
                                    <td class="center-th {{ show.ruta ? '' : 'hide' }}">{{ row.ruta }}</td>
                                    <td class="center-th {{ show.sector ? '' : 'hide' }}">{{ row.sector }}</td>
                                    <td class="center-th {{ show.acciones ? '' : 'hide' }}"><a href="/lancofruit?id={{row.id}}" class="btn blue"  data-id="{{ row.id }}">Editar</a></td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="40">
                                        <div class="col-md-5 col-sm-12">
                                            Página {{ (search.actual_page | num) || 0 }} de {{ (table.length / (search.limit | num) | num) + (table.length % (search.limit | num) == 0 ? 0 : 1) }}
                                        </div>
                                        <div class="col-md-7 col-sm-12">
                                            <a class="btn btn-sm default prev" ng-click="prev(table)"><</a>
                                            <input type="text" maxlength="5" class="pagination-panel-input form-control input-sm input-inline input-mini" ng-model="search.actual_page"/>
                                            <a class="btn btn-sm default next" ng-click="next(table)">></a>
                                        </div>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                        <table class="hide" id="lancofruit">
                            <thead>
                                <tr>
                                    <th class="center-th {{ show.id ? '' : 'hide' }} selected sorting_asc" id="id_column" ng-click="changeSort('id')"> &nbsp;ID&nbsp; </th>
                                    <th class="center-th {{ show.fecha ? '' : 'hide' }}" id="fecha_column" ng-click="changeSort('fecha')"> &nbsp;&nbsp;&nbsp;Fecha&nbsp;&nbsp; </th>
                                    <th class="center-th {{ show.status ? '' : 'hide' }}" id="status_column" ng-click="changeSort('status')"> Status</th>
                                    <th class="center-th {{ show.tipo_local ? '' : 'hide' }}" id="tipo_local_column" ng-click="changeSort('tipo_local')"> Tipo </th>
                                    <th class="center-th {{ show.tipo_local_subcategoria ? '' : 'hide' }}" id="tipo_local_subcategoria_column" ng-click="changeSort('tipo_local_subcategoria')"> Subtipo </th>
                                    <th class="center-th {{ show.nombre_propietario ? '' : 'hide' }}" id="nombre_propietario_column" ng-click="changeSort('nombre_propietario')"> Nombre </th>
                                    <th class="center-th {{ show.tendero ? '' : 'hide' }}" id="tendero_column" ng-click="changeSort('tendero')"> Tendero </th>
                                    <th class="center-th {{ show.nombre_local ? '' : 'hide' }}" id="nombre_local_column" ng-click="changeSort('nombre_local')"> Local </th>
                                    <th class="center-th {{ show.referencia ? '' : 'hide' }}" id="referencia_column" ng-click="changeSort('referencia')"> Referencia </th>
                                    <th class="center-th {{ show.direccion ? '' : 'hide' }}" id="direccion_column" ng-click="changeSort('direccion')"> Dirección </th>
                                    <th class="center-th {{ show.ruc ? '' : 'hide' }}" id="ruc_column" ng-click="changeSort('ruc')"> Ruc </th>
                                    <th class="center-th {{ show.telefono ? '' : 'hide' }}" id="telefono_column" ng-click="changeSort('telefono')"> Teléfono </th>
                                    <th class="center-th {{ show.correo ? '' : 'hide' }}" id="correo_column" ng-click="changeSort('correo')"> Correo </th>
                                    <th class="center-th {{ show.ruta ? '' : 'hide' }}" id="ruta_column" ng-click="changeSort('ruta')"> Ruta </th>
                                    <th class="center-th {{ show.sector ? '' : 'hide' }}" id="sector_column" ng-click="changeSort('sector')"> Sector </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr role="row" ng-repeat="row in table | filter : {id: search.id, nombre_propietario : search.nombre_propietario , status : search.status, nombre_local : search.nombre_local, tipo_local : search.tipo_local, referencia: search.referencia, direccion: search.direccion, ruc: search.ruc, telefono: search.telefono, correo: search.correo, ruta: search.ruta , sector: search.sector} | orderObjectBy : search.orderBy : search.reverse | startFrom:(search.actual_page-1)*(search.limit | num) | limitTo : search.limit">
                                    <td class="center-th {{ show.id ? '' : 'hide' }}">{{ row.id }}</td>
                                    <td class="center-th {{ show.fecha ? '' : 'hide' }}">{{ row.fecha }}</td>
                                    <td class="center-th {{ show.status ? '' : 'hide' }}">{{ row.status }}</td>
                                    <td class="center-th {{ show.tipo_local ? '' : 'hide' }}">{{ row.tipo_local }}</td>
                                    <td class="center-th {{ show.tipo_local_subcategoria ? '' : 'hide' }}">{{ row.tipo_local_subcategoria }}</td>
                                    <td class="center-th {{ show.nombre_propietario ? '' : 'hide' }}">{{ row.nombre_propietario }}</td>
                                    <td class="center-th {{ show.tendero ? '' : 'hide' }}">{{ row.tendero }}</td>
                                    <td class="center-th {{ show.nombre_local ? '' : 'hide' }}">{{ row.nombre_local }}</td>
                                    <td class="center-th {{ show.referencia ? '' : 'hide' }}">{{ row.referencia }}</td>
                                    <td class="center-th {{ show.direccion ? '' : 'hide' }}">{{ row.direccion }}</td>
                                    <td class="center-th {{ show.ruc ? '' : 'hide' }}">{{ row.ruc != '' ? '#':'' }}{{ row.ruc }}</td>
                                    <td class="center-th {{ show.telefono ? '' : 'hide' }}">{{ row.telefono }}</td>
                                    <td class="center-th {{ show.correo ? '' : 'hide' }}">{{ row.correo }}</td>
                                    <td class="center-th {{ show.ruta ? '' : 'hide' }}">{{ row.ruta }}</td>
                                    <td class="center-th {{ show.sector ? '' : 'hide' }}">{{ row.sector }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <span class="caption"></span>
                    <div class="tools">
                    </div>
                </div>
                <div class="portlet-body" style="height:350px;">
                    <div class="col-md-12">
                        <select class="input-sm pull-right" ng-model="filters.id_categoria_pastel" ng-change="changeCategoriaPastel()">
                            <option value="{{c.id}}" ng-repeat="c in categorias_pie">{{ c.nombre }}</option>
                        </select>
                    </div>
                    <div class="col-md-6">
                        <div id="echarts_status" style="height:300px;"></div>
                    </div>
                    <div class="col-md-6">
                        <div id="echarts_tipo" style="height:300px;"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <span class="caption"></span>
                    <div class="tools">
                        
                    </div>
                </div>
                <div class="portlet-body" style="height:600px;">
                    <iframe src="http://livebahn.com/livebahn/#/" width="100%" height="100%"></iframe>
                </div>
            </div>
        </div>
    </div>

    <div id="nsecado" class="modal fade" tabindex="-1" data-width="400">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header blue">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">CONFIGURACIÓN</h4>
                </div>
                <div class="modal-body form">
                    <form role="form" class="form-horizontal">
                        <div class="form-actions">
                            <div class="col-md-2">
                                <label class="control-label">Ruta: </label>
                            </div>
                            <div class="col-md-4" style="font-size: 24px;">
                                <select id="ruta" class="form-control" ng-model="config.index_selected_ruta" ng-show="!config.modeNew" ng-change="config.refreshRuta()">
                                    <option ng-repeat="ruta in config.rutas" value="{{ $index }}">{{ ruta.nombre }}</option>
                                </select>
                                <input type="text" class="form-control" ng-model="config.ruta" ng-show="config.modeNew"/>
                            </div>
                            <div class="col-md-6">
                                <button class="btn green-meadow" type="button" ng-click="config.newRuta()">Nuevo</button>
                                <button type="button" class="btn dark btn-outline" ng-show="config.modeNew" ng-click="config.cancel()">Cancelar</button>
                                <button type="button" class="btn blue" ng-show="config.modeNew" ng-click="config.save()">Guardar</button>
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="col-md-2">
                                <label class="control-label" ng-show="config.modeNewVendedor">Vendedor: </label>
                            </div>
                            <div class="col-md-4" style="font-size: 24px;">
                                <input type="text" class="form-control" ng-model="config.vendedor" ng-show="config.modeNewVendedor"/>
                            </div>
                            <div class="col-md-6">
                                <button class="btn green-meadow" type="button" ng-click="config.newVendedor()">Nuevo Vendedor</button>
                                <button type="button" class="btn dark btn-outline" ng-show="config.modeNewVendedor" ng-click="config.cancel()">Cancelar</button>
                                <button type="button" class="btn blue" ng-show="config.modeNewVendedor" ng-click="config.saveVendedor()">Guardar</button>
                            </div>
                        </div>
                        <div class="form-body">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <select multiple="multiple" class="multi-select form-control" id="my_multi_select1" name="my_multi_select1[]">
                                        <option ng-repeat="v in config.no_asignados" value="{{v.id}}">{{v.nombre}}</option>
                                        <option ng-repeat="v in config.selected_ruta.vendedores" value="{{v.id}}" selected>{{v.nombre}}</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-7" style="font-size: 24px;" id="total_sacos">
                                    
                                </div>
                                <div class="col-md-5">
                                    <button type="button" ng-click="config.saveConfig()" class="btn dark btn-outline">Guardar</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>