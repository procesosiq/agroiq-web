<style>
    .chart {
        height : 400px;
    }
    td, th {
        text-align : center;
    }
    .card {
        /* Add shadows to create the "card" effect */
        box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
        transition: 0.3s;
        margin-bottom: 15px;
    }

    /* On mouse-over, add a deeper shadow */
    .card:hover {
        box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
    }

    /* Add some padding inside the card container */
    .container {
        padding: 2px 16px;
    }

    .main-row {
        border: 2px solid #d6d6d6;
        background-color: #dfdfdf87;
    }
    .pointer {
        cursor : pointer;
    }

    .calendar-table td.available {
		background-color: green !important;
    	color: white !important;
	}
	.icalendar-table td.available:hover {
		background-color: #285e8e !important;
    	color: white !important;
	}
</style>

<script src="assets/global/plugins/FileSaver.min.js"></script>

<div ng-controller="controller" ng-cloak>
    <div style="width: 100%;margin-bottom: 10px;display:table;">
        <h3 class="page-title" style="display: table-cell !important;"> 
            Reporte de Comparación
        </h3>
    </div>

    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <i class="icon-home"></i>
                <a>Calidad</a>
                <i class="fa fa-angle-right"></i>
            </li>
        </ul>
        <div class="page-toolbar">
            Marca
            <select class="input-sm" ng-model="filters.marca" ng-change="init()" style="height: 40px;">
                <option value="">TODOS</option>
                <option value="{{marca}}" ng-repeat="marca in marcas">{{ marca }}</option>
            </select>
            <ng-calendarapp  search="search"></ng-calendarapp>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet box green-haze">
                <div class="portlet-title">
                    <div class="caption">
                        <span class="caption-subject bold uppercase">Comparación</span>
                    </div>
                    <ul class="nav nav-tabs">
                        <li ng-class="{ 'active' : mode == 'general' }">
                            <a ng-click="mode = 'general'">General</a>
                        </li>
                        <li ng-class="{ 'active' : mode == 'defectos' }">
                            <a ng-click="mode = 'defectos'">Defectos</a>
                        </li>
                        <li ng-class="{ 'active' : mode == 'empaque' }" ng-show="config.calidad_empaque">
                            <a ng-click="mode = 'empaque'">Detalles Caja</a>
                        </li>
                        <li ng-class="{ 'active' : mode == 'cluster' }">
                            <a ng-click="mode = 'cluster'">Cluster</a>
                        </li>
                    </ul>
                </div>
                <div class="portlet-body" id="principal">

                    <div class="table-responsive text-right" ng-class="{ 'hide' : mode != 'general' }">
                        <button class="btn bg-dark bg-font-dark" ng-click="exportExcel('general', 'General')">
                            Exportar
                        </button>
                        <table class="table table-hover table-bordered" id="general">
                            <thead>
                                <tr>
                                    <th class="pointer" ng-click="toggleOrder('general', 'zona')">Detalle</th>
                                    <th class="pointer" ng-click="toggleOrder('general', 'calidad_cluster')">% Calidad Cluster</th>
                                    <th class="pointer" ng-click="toggleOrder('general', 'calidad_dedos')">% Calidad Dedos</th>
                                    <th ng-if="config.calidad_empaque" class="pointer" ng-click="toggleOrder('general', 'calidad_empaque')">% Calidad Empaque</th>
                                    <th class="pointer" ng-click="toggleOrder('general', 'cluster_promedio')">Cluster Prom/Caja</th>
                                    <th class="pointer" ng-click="toggleOrder('general', 'dedos_promedio')">Dedos Prom/Caja</th>
                                    <th ng-if="config.peso_prom_cluster" class="pointer" ng-click="toggleOrder('general', 'peso_prom_cluster')">Peso Prom Cluster (gr)</th>
                                    <th class="pointer" ng-click="toggleOrder('general', 'muestras')">Muestras</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="main-row" ng-repeat-start="row in data.general | orderObjectBy : orderBy.general.key : orderBy.general.direction" ng-click="row.expanded = !row.expanded">
                                    <td>{{ row.zona }}</td>
                                    <td class="{{ getClusterUmbral(row.calidad_cluster) }}">{{ row.calidad_cluster | number : 2 }}</td>
                                    <td class="{{ getDedosUmbral(row.calidad_dedos) }}">{{ row.calidad_dedos | number : 2 }}</td>
                                    <td ng-if="config.calidad_empaque" class="{{ getEmpaqueUmbral(row.calidad_empaque) }}">{{ row.calidad_empaque | number : 2 }}</td>
                                    <td>{{ row.cluster_promedio | number : 2 }}</td>
                                    <td>{{ row.dedos_promedio | number : 2 }}</td>
                                    <td>{{ row.peso_prom_cluster | number : 2 }}</td>
                                    <td>{{ row.muestras | number : 2 }}</td>
                                </tr>
                                <tr ng-repeat-end ng-repeat="finca in row.detalle" ng-show="row.expanded">
                                    <td>{{ finca.zona }}</td>
                                    <td class="{{ getClusterUmbral(finca.calidad_cluster) }}">{{ finca.calidad_cluster | number : 2 }}</td>
                                    <td class="{{ getDedosUmbral(finca.calidad_dedos) }}">{{ finca.calidad_dedos | number : 2 }}</td>
                                    <td ng-if="config.calidad_empaque" class="{{ getEmpaqueUmbral(finca.calidad_empaque) }}">{{ finca.calidad_empaque | number : 2 }}</td>
                                    <td>{{ finca.cluster_promedio | number : 2 }}</td>
                                    <td>{{ finca.dedos_promedio | number : 2 }}</td>
                                    <td ng-if="config.peso_prom_cluster">{{ finca.peso_prom_cluster | number : 2 }}</td>
                                    <td>{{ finca.muestras }}</td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>TODOS</th>
                                    <th class="{{ getClusterUmbral((data.general | avgOfValue : 'calidad_cluster')) }}">{{ data.general | avgOfValue : 'calidad_cluster' | number : 2 }}</th>
                                    <th class="{{ getDedosUmbral((data.general | avgOfValue : 'calidad_dedos')) }}">{{ data.general | avgOfValue : 'calidad_dedos' | number : 2 }}</th>
                                    <th ng-if="config.calidad_empaque" class="{{ getEmpaqueUmbral((data.general | avgOfValue : 'calidad_empaque')) }}">{{ data.general | avgOfValue : 'calidad_empaque' | number : 2 }}</th>
                                    <th>{{ data.general | avgOfValue : 'cluster_promedio' | number : 2 }}</th>
                                    <th>{{ data.general | avgOfValue : 'dedos_promedio' | number : 2 }}</th>
                                    <th ng-if="config.peso_prom_cluster">{{ data.general | avgOfValue : 'peso_prom_cluster' | number : 2 }}</th>
                                    <th>{{ data.general | avgOfValue : 'muestras' | number : 2 }}</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>

                    <div class="table-responsive text-right" ng-class="{ 'hide' : mode != 'defectos' }">
                        <button class="btn bg-dark bg-font-dark" ng-click="exportExcel('defectos', 'Defectos')">
                            Exportar
                        </button>
                        <table class="table table-hover table-bordered" id="defectos">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th colspan="{{ categoria.defectos.length }}" ng-repeat="categoria in data.defectos.categorias">{{ categoria.type }}</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                                <tr>
                                    <th class="pointer" ng-click="toggleOrder('defectos', 'zona')">Detalle</th>
                                    <!-- DEFECTOS -->
                                    <td ng-repeat-start="categoria in data.defectos.categorias" class="hide"></td>
                                    <td class="pointer" ng-click="toggleOrder('defectos', categoria.type+'_'+d.siglas)" ng-repeat-end ng-repeat="d in categoria.defectos" title="{{ d.descripcion }}">{{ d.siglas }}</td>
                                    <th class="pointer" ng-click="toggleOrder('defectos', 'total_defectos')">Total</th>
                                    <th class="pointer" ng-click="toggleOrder('defectos', 'total_defectos_porc')">%</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="main-row" ng-repeat-start="row in data.defectos.data | orderObjectBy : orderBy.defectos.key : orderBy.defectos.direction" ng-click="row.expanded = !row.expanded">
                                    <td>{{ row.zona }}</td>
                                    <!-- DEFECTOS -->
                                    <td ng-repeat-start="categoria in data.defectos.categorias" class="hide"></td>
                                    <td ng-repeat-end ng-repeat="d in categoria.defectos">{{ row[categoria.type+'_'+d.siglas] }}</td>
                                    <td>{{ row.total_defectos }}</td>
                                    <td>{{ row.total_defectos_porc = (row.total_defectos/(data.defectos.data | sumOfValue : 'total_defectos')*100) | number : 2 }}</td>
                                </tr>
                                <tr ng-repeat-end ng-repeat="finca in row.detalle" ng-show="row.expanded">
                                    <td>{{ finca.zona }}</td>
                                    <!-- DEFECTOS -->
                                    <td ng-repeat-start="categoria in data.defectos.categorias" class="hide"></td>
                                    <td ng-repeat-end ng-repeat="d in categoria.defectos">{{ finca[categoria.type+'_'+d.siglas] }}</td>
                                    <td>{{ finca.total_defectos }}</td>
                                    <td>{{ finca.total_defectos/(data.defectos.data | sumOfValue : 'total_defectos')*100 | number : 2 }}</td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr style="border-top : 1px solid black;">
                                    <th>TODOS</th>
                                    <td ng-repeat-start="categoria in data.defectos.categorias" class="hide"></td>
                                    <td ng-repeat-end ng-repeat="d in categoria.defectos">{{ data.defectos.data | sumOfValue : categoria.type+'_'+d.siglas }}</td>
                                    <th>{{ data.defectos.data | sumOfValue : 'total_defectos' }}</th>
                                    <th>100%</th>
                                </tr>
                                <tr>
                                    <th></th>
                                    <td ng-repeat-start="categoria in data.defectos.categorias" class="hide"></td>
                                    <td ng-repeat-end ng-repeat="d in categoria.defectos"><span class="{{ getUmbralDefectos(totales_defectos[categoria.type+'_'+d.siglas]) }}">{{ totales_defectos[categoria.type+'_'+d.siglas] = ((data.defectos.data | sumOfValue : categoria.type+'_'+d.siglas)/(data.defectos.data | sumOfValue : 'total_defectos')*100) | number:2 }}%</span></td>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>

                    <div class="table-responsive text-right" ng-class="{ 'hide' : mode != 'empaque' }">
                        <button class="btn bg-dark bg-font-dark" ng-click="exportExcel('empaque', 'Empaque')">
                            Exportar
                        </button>
                        <table class="table table-hover table-bordered" id="empaque">
                            <thead>
                                <tr>
                                    <th class="pointer" ng-click="toggleOrder('empaque', 'zona')">Detalle</th>
                                    <!-- DEFECTOS -->
                                    <th class="pointer" ng-click="toggleOrder('empaque', d)" ng-repeat="d in data.empaque.defectos">{{ d }}</th>
                                    <th class="pointer" ng-click="toggleOrder('empaque', 'total_defectos')">Total</th>
                                    <th class="pointer" ng-click="toggleOrder('empaque', 'total_defectos_porc')">%</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="main-row" ng-repeat-start="row in data.empaque.data | orderObjectBy : orderBy.empaque.key : orderBy.empaque.direction" ng-click="row.expanded = !row.expanded">
                                    <td>{{ row.zona }}</td>
                                    <!-- DEFECTOS -->
                                    <td ng-repeat="d in data.empaque.defectos" width="10%">{{ row[d] > 0 ? row[d] : '' }}</td>
                                    <td>{{ row.total_defectos }}</td>
                                    <td>{{ row.total_defectos_porc = (row.total_defectos/(data.empaque.data | sumOfValue : 'total_defectos')*100) | number : 2 }}</td>
                                </tr>
                                <tr ng-repeat-end ng-repeat="finca in row.detalle" ng-show="row.expanded">
                                    <td>{{ finca.zona }}</td>
                                    <td ng-repeat="d in data.empaque.defectos" width="10%">{{ finca[d] > 0 ? finca[d] : '' }}</td>
                                    <td>{{ finca.total_defectos }}</td>
                                    <td>{{ finca.total_defectos_porc = (finca.total_defectos/(data.empaque.data | sumOfValue : 'total_defectos')*100) | number : 2 }}</td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>TODOS</th>
                                    <th ng-repeat="d in data.empaque.defectos">{{ data.empaque.data | sumOfValue : d }}</th>
                                    <th>{{ data.empaque.data | sumOfValue : 'total_defectos' }}</th>
                                    <th>100%</th>
                                </tr>
                                <tr>
                                    <th></th>
                                    <th ng-repeat="d in data.empaque.defectos"><span class="{{ getUmbralEmpaque(totales_empaque[d]) }}">{{ totales_empaque[d] = ((data.empaque.data | sumOfValue : d)/(data.empaque.data | sumOfValue : 'total_defectos')*100) | number : 2 }}%</span></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>

                    <div class="table-responsive text-right" ng-class="{ 'hide' : mode != 'cluster' }">
                        <button class="btn bg-dark bg-font-dark" ng-click="exportExcel('cluster', 'Cluster')">
                            Exportar
                        </button>
                        <table class="table table-hover table-bordered" id="cluster">
                            <thead>
                                <tr>
                                    <th class="pointer" ng-click="toggleOrder('cluster', 'zona')">Detalle</th>
                                    <!-- DEFECTOS -->
                                    <th class="pointer" ng-click="toggleOrder('cluster', 'tipo_'+t)" ng-repeat="t in data.cluster.tipos">{{ t }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="main-row" ng-repeat-start="row in data.cluster.data | orderObjectBy : orderBy.cluster.key : orderBy.cluster.direction" ng-click="row.expanded = !row.expanded">
                                    <td>{{ row.zona }}</td>
                                    <!-- DEFECTOS -->
                                    <td ng-repeat="t in data.cluster.tipos">{{ row['tipo_'+t] }}</td>
                                </tr>
                                <tr ng-repeat-end ng-repeat="finca in row.detalle" ng-show="row.expanded">
                                    <td>{{ finca.zona }}</td>
                                    <!-- DEFECTOS -->
                                    <td ng-repeat="t in data.cluster.tipos">{{ finca['tipo_'+t] }}</td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>TODOS</th>
                                    <th ng-repeat="t in data.cluster.tipos">{{ data.cluster.data | avgOfValue : 'tipo_'+t | number : 2 }}</th>
                                </tr>
                                <tr>
                                    <th></th>
                                    <th ng-repeat="t in data.cluster.tipos">{{ ((data.cluster.data | sumOfValue : 'tipo_'+t)/(data.cluster.data | sumOfValue : 'total')*100) | number : 2 }}%</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="tabbable tabbable-tabdrop">
                <ul class="nav nav-tabs">
                    <li ng-class="{ 'active' : mode_graficas == 'defectos' }">
                        <a ng-click="mode_graficas = 'defectos'; moda_graficas_desc = '(# Daños)'; renderGraficaZona(modeCategoriaGraficaZona); renderGraficaFinca(2, modeCategoriaGraficaZona)">Defectos</a>
                    </li>
                    <li ng-class="{ 'active' : mode_graficas == 'empaque' }" ng-show="config.calidad_empaque">
                        <a ng-click="mode_graficas = 'empaque'; moda_graficas_desc = '(# Problemas)'; renderGraficaZona(); renderGraficaFinca()">Detalles Caja</a>
                    </li>
                    <li ng-class="{ 'active' : mode_graficas == 'cluster' }">
                        <a ng-click="mode_graficas = 'cluster'; moda_graficas_desc = '(# Cluster)'; renderGraficaZona(); renderGraficaFinca()">Cluster</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-md-6">
            <div class="portlet box green-haze">
                <div class="portlet-title">
                    <div class="caption">
                        <span class="caption-subject bold uppercase">Grafica Comp. Zona. {{moda_graficas_desc}}</span>
                    </div>
                    <ul class="nav nav-tabs">
                        <li ng-repeat="categoria in dataGraficaZona[mode_graficas].categorias" ng-class="{ 'active' : modeCategoriaGraficaZona == categoria }">
                            <a ng-click="renderGraficaZona(categoria); renderGraficaFinca(categoria)">{{ categoria }}</a>
                        </li>
                    </ul>
                </div>
                <div class="portlet-body" id="graficas-zona">
                    <div id="grafica-zona-chart" class="chart">
                    
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="portlet box green-haze">
                <div class="portlet-title">
                    <div class="caption">
                        <span class="caption-subject bold uppercase">Grafica Com. Finca. {{ moda_graficas_desc }}</span>
                    </div>
                    <div class="tools">
                        Zona 
                        <select class="input-sm" style="color : black;" ng-model="modeZonaGraficaFinca" ng-change="renderGraficaFinca(modeCategoriaGraficaZona)">
                            <option value="{{zona}}" ng-repeat="zona in dataGraficaFinca[mode_graficas].zonas">{{zona}}</option>
                        </select>
                    </div>
                </div>
                <div class="portlet-body" id="graficas-finca">
                    <div id="grafica-finca-chart" class="chart">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>