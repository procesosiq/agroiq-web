<?php
    $cdn = "http://cdn.procesos-iq.com/";
    $factory->path = "sigat";
    $response = $factory->Fincas->edit();
?>
<style>
    .center-th {
        text-align : center;
    }
    #rowsFilas > tr > td {
        text-align : center;   
    }
    tfoot > tr > td {
        text-align : center;   
    }
</style>
<div class="page-title">
    <h1>Módulo de Fincas</h1>
</div>
<div class="page-bar">
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="fincas">Listado de Fincas</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <span class="active">Registro de Fincas</span>
        </li>
    </ul>
</div>
<input type="hidden" id="json_gerentes" value='<?= json_encode($response->gerentes) ?>'/>
<!-- END PAGE BREADCRUMB -->
<!-- BEGIN PAGE BASE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <div class="tab-pane" id="tab_1">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift"></i>AGREGAR NUEVO FINCA</div>
                </div>
                <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    <form action="#" class="horizontal-form" id="formcli" method="post">
                        <div class="form-actions right">
                            <button type="button" class="btn default cancel">Cancelar</button>
                            <button type="button" class="btn blue btnadd" >
                                <i class="fa fa-check"></i><span class="lbladd">Registrar<span></button>
                        </div>
                        <div class="form-body">
                            <h3 class="form-section row">
                                <div class="col-md-4">
                                    INFORMACIÓN DE LA FINCA
                                </div>
                                <div class="col-md-4 col-md-offset-4">
                                    Fecha de Registro
                                    <input disabled type="text" value="<?php echo $response->data->fecha?>" id="txtfec" class="form-control save" placeholder="dd/mm/yyyy">
                                </div>
                            </h3>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Nombre</label>
                                        <input type="text" value="<?php echo $response->data->nombre?>" id="txtnom" class="form-control save">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Gerentes</label>
                                        <select id="gerentes" name="gerentes" class="form-control" style="height: 34px;">
                                            <option></option>
                                            <?php foreach($response->gerentes as $gerente) : ?>
                                                <option value="<?= $gerente->id ?>" <?=$gerente->selected?> >
                                                        <?=$gerente->nombre?>
                                                </option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <!--/row-->
                            <?php
                                if(isset($_GET['id']) && $_GET['id'] > 0):
                            ?>
                            <h3 class="form-section">ASIGNAR SECTORES</h3>
                            <div class="row">
                                <div class="col-md-12 ">
                                    <table id="shorter" class="table table-striped table-bordered table-hover table-checkable">
                                        <thead>
                                            <tr style="cursor: pointer;">
                                                <th class="alphanumeric center-th" width="10%">Sector</th>
                                                <th class="alphanumeric center-th" width="20%">Hectarea de<br> Fumigación</th>
                                                <th class="alphanumeric center-th" width="20%">Hectarea de<br> Producción</th>
                                                <th class="alphanumeric center-th" width="20%">Hectarea<br> Neta</th>
                                                <th width="30%">Acciones</th>
                                            </tr>
                                            <tr>
                                                <td>
                                                <select class="form-control" data-placeholder="Seleccione un Sector" tabindex="1" id="s_sector">
                                                    <option value="A">A</option>
                                                    <option value="B">B</option>
                                                    <option value="C">C</option>
                                                    <option value="D">D</option>
                                                    <option value="E">E</option>
                                                    <option value="F">F</option>
                                                </select>
                                                </td>
                                                <td><input type="text" value="" id="fumigacion" class="form-control"></td>
                                                <td><input type="text" value="" id="produccion" class="form-control"></td>
                                                <td><input type="text" value="" id="neta" class="form-control"></td>
                                                <td><button class="btn btn-success addRow" id="btnaddarea" type="button">
                                                        <i class="fa fa-plus"></i> Agregar</button></td>
                                            </tr>
                                        </thead>
                                        <tbody id="rowsFilas">
                                            <?php
                                                if(count($response->sectores) > 0){
                                                    $hec_fumigacion = 0;
                                                    $hec_produccion = 0;
                                                    $hec_neta = 0;
                                                    foreach ($response->sectores as $key => $value) {
                                                        $hec_fumigacion += $value->hec_fumigacion;
                                                        $hec_produccion += $value->hec_produccion;
                                                        $hec_neta += $value->hec_neta;
                                                        echo '<tr>';
                                                        echo '<td>'.$value->sector.'</td>';
                                                        echo '<td class="hec_fumigacion">'.$value->hec_fumigacion.'</td>';
                                                        echo '<td class="hec_produccion">'.$value->hec_produccion.'</td>';
                                                        echo '<td class="hec_neta">'.$value->hec_neta.'</td>';
                                                        echo '<td><button type="button" class="btn btn-sm red-thunderbird removeRow" id="'.$value->id.'">Eliminar</button>
                                                        </td>';
                                                        echo '</tr>';
                                                    }
                                                }
                                            ?>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td>Total : </td>
                                                <td class="hec_fumigacion_total"><?=$hec_fumigacion?></td>
                                                <td class="hec_produccion_total"><?=$hec_produccion?></td>
                                                <td class="hec_neta_total"><?=$hec_neta?></td>
                                                <td></td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                            <?php endif;?>
                            <!-- <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>RUC</label>
                                        <input type="text" class="form-control" id="txtruc"> </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Dirección</label>
                                        <input type="text" class="form-control" id="txtdircli"> </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Teléfono</label>
                                        <input type="text" class="form-control" id="txttel"> </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Ciudad</label>
                                        <input type="text" class="form-control" id="txtciudad"> </div>
                                </div>
                            </div> -->
                        </div>
                        <div class="form-actions right">
                            <button type="button" class="btn default cancel">Cancelar</button>
                            <button type="button" class="btn blue btnadd" >
                                <i class="fa fa-check"></i>Registrar</button>
                        </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END PAGE BASE CONTENT -->