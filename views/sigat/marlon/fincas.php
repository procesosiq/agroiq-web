<?php
    /*=============================================================
    =            Insertar aqui validacion  de usuarios            =
    =============================================================*/
    $cdn = "http://cdn.procesos-iq.com/";
    
    
    /*=====  End of Insertar aqui validacion  de usuarios  ======*/
?>
<div class="page-title">
    <h1>Listado de Fincas
        <small>Fincas actualmente registrados</small>
    </h1>
</div>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="javascript:;">Inicio</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="#">Fincas</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <span class="active">Listado</span>
        </li>
    </ul>
</div>
<div class="row">
    <div class="col-md-12">
        <!-- Begin: life time stats -->
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject font-dark sbold uppercase">Listado de Fincas</span>
                </div>
                <div class="actions">
                    <div class="btn-group btn-group-devided" data-toggle="buttons">
                            <a class="btn newFinca sbold uppercase btn-outline blue-ebonyclay">Nueva Finca</a>
                    </div>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-container">
                    <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax">
                        <thead>
                            <tr role="row" class="heading">
                                <th width="15%"> Fecha Activo </th>
                                <th width="5%"> ID&nbsp;# </th>
                                <th width="25%"> Nombre </th>
                                <th width="10%"> Estado </th>
                                <th width="25%"> Gerente de<br>Producción </th>
                                <th width="15%"> Acciones </th>
                            </tr>
                            <tr role="row" class="filter">
                                <td>
                                    <div class="input-group date date-picker margin-bottom-5" data-date-format="yyyy-mm-dd">
                                        <input type="text" class="form-control form-filter input-sm" readonly name="search_date_from" placeholder="From">
                                        <span class="input-group-btn">
                                            <button class="btn btn-sm default" type="button">
                                                <i class="fa fa-calendar"></i>
                                            </button>
                                        </span>
                                    </div>
                                    <div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
                                        <input type="text" class="form-control form-filter input-sm" readonly name="search_date_to" placeholder="To">
                                        <span class="input-group-btn">
                                            <button class="btn btn-sm default" type="button">
                                                <i class="fa fa-calendar"></i>
                                            </button>
                                        </span>
                                    </div>
                                </td>
                                <td>
                                    <input type="text" class="form-control form-filter input-sm" name="search_id"> 
                                </td>
                                <td>
                                    <input type="text" class="form-control form-filter input-sm" name="search_name"> 
                                </td>
                                <td>
                                    <select name="order_status" class="form-control form-filter input-sm">
                                        <option value="">Select...</option>
                                        <option value="1">Activo</option>
                                        <option value="0">Inactivo</option>
                                    </select>
                                </td>
                                <td>
                                    <input type="text" class="form-control form-filter input-sm" name="search_gerente"> 
                                </td>
                                <td>
                                    <div class="margin-bottom-5">
                                        <button class="btn btn-sm green btn-outline filter-submit margin-bottom"><i class="fa fa-search"></i> </button>
                                        <button class="btn btn-sm red btn-outline filter-cancel"><i class="fa fa-times"></i> </button>
                                    </div>
                                </td>
                            </tr>
                        </thead>
                        <tbody> </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>
</div>