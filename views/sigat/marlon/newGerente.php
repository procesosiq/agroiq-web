<?php

    $cdn = "http://cdn.procesos-iq.com/";
    $response = json_decode($loader->edit());
?>
<style>
    .custom-header{
        background: #337ab7;
        color: white;
        padding: 2px;
        font-weight: bold;
        text-align: center;
    }
</style>
<link href="<?=$cdn?>/global/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" type="text/css" />
<link href="<?=$cdn?>/global/plugins/jquery-multi-select/css/multi-select.css" rel="stylesheet" type="text/css" />
<link href="<?=$cdn?>/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<link href="<?=$cdn?>/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Módulo de Gerente</h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="gerentesList">Listado de Gerente</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">Registro de Gerente</span>
                        </li>
                    </ul>
                    <!-- END PAGE BREADCRUMB -->
                    <!-- BEGIN PAGE BASE CONTENT -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="tab-pane" id="tab_1">
                                <div class="portlet box blue">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-gift"></i>AGREGAR NUEVO GERENTE DE PRODUCCIÓN</div>
                                    </div>
                                    <div class="portlet-body form">
                                        <!-- BEGIN FORM-->
                                        <form action="#" class="horizontal-form" id="formcli" method="post">
                                            <div class="form-actions right">
                                                <input type="hidden" id="fincas_json" value="<?=$response->fincas?>">
                                                <button type="button" class="btn default cancel">Cancelar</button>
                                                <button type="button" class="btn blue btnadd" >
                                                    <i class="fa fa-check"></i><span class="lbladd">Registrar</span></button>
                                            </div>
                                            <div class="form-body">
                                                <h3 class="form-section">INFORMACIÓN DEL GERENTE DE PRODUCCIÓN</h3>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label">Nombre</label>
                                                                <input type="text" value="<?php echo $response->data->nombre?>" id="nombre" class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>
                                                     <div class="col-md-6">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label">Correo</label>
                                                                <input type="text" value="<?php echo $response->data->email?>" id="user" class="form-control">
                                                            </div>
                                                        </div>
                                                         <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label">Contraseña</label>
                                                                <input type="password" value="<?php echo $response->data->password?>" id="pass" class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-9">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label">Fincas</label>
                                                                <select multiple="multiple" class="multi-select" id="fincas" name="fincas[]">
                                                                <?php
                                                                    if(count($response->fincas) > 0){
                                                                        $optgroup = "";
                                                                        $count = 0;
                                                                        foreach ($response->fincas as $key => $value) {
                                                                            if($optgroup != $value->grupo){
                                                                                $optgroup = $value->grupo;
                                                                                if($count > 0){
                                                                                    echo '</optgroup>';
                                                                                }
                                                                                echo "<optgroup label='{$value->grupo}'>";
                                                                            }
                                                                            echo '<option value="'.$value->id.'" '.$value->selected.'>'.$value->nombre.'</option>';
                                                                        }
                                                                        echo '</optgroup>';
                                                                    }
                                                                ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/row-->
                                            </div>
                                            <div class="form-actions right">
                                                <button type="button" class="btn default cancel">Cancelar</button>
                                                <button type="button" class="btn blue btnadd" >
                                                    <i class="fa fa-check"></i><span class="lbladd">Registrar</span></button>
                                            </div>
                                        </form>
                                        <!-- END FORM-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->