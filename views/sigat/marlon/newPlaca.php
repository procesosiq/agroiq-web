<?php
    $response = json_decode($loader->edit());
    // print_r($response);
?>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Módulo de Placas</h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="placasList">Listado de Placas</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">Registro de Placas</span>
                        </li>
                    </ul>
                    <!-- END PAGE BREADCRUMB -->
                    <!-- BEGIN PAGE BASE CONTENT -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="tab-pane" id="tab_1">
                                <div class="portlet box blue">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-gift"></i>AGREGAR NUEVA PLACA</div>
                                    </div>
                                    <div class="portlet-body form">
                                        <!-- BEGIN FORM-->
                                        <form action="#" class="horizontal-form" id="formcli" method="post">
                                            <div class="form-actions right">
                                                <button type="button" class="btn default cancel">Cancelar</button>
                                                <button type="button" class="btn blue btnadd" >
                                                    <i class="fa fa-check"></i>Registrar</button>
                                            </div>
                                            <div class="form-body">
                                                <h3 class="form-section">INFORMACIÓN DE LA PLACA</h3>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Nombre</label>
                                                            <input type="text" value="<?php echo $response->data->nombre?>" id="txtnom" class="form-control">
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Fumigadora</label>
                                                            <select class="form-control" data-placeholder="Seleccione una Fumigadora" tabindex="1" id="s_fumigadora">
                                                            <option value="">Seleccione</option>
                                                            option
                                                            <?php
                                                                if(count($response->fumigadoras) > 0){
                                                                    $selected = "";
                                                                    foreach ($response->fumigadoras as $key => $value) {
                                                                        $selected = "";
                                                                        if(isset($response->data->id_fumigadora) && $value->id == $response->data->id_fumigadora){
                                                                            $selected="selected";
                                                                        }
                                                                        echo '<option value="'.$value->id.'" '.$selected.'>'.$value->nombre.'</option>';
                                                                    }
                                                                }

                                                            ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/row-->
                                            </div>
                                            <div class="form-actions right">
                                                <button type="button" class="btn default cancel">Cancelar</button>
                                                <button type="button" class="btn blue btnadd" >
                                                    <i class="fa fa-check"></i>Registrar</button>
                                            </div>
                                        </form>
                                        <!-- END FORM-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->