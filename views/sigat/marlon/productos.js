'use strict';

app.filter('startFrom', function () {
    return function (input, start) {
        start = +start; //parse to int
        return input.slice(start);
    };
});

app.service('productos', function ($http, $q) {
    var defered = $q.defer();
    var promise = defered.promise;
    var service = {};
    var options = {
        url: "./controllers/index.php",
        method: "POST"
    };

    service.listado = function () {
        var option = options;
        option.data = {
            accion: "Productos"
        };
        return $http(option);
    };

    service.formuladoras = function () {
        var option = options;
        option.data = {
            accion: "Productos.getFormuladoras"
        };
        return $http(option);
    };

    service.proveedores = function () {
        var option = options;
        option.data = {
            accion: "Productos.getProveedores"
        };
        return $http(option);
    };

    service.tipoProductos = function () {
        var option = options;
        option.data = {
            accion: "Productos.getTipoProductos"
        };
        return $http(option);
    };

    service.frac = function () {
        var option = options;
        option.data = {
            accion: "Productos.getFrac"
        };
        return $http(option);
    };

    service.acciones = function () {
        var option = options;
        option.data = {
            accion: "Productos.getAccion"
        };
        return $http(option);
    };

    service.show = function (id_producto) {
        var option = options;
        option.data = {
            accion: "Productos.show",
            id_producto: id_producto
        };
        return $http(option);
    };

    service.create = function (producto) {
        var option = options;
        producto.accion = "Productos.create";
        option.data = producto;
        return $http(option);
    };

    service.update = function (producto) {
        var option = options;
        producto.accion = "Productos.update";
        option.data = producto;
        return $http(option);
    };

    return service;
});

app.controller('productos', ['$scope', '$http', '$interval', 'productos', '$controller', function ($scope, $http, $interval, productos, $controller) {

    $scope.formuladoras = [];
    $scope.proveedores = [];
    $scope.tipoProductos = [];
    $scope.ingredientes_actios = [];
    $scope.fracs = [];
    $scope.acciones = [];

    // 25/04/2017 - TAG: MODEL FOR PRODUCTS
    $scope.producto = {
        id_producto: 1,
        codigo: "",
        nombreComercial: "",
        formuladora: "",
        proveedor: "",
        tipo_producto: "",
        ingrediente_activo: "",
        frac: "",
        action: ""
    };

    $scope.proccess = function (r) {
        if (r.hasOwnProperty("data")) {
            return r.data;
        }
    };

    $scope.formuladoras = function () {
        productos.formuladoras().then(function (data) {
            return $scope.formuladoras = $scope.proccess(data);
        }).catch(function (error) {
            return console.log(error);
        });
    };

    $scope.proveedores = function () {
        productos.proveedores().then(function (data) {
            return $scope.proveedores = $scope.proccess(data);
        }).catch(function (error) {
            return console.log(error);
        });
    };

    $scope.tipoProductos = function () {
        productos.tipoProductos().then(function (data) {
            return $scope.tipoProductos = $scope.proccess(data);
        }).catch(function (error) {
            return console.log(error);
        });
    };

    $scope.frac = function () {
        productos.frac().then(function (data) {
            return $scope.fracs = $scope.proccess(data);
        }).catch(function (error) {
            return console.log(error);
        });
    };

    $scope.acciones = function () {
        productos.acciones().then(function (data) {
            return $scope.acciones = $scope.proccess(data);
        }).catch(function (error) {
            return console.log(error);
        });
    };

    $scope.show = function () {
        productos.show($scope.producto.id_producto).then(function (data) {
            var response = $scope.proccess(data);
            if (response.hasOwnProperty("success") && response.success == 200) {
                $scope.producto = response.data;
            }
        }).catch(function (error) {
            return console.log(error);
        });
    };

    $scope.init = function () {
        $scope.formuladoras();
        $scope.proveedores();
        $scope.tipoProductos();
        $scope.frac();
        $scope.acciones();
        $scope.show();
    };

    $scope.save = function () {
        if (!angular.isObject($scope.producto)) {
            return false;
        } else if ($scope.producto.nombreComercial.length <= 0 || $scope.producto.nombreComercial == "") {
            alert("Favor de ingresar Nombre de Producto");
        } else if ($scope.producto.proveedor.length <= 0 || $scope.producto.proveedor == "") {
            alert("Favor de ingresar Nombre del Proveedor");
        } else if ($scope.producto.tipo_producto.length <= 0 || $scope.producto.tipo_producto == "") {
            alert("Favor de ingresar Tipo de Producto");
        } else if ($scope.producto.ingrediente_activo.length <= 0 || $scope.producto.ingrediente_activo == "") {
            alert("Favor de ingresar Ingrediente Activo");
        } else if ($scope.producto.frac.length <= 0 || $scope.producto.frac == "") {
            alert("Favor de ingresar FRAC");
        } else if ($scope.producto.action.length <= 0 || $scope.producto.action == "") {
            alert("Favor de ingresar Acción");
        } else {
            if ($scope.producto.id_producto <= 0) {
                productos.create($scope.producto).then(function (data) {
                    return console.log($scope.proccess(data));
                }).catch(function (error) {
                    return console.log(error);
                });
            } else {
                productos.update($scope.producto).then(function (data) {
                    return console.log($scope.proccess(data));
                }).catch(function (error) {
                    return console.log(error);
                });
            }
        }
    };

    $scope.success = function (r) {
        alert("Registro registrado/modificado con el ID " + r.data, "Fincas", "success");
        $scope.producto.id_producto = r.data;
    };

    // 25/04/2017 - TAG: SECCION FOR TABLE 
    $scope.table = [];
    $scope.index = function () {
        productos.listado().then(function (data) {
            return $scope.table = $scope.proccess(data);
        }).catch(function (error) {
            return console.log(error);
        });
    };

    //parametros de busqueda
    $scope.search = {
        nombre: "",
        orderBy: "id",
        reverse: false,
        limit: 10,
        actual_page: 1 // las paginas comienzan apartir del 1
    };

    //ordenamiento por columnas
    $scope.changeSort = function (column) {
        if ($scope.search.orderBy != column) {
            var previous = $("th.selected")[0];
            $(previous).removeClass("selected");
            $(previous).removeClass("sorting_asc");
            $(previous).removeClass("sorting_desc");
        }
        $scope.search.reverse = $scope.search.orderBy != column ? false : !$scope.search.reverse;
        $scope.search.orderBy = column;
        var actual_select = $("#" + column + "_column");
        if (!actual_select.hasClass("selected")) {
            actual_select.addClass("selected");
        }
        if ($scope.search.reverse) {
            actual_select.addClass("sorting_desc");
            actual_select.removeClass("sorting_asc");
        } else {
            actual_select.addClass("sorting_asc");
            actual_select.removeClass("sorting_desc");
        }
    };

    //ir a la siguiente pagina
    $scope.next = function (dataSource) {
        if ($scope.search.actual_page < parseInt(dataSource.length / parseInt($scope.search.limit)) + (dataSource.length % parseInt($scope.search.limit) == 0 ? 0 : 1)) $scope.search.actual_page++;
    };

    //ir a la pagina anterior
    $scope.prev = function (dataSource) {
        if ($scope.search.actual_page > 1) $scope.search.actual_page--;
    };
}]);
