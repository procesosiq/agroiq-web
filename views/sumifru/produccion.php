<style>
	.margin-5 {
		margin-bottom: 5px !important;
	}
	table > th,td{
		text-align: center !important;
	}
	.center-th {
		text-align: center !important;
	}
	.left-td {
		text-align: left !important;
		padding-left: 10px !important;
	}
	table {
		cursor: pointer !important;
	}
	.anchoTable {
		width: 100px;
	}
    .delete  {
        background-color : #c8d8e8 !important;
    }
    .form-control, .input-sm, select, .btn, .portlet {
        border-radius : 3px !important;
    }
    .highlight {
		background-color: green;
    	color: white !important;
	}
	.highlight:hover {
		background-color: #285e8e !important;
    	color: white !important;
	}
</style>
<script src="assets/global/plugins/FileSaver.min.js"></script>
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.0.3/socket.io.js"></script>-->
<div ng-controller="produccion" ng-cloak>
	<h3 class="page-title"> 
          Producción. Racimos Diario
     </h3>
     <div class="page-bar" ng-init="getLastDay()">
         <ul class="page-breadcrumb">
             <li>
                 <i class="icon-home"></i>
                 <a>Producción</a>
                 <i class="fa fa-angle-right"></i>
             </li>
         </ul>
         <div class="page-toolbar">
            <button class="btn grey-mint" ng-disabled="cargandoHistorico" ng-click="importar()">Importar</button>
            <button class="btn green-jungle" ng-disabled="cargandoHistorico" ng-click="exportarFormatoEspecial()">Excel</button>
            <input id="datepicker" class="input-sm" sytle="height: 36px;" data-provide="datepicker" data-date-format="yyyy-mm-dd" readonly>
        </div>
    </div>

    <div class="tabbable tabbable-tabdrop">
        <ul class="nav nav-tabs">
            <li ng-repeat="(key, value) in fincas" class="{{ produccion.params.finca == key ? 'active' : '' }}">
                <a ng-click="produccion.params.finca = key; produccion.nocache(); getMuestreo()">{{value}}</a>
            </li>
        </ul>
    </div>

	<?php include('./views/sumifru/tags_produccion_dia.php') ?>

	<div class="row">
		<div class="col-md-5">
			<div class="portlet box green-haze">
				<div class="portlet-title">
					<span class="caption">RESUMEN POR EDAD</span>
				</div>
				<div class="portlet-body" >
					<div class="table-responsive" id="racimos_edad">
						<table class="table table-striped table-bordered table-hover">
							<thead>
								<tr>
                                    <th class="center-th">EDAD</th>
                                    <th class="center-th">CALIBRE<br>PROM</th>
                                    <th class="center-th">PROC</th>
                                    <th class="center-th">RECU</th>
                                    <th class="center-th">CORT</th>
                                    <th class="center-th">%</th>
								</tr>
							</thead>
							<tbody>
								<tr ng-repeat="color in tabla.edades | orderObjectBy : 'edad'" ng-show="color.total_cosechados > 0">
                                    <td class="{{ color.class }}">{{ (color.edad > 0) ? color.edad : 'S/C' }}</td>
                                    <td>{{ (color.calibre > 0) ? (color.calibre  | number : 2) : '' }}</td>
                                    <td>{{color.total_procesados}}</td>
                                    <td>{{color.total_recusados}}</td>
                                    <td>{{color.total_cosechados}}</td>
                                    <td>{{color.total_cosechados/totales.total_cosechados*100 | number : 2}}</td>
								</tr>
							</tbody>
							<tfoot>
								<tr>
                                    <td>{{totales.edad | number: 2}}</td>
                                    <td>{{totales.calibracion | number: 2}}</td>
                                    <td>{{totales.total_procesada}}</td>
                                    <td>{{totales.recusados}}</td>
                                    <td>{{totales.total_cosechados}}</td>
                                    <td></td>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-7">
            <div class="portlet box green-haze">
				<div class="portlet-title">
					<span class="caption">RESUMEN DE RECUSADOS</span>
				</div>
				<div class="portlet-body">
                    <div class="row">
                        <div class="table-responsive" id="racimos_recusados">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th class="text-center">CAUSA</th>
                                        <th class="text-center">CANTIDAD</th>
                                        <th class="text-center">PORCENTAJE</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="r in recusados">
                                        <td>{{ r.causa }}</td>
                                        <td>{{ r.cantidad }}</td>
                                        <td>{{ r.porcentaje | number: 2 }}</td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th class="text-center">RECUSADOS</th>
                                        <th class="text-center">{{ recusados | sumOfValue : 'cantidad' | number }}</th>
                                        <th class="text-center">100</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                        <div class="col-md-6 hide">
                            <div id="grafica-defectos" class="chart"></div>
                        </div>
                    </div>
                </div>
            </div>
		</div>
	</div>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet box green-haze">
                <div class="portlet-title">
                    <span class="caption">RESUMEN POR LOTE</span>
                </div>
                <div class="portlet-body" >
                    <div class="table-responsive" id="promedios_lotes">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th class="center-th"></th>
                                    <th class="center-th" colspan="{{edades.length}}" ng-if="edades.length > 0">EDAD</th>
                                    <th class="center-th"></th>
                                    <th class="center-th"></th>
                                    <th class="center-th"></th>
                                    <th class="center-th"></th>
                                    <th class="center-th"></th>
                                    <th class="center-th"></th>
                                    <th class="center-th"></th>
                                </tr>
                                <tr>
                                    <th class="center-th">LOTE</th>
                                    <th class="center-th {{ e.class }}" ng-repeat="e in edades | orderObjectBy:'edad'">{{ e.edad }}</th>
                                    <th class="center-th">COSE</th>
                                    <th class="center-th">PROC</th>
                                    <th class="center-th">RECU</th>
                                    <th class="center-th">PESO</th>
                                    <th class="center-th">CALIBRE</th>
                                    <th class="center-th">MANOS</th>
                                    <th class="center-th">DEDOS</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="row in promedios">
                                    <td>{{ row.lote }}</td>
                                    <td ng-repeat="e in edades | orderObjectBy:'edad'">{{ row['edad_'+e.edad] ? row['edad_'+e.edad] : '' }}</td>
                                    <td>{{ row.cosechados }}</td>
                                    <td>{{ row.procesados | number }}</td>
                                    <td>{{ row.recusados }}</td>
                                    <td>{{ row.peso }}</td>
                                    <td>{{ (row.calibre > 0) ? row.calibre : '' }}</td>
                                    <td>{{ (row.manos > 0) ? row.manos : '' }}</td>
                                    <td>{{ (row.dedos > 0) ? row.dedos : '' }}</td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>TOTAL</th>
                                    <th class="center-th" ng-repeat="e in edades | orderObjectBy:'edad'">{{ promedios | sumOfValue : 'edad_'+e.edad }}</th>
                                    <th class="center-th">{{ promedios | sumOfValue : 'cosechados' | number }}</th>
                                    <th class="center-th">{{ promedios | sumOfValue : 'procesados' | number }}</th>
                                    <th class="center-th">{{ promedios | sumOfValue : 'recusados' | number }}</th>
                                    <th class="center-th">{{ promedios | avgOfValue : 'peso' | number: 2 }}</th>
                                    <th class="center-th">{{ promedios | avgOfValue : 'calibre' | number: 2 }}</th>
                                    <th class="center-th">{{ promedios | avgOfValue : 'manos' | number: 2 }}</th>
                                    <th class="center-th">{{ promedios | avgOfValue : 'dedos' | number: 2 }}</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="portlet box green-haze">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-gift"></i> Racimos </div>
            <div class="tools">
            </div>
        </div>
        <div class="portlet-body">
            <div class="panel-group accordion" id="accordion3">

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_2" aria-expanded="false"> Racimos por Lote </a>
                        </h4>
                    </div>
                    <div id="collapse_3_2" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                        <div class="panel-body">
                            <div class="btn-group pull-right" style="margin-right: 5px;">
                                <button class="btn dark btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false"> 
                                    Exportar <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-right">
                                    <li>
                                        <a href="javascript:;" ng-click="exportPrint('table_racimos_lote')"> Imprimir </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;" ng-click="exportExcel('table_racimos_lote')">Excel</a>
                                    </li>
                                </ul>
                            </div>
                            <!-- begin racimos por lote -->
                            <div class="table-responsive table-scrollable" id="table_racimos_lote">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr role="row" class="heading">
                                            <th width="8.33%" class="center-th"> Lote  </th>
                                            <th width="8.33%" class="center-th"> Edad </th>
                                            <th width="8.33%" class="center-th"> Cuadrilla  </th>
                                            <th width="8.33%" class="center-th"> Cosechados  </th>
                                            <th width="8.33%" class="center-th"> Recusados </th>                                                    
                                            <th width="8.33%" class="center-th"> Procesados </th>                                                    
                                            <th width="8.33%" class="center-th"> Muestreados </th>                                                    
                                            <th width="8.33%" class="center-th"> % </th>
                                            <th width="8.33%" class="center-th"> KG </th>                                                  
                                            <th width="8.33%" class="center-th"> Manos </th>
                                            <th width="8.33%" class="center-th"> Calibre </th>
                                            <th width="8.33%" class="center-th"> Dedos </th>
                                        </tr>
                                    <thead>
                                    <tbody>
                                        <tr ng-repeat-start="detalle in tabla.produccion" ng-click="openDetallePalanca(detalle)">
                                            <td width="8.33%" class="center-th">{{detalle.lote}}</td>
                                            <td width="8.33%">{{ (detalle.edad > 0) ? detalle.edad : '' }}</td>
                                            <td width="8.33%"></td>
                                            <td width="8.33%">{{ detalle.total_cosechados }}</td>
                                            <td width="8.33%">{{ (detalle.total_recusados > 0) ? detalle.total_recusados : '' }}</td>
                                            <td width="8.33%">{{ (detalle.total_procesados > 0) ? detalle.total_procesados : '' }}</td>
                                            <td width="8.33%">{{ (detalle.muestreados > 0) ? detalle.muestreados : '' }}</td>
                                            <td width="8.33%">{{ (detalle.muestreados > 0) ? (detalle.muestreados / detalle.total_procesados * 100 | number : 2) : '' }}</td>
                                            <td width="8.33%">{{ detalle.peso  | number : 2}}</td>
                                            <td width="8.33%">{{ (detalle.manos > 0) ? (detalle.manos  | number : 2) : '' }}</td>
                                            <td width="8.33%">{{ (detalle.calibracion > 0) ? (detalle.calibracion  | number : 2) : '' }}</td>
                                            <td width="8.33%">{{ (detalle.dedos > 0) ? (detalle.dedos  | number : 2) : '' }}</td>
                                        </tr>
                                        <tr ng-show="detalle.expanded" ng-repeat-start="details in detalle.cables | orderObjectBy : 'edad'" ng-click="openDetalleLote(details)">
                                            <td width="8.33%"></td>
                                            <td width="8.33%" class="{{ details.class }}">{{ (details.cable == 'S/C') ? 'S/C' : (details.edad | number) }}</td>
                                            <td width="8.33%"></td>
                                            <td width="8.33%">{{ details.total_cosechados }}</td>
                                            <td width="8.33%">{{ (details.total_recusados > 0) ? details.total_recusados : '' }}</td>
                                            <td width="8.33%">{{ (details.total_procesados > 0) ? details.total_procesados : '' }}</td>
                                            <td width="8.33%">{{ (details.muestreados > 0) ? details.muestreados : '' }}</td>
                                            <td width="8.33%">{{ (details.muestreados > 0) ? (details.muestreados / details.total_procesados * 100 | number : 2) : ''}}</td>
                                            <td width="8.33%">{{ details.peso  | number : 2}}</td>
                                            <td width="8.33%">{{ (details.manos > 0) ? (details.manos  | number : 2) : '' }}</td>
                                            <td width="8.33%">{{ (details.calibracion > 0) ? (details.calibracion  | number : 2) : '' }}</td>
                                            <td width="8.33%">{{ (details.dedos > 0) ? (details.dedos  | number : 2) : '' }}</td>
                                        </tr>
                                        <tr ng-show="details.expanded" ng-repeat="palanca in details.cuadrillas">
                                            <td width="8.33%"></td>
                                            <td width="8.33%"></td>
                                            <td width="8.33%">{{ palanca.cuadrilla}}</td>
                                            <td width="8.33%">{{ palanca.total_cosechados}}</td>
                                            <td width="8.33%">{{ (palanca.total_recusados > 0) ? palanca.total_recusados : '' }}</td>
                                            <td width="8.33%">{{ (palanca.procesados > 0) ? palanca.procesados : '' }}</td>
                                            <td width="8.33%">{{ (palanca.muestreados > 0) ? palanca.muestreados : '' }}</td>
                                            <td width="8.33%">{{ (palanca.muestreados > 0) ? ((palanca.muestreados / palanca.total_cosechados) * 100 | number : 2) : '' }}</td>
                                            <td width="8.33%">{{ palanca.peso || 0  | number : 2}}</td>
                                            <td width="8.33%">{{ (palanca.manos > 0) ? (palanca.manos || 0  | number : 2) : '' }}</td>
                                            <td width="8.33%">{{ (palanca.calibracion > 0) ? (palanca.calibracion || 0  | number : 2) : '' }}</td>
                                            <td width="8.33%">{{ (palanca.dedos > 0) ? (palanca.dedos || 0  | number : 2) : '' }}</td>
                                        </tr>
                                        <tr ng-repeat-end="" ng-hide="true"></tr>
                                        <tr ng-repeat-end="" ng-hide="true"></tr>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td>Total:</td>
                                            <td>{{ totales.edad }}</td>
                                            <td></td>
                                            <td>{{totales.total_cosechados}} </td>
                                            <td>{{totales.recusados}}</td>
                                            <td>{{totales.total_procesada}}</td>
                                            <td>{{totales.muestreados}}</td>
                                            <td>{{totales.porcen | number : 2}}</td>
                                            <td>{{totales.peso  | number : 2}}</td>
                                            <td>{{totales.manos  | number : 2}}</td>
                                            <td>{{totales.calibracion  | number : 2}}</td>
                                            <td>{{totales.dedos  | number : 2}}</td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <!-- end racimos por lote -->
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_3" aria-expanded="false"> Racimos por Edad </a>
                        </h4>
                    </div>
                    <div id="collapse_3_3" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                        <div class="panel-body">
                            <div class="btn-group pull-right" style="margin-right: 5px;">
                                <button class="btn dark btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false"> 
                                    Exportar <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-right">
                                    <li>
                                        <a href="javascript:;" ng-click="exportPrint('table_racimos_edad')"> Imprimir </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;" ng-click="exportExcel('table_racimos_edad')">Excel</a>
                                    </li>
                                </ul>
                            </div>
                            <!-- begin racimos por edad -->
                            <div class="table-responsive table-scrollable" id="table_racimos_edad">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                            <tr role="row" class="heading">
                                                <th width="8.33%" class="center-th"> Edad  </th>
                                                <th width="8.33%" class="center-th"> Lote  </th>
                                                <th width="8.33%" class="center-th"> Cuadrilla </th>
                                                <th width="8.33%" class="center-th"> Cosechados  </th>
                                                <th width="8.33%" class="center-th"> Recusados </th>    
                                                <th width="8.33%" class="center-th"> Procesados </th>
                                                <th width="8.33%" class="center-th"> Muestreados </th>
                                                <th width="8.33%" class="center-th"> % </th>
                                                <th width="8.33%" class="center-th"> KG </th>
                                                <th width="8.33%" class="center-th"> Manos </th>
                                                <th width="8.33%" class="center-th"> Calibre </th>
                                                <th width="8.33%" class="center-th"> Dedos </th>
                                            </tr>
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat-start="color in tabla.edades | orderObjectBy : 'edad'" ng-click="color.expand = !color.expand">
                                            <td id="fila1" class="{{ color.class }}">{{ color.edad > 0 ? (color.edad | number) : 'S/C' }}</td>
                                            <td></td>
                                            <td></td>
                                            <td>{{ color.total_cosechados }}</td>
                                            <td>{{ (color.total_recusados > 0) ? color.total_recusados : '' }}</td>
                                            <td>{{ (color.total_procesados > 0) ? color.total_procesados : '' }}</td>
                                            <td>{{ (color.muestreados > 0) ? color.muestreados : '' }}</td>
                                            <td>{{ (color.muestreados > 0) ? ((color.muestreados / color.cosechados) * 100 | number : 2) : '' }}</td>
                                            <td>{{color.peso  | number : 2}}</td>
                                            <td>{{ (color.manos > 0) ? (color.manos  | number : 2) : '' }}</td>
                                            <td>{{ (color.calibracion > 0) ? (color.calibracion  | number : 2) : '' }}</td>
                                            <td>{{ (color.dedos > 0) ? (color.dedos  | number : 2) : '' }}</td>
                                        </tr>
                                        <tr ng-show="color.expand" ng-repeat-start="lote in color.lotes" ng-click="openDetalleLote(lote)">
                                            <td width="8.33%"></td>
                                            <td width="8.33%">{{lote.lote}}</td>
                                            <td width="8.33%"></td>
                                            <td width="8.33%">{{lote.total_cosechados}}</td>
                                            <td width="8.33%">{{ (lote.total_recusados > 0) ? lote.total_recusados : '' }}</td>
                                            <td width="8.33%">{{ (lote.total_procesados > 0) ? lote.total_procesados : '' }}</td>
                                            <td width="8.33%">{{ (lote.muestreados > 0) ? lote.muestreados : '' }}</td>
                                            <td width="8.33%">{{ (lote.muestreados > 0) ? ((lote.muestreados / lote.cosechados) * 100 | number : 2) : '' }}</td>
                                            <td width="8.33%">{{ lote.peso  | number : 2}}</td>
                                            <td width="8.33%">{{ (lote.manos > 0) ? (lote.manos  | number : 2) : '' }}</td>
                                            <td width="8.33%">{{ (lote.calibracion > 0) ? (lote.calibracion  | number : 2) : '' }}</td>
                                            <td width="8.33%">{{ (lote.dedos > 0) ? (lote.dedos  | number : 2) : '' }}</td>
                                        </tr>
                                        <tr ng-show="lote.expanded" ng-repeat="cable in lote.cuadrillas">
                                            <td width="8.33%"></td>
                                            <td width="8.33%"></td>
                                            <td width="8.33%">{{cable.cuadrilla}}</td>
                                            <td width="8.33%">{{cable.total_cosechados}}</td>
                                            <td width="8.33%">{{ (cable.total_recusados > 0) ? cable.total_recusados : '' }}</td>
                                            <td width="8.33%">{{ (cable.total_procesados > 0) ? cable.total_procesados : '' }}</td>
                                            <td width="8.33%">{{ (cable.muestreados > 0) ? cable.muestreados : '' }}</td>
                                            <td width="8.33%">{{ (cable.muestreados > 0) ? ((cable.muestreados / cable.cosechados) * 100 | number : 2) : '' }}</td>
                                            <td width="8.33%">{{ (cable.peso > 0) ? (cable.peso  | number : 2) : '' }}</td>
                                            <td width="8.33%">{{ (cable.manos > 0) ? (cable.manos  | number : 2) : '' }}</td>
                                            <td width="8.33%">{{ (cable.calibracion > 0) ? (cable.calibracion  | number : 2) : '' }}</td>
                                            <td width="8.33%">{{ (cable.dedos > 0) ? (cable.dedos  | number : 2) : '' }}</td>
                                        </tr>
                                        <tr ng-repeat-end ng-hide="true"></tr>
                                        <tr ng-repeat-end ng-hide="true"></tr>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td>{{ totales.edad }}</td>
                                            <td></td>
                                            <td></td>
                                            <td>{{totales.total_cosechados}} </td>
                                            <td>{{totales.recusados}}</td>
                                            <td>{{totales.total_procesada}}</td>
                                            <td>{{totales.muestreados}}</td>
                                            <td>{{totales.porcen | number : 2}}</td>
                                            <td>{{totales.peso  | number : 2}}</td>
                                            <td>{{totales.manos  | number : 2}}</td>
                                            <td>{{totales.calibracion  | number : 2}}</td>
                                            <td>{{totales.dedos  | number : 2}}</td>
                                        </tr>	
                                    </tfoot>
                                </table>
                            </div>
                            <!-- end racimos por edad -->
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_4" aria-expanded="false"> Base de Datos Racimos </a>
                        </h4>
                    </div>
                    <div id="collapse_3_4" class="panel-collapse collapse" aria-expanded="true" style="">
                        <div class="panel-body">
                            <div class="btn-group pull-right" style="margin-right: 5px;">
                                <button class="btn dark btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false"> 
                                    Exportar <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-right">
                                    <li>
                                        <a href="javascript:;" ng-click="exportPrint('registros')"> Imprimir </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;" ng-click="exportExcel('registros')">Excel</a>
                                    </li>
                                </ul>
                            </div>
                            <button class="btn red-thunderbird pull-right" style="height: 30px;" ng-click="eliminar()">Eliminar</button>
                            <button class="btn green-jungle pull-right" ng-show="editing" style="height: 30px;" ng-click="guardar()">Guardar</button>
                            <br>
                            <div class="table-responsive table-scrollable">
                                <table class="table table-striped table-bordered table-hover" id="edit_registros">
                                    <thead>
                                        <tr role="row" class="heading">
                                            <th class="center-th anchoTable" ng-click="setOrderTable('id')"> ID </th>
                                            <th class="center-th anchoTable" style="min-width: 120px;" style="min-width: 70px;" ng-click="setOrderTable('fecha')"> Fecha </th>
                                            <th class="center-th anchoTable" ng-click="setOrderTable('hora')"> Hora </th>
                                            <th class="center-th anchoTable" ng-click="setOrderTable('cuadrilla')"> Cuadrilla  </th>
                                            <th class="center-th anchoTable" ng-click="setOrderTable('lote')"> Lote </th>
                                            <th class="center-th anchoTable" ng-click="setOrderTable('edad')"> Edad </th>
                                            <th class="center-th anchoTable" ng-click="setOrderTable('peso')"> KG </th>
                                            <th class="center-th anchoTable" ng-click="setOrderTable('manos')"> Manos </th>
                                            <th class="center-th anchoTable" ng-click="setOrderTable('calibre')"> Calibre </th>
                                            <th class="center-th anchoTable" ng-click="setOrderTable('dedos')"> Dedos </th>
                                            <th class="center-th anchoTable" style="min-width: 100px;" ng-click="setOrderTable('tipo')"> Tipo </th>
                                            <th class="center-th anchoTable" ng-click="setOrderTable('causa')"> Causa </th>
                                        </tr>
                                        <tr>
                                            <th><input type="text" class="form-control input-filter" ng-model="searchFilter.id" /></th>
                                            <th><input type="text" class="form-control input-filter" ng-model="searchFilter.fecha" /></th>
                                            <th><input type="text" class="form-control input-filter" ng-model="searchFilter.hora" /></th>
                                            <th><input type="text" class="form-control input-filter" ng-model="searchFilter.cuadrilla" /></th>
                                            <th><input type="text" class="form-control input-filter" ng-model="searchFilter.lote" /></th>
                                            <th><input type="text" class="form-control input-filter" ng-model="searchFilter.edad" /></th>
                                            <th><input type="text" class="form-control input-filter" ng-model="searchFilter.peso" /></th>
                                            <th><input type="text" class="form-control input-filter" ng-model="searchFilter.manos" /></th>
                                            <th><input type="text" class="form-control input-filter" ng-model="searchFilter.calibre" /></th>
                                            <th><input type="text" class="form-control input-filter" ng-model="searchFilter.dedos" /></th>
                                            <th><input type="text" class="form-control input-filter" ng-model="searchFilter.tipo" /></th>
                                            <th><input type="text" class="form-control input-filter" ng-model="searchFilter.causa" /></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr class="{{ row.delete ? 'delete' : '' }}" 
                                            data-id="{{row.id}}" 
                                            data-marca="{{row.marca}}" 
                                            data-index="{{$index}}"
                                            ng-dblclick="edit(row)"
                                            ng-repeat="row in resultValue = (registros | filter: searchFilter | orderObjectBy : searchTable.orderBy : searchTable.reverse | startFrom: searchTable.startFrom | limitTo : searchTable.limit)">
                                            <td class="anchoTable">
                                                <input type="checkbox" ng-model="row.delete" />
                                                {{ row.id }}
                                            </td>
                                            <td class="anchoTable">{{ row.fecha }}</td>
                                            <td class="anchoTable">{{ row.hora }}</td>
                                            <td class="anchoTable">
                                                <span ng-show="!row.editing">{{row.cuadrilla}}</span>
                                                <input type="text" class="form-control input-sm" ng-if="row.editing" ng-model="row.cuadrilla"/>
                                            </td>
                                            <td class="anchoTable">
                                                <span ng-show="!row.editing">{{ row.lote }}</span>
                                                <input type="text" class="form-control input-sm" ng-if="row.editing" ng-model="row.lote"/>
                                            </td>
                                            <td class="anchoTable {{ row.class }}">
                                                <span ng-show="!row.editing">{{ row.edad }}</span>
                                                <input type="text" class="form-control input-sm" ng-if="row.editing" ng-model="row.edad"/>
                                            </td>
                                            <td class="anchoTable">
                                                <span ng-show="!row.editing">{{ (row.peso > 0) ? (row.peso  | number : 2) : '' }}</span>
                                                <input type="text" class="form-control input-sm" ng-if="row.editing" ng-model="row.peso"/>
                                            </td>
                                            <td class="anchoTable">
                                                <span ng-show="!row.editing">{{ (row.manos > 0 && row.tipo != 'RECUSADO') ? (row.manos  | number : 2) : '' }}</span>
                                                <input type="text" class="form-control input-sm" ng-if="row.editing && row.tipo != 'RECUSADO'" ng-model="row.manos"/>
                                            </td>
                                            <td class="anchoTable">
                                                <span ng-show="!row.editing">{{ (row.calibre > 0 && row.tipo != 'RECUSADO') ? (row.calibre  | number : 2) : '' }}</span>
                                                <input type="text" class="form-control input-sm" ng-if="row.editing && row.tipo != 'RECUSADO'" ng-model="row.calibre"/>
                                            </td>
                                            <td class="anchoTable">
                                                <span ng-show="!row.editing">{{ (row.dedos > 0 && row.tipo != 'RECUSADO') ? (row.dedos  | number : 2) : '' }}</span>
                                                <input type="text" class="form-control input-sm" ng-if="row.editing && row.tipo != 'RECUSADO'" ng-model="row.dedos"/>
                                            </td>
                                            <td class="anchoTable">
                                                <span ng-show="!row.editing">{{ row.tipo == 'RECUSADO' ? 'RECU' : row.tipo }}</span>
                                                <select class="form-control" ng-model="row.tipo" ng-if="row.editing">
                                                    <option value="PROC">PROC</option>
                                                    <option value="RECUSADO">RECU</option>
                                                </select>
                                            </td>
                                            <td class="anchoTable">
                                                <span ng-show="!row.editing && row.tipo == 'RECUSADO'">{{ row.causa }}</span>
                                                <input type="text" class="form-control input-sm" ng-if="row.editing && row.tipo == 'RECUSADO'" ng-model="row.causa"/>
                                            </td>
                                        </tr>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td>Total:</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>{{ (resultValue | avgOfValue:'edad') | number : 2 }}</td>
                                            <td>{{ (resultValue | sumOfValue:'peso') | number : 2 }}</td>
                                            <td>{{ (resultValue | sumOfValue:'manos') | number : 2 }}</td>
                                            <td>{{ (resultValue | sumOfValue:'calibre') | number : 2 }}</td>
                                            <td>{{ (resultValue | sumOfValue:'dedos') | number : 2 }}</td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td colspan="12">
                                                <div class="col-md-5 col-sm-12">
                                                    Página {{ (searchTable.actual_page | num) || 0 }} de {{ searchTable.numPages }}
                                                </div>
                                                <div class="col-md-7 col-sm-12">
                                                    <div class="pull-left">
                                                        <a class="btn btn-sm default prev" ng-click="prev(registros)"><</a>
                                                        <input type="text" maxlength="5" class="pagination-panel-input form-control input-sm input-inline input-mini" ng-model="searchTable.actual_page" readonly/>
                                                        <a class="btn btn-sm default next" ng-click="next(registros)">></a>

                                                        <select class="input-sm pull-right" style="color: #555;" ng-model="searchTable.limit" ng-change="searchTable.changePagination()">
                                                            <option value="{{ registros.length }}">TODOS</option>
                                                            <option ng-repeat="opt in searchTable.optionsPagination" value="{{ opt > 0 ? opt : '' }}" ng-selected="opt == searchTable.limit">{{ opt > 0 ? opt : 'TODOS' }}</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>

                                <table class="table table-striped table-bordered table-hover" style="display: none;" id="registros">
                                    <thead>
                                        <tr role="row" class="heading">
                                            <th class="center-th anchoTable"> ID </th>
                                            <th class="center-th anchoTable" style="min-width: 120px;" style="min-width: 70px;"> Fecha </th>
                                            <th class="center-th anchoTable"> Hora </th>
                                            <th class="center-th anchoTable"> Cuadrilla  </th>
                                            <th class="center-th anchoTable"> Lote </th>
                                            <th class="center-th anchoTable"> Edad </th>
                                            <th class="center-th anchoTable"> Cinta </th>
                                            <th class="center-th anchoTable"> KG </th>
                                            <th class="center-th anchoTable"> Manos </th>
                                            <th class="center-th anchoTable"> Calibre </th>
                                            <th class="center-th anchoTable"> Dedos </th>
                                            <th class="center-th anchoTable" > Tipo </th>
                                            <th class="center-th anchoTable"> Causa </th>
                                        </tr>
                                    <tbody>
                                        <tr ng-repeat="row in resultValue = (registros | orderObjectBy : searchTable.orderBy : searchTable.reverse | startFrom: searchTable.startFrom | limitTo : searchTable.limit)">
                                            <td class="anchoTable">{{ row.id }}</td>
                                            <td class="anchoTable">{{ row.fecha }}</td>
                                            <td class="anchoTable">{{ row.hora }}</td>
                                            <td class="anchoTable">{{row.cuadrilla}}</td>
                                            <td class="anchoTable">{{ row.lote }}</td>
                                            <td class="anchoTable">{{ row.edad }}</td>
                                            <td class="anchoTable">{{ row.cinta }}</td>
                                            <td class="anchoTable">{{ (row.peso > 0) ? (row.peso  | number : 2) : '' }}</td>
                                            <td class="anchoTable">{{ (row.manos > 0 && row.tipo != 'RECUSADO') ? (row.manos  | number : 2) : '' }}</td>
                                            <td class="anchoTable">{{ (row.calibre > 0 && row.tipo != 'RECUSADO') ? (row.calibre  | number : 2) : '' }}</td>
                                            <td class="anchoTable">{{ (row.dedos > 0 && row.tipo != 'RECUSADO') ? (row.dedos  | number : 2) : '' }}</td>
                                            <td class="anchoTable">{{ row.tipo == 'RECUSADO' ? 'RECU' : row.tipo  }}</td>
                                            <td class="anchoTable">{{ row.causa }}</td>
                                        </tr>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td>Total:</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>{{ (resultValue | avgOfValue:'edad') | number : 2 }}</td>
                                            <td></td>
                                            <td>{{ (resultValue | sumOfValue:'peso') | number : 2 }}</td>
                                            <td>{{ (resultValue | sumOfValue:'manos') | number : 2 }}</td>
                                            <td>{{ (resultValue | sumOfValue:'calibre') | number : 2 }}</td>
                                            <td>{{ (resultValue | sumOfValue:'dedos') | number : 2 }}</td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_7" aria-expanded="false"> Análisis de Defectos. Resumen del Año </a>
                        </h4>
                    </div>
                    <div id="collapse_3_7" class="panel-collapse collapse" aria-expanded="true" style="">
                        <div class="panel-body">
                            <div class="col-md-3 pull-right">
                                <select class="form-control" ng-model="produccion.params.var_recusado" ng-init="produccion.params.var_recusado = 'cant';" ng-change="getAnalisisRecusados()">
                                    <option value="cant">CANTIDAD</option>
                                    <option value="porc">% </option>
                                </select>
                            </div>
                            <div id="table-defectos"></div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a ng-click="openMuestreo()" class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_6" aria-expanded="false"> Muestreo </a>
                        </h4>
                    </div>
                    <div id="collapse_3_6" class="panel-collapse collapse" aria-expanded="true" style="">
                        <div class="panel-body">
                            <div id="grafica-muestreo" class="chart"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="importar" role="basic" aria-hidden="true">
		<div class="modal-dialog" style="background: white;">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">IMPORTAR EXCEL DE INFORMACIÓN</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-4">
							<label class="control-label">Excel (.xlsx)</label>
						</div>
						<div class="col-md-8">
                            <input id="input-file" file="file" type="file" accept=".xlsx">
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn default" data-dismiss="modal">Cancelar</button>
				<button type="button" class="btn blue" ng-click="guardarExcel()">Guardar</button>
			</div>
		</div>
	</div>
</div>

<script src="componentes/FilterableSortableTable.js"></script>