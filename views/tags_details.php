
                    <!-- END PAGE HEADER-->
                    <div class="row">
                        <div class="{{id_company != 7 ? 'col-lg-3 col-md-3 col-sm-6 col-xs-12' : 'col-md-4 col-xs-12'}}">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.auditoria.valor) }}">
                                            <span class="counter_tags" data-value="{{tags.auditoria.valor}}"></span>
                                            <small class="font-{{ revision(tags.auditoria.valor) }}">%</small>
                                        </h3>
                                        <small>ULTIMA AUDITORIA</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-settings"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width:{{tags.auditoria.valor}}%;" class="progress-bar progress-bar-success {{ revision(tags.auditoria.valor) }}">
                                            <span class="sr-only">{{tags.auditoria.valor}}%</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"> PROMEDIO ACUMULADO </div>
                                        <div class="status-number">{{tags.auditoria.valor}}% </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="{{id_company != 7 ? 'col-lg-3 col-md-3 col-sm-6 col-xs-12' : 'col-md-4 col-xs-12'}}">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.labor.valor) }}">
                                            <span class="counter_tags" data-value="{{tags.labor.valor}}">0</span>
                                            <small class="font-{{ revision(tags.labor.valor) }}">%</small>
                                        </h3>
                                        <small>LABOR MÁS BAJA: {{tags.labor.label}}</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-pie-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width: {{tags.labor.valor}}%;" class="progress-bar progress-bar-success {{ revision(tags.labor.valor) }}">
                                            <span class="sr-only">{{tags.labor.valor}}% progress</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"> PROMEDIO ACUMULADO </div>
                                        <div class="status-number"> {{tags.labor.valor}}% </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="{{id_company != 7 ? 'col-lg-3 col-md-3 col-sm-6 col-xs-12' : 'col-md-4 col-xs-12'}}">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.lote.valor) }}">
                                            <span class="counter_tags" data-value=" {{tags.lote.valor}}">0</span>
                                            <small class="font-{{ revision(tags.lote.valor) }}">%</small>
                                        </h3>
                                        <small>LOTE MÁS BAJO:  {{tags.lote.label}}</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-bar-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width:  {{tags.lote.valor}}%;" class="progress-bar progress-bar-success {{ revision(tags.lote.valor) }}">
                                            <span class="sr-only"> {{tags.lote.valor}}% change</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"> PROMEDIO ACUMULADO </div>
                                        <div class="status-number">  {{tags.lote.valor}}% </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div ng-show="id_company != 7" class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.operador.valor) }}">
                                            <span class="counter_tags" data-value=" {{tags.operador.valor}}">0</span>
                                            <small class="font-{{ revision(tags.operador.valor) }}">%</small>
                                        </h3>
                                        <small>PERSONAL MÁS BAJO: {{tags.operador.label}}</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-user"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width: {{tags.operador.valor}}%;" class="progress-bar progress-bar-success  {{ revision(tags.operador.valor) }}">
                                            <span class="sr-only">0% grow</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"> PROMEDIO ACUMULADO </div>
                                        <div class="status-number"> {{tags.operador.valor}}% </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>