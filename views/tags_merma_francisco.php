
                    <!-- END PAGE HEADER-->
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.merma.value) }}">
                                            <span class="counter_tags" data-value="{{tags.merma.value}}">0</span>
                                            <small class="font-{{ revision(tags.merma.value) }}">%</small>
                                        </h3>
                                        <small>% MERMA NETA</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-pie-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width: {{tags.merma.value}}%;" class="progress-bar progress-bar-success {{ revision(tags.merma.value) }}">
                                            <span class="sr-only">{{tags.merma.value}}% progress</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number"> {{tags.merma.value}}% </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.merma_procesada.value) }}">
                                            <span class="counter_tags" data-value="{{tags.merma_procesada.value}}">0</span>
                                            <small class="font-{{ revision(tags.merma_procesada.value) }}">%</small>
                                        </h3>
                                        <small>% MERMA PROCESADA</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-pie-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width: {{tags.merma_procesada.value}}%;" class="progress-bar progress-bar-success {{ revision(tags.merma_procesada.value) }}">
                                            <span class="sr-only">{{tags.merma_procesada.value}}% progress</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number"> {{tags.merma_procesada.value}}% </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.merma_cortada.value) }}">
                                            <span class="counter_tags" data-value="{{tags.merma_cortada.value}}">0</span>
                                            <small class="font-{{ revision(tags.merma_cortada.value) }}">%</small>
                                        </h3>
                                        <small>% MERMA CORTADA</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-pie-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width: {{tags.merma_cortada.value}}%;" class="progress-bar progress-bar-success {{ revision(tags.merma_cortada.value) }}">
                                            <span class="sr-only">{{tags.merma_cortada.value}}% progress</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number"> {{tags.merma_cortada.value}}% </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.tallo.value) }}">
                                            <span class="counter_tags" data-value="{{tags.tallo.value}}">0</span>
                                            <small class="font-{{ revision(tags.tallo.value) }}">%</small>
                                        </h3>
                                        <small>% TALLO</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-pie-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width: {{tags.tallo.value}}%;" class="progress-bar progress-bar-success {{ revision(tags.tallo.value) }}">
                                            <span class="sr-only">{{tags.tallo.value}}% progress</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number"> {{tags.tallo.value}}% </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.cosecha.value) }}">
                                            <span class="counter_tags" data-value="{{tags.cosecha.value}}">0</span>
                                            <small class="font-{{ revision(tags.cosecha.value) }}">%</small>
                                        </h3>
                                        <small>COSECHA</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-bar-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width:{{tags.cosecha.value}}%;" class="progress-bar progress-bar-success {{ revision(tags.cosecha.value) }}">
                                            <span class="sr-only">{{tags.cosecha.value}}%</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.aereo.value) }}">
                                            <span class="counter_tags" data-value=" {{tags.aereo.value}}">0</span>
                                            <small class="font-{{ revision(tags.aereo.value) }}">%</small>
                                        </h3>
                                        <small>LOTERO AEREO</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-bar-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width: {{tags.aereo.value}}%;" class="progress-bar progress-bar-success  {{ revision(tags.aereo.value) }}">
                                            <span class="sr-only">0% grow</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number"> {{tags.aereo.value}}% </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.terrestre.value) }}">
                                            <span class="counter_tags" data-value=" {{tags.terrestre.value}}">0</span>
                                            <small class="font-{{ revision(tags.terrestre.value) }}">%</small>
                                        </h3>
                                        <small>LOTERO TERRESTRE</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-bar-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width: {{tags.terrestre.value}}%;" class="progress-bar progress-bar-success  {{ revision(tags.terrestre.value) }}">
                                            <span class="sr-only">0% grow</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number"> {{tags.terrestre.value}}% </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.fisiologicos.value) }}">
                                            <span class="counter_tags" data-value=" {{tags.fisiologicos.value}}">0</span>
                                            <small class="font-{{ revision(tags.fisiologicos.value) }}">%</small>
                                        </h3>
                                        <small>FISIOLOGICOS</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-bar-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width:  {{tags.fisiologicos.value}}%;" class="progress-bar progress-bar-success {{ revision(tags.fisiologicos.value) }}">
                                            <span class="sr-only"> {{tags.fisiologicos.value}}% progress</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.empacadora.value) }}">
                                            <span class="counter_tags" data-value="{{tags.empacadora.value}}">0</span>
                                            <small class="font-{{ revision(tags.empacadora.value) }}">%</small>
                                        </h3>
                                        <small>EMPACADORA</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-bar-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width: {{tags.empacadora.value}}%;" class="progress-bar progress-bar-success {{ revision(tags.empacadora.value) }}">
                                            <span class="sr-only">{{tags.empacadora.value}}% progress</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.insectos.value) }}">
                                            <span class="counter_tags" data-value=" {{tags.insectos.value}}">0</span>
                                            <small class="font-{{ revision(tags.insectos.value) }}">%</small>
                                        </h3>
                                        <small>INSECTOS</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-bar-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width:  {{tags.insectos.value}}%;" class="progress-bar progress-bar-success {{ revision(tags.insectos.value) }}">
                                            <span class="sr-only"> {{tags.insectos.value}}% change</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number">  {{tags.insectos.value}}% </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.hongos.value) }}">
                                            <span class="counter_tags" data-value=" {{tags.hongos.value}}">0</span>
                                            <small class="font-{{ revision(tags.hongos.value) }}">%</small>
                                        </h3>
                                        <small>HONGOS</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-bar-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width:  {{tags.hongos.value}}%;" class="progress-bar progress-bar-success {{ revision(tags.hongos.value) }}">
                                            <span class="sr-only"> {{tags.hongos.value}}% change</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.animal.value) }}">
                                            <span class="counter_tags" data-value=" {{tags.animal.value}}">0</span>
                                            <small class="font-{{ revision(tags.animal.value) }}">%</small>
                                        </h3>
                                        <small>ANIMALES</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-bar-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width:  {{tags.animal.value}}%;" class="progress-bar progress-bar-success {{ revision(tags.animal.value) }}">
                                            <span class="sr-only"> {{tags.animal.value}}% change</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.cajas.value) }}">
                                            <span class="counter_tags" data-value=" {{tags.cajas.value}}">0</span>
                                            <small class="font-{{ revision(tags.cajas.value) }}"></small>
                                        </h3>
                                        <small>CAJAS ACUMULADAS</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-bar-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width:  {{tags.cajas.value}}%;" class="progress-bar progress-bar-success {{ revision(tags.cajas.value) }}">
                                            <span class="sr-only"> {{tags.cajas.value}}</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.usd.value) }}">
                                            $ <span class="counter_tags" data-value=" {{tags.usd.value}}">0</span>
                                            <small class="font-{{ revision(tags.usd.value) }}"></small>
                                        </h3>
                                        <small>DOLARES ACUMULADOS</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-bar-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width:  {{tags.usd.value}}%;" class="progress-bar progress-bar-success {{ revision(tags.usd.value) }}">
                                            <span class="sr-only"> {{tags.usd.value}}</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>