
                    <!-- END PAGE HEADER-->
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.merma.value) }}">
                                            <span class="counter_tags" data-value="{{tags.merma.value}}">0</span>
                                            <small class="font-{{ revision(tags.merma.value) }}">%</small>
                                        </h3>
                                        <small>% MERMA NETA</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-pie-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width: {{tags.merma.value}}%;" class="progress-bar progress-bar-success {{ revision(tags.merma.value) }}">
                                            <span class="sr-only">{{tags.merma.value}}% progress</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number"> {{tags.merma.value}}% </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.merma_procesada.value) }}">
                                            <span class="counter_tags" data-value="{{tags.merma_procesada.value}}">0</span>
                                            <small class="font-{{ revision(tags.merma_procesada.value) }}">%</small>
                                        </h3>
                                        <small>% MERMA PROCESADA</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-pie-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width: {{tags.merma_procesada.value}}%;" class="progress-bar progress-bar-success {{ revision(tags.merma_procesada.value) }}">
                                            <span class="sr-only">{{tags.merma_procesada.value}}% progress</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number"> {{tags.merma_procesada.value}}% </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.merma_cortada.value) }}">
                                            <span class="counter_tags" data-value="{{tags.merma_cortada.value}}">0</span>
                                            <small class="font-{{ revision(tags.merma_cortada.value) }}">%</small>
                                        </h3>
                                        <small>% MERMA CORTADA</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-pie-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width: {{tags.merma_cortada.value}}%;" class="progress-bar progress-bar-success {{ revision(tags.merma_cortada.value) }}">
                                            <span class="sr-only">{{tags.merma_cortada.value}}% progress</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number"> {{tags.merma_cortada.value}}% </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.tallo.value) }}">
                                            <span class="counter_tags" data-value="{{tags.tallo.value}}">0</span>
                                            <small class="font-{{ revision(tags.tallo.value) }}">%</small>
                                        </h3>
                                        <small>% TALLO</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-pie-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width: {{tags.tallo.value}}%;" class="progress-bar progress-bar-success {{ revision(tags.tallo.value) }}">
                                            <span class="sr-only">{{tags.tallo.value}}% progress</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number"> {{tags.tallo.value}}% </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.cosecha.value) }}">
                                            <span class="counter_tags" data-value="{{tags.cosecha.value}}">0</span>
                                            <small class="font-{{ revision(tags.cosecha.value) }}">%</small>
                                        </h3>
                                        <small>COSECHA</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-bar-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width:{{tags.cosecha.value}}%;" class="progress-bar progress-bar-success {{ revision(tags.cosecha.value) }}">
                                            <span class="sr-only">{{tags.cosecha.value}}%</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.lotero.value) }}">
                                            <span class="counter_tags" data-value="{{tags.lotero.value}}">0</span>
                                            <small class="font-{{ revision(tags.lotero.value) }}">%</small>
                                        </h3>
                                        <small>LOTERO AEREO</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-bar-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width: {{tags.lotero.value}}%;" class="progress-bar progress-bar-success {{ revision(tags.lotero.value) }}">
                                            <span class="sr-only">{{tags.lotero.value}}% progress</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.deshoje.value) }}">
                                            <span class="counter_tags" data-value=" {{tags.deshoje.value}}">0</span>
                                            <small class="font-{{ revision(tags.deshoje.value) }}">%</small>
                                        </h3>
                                        <small>DESHOJE</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-bar-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width: {{tags.deshoje.value}}%;" class="progress-bar progress-bar-success  {{ revision(tags.deshoje.value) }}">
                                            <span class="sr-only">0% grow</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number"> {{tags.deshoje.value}}% </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.empaque.value) }}">
                                            <span class="counter_tags" data-value=" {{tags.empaque.value}}">0</span>
                                            <small class="font-{{ revision(tags.empaque.value) }}">%</small>
                                        </h3>
                                        <small>EMPAQUE</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-bar-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width:  {{tags.empaque.value}}%;" class="progress-bar progress-bar-success {{ revision(tags.empaque.value) }}">
                                            <span class="sr-only"> {{tags.empaque.value}}% change</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number">  {{tags.empaque.value}}% </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.administracion.value) }}">
                                            <span class="counter_tags" data-value=" {{tags.administracion.value}}">0</span>
                                            <small class="font-{{ revision(tags.administracion.value) }}">%</small>
                                        </h3>
                                        <small>ADMINISTRACION</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-bar-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width:  {{tags.administracion.value}}%;" class="progress-bar progress-bar-success {{ revision(tags.administracion.value) }}">
                                            <span class="sr-only"> {{tags.administracion.value}}% change</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.fisiologicos.value) }}">
                                            <span class="counter_tags" data-value=" {{tags.fisiologicos.value}}">0</span>
                                            <small class="font-{{ revision(tags.fisiologicos.value) }}">%</small>
                                        </h3>
                                        <small>FISIOLOGICOS</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-bar-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width:  {{tags.fisiologicos.value}}%;" class="progress-bar progress-bar-success {{ revision(tags.fisiologicos.value) }}">
                                            <span class="sr-only"> {{tags.fisiologicos.value}}% progress</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.cajas.value) }}">
                                            <span class="counter_tags" data-value=" {{tags.cajas.value}}">0</span>
                                            <small class="font-{{ revision(tags.cajas.value) }}"></small>
                                        </h3>
                                        <small>CAJAS ACUMULADAS</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-bar-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width:  {{tags.cajas.value}}%;" class="progress-bar progress-bar-success {{ revision(tags.cajas.value) }}">
                                            <span class="sr-only"> {{tags.cajas.value}}</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.usd.value) }}">
                                            $ <span class="counter_tags" data-value=" {{tags.usd.value}}">0</span>
                                            <small class="font-{{ revision(tags.usd.value) }}"></small>
                                        </h3>
                                        <small>DOLARES ACUMULADOS</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-bar-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width:  {{tags.usd.value}}%;" class="progress-bar progress-bar-success {{ revision(tags.usd.value) }}">
                                            <span class="sr-only"> {{tags.usd.value}}</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>