
                    <!-- END PAGE HEADER-->
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 hide">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.merma.value) }}">
                                            <span class="counter_tags" data-value="{{tags.merma.value}}">0</span>
                                            <small class="font-{{ revision(tags.merma.value) }}">%</small>
                                        </h3>
                                        <small>% MERMA {{calidad.params.idMerma}}</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-pie-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width: {{tags.merma.value}}%;" class="progress-bar progress-bar-success {{ revision(tags.merma.value) }}">
                                            <span class="sr-only">{{tags.merma.value}}% progress</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number"> {{tags.merma.value}}% </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.merma_neta.value) }}">
                                            <span class="counter_tags" data-value="{{tags.merma_neta.value}}">0</span>
                                            <small class="font-{{ revision(tags.merma_neta.value) }}">%</small>
                                        </h3>
                                        <small>% MERMA NETA</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-pie-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width: {{tags.merma_neta.value}}%;" class="progress-bar progress-bar-success {{ revision(tags.merma_neta.value) }}">
                                            <span class="sr-only">{{tags.merma_neta.value}}% progress</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number"> {{tags.merma_neta.value}}% </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.tallo.value) }}">
                                            <span class="counter_tags" data-value="{{tags.tallo.value}}">0</span>
                                            <small class="font-{{ revision(tags.tallo.value) }}">%</small>
                                        </h3>
                                        <small>% TALLO</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-pie-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width: {{tags.tallo.value}}%;" class="progress-bar progress-bar-success {{ revision(tags.tallo.value) }}">
                                            <span class="sr-only">{{tags.tallo.value}}% progress</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number"> {{tags.tallo.value}}% </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="display: none" class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.merma_cortada.value) }}">
                                            <span class="counter_tags" data-value="{{tags.merma_cortada.value}}">0</span>
                                            <small class="font-{{ revision(tags.merma_cortada.value) }}">%</small>
                                        </h3>
                                        <small>% MERMA CORTADA</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-pie-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width: {{tags.merma_cortada.value}}%;" class="progress-bar progress-bar-success {{ revision(tags.merma_cortada.value) }}">
                                            <span class="sr-only">{{tags.merma_cortada.value}}% progress</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number"> {{tags.merma_cortada.value}}% </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ umbralMostHigh(tags.cosecha.value) }}">
                                            <span class="counter_tags" data-value="{{tags.cosecha.value}}">0</span>
                                            <small class="font-{{ umbralMostHigh(tags.cosecha.value) }}">%</small>
                                        </h3>
                                        <small>COSECHA</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-bar-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width:{{tags.cosecha.value}}%;" class="progress-bar progress-bar-success {{ umbralMostHigh(tags.cosecha.value) }}">
                                            <span class="sr-only">{{tags.cosecha.value}}%</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ umbralMostHigh(tags.enfunde.value) }}">
                                            <span class="counter_tags" data-value=" {{tags.enfunde.value}}">0</span>
                                            <small class="font-{{ umbralMostHigh(tags.enfunde.value) }}">%</small>
                                        </h3>
                                        <small>ENFUNDE</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-bar-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width:  {{tags.enfunde.value}}%;" class="progress-bar progress-bar-success {{ umbralMostHigh(tags.enfunde.value) }}">
                                            <span class="sr-only"> {{tags.enfunde.value}}% change</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number">  {{tags.enfunde.value}}% </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ umbralMostHigh(tags.adm.value) }}">
                                            <span class="counter_tags" data-value=" {{tags.adm.value}}">0</span>
                                            <small class="font-{{ umbralMostHigh(tags.adm.value) }}">%</small>
                                        </h3>
                                        <small>ADM</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-bar-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width: {{tags.adm.value}}%;" class="progress-bar progress-bar-success  {{ umbralMostHigh(tags.adm.value) }}">
                                            <span class="sr-only">0% grow</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number"> {{tags.adm.value}}% </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ umbralMostHigh(tags.natural.value) }}">
                                            <span class="counter_tags" data-value=" {{tags.natural.value}}">0</span>
                                            <small class="font-{{ umbralMostHigh(tags.natural.value) }}">%</small>
                                        </h3>
                                        <small>NATURAL</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-bar-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width:  {{tags.natural.value}}%;" class="progress-bar progress-bar-success {{ umbralMostHigh(tags.natural.value) }}">
                                            <span class="sr-only"> {{tags.natural.value}}% change</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
						<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.proceso.value) }}">
                                            <span class="counter_tags" data-value=" {{tags.proceso.value}}">0</span>
                                            <small class="font-{{ revision(tags.proceso.value) }}">%</small>
                                        </h3>
                                        <small>PROCESO</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-bar-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width:  {{tags.proceso.value}}%;" class="progress-bar progress-bar-success {{ revision(tags.proceso.value) }}">
                                            <span class="sr-only"> {{tags.proceso.value}}% progress</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.cajas.value) }}">
                                            <span class="counter_tags" data-value=" {{tags.cajas.value}}">0</span>
                                            <small class="font-{{ revision(tags.cajas.value) }}"></small>
                                        </h3>
                                        <small>CAJAS ACUMULADAS</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-bar-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width:  {{tags.cajas.value}}%;" class="progress-bar progress-bar-success {{ revision(tags.cajas.value) }}">
                                            <span class="sr-only"> {{tags.cajas.value}}</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.usd.value) }}">
                                            $ <span class="counter_tags" data-value=" {{tags.usd.value}}">0</span>
                                            <small class="font-{{ revision(tags.usd.value) }}"></small>
                                        </h3>
                                        <small>DOLARES ACUMULADOS</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-bar-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width:  {{tags.usd.value}}%;" class="progress-bar progress-bar-success {{ revision(tags.usd.value) }}">
                                            <span class="sr-only"> {{tags.usd.value}}</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>