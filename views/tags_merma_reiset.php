
                    <!-- END PAGE HEADER-->
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ fontUmbral(tags.merma.value) }}">
                                            <span class="counter_tags" data-value="{{tags.merma.value}}">0</span>
                                            <small class="font-{{ fontUmbral(tags.merma.value) }}">%</small>
                                        </h3>
                                        <small>% MERMA NETA</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-pie-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width: {{tags.merma.value}}%;" class="progress-bar progress-bar-success bg-{{ fontUmbral(tags.merma.value) }}">
                                            <span class="sr-only">{{tags.merma.value}}% progress</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number"> {{tags.merma.peso | number : 2}} kg </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.tallo.value) }}">
                                            <span class="counter_tags" data-value="{{tags.tallo.value}}">0</span>
                                            <small class="font-{{ revision(tags.tallo.value) }}">%</small>
                                        </h3>
                                        <small>% TALLO</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-pie-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width: {{tags.tallo.value}}%;" class="progress-bar progress-bar-success {{ revision(tags.tallo.value) }}">
                                            <span class="sr-only">{{tags.tallo.value}}% progress</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number"> {{tags.tallo.value}}% </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ umbralMostHigh(tags.enfunde.value) }}">
                                            <span class="counter_tags" data-value="{{tags.enfunde.value}}">0</span>
                                            <small class="font-{{ umbralMostHigh(tags.enfunde.value) }}">%</small>
                                        </h3>
                                        <small>ENFUNDE</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-bar-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width:{{tags.enfunde.value}}%;" class="progress-bar progress-bar-success {{ umbralMostHigh(tags.enfunde.value) }}">
                                            <span class="sr-only">{{tags.enfunde.value}}%</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number">{{tags.enfunde.peso}} kg</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ umbralMostHigh(tags.deshoje.value) }}">
                                            <span class="counter_tags" data-value=" {{tags.deshoje.value}}">0</span>
                                            <small class="font-{{ umbralMostHigh(tags.deshoje.value) }}">%</small>
                                        </h3>
                                        <small>DESHOJE</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-bar-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width: {{tags.deshoje.value}}%;" class="progress-bar progress-bar-success  {{ umbralMostHigh(tags.deshoje.value) }}">
                                            <span class="sr-only">0% grow</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number"> {{tags.deshoje.peso}} kg</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ umbralMostHigh(tags.cosecha.value) }}">
                                            <span class="counter_tags" data-value=" {{tags.cosecha.value}}">0</span>
                                            <small class="font-{{ umbralMostHigh(tags.cosecha.value) }}">%</small>
                                        </h3>
                                        <small>COSECHA</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-bar-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width: {{tags.cosecha.value}}%;" class="progress-bar progress-bar-success  {{ umbralMostHigh(tags.cosecha.value) }}">
                                            <span class="sr-only">0% grow</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number"> {{tags.cosecha.peso}} kg</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ umbralMostHigh(tags.empacadora.value) }}">
                                            <span class="counter_tags" data-value=" {{tags.empacadora.value}}">0</span>
                                            <small class="font-{{ umbralMostHigh(tags.empacadora.value) }}">%</small>
                                        </h3>
                                        <small>EMPAQUE</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-bar-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width:  {{tags.empacadora.value}}%;" class="progress-bar progress-bar-success {{ umbralMostHigh(tags.empacadora.value) }}">
                                            <span class="sr-only"> {{tags.empacadora.value}}% change</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number">  {{tags.empacadora.peso}} kg</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ umbralMostHigh(tags.administracion.value) }}">
                                            <span class="counter_tags" data-value=" {{tags.administracion.value}}">0</span>
                                            <small class="font-{{ umbralMostHigh(tags.administracion.value) }}">%</small>
                                        </h3>
                                        <small>ADMINISTRACION</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-bar-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width:  {{tags.administracion.value}}%;" class="progress-bar progress-bar-success {{ umbralMostHigh(tags.administracion.value) }}">
                                            <span class="sr-only"> {{tags.administracion.value}}% change</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number">{{tags.administracion.peso}} kg</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ umbralMostHigh(tags.animales.value) }}">
                                            <span class="counter_tags" data-value=" {{tags.animales.value}}">0</span>
                                            <small class="font-{{ umbralMostHigh(tags.animales.value) }}">%</small>
                                        </h3>
                                        <small>ANIMALES</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-bar-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width:  {{tags.animales.value}}%;" class="progress-bar progress-bar-success {{ umbralMostHigh(tags.animales.value) }}">
                                            <span class="sr-only"> {{tags.animales.value}}% progress</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number">{{tags.animales.peso}} kg</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ umbralMostHigh(tags.amarrador.value) }}">
                                            <span class="counter_tags" data-value=" {{tags.amarrador.value}}">0</span>
                                            <small class="font-{{ umbralMostHigh(tags.amarrador.value) }}">%</small>
                                        </h3>
                                        <small>AMARRADOR</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-bar-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width:  {{tags.amarrador.value}}%;" class="progress-bar progress-bar-success {{ umbralMostHigh(tags.amarrador.value) }}">
                                            <span class="sr-only"> {{tags.amarrador.value}}% progress</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number">{{tags.amarrador.peso}} kg</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>