
                    <!-- END PAGE HEADER-->
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.racimo.value) }}">
                                            <span class="counter_tags" data-value="{{tags.racimo.value}}">0</span>
                                            <small class="font-{{ revision(tags.racimo.value) }}"></small>
                                        </h3>
                                        <small>PESO RACIMO PROM</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-pie-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width: {{tags.racimo.value}}%;" class="progress-bar progress-bar-success {{ revision(tags.racimo.value) }}">
                                            <span class="sr-only">{{tags.racimo.value}}</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number"> {{tags.racimo.value}} </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.ratio_cortado.value) }}">
                                            <span class="counter_tags" data-value=" {{tags.ratio_cortado.value}}">0</span>
                                            <small class="font-{{ revision(tags.ratio_cortado.value) }}"></small>
                                        </h3>
                                        <small>RATIO CORTADO</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-bar-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width:  {{tags.ratio_cortado.value}}%;" class="progress-bar progress-bar-success {{ revision(tags.ratio_cortado.value) }}">
                                            <span class="sr-only"> {{tags.ratio_cortado.value}}% change</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number">  {{tags.ratio_cortado.value}} </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.ratio_procesado.value) }}">
                                            <span class="counter_tags" data-value=" {{tags.ratio_procesado.value}}">0</span>
                                            <small class="font-{{ revision(tags.ratio_procesado.value) }}"></small>
                                        </h3>
                                        <small>RATIO PROCESADO</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-bar-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width: {{tags.ratio_procesado.value}}%;" class="progress-bar progress-bar-success  {{ revision(tags.ratio_procesado.value) }}">
                                            <!-- <span class="sr-only"></span> -->
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number"> {{tags.ratio_procesado.value}}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.recusados.value) }}">
                                            <span class="counter_tags" data-value="{{tags.recusados.value}}">0</span>
                                            <small class="font-{{ revision(tags.recusados.value) }}">%</small>
                                        </h3>
                                        <small>% RACIMOS RECUSADOS</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-bar-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width:{{tags.recusados.value}}%;" class="progress-bar progress-bar-success {{ revision(tags.recusados.value) }}">
                                            <span class="sr-only">{{tags.recusados.value}}%</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.ratooning.value) }}">
                                            <span class="counter_tags" data-value="{{tags.ratooning.value}}">0</span>
                                            <small class="font-{{ revision(tags.ratooning.value) }}"></small>
                                        </h3>
                                        <small>RATOONING</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-bar-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width: {{tags.ratooning.value}}%;" class="progress-bar progress-bar-success {{ revision(tags.ratooning.value) }}">
                                            <span class="sr-only">{{tags.ratooning.value}}%</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.merma_cortada.value) }}">
                                            <span class="counter_tags" data-value=" {{tags.merma_cortada.value}}">0</span>
                                            <small class="font-{{ revision(tags.merma_cortada.value) }}">%</small>
                                        </h3>
                                        <small>% MERMA CORTADA</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-bar-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width:  {{tags.merma_cortada.value}}%;" class="progress-bar progress-bar-success {{ revision(tags.merma_cortada.value) }}">
                                            <span class="sr-only"> {{tags.merma_cortada.value}}%</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
						<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.merma_procesada.value) }}">
                                            <span class="counter_tags" data-value=" {{tags.merma_procesada.value}}">0</span>
                                            <small class="font-{{ revision(tags.merma_procesada.value) }}">%</small>
                                        </h3>
                                        <small>% MERMA PROCESADA</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-bar-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width:  {{tags.merma_procesada.value}}%;" class="progress-bar progress-bar-success {{ revision(tags.merma_procesada.value) }}">
                                            <span class="sr-only"> {{tags.merma_procesada.value}}%</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
						<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.edad.value) }}">
                                            <span class="counter_tags" data-value=" {{tags.edad.value}}">0</span>
                                            <small class="font-{{ revision(tags.edad.value) }}"></small>
                                        </h3>
                                        <small>PROM EDAD COSECHA</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-bar-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width:  {{tags.edad.value}}%;" class="progress-bar progress-bar-success {{ revision(tags.edad.value) }}">
                                            <span class="sr-only"> {{tags.edad.value}}</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>