
                    <!-- END PAGE HEADER-->
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.cajas.value) }}">
                                            <span class="counter_tags" data-value="{{tags.cajas.value}}">{{tags.cajas.value}}</span>
                                            <small class="font-{{ revision(tags.cajas.value) }}"></small>
                                        </h3>
                                        <small>PESO PROM CAJAS CONV</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-pie-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width: {{tags.cajas.value}}%;" class="progress-bar progress-bar-success {{ revision(tags.cajas.value) }}">
                                            <span class="sr-only">{{tags.cajas.value}}</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number"> {{tags.cajas.value}} </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 hide">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.otras_cajas.value) }}">
                                            <span class="counter_tags" data-value="{{tags.otras_cajas.value}}">{{tags.otras_cajas.value}}</span>
                                            <small class="font-{{ revision(tags.otras_cajas.value) }}"></small>
                                        </h3>
                                        <small>PESO PROM OTRAS CAJAS</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-bar-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width:  {{tags.otras_cajas.value}}%;" class="progress-bar progress-bar-success {{ revision(tags.otras_cajas.value) }}">
                                            <span class="sr-only"> {{tags.otras_cajas.value}}</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.racimo.value) }}">
                                            <span class="counter_tags" data-value="{{tags.racimo.value}}">{{tags.racimo.value}}</span>
                                            <small class="font-{{ revision(tags.racimo.value) }}"></small>
                                        </h3>
                                        <small>PESO PROM RACIMO</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-pie-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width: {{tags.racimo.value}}%;" class="progress-bar progress-bar-success {{ revision(tags.racimo.value) }}">
                                            <span class="sr-only">{{tags.racimo.value}}</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number"> {{tags.racimo.value}} </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.calibracion.value) }}">
                                            <span class="counter_tags" data-value="{{tags.calibracion.value}}">{{tags.calibracion.value}}</span>
                                            <small class="font-{{ revision(tags.calibracion.value) }}"></small>
                                        </h3>
                                        <small>CALIBRACIÓN</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-bar-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width:  {{tags.calibracion.value}}%;" class="progress-bar progress-bar-success {{ revision(tags.calibracion.value) }}">
                                            <span class="sr-only"> {{tags.calibracion.value}}% change</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number">  {{tags.calibracion.value}} </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.edad.value) }}">
                                            <span class="counter_tags" data-value="{{tags.edad.value}}">{{tags.edad.value}}</span>
                                            <small class="font-{{ revision(tags.edad.value) }}"></small>
                                        </h3>
                                        <small>PROM EDAD COSECHA</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-bar-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width:  {{tags.edad.value}}%;" class="progress-bar progress-bar-success {{ revision(tags.edad.value) }}">
                                            <span class="sr-only"> {{tags.edad.value}}</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 hide">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.ratio_cortado.value) }}">
                                            <span class="counter_tags" data-value="{{tags.ratio_cortado.value}}">{{tags.ratio_cortado.value}}</span>
                                            <small class="font-{{ revision(tags.ratio_cortado.value) }}"></small>
                                        </h3>
                                        <small>RATIO CORTADO</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-bar-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width:  {{tags.ratio_cortado.value}}%;" class="progress-bar progress-bar-success {{ revision(tags.ratio_cortado.value) }}">
                                            <span class="sr-only"> {{tags.ratio_cortado.value}}% change</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number">  {{tags.ratio_cortado.value}} </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.ratio_procesado.value) }}">
                                            <span class="counter_tags" data-value="{{tags.ratio_procesado.value}}">{{tags.ratio_procesado.value}}</span>
                                            <small class="font-{{ revision(tags.ratio_procesado.value) }}"></small>
                                        </h3>
                                        <small>RATIO PROCESADO</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-bar-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width: {{tags.ratio_procesado.value}}%;" class="progress-bar progress-bar-success  {{ revision(tags.ratio_procesado.value) }}">
                                            <!-- <span class="sr-only"></span> -->
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title">RATIO CORTADO</div>
                                        <div class="status-number"> {{tags.ratio_cortado.value}}</div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!--<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.recusados.value) }}">
                                            <span class="counter_tags" data-value="{{tags.recusados.value}}">0</span>
                                            <small class="font-{{ revision(tags.recusados.value) }}">%</small>
                                        </h3>
                                        <small>% RACIMOS RECUSADOS</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-bar-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width:{{tags.recusados.value}}%;" class="progress-bar progress-bar-success {{ revision(tags.recusados.value) }}">
                                            <span class="sr-only">{{tags.recusados.value}}%</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number"></div>
                                    </div>
                                </div>
                            </div>
                        </div>-->
                        
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 hide">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.merma_cosechada.value) }}">
                                            <span class="counter_tags" data-value="{{tags.merma_cosechada.value}}">{{tags.merma_cosechada.value}}</span>
                                            <small class="font-{{ revision(tags.merma_cosechada.value) }}">%</small>
                                        </h3>
                                        <small>% MERMA COSECHADA</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-bar-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width:  {{tags.merma_cosechada.value}}%;" class="progress-bar progress-bar-success {{ revision(tags.merma_cosechada.value) }}">
                                            <span class="sr-only"> {{tags.merma_cosechada.value}}%</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

						<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.merma_procesada.value) }}">
                                            <span class="counter_tags" data-value="{{tags.merma_procesada.value}}">{{tags.merma_procesada.value}}</span>
                                            <small class="font-{{ revision(tags.merma_procesada.value) }}">%</small>
                                        </h3>
                                        <small>% MERMA PROCESADA</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-bar-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width:  {{tags.merma_procesada.value}}%;" class="progress-bar progress-bar-success {{ revision(tags.merma_procesada.value) }}">
                                            <span class="sr-only"> {{tags.merma_procesada.value}}%</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title">% MERMA CORTADA</div>
                                        <div class="status-number">{{tags.merma_cosechada.value}} %</div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.merma_neta.value) }}">
                                            <span class="counter_tags" data-value="{{tags.merma_neta.value}}">{{tags.merma_neta.value}}</span>
                                            <small class="font-{{ revision(tags.merma_neta.value) }}">%</small>
                                        </h3>
                                        <small>% MERMA NETA (MUESTREO)</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-bar-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width:  {{tags.merma_neta.value}}%;" class="progress-bar progress-bar-success {{ revision(tags.merma_neta.value) }}">
                                            <span class="sr-only"> {{tags.merma_neta.value}}%</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.recobro.value) }}">
                                            <span class="counter_tags" data-value="{{tags.recobro.value}}">{{tags.recobro.value}}</span>
                                            <small class="font-{{ revision(tags.recobro.value) }}">%</small>
                                        </h3>
                                        <small>RECOBRO</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-bar-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width:  {{tags.recobro.value}}%;" class="progress-bar progress-bar-success {{ revision(tags.recobro.value) }}">
                                            <span class="sr-only"> {{tags.recobro.value}}</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.rac_cos_ha_sem.value) }}">
                                            <span class="counter_tags" data-value="{{tags.rac_cos_ha_sem.value}}">{{tags.rac_cos_ha_sem.value}}</span>
                                            <small class="font-{{ revision(tags.rac_cos_ha_sem.value) }}"></small>
                                        </h3>
                                        <small>RAC / HA SEM</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-bar-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width:  {{tags.rac_cos_ha_sem.value}}%;" class="progress-bar progress-bar-success {{ revision(tags.rac_cos_ha_sem.value) }}">
                                            <span class="sr-only"> {{tags.rac_cos_ha_sem.value}}</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title">RAC / HA AÑO (PROYECCION)</div>
                                        <div class="status-number">{{tags.rac_cos_ha_anio.value}}</div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.cajas_ha_sem.value) }}">
                                            <span class="counter_tags" data-value=" {{tags.cajas_ha_sem.value}}">{{tags.cajas_ha_sem.value}}</span>
                                            <small class="font-{{ revision(tags.cajas_ha_sem.value) }}"></small>
                                        </h3>
                                        <small>CAJAS / HA SEM</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-bar-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width:  {{tags.cajas_ha_sem.value}}%;" class="progress-bar progress-bar-success {{ revision(tags.cajas_ha_sem.value) }}">
                                            <span class="sr-only"> {{tags.cajas_ha_sem.value}}</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title">CAJAS / HA AÑO (PROYECCION)</div>
                                        <div class="status-number">{{tags.cajas_ha_anio.value}}</div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.enfunde.value) }}">
                                            <span class="counter_tags" data-value="{{tags.enfunde.value}}">{{tags.enfunde.value}}</span>
                                            <small class="font-{{ revision(tags.enfunde.value) }}"></small>
                                        </h3>
                                        <small>ENFUNDE POR HA</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-bar-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width:  {{tags.enfunde.value}}%;" class="progress-bar progress-bar-success {{ revision(tags.enfunde.value) }}">
                                            <span class="sr-only"> {{tags.enfunde.value}}</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 hide">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.merma_kg.value) }}">
                                            <span class="counter_tags" data-value=" {{tags.merma_kg.value}}">0</span>
                                            <small class="font-{{ revision(tags.merma_kg.value) }}"></small>
                                        </h3>
                                        <small>MERMA (KG)</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-bar-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width:  {{tags.merma_kg.value}}%;" class="progress-bar progress-bar-success {{ revision(tags.merma_kg.value) }}">
                                            <span class="sr-only"> {{tags.merma_kg.value}}</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 hide">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.merma_cajas.value) }}">
                                            <span class="counter_tags" data-value=" {{tags.merma_cajas.value}}">0</span>
                                            <small class="font-{{ revision(tags.merma_cajas.value) }}"></small>
                                        </h3>
                                        <small>MERMA CAJAS</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-bar-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width:  {{tags.merma_cajas.value}}%;" class="progress-bar progress-bar-success {{ revision(tags.merma_cajas.value) }}">
                                            <span class="sr-only"> {{tags.merma_cajas.value}}</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 hide">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.merma_dolares.value) }}">
                                            <span class="counter_tags" data-value="{{tags.merma_dolares.value}}">0</span>
                                            <small class="font-{{ revision(tags.merma_dolares.value) }}"></small>
                                        </h3>
                                        <small>MERMA DOLARES</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-bar-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width:  {{tags.merma_dolares.value}}%;" class="progress-bar progress-bar-success {{ revision(tags.merma_dolares.value) }}">
                                            <span class="sr-only"> {{tags.merma_dolares.value}}</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.ratooning.value) }}">
                                            <span class="counter_tags" data-value="{{tags.ratooning.value}}">{{tags.ratooning.value}}</span>
                                            <small class="font-{{ revision(tags.ratooning.value) }}"></small>
                                        </h3>
                                        <small>RATOONING</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-bar-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width: {{tags.ratooning.value}}%;" class="progress-bar progress-bar-success {{ revision(tags.ratooning.value) }}">
                                            <span class="sr-only">{{tags.ratooning.value}}%</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>