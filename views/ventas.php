<style>
	.margin-5 {
		margin-bottom: 5px !important;
	}
	table > th,td{
		text-align: center !important;
	}
	.center-th {
		text-align: center !important;
	}
	.left-td {
		text-align: left !important;
		padding-left: 10px !important;
	}
	table {
		cursor: pointer !important;
	}
	.anchoTable {
		width: 100px;
	}
</style>
<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.0.3/socket.io.js"></script>
<div ng-controller="myController" ng-cloak>
     <div class="page-bar" ng-init="getLastDay()">
         <ul class="page-breadcrumb">
             <li>
                 <i class="icon-home"></i>
                 <a>Ventas</a>
                 <i class="fa fa-angle-right"></i>
             </li>
         </ul>
         <div class="page-toolbar">
            <label for="">
                Año : 
            </label>
            <select ng-change="changeYear()" name="year" id="year" ng-model="filters.year" style="margin: 2px;height: 36px;">
                <option ng-repeat="value in anios" value="{{value}}" ng-selected="value == filters.year">{{value}}</option>
            </select>
            <!--<ng-calendarapp search="search"></ng-calendarapp>-->
        </div>
    </div>

    <div class="portlet box green">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-gift"></i> Ventas </div>
            <div class="tools">
            </div>
        </div>
        <div class="portlet-body">
            <div class="panel-group accordion" id="accordion3">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a ng-click="getDataDia()" class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_1" aria-expanded="false"> Por Dia </a>
                        </h4>
                    </div>
                    <div id="collapse_3_1" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                        <div class="panel-body">
                            <div class="pull-right">
                                <label>Sem</label>
                                <select class="input-sm" ng-model="filters.semana" ng-change="getDataDia()">
                                    <option ng-repeat="(key, value) in semanas" value="{{key}}" ng-selected="filters.semana == key">{{value}}</option>
                                </select>
                            </div>
                            <div style="margin-top: 30px;">
                                <div id="lineal_dia" style="height: 400px;">

                                </div>
                            </div>
                            <div class="row" style="margin-top: 20px;">
                                <div class="col-md-6">
                                    <div class="table-responsive table-scrollable">
                                        <table class="table table-striped table-bordered table-hover">
                                            <thead>
                                                <th class="center-th"></th>
                                                <th class="center-th" ng-repeat="value in rutas_dia | orderObjectBy:'id':false"> {{ (value.label == '002') ? 'RUTA 1' : 'RUTA 2' }}</th>
                                                <th class="center-th">Total</th>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <th class="center-th">MIN</th>
                                                    <th class="center-th" ng-repeat="value in rutas_dia | orderObjectBy:'id':false">{{ totales_dia.min[value.label] | number: 2 }}</th>
                                                    <th class="center-th">{{ totales_dia.min | sum | number: 2 }}</th>
                                                </tr>
                                                <tr>
                                                    <th class="center-th">MAX</th>
                                                    <th class="center-th" ng-repeat="value in rutas_dia | orderObjectBy:'id':false">{{ totales_dia.max[value.label] | number: 2 }}</th>
                                                    <th class="center-th">{{ totales_dia.max | sum | number: 2 }}</th>
                                                </tr>
                                                <tr>
                                                    <th class="center-th">PROM</th>
                                                    <th class="center-th" ng-repeat="value in rutas_dia | orderObjectBy:'id':false">{{ totales_dia.prom[value.label] | number: 2 }}</th>
                                                    <th class="center-th">{{ totales_dia.prom | sum | number: 2 }}</th>
                                                </tr>
                                                <tr>
                                                    <th class="center-th">TOTAL</th>
                                                    <th class="center-th" ng-repeat="value in rutas_dia | orderObjectBy:'id':false">{{ totales_dia.total[value.label] | number: 2}}</th>
                                                    <th class="center-th">{{ totales_dia.total | sum | number: 2 }}</th>
                                                </tr>
                                                <tr ng-repeat="row in data.dia | orderObjectBy:'num_dia':false">
                                                    <td>{{ row.dia }}</td>
                                                    <td ng-repeat="value in rutas_dia | orderObjectBy:'id':false">
                                                        {{ (row.rutas[value.id] > 0) ? (row.rutas[value.id] | number: 2) : '' }}
                                                    </td>
                                                    <td>{{ (row.total > 0) ? (row.total | number: 2) : '' }}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div id="pastel_dia" style="height: 400px;">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a ng-click="getDataSemana()" class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_2" aria-expanded="false"> Por Semana </a>
                        </h4>
                    </div>
                    <div id="collapse_3_2" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                        <div class="panel-body">
                            <div style="margin-top: 30px;">
                                <div id="lineal_semana" style="height: 400px;">

                                </div>
                            </div>
                            <div class="row" style="margin-top: 20px;">
                                <div class="col-md-6">
                                    <div class="table-responsive table-scrollable">
                                        <table class="table table-striped table-bordered table-hover">
                                            <thead>
                                                <th class="center-th"></th>
                                                <th class="center-th" ng-repeat="value in rutas_semana | orderObjectBy:'id':false"> {{ (value.label == '002') ? 'RUTA 1' : 'RUTA 2' }}</th>
                                                <th class="center-th">Total</th>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <th class="center-th">MIN</th>
                                                    <th class="center-th" ng-repeat="value in rutas_semana | orderObjectBy:'id':false">{{ totales_semana.min[value.label] | number: 2 }}</th>
                                                    <th class="center-th">{{ totales_semana.min | sum | number: 2 }}</th>
                                                </tr>
                                                <tr>
                                                    <th class="center-th">MAX</th>
                                                    <th class="center-th" ng-repeat="value in rutas_semana | orderObjectBy:'id':false">{{ totales_semana.max[value.label] | number: 2 }}</th>
                                                    <th class="center-th">{{ totales_semana.max | sum | number: 2 }}</th>
                                                </tr>
                                                <tr>
                                                    <th class="center-th">PROM</th>
                                                    <th class="center-th" ng-repeat="value in rutas_semana | orderObjectBy:'id':false">{{ totales_semana.prom[value.label] | number: 2 }}</th>
                                                    <th class="center-th">{{ totales_semana.prom | sum | number: 2 }}</th>
                                                </tr>
                                                <tr>
                                                    <th class="center-th">TOTAL</th>
                                                    <th class="center-th" ng-repeat="value in rutas_semana | orderObjectBy:'id':false">{{ totales_semana.total[value.label] | number: 2}}</th>
                                                    <th class="center-th">{{ totales_semana.total | sum | number: 2 }}</th>
                                                </tr>
                                                <tr ng-repeat="row in data.semana | orderObjectBy:'semana':false">
                                                    <td>{{ row.semana }}</td>
                                                    <td ng-repeat="value in rutas_semana | orderObjectBy:'id':false">{{ (row.rutas[value.id] > 0) ? (row.rutas[value.id] | number: 2) : '' }}</td>
                                                    <td>{{ (row.total > 0) ? (row.total | number: 2) : '' }}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div id="pastel_semana" style="height: 400px;">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a ng-click="getDataMes()" class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_3" aria-expanded="false"> Por Mes </a>
                        </h4>
                    </div>
                    <div id="collapse_3_3" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                        <div class="panel-body">
                            <div style="margin-top: 30px;">
                                <div id="lineal_mes" style="height: 400px;">

                                </div>
                            </div>
                            <div class="row" style="margin-top: 20px;">
                                <div class="col-md-6">
                                    <div class="table-responsive table-scrollable">
                                        <table class="table table-striped table-bordered table-hover">
                                            <thead>
                                                <th class="center-th"></th>
                                                <th class="center-th" ng-repeat="value in rutas_mes | orderObjectBy:'id':false"> {{ (value.label == '002') ? 'RUTA 1' : 'RUTA 2' }}</th>
                                                <th class="center-th">Total</th>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <th class="center-th">MIN</th>
                                                    <th class="center-th" ng-repeat="value in rutas_mes | orderObjectBy:'id':false">{{ totales_mes.min[value.label] | number: 2 }}</th>
                                                    <th class="center-th">{{ totales_mes.min | sum | number: 2 }}</th>
                                                </tr>
                                                <tr>
                                                    <th class="center-th">MAX</th>
                                                    <th class="center-th" ng-repeat="value in rutas_mes | orderObjectBy:'id':false">{{ totales_mes.max[value.label] | number: 2 }}</th>
                                                    <th class="center-th">{{ totales_mes.max | sum | number: 2 }}</th>
                                                </tr>
                                                <tr>
                                                    <th class="center-th">PROM</th>
                                                    <th class="center-th" ng-repeat="value in rutas_mes | orderObjectBy:'id':false">{{ totales_mes.prom[value.label] | number: 2 }}</th>
                                                    <th class="center-th">{{ totales_mes.prom | sum | number: 2 }}</th>
                                                </tr>
                                                <tr>
                                                    <th class="center-th">TOTAL</th>
                                                    <th class="center-th" ng-repeat="value in rutas_mes | orderObjectBy:'id':false">{{ totales_mes.total[value.label] | number: 2}}</th>
                                                    <th class="center-th">{{ totales_mes.total | sum | number: 2 }}</th>
                                                </tr>
                                                <tr ng-repeat="row in data.mes | orderObjectBy:'mes':false">
                                                    <td>{{ row.name }}</td>
                                                    <td ng-repeat="value in rutas_mes | orderObjectBy:'id':false">{{ (row.rutas[value.id] > 0) ? (row.rutas[value.id] | number: 2) : '' }}</td>
                                                    <td>{{ (row.total > 0) ? (row.total | number: 2) : '' }}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div id="pastel_mes" style="height: 400px;">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- table table-striped table-bordered table-hover table-header-fixed dataTable no-footer -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a ng-click="getDataAnio()" class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_4" aria-expanded="false"> Por Año </a>
                        </h4>
                    </div>
                    <div id="collapse_3_4" class="panel-collapse collapse" aria-expanded="true" style="">
                        <div class="panel-body">
                            <div style="margin-top: 30px;">
                                <div id="lineal_anio" style="height: 400px;">

                                </div>
                            </div>
                            <div class="row" style="margin-top: 20px;">
                                <div class="col-md-6">
                                    <div class="table-responsive table-scrollable">
                                        <table class="table table-striped table-bordered table-hover">
                                            <thead>
                                                <th class="center-th"></th>
                                                <th class="center-th" ng-repeat="value in rutas_anio | orderObjectBy:'id':false"> {{ (value.label == '002') ? 'RUTA 1' : 'RUTA 2' }}</th>
                                                <th class="center-th">Total</th>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <th class="center-th">MIN</th>
                                                    <th class="center-th" ng-repeat="value in rutas_anio | orderObjectBy:'id':false">{{ totales_anio.min[value.label] | number: 2 }}</th>
                                                    <th class="center-th">{{ totales_anio.min | sum | number: 2 }}</th>
                                                </tr>
                                                <tr>
                                                    <th class="center-th">MAX</th>
                                                    <th class="center-th" ng-repeat="value in rutas_anio | orderObjectBy:'id':false">{{ totales_anio.max[value.label] | number: 2 }}</th>
                                                    <th class="center-th">{{ totales_anio.max | sum | number: 2 }}</th>
                                                </tr>
                                                <tr>
                                                    <th class="center-th">PROM</th>
                                                    <th class="center-th" ng-repeat="value in rutas_anio | orderObjectBy:'id':false">{{ totales_anio.prom[value.label] | number: 2 }}</th>
                                                    <th class="center-th">{{ totales_anio.prom | sum | number: 2 }}</th>
                                                </tr>
                                                <tr>
                                                    <th class="center-th">TOTAL</th>
                                                    <th class="center-th" ng-repeat="value in rutas_anio | orderObjectBy:'id':false">{{ totales_anio.total[value.label] | number: 2}}</th>
                                                    <th class="center-th">{{ totales_anio.total | sum | number: 2 }}</th>
                                                </tr>
                                                <tr ng-repeat="row in data.anio | orderObjectBy:'anio':false">
                                                    <td>{{ row.anio }}</td>
                                                    <td ng-repeat="value in rutas_anio | orderObjectBy:'id':false">{{ (row.rutas[value.id] > 0) ? (row.rutas[value.id] | number: 2) : '' }}</td>
                                                    <td>{{ (row.total > 0) ? (row.total | number: 2) : '' }}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div id="pastel_anio" style="height: 400px;">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
