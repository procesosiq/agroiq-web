const { readdirSync } = require('fs')
const path = require('path')
//const UglifyJSPlugin = require('uglifyjs-webpack-plugin');

function getFilesFolder(folder){
    let files = readdirSync(folder)
            .filter(
                (file) => file.match(/.*\.js$/)
            )
            .map((file) => {
                return {
                    name: file.substring(0, file.length - 3),
                    path: folder + file
                }
            }).reduce((memo, file) => {
                memo[file.name] = file.path
                return memo;
            }, {})
    return files
}

const config_folder = function(from_folder, to_folder){
    const entrys = getFilesFolder(`${from_folder}`)
    return {
        entry : entrys,
        output : {
            path: __dirname + `${to_folder}`,
            filename: '[name].js',
        },
        resolve : {
            modules : [
                'node_modules',
                __dirname
            ]
        },
        module : {
            rules : [
                {
                    test: /\.css$/,
                    use: [ 'style-loader', 'css-loader' ]
                },
                {
                    loader :'babel-loader',
                    query: {
                        presets: ['env', 'es2017', 'react'],
                        plugins: ['transform-class-properties', 'transform-es2015-arrow-functions'],
                    },
                    test : /\.js$/,
                    exclude : /node_modules/
                },
                {
                    test: /.jsx?$/,
                    loader: 'babel-loader',
                    exclude: /node_modules/,
                    query: {
                        presets: ['es2017', 'react']
                    }
                }
            ]
        },
        devServer: {
            contentBase: "./dist"
        }
    }
}

const config = [
    config_folder('./js_modules/', '/js/'),
    config_folder('./js_modules/agroaereo/', '/js/agroaereo/'),
    config_folder('./js_modules/agroban/', '/js/agroban/'),
    config_folder('./js_modules/arregui/', '/js/arregui/'),
    config_folder('./js_modules/clementina/', '/js/clementina/'),
    config_folder('./js_modules/eugenia/', '/js/eugenia/'),
    config_folder('./js_modules/marcel/', '/js/marcel/'),
    config_folder('./js_modules/mario/', '/js/mario/'),
    config_folder('./js_modules/marlon/', '/js/marlon/'),
    config_folder('./js_modules/marun/', '/js/marun/'),
    config_folder('./js_modules/orodelti/', '/js/orodelti/'),
    config_folder('./js_modules/palmar/', '/js/palmar/'),
    config_folder('./js_modules/quintana/', '/js/quintana/'),
    config_folder('./js_modules/reiset/', '/js/reiset/'),
    config_folder('./js_modules/sumifru/', '/js/sumifru/'),
    config_folder('./js_modules/rufcorp/', '/js/rufcorp/'),
]
module.exports = config